use fxhash::FxHashMap;
use html2md::parse_html;
use log::{debug, error, warn};
use once_cell::sync::Lazy;
use proc_macro2::{TokenStream};
use quote::{ToTokens};
use regex::{Regex, Replacer};
use reqwest::{blocking::Client, StatusCode};
use scraper::Selector;

use crate::{
    declaration::declaration_ident,
    items::functions::ExtensionType,
    origin::Origin,
    source::{Source, Value},
};
use std::{fmt::Display, time::Instant};

static SELECTOR_HEADER: Lazy<Selector> =
    Lazy::new(|| Selector::parse("div#header > div.sectionbody").unwrap());
static SELECTOR_SECTIONS: Lazy<Selector> =
    Lazy::new(|| Selector::parse("div#content > div.sect1").unwrap());
static REGEX_INTRA_LINK_FIELD: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r#"\[([a-zA-Z0-9_]+)\]\(([a-zA-Z0-9_]+.html)\)::`([a-zA-Z0-9_]+)`"#).unwrap()
});
static REGEX_INTRA_LINK_WEIRD: Lazy<Regex> =
    Lazy::new(|| Regex::new(r#"`\[([a-zA-Z0-9_]+)\]\(([a-zA-Z0-9_]+.html)\)`"#).unwrap());
static REGEX_INTRA_LINK: Lazy<Regex> =
    Lazy::new(|| Regex::new(r#"\[([a-zA-Z0-9_]+)\]\(([a-zA-Z0-9_]+.html)\)"#).unwrap());
static REGEX_SYMBOL: Lazy<Regex> = Lazy::new(|| Regex::new(r#"`([a-zA-Z0-9_]+)`"#).unwrap());

pub struct DocCommentGen {
    client: Client,
    cache: FxHashMap<String, Option<String>>,
    version: (u32, u32, u32),
}

impl DocCommentGen {
    pub fn new(version: (u32, u32, u32)) -> DocCommentGen {
        DocCommentGen {
            client: reqwest::blocking::Client::new(),
            cache: FxHashMap::default(),
            version,
        }
    }

    pub fn def<'a, T>(
        &mut self,
        full_name: Option<&str>,
        description: T,
        origin: Option<&Origin>,
        this: Value<'a>,
        source: &'a Source,
    ) -> String
    where
        T: Display,
    {
        let doc = full_name.and_then(|name| {
            let url = format!(
                "https://www.khronos.org/registry/vulkan/specs/{}.{}-extensions/man/html/{}.html",
                self.version.0, self.version.1, name,
            );

            if let Some(doc) = self.cache.get(&url) {
                return doc.clone();
            }

            let start = Instant::now();
            let response = self.client.get(&url).send();

            if let Ok(doc) = response {
                if doc.status() == StatusCode::NOT_FOUND {
                    warn!("No documentation for {}", full_name.unwrap());
                    self.cache.insert(url, None);
                    None
                } else {
                    if let Ok(content) = doc.text() {
                        let html = scraper::Html::parse_document(&content);

                        let mut header = html.select(&*SELECTOR_HEADER);
                        let body = html.select(&*SELECTOR_SECTIONS).collect::<Vec<_>>();

                        let mut out = String::new();
                        if let Some(head) = header.next() {
                            out.push_str(&parse_html(&head.html()));
                            out.push('\n');
                        }

                        for section in body.iter().take(if body.len() == 5 { 3 } else { 4 }) {
                            out.push_str(&parse_html(&section.html()));
                            out.push('\n');
                        }

                        let out = process_markdown(source, this, &out);
                        
                        debug!("{} took {:?}", url, start.elapsed());

                        self.cache.insert(url, Some(out.clone()));

                        Some(out)
                    } else {
                        error!("Failed to get content for {}", full_name.unwrap());
                        None
                    }
                }
            } else {
                warn!("No documentation for {}", full_name.unwrap());
                self.cache.insert(url, None);
                None
            }
        });

        let man_page = if matches!(origin, Some(Origin::External { .. })) || full_name.is_none() {
            "<s>Vulkan Manual Page</s>".into()
        } else {
            format!("[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/{}.{}-extensions/man/html/{}.html))", 
            self.version.0,
            self.version.1,
            full_name.unwrap())
        };

        format!(
            "{} · {} <br/> {}",
            man_page,
            description,
            doc.as_ref().map(|s| s as &str).unwrap_or("")
        )
    }

    pub fn provided_by(&self, target_location: &Origin) -> String {
        format!(
            "Provided by [`crate::{}`]",
            target_location.module_path_pretty()
        )
    }

    pub fn major_version(&self) -> u32 {
        self.version.0
    }

    pub fn minor_version(&self) -> u32 {
        self.version.1
    }

    pub fn patch_version(&self) -> u32 {
        self.version.2
    }
}

pub fn process_markdown<'a>(source: &'a Source, this: Value<'a>, md: &'a String) -> String {
    let out = REGEX_INTRA_LINK_FIELD.replace_all(&md, ReplacerLinkField { source });
    let out = REGEX_INTRA_LINK_WEIRD.replace_all(&out, ReplaceIntraLink { source });
    let out = REGEX_INTRA_LINK.replace_all(&out, ReplaceIntraLink { source });
    let out = REGEX_SYMBOL.replace_all(&out, ReplacerSymbol { source, this });

    out.into_owned()
}

fn value_to_ident<'a>(value: Value<'a>) -> TokenStream {
    match value {
        Value::Alias(alias) => alias.alias.ident().to_token_stream(),
        Value::Structure(structure) => structure.name.ident().to_token_stream(),
        Value::Function(func) => match func.extension_type {
            Some(ExtensionType::Device) => {
                let ident = func.name.pretty_ident();

                quote::quote! {
                    DeviceLoader::#ident
                }
            }
            Some(ExtensionType::Instance) => {
                let ident = func.name.pretty_ident();

                quote::quote! {
                    InstanceLoader::#ident
                }
            }
            None => func.name.pointer_ident().to_token_stream(),
        },
        Value::FunctionPointer(func) => func.name.pointer_ident().to_token_stream(),
        Value::Handle(handle) => handle.name.ident().to_token_stream(),
        Value::Const(cst) => cst.ident().to_token_stream(),
        Value::BaseType(ty) => ty.name.ident().to_token_stream(),
        Value::Enum(en) => en.kind.enum_ident().to_token_stream(),
        Value::EnumVariant(en, var) => {
            let kind = en.kind.enum_ident();
            let variant = en.variants[var].name.ident();
            quote::quote! {
                #kind::#variant
            }
        },
        Value::None => panic!("Found `Value::None` with linked doc"),
    }
}

struct ReplacerLinkField<'s> {
    source: &'s Source,
}

impl<'s> Replacer for ReplacerLinkField<'s> {
    fn replace_append(&mut self, caps: &regex::Captures, dst: &mut String) {
        if let Some(value) = self.source.find(caps.get(1).unwrap().as_str()) {
            let ty = value_to_ident(value);
            let field = declaration_ident(caps.get(3).unwrap().as_str()).unwrap();

            caps.expand(
                &format!("[`crate::vk::{}::{}`]", ty, field).replace(' ', ""),
                dst,
            );
        } else {
            let ident = caps.get(1).unwrap().as_str();
            let page = caps.get(2).unwrap().as_str();
            let field = caps.get(3).unwrap().as_str();

            caps.expand(&format!(
                "[{}](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/{})::`{}`",
                ident,
                page,
                field
            ), dst);
        }
    }
}

struct ReplaceIntraLink<'s> {
    source: &'s Source,
}

impl<'s> Replacer for ReplaceIntraLink<'s> {
    fn replace_append(&mut self, caps: &regex::Captures, dst: &mut String) {
        let text = caps.get(1).unwrap().as_str();
        if let Some(_) = self.source.find(text) {
            caps.expand(&format!("`{}`", text).replace(' ', ""), dst);
        } else {
            let ident = caps.get(1).unwrap().as_str();
            let page = caps.get(2).unwrap().as_str();

            caps.expand(
                &format!(
                "[{}](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/{})",
                ident,
                page,
            ),
                dst,
            );
        }
    }
}

struct ReplacerSymbol<'s> {
    source: &'s Source,
    this: Value<'s>,
}

impl<'s> Replacer for ReplacerSymbol<'s> {
    fn replace_append(&mut self, caps: &regex::Captures, dst: &mut String) {
        let ident = caps.get(1).unwrap().as_str();
        match self.this {
            Value::Structure(s) => {
                if let Some(field) = s.fields.iter().find(|field| {
                    field.main_decl().name.as_ref().map(|s| s as &str) == Some(ident)
                }) {
                    caps.expand(&format!("[`Self::{}`]", field.ident(),).replace(' ', ""), dst);

                    return;
                }
            }
            Value::Function(func) => {
                if let Some(decl) = func
                    .parameters
                    .iter()
                    .find(|param| param.name.as_ref().map(|s| s as &str) == Some(ident))
                {
                    caps.expand(&format!("[`Self::{}`]", decl.ident(),).replace(' ', ""), dst);

                    return;
                }
            }
            Value::FunctionPointer(func) => {
                if let Some(decl) = func
                    .parameters
                    .iter()
                    .find(|param| param.name.as_ref().map(|s| s as &str) == Some(ident))
                {
                    caps.expand(&format!("[`Self::{}`]", decl.ident(),).replace(' ', ""), dst);

                    return;
                }
            }
            Value::Enum(en) => {
                if let Some(variant) = en
                    .variants
                    .iter()
                    .find(|variant| variant.name.original.to_string().eq(ident))
                {
                    caps.expand(&format!("[`Self::{}`]", variant.name.ident(),).replace(' ', ""), dst);

                    return;
                }
            }
            _ => {}
        };

        if let Some(value) = self.source.find(caps.get(1).unwrap().as_str()) {
            let ty = value_to_ident(value);

            caps.expand(&format!("[`crate::vk::{}`]", ty).replace(' ', ""), dst);
            return;
        } 

        caps.expand(&format!("`{}`", ident,), dst);
    }
}
