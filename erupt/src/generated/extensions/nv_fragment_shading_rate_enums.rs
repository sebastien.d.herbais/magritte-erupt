#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_FRAGMENT_SHADING_RATE_ENUMS_SPEC_VERSION")]
pub const NV_FRAGMENT_SHADING_RATE_ENUMS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_FRAGMENT_SHADING_RATE_ENUMS_EXTENSION_NAME")]
pub const NV_FRAGMENT_SHADING_RATE_ENUMS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_fragment_shading_rate_enums");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_FRAGMENT_SHADING_RATE_ENUM_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdSetFragmentShadingRateEnumNV");
#[doc = "Provided by [`crate::extensions::nv_fragment_shading_rate_enums`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV: Self = Self(1000326000);
    pub const PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV: Self = Self(1000326001);
    pub const PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV: Self = Self(1000326002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFragmentShadingRateNV.html)) · Enum <br/> VkFragmentShadingRateNV - Enumeration with fragment shading rates\n[](#_c_specification)C Specification\n----------\n\nIf the `fragmentShadingRateEnums` feature is enabled, fragment shading\nrates may be specified using the [`crate::vk::FragmentShadingRateNV`] enumerated\ntype defined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef enum VkFragmentShadingRateNV {\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV = 0,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV = 1,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV = 4,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV = 5,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV = 6,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV = 9,\n    VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV = 10,\n    VK_FRAGMENT_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV = 11,\n    VK_FRAGMENT_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV = 12,\n    VK_FRAGMENT_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV = 13,\n    VK_FRAGMENT_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV = 14,\n    VK_FRAGMENT_SHADING_RATE_NO_INVOCATIONS_NV = 15,\n} VkFragmentShadingRateNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::_1_INVOCATION_PER_PIXEL_NV`] specifies a\n  fragment size of 1x1 pixels.\n\n* [`Self::_1_INVOCATION_PER_1X2_PIXELS_NV`] specifies\n  a fragment size of 1x2 pixels.\n\n* [`Self::_1_INVOCATION_PER_2X1_PIXELS_NV`] specifies\n  a fragment size of 2x1 pixels.\n\n* [`Self::_1_INVOCATION_PER_2X2_PIXELS_NV`] specifies\n  a fragment size of 2x2 pixels.\n\n* [`Self::_1_INVOCATION_PER_2X4_PIXELS_NV`] specifies\n  a fragment size of 2x4 pixels.\n\n* [`Self::_1_INVOCATION_PER_4X2_PIXELS_NV`] specifies\n  a fragment size of 4x2 pixels.\n\n* [`Self::_1_INVOCATION_PER_4X4_PIXELS_NV`] specifies\n  a fragment size of 4x4 pixels.\n\n* [`Self::_2_INVOCATIONS_PER_PIXEL_NV`] specifies a\n  fragment size of 1x1 pixels, with two fragment shader invocations per\n  fragment.\n\n* [`Self::_4_INVOCATIONS_PER_PIXEL_NV`] specifies a\n  fragment size of 1x1 pixels, with four fragment shader invocations per\n  fragment.\n\n* [`Self::_8_INVOCATIONS_PER_PIXEL_NV`] specifies a\n  fragment size of 1x1 pixels, with eight fragment shader invocations per\n  fragment.\n\n* [`Self::_16_INVOCATIONS_PER_PIXEL_NV`] specifies a\n  fragment size of 1x1 pixels, with sixteen fragment shader invocations\n  per fragment.\n\n* [`Self::NO_INVOCATIONS_NV`] specifies that any\n  portions of a primitive that use that shading rate should be discarded\n  without invoking any fragment shader.\n\nTo use the shading rates[`Self::_2_INVOCATIONS_PER_PIXEL_NV`],[`Self::_4_INVOCATIONS_PER_PIXEL_NV`],[`Self::_8_INVOCATIONS_PER_PIXEL_NV`], and[`Self::_16_INVOCATIONS_PER_PIXEL_NV`] as a pipeline,\nprimitive, or attachment shading rate, the`supersampleFragmentShadingRates` feature **must** be enabled.\nTo use the shading rate [`Self::NO_INVOCATIONS_NV`] as\na pipeline, primitive, or attachment shading rate, the`noInvocationFragmentShadingRates` feature **must** be enabled.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`], [`crate::vk::DeviceLoader::cmd_set_fragment_shading_rate_enum_nv`]\n"]
#[doc(alias = "VkFragmentShadingRateNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct FragmentShadingRateNV(pub i32);
impl std::fmt::Debug for FragmentShadingRateNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::_1_INVOCATION_PER_PIXEL_NV => "_1_INVOCATION_PER_PIXEL_NV",
            &Self::_1_INVOCATION_PER_1X2_PIXELS_NV => "_1_INVOCATION_PER_1X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_2X1_PIXELS_NV => "_1_INVOCATION_PER_2X1_PIXELS_NV",
            &Self::_1_INVOCATION_PER_2X2_PIXELS_NV => "_1_INVOCATION_PER_2X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_2X4_PIXELS_NV => "_1_INVOCATION_PER_2X4_PIXELS_NV",
            &Self::_1_INVOCATION_PER_4X2_PIXELS_NV => "_1_INVOCATION_PER_4X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_4X4_PIXELS_NV => "_1_INVOCATION_PER_4X4_PIXELS_NV",
            &Self::_2_INVOCATIONS_PER_PIXEL_NV => "_2_INVOCATIONS_PER_PIXEL_NV",
            &Self::_4_INVOCATIONS_PER_PIXEL_NV => "_4_INVOCATIONS_PER_PIXEL_NV",
            &Self::_8_INVOCATIONS_PER_PIXEL_NV => "_8_INVOCATIONS_PER_PIXEL_NV",
            &Self::_16_INVOCATIONS_PER_PIXEL_NV => "_16_INVOCATIONS_PER_PIXEL_NV",
            &Self::NO_INVOCATIONS_NV => "NO_INVOCATIONS_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_fragment_shading_rate_enums`]"]
impl crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateNV {
    pub const _1_INVOCATION_PER_PIXEL_NV: Self = Self(0);
    pub const _1_INVOCATION_PER_1X2_PIXELS_NV: Self = Self(1);
    pub const _1_INVOCATION_PER_2X1_PIXELS_NV: Self = Self(4);
    pub const _1_INVOCATION_PER_2X2_PIXELS_NV: Self = Self(5);
    pub const _1_INVOCATION_PER_2X4_PIXELS_NV: Self = Self(6);
    pub const _1_INVOCATION_PER_4X2_PIXELS_NV: Self = Self(9);
    pub const _1_INVOCATION_PER_4X4_PIXELS_NV: Self = Self(10);
    pub const _2_INVOCATIONS_PER_PIXEL_NV: Self = Self(11);
    pub const _4_INVOCATIONS_PER_PIXEL_NV: Self = Self(12);
    pub const _8_INVOCATIONS_PER_PIXEL_NV: Self = Self(13);
    pub const _16_INVOCATIONS_PER_PIXEL_NV: Self = Self(14);
    pub const NO_INVOCATIONS_NV: Self = Self(15);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFragmentShadingRateTypeNV.html)) · Enum <br/> VkFragmentShadingRateTypeNV - Enumeration with fragment shading rate types\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FragmentShadingRateTypeNV`] enumerated type specifies whether a\ngraphics pipeline gets its pipeline fragment shading rates and combiners\nfrom the [`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure\nor the [`crate::vk::PipelineFragmentShadingRateStateCreateInfoKHR`] structure.\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef enum VkFragmentShadingRateTypeNV {\n    VK_FRAGMENT_SHADING_RATE_TYPE_FRAGMENT_SIZE_NV = 0,\n    VK_FRAGMENT_SHADING_RATE_TYPE_ENUMS_NV = 1,\n} VkFragmentShadingRateTypeNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::FRAGMENT_SIZE_NV`] specifies that a\n  graphics pipeline should obtain its pipeline fragment shading rate and\n  shading rate combiner state from the[`crate::vk::PipelineFragmentShadingRateStateCreateInfoKHR`] structure and that\n  any state specified by the[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure\n  should be ignored.\n\n* [`Self::ENUMS_NV`] specifies that a graphics\n  pipeline should obtain its pipeline fragment shading rate and shading\n  rate combiner state from the[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure and\n  that any state specified by the[`crate::vk::PipelineFragmentShadingRateStateCreateInfoKHR`] structure should\n  be ignored.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`]\n"]
#[doc(alias = "VkFragmentShadingRateTypeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct FragmentShadingRateTypeNV(pub i32);
impl std::fmt::Debug for FragmentShadingRateTypeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::FRAGMENT_SIZE_NV => "FRAGMENT_SIZE_NV",
            &Self::ENUMS_NV => "ENUMS_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_fragment_shading_rate_enums`]"]
impl crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateTypeNV {
    pub const FRAGMENT_SIZE_NV: Self = Self(0);
    pub const ENUMS_NV: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetFragmentShadingRateEnumNV.html)) · Function <br/> vkCmdSetFragmentShadingRateEnumNV - Set pipeline fragment shading rate dynamically using enums\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::FRAGMENT_SHADING_RATE_KHR`] enabled, the pipeline\nfragment shading rate and combiner operation **may** be set by the command:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\nvoid vkCmdSetFragmentShadingRateEnumNV(\n    VkCommandBuffer                             commandBuffer,\n    VkFragmentShadingRateNV                     shadingRate,\n    const VkFragmentShadingRateCombinerOpKHR    combinerOps[2]);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::shading_rate`] specifies a [`crate::vk::FragmentShadingRateNV`] enum\n  indicating the pipeline fragment shading rate for subsequent drawing\n  commands.\n\n* [`Self::combiner_ops`] specifies a [`crate::vk::FragmentShadingRateCombinerOpKHR`]determining how the[pipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-pipeline),[primitive](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-primitive), and[attachment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-attachment)are [combined](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-combining) for fragments\n  generated by subsequent drawing commands.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-pipelineFragmentShadingRate-04576  \n   If [`pipelineFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineFragmentShadingRate) is not enabled, [`Self::shading_rate`]**must** be [`crate::vk::FragmentShadingRateNV::_1_INVOCATION_PER_PIXEL_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-supersampleFragmentShadingRates-04577  \n   If [`supersampleFragmentShadingRates`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-supersampleFragmentShadingRates) is not enabled,[`Self::shading_rate`] **must** not be[`crate::vk::FragmentShadingRateNV::_2_INVOCATIONS_PER_PIXEL_NV`],[`crate::vk::FragmentShadingRateNV::_4_INVOCATIONS_PER_PIXEL_NV`],[`crate::vk::FragmentShadingRateNV::_8_INVOCATIONS_PER_PIXEL_NV`], or[`crate::vk::FragmentShadingRateNV::_16_INVOCATIONS_PER_PIXEL_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-noInvocationFragmentShadingRates-04578  \n   If [`noInvocationFragmentShadingRates`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-noInvocationFragmentShadingRates) is not enabled,[`Self::shading_rate`] **must** not be[`crate::vk::FragmentShadingRateNV::NO_INVOCATIONS_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-fragmentShadingRateEnums-04579  \n  [`fragmentShadingRateEnums`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-fragmentShadingRateEnums)**must** be enabled\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-pipelineFragmentShadingRate-04580  \n   One of [`pipelineFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineFragmentShadingRate),[`primitiveFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-primitiveFragmentShadingRate), or[`attachmentFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-attachmentFragmentShadingRate) **must** be enabled\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-primitiveFragmentShadingRate-04581  \n   If the [`primitiveFragmentShadingRate` feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#feature-primitiveFragmentShadingRate) is not enabled,[`Self::combiner_ops`][0] **must** be[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-attachmentFragmentShadingRate-04582  \n   If the [`attachmentFragmentShadingRate` feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#feature-attachmentFragmentShadingRate) is not enabled,[`Self::combiner_ops`][1] **must** be[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-fragmentSizeNonTrivialCombinerOps-04583  \n   If the [`fragmentSizeNonTrivialCombinerOps`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-fragmentShadingRateNonTrivialCombinerOps) limit is not supported,\n  elements of [`Self::combiner_ops`] **must** be either[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`] or[`crate::vk::FragmentShadingRateCombinerOpKHR::REPLACE_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::FragmentShadingRateNV`] value\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-combinerOps-parameter  \n   Any given element of [`Self::combiner_ops`] **must** be a valid [`crate::vk::FragmentShadingRateCombinerOpKHR`] value\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::FragmentShadingRateCombinerOpKHR`], [`crate::vk::FragmentShadingRateNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetFragmentShadingRateEnumNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, shading_rate: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateNV, combiner_ops: [crate::extensions::khr_fragment_shading_rate::FragmentShadingRateCombinerOpKHR; 2]) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceFragmentShadingRateEnumsFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineFragmentShadingRateEnumStateCreateInfoNV> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'_>> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentShadingRateEnumsFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentShadingRateEnumsPropertiesNV> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV - Structure indicating support for fragment shading rate enums\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fragmentShadingRateEnums;\n    VkBool32           supersampleFragmentShadingRates;\n    VkBool32           noInvocationFragmentShadingRates;\n} VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::fragment_shading_rate_enums`]indicates that the implementation supports specifying fragment shading\n  rates using the [`crate::vk::FragmentShadingRateNV`] enumerated type.\n\n* []()[`Self::supersample_fragment_shading_rates`] indicates that the implementation\n  supports fragment shading rate enum values indicating more than one\n  invocation per fragment.\n\n* []()[`Self::no_invocation_fragment_shading_rates`] indicates that the implementation\n  supports a fragment shading rate enum value indicating that no fragment\n  shaders should be invoked when that shading rate is used.\n\nIf the [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub fragment_shading_rate_enums: crate::vk1_0::Bool32,
    pub supersample_fragment_shading_rates: crate::vk1_0::Bool32,
    pub no_invocation_fragment_shading_rates: crate::vk1_0::Bool32,
}
impl PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV;
}
impl Default for PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), fragment_shading_rate_enums: Default::default(), supersample_fragment_shading_rates: Default::default(), no_invocation_fragment_shading_rates: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceFragmentShadingRateEnumsFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fragment_shading_rate_enums", &(self.fragment_shading_rate_enums != 0)).field("supersample_fragment_shading_rates", &(self.supersample_fragment_shading_rates != 0)).field("no_invocation_fragment_shading_rates", &(self.no_invocation_fragment_shading_rates != 0)).finish()
    }
}
impl PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
        PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV.html)) · Builder of [`PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] <br/> VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV - Structure indicating support for fragment shading rate enums\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fragmentShadingRateEnums;\n    VkBool32           supersampleFragmentShadingRates;\n    VkBool32           noInvocationFragmentShadingRates;\n} VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::fragment_shading_rate_enums`]indicates that the implementation supports specifying fragment shading\n  rates using the [`crate::vk::FragmentShadingRateNV`] enumerated type.\n\n* []()[`Self::supersample_fragment_shading_rates`] indicates that the implementation\n  supports fragment shading rate enum values indicating more than one\n  invocation per fragment.\n\n* []()[`Self::no_invocation_fragment_shading_rates`] indicates that the implementation\n  supports a fragment shading rate enum value indicating that no fragment\n  shaders should be invoked when that shading rate is used.\n\nIf the [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceFragmentShadingRateEnumsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a>(PhysicalDeviceFragmentShadingRateEnumsFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
        PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fragment_shading_rate_enums(mut self, fragment_shading_rate_enums: bool) -> Self {
        self.0.fragment_shading_rate_enums = fragment_shading_rate_enums as _;
        self
    }
    #[inline]
    pub fn supersample_fragment_shading_rates(mut self, supersample_fragment_shading_rates: bool) -> Self {
        self.0.supersample_fragment_shading_rates = supersample_fragment_shading_rates as _;
        self
    }
    #[inline]
    pub fn no_invocation_fragment_shading_rates(mut self, no_invocation_fragment_shading_rates: bool) -> Self {
        self.0.no_invocation_fragment_shading_rates = no_invocation_fragment_shading_rates as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceFragmentShadingRateEnumsFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceFragmentShadingRateEnumsFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV.html)) · Structure <br/> VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV - Structure describing fragment shading rate limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsPropertiesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV {\n    VkStructureType          sType;\n    void*                    pNext;\n    VkSampleCountFlagBits    maxFragmentShadingRateInvocationCount;\n} VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::max_fragment_shading_rate_invocation_count`] is a[VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value indicating the maximum number of\n  fragment shader invocations per fragment supported in pipeline,\n  primitive, and attachment fragment shading rates.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nThese properties are related to [fragment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-primsrast-fragment-shading-rate).\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV`]\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV-maxFragmentShadingRateInvocationCount-parameter  \n  [`Self::max_fragment_shading_rate_invocation_count`] **must** be a valid [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_fragment_shading_rate_invocation_count: crate::vk1_0::SampleCountFlagBits,
}
impl PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV;
}
impl Default for PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_fragment_shading_rate_invocation_count: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceFragmentShadingRateEnumsPropertiesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_fragment_shading_rate_invocation_count", &self.max_fragment_shading_rate_invocation_count).finish()
    }
}
impl PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
        PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV.html)) · Builder of [`PhysicalDeviceFragmentShadingRateEnumsPropertiesNV`] <br/> VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV - Structure describing fragment shading rate limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsPropertiesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV {\n    VkStructureType          sType;\n    void*                    pNext;\n    VkSampleCountFlagBits    maxFragmentShadingRateInvocationCount;\n} VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::max_fragment_shading_rate_invocation_count`] is a[VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value indicating the maximum number of\n  fragment shader invocations per fragment supported in pipeline,\n  primitive, and attachment fragment shading rates.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceFragmentShadingRateEnumsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nThese properties are related to [fragment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-primsrast-fragment-shading-rate).\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV`]\n\n* []() VUID-VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV-maxFragmentShadingRateInvocationCount-parameter  \n  [`Self::max_fragment_shading_rate_invocation_count`] **must** be a valid [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a>(PhysicalDeviceFragmentShadingRateEnumsPropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
        PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_fragment_shading_rate_invocation_count(mut self, max_fragment_shading_rate_invocation_count: crate::vk1_0::SampleCountFlagBits) -> Self {
        self.0.max_fragment_shading_rate_invocation_count = max_fragment_shading_rate_invocation_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
    fn default() -> PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
    type Target = PhysicalDeviceFragmentShadingRateEnumsPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceFragmentShadingRateEnumsPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineFragmentShadingRateEnumStateCreateInfoNV.html)) · Structure <br/> VkPipelineFragmentShadingRateEnumStateCreateInfoNV - Structure specifying parameters controlling the fragment shading rate using rate enums\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPipelineFragmentShadingRateEnumStateCreateInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkFragmentShadingRateTypeNV           shadingRateType;\n    VkFragmentShadingRateNV               shadingRate;\n    VkFragmentShadingRateCombinerOpKHR    combinerOps[2];\n} VkPipelineFragmentShadingRateEnumStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::shading_rate_type`] specifies a [`crate::vk::FragmentShadingRateTypeNV`]value indicating whether fragment shading rates are specified using\n  fragment sizes or [`crate::vk::FragmentShadingRateNV`] enums.\n\n* [`Self::shading_rate`] specifies a [`crate::vk::FragmentShadingRateNV`] value\n  indicating the pipeline fragment shading rate.\n\n* [`Self::combiner_ops`] specifies [`crate::vk::FragmentShadingRateCombinerOpKHR`]values determining how the[pipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-pipeline),[primitive](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-primitive), and[attachment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-attachment)are [combined](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-combining) for fragments\n  generated by drawing commands using the created pipeline.\n[](#_description)Description\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::GraphicsPipelineCreateInfo`] includes a[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the pipeline fragment\nshading rate.\n\nIf this structure is not present, [`Self::shading_rate_type`] is considered to be\nequal to [`crate::vk::FragmentShadingRateTypeNV::FRAGMENT_SIZE_NV`],[`Self::shading_rate`] is considered to be equal to[`crate::vk::FragmentShadingRateNV::_1_INVOCATION_PER_PIXEL_NV`], and both elements\nof [`Self::combiner_ops`] are considered to be equal to[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-shadingRateType-parameter  \n  [`Self::shading_rate_type`] **must** be a valid [`crate::vk::FragmentShadingRateTypeNV`] value\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::FragmentShadingRateNV`] value\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-combinerOps-parameter  \n   Any given element of [`Self::combiner_ops`] **must** be a valid [`crate::vk::FragmentShadingRateCombinerOpKHR`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FragmentShadingRateCombinerOpKHR`], [`crate::vk::FragmentShadingRateNV`], [`crate::vk::FragmentShadingRateTypeNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineFragmentShadingRateEnumStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineFragmentShadingRateEnumStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub shading_rate_type: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateTypeNV,
    pub shading_rate: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateNV,
    pub combiner_ops: [crate::extensions::khr_fragment_shading_rate::FragmentShadingRateCombinerOpKHR; 2],
}
impl PipelineFragmentShadingRateEnumStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV;
}
impl Default for PipelineFragmentShadingRateEnumStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), shading_rate_type: Default::default(), shading_rate: Default::default(), combiner_ops: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for PipelineFragmentShadingRateEnumStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineFragmentShadingRateEnumStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shading_rate_type", &self.shading_rate_type).field("shading_rate", &self.shading_rate).field("combiner_ops", &self.combiner_ops).finish()
    }
}
impl PipelineFragmentShadingRateEnumStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
        PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineFragmentShadingRateEnumStateCreateInfoNV.html)) · Builder of [`PipelineFragmentShadingRateEnumStateCreateInfoNV`] <br/> VkPipelineFragmentShadingRateEnumStateCreateInfoNV - Structure specifying parameters controlling the fragment shading rate using rate enums\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\ntypedef struct VkPipelineFragmentShadingRateEnumStateCreateInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkFragmentShadingRateTypeNV           shadingRateType;\n    VkFragmentShadingRateNV               shadingRate;\n    VkFragmentShadingRateCombinerOpKHR    combinerOps[2];\n} VkPipelineFragmentShadingRateEnumStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::shading_rate_type`] specifies a [`crate::vk::FragmentShadingRateTypeNV`]value indicating whether fragment shading rates are specified using\n  fragment sizes or [`crate::vk::FragmentShadingRateNV`] enums.\n\n* [`Self::shading_rate`] specifies a [`crate::vk::FragmentShadingRateNV`] value\n  indicating the pipeline fragment shading rate.\n\n* [`Self::combiner_ops`] specifies [`crate::vk::FragmentShadingRateCombinerOpKHR`]values determining how the[pipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-pipeline),[primitive](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-primitive), and[attachment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-attachment)are [combined](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-combining) for fragments\n  generated by drawing commands using the created pipeline.\n[](#_description)Description\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::GraphicsPipelineCreateInfo`] includes a[`crate::vk::PipelineFragmentShadingRateEnumStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the pipeline fragment\nshading rate.\n\nIf this structure is not present, [`Self::shading_rate_type`] is considered to be\nequal to [`crate::vk::FragmentShadingRateTypeNV::FRAGMENT_SIZE_NV`],[`Self::shading_rate`] is considered to be equal to[`crate::vk::FragmentShadingRateNV::_1_INVOCATION_PER_PIXEL_NV`], and both elements\nof [`Self::combiner_ops`] are considered to be equal to[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-shadingRateType-parameter  \n  [`Self::shading_rate_type`] **must** be a valid [`crate::vk::FragmentShadingRateTypeNV`] value\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::FragmentShadingRateNV`] value\n\n* []() VUID-VkPipelineFragmentShadingRateEnumStateCreateInfoNV-combinerOps-parameter  \n   Any given element of [`Self::combiner_ops`] **must** be a valid [`crate::vk::FragmentShadingRateCombinerOpKHR`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FragmentShadingRateCombinerOpKHR`], [`crate::vk::FragmentShadingRateNV`], [`crate::vk::FragmentShadingRateTypeNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a>(PipelineFragmentShadingRateEnumStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
        PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate_type(mut self, shading_rate_type: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateTypeNV) -> Self {
        self.0.shading_rate_type = shading_rate_type as _;
        self
    }
    #[inline]
    pub fn shading_rate(mut self, shading_rate: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateNV) -> Self {
        self.0.shading_rate = shading_rate as _;
        self
    }
    #[inline]
    pub fn combiner_ops(mut self, combiner_ops: [crate::extensions::khr_fragment_shading_rate::FragmentShadingRateCombinerOpKHR; 2]) -> Self {
        self.0.combiner_ops = combiner_ops as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineFragmentShadingRateEnumStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
    type Target = PipelineFragmentShadingRateEnumStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineFragmentShadingRateEnumStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_fragment_shading_rate_enums`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetFragmentShadingRateEnumNV.html)) · Function <br/> vkCmdSetFragmentShadingRateEnumNV - Set pipeline fragment shading rate dynamically using enums\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::FRAGMENT_SHADING_RATE_KHR`] enabled, the pipeline\nfragment shading rate and combiner operation **may** be set by the command:\n\n```\n// Provided by VK_NV_fragment_shading_rate_enums\nvoid vkCmdSetFragmentShadingRateEnumNV(\n    VkCommandBuffer                             commandBuffer,\n    VkFragmentShadingRateNV                     shadingRate,\n    const VkFragmentShadingRateCombinerOpKHR    combinerOps[2]);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::shading_rate`] specifies a [`crate::vk::FragmentShadingRateNV`] enum\n  indicating the pipeline fragment shading rate for subsequent drawing\n  commands.\n\n* [`Self::combiner_ops`] specifies a [`crate::vk::FragmentShadingRateCombinerOpKHR`]determining how the[pipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-pipeline),[primitive](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-primitive), and[attachment shading rates](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-attachment)are [combined](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-fragment-shading-rate-combining) for fragments\n  generated by subsequent drawing commands.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-pipelineFragmentShadingRate-04576  \n   If [`pipelineFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineFragmentShadingRate) is not enabled, [`Self::shading_rate`]**must** be [`crate::vk::FragmentShadingRateNV::_1_INVOCATION_PER_PIXEL_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-supersampleFragmentShadingRates-04577  \n   If [`supersampleFragmentShadingRates`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-supersampleFragmentShadingRates) is not enabled,[`Self::shading_rate`] **must** not be[`crate::vk::FragmentShadingRateNV::_2_INVOCATIONS_PER_PIXEL_NV`],[`crate::vk::FragmentShadingRateNV::_4_INVOCATIONS_PER_PIXEL_NV`],[`crate::vk::FragmentShadingRateNV::_8_INVOCATIONS_PER_PIXEL_NV`], or[`crate::vk::FragmentShadingRateNV::_16_INVOCATIONS_PER_PIXEL_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-noInvocationFragmentShadingRates-04578  \n   If [`noInvocationFragmentShadingRates`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-noInvocationFragmentShadingRates) is not enabled,[`Self::shading_rate`] **must** not be[`crate::vk::FragmentShadingRateNV::NO_INVOCATIONS_NV`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-fragmentShadingRateEnums-04579  \n  [`fragmentShadingRateEnums`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-fragmentShadingRateEnums)**must** be enabled\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-pipelineFragmentShadingRate-04580  \n   One of [`pipelineFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineFragmentShadingRate),[`primitiveFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-primitiveFragmentShadingRate), or[`attachmentFragmentShadingRate`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-attachmentFragmentShadingRate) **must** be enabled\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-primitiveFragmentShadingRate-04581  \n   If the [`primitiveFragmentShadingRate` feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#feature-primitiveFragmentShadingRate) is not enabled,[`Self::combiner_ops`][0] **must** be[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-attachmentFragmentShadingRate-04582  \n   If the [`attachmentFragmentShadingRate` feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#feature-attachmentFragmentShadingRate) is not enabled,[`Self::combiner_ops`][1] **must** be[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`]\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-fragmentSizeNonTrivialCombinerOps-04583  \n   If the [`fragmentSizeNonTrivialCombinerOps`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-fragmentShadingRateNonTrivialCombinerOps) limit is not supported,\n  elements of [`Self::combiner_ops`] **must** be either[`crate::vk::FragmentShadingRateCombinerOpKHR::KEEP_KHR`] or[`crate::vk::FragmentShadingRateCombinerOpKHR::REPLACE_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::FragmentShadingRateNV`] value\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-combinerOps-parameter  \n   Any given element of [`Self::combiner_ops`] **must** be a valid [`crate::vk::FragmentShadingRateCombinerOpKHR`] value\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetFragmentShadingRateEnumNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::FragmentShadingRateCombinerOpKHR`], [`crate::vk::FragmentShadingRateNV`]\n"]
    #[doc(alias = "vkCmdSetFragmentShadingRateEnumNV")]
    pub unsafe fn cmd_set_fragment_shading_rate_enum_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, shading_rate: crate::extensions::nv_fragment_shading_rate_enums::FragmentShadingRateNV, combiner_ops: [crate::extensions::khr_fragment_shading_rate::FragmentShadingRateCombinerOpKHR; 2]) -> () {
        let _function = self.cmd_set_fragment_shading_rate_enum_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, shading_rate as _, combiner_ops as _);
        ()
    }
}
