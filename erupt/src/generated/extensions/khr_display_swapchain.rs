#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_DISPLAY_SWAPCHAIN_SPEC_VERSION")]
pub const KHR_DISPLAY_SWAPCHAIN_SPEC_VERSION: u32 = 10;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME")]
pub const KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_display_swapchain");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_SHARED_SWAPCHAINS_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateSharedSwapchainsKHR");
#[doc = "Provided by [`crate::extensions::khr_display_swapchain`]"]
impl crate::vk1_0::Result {
    pub const ERROR_INCOMPATIBLE_DISPLAY_KHR: Self = Self(-1000003001);
}
#[doc = "Provided by [`crate::extensions::khr_display_swapchain`]"]
impl crate::vk1_0::StructureType {
    pub const DISPLAY_PRESENT_INFO_KHR: Self = Self(1000003000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateSharedSwapchainsKHR.html)) · Function <br/> vkCreateSharedSwapchainsKHR - Create multiple swapchains that share presentable images\n[](#_c_specification)C Specification\n----------\n\nWhen the `VK_KHR_display_swapchain` extension is enabled, multiple\nswapchains that share presentable images are created by calling:\n\n```\n// Provided by VK_KHR_display_swapchain\nVkResult vkCreateSharedSwapchainsKHR(\n    VkDevice                                    device,\n    uint32_t                                    swapchainCount,\n    const VkSwapchainCreateInfoKHR*             pCreateInfos,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSwapchainKHR*                             pSwapchains);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device to create the swapchains for.\n\n* [`Self::swapchain_count`] is the number of swapchains to create.\n\n* [`Self::p_create_infos`] is a pointer to an array of[`crate::vk::SwapchainCreateInfoKHR`] structures specifying the parameters of\n  the created swapchains.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  swapchain objects when there is no more specific allocator available\n  (see [Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_swapchains`] is a pointer to an array of [`crate::vk::SwapchainKHR`]handles in which the created swapchain objects will be returned.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::create_shared_swapchains_khr`] is similar to [`crate::vk::DeviceLoader::create_swapchain_khr`],\nexcept that it takes an array of [`crate::vk::SwapchainCreateInfoKHR`] structures,\nand returns an array of swapchain objects.\n\nThe swapchain creation parameters that affect the properties and number of\npresentable images **must** match between all the swapchains.\nIf the displays used by any of the swapchains do not use the same\npresentable image layout or are incompatible in a way that prevents sharing\nimages, swapchain creation will fail with the result code[`crate::vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR`].\nIf any error occurs, no swapchains will be created.\nImages presented to multiple swapchains **must** be re-acquired from all of\nthem before transitioning away from [`crate::vk::ImageLayout::PRESENT_SRC_KHR`].\nAfter destroying one or more of the swapchains, the remaining swapchains and\nthe presentable images **can** continue to be used.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateSharedSwapchainsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pCreateInfos-parameter  \n  [`Self::p_create_infos`] **must** be a valid pointer to an array of [`Self::swapchain_count`] valid [`crate::vk::SwapchainCreateInfoKHR`] structures\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pSwapchains-parameter  \n  [`Self::p_swapchains`] **must** be a valid pointer to an array of [`Self::swapchain_count`] [`crate::vk::SwapchainKHR`] handles\n\n* []() VUID-vkCreateSharedSwapchainsKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::p_create_infos`][].surface **must** be externally synchronized\n\n* Host access to [`Self::p_create_infos`][].oldSwapchain **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::SwapchainCreateInfoKHR`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateSharedSwapchainsKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain_count: u32, p_create_infos: *const crate::extensions::khr_swapchain::SwapchainCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_swapchains: *mut crate::extensions::khr_swapchain::SwapchainKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPresentInfoKHR.html)) · Structure <br/> VkDisplayPresentInfoKHR - Structure describing parameters of a queue presentation to a swapchain\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPresentInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display_swapchain\ntypedef struct VkDisplayPresentInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkRect2D           srcRect;\n    VkRect2D           dstRect;\n    VkBool32           persistent;\n} VkDisplayPresentInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::src_rect`] is a rectangular region of pixels to present.\n  It **must** be a subset of the image being presented.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, this region will be\n  assumed to be the entire presentable image.\n\n* [`Self::dst_rect`] is a rectangular region within the visible region of the\n  swapchain’s display mode.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, this region will be\n  assumed to be the entire visible region of the swapchain’s mode.\n  If the specified rectangle is a subset of the display mode’s visible\n  region, content from display planes below the swapchain’s plane will be\n  visible outside the rectangle.\n  If there are no planes below the swapchain’s, the area outside the\n  specified rectangle will be black.\n  If portions of the specified rectangle are outside of the display’s\n  visible region, pixels mapping only to those portions of the rectangle\n  will be discarded.\n\n* [`Self::persistent`]: If this is [`crate::vk::TRUE`], the display engine will\n  enable buffered mode on displays that support it.\n  This allows the display engine to stop sending content to the display\n  until a new image is presented.\n  The display will instead maintain a copy of the last presented image.\n  This allows less power to be used, but **may** increase presentation\n  latency.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, persistent mode will\n  not be used.\n[](#_description)Description\n----------\n\nIf the extent of the [`Self::src_rect`] and [`Self::dst_rect`] are not equal, the\npresented pixels will be scaled accordingly.\n\nValid Usage\n\n* []() VUID-VkDisplayPresentInfoKHR-srcRect-01257  \n  [`Self::src_rect`] **must** specify a rectangular region that is a subset of the\n  image being presented\n\n* []() VUID-VkDisplayPresentInfoKHR-dstRect-01258  \n  [`Self::dst_rect`] **must** specify a rectangular region that is a subset of the`visibleRegion` parameter of the display mode the swapchain being\n  presented uses\n\n* []() VUID-VkDisplayPresentInfoKHR-persistentContent-01259  \n   If the `persistentContent` member of the[`crate::vk::DisplayPropertiesKHR`] structure returned by[`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`] for the display the\n  present operation targets is [`crate::vk::FALSE`], then [`Self::persistent`] **must**be [`crate::vk::FALSE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayPresentInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_PRESENT_INFO_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDisplayPresentInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayPresentInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub src_rect: crate::vk1_0::Rect2D,
    pub dst_rect: crate::vk1_0::Rect2D,
    pub persistent: crate::vk1_0::Bool32,
}
impl DisplayPresentInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DISPLAY_PRESENT_INFO_KHR;
}
impl Default for DisplayPresentInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), src_rect: Default::default(), dst_rect: Default::default(), persistent: Default::default() }
    }
}
impl std::fmt::Debug for DisplayPresentInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayPresentInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("src_rect", &self.src_rect).field("dst_rect", &self.dst_rect).field("persistent", &(self.persistent != 0)).finish()
    }
}
impl DisplayPresentInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayPresentInfoKHRBuilder<'a> {
        DisplayPresentInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPresentInfoKHR.html)) · Builder of [`DisplayPresentInfoKHR`] <br/> VkDisplayPresentInfoKHR - Structure describing parameters of a queue presentation to a swapchain\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPresentInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display_swapchain\ntypedef struct VkDisplayPresentInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkRect2D           srcRect;\n    VkRect2D           dstRect;\n    VkBool32           persistent;\n} VkDisplayPresentInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::src_rect`] is a rectangular region of pixels to present.\n  It **must** be a subset of the image being presented.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, this region will be\n  assumed to be the entire presentable image.\n\n* [`Self::dst_rect`] is a rectangular region within the visible region of the\n  swapchain’s display mode.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, this region will be\n  assumed to be the entire visible region of the swapchain’s mode.\n  If the specified rectangle is a subset of the display mode’s visible\n  region, content from display planes below the swapchain’s plane will be\n  visible outside the rectangle.\n  If there are no planes below the swapchain’s, the area outside the\n  specified rectangle will be black.\n  If portions of the specified rectangle are outside of the display’s\n  visible region, pixels mapping only to those portions of the rectangle\n  will be discarded.\n\n* [`Self::persistent`]: If this is [`crate::vk::TRUE`], the display engine will\n  enable buffered mode on displays that support it.\n  This allows the display engine to stop sending content to the display\n  until a new image is presented.\n  The display will instead maintain a copy of the last presented image.\n  This allows less power to be used, but **may** increase presentation\n  latency.\n  If [`crate::vk::DisplayPresentInfoKHR`] is not specified, persistent mode will\n  not be used.\n[](#_description)Description\n----------\n\nIf the extent of the [`Self::src_rect`] and [`Self::dst_rect`] are not equal, the\npresented pixels will be scaled accordingly.\n\nValid Usage\n\n* []() VUID-VkDisplayPresentInfoKHR-srcRect-01257  \n  [`Self::src_rect`] **must** specify a rectangular region that is a subset of the\n  image being presented\n\n* []() VUID-VkDisplayPresentInfoKHR-dstRect-01258  \n  [`Self::dst_rect`] **must** specify a rectangular region that is a subset of the`visibleRegion` parameter of the display mode the swapchain being\n  presented uses\n\n* []() VUID-VkDisplayPresentInfoKHR-persistentContent-01259  \n   If the `persistentContent` member of the[`crate::vk::DisplayPropertiesKHR`] structure returned by[`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`] for the display the\n  present operation targets is [`crate::vk::FALSE`], then [`Self::persistent`] **must**be [`crate::vk::FALSE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayPresentInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_PRESENT_INFO_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DisplayPresentInfoKHRBuilder<'a>(DisplayPresentInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayPresentInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayPresentInfoKHRBuilder<'a> {
        DisplayPresentInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn src_rect(mut self, src_rect: crate::vk1_0::Rect2D) -> Self {
        self.0.src_rect = src_rect as _;
        self
    }
    #[inline]
    pub fn dst_rect(mut self, dst_rect: crate::vk1_0::Rect2D) -> Self {
        self.0.dst_rect = dst_rect as _;
        self
    }
    #[inline]
    pub fn persistent(mut self, persistent: bool) -> Self {
        self.0.persistent = persistent as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayPresentInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayPresentInfoKHRBuilder<'a> {
    fn default() -> DisplayPresentInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayPresentInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayPresentInfoKHRBuilder<'a> {
    type Target = DisplayPresentInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayPresentInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromConst<'a, DisplayPresentInfoKHR> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DisplayPresentInfoKHRBuilder<'_>> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
#[doc = "Provided by [`crate::extensions::khr_display_swapchain`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateSharedSwapchainsKHR.html)) · Function <br/> vkCreateSharedSwapchainsKHR - Create multiple swapchains that share presentable images\n[](#_c_specification)C Specification\n----------\n\nWhen the `VK_KHR_display_swapchain` extension is enabled, multiple\nswapchains that share presentable images are created by calling:\n\n```\n// Provided by VK_KHR_display_swapchain\nVkResult vkCreateSharedSwapchainsKHR(\n    VkDevice                                    device,\n    uint32_t                                    swapchainCount,\n    const VkSwapchainCreateInfoKHR*             pCreateInfos,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSwapchainKHR*                             pSwapchains);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device to create the swapchains for.\n\n* [`Self::swapchain_count`] is the number of swapchains to create.\n\n* [`Self::p_create_infos`] is a pointer to an array of[`crate::vk::SwapchainCreateInfoKHR`] structures specifying the parameters of\n  the created swapchains.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  swapchain objects when there is no more specific allocator available\n  (see [Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_swapchains`] is a pointer to an array of [`crate::vk::SwapchainKHR`]handles in which the created swapchain objects will be returned.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::create_shared_swapchains_khr`] is similar to [`crate::vk::DeviceLoader::create_swapchain_khr`],\nexcept that it takes an array of [`crate::vk::SwapchainCreateInfoKHR`] structures,\nand returns an array of swapchain objects.\n\nThe swapchain creation parameters that affect the properties and number of\npresentable images **must** match between all the swapchains.\nIf the displays used by any of the swapchains do not use the same\npresentable image layout or are incompatible in a way that prevents sharing\nimages, swapchain creation will fail with the result code[`crate::vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR`].\nIf any error occurs, no swapchains will be created.\nImages presented to multiple swapchains **must** be re-acquired from all of\nthem before transitioning away from [`crate::vk::ImageLayout::PRESENT_SRC_KHR`].\nAfter destroying one or more of the swapchains, the remaining swapchains and\nthe presentable images **can** continue to be used.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateSharedSwapchainsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pCreateInfos-parameter  \n  [`Self::p_create_infos`] **must** be a valid pointer to an array of [`Self::swapchain_count`] valid [`crate::vk::SwapchainCreateInfoKHR`] structures\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateSharedSwapchainsKHR-pSwapchains-parameter  \n  [`Self::p_swapchains`] **must** be a valid pointer to an array of [`Self::swapchain_count`] [`crate::vk::SwapchainKHR`] handles\n\n* []() VUID-vkCreateSharedSwapchainsKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::p_create_infos`][].surface **must** be externally synchronized\n\n* Host access to [`Self::p_create_infos`][].oldSwapchain **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::SwapchainCreateInfoKHR`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkCreateSharedSwapchainsKHR")]
    pub unsafe fn create_shared_swapchains_khr(&self, create_infos: &[crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder], allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_swapchain::SwapchainKHR>> {
        let _function = self.create_shared_swapchains_khr.expect(crate::NOT_LOADED_MESSAGE);
        let swapchain_count = create_infos.len();
        let mut swapchains = crate::SmallVec::from_elem(Default::default(), swapchain_count as _);
        let _return = _function(
            self.handle,
            swapchain_count as _,
            create_infos.as_ptr() as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            swapchains.as_mut_ptr(),
        );
        crate::utils::VulkanResult::new(_return, swapchains)
    }
}
