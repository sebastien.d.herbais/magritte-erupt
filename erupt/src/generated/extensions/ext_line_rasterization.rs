#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_LINE_RASTERIZATION_SPEC_VERSION")]
pub const EXT_LINE_RASTERIZATION_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_LINE_RASTERIZATION_EXTENSION_NAME")]
pub const EXT_LINE_RASTERIZATION_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_line_rasterization");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_LINE_STIPPLE_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdSetLineStippleEXT");
#[doc = "Provided by [`crate::extensions::ext_line_rasterization`]"]
impl crate::vk1_0::DynamicState {
    pub const LINE_STIPPLE_EXT: Self = Self(1000259000);
}
#[doc = "Provided by [`crate::extensions::ext_line_rasterization`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT: Self = Self(1000259000);
    pub const PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT: Self = Self(1000259001);
    pub const PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT: Self = Self(1000259002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkLineRasterizationModeEXT.html)) · Enum <br/> VkLineRasterizationModeEXT - Line rasterization modes\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT::line_rasterization_mode`]are:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef enum VkLineRasterizationModeEXT {\n    VK_LINE_RASTERIZATION_MODE_DEFAULT_EXT = 0,\n    VK_LINE_RASTERIZATION_MODE_RECTANGULAR_EXT = 1,\n    VK_LINE_RASTERIZATION_MODE_BRESENHAM_EXT = 2,\n    VK_LINE_RASTERIZATION_MODE_RECTANGULAR_SMOOTH_EXT = 3,\n} VkLineRasterizationModeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::DEFAULT_EXT`] is equivalent to[`Self::RECTANGULAR_EXT`] if[`crate::vk::PhysicalDeviceLimits::strict_lines`] is [`crate::vk::TRUE`],\n  otherwise lines are drawn as non-`strictLines` parallelograms.\n  Both of these modes are defined in [Basic Line\n  Segment Rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-basic).\n\n* [`Self::RECTANGULAR_EXT`] specifies lines drawn\n  as if they were rectangles extruded from the line\n\n* [`Self::BRESENHAM_EXT`] specifies lines drawn by\n  determining which pixel diamonds the line intersects and exits, as\n  defined in [Bresenham Line Segment\n  Rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-bresenham).\n\n* [`Self::RECTANGULAR_SMOOTH_EXT`] specifies lines\n  drawn if they were rectangles extruded from the line, with alpha\n  falloff, as defined in [Smooth Lines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-smooth).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT`]\n"]
#[doc(alias = "VkLineRasterizationModeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct LineRasterizationModeEXT(pub i32);
impl std::fmt::Debug for LineRasterizationModeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_EXT => "DEFAULT_EXT",
            &Self::RECTANGULAR_EXT => "RECTANGULAR_EXT",
            &Self::BRESENHAM_EXT => "BRESENHAM_EXT",
            &Self::RECTANGULAR_SMOOTH_EXT => "RECTANGULAR_SMOOTH_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_line_rasterization`]"]
impl crate::extensions::ext_line_rasterization::LineRasterizationModeEXT {
    pub const DEFAULT_EXT: Self = Self(0);
    pub const RECTANGULAR_EXT: Self = Self(1);
    pub const BRESENHAM_EXT: Self = Self(2);
    pub const RECTANGULAR_SMOOTH_EXT: Self = Self(3);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetLineStippleEXT.html)) · Function <br/> vkCmdSetLineStippleEXT - Set the dynamic line width state\n[](#_c_specification)C Specification\n----------\n\nThe line stipple factor and pattern are specified by the[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT::line_stipple_factor`]and[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT::line_stipple_pattern`]members of the currently active pipeline, if the pipeline was not created\nwith [`crate::vk::DynamicState::LINE_STIPPLE_EXT`] enabled.\n\nOtherwise, the line stipple factor and pattern are set by calling[`crate::vk::DeviceLoader::cmd_set_line_stipple_ext`]:\n\n```\n// Provided by VK_EXT_line_rasterization\nvoid vkCmdSetLineStippleEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    lineStippleFactor,\n    uint16_t                                    lineStipplePattern);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::line_stipple_factor`] is the repeat factor used in stippled line\n  rasterization.\n\n* [`Self::line_stipple_pattern`] is the bit pattern used in stippled line\n  rasterization.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetLineStippleEXT-lineStippleFactor-02776  \n  [`Self::line_stipple_factor`] **must** be in the range [1,256]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetLineStippleEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, line_stipple_factor: u32, line_stipple_pattern: u16) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceLineRasterizationFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationLineStateCreateInfoEXT> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationLineStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceLineRasterizationFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceLineRasterizationPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceLineRasterizationFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceLineRasterizationFeaturesEXT - Structure describing the line rasterization features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPhysicalDeviceLineRasterizationFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rectangularLines;\n    VkBool32           bresenhamLines;\n    VkBool32           smoothLines;\n    VkBool32           stippledRectangularLines;\n    VkBool32           stippledBresenhamLines;\n    VkBool32           stippledSmoothLines;\n} VkPhysicalDeviceLineRasterizationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::rectangular_lines`] indicates whether\n  the implementation supports [rectangular line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines).\n\n* []() [`Self::bresenham_lines`] indicates whether the\n  implementation supports [Bresenham-style line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-bresenham).\n\n* []() [`Self::smooth_lines`] indicates whether the\n  implementation supports [smooth line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-smooth).\n\n* []() [`Self::stippled_rectangular_lines`]indicates whether the implementation supports[stippled line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`] lines, or with[`crate::vk::LineRasterizationModeEXT::DEFAULT_EXT`] lines if[`crate::vk::PhysicalDeviceLimits::strict_lines`] is [`crate::vk::TRUE`].\n\n* []() [`Self::stippled_bresenham_lines`]indicates whether the implementation supports[stippled line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`] lines.\n\n* []() [`Self::stippled_smooth_lines`] indicates\n  whether the implementation supports [stippled\n  line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`] lines.\n\nIf the [`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceLineRasterizationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceLineRasterizationFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceLineRasterizationFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub rectangular_lines: crate::vk1_0::Bool32,
    pub bresenham_lines: crate::vk1_0::Bool32,
    pub smooth_lines: crate::vk1_0::Bool32,
    pub stippled_rectangular_lines: crate::vk1_0::Bool32,
    pub stippled_bresenham_lines: crate::vk1_0::Bool32,
    pub stippled_smooth_lines: crate::vk1_0::Bool32,
}
impl PhysicalDeviceLineRasterizationFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT;
}
impl Default for PhysicalDeviceLineRasterizationFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), rectangular_lines: Default::default(), bresenham_lines: Default::default(), smooth_lines: Default::default(), stippled_rectangular_lines: Default::default(), stippled_bresenham_lines: Default::default(), stippled_smooth_lines: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceLineRasterizationFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceLineRasterizationFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("rectangular_lines", &(self.rectangular_lines != 0)).field("bresenham_lines", &(self.bresenham_lines != 0)).field("smooth_lines", &(self.smooth_lines != 0)).field("stippled_rectangular_lines", &(self.stippled_rectangular_lines != 0)).field("stippled_bresenham_lines", &(self.stippled_bresenham_lines != 0)).field("stippled_smooth_lines", &(self.stippled_smooth_lines != 0)).finish()
    }
}
impl PhysicalDeviceLineRasterizationFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
        PhysicalDeviceLineRasterizationFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceLineRasterizationFeaturesEXT.html)) · Builder of [`PhysicalDeviceLineRasterizationFeaturesEXT`] <br/> VkPhysicalDeviceLineRasterizationFeaturesEXT - Structure describing the line rasterization features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPhysicalDeviceLineRasterizationFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rectangularLines;\n    VkBool32           bresenhamLines;\n    VkBool32           smoothLines;\n    VkBool32           stippledRectangularLines;\n    VkBool32           stippledBresenhamLines;\n    VkBool32           stippledSmoothLines;\n} VkPhysicalDeviceLineRasterizationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::rectangular_lines`] indicates whether\n  the implementation supports [rectangular line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines).\n\n* []() [`Self::bresenham_lines`] indicates whether the\n  implementation supports [Bresenham-style line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-bresenham).\n\n* []() [`Self::smooth_lines`] indicates whether the\n  implementation supports [smooth line\n  rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-smooth).\n\n* []() [`Self::stippled_rectangular_lines`]indicates whether the implementation supports[stippled line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`] lines, or with[`crate::vk::LineRasterizationModeEXT::DEFAULT_EXT`] lines if[`crate::vk::PhysicalDeviceLimits::strict_lines`] is [`crate::vk::TRUE`].\n\n* []() [`Self::stippled_bresenham_lines`]indicates whether the implementation supports[stippled line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`] lines.\n\n* []() [`Self::stippled_smooth_lines`] indicates\n  whether the implementation supports [stippled\n  line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple) with[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`] lines.\n\nIf the [`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceLineRasterizationFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceLineRasterizationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a>(PhysicalDeviceLineRasterizationFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
        PhysicalDeviceLineRasterizationFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn rectangular_lines(mut self, rectangular_lines: bool) -> Self {
        self.0.rectangular_lines = rectangular_lines as _;
        self
    }
    #[inline]
    pub fn bresenham_lines(mut self, bresenham_lines: bool) -> Self {
        self.0.bresenham_lines = bresenham_lines as _;
        self
    }
    #[inline]
    pub fn smooth_lines(mut self, smooth_lines: bool) -> Self {
        self.0.smooth_lines = smooth_lines as _;
        self
    }
    #[inline]
    pub fn stippled_rectangular_lines(mut self, stippled_rectangular_lines: bool) -> Self {
        self.0.stippled_rectangular_lines = stippled_rectangular_lines as _;
        self
    }
    #[inline]
    pub fn stippled_bresenham_lines(mut self, stippled_bresenham_lines: bool) -> Self {
        self.0.stippled_bresenham_lines = stippled_bresenham_lines as _;
        self
    }
    #[inline]
    pub fn stippled_smooth_lines(mut self, stippled_smooth_lines: bool) -> Self {
        self.0.stippled_smooth_lines = stippled_smooth_lines as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceLineRasterizationFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceLineRasterizationFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceLineRasterizationFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceLineRasterizationPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceLineRasterizationPropertiesEXT - Structure describing line rasterization properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceLineRasterizationPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPhysicalDeviceLineRasterizationPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           lineSubPixelPrecisionBits;\n} VkPhysicalDeviceLineRasterizationPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::line_sub_pixel_precision_bits`] is\n  the number of bits of subpixel precision in framebuffer coordinatesx<sub>f</sub> and y<sub>f</sub> when rasterizing [line\n  segments](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines).\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceLineRasterizationPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceLineRasterizationPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceLineRasterizationPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceLineRasterizationPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub line_sub_pixel_precision_bits: u32,
}
impl PhysicalDeviceLineRasterizationPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceLineRasterizationPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), line_sub_pixel_precision_bits: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceLineRasterizationPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceLineRasterizationPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("line_sub_pixel_precision_bits", &self.line_sub_pixel_precision_bits).finish()
    }
}
impl PhysicalDeviceLineRasterizationPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
        PhysicalDeviceLineRasterizationPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceLineRasterizationPropertiesEXT.html)) · Builder of [`PhysicalDeviceLineRasterizationPropertiesEXT`] <br/> VkPhysicalDeviceLineRasterizationPropertiesEXT - Structure describing line rasterization properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceLineRasterizationPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPhysicalDeviceLineRasterizationPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           lineSubPixelPrecisionBits;\n} VkPhysicalDeviceLineRasterizationPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::line_sub_pixel_precision_bits`] is\n  the number of bits of subpixel precision in framebuffer coordinatesx<sub>f</sub> and y<sub>f</sub> when rasterizing [line\n  segments](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines).\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceLineRasterizationPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceLineRasterizationPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a>(PhysicalDeviceLineRasterizationPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
        PhysicalDeviceLineRasterizationPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn line_sub_pixel_precision_bits(mut self, line_sub_pixel_precision_bits: u32) -> Self {
        self.0.line_sub_pixel_precision_bits = line_sub_pixel_precision_bits as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceLineRasterizationPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceLineRasterizationPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceLineRasterizationPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationLineStateCreateInfoEXT.html)) · Structure <br/> VkPipelineRasterizationLineStateCreateInfoEXT - Structure specifying parameters of a newly created pipeline line rasterization state\n[](#_c_specification)C Specification\n----------\n\nLine segment rasterization options are controlled by the[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT`] structure.\n\nThe [`crate::vk::PipelineRasterizationLineStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPipelineRasterizationLineStateCreateInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkLineRasterizationModeEXT    lineRasterizationMode;\n    VkBool32                      stippledLineEnable;\n    uint32_t                      lineStippleFactor;\n    uint16_t                      lineStipplePattern;\n} VkPipelineRasterizationLineStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::line_rasterization_mode`] is a [`crate::vk::LineRasterizationModeEXT`] value\n  selecting the style of line rasterization.\n\n* [`Self::stippled_line_enable`] enables [stippled\n  line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple).\n\n* [`Self::line_stipple_factor`] is the repeat factor used in stippled line\n  rasterization.\n\n* [`Self::line_stipple_pattern`] is the bit pattern used in stippled line\n  rasterization.\n[](#_description)Description\n----------\n\nIf [`Self::stippled_line_enable`] is [`crate::vk::FALSE`], the values of[`Self::line_stipple_factor`] and [`Self::line_stipple_pattern`] are ignored.\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02768  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`], then the[rectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-rectangularLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02769  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`], then the[bresenhamLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bresenhamLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02770  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`], then the[smoothLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bresenhamLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02771  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`], then the[stippledRectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledRectangularLines) feature**must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02772  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`], then the[stippledBresenhamLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledBresenhamLines) feature **must**be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02773  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`], then the[stippledSmoothLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledSmoothLines) feature **must** be\n  enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02774  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::DEFAULT_EXT`], then the[stippledRectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledRectangularLines) feature**must** be enabled and [`crate::vk::PhysicalDeviceLimits::strict_lines`]**must** be [`crate::vk::TRUE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-parameter  \n  [`Self::line_rasterization_mode`] **must** be a valid [`crate::vk::LineRasterizationModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::LineRasterizationModeEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineRasterizationLineStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineRasterizationLineStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub line_rasterization_mode: crate::extensions::ext_line_rasterization::LineRasterizationModeEXT,
    pub stippled_line_enable: crate::vk1_0::Bool32,
    pub line_stipple_factor: u32,
    pub line_stipple_pattern: u16,
}
impl PipelineRasterizationLineStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineRasterizationLineStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), line_rasterization_mode: Default::default(), stippled_line_enable: Default::default(), line_stipple_factor: Default::default(), line_stipple_pattern: Default::default() }
    }
}
impl std::fmt::Debug for PipelineRasterizationLineStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineRasterizationLineStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("line_rasterization_mode", &self.line_rasterization_mode).field("stippled_line_enable", &(self.stippled_line_enable != 0)).field("line_stipple_factor", &self.line_stipple_factor).field("line_stipple_pattern", &self.line_stipple_pattern).finish()
    }
}
impl PipelineRasterizationLineStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationLineStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationLineStateCreateInfoEXT.html)) · Builder of [`PipelineRasterizationLineStateCreateInfoEXT`] <br/> VkPipelineRasterizationLineStateCreateInfoEXT - Structure specifying parameters of a newly created pipeline line rasterization state\n[](#_c_specification)C Specification\n----------\n\nLine segment rasterization options are controlled by the[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT`] structure.\n\nThe [`crate::vk::PipelineRasterizationLineStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_line_rasterization\ntypedef struct VkPipelineRasterizationLineStateCreateInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkLineRasterizationModeEXT    lineRasterizationMode;\n    VkBool32                      stippledLineEnable;\n    uint32_t                      lineStippleFactor;\n    uint16_t                      lineStipplePattern;\n} VkPipelineRasterizationLineStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::line_rasterization_mode`] is a [`crate::vk::LineRasterizationModeEXT`] value\n  selecting the style of line rasterization.\n\n* [`Self::stippled_line_enable`] enables [stippled\n  line rasterization](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-lines-stipple).\n\n* [`Self::line_stipple_factor`] is the repeat factor used in stippled line\n  rasterization.\n\n* [`Self::line_stipple_pattern`] is the bit pattern used in stippled line\n  rasterization.\n[](#_description)Description\n----------\n\nIf [`Self::stippled_line_enable`] is [`crate::vk::FALSE`], the values of[`Self::line_stipple_factor`] and [`Self::line_stipple_pattern`] are ignored.\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02768  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`], then the[rectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-rectangularLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02769  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`], then the[bresenhamLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bresenhamLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-02770  \n   If [`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`], then the[smoothLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bresenhamLines) feature **must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02771  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_EXT`], then the[stippledRectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledRectangularLines) feature**must** be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02772  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::BRESENHAM_EXT`], then the[stippledBresenhamLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledBresenhamLines) feature **must**be enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02773  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::RECTANGULAR_SMOOTH_EXT`], then the[stippledSmoothLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledSmoothLines) feature **must** be\n  enabled\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-stippledLineEnable-02774  \n   If [`Self::stippled_line_enable`] is [`crate::vk::TRUE`] and[`Self::line_rasterization_mode`] is[`crate::vk::LineRasterizationModeEXT::DEFAULT_EXT`], then the[stippledRectangularLines](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-stippledRectangularLines) feature**must** be enabled and [`crate::vk::PhysicalDeviceLimits::strict_lines`]**must** be [`crate::vk::TRUE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationLineStateCreateInfoEXT-lineRasterizationMode-parameter  \n  [`Self::line_rasterization_mode`] **must** be a valid [`crate::vk::LineRasterizationModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::LineRasterizationModeEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineRasterizationLineStateCreateInfoEXTBuilder<'a>(PipelineRasterizationLineStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationLineStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn line_rasterization_mode(mut self, line_rasterization_mode: crate::extensions::ext_line_rasterization::LineRasterizationModeEXT) -> Self {
        self.0.line_rasterization_mode = line_rasterization_mode as _;
        self
    }
    #[inline]
    pub fn stippled_line_enable(mut self, stippled_line_enable: bool) -> Self {
        self.0.stippled_line_enable = stippled_line_enable as _;
        self
    }
    #[inline]
    pub fn line_stipple_factor(mut self, line_stipple_factor: u32) -> Self {
        self.0.line_stipple_factor = line_stipple_factor as _;
        self
    }
    #[inline]
    pub fn line_stipple_pattern(mut self, line_stipple_pattern: u16) -> Self {
        self.0.line_stipple_pattern = line_stipple_pattern as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineRasterizationLineStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineRasterizationLineStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineRasterizationLineStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_line_rasterization`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetLineStippleEXT.html)) · Function <br/> vkCmdSetLineStippleEXT - Set the dynamic line width state\n[](#_c_specification)C Specification\n----------\n\nThe line stipple factor and pattern are specified by the[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT::line_stipple_factor`]and[`crate::vk::PipelineRasterizationLineStateCreateInfoEXT::line_stipple_pattern`]members of the currently active pipeline, if the pipeline was not created\nwith [`crate::vk::DynamicState::LINE_STIPPLE_EXT`] enabled.\n\nOtherwise, the line stipple factor and pattern are set by calling[`crate::vk::DeviceLoader::cmd_set_line_stipple_ext`]:\n\n```\n// Provided by VK_EXT_line_rasterization\nvoid vkCmdSetLineStippleEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    lineStippleFactor,\n    uint16_t                                    lineStipplePattern);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::line_stipple_factor`] is the repeat factor used in stippled line\n  rasterization.\n\n* [`Self::line_stipple_pattern`] is the bit pattern used in stippled line\n  rasterization.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetLineStippleEXT-lineStippleFactor-02776  \n  [`Self::line_stipple_factor`] **must** be in the range [1,256]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetLineStippleEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdSetLineStippleEXT")]
    pub unsafe fn cmd_set_line_stipple_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, line_stipple_factor: u32, line_stipple_pattern: u16) -> () {
        let _function = self.cmd_set_line_stipple_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, line_stipple_factor as _, line_stipple_pattern as _);
        ()
    }
}
