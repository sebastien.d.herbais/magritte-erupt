#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_MVK_MACOS_SURFACE_SPEC_VERSION")]
pub const MVK_MACOS_SURFACE_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_MVK_MACOS_SURFACE_EXTENSION_NAME")]
pub const MVK_MACOS_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_MVK_macos_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_MAC_OS_SURFACE_MVK: *const std::os::raw::c_char = crate::cstr!("vkCreateMacOSSurfaceMVK");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMacOSSurfaceCreateFlagsMVK.html)) · Bitmask of [`MacOSSurfaceCreateFlagBitsMVK`] <br/> VkMacOSSurfaceCreateFlagsMVK - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_MVK_macos_surface\ntypedef VkFlags VkMacOSSurfaceCreateFlagsMVK;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::MacOSSurfaceCreateFlagBitsMVK`] is a bitmask type for setting a mask, but\nis currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MacOSSurfaceCreateInfoMVK`]\n"] # [doc (alias = "VkMacOSSurfaceCreateFlagsMVK")] # [derive (Default)] # [repr (transparent)] pub struct MacOSSurfaceCreateFlagsMVK : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`MacOSSurfaceCreateFlagsMVK`] <br/> "]
#[doc(alias = "VkMacOSSurfaceCreateFlagBitsMVK")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct MacOSSurfaceCreateFlagBitsMVK(pub u32);
impl MacOSSurfaceCreateFlagBitsMVK {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> MacOSSurfaceCreateFlagsMVK {
        MacOSSurfaceCreateFlagsMVK::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for MacOSSurfaceCreateFlagBitsMVK {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::mvk_macos_surface`]"]
impl crate::vk1_0::StructureType {
    pub const MACOS_SURFACE_CREATE_INFO_MVK: Self = Self(1000123000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateMacOSSurfaceMVK.html)) · Function <br/> vkCreateMacOSSurfaceMVK - Create a VkSurfaceKHR object for a macOS NSView\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a macOS `NSView` or[`crate::vk::CAMetalLayer`], call:\n\n```\n// Provided by VK_MVK_macos_surface\nVkResult vkCreateMacOSSurfaceMVK(\n    VkInstance                                  instance,\n    const VkMacOSSurfaceCreateInfoMVK*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n|   |Note<br/><br/>The [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`] function is considered deprecated and has been<br/>superseded by [`crate::vk::InstanceLoader::create_metal_surface_ext`] from the[VK_EXT_metal_surface](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_metal_surface.html) extension.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_description)Description\n----------\n\n* [`Self::instance`] is the instance with which to associate the surface.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::MacOSSurfaceCreateInfoMVK`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateMacOSSurfaceMVK-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::MacOSSurfaceCreateInfoMVK`] structure\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::MacOSSurfaceCreateInfoMVK`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateMacOSSurfaceMVK = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::mvk_macos_surface::MacOSSurfaceCreateInfoMVK, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMacOSSurfaceCreateInfoMVK.html)) · Structure <br/> VkMacOSSurfaceCreateInfoMVK - Structure specifying parameters of a newly created macOS surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MacOSSurfaceCreateInfoMVK`] structure is defined as:\n\n```\n// Provided by VK_MVK_macos_surface\ntypedef struct VkMacOSSurfaceCreateInfoMVK {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkMacOSSurfaceCreateFlagsMVK    flags;\n    const void*                     pView;\n} VkMacOSSurfaceCreateInfoMVK;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::p_view`] is a reference to either a [`crate::vk::CAMetalLayer`] object or\n  an `NSView` object.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pView-04144  \n   If [`Self::p_view`] is a [`crate::vk::CAMetalLayer`] object, it **must** be a valid[`crate::vk::CAMetalLayer`].\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pView-01317  \n   If [`Self::p_view`] is an `NSView` object, it **must** be a valid`NSView`, **must** be backed by a `CALayer` object of type[`crate::vk::CAMetalLayer`], and [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`] **must** be called\n  on the main thread.\n\nValid Usage (Implicit)\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MACOS_SURFACE_CREATE_INFO_MVK`]\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MacOSSurfaceCreateFlagBitsMVK`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`]\n"]
#[doc(alias = "VkMacOSSurfaceCreateInfoMVK")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MacOSSurfaceCreateInfoMVK {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::mvk_macos_surface::MacOSSurfaceCreateFlagsMVK,
    pub p_view: *const std::ffi::c_void,
}
impl MacOSSurfaceCreateInfoMVK {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MACOS_SURFACE_CREATE_INFO_MVK;
}
impl Default for MacOSSurfaceCreateInfoMVK {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), p_view: std::ptr::null() }
    }
}
impl std::fmt::Debug for MacOSSurfaceCreateInfoMVK {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MacOSSurfaceCreateInfoMVK").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("p_view", &self.p_view).finish()
    }
}
impl MacOSSurfaceCreateInfoMVK {
    #[inline]
    pub fn into_builder<'a>(self) -> MacOSSurfaceCreateInfoMVKBuilder<'a> {
        MacOSSurfaceCreateInfoMVKBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMacOSSurfaceCreateInfoMVK.html)) · Builder of [`MacOSSurfaceCreateInfoMVK`] <br/> VkMacOSSurfaceCreateInfoMVK - Structure specifying parameters of a newly created macOS surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MacOSSurfaceCreateInfoMVK`] structure is defined as:\n\n```\n// Provided by VK_MVK_macos_surface\ntypedef struct VkMacOSSurfaceCreateInfoMVK {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkMacOSSurfaceCreateFlagsMVK    flags;\n    const void*                     pView;\n} VkMacOSSurfaceCreateInfoMVK;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::p_view`] is a reference to either a [`crate::vk::CAMetalLayer`] object or\n  an `NSView` object.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pView-04144  \n   If [`Self::p_view`] is a [`crate::vk::CAMetalLayer`] object, it **must** be a valid[`crate::vk::CAMetalLayer`].\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pView-01317  \n   If [`Self::p_view`] is an `NSView` object, it **must** be a valid`NSView`, **must** be backed by a `CALayer` object of type[`crate::vk::CAMetalLayer`], and [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`] **must** be called\n  on the main thread.\n\nValid Usage (Implicit)\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MACOS_SURFACE_CREATE_INFO_MVK`]\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMacOSSurfaceCreateInfoMVK-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MacOSSurfaceCreateFlagBitsMVK`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`]\n"]
#[repr(transparent)]
pub struct MacOSSurfaceCreateInfoMVKBuilder<'a>(MacOSSurfaceCreateInfoMVK, std::marker::PhantomData<&'a ()>);
impl<'a> MacOSSurfaceCreateInfoMVKBuilder<'a> {
    #[inline]
    pub fn new() -> MacOSSurfaceCreateInfoMVKBuilder<'a> {
        MacOSSurfaceCreateInfoMVKBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::mvk_macos_surface::MacOSSurfaceCreateFlagsMVK) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn view(mut self, view: *const std::ffi::c_void) -> Self {
        self.0.p_view = view;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MacOSSurfaceCreateInfoMVK {
        self.0
    }
}
impl<'a> std::default::Default for MacOSSurfaceCreateInfoMVKBuilder<'a> {
    fn default() -> MacOSSurfaceCreateInfoMVKBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MacOSSurfaceCreateInfoMVKBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MacOSSurfaceCreateInfoMVKBuilder<'a> {
    type Target = MacOSSurfaceCreateInfoMVK;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MacOSSurfaceCreateInfoMVKBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::mvk_macos_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateMacOSSurfaceMVK.html)) · Function <br/> vkCreateMacOSSurfaceMVK - Create a VkSurfaceKHR object for a macOS NSView\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a macOS `NSView` or[`crate::vk::CAMetalLayer`], call:\n\n```\n// Provided by VK_MVK_macos_surface\nVkResult vkCreateMacOSSurfaceMVK(\n    VkInstance                                  instance,\n    const VkMacOSSurfaceCreateInfoMVK*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n|   |Note<br/><br/>The [`crate::vk::InstanceLoader::create_mac_os_surface_mvk`] function is considered deprecated and has been<br/>superseded by [`crate::vk::InstanceLoader::create_metal_surface_ext`] from the[VK_EXT_metal_surface](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_metal_surface.html) extension.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_description)Description\n----------\n\n* [`Self::instance`] is the instance with which to associate the surface.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::MacOSSurfaceCreateInfoMVK`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateMacOSSurfaceMVK-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::MacOSSurfaceCreateInfoMVK`] structure\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateMacOSSurfaceMVK-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::MacOSSurfaceCreateInfoMVK`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateMacOSSurfaceMVK")]
    pub unsafe fn create_mac_os_surface_mvk(&self, create_info: &crate::extensions::mvk_macos_surface::MacOSSurfaceCreateInfoMVK, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_mac_os_surface_mvk.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
