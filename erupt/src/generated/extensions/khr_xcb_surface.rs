#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_XCB_SURFACE_SPEC_VERSION")]
pub const KHR_XCB_SURFACE_SPEC_VERSION: u32 = 6;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_XCB_SURFACE_EXTENSION_NAME")]
pub const KHR_XCB_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_xcb_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_XCB_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateXcbSurfaceKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_XCB_PRESENTATION_SUPPORT_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceXcbPresentationSupportKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXcbSurfaceCreateFlagsKHR.html)) · Bitmask of [`XcbSurfaceCreateFlagBitsKHR`] <br/> VkXcbSurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_xcb_surface\ntypedef VkFlags VkXcbSurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::XcbSurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask, but\nis currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::XcbSurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkXcbSurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct XcbSurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`XcbSurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkXcbSurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct XcbSurfaceCreateFlagBitsKHR(pub u32);
impl XcbSurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> XcbSurfaceCreateFlagsKHR {
        XcbSurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for XcbSurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_xcb_surface`]"]
impl crate::vk1_0::StructureType {
    pub const XCB_SURFACE_CREATE_INFO_KHR: Self = Self(1000005000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateXcbSurfaceKHR.html)) · Function <br/> vkCreateXcbSurfaceKHR - Create a slink:VkSurfaceKHR object for a X11 window, using the XCB client-side library\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an X11 window, using the XCB\nclient-side library, call:\n\n```\n// Provided by VK_KHR_xcb_surface\nVkResult vkCreateXcbSurfaceKHR(\n    VkInstance                                  instance,\n    const VkXcbSurfaceCreateInfoKHR*            pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::XcbSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateXcbSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateXcbSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::XcbSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateXcbSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateXcbSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::XcbSurfaceCreateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateXcbSurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_xcb_surface::XcbSurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceXcbPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceXcbPresentationSupportKHR - Query physical device for presentation to X11 server using XCB\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to an X11 server, using the XCB client-side library, call:\n\n```\n// Provided by VK_KHR_xcb_surface\nVkBool32 vkGetPhysicalDeviceXcbPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    xcb_connection_t*                           connection,\n    xcb_visualid_t                              visual_id);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::connection`] is a pointer to an `xcb_connection_t` to the X\n  server.\n\n* [`Self::visual_id`] is an X11 visual (`xcb_visualid_t`).\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-queueFamilyIndex-01312  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-connection-parameter  \n  [`Self::connection`] **must** be a valid pointer to an `xcb_connection_t` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceXcbPresentationSupportKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, connection: *mut std::ffi::c_void, visual_id: u32) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXcbSurfaceCreateInfoKHR.html)) · Structure <br/> VkXcbSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Xcb surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::XcbSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_xcb_surface\ntypedef struct VkXcbSurfaceCreateInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkXcbSurfaceCreateFlagsKHR    flags;\n    xcb_connection_t*             connection;\n    xcb_window_t                  window;\n} VkXcbSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::connection`] is a pointer to an `xcb_connection_t` to the X\n  server.\n\n* [`Self::window`] is the `xcb_window_t` for the X11 window to associate\n  the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-connection-01310  \n  [`Self::connection`] **must** point to a valid X11 `xcb_connection_t`\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-window-01311  \n  [`Self::window`] **must** be a valid X11 `xcb_window_t`\n\nValid Usage (Implicit)\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::XCB_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::XcbSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_xcb_surface_khr`]\n"]
#[doc(alias = "VkXcbSurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct XcbSurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_xcb_surface::XcbSurfaceCreateFlagsKHR,
    pub connection: *mut std::ffi::c_void,
    pub window: u32,
}
impl XcbSurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::XCB_SURFACE_CREATE_INFO_KHR;
}
impl Default for XcbSurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), connection: std::ptr::null_mut(), window: Default::default() }
    }
}
impl std::fmt::Debug for XcbSurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("XcbSurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("connection", &self.connection).field("window", &self.window).finish()
    }
}
impl XcbSurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> XcbSurfaceCreateInfoKHRBuilder<'a> {
        XcbSurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXcbSurfaceCreateInfoKHR.html)) · Builder of [`XcbSurfaceCreateInfoKHR`] <br/> VkXcbSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Xcb surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::XcbSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_xcb_surface\ntypedef struct VkXcbSurfaceCreateInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkXcbSurfaceCreateFlagsKHR    flags;\n    xcb_connection_t*             connection;\n    xcb_window_t                  window;\n} VkXcbSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::connection`] is a pointer to an `xcb_connection_t` to the X\n  server.\n\n* [`Self::window`] is the `xcb_window_t` for the X11 window to associate\n  the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-connection-01310  \n  [`Self::connection`] **must** point to a valid X11 `xcb_connection_t`\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-window-01311  \n  [`Self::window`] **must** be a valid X11 `xcb_window_t`\n\nValid Usage (Implicit)\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::XCB_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkXcbSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::XcbSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_xcb_surface_khr`]\n"]
#[repr(transparent)]
pub struct XcbSurfaceCreateInfoKHRBuilder<'a>(XcbSurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> XcbSurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> XcbSurfaceCreateInfoKHRBuilder<'a> {
        XcbSurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_xcb_surface::XcbSurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn connection(mut self, connection: *mut std::ffi::c_void) -> Self {
        self.0.connection = connection;
        self
    }
    #[inline]
    pub fn window(mut self, window: u32) -> Self {
        self.0.window = window as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> XcbSurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for XcbSurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> XcbSurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for XcbSurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for XcbSurfaceCreateInfoKHRBuilder<'a> {
    type Target = XcbSurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for XcbSurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_xcb_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateXcbSurfaceKHR.html)) · Function <br/> vkCreateXcbSurfaceKHR - Create a slink:VkSurfaceKHR object for a X11 window, using the XCB client-side library\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an X11 window, using the XCB\nclient-side library, call:\n\n```\n// Provided by VK_KHR_xcb_surface\nVkResult vkCreateXcbSurfaceKHR(\n    VkInstance                                  instance,\n    const VkXcbSurfaceCreateInfoKHR*            pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::XcbSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateXcbSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateXcbSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::XcbSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateXcbSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateXcbSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::XcbSurfaceCreateInfoKHR`]\n"]
    #[doc(alias = "vkCreateXcbSurfaceKHR")]
    pub unsafe fn create_xcb_surface_khr(&self, create_info: &crate::extensions::khr_xcb_surface::XcbSurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_xcb_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceXcbPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceXcbPresentationSupportKHR - Query physical device for presentation to X11 server using XCB\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to an X11 server, using the XCB client-side library, call:\n\n```\n// Provided by VK_KHR_xcb_surface\nVkBool32 vkGetPhysicalDeviceXcbPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    xcb_connection_t*                           connection,\n    xcb_visualid_t                              visual_id);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::connection`] is a pointer to an `xcb_connection_t` to the X\n  server.\n\n* [`Self::visual_id`] is an X11 visual (`xcb_visualid_t`).\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-queueFamilyIndex-01312  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceXcbPresentationSupportKHR-connection-parameter  \n  [`Self::connection`] **must** be a valid pointer to an `xcb_connection_t` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceXcbPresentationSupportKHR")]
    pub unsafe fn get_physical_device_xcb_presentation_support_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, connection: *mut std::ffi::c_void, visual_id: u32) -> bool {
        let _function = self.get_physical_device_xcb_presentation_support_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _, connection, visual_id as _);
        _return != 0
    }
}
