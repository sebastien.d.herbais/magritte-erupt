#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_RDMA_SPEC_VERSION")]
pub const NV_EXTERNAL_MEMORY_RDMA_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_RDMA_EXTENSION_NAME")]
pub const NV_EXTERNAL_MEMORY_RDMA_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_external_memory_rdma");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_REMOTE_ADDRESS_NV: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryRemoteAddressNV");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRemoteAddressNV.html)) · Basetype <br/> "]
#[doc(alias = "VkRemoteAddressNV")]
pub type RemoteAddressNV = *mut std::ffi::c_void;
#[doc = "Provided by [`crate::extensions::nv_external_memory_rdma`]"]
impl crate::vk1_0::MemoryPropertyFlagBits {
    pub const RDMA_CAPABLE_NV: Self = Self(256);
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_rdma`]"]
impl crate::vk1_0::StructureType {
    pub const MEMORY_GET_REMOTE_ADDRESS_INFO_NV: Self = Self(1000371000);
    pub const PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV: Self = Self(1000371001);
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_rdma`]"]
impl crate::vk1_1::ExternalMemoryHandleTypeFlagBits {
    pub const RDMA_ADDRESS_NV: Self = Self(4096);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryRemoteAddressNV.html)) · Function <br/> vkGetMemoryRemoteAddressNV - Get an address for a memory object accessible by remote devices\n[](#_c_specification)C Specification\n----------\n\nTo export an address representing the payload of a Vulkan device memory\nobject accessible by remote devices, call:\n\n```\n// Provided by VK_NV_external_memory_rdma\nVkResult vkGetMemoryRemoteAddressNV(\n    VkDevice                                    device,\n    const VkMemoryGetRemoteAddressInfoNV*       pMemoryGetRemoteAddressInfo,\n    VkRemoteAddressNV*                          pAddress);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_memory_get_remote_address_info`] is a pointer to a[`crate::vk::MemoryGetRemoteAddressInfoNV`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_address`] will return the address representing the payload of the\n  device memory object.\n[](#_description)Description\n----------\n\nMore communication may be required between the kernel-mode drivers of the\ndevices involved.\nThis information is out of scope of this documentation and should be\nrequested from the vendors of the devices.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryRemoteAddressNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryRemoteAddressNV-pMemoryGetRemoteAddressInfo-parameter  \n  [`Self::p_memory_get_remote_address_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetRemoteAddressInfoNV`] structure\n\n* []() VUID-vkGetMemoryRemoteAddressNV-pAddress-parameter  \n  [`Self::p_address`] **must** be a valid pointer to a [`crate::vk::RemoteAddressNV`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetRemoteAddressInfoNV`], [`crate::vk::RemoteAddressNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryRemoteAddressNV = unsafe extern "system" fn(device: crate::vk1_0::Device, p_memory_get_remote_address_info: *const crate::extensions::nv_external_memory_rdma::MemoryGetRemoteAddressInfoNV, p_address: *mut crate::extensions::nv_external_memory_rdma::RemoteAddressNV) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceExternalMemoryRDMAFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExternalMemoryRDMAFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExternalMemoryRDMAFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceExternalMemoryRDMAFeaturesNV - Structure describing the external memory RDMA features supported by the implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_external_memory_rdma\ntypedef struct VkPhysicalDeviceExternalMemoryRDMAFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           externalMemoryRDMA;\n} VkPhysicalDeviceExternalMemoryRDMAFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::external_memory_rdma`] indicates\n  whether the implementation has support for the[`crate::vk::MemoryPropertyFlagBits::RDMA_CAPABLE_NV`] memory property and the[`crate::vk::ExternalMemoryHandleTypeFlagBits::RDMA_ADDRESS_NV`] external memory\n  handle type.\n\nIf the [`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExternalMemoryRDMAFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceExternalMemoryRDMAFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub external_memory_rdma: crate::vk1_0::Bool32,
}
impl PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV;
}
impl Default for PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), external_memory_rdma: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceExternalMemoryRDMAFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("external_memory_rdma", &(self.external_memory_rdma != 0)).finish()
    }
}
impl PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
        PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExternalMemoryRDMAFeaturesNV.html)) · Builder of [`PhysicalDeviceExternalMemoryRDMAFeaturesNV`] <br/> VkPhysicalDeviceExternalMemoryRDMAFeaturesNV - Structure describing the external memory RDMA features supported by the implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_external_memory_rdma\ntypedef struct VkPhysicalDeviceExternalMemoryRDMAFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           externalMemoryRDMA;\n} VkPhysicalDeviceExternalMemoryRDMAFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::external_memory_rdma`] indicates\n  whether the implementation has support for the[`crate::vk::MemoryPropertyFlagBits::RDMA_CAPABLE_NV`] memory property and the[`crate::vk::ExternalMemoryHandleTypeFlagBits::RDMA_ADDRESS_NV`] external memory\n  handle type.\n\nIf the [`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceExternalMemoryRDMAFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExternalMemoryRDMAFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a>(PhysicalDeviceExternalMemoryRDMAFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
        PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn external_memory_rdma(mut self, external_memory_rdma: bool) -> Self {
        self.0.external_memory_rdma = external_memory_rdma as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceExternalMemoryRDMAFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceExternalMemoryRDMAFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceExternalMemoryRDMAFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetRemoteAddressInfoNV.html)) · Structure <br/> VkMemoryGetRemoteAddressInfoNV - Structure describing a remote accessible address export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetRemoteAddressInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_rdma\ntypedef struct VkMemoryGetRemoteAddressInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetRemoteAddressInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the remote accessible\n  address will be exported.\n\n* [`Self::handle_type`] is the type of handle requested.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-handleType-04966  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_REMOTE_ADDRESS_INFO_NV`]\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_remote_address_nv`]\n"]
#[doc(alias = "VkMemoryGetRemoteAddressInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryGetRemoteAddressInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory: crate::vk1_0::DeviceMemory,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
}
impl MemoryGetRemoteAddressInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_GET_REMOTE_ADDRESS_INFO_NV;
}
impl Default for MemoryGetRemoteAddressInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for MemoryGetRemoteAddressInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryGetRemoteAddressInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory", &self.memory).field("handle_type", &self.handle_type).finish()
    }
}
impl MemoryGetRemoteAddressInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryGetRemoteAddressInfoNVBuilder<'a> {
        MemoryGetRemoteAddressInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetRemoteAddressInfoNV.html)) · Builder of [`MemoryGetRemoteAddressInfoNV`] <br/> VkMemoryGetRemoteAddressInfoNV - Structure describing a remote accessible address export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetRemoteAddressInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_rdma\ntypedef struct VkMemoryGetRemoteAddressInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetRemoteAddressInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the remote accessible\n  address will be exported.\n\n* [`Self::handle_type`] is the type of handle requested.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-handleType-04966  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_REMOTE_ADDRESS_INFO_NV`]\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetRemoteAddressInfoNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_remote_address_nv`]\n"]
#[repr(transparent)]
pub struct MemoryGetRemoteAddressInfoNVBuilder<'a>(MemoryGetRemoteAddressInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryGetRemoteAddressInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryGetRemoteAddressInfoNVBuilder<'a> {
        MemoryGetRemoteAddressInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory(mut self, memory: crate::vk1_0::DeviceMemory) -> Self {
        self.0.memory = memory as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryGetRemoteAddressInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for MemoryGetRemoteAddressInfoNVBuilder<'a> {
    fn default() -> MemoryGetRemoteAddressInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryGetRemoteAddressInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryGetRemoteAddressInfoNVBuilder<'a> {
    type Target = MemoryGetRemoteAddressInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryGetRemoteAddressInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_rdma`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryRemoteAddressNV.html)) · Function <br/> vkGetMemoryRemoteAddressNV - Get an address for a memory object accessible by remote devices\n[](#_c_specification)C Specification\n----------\n\nTo export an address representing the payload of a Vulkan device memory\nobject accessible by remote devices, call:\n\n```\n// Provided by VK_NV_external_memory_rdma\nVkResult vkGetMemoryRemoteAddressNV(\n    VkDevice                                    device,\n    const VkMemoryGetRemoteAddressInfoNV*       pMemoryGetRemoteAddressInfo,\n    VkRemoteAddressNV*                          pAddress);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_memory_get_remote_address_info`] is a pointer to a[`crate::vk::MemoryGetRemoteAddressInfoNV`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_address`] will return the address representing the payload of the\n  device memory object.\n[](#_description)Description\n----------\n\nMore communication may be required between the kernel-mode drivers of the\ndevices involved.\nThis information is out of scope of this documentation and should be\nrequested from the vendors of the devices.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryRemoteAddressNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryRemoteAddressNV-pMemoryGetRemoteAddressInfo-parameter  \n  [`Self::p_memory_get_remote_address_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetRemoteAddressInfoNV`] structure\n\n* []() VUID-vkGetMemoryRemoteAddressNV-pAddress-parameter  \n  [`Self::p_address`] **must** be a valid pointer to a [`crate::vk::RemoteAddressNV`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetRemoteAddressInfoNV`], [`crate::vk::RemoteAddressNV`]\n"]
    #[doc(alias = "vkGetMemoryRemoteAddressNV")]
    pub unsafe fn get_memory_remote_address_nv(&self, memory_get_remote_address_info: &crate::extensions::nv_external_memory_rdma::MemoryGetRemoteAddressInfoNV) -> crate::utils::VulkanResult<crate::extensions::nv_external_memory_rdma::RemoteAddressNV> {
        let _function = self.get_memory_remote_address_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut address = std::ptr::null_mut();
        let _return = _function(self.handle, memory_get_remote_address_info as _, &mut address);
        crate::utils::VulkanResult::new(_return, address)
    }
}
