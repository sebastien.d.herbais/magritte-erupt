#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_RAY_TRACING_MOTION_BLUR_SPEC_VERSION")]
pub const NV_RAY_TRACING_MOTION_BLUR_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_RAY_TRACING_MOTION_BLUR_EXTENSION_NAME")]
pub const NV_RAY_TRACING_MOTION_BLUR_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_ray_tracing_motion_blur");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInfoFlagsNV.html)) · Bitmask of [`AccelerationStructureMotionInfoFlagBitsNV`] <br/> "] # [doc (alias = "VkAccelerationStructureMotionInfoFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct AccelerationStructureMotionInfoFlagsNV : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`AccelerationStructureMotionInfoFlagsNV`] <br/> "]
#[doc(alias = "VkAccelerationStructureMotionInfoFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct AccelerationStructureMotionInfoFlagBitsNV(pub u32);
impl AccelerationStructureMotionInfoFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> AccelerationStructureMotionInfoFlagsNV {
        AccelerationStructureMotionInfoFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for AccelerationStructureMotionInfoFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInstanceFlagsNV.html)) · Bitmask of [`AccelerationStructureMotionInstanceFlagBitsNV`] <br/> "] # [doc (alias = "VkAccelerationStructureMotionInstanceFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct AccelerationStructureMotionInstanceFlagsNV : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`AccelerationStructureMotionInstanceFlagsNV`] <br/> "]
#[doc(alias = "VkAccelerationStructureMotionInstanceFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct AccelerationStructureMotionInstanceFlagBitsNV(pub u32);
impl AccelerationStructureMotionInstanceFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> AccelerationStructureMotionInstanceFlagsNV {
        AccelerationStructureMotionInstanceFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for AccelerationStructureMotionInstanceFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_ray_tracing_motion_blur`]"]
impl crate::vk1_0::PipelineCreateFlagBits {
    pub const RAY_TRACING_ALLOW_MOTION_NV: Self = Self(1048576);
}
#[doc = "Provided by [`crate::extensions::nv_ray_tracing_motion_blur`]"]
impl crate::vk1_0::StructureType {
    pub const ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV: Self = Self(1000327000);
    pub const PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV: Self = Self(1000327001);
    pub const ACCELERATION_STRUCTURE_MOTION_INFO_NV: Self = Self(1000327002);
}
#[doc = "Provided by [`crate::extensions::nv_ray_tracing_motion_blur`]"]
impl crate::extensions::khr_acceleration_structure::BuildAccelerationStructureFlagBitsKHR {
    pub const MOTION_NV: Self = Self(32);
}
#[doc = "Provided by [`crate::extensions::nv_ray_tracing_motion_blur`]"]
impl crate::extensions::khr_acceleration_structure::AccelerationStructureCreateFlagBitsKHR {
    pub const MOTION_NV: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInstanceTypeNV.html)) · Enum <br/> VkAccelerationStructureMotionInstanceTypeNV - Enum specifying a type of acceleration structure motion instance data for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AccelerationStructureMotionInstanceTypeNV`] enumeration is defined\nas:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef enum VkAccelerationStructureMotionInstanceTypeNV {\n    VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_STATIC_NV = 0,\n    VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MATRIX_MOTION_NV = 1,\n    VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_SRT_MOTION_NV = 2,\n} VkAccelerationStructureMotionInstanceTypeNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::STATIC_NV`] specifies\n  that the instance is a static instance with no instance motion.\n\n* [`Self::MATRIX_MOTION_NV`]specifies that the instance is a motion instance with motion specified\n  by interpolation between two matrices.\n\n* [`Self::SRT_MOTION_NV`]specifies that the instance is a motion instance with motion specified\n  by interpolation in the SRT decomposition.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceNV`]\n"]
#[doc(alias = "VkAccelerationStructureMotionInstanceTypeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct AccelerationStructureMotionInstanceTypeNV(pub i32);
impl std::fmt::Debug for AccelerationStructureMotionInstanceTypeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::STATIC_NV => "STATIC_NV",
            &Self::MATRIX_MOTION_NV => "MATRIX_MOTION_NV",
            &Self::SRT_MOTION_NV => "SRT_MOTION_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_ray_tracing_motion_blur`]"]
impl crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceTypeNV {
    pub const STATIC_NV: Self = Self(0);
    pub const MATRIX_MOTION_NV: Self = Self(1);
    pub const SRT_MOTION_NV: Self = Self(2);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRayTracingMotionBlurFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRayTracingMotionBlurFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, AccelerationStructureGeometryMotionTrianglesDataNV> for crate::extensions::khr_acceleration_structure::AccelerationStructureGeometryTrianglesDataKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'_>> for crate::extensions::khr_acceleration_structure::AccelerationStructureGeometryTrianglesDataKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, AccelerationStructureMotionInfoNV> for crate::extensions::khr_acceleration_structure::AccelerationStructureCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, AccelerationStructureMotionInfoNVBuilder<'_>> for crate::extensions::khr_acceleration_structure::AccelerationStructureCreateInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingMotionBlurFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceRayTracingMotionBlurFeaturesNV - Structure describing the ray tracing motion blur features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkPhysicalDeviceRayTracingMotionBlurFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rayTracingMotionBlur;\n    VkBool32           rayTracingMotionBlurPipelineTraceRaysIndirect;\n} VkPhysicalDeviceRayTracingMotionBlurFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature: \\* [`Self::s_type`] is the type of this structure. \\* [`Self::p_next`] is `NULL` or a pointer to a structure extending this structure. \\* []() [`Self::ray_tracing_motion_blur`] indicates whether the implementation supports the motion blur feature. \\* []() [`Self::ray_tracing_motion_blur_pipeline_trace_rays_indirect`] indicates whether the implementation supports indirect trace ray commands with the motion blur feature enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRayTracingMotionBlurFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceRayTracingMotionBlurFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub ray_tracing_motion_blur: crate::vk1_0::Bool32,
    pub ray_tracing_motion_blur_pipeline_trace_rays_indirect: crate::vk1_0::Bool32,
}
impl PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV;
}
impl Default for PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), ray_tracing_motion_blur: Default::default(), ray_tracing_motion_blur_pipeline_trace_rays_indirect: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceRayTracingMotionBlurFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("ray_tracing_motion_blur", &(self.ray_tracing_motion_blur != 0)).field("ray_tracing_motion_blur_pipeline_trace_rays_indirect", &(self.ray_tracing_motion_blur_pipeline_trace_rays_indirect != 0)).finish()
    }
}
impl PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
        PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayTracingMotionBlurFeaturesNV.html)) · Builder of [`PhysicalDeviceRayTracingMotionBlurFeaturesNV`] <br/> VkPhysicalDeviceRayTracingMotionBlurFeaturesNV - Structure describing the ray tracing motion blur features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkPhysicalDeviceRayTracingMotionBlurFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rayTracingMotionBlur;\n    VkBool32           rayTracingMotionBlurPipelineTraceRaysIndirect;\n} VkPhysicalDeviceRayTracingMotionBlurFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature: \\* [`Self::s_type`] is the type of this structure. \\* [`Self::p_next`] is `NULL` or a pointer to a structure extending this structure. \\* []() [`Self::ray_tracing_motion_blur`] indicates whether the implementation supports the motion blur feature. \\* []() [`Self::ray_tracing_motion_blur_pipeline_trace_rays_indirect`] indicates whether the implementation supports indirect trace ray commands with the motion blur feature enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRayTracingMotionBlurFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRayTracingMotionBlurFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a>(PhysicalDeviceRayTracingMotionBlurFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
        PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn ray_tracing_motion_blur(mut self, ray_tracing_motion_blur: bool) -> Self {
        self.0.ray_tracing_motion_blur = ray_tracing_motion_blur as _;
        self
    }
    #[inline]
    pub fn ray_tracing_motion_blur_pipeline_trace_rays_indirect(mut self, ray_tracing_motion_blur_pipeline_trace_rays_indirect: bool) -> Self {
        self.0.ray_tracing_motion_blur_pipeline_trace_rays_indirect = ray_tracing_motion_blur_pipeline_trace_rays_indirect as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceRayTracingMotionBlurFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceRayTracingMotionBlurFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceRayTracingMotionBlurFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureGeometryMotionTrianglesDataNV.html)) · Structure <br/> VkAccelerationStructureGeometryMotionTrianglesDataNV - Structure specifying vertex motion in a bottom-level acceleration structure\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureGeometryMotionTrianglesDataNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkDeviceOrHostAddressConstKHR    vertexData;\n} VkAccelerationStructureGeometryMotionTrianglesDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::vertex_data`] is a pointer to vertex data for this geometry at time\n  1.0\n[](#_description)Description\n----------\n\nIf [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] is included in\nthe [`Self::p_next`] chain of a[`crate::vk::AccelerationStructureGeometryTrianglesDataKHR`] structure, the basic\nvertex positions are used for the position of the triangles in the geometry\nat time 0.0 and the [`Self::vertex_data`] in[`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] is used for the\nvertex positions at time 1.0, with positions linearly interpolated at\nintermediate times.\n\nIndexing for [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`][`Self::vertex_data`] is equivalent to the basic vertex position data.\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureGeometryMotionTrianglesDataNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceOrHostAddressConstKHR`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkAccelerationStructureGeometryMotionTrianglesDataNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureGeometryMotionTrianglesDataNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub vertex_data: crate::extensions::khr_acceleration_structure::DeviceOrHostAddressConstKHR,
}
impl AccelerationStructureGeometryMotionTrianglesDataNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV;
}
impl Default for AccelerationStructureGeometryMotionTrianglesDataNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), vertex_data: Default::default() }
    }
}
impl std::fmt::Debug for AccelerationStructureGeometryMotionTrianglesDataNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureGeometryMotionTrianglesDataNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("vertex_data", &self.vertex_data).finish()
    }
}
impl AccelerationStructureGeometryMotionTrianglesDataNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
        AccelerationStructureGeometryMotionTrianglesDataNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureGeometryMotionTrianglesDataNV.html)) · Builder of [`AccelerationStructureGeometryMotionTrianglesDataNV`] <br/> VkAccelerationStructureGeometryMotionTrianglesDataNV - Structure specifying vertex motion in a bottom-level acceleration structure\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureGeometryMotionTrianglesDataNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkDeviceOrHostAddressConstKHR    vertexData;\n} VkAccelerationStructureGeometryMotionTrianglesDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::vertex_data`] is a pointer to vertex data for this geometry at time\n  1.0\n[](#_description)Description\n----------\n\nIf [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] is included in\nthe [`Self::p_next`] chain of a[`crate::vk::AccelerationStructureGeometryTrianglesDataKHR`] structure, the basic\nvertex positions are used for the position of the triangles in the geometry\nat time 0.0 and the [`Self::vertex_data`] in[`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`] is used for the\nvertex positions at time 1.0, with positions linearly interpolated at\nintermediate times.\n\nIndexing for [`crate::vk::AccelerationStructureGeometryMotionTrianglesDataNV`][`Self::vertex_data`] is equivalent to the basic vertex position data.\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureGeometryMotionTrianglesDataNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceOrHostAddressConstKHR`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a>(AccelerationStructureGeometryMotionTrianglesDataNV, std::marker::PhantomData<&'a ()>);
impl<'a> AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
        AccelerationStructureGeometryMotionTrianglesDataNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn vertex_data(mut self, vertex_data: crate::extensions::khr_acceleration_structure::DeviceOrHostAddressConstKHR) -> Self {
        self.0.vertex_data = vertex_data as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureGeometryMotionTrianglesDataNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
    fn default() -> AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
    type Target = AccelerationStructureGeometryMotionTrianglesDataNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureGeometryMotionTrianglesDataNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInfoNV.html)) · Structure <br/> VkAccelerationStructureMotionInfoNV - Structure specifying the parameters of a newly created acceleration structure object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AccelerationStructureMotionInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMotionInfoNV {\n    VkStructureType                             sType;\n    const void*                                 pNext;\n    uint32_t                                    maxInstances;\n    VkAccelerationStructureMotionInfoFlagsNV    flags;\n} VkAccelerationStructureMotionInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_instances`] is the maximum number of instances that **may** be used\n  in the motion top-level acceleration structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMotionInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACCELERATION_STRUCTURE_MOTION_INFO_NV`]\n\n* []() VUID-VkAccelerationStructureMotionInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInfoFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkAccelerationStructureMotionInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureMotionInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub max_instances: u32,
    pub flags: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInfoFlagsNV,
}
impl AccelerationStructureMotionInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ACCELERATION_STRUCTURE_MOTION_INFO_NV;
}
impl Default for AccelerationStructureMotionInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), max_instances: Default::default(), flags: Default::default() }
    }
}
impl std::fmt::Debug for AccelerationStructureMotionInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureMotionInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_instances", &self.max_instances).field("flags", &self.flags).finish()
    }
}
impl AccelerationStructureMotionInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureMotionInfoNVBuilder<'a> {
        AccelerationStructureMotionInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInfoNV.html)) · Builder of [`AccelerationStructureMotionInfoNV`] <br/> VkAccelerationStructureMotionInfoNV - Structure specifying the parameters of a newly created acceleration structure object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AccelerationStructureMotionInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMotionInfoNV {\n    VkStructureType                             sType;\n    const void*                                 pNext;\n    uint32_t                                    maxInstances;\n    VkAccelerationStructureMotionInfoFlagsNV    flags;\n} VkAccelerationStructureMotionInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_instances`] is the maximum number of instances that **may** be used\n  in the motion top-level acceleration structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMotionInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACCELERATION_STRUCTURE_MOTION_INFO_NV`]\n\n* []() VUID-VkAccelerationStructureMotionInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInfoFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct AccelerationStructureMotionInfoNVBuilder<'a>(AccelerationStructureMotionInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> AccelerationStructureMotionInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureMotionInfoNVBuilder<'a> {
        AccelerationStructureMotionInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_instances(mut self, max_instances: u32) -> Self {
        self.0.max_instances = max_instances as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInfoFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureMotionInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureMotionInfoNVBuilder<'a> {
    fn default() -> AccelerationStructureMotionInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureMotionInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureMotionInfoNVBuilder<'a> {
    type Target = AccelerationStructureMotionInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureMotionInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSRTDataNV.html)) · Structure <br/> VkSRTDataNV - Structure specifying a transform in SRT decomposition\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure SRT transform is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkSRTDataNV {\n    float    sx;\n    float    a;\n    float    b;\n    float    pvx;\n    float    sy;\n    float    c;\n    float    pvy;\n    float    sz;\n    float    pvz;\n    float    qx;\n    float    qy;\n    float    qz;\n    float    qw;\n    float    tx;\n    float    ty;\n    float    tz;\n} VkSRTDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::sx`] is the x component of the scale of the transform\n\n* [`Self::a`] is one component of the shear for the transform\n\n* [`Self::b`] is one component of the shear for the transform\n\n* [`Self::pvx`] is the x component of the pivot point of the transform\n\n* [`Self::sy`] is the y component of the scale of the transform\n\n* [`Self::c`] is one component of the shear for the transform\n\n* [`Self::pvy`] is the y component of the pivot point of the transform\n\n* [`Self::sz`] is the z component of the scale of the transform\n\n* [`Self::pvz`] is the z component of the pivot point of the transform\n\n* [`Self::qx`] is the x component of the rotation quaternion\n\n* [`Self::qy`] is the y component of the rotation quaternion\n\n* [`Self::qz`] is the z component of the rotation quaternion\n\n* [`Self::qw`] is the w component of the rotation quaternion\n\n* [`Self::tx`] is the x component of the post-rotation translation\n\n* [`Self::ty`] is the y component of the post-rotation translation\n\n* [`Self::tz`] is the z component of the post-rotation translation\n[](#_description)Description\n----------\n\nThis transform decomposition consists of three elements.\nThe first is a matrix S, consisting of a scale, shear, and translation,\nusually used to define the pivot point of the following rotation.\nThis matrix is constructed from the parameters above by:\n\nS=⎝⎜⎛\u{200b}sx00\u{200b}asy0\u{200b}bcsz\u{200b}pvxpvypvz\u{200b}⎠⎟⎞\u{200b}\n\nThe rotation quaternion is defined as:\n\n`R` = [ [`Self::qx`], [`Self::qy`], [`Self::qz`], [`Self::qw`] ]\n\nThis is a rotation around a conceptual normalized axis [ ax, ay, az ]of amount `theta` such that:\n\n[ [`Self::qx`], [`Self::qy`], [`Self::qz`] ] = sin(`theta`/2)\n× [ `ax`, `ay`, `az` ]\n\nand\n\n[`Self::qw`] = cos(`theta`/2)\n\nFinally, the transform has a translation T constructed from the parameters\nabove by:\n\nT=⎝⎜⎛\u{200b}100\u{200b}010\u{200b}001\u{200b}txtytz\u{200b}⎠⎟⎞\u{200b}\n\nThe effective derived transform is then given by\n\n`T` × `R` × `S`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureSRTMotionInstanceNV`]\n"]
#[doc(alias = "VkSRTDataNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SRTDataNV {
    pub sx: std::os::raw::c_float,
    pub a: std::os::raw::c_float,
    pub b: std::os::raw::c_float,
    pub pvx: std::os::raw::c_float,
    pub sy: std::os::raw::c_float,
    pub c: std::os::raw::c_float,
    pub pvy: std::os::raw::c_float,
    pub sz: std::os::raw::c_float,
    pub pvz: std::os::raw::c_float,
    pub qx: std::os::raw::c_float,
    pub qy: std::os::raw::c_float,
    pub qz: std::os::raw::c_float,
    pub qw: std::os::raw::c_float,
    pub tx: std::os::raw::c_float,
    pub ty: std::os::raw::c_float,
    pub tz: std::os::raw::c_float,
}
impl Default for SRTDataNV {
    fn default() -> Self {
        Self { sx: Default::default(), a: Default::default(), b: Default::default(), pvx: Default::default(), sy: Default::default(), c: Default::default(), pvy: Default::default(), sz: Default::default(), pvz: Default::default(), qx: Default::default(), qy: Default::default(), qz: Default::default(), qw: Default::default(), tx: Default::default(), ty: Default::default(), tz: Default::default() }
    }
}
impl std::fmt::Debug for SRTDataNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SRTDataNV").field("sx", &self.sx).field("a", &self.a).field("b", &self.b).field("pvx", &self.pvx).field("sy", &self.sy).field("c", &self.c).field("pvy", &self.pvy).field("sz", &self.sz).field("pvz", &self.pvz).field("qx", &self.qx).field("qy", &self.qy).field("qz", &self.qz).field("qw", &self.qw).field("tx", &self.tx).field("ty", &self.ty).field("tz", &self.tz).finish()
    }
}
impl SRTDataNV {
    #[inline]
    pub fn into_builder<'a>(self) -> SRTDataNVBuilder<'a> {
        SRTDataNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSRTDataNV.html)) · Builder of [`SRTDataNV`] <br/> VkSRTDataNV - Structure specifying a transform in SRT decomposition\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure SRT transform is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkSRTDataNV {\n    float    sx;\n    float    a;\n    float    b;\n    float    pvx;\n    float    sy;\n    float    c;\n    float    pvy;\n    float    sz;\n    float    pvz;\n    float    qx;\n    float    qy;\n    float    qz;\n    float    qw;\n    float    tx;\n    float    ty;\n    float    tz;\n} VkSRTDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::sx`] is the x component of the scale of the transform\n\n* [`Self::a`] is one component of the shear for the transform\n\n* [`Self::b`] is one component of the shear for the transform\n\n* [`Self::pvx`] is the x component of the pivot point of the transform\n\n* [`Self::sy`] is the y component of the scale of the transform\n\n* [`Self::c`] is one component of the shear for the transform\n\n* [`Self::pvy`] is the y component of the pivot point of the transform\n\n* [`Self::sz`] is the z component of the scale of the transform\n\n* [`Self::pvz`] is the z component of the pivot point of the transform\n\n* [`Self::qx`] is the x component of the rotation quaternion\n\n* [`Self::qy`] is the y component of the rotation quaternion\n\n* [`Self::qz`] is the z component of the rotation quaternion\n\n* [`Self::qw`] is the w component of the rotation quaternion\n\n* [`Self::tx`] is the x component of the post-rotation translation\n\n* [`Self::ty`] is the y component of the post-rotation translation\n\n* [`Self::tz`] is the z component of the post-rotation translation\n[](#_description)Description\n----------\n\nThis transform decomposition consists of three elements.\nThe first is a matrix S, consisting of a scale, shear, and translation,\nusually used to define the pivot point of the following rotation.\nThis matrix is constructed from the parameters above by:\n\nS=⎝⎜⎛\u{200b}sx00\u{200b}asy0\u{200b}bcsz\u{200b}pvxpvypvz\u{200b}⎠⎟⎞\u{200b}\n\nThe rotation quaternion is defined as:\n\n`R` = [ [`Self::qx`], [`Self::qy`], [`Self::qz`], [`Self::qw`] ]\n\nThis is a rotation around a conceptual normalized axis [ ax, ay, az ]of amount `theta` such that:\n\n[ [`Self::qx`], [`Self::qy`], [`Self::qz`] ] = sin(`theta`/2)\n× [ `ax`, `ay`, `az` ]\n\nand\n\n[`Self::qw`] = cos(`theta`/2)\n\nFinally, the transform has a translation T constructed from the parameters\nabove by:\n\nT=⎝⎜⎛\u{200b}100\u{200b}010\u{200b}001\u{200b}txtytz\u{200b}⎠⎟⎞\u{200b}\n\nThe effective derived transform is then given by\n\n`T` × `R` × `S`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureSRTMotionInstanceNV`]\n"]
#[repr(transparent)]
pub struct SRTDataNVBuilder<'a>(SRTDataNV, std::marker::PhantomData<&'a ()>);
impl<'a> SRTDataNVBuilder<'a> {
    #[inline]
    pub fn new() -> SRTDataNVBuilder<'a> {
        SRTDataNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sx(mut self, sx: std::os::raw::c_float) -> Self {
        self.0.sx = sx as _;
        self
    }
    #[inline]
    pub fn a(mut self, a: std::os::raw::c_float) -> Self {
        self.0.a = a as _;
        self
    }
    #[inline]
    pub fn b(mut self, b: std::os::raw::c_float) -> Self {
        self.0.b = b as _;
        self
    }
    #[inline]
    pub fn pvx(mut self, pvx: std::os::raw::c_float) -> Self {
        self.0.pvx = pvx as _;
        self
    }
    #[inline]
    pub fn sy(mut self, sy: std::os::raw::c_float) -> Self {
        self.0.sy = sy as _;
        self
    }
    #[inline]
    pub fn c(mut self, c: std::os::raw::c_float) -> Self {
        self.0.c = c as _;
        self
    }
    #[inline]
    pub fn pvy(mut self, pvy: std::os::raw::c_float) -> Self {
        self.0.pvy = pvy as _;
        self
    }
    #[inline]
    pub fn sz(mut self, sz: std::os::raw::c_float) -> Self {
        self.0.sz = sz as _;
        self
    }
    #[inline]
    pub fn pvz(mut self, pvz: std::os::raw::c_float) -> Self {
        self.0.pvz = pvz as _;
        self
    }
    #[inline]
    pub fn qx(mut self, qx: std::os::raw::c_float) -> Self {
        self.0.qx = qx as _;
        self
    }
    #[inline]
    pub fn qy(mut self, qy: std::os::raw::c_float) -> Self {
        self.0.qy = qy as _;
        self
    }
    #[inline]
    pub fn qz(mut self, qz: std::os::raw::c_float) -> Self {
        self.0.qz = qz as _;
        self
    }
    #[inline]
    pub fn qw(mut self, qw: std::os::raw::c_float) -> Self {
        self.0.qw = qw as _;
        self
    }
    #[inline]
    pub fn tx(mut self, tx: std::os::raw::c_float) -> Self {
        self.0.tx = tx as _;
        self
    }
    #[inline]
    pub fn ty(mut self, ty: std::os::raw::c_float) -> Self {
        self.0.ty = ty as _;
        self
    }
    #[inline]
    pub fn tz(mut self, tz: std::os::raw::c_float) -> Self {
        self.0.tz = tz as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SRTDataNV {
        self.0
    }
}
impl<'a> std::default::Default for SRTDataNVBuilder<'a> {
    fn default() -> SRTDataNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SRTDataNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SRTDataNVBuilder<'a> {
    type Target = SRTDataNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SRTDataNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureSRTMotionInstanceNV.html)) · Structure <br/> VkAccelerationStructureSRTMotionInstanceNV - Structure specifying a single acceleration structure SRT motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure SRT motion instance is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureSRTMotionInstanceNV {\n    VkSRTDataNV                   transformT0;\n    VkSRTDataNV                   transformT1;\n    uint32_t                      instanceCustomIndex:24;\n    uint32_t                      mask:8;\n    uint32_t                      instanceShaderBindingTableRecordOffset:24;\n    VkGeometryInstanceFlagsKHR    flags:8;\n    uint64_t                      accelerationStructureReference;\n} VkAccelerationStructureSRTMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::transform_t0`] is a [`crate::vk::SRTDataNV`] structure describing a\n  transformation to be applied to the acceleration structure at time 0.\n\n* [`Self::transform_t1`] is a [`crate::vk::SRTDataNV`] structure describing a\n  transformation to be applied to the acceleration structure at time 1.\n\n* [`Self::instance_custom_index_and_mask`] is a 24-bit user-specified index value\n  accessible to ray shaders in the `InstanceCustomIndexKHR` built-in.\n\n* `mask` is an 8-bit visibility mask for the geometry.\n  The instance **may** only be hit if `rayMask & instance.mask != 0`\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] is a 24-bit offset used in\n  calculating the hit shader binding table index.\n\n* `flags` is an 8-bit mask of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html)values to apply to this instance.\n\n* [`Self::acceleration_structure_reference`] is either:\n\n  * a device address containing the value obtained from[`crate::vk::DeviceLoader::get_acceleration_structure_device_address_khr`]or[`crate::vk::DeviceLoader::get_acceleration_structure_handle_nv`] (used by device operations which reference acceleration structures) or,\n\n  * a [`crate::vk::AccelerationStructureKHR`] object (used by host operations\n    which reference acceleration structures).\n[](#_description)Description\n----------\n\nThe C language specification does not define the ordering of bit-fields, but\nin practice, this struct produces the correct layout with existing\ncompilers.\nThe intended bit pattern is for the following:\n\n* [`Self::instance_custom_index_and_mask`] and `mask` occupy the same memory as if a\n  single `uint32_t` was specified in their place\n\n  * [`Self::instance_custom_index_and_mask`] occupies the 24 least significant bits of\n    that memory\n\n  * `mask` occupies the 8 most significant bits of that memory\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] and `flags` occupy the\n  same memory as if a single `uint32_t` was specified in their place\n\n  * [`Self::instance_shader_binding_table_record_offset_and_flags`] occupies the 24 least\n    significant bits of that memory\n\n  * `flags` occupies the 8 most significant bits of that memory\n\nIf a compiler produces code that diverges from that pattern, applications**must** employ another method to set values according to the correct bit\npattern.\n\nThe transform for a SRT motion instance at a point in time is derived from\ncomponent-wise linear interpolation of the two SRT transforms.\nThat is, for a `time` in [0,1] the resulting transform is\n\n[`Self::transform_t0`] × (1 - `time`) + [`Self::transform_t1`] × `time`\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureSRTMotionInstanceNV-flags-parameter  \n  `flags` **must** be a valid combination of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::GeometryInstanceFlagBitsKHR`], [`crate::vk::SRTDataNV`]\n"]
#[doc(alias = "VkAccelerationStructureSRTMotionInstanceNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureSRTMotionInstanceNV {
    pub transform_t0: crate::extensions::nv_ray_tracing_motion_blur::SRTDataNV,
    pub transform_t1: crate::extensions::nv_ray_tracing_motion_blur::SRTDataNV,
    pub instance_custom_index_and_mask: u32,
    pub instance_shader_binding_table_record_offset_and_flags: u32,
    pub acceleration_structure_reference: u64,
}
impl Default for AccelerationStructureSRTMotionInstanceNV {
    fn default() -> Self {
        Self { transform_t0: Default::default(), transform_t1: Default::default(), instance_custom_index_and_mask: Default::default(), instance_shader_binding_table_record_offset_and_flags: Default::default(), acceleration_structure_reference: Default::default() }
    }
}
impl std::fmt::Debug for AccelerationStructureSRTMotionInstanceNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureSRTMotionInstanceNV").field("transform_t0", &self.transform_t0).field("transform_t1", &self.transform_t1).field("instance_custom_index_and_mask", &format!("{:#b}", &self.instance_custom_index_and_mask)).field("instance_shader_binding_table_record_offset_and_flags", &format!("{:#b}", &self.instance_shader_binding_table_record_offset_and_flags)).field("acceleration_structure_reference", &self.acceleration_structure_reference).finish()
    }
}
impl AccelerationStructureSRTMotionInstanceNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
        AccelerationStructureSRTMotionInstanceNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureSRTMotionInstanceNV.html)) · Builder of [`AccelerationStructureSRTMotionInstanceNV`] <br/> VkAccelerationStructureSRTMotionInstanceNV - Structure specifying a single acceleration structure SRT motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure SRT motion instance is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureSRTMotionInstanceNV {\n    VkSRTDataNV                   transformT0;\n    VkSRTDataNV                   transformT1;\n    uint32_t                      instanceCustomIndex:24;\n    uint32_t                      mask:8;\n    uint32_t                      instanceShaderBindingTableRecordOffset:24;\n    VkGeometryInstanceFlagsKHR    flags:8;\n    uint64_t                      accelerationStructureReference;\n} VkAccelerationStructureSRTMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::transform_t0`] is a [`crate::vk::SRTDataNV`] structure describing a\n  transformation to be applied to the acceleration structure at time 0.\n\n* [`Self::transform_t1`] is a [`crate::vk::SRTDataNV`] structure describing a\n  transformation to be applied to the acceleration structure at time 1.\n\n* [`Self::instance_custom_index_and_mask`] is a 24-bit user-specified index value\n  accessible to ray shaders in the `InstanceCustomIndexKHR` built-in.\n\n* `mask` is an 8-bit visibility mask for the geometry.\n  The instance **may** only be hit if `rayMask & instance.mask != 0`\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] is a 24-bit offset used in\n  calculating the hit shader binding table index.\n\n* `flags` is an 8-bit mask of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html)values to apply to this instance.\n\n* [`Self::acceleration_structure_reference`] is either:\n\n  * a device address containing the value obtained from[`crate::vk::DeviceLoader::get_acceleration_structure_device_address_khr`]or[`crate::vk::DeviceLoader::get_acceleration_structure_handle_nv`] (used by device operations which reference acceleration structures) or,\n\n  * a [`crate::vk::AccelerationStructureKHR`] object (used by host operations\n    which reference acceleration structures).\n[](#_description)Description\n----------\n\nThe C language specification does not define the ordering of bit-fields, but\nin practice, this struct produces the correct layout with existing\ncompilers.\nThe intended bit pattern is for the following:\n\n* [`Self::instance_custom_index_and_mask`] and `mask` occupy the same memory as if a\n  single `uint32_t` was specified in their place\n\n  * [`Self::instance_custom_index_and_mask`] occupies the 24 least significant bits of\n    that memory\n\n  * `mask` occupies the 8 most significant bits of that memory\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] and `flags` occupy the\n  same memory as if a single `uint32_t` was specified in their place\n\n  * [`Self::instance_shader_binding_table_record_offset_and_flags`] occupies the 24 least\n    significant bits of that memory\n\n  * `flags` occupies the 8 most significant bits of that memory\n\nIf a compiler produces code that diverges from that pattern, applications**must** employ another method to set values according to the correct bit\npattern.\n\nThe transform for a SRT motion instance at a point in time is derived from\ncomponent-wise linear interpolation of the two SRT transforms.\nThat is, for a `time` in [0,1] the resulting transform is\n\n[`Self::transform_t0`] × (1 - `time`) + [`Self::transform_t1`] × `time`\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureSRTMotionInstanceNV-flags-parameter  \n  `flags` **must** be a valid combination of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::GeometryInstanceFlagBitsKHR`], [`crate::vk::SRTDataNV`]\n"]
#[repr(transparent)]
pub struct AccelerationStructureSRTMotionInstanceNVBuilder<'a>(AccelerationStructureSRTMotionInstanceNV, std::marker::PhantomData<&'a ()>);
impl<'a> AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
        AccelerationStructureSRTMotionInstanceNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn transform_t0(mut self, transform_t0: crate::extensions::nv_ray_tracing_motion_blur::SRTDataNV) -> Self {
        self.0.transform_t0 = transform_t0 as _;
        self
    }
    #[inline]
    pub fn transform_t1(mut self, transform_t1: crate::extensions::nv_ray_tracing_motion_blur::SRTDataNV) -> Self {
        self.0.transform_t1 = transform_t1 as _;
        self
    }
    #[inline]
    pub fn instance_custom_index(mut self, instance_custom_index: u32) -> Self {
        self.0.instance_custom_index_and_mask = crate::bits_copy!(self.0.instance_custom_index_and_mask, instance_custom_index, 0usize, 23usize);
        self
    }
    #[inline]
    pub fn mask(mut self, mask: u32) -> Self {
        self.0.instance_custom_index_and_mask = crate::bits_copy!(self.0.instance_custom_index_and_mask, mask, 24usize, 31usize);
        self
    }
    #[inline]
    pub fn instance_shader_binding_table_record_offset(mut self, instance_shader_binding_table_record_offset: u32) -> Self {
        self.0.instance_shader_binding_table_record_offset_and_flags = crate::bits_copy!(self.0.instance_shader_binding_table_record_offset_and_flags, instance_shader_binding_table_record_offset, 0usize, 23usize);
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_acceleration_structure::GeometryInstanceFlagsKHR) -> Self {
        self.0.instance_shader_binding_table_record_offset_and_flags = crate::bits_copy!(self.0.instance_shader_binding_table_record_offset_and_flags, flags.bits(), 24usize, 31usize);
        self
    }
    #[inline]
    pub fn acceleration_structure_reference(mut self, acceleration_structure_reference: u64) -> Self {
        self.0.acceleration_structure_reference = acceleration_structure_reference as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureSRTMotionInstanceNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
    fn default() -> AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
    type Target = AccelerationStructureSRTMotionInstanceNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureSRTMotionInstanceNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMatrixMotionInstanceNV.html)) · Structure <br/> VkAccelerationStructureMatrixMotionInstanceNV - Structure specifying a single acceleration structure matrix motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure matrix motion instance is defined by the\nstructure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMatrixMotionInstanceNV {\n    VkTransformMatrixKHR          transformT0;\n    VkTransformMatrixKHR          transformT1;\n    uint32_t                      instanceCustomIndex:24;\n    uint32_t                      mask:8;\n    uint32_t                      instanceShaderBindingTableRecordOffset:24;\n    VkGeometryInstanceFlagsKHR    flags:8;\n    uint64_t                      accelerationStructureReference;\n} VkAccelerationStructureMatrixMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::transform_t0`] is a [`crate::vk::TransformMatrixKHR`] structure describing a\n  transformation to be applied to the acceleration structure at time 0.\n\n* [`Self::transform_t1`] is a [`crate::vk::TransformMatrixKHR`] structure describing a\n  transformation to be applied to the acceleration structure at time 1.\n\n* [`Self::instance_custom_index_and_mask`] is a 24-bit user-specified index value\n  accessible to ray shaders in the `InstanceCustomIndexKHR` built-in.\n\n* `mask` is an 8-bit visibility mask for the geometry.\n  The instance **may** only be hit if `rayMask & instance.mask != 0`\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] is a 24-bit offset used in\n  calculating the hit shader binding table index.\n\n* `flags` is an 8-bit mask of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html)values to apply to this instance.\n\n* [`Self::acceleration_structure_reference`] is either:\n\n  * a device address containing the value obtained from[`crate::vk::DeviceLoader::get_acceleration_structure_device_address_khr`]or[`crate::vk::DeviceLoader::get_acceleration_structure_handle_nv`] (used by device operations which reference acceleration structures) or,\n\n  * a [`crate::vk::AccelerationStructureKHR`] object (used by host operations\n    which reference acceleration structures).\n[](#_description)Description\n----------\n\nThe C language specification does not define the ordering of bit-fields, but\nin practice, this struct produces the correct layout with existing\ncompilers.\nThe intended bit pattern is for the following:\n\n* [`Self::instance_custom_index_and_mask`] and `mask` occupy the same memory as if a\n  single `uint32_t` was specified in their place\n\n  * [`Self::instance_custom_index_and_mask`] occupies the 24 least significant bits of\n    that memory\n\n  * `mask` occupies the 8 most significant bits of that memory\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] and `flags` occupy the\n  same memory as if a single `uint32_t` was specified in their place\n\n  * [`Self::instance_shader_binding_table_record_offset_and_flags`] occupies the 24 least\n    significant bits of that memory\n\n  * `flags` occupies the 8 most significant bits of that memory\n\nIf a compiler produces code that diverges from that pattern, applications**must** employ another method to set values according to the correct bit\npattern.\n\nThe transform for a matrix motion instance at a point in time is derived by\ncomponent-wise linear interpolation of the two transforms.\nThat is, for a `time` in [0,1] the resulting transform is\n\n[`Self::transform_t0`] × (1 - `time`) + [`Self::transform_t1`] × `time`\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMatrixMotionInstanceNV-flags-parameter  \n  `flags` **must** be a valid combination of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::GeometryInstanceFlagBitsKHR`], [`crate::vk::TransformMatrixKHR`]\n"]
#[doc(alias = "VkAccelerationStructureMatrixMotionInstanceNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureMatrixMotionInstanceNV {
    pub transform_t0: crate::extensions::khr_acceleration_structure::TransformMatrixKHR,
    pub transform_t1: crate::extensions::khr_acceleration_structure::TransformMatrixKHR,
    pub instance_custom_index_and_mask: u32,
    pub instance_shader_binding_table_record_offset_and_flags: u32,
    pub acceleration_structure_reference: u64,
}
impl Default for AccelerationStructureMatrixMotionInstanceNV {
    fn default() -> Self {
        Self { transform_t0: Default::default(), transform_t1: Default::default(), instance_custom_index_and_mask: Default::default(), instance_shader_binding_table_record_offset_and_flags: Default::default(), acceleration_structure_reference: Default::default() }
    }
}
impl std::fmt::Debug for AccelerationStructureMatrixMotionInstanceNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureMatrixMotionInstanceNV").field("transform_t0", &self.transform_t0).field("transform_t1", &self.transform_t1).field("instance_custom_index_and_mask", &format!("{:#b}", &self.instance_custom_index_and_mask)).field("instance_shader_binding_table_record_offset_and_flags", &format!("{:#b}", &self.instance_shader_binding_table_record_offset_and_flags)).field("acceleration_structure_reference", &self.acceleration_structure_reference).finish()
    }
}
impl AccelerationStructureMatrixMotionInstanceNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
        AccelerationStructureMatrixMotionInstanceNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMatrixMotionInstanceNV.html)) · Builder of [`AccelerationStructureMatrixMotionInstanceNV`] <br/> VkAccelerationStructureMatrixMotionInstanceNV - Structure specifying a single acceleration structure matrix motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nAn acceleration structure matrix motion instance is defined by the\nstructure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMatrixMotionInstanceNV {\n    VkTransformMatrixKHR          transformT0;\n    VkTransformMatrixKHR          transformT1;\n    uint32_t                      instanceCustomIndex:24;\n    uint32_t                      mask:8;\n    uint32_t                      instanceShaderBindingTableRecordOffset:24;\n    VkGeometryInstanceFlagsKHR    flags:8;\n    uint64_t                      accelerationStructureReference;\n} VkAccelerationStructureMatrixMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::transform_t0`] is a [`crate::vk::TransformMatrixKHR`] structure describing a\n  transformation to be applied to the acceleration structure at time 0.\n\n* [`Self::transform_t1`] is a [`crate::vk::TransformMatrixKHR`] structure describing a\n  transformation to be applied to the acceleration structure at time 1.\n\n* [`Self::instance_custom_index_and_mask`] is a 24-bit user-specified index value\n  accessible to ray shaders in the `InstanceCustomIndexKHR` built-in.\n\n* `mask` is an 8-bit visibility mask for the geometry.\n  The instance **may** only be hit if `rayMask & instance.mask != 0`\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] is a 24-bit offset used in\n  calculating the hit shader binding table index.\n\n* `flags` is an 8-bit mask of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html)values to apply to this instance.\n\n* [`Self::acceleration_structure_reference`] is either:\n\n  * a device address containing the value obtained from[`crate::vk::DeviceLoader::get_acceleration_structure_device_address_khr`]or[`crate::vk::DeviceLoader::get_acceleration_structure_handle_nv`] (used by device operations which reference acceleration structures) or,\n\n  * a [`crate::vk::AccelerationStructureKHR`] object (used by host operations\n    which reference acceleration structures).\n[](#_description)Description\n----------\n\nThe C language specification does not define the ordering of bit-fields, but\nin practice, this struct produces the correct layout with existing\ncompilers.\nThe intended bit pattern is for the following:\n\n* [`Self::instance_custom_index_and_mask`] and `mask` occupy the same memory as if a\n  single `uint32_t` was specified in their place\n\n  * [`Self::instance_custom_index_and_mask`] occupies the 24 least significant bits of\n    that memory\n\n  * `mask` occupies the 8 most significant bits of that memory\n\n* [`Self::instance_shader_binding_table_record_offset_and_flags`] and `flags` occupy the\n  same memory as if a single `uint32_t` was specified in their place\n\n  * [`Self::instance_shader_binding_table_record_offset_and_flags`] occupies the 24 least\n    significant bits of that memory\n\n  * `flags` occupies the 8 most significant bits of that memory\n\nIf a compiler produces code that diverges from that pattern, applications**must** employ another method to set values according to the correct bit\npattern.\n\nThe transform for a matrix motion instance at a point in time is derived by\ncomponent-wise linear interpolation of the two transforms.\nThat is, for a `time` in [0,1] the resulting transform is\n\n[`Self::transform_t0`] × (1 - `time`) + [`Self::transform_t1`] × `time`\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMatrixMotionInstanceNV-flags-parameter  \n  `flags` **must** be a valid combination of [VkGeometryInstanceFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeometryInstanceFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::GeometryInstanceFlagBitsKHR`], [`crate::vk::TransformMatrixKHR`]\n"]
#[repr(transparent)]
pub struct AccelerationStructureMatrixMotionInstanceNVBuilder<'a>(AccelerationStructureMatrixMotionInstanceNV, std::marker::PhantomData<&'a ()>);
impl<'a> AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
        AccelerationStructureMatrixMotionInstanceNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn transform_t0(mut self, transform_t0: crate::extensions::khr_acceleration_structure::TransformMatrixKHR) -> Self {
        self.0.transform_t0 = transform_t0 as _;
        self
    }
    #[inline]
    pub fn transform_t1(mut self, transform_t1: crate::extensions::khr_acceleration_structure::TransformMatrixKHR) -> Self {
        self.0.transform_t1 = transform_t1 as _;
        self
    }
    #[inline]
    pub fn instance_custom_index(mut self, instance_custom_index: u32) -> Self {
        self.0.instance_custom_index_and_mask = crate::bits_copy!(self.0.instance_custom_index_and_mask, instance_custom_index, 0usize, 23usize);
        self
    }
    #[inline]
    pub fn mask(mut self, mask: u32) -> Self {
        self.0.instance_custom_index_and_mask = crate::bits_copy!(self.0.instance_custom_index_and_mask, mask, 24usize, 31usize);
        self
    }
    #[inline]
    pub fn instance_shader_binding_table_record_offset(mut self, instance_shader_binding_table_record_offset: u32) -> Self {
        self.0.instance_shader_binding_table_record_offset_and_flags = crate::bits_copy!(self.0.instance_shader_binding_table_record_offset_and_flags, instance_shader_binding_table_record_offset, 0usize, 23usize);
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_acceleration_structure::GeometryInstanceFlagsKHR) -> Self {
        self.0.instance_shader_binding_table_record_offset_and_flags = crate::bits_copy!(self.0.instance_shader_binding_table_record_offset_and_flags, flags.bits(), 24usize, 31usize);
        self
    }
    #[inline]
    pub fn acceleration_structure_reference(mut self, acceleration_structure_reference: u64) -> Self {
        self.0.acceleration_structure_reference = acceleration_structure_reference as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureMatrixMotionInstanceNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
    fn default() -> AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
    type Target = AccelerationStructureMatrixMotionInstanceNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureMatrixMotionInstanceNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInstanceDataNV.html)) · Structure <br/> VkAccelerationStructureMotionInstanceDataNV - Union specifying a acceleration structure motion instance data for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\nAcceleration structure motion instance is defined by the union:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef union VkAccelerationStructureMotionInstanceDataNV {\n    VkAccelerationStructureInstanceKHR               staticInstance;\n    VkAccelerationStructureMatrixMotionInstanceNV    matrixMotionInstance;\n    VkAccelerationStructureSRTMotionInstanceNV       srtMotionInstance;\n} VkAccelerationStructureMotionInstanceDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::static_instance`] is a [`crate::vk::AccelerationStructureInstanceKHR`]structure containing data for a static instance.\n\n* [`Self::matrix_motion_instance`] is a[`crate::vk::AccelerationStructureMatrixMotionInstanceNV`] structure containing\n  data for a matrix motion instance.\n\n* [`Self::srt_motion_instance`] is a[`crate::vk::AccelerationStructureSRTMotionInstanceNV`] structure containing\n  data for an SRT motion instance.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureInstanceKHR`], [`crate::vk::AccelerationStructureMatrixMotionInstanceNV`], [`crate::vk::AccelerationStructureMotionInstanceNV`], [`crate::vk::AccelerationStructureSRTMotionInstanceNV`]\n"]
#[doc(alias = "VkAccelerationStructureMotionInstanceDataNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub union AccelerationStructureMotionInstanceDataNV {
    pub static_instance: crate::extensions::khr_acceleration_structure::AccelerationStructureInstanceKHR,
    pub matrix_motion_instance: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMatrixMotionInstanceNV,
    pub srt_motion_instance: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureSRTMotionInstanceNV,
}
impl Default for AccelerationStructureMotionInstanceDataNV {
    fn default() -> Self {
        unsafe { std::mem::zeroed() }
    }
}
impl std::fmt::Debug for AccelerationStructureMotionInstanceDataNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureMotionInstanceDataNV").finish()
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInstanceNV.html)) · Structure <br/> VkAccelerationStructureMotionInstanceNV - Structure specifying a single acceleration structure motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\n*Acceleration structure motion instances* **can** be built into top-level\nacceleration structures.\nEach acceleration structure instance is a separate entry in the top-level\nacceleration structure which includes all the geometry of a bottom-level\nacceleration structure at a transformed location including a type of motion\nand parameters to determine the motion of the instance over time.\n\nAn acceleration structure motion instance is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMotionInstanceNV {\n    VkAccelerationStructureMotionInstanceTypeNV     type;\n    VkAccelerationStructureMotionInstanceFlagsNV    flags;\n    VkAccelerationStructureMotionInstanceDataNV     data;\n} VkAccelerationStructureMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is a [`crate::vk::AccelerationStructureMotionInstanceTypeNV`]enumerant identifying which type of motion instance this is and which\n  type of the union is valid.\n\n* [`Self::flags`] is currently unused, but is required to keep natural\n  alignment of [`Self::data`].\n\n* [`Self::data`] is a [`crate::vk::AccelerationStructureMotionInstanceDataNV`]containing motion instance data for this instance.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>If writing this other than with a standard C compiler, note that the final<br/>structure should be 152 bytes in size.|\n|---|-----------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::AccelerationStructureMotionInstanceTypeNV`] value\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-staticInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::STATIC_NV`], the `staticInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureInstanceKHR`] structure\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-matrixMotionInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::MATRIX_MOTION_NV`], the `matrixMotionInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureMatrixMotionInstanceNV`] structure\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-srtMotionInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::SRT_MOTION_NV`], the `srtMotionInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureSRTMotionInstanceNV`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::AccelerationStructureMotionInstanceFlagBitsNV`], [`crate::vk::AccelerationStructureMotionInstanceTypeNV`]\n"]
#[doc(alias = "VkAccelerationStructureMotionInstanceNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AccelerationStructureMotionInstanceNV {
    pub _type: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceTypeNV,
    pub flags: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceFlagsNV,
    pub data: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceDataNV,
}
impl Default for AccelerationStructureMotionInstanceNV {
    fn default() -> Self {
        Self { _type: Default::default(), flags: Default::default(), data: Default::default() }
    }
}
impl std::fmt::Debug for AccelerationStructureMotionInstanceNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AccelerationStructureMotionInstanceNV").field("_type", &self._type).field("flags", &self.flags).field("data", &self.data).finish()
    }
}
impl AccelerationStructureMotionInstanceNV {
    #[inline]
    pub fn into_builder<'a>(self) -> AccelerationStructureMotionInstanceNVBuilder<'a> {
        AccelerationStructureMotionInstanceNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAccelerationStructureMotionInstanceNV.html)) · Builder of [`AccelerationStructureMotionInstanceNV`] <br/> VkAccelerationStructureMotionInstanceNV - Structure specifying a single acceleration structure motion instance for building into an acceleration structure geometry\n[](#_c_specification)C Specification\n----------\n\n*Acceleration structure motion instances* **can** be built into top-level\nacceleration structures.\nEach acceleration structure instance is a separate entry in the top-level\nacceleration structure which includes all the geometry of a bottom-level\nacceleration structure at a transformed location including a type of motion\nand parameters to determine the motion of the instance over time.\n\nAn acceleration structure motion instance is defined by the structure:\n\n```\n// Provided by VK_NV_ray_tracing_motion_blur\ntypedef struct VkAccelerationStructureMotionInstanceNV {\n    VkAccelerationStructureMotionInstanceTypeNV     type;\n    VkAccelerationStructureMotionInstanceFlagsNV    flags;\n    VkAccelerationStructureMotionInstanceDataNV     data;\n} VkAccelerationStructureMotionInstanceNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is a [`crate::vk::AccelerationStructureMotionInstanceTypeNV`]enumerant identifying which type of motion instance this is and which\n  type of the union is valid.\n\n* [`Self::flags`] is currently unused, but is required to keep natural\n  alignment of [`Self::data`].\n\n* [`Self::data`] is a [`crate::vk::AccelerationStructureMotionInstanceDataNV`]containing motion instance data for this instance.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>If writing this other than with a standard C compiler, note that the final<br/>structure should be 152 bytes in size.|\n|---|-----------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::AccelerationStructureMotionInstanceTypeNV`] value\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-staticInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::STATIC_NV`], the `staticInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureInstanceKHR`] structure\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-matrixMotionInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::MATRIX_MOTION_NV`], the `matrixMotionInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureMatrixMotionInstanceNV`] structure\n\n* []() VUID-VkAccelerationStructureMotionInstanceNV-srtMotionInstance-parameter  \n   If [`Self::_type`] is [`crate::vk::AccelerationStructureMotionInstanceTypeNV::SRT_MOTION_NV`], the `srtMotionInstance` member of [`Self::data`] **must** be a valid [`crate::vk::AccelerationStructureSRTMotionInstanceNV`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AccelerationStructureMotionInstanceDataNV`], [`crate::vk::AccelerationStructureMotionInstanceFlagBitsNV`], [`crate::vk::AccelerationStructureMotionInstanceTypeNV`]\n"]
#[repr(transparent)]
pub struct AccelerationStructureMotionInstanceNVBuilder<'a>(AccelerationStructureMotionInstanceNV, std::marker::PhantomData<&'a ()>);
impl<'a> AccelerationStructureMotionInstanceNVBuilder<'a> {
    #[inline]
    pub fn new() -> AccelerationStructureMotionInstanceNVBuilder<'a> {
        AccelerationStructureMotionInstanceNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(mut self, _type: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceTypeNV) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn data(mut self, data: crate::extensions::nv_ray_tracing_motion_blur::AccelerationStructureMotionInstanceDataNV) -> Self {
        self.0.data = data as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AccelerationStructureMotionInstanceNV {
        self.0
    }
}
impl<'a> std::default::Default for AccelerationStructureMotionInstanceNVBuilder<'a> {
    fn default() -> AccelerationStructureMotionInstanceNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AccelerationStructureMotionInstanceNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AccelerationStructureMotionInstanceNVBuilder<'a> {
    type Target = AccelerationStructureMotionInstanceNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AccelerationStructureMotionInstanceNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
