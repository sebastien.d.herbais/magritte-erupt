#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_QCOM_RENDER_PASS_TRANSFORM_SPEC_VERSION")]
pub const QCOM_RENDER_PASS_TRANSFORM_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_QCOM_RENDER_PASS_TRANSFORM_EXTENSION_NAME")]
pub const QCOM_RENDER_PASS_TRANSFORM_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_QCOM_render_pass_transform");
#[doc = "Provided by [`crate::extensions::qcom_render_pass_transform`]"]
impl crate::vk1_0::RenderPassCreateFlagBits {
    pub const TRANSFORM_QCOM: Self = Self(2);
}
#[doc = "Provided by [`crate::extensions::qcom_render_pass_transform`]"]
impl crate::vk1_0::StructureType {
    pub const COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM: Self = Self(1000282000);
    pub const RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM: Self = Self(1000282001);
}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceRenderPassTransformInfoQCOM> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'_>> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, RenderPassTransformBeginInfoQCOM> for crate::vk1_0::RenderPassBeginInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, RenderPassTransformBeginInfoQCOMBuilder<'_>> for crate::vk1_0::RenderPassBeginInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassTransformBeginInfoQCOM.html)) · Structure <br/> VkRenderPassTransformBeginInfoQCOM - Structure describing transform parameters of a render pass instance\n[](#_c_specification)C Specification\n----------\n\nTo begin a renderpass instance with [render pass transform](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vertexpostproc-renderpass-transform) enabled, add the[`crate::vk::RenderPassTransformBeginInfoQCOM`] to the [`Self::p_next`] chain of[`crate::vk::RenderPassBeginInfo`] structure passed to the[`crate::vk::PFN_vkCmdBeginRenderPass`] command specifying the renderpass transform.\n\nThe [`crate::vk::RenderPassTransformBeginInfoQCOM`] structure is defined as:\n\n```\n// Provided by VK_QCOM_render_pass_transform\ntypedef struct VkRenderPassTransformBeginInfoQCOM {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkSurfaceTransformFlagBitsKHR    transform;\n} VkRenderPassTransformBeginInfoQCOM;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  describing the transform to be applied to rasterization.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-transform-02871  \n  [`Self::transform`] **must** be [`crate::vk::SurfaceTransformFlagBitsKHR::IDENTITY_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_90_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_180_KHR`], or[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_270_KHR`]\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-flags-02872  \n   The `renderpass` **must** have been created with[`crate::vk::RenderPassCreateInfo::flags`] containing[`crate::vk::RenderPassCreateFlagBits::TRANSFORM_QCOM`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html)\n"]
#[doc(alias = "VkRenderPassTransformBeginInfoQCOM")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RenderPassTransformBeginInfoQCOM {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR,
}
impl RenderPassTransformBeginInfoQCOM {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM;
}
impl Default for RenderPassTransformBeginInfoQCOM {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), transform: Default::default() }
    }
}
impl std::fmt::Debug for RenderPassTransformBeginInfoQCOM {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RenderPassTransformBeginInfoQCOM").field("s_type", &self.s_type).field("p_next", &self.p_next).field("transform", &self.transform).finish()
    }
}
impl RenderPassTransformBeginInfoQCOM {
    #[inline]
    pub fn into_builder<'a>(self) -> RenderPassTransformBeginInfoQCOMBuilder<'a> {
        RenderPassTransformBeginInfoQCOMBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassTransformBeginInfoQCOM.html)) · Builder of [`RenderPassTransformBeginInfoQCOM`] <br/> VkRenderPassTransformBeginInfoQCOM - Structure describing transform parameters of a render pass instance\n[](#_c_specification)C Specification\n----------\n\nTo begin a renderpass instance with [render pass transform](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vertexpostproc-renderpass-transform) enabled, add the[`crate::vk::RenderPassTransformBeginInfoQCOM`] to the [`Self::p_next`] chain of[`crate::vk::RenderPassBeginInfo`] structure passed to the[`crate::vk::PFN_vkCmdBeginRenderPass`] command specifying the renderpass transform.\n\nThe [`crate::vk::RenderPassTransformBeginInfoQCOM`] structure is defined as:\n\n```\n// Provided by VK_QCOM_render_pass_transform\ntypedef struct VkRenderPassTransformBeginInfoQCOM {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkSurfaceTransformFlagBitsKHR    transform;\n} VkRenderPassTransformBeginInfoQCOM;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  describing the transform to be applied to rasterization.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-transform-02871  \n  [`Self::transform`] **must** be [`crate::vk::SurfaceTransformFlagBitsKHR::IDENTITY_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_90_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_180_KHR`], or[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_270_KHR`]\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-flags-02872  \n   The `renderpass` **must** have been created with[`crate::vk::RenderPassCreateInfo::flags`] containing[`crate::vk::RenderPassCreateFlagBits::TRANSFORM_QCOM`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkRenderPassTransformBeginInfoQCOM-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html)\n"]
#[repr(transparent)]
pub struct RenderPassTransformBeginInfoQCOMBuilder<'a>(RenderPassTransformBeginInfoQCOM, std::marker::PhantomData<&'a ()>);
impl<'a> RenderPassTransformBeginInfoQCOMBuilder<'a> {
    #[inline]
    pub fn new() -> RenderPassTransformBeginInfoQCOMBuilder<'a> {
        RenderPassTransformBeginInfoQCOMBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn transform(mut self, transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR) -> Self {
        self.0.transform = transform as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RenderPassTransformBeginInfoQCOM {
        self.0
    }
}
impl<'a> std::default::Default for RenderPassTransformBeginInfoQCOMBuilder<'a> {
    fn default() -> RenderPassTransformBeginInfoQCOMBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RenderPassTransformBeginInfoQCOMBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RenderPassTransformBeginInfoQCOMBuilder<'a> {
    type Target = RenderPassTransformBeginInfoQCOM;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RenderPassTransformBeginInfoQCOMBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceRenderPassTransformInfoQCOM.html)) · Structure <br/> VkCommandBufferInheritanceRenderPassTransformInfoQCOM - Structure describing transformed render pass parameters command buffer\n[](#_c_specification)C Specification\n----------\n\nTo begin recording a secondary command buffer compatible with execution\ninside a render pass using [render\npass transform](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vertexpostproc-renderpass-transform), add the[`crate::vk::CommandBufferInheritanceRenderPassTransformInfoQCOM`] to the[`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] structure passed\nto the [`crate::vk::PFN_vkBeginCommandBuffer`] command specifying the parameters for\ntransformed rasterization.\n\nThe [`crate::vk::CommandBufferInheritanceRenderPassTransformInfoQCOM`] structure is\ndefined as:\n\n```\n// Provided by VK_QCOM_render_pass_transform\ntypedef struct VkCommandBufferInheritanceRenderPassTransformInfoQCOM {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkSurfaceTransformFlagBitsKHR    transform;\n    VkRect2D                         renderArea;\n} VkCommandBufferInheritanceRenderPassTransformInfoQCOM;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  describing the transform to be applied to the render pass.\n\n* [`Self::render_area`] is the render area that is affected by the command\n  buffer.\n[](#_description)Description\n----------\n\nWhen the secondary is recorded to execute within a render pass instance\nusing [`crate::vk::PFN_vkCmdExecuteCommands`], the render pass transform parameters of\nthe secondary command buffer **must** be consistent with the render pass\ntransform parameters specified for the render pass instance.\nIn particular, the [`Self::transform`] and [`Self::render_area`] for command buffer**must** be identical to the [`Self::transform`] and [`Self::render_area`] of the render\npass instance.\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceRenderPassTransformInfoQCOM-transform-02864  \n  [`Self::transform`] **must** be [`crate::vk::SurfaceTransformFlagBitsKHR::IDENTITY_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_90_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_180_KHR`], or[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_270_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceRenderPassTransformInfoQCOM-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Rect2D`], [`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html)\n"]
#[doc(alias = "VkCommandBufferInheritanceRenderPassTransformInfoQCOM")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CommandBufferInheritanceRenderPassTransformInfoQCOM {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR,
    pub render_area: crate::vk1_0::Rect2D,
}
impl CommandBufferInheritanceRenderPassTransformInfoQCOM {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM;
}
impl Default for CommandBufferInheritanceRenderPassTransformInfoQCOM {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), transform: Default::default(), render_area: Default::default() }
    }
}
impl std::fmt::Debug for CommandBufferInheritanceRenderPassTransformInfoQCOM {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CommandBufferInheritanceRenderPassTransformInfoQCOM").field("s_type", &self.s_type).field("p_next", &self.p_next).field("transform", &self.transform).field("render_area", &self.render_area).finish()
    }
}
impl CommandBufferInheritanceRenderPassTransformInfoQCOM {
    #[inline]
    pub fn into_builder<'a>(self) -> CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
        CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceRenderPassTransformInfoQCOM.html)) · Builder of [`CommandBufferInheritanceRenderPassTransformInfoQCOM`] <br/> VkCommandBufferInheritanceRenderPassTransformInfoQCOM - Structure describing transformed render pass parameters command buffer\n[](#_c_specification)C Specification\n----------\n\nTo begin recording a secondary command buffer compatible with execution\ninside a render pass using [render\npass transform](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vertexpostproc-renderpass-transform), add the[`crate::vk::CommandBufferInheritanceRenderPassTransformInfoQCOM`] to the[`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] structure passed\nto the [`crate::vk::PFN_vkBeginCommandBuffer`] command specifying the parameters for\ntransformed rasterization.\n\nThe [`crate::vk::CommandBufferInheritanceRenderPassTransformInfoQCOM`] structure is\ndefined as:\n\n```\n// Provided by VK_QCOM_render_pass_transform\ntypedef struct VkCommandBufferInheritanceRenderPassTransformInfoQCOM {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkSurfaceTransformFlagBitsKHR    transform;\n    VkRect2D                         renderArea;\n} VkCommandBufferInheritanceRenderPassTransformInfoQCOM;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  describing the transform to be applied to the render pass.\n\n* [`Self::render_area`] is the render area that is affected by the command\n  buffer.\n[](#_description)Description\n----------\n\nWhen the secondary is recorded to execute within a render pass instance\nusing [`crate::vk::PFN_vkCmdExecuteCommands`], the render pass transform parameters of\nthe secondary command buffer **must** be consistent with the render pass\ntransform parameters specified for the render pass instance.\nIn particular, the [`Self::transform`] and [`Self::render_area`] for command buffer**must** be identical to the [`Self::transform`] and [`Self::render_area`] of the render\npass instance.\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceRenderPassTransformInfoQCOM-transform-02864  \n  [`Self::transform`] **must** be [`crate::vk::SurfaceTransformFlagBitsKHR::IDENTITY_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_90_KHR`],[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_180_KHR`], or[`crate::vk::SurfaceTransformFlagBitsKHR::ROTATE_270_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceRenderPassTransformInfoQCOM-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Rect2D`], [`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html)\n"]
#[repr(transparent)]
pub struct CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a>(CommandBufferInheritanceRenderPassTransformInfoQCOM, std::marker::PhantomData<&'a ()>);
impl<'a> CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
    #[inline]
    pub fn new() -> CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
        CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn transform(mut self, transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR) -> Self {
        self.0.transform = transform as _;
        self
    }
    #[inline]
    pub fn render_area(mut self, render_area: crate::vk1_0::Rect2D) -> Self {
        self.0.render_area = render_area as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CommandBufferInheritanceRenderPassTransformInfoQCOM {
        self.0
    }
}
impl<'a> std::default::Default for CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
    fn default() -> CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
    type Target = CommandBufferInheritanceRenderPassTransformInfoQCOM;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CommandBufferInheritanceRenderPassTransformInfoQCOMBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
