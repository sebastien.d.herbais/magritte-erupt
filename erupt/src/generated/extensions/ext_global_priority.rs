#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_GLOBAL_PRIORITY_SPEC_VERSION")]
pub const EXT_GLOBAL_PRIORITY_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_GLOBAL_PRIORITY_EXTENSION_NAME")]
pub const EXT_GLOBAL_PRIORITY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_global_priority");
#[doc = "Provided by [`crate::extensions::ext_global_priority`]"]
impl crate::vk1_0::Result {
    pub const ERROR_NOT_PERMITTED_EXT: Self = Self(-1000174001);
}
#[doc = "Provided by [`crate::extensions::ext_global_priority`]"]
impl crate::vk1_0::StructureType {
    pub const DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT: Self = Self(1000174000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueGlobalPriorityEXT.html)) · Enum <br/> VkQueueGlobalPriorityEXT - Values specifying a system-wide queue priority\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT::global_priority`],\nspecifying a system-wide priority level are:\n\n```\n// Provided by VK_EXT_global_priority\ntypedef enum VkQueueGlobalPriorityEXT {\n    VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT = 128,\n    VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT = 256,\n    VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT = 512,\n    VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT = 1024,\n} VkQueueGlobalPriorityEXT;\n```\n[](#_description)Description\n----------\n\nPriority values are sorted in ascending order.\nA comparison operation on the enum values can be used to determine the\npriority order.\n\n* [`Self::LOW_EXT`] is below the system default.\n  Useful for non-interactive tasks.\n\n* [`Self::MEDIUM_EXT`] is the system default\n  priority.\n\n* [`Self::HIGH_EXT`] is above the system default.\n\n* [`Self::REALTIME_EXT`] is the highest priority.\n  Useful for critical tasks.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`], [`crate::vk::QueueFamilyGlobalPriorityPropertiesEXT`]\n"]
#[doc(alias = "VkQueueGlobalPriorityEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct QueueGlobalPriorityEXT(pub i32);
impl std::fmt::Debug for QueueGlobalPriorityEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::LOW_EXT => "LOW_EXT",
            &Self::MEDIUM_EXT => "MEDIUM_EXT",
            &Self::HIGH_EXT => "HIGH_EXT",
            &Self::REALTIME_EXT => "REALTIME_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_global_priority`]"]
impl crate::extensions::ext_global_priority::QueueGlobalPriorityEXT {
    pub const LOW_EXT: Self = Self(128);
    pub const MEDIUM_EXT: Self = Self(256);
    pub const HIGH_EXT: Self = Self(512);
    pub const REALTIME_EXT: Self = Self(1024);
}
impl<'a> crate::ExtendableFromConst<'a, DeviceQueueGlobalPriorityCreateInfoEXT> for crate::vk1_0::DeviceQueueCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'_>> for crate::vk1_0::DeviceQueueCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceQueueGlobalPriorityCreateInfoEXT.html)) · Structure <br/> VkDeviceQueueGlobalPriorityCreateInfoEXT - Specify a system wide priority\n[](#_c_specification)C Specification\n----------\n\nA queue **can** be created with a system-wide priority by adding a[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::DeviceQueueCreateInfo`].\n\nThe [`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_global_priority\ntypedef struct VkDeviceQueueGlobalPriorityCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkQueueGlobalPriorityEXT    globalPriority;\n} VkDeviceQueueGlobalPriorityCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::global_priority`] is the system-wide priority associated to this\n  queue as specified by [`crate::vk::QueueGlobalPriorityEXT`]\n[](#_description)Description\n----------\n\nA queue created without specifying[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] will default to[`crate::vk::QueueGlobalPriorityEXT::MEDIUM_EXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceQueueGlobalPriorityCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT`]\n\n* []() VUID-VkDeviceQueueGlobalPriorityCreateInfoEXT-globalPriority-parameter  \n  [`Self::global_priority`] **must** be a valid [`crate::vk::QueueGlobalPriorityEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueueGlobalPriorityEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDeviceQueueGlobalPriorityCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DeviceQueueGlobalPriorityCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub global_priority: crate::extensions::ext_global_priority::QueueGlobalPriorityEXT,
}
impl DeviceQueueGlobalPriorityCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT;
}
impl Default for DeviceQueueGlobalPriorityCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), global_priority: Default::default() }
    }
}
impl std::fmt::Debug for DeviceQueueGlobalPriorityCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DeviceQueueGlobalPriorityCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("global_priority", &self.global_priority).finish()
    }
}
impl DeviceQueueGlobalPriorityCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
        DeviceQueueGlobalPriorityCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceQueueGlobalPriorityCreateInfoEXT.html)) · Builder of [`DeviceQueueGlobalPriorityCreateInfoEXT`] <br/> VkDeviceQueueGlobalPriorityCreateInfoEXT - Specify a system wide priority\n[](#_c_specification)C Specification\n----------\n\nA queue **can** be created with a system-wide priority by adding a[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::DeviceQueueCreateInfo`].\n\nThe [`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_global_priority\ntypedef struct VkDeviceQueueGlobalPriorityCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkQueueGlobalPriorityEXT    globalPriority;\n} VkDeviceQueueGlobalPriorityCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::global_priority`] is the system-wide priority associated to this\n  queue as specified by [`crate::vk::QueueGlobalPriorityEXT`]\n[](#_description)Description\n----------\n\nA queue created without specifying[`crate::vk::DeviceQueueGlobalPriorityCreateInfoEXT`] will default to[`crate::vk::QueueGlobalPriorityEXT::MEDIUM_EXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceQueueGlobalPriorityCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT`]\n\n* []() VUID-VkDeviceQueueGlobalPriorityCreateInfoEXT-globalPriority-parameter  \n  [`Self::global_priority`] **must** be a valid [`crate::vk::QueueGlobalPriorityEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueueGlobalPriorityEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a>(DeviceQueueGlobalPriorityCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
        DeviceQueueGlobalPriorityCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn global_priority(mut self, global_priority: crate::extensions::ext_global_priority::QueueGlobalPriorityEXT) -> Self {
        self.0.global_priority = global_priority as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DeviceQueueGlobalPriorityCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
    fn default() -> DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
    type Target = DeviceQueueGlobalPriorityCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DeviceQueueGlobalPriorityCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
