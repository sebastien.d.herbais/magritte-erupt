#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_MEMORY_PRIORITY_SPEC_VERSION")]
pub const EXT_MEMORY_PRIORITY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_MEMORY_PRIORITY_EXTENSION_NAME")]
pub const EXT_MEMORY_PRIORITY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_memory_priority");
#[doc = "Provided by [`crate::extensions::ext_memory_priority`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT: Self = Self(1000238000);
    pub const MEMORY_PRIORITY_ALLOCATE_INFO_EXT: Self = Self(1000238001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceMemoryPriorityFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MemoryPriorityAllocateInfoEXT> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MemoryPriorityAllocateInfoEXTBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMemoryPriorityFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryPriorityFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceMemoryPriorityFeaturesEXT - Structure describing memory priority features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_priority\ntypedef struct VkPhysicalDeviceMemoryPriorityFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           memoryPriority;\n} VkPhysicalDeviceMemoryPriorityFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::memory_priority`] indicates that the\n  implementation supports memory priorities specified at memory allocation\n  time via [`crate::vk::MemoryPriorityAllocateInfoEXT`].\n\nIf the [`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMemoryPriorityFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceMemoryPriorityFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceMemoryPriorityFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub memory_priority: crate::vk1_0::Bool32,
}
impl PhysicalDeviceMemoryPriorityFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT;
}
impl Default for PhysicalDeviceMemoryPriorityFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), memory_priority: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceMemoryPriorityFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceMemoryPriorityFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_priority", &(self.memory_priority != 0)).finish()
    }
}
impl PhysicalDeviceMemoryPriorityFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
        PhysicalDeviceMemoryPriorityFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryPriorityFeaturesEXT.html)) · Builder of [`PhysicalDeviceMemoryPriorityFeaturesEXT`] <br/> VkPhysicalDeviceMemoryPriorityFeaturesEXT - Structure describing memory priority features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_priority\ntypedef struct VkPhysicalDeviceMemoryPriorityFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           memoryPriority;\n} VkPhysicalDeviceMemoryPriorityFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::memory_priority`] indicates that the\n  implementation supports memory priorities specified at memory allocation\n  time via [`crate::vk::MemoryPriorityAllocateInfoEXT`].\n\nIf the [`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceMemoryPriorityFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMemoryPriorityFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a>(PhysicalDeviceMemoryPriorityFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
        PhysicalDeviceMemoryPriorityFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_priority(mut self, memory_priority: bool) -> Self {
        self.0.memory_priority = memory_priority as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceMemoryPriorityFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceMemoryPriorityFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceMemoryPriorityFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryPriorityAllocateInfoEXT.html)) · Structure <br/> VkMemoryPriorityAllocateInfoEXT - Specify a memory allocation priority\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::MemoryPriorityAllocateInfoEXT`]structure, then that structure includes a priority for the memory.\n\nThe [`crate::vk::MemoryPriorityAllocateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_priority\ntypedef struct VkMemoryPriorityAllocateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    float              priority;\n} VkMemoryPriorityAllocateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::priority`] is a floating-point value between `0` and `1`, indicating\n  the priority of the allocation relative to other memory allocations.\n  Larger values are higher priority.\n  The granularity of the priorities is implementation-dependent.\n[](#_description)Description\n----------\n\nMemory allocations with higher priority **may** be more likely to stay in\ndevice-local memory when the system is under memory pressure.\n\nIf this structure is not included, it is as if the [`Self::priority`] value were`0.5`.\n\nValid Usage\n\n* []() VUID-VkMemoryPriorityAllocateInfoEXT-priority-02602  \n  [`Self::priority`] **must** be between `0` and `1`, inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryPriorityAllocateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_PRIORITY_ALLOCATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkMemoryPriorityAllocateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryPriorityAllocateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub priority: std::os::raw::c_float,
}
impl MemoryPriorityAllocateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_PRIORITY_ALLOCATE_INFO_EXT;
}
impl Default for MemoryPriorityAllocateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), priority: Default::default() }
    }
}
impl std::fmt::Debug for MemoryPriorityAllocateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryPriorityAllocateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("priority", &self.priority).finish()
    }
}
impl MemoryPriorityAllocateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryPriorityAllocateInfoEXTBuilder<'a> {
        MemoryPriorityAllocateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryPriorityAllocateInfoEXT.html)) · Builder of [`MemoryPriorityAllocateInfoEXT`] <br/> VkMemoryPriorityAllocateInfoEXT - Specify a memory allocation priority\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::MemoryPriorityAllocateInfoEXT`]structure, then that structure includes a priority for the memory.\n\nThe [`crate::vk::MemoryPriorityAllocateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_priority\ntypedef struct VkMemoryPriorityAllocateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    float              priority;\n} VkMemoryPriorityAllocateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::priority`] is a floating-point value between `0` and `1`, indicating\n  the priority of the allocation relative to other memory allocations.\n  Larger values are higher priority.\n  The granularity of the priorities is implementation-dependent.\n[](#_description)Description\n----------\n\nMemory allocations with higher priority **may** be more likely to stay in\ndevice-local memory when the system is under memory pressure.\n\nIf this structure is not included, it is as if the [`Self::priority`] value were`0.5`.\n\nValid Usage\n\n* []() VUID-VkMemoryPriorityAllocateInfoEXT-priority-02602  \n  [`Self::priority`] **must** be between `0` and `1`, inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryPriorityAllocateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_PRIORITY_ALLOCATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct MemoryPriorityAllocateInfoEXTBuilder<'a>(MemoryPriorityAllocateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryPriorityAllocateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryPriorityAllocateInfoEXTBuilder<'a> {
        MemoryPriorityAllocateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn priority(mut self, priority: std::os::raw::c_float) -> Self {
        self.0.priority = priority as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryPriorityAllocateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for MemoryPriorityAllocateInfoEXTBuilder<'a> {
    fn default() -> MemoryPriorityAllocateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryPriorityAllocateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryPriorityAllocateInfoEXTBuilder<'a> {
    type Target = MemoryPriorityAllocateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryPriorityAllocateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
