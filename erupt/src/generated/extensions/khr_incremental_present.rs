#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_INCREMENTAL_PRESENT_SPEC_VERSION")]
pub const KHR_INCREMENTAL_PRESENT_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_INCREMENTAL_PRESENT_EXTENSION_NAME")]
pub const KHR_INCREMENTAL_PRESENT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_incremental_present");
#[doc = "Provided by [`crate::extensions::khr_incremental_present`]"]
impl crate::vk1_0::StructureType {
    pub const PRESENT_REGIONS_KHR: Self = Self(1000084000);
}
impl<'a> crate::ExtendableFromConst<'a, PresentRegionsKHR> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PresentRegionsKHRBuilder<'_>> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentRegionsKHR.html)) · Structure <br/> VkPresentRegionsKHR - Structure hint of rectangular regions changed by vkQueuePresentKHR\n[](#_c_specification)C Specification\n----------\n\nWhen the `VK_KHR_incremental_present` extension is enabled, additional\nfields **can** be specified that allow an application to specify that only\ncertain rectangular regions of the presentable images of a swapchain are\nchanged.\nThis is an optimization hint that a presentation engine **may** use to only\nupdate the region of a surface that is actually changing.\nThe application still **must** ensure that all pixels of a presented image\ncontain the desired values, in case the presentation engine ignores this\nhint.\nAn application **can** provide this hint by adding a [`crate::vk::PresentRegionsKHR`]structure to the [`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentRegionsKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkPresentRegionsKHR {\n    VkStructureType              sType;\n    const void*                  pNext;\n    uint32_t                     swapchainCount;\n    const VkPresentRegionKHR*    pRegions;\n} VkPresentRegionsKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to by\n  this command.\n\n* [`Self::p_regions`] is `NULL` or a pointer to an array of[`crate::vk::PresentRegionKHR`] elements with [`Self::swapchain_count`] entries.\n  If not `NULL`, each element of [`Self::p_regions`] contains the region that\n  has changed since the last present to the swapchain in the corresponding\n  entry in the [`crate::vk::PresentInfoKHR`]::`pSwapchains` array.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentRegionsKHR-swapchainCount-01260  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where[`crate::vk::PresentInfoKHR`] is included in the [`Self::p_next`] chain of this[`crate::vk::PresentRegionsKHR`] structure\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentRegionsKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_REGIONS_KHR`]\n\n* []() VUID-VkPresentRegionsKHR-pRegions-parameter  \n   If [`Self::p_regions`] is not `NULL`, [`Self::p_regions`] **must** be a valid pointer to an array of [`Self::swapchain_count`] valid [`crate::vk::PresentRegionKHR`] structures\n\n* []() VUID-VkPresentRegionsKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentRegionKHR`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPresentRegionsKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentRegionsKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub swapchain_count: u32,
    pub p_regions: *const crate::extensions::khr_incremental_present::PresentRegionKHR,
}
impl PresentRegionsKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PRESENT_REGIONS_KHR;
}
impl Default for PresentRegionsKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), swapchain_count: Default::default(), p_regions: std::ptr::null() }
    }
}
impl std::fmt::Debug for PresentRegionsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentRegionsKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("swapchain_count", &self.swapchain_count).field("p_regions", &self.p_regions).finish()
    }
}
impl PresentRegionsKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentRegionsKHRBuilder<'a> {
        PresentRegionsKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentRegionsKHR.html)) · Builder of [`PresentRegionsKHR`] <br/> VkPresentRegionsKHR - Structure hint of rectangular regions changed by vkQueuePresentKHR\n[](#_c_specification)C Specification\n----------\n\nWhen the `VK_KHR_incremental_present` extension is enabled, additional\nfields **can** be specified that allow an application to specify that only\ncertain rectangular regions of the presentable images of a swapchain are\nchanged.\nThis is an optimization hint that a presentation engine **may** use to only\nupdate the region of a surface that is actually changing.\nThe application still **must** ensure that all pixels of a presented image\ncontain the desired values, in case the presentation engine ignores this\nhint.\nAn application **can** provide this hint by adding a [`crate::vk::PresentRegionsKHR`]structure to the [`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentRegionsKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkPresentRegionsKHR {\n    VkStructureType              sType;\n    const void*                  pNext;\n    uint32_t                     swapchainCount;\n    const VkPresentRegionKHR*    pRegions;\n} VkPresentRegionsKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to by\n  this command.\n\n* [`Self::p_regions`] is `NULL` or a pointer to an array of[`crate::vk::PresentRegionKHR`] elements with [`Self::swapchain_count`] entries.\n  If not `NULL`, each element of [`Self::p_regions`] contains the region that\n  has changed since the last present to the swapchain in the corresponding\n  entry in the [`crate::vk::PresentInfoKHR`]::`pSwapchains` array.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentRegionsKHR-swapchainCount-01260  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where[`crate::vk::PresentInfoKHR`] is included in the [`Self::p_next`] chain of this[`crate::vk::PresentRegionsKHR`] structure\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentRegionsKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_REGIONS_KHR`]\n\n* []() VUID-VkPresentRegionsKHR-pRegions-parameter  \n   If [`Self::p_regions`] is not `NULL`, [`Self::p_regions`] **must** be a valid pointer to an array of [`Self::swapchain_count`] valid [`crate::vk::PresentRegionKHR`] structures\n\n* []() VUID-VkPresentRegionsKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentRegionKHR`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PresentRegionsKHRBuilder<'a>(PresentRegionsKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PresentRegionsKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PresentRegionsKHRBuilder<'a> {
        PresentRegionsKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn regions(mut self, regions: &'a [crate::extensions::khr_incremental_present::PresentRegionKHRBuilder]) -> Self {
        self.0.p_regions = regions.as_ptr() as _;
        self.0.swapchain_count = regions.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentRegionsKHR {
        self.0
    }
}
impl<'a> std::default::Default for PresentRegionsKHRBuilder<'a> {
    fn default() -> PresentRegionsKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentRegionsKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentRegionsKHRBuilder<'a> {
    type Target = PresentRegionsKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentRegionsKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentRegionKHR.html)) · Structure <br/> VkPresentRegionKHR - Structure containing rectangular region changed by vkQueuePresentKHR for a given VkImage\n[](#_c_specification)C Specification\n----------\n\nFor a given image and swapchain, the region to present is specified by the[`crate::vk::PresentRegionKHR`] structure, which is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkPresentRegionKHR {\n    uint32_t                 rectangleCount;\n    const VkRectLayerKHR*    pRectangles;\n} VkPresentRegionKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::rectangle_count`] is the number of rectangles in [`Self::p_rectangles`],\n  or zero if the entire image has changed and should be presented.\n\n* [`Self::p_rectangles`] is either `NULL` or a pointer to an array of[`crate::vk::RectLayerKHR`] structures.\n  The [`crate::vk::RectLayerKHR`] structure is the framebuffer coordinates, plus\n  layer, of a portion of a presentable image that has changed and **must** be\n  presented.\n  If non-`NULL`, each entry in [`Self::p_rectangles`] is a rectangle of the\n  given image that has changed since the last image was presented to the\n  given swapchain.\n  The rectangles **must** be specified relative to[`crate::vk::SurfaceCapabilitiesKHR::current_transform`], regardless of\n  the swapchain’s `preTransform`.\n  The presentation engine will apply the `preTransform` transformation\n  to the rectangles, along with any further transformation it applies to\n  the image content.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentRegionKHR-pRectangles-parameter  \n   If [`Self::rectangle_count`] is not `0`, and [`Self::p_rectangles`] is not `NULL`, [`Self::p_rectangles`] **must** be a valid pointer to an array of [`Self::rectangle_count`] valid [`crate::vk::RectLayerKHR`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentRegionsKHR`], [`crate::vk::RectLayerKHR`]\n"]
#[doc(alias = "VkPresentRegionKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentRegionKHR {
    pub rectangle_count: u32,
    pub p_rectangles: *const crate::extensions::khr_incremental_present::RectLayerKHR,
}
impl Default for PresentRegionKHR {
    fn default() -> Self {
        Self { rectangle_count: Default::default(), p_rectangles: std::ptr::null() }
    }
}
impl std::fmt::Debug for PresentRegionKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentRegionKHR").field("rectangle_count", &self.rectangle_count).field("p_rectangles", &self.p_rectangles).finish()
    }
}
impl PresentRegionKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentRegionKHRBuilder<'a> {
        PresentRegionKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentRegionKHR.html)) · Builder of [`PresentRegionKHR`] <br/> VkPresentRegionKHR - Structure containing rectangular region changed by vkQueuePresentKHR for a given VkImage\n[](#_c_specification)C Specification\n----------\n\nFor a given image and swapchain, the region to present is specified by the[`crate::vk::PresentRegionKHR`] structure, which is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkPresentRegionKHR {\n    uint32_t                 rectangleCount;\n    const VkRectLayerKHR*    pRectangles;\n} VkPresentRegionKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::rectangle_count`] is the number of rectangles in [`Self::p_rectangles`],\n  or zero if the entire image has changed and should be presented.\n\n* [`Self::p_rectangles`] is either `NULL` or a pointer to an array of[`crate::vk::RectLayerKHR`] structures.\n  The [`crate::vk::RectLayerKHR`] structure is the framebuffer coordinates, plus\n  layer, of a portion of a presentable image that has changed and **must** be\n  presented.\n  If non-`NULL`, each entry in [`Self::p_rectangles`] is a rectangle of the\n  given image that has changed since the last image was presented to the\n  given swapchain.\n  The rectangles **must** be specified relative to[`crate::vk::SurfaceCapabilitiesKHR::current_transform`], regardless of\n  the swapchain’s `preTransform`.\n  The presentation engine will apply the `preTransform` transformation\n  to the rectangles, along with any further transformation it applies to\n  the image content.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentRegionKHR-pRectangles-parameter  \n   If [`Self::rectangle_count`] is not `0`, and [`Self::p_rectangles`] is not `NULL`, [`Self::p_rectangles`] **must** be a valid pointer to an array of [`Self::rectangle_count`] valid [`crate::vk::RectLayerKHR`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentRegionsKHR`], [`crate::vk::RectLayerKHR`]\n"]
#[repr(transparent)]
pub struct PresentRegionKHRBuilder<'a>(PresentRegionKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PresentRegionKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PresentRegionKHRBuilder<'a> {
        PresentRegionKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn rectangles(mut self, rectangles: &'a [crate::extensions::khr_incremental_present::RectLayerKHRBuilder]) -> Self {
        self.0.p_rectangles = rectangles.as_ptr() as _;
        self.0.rectangle_count = rectangles.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentRegionKHR {
        self.0
    }
}
impl<'a> std::default::Default for PresentRegionKHRBuilder<'a> {
    fn default() -> PresentRegionKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentRegionKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentRegionKHRBuilder<'a> {
    type Target = PresentRegionKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentRegionKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRectLayerKHR.html)) · Structure <br/> VkRectLayerKHR - Structure containing a rectangle, including layer, changed by vkQueuePresentKHR for a given VkImage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::RectLayerKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkRectLayerKHR {\n    VkOffset2D    offset;\n    VkExtent2D    extent;\n    uint32_t      layer;\n} VkRectLayerKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::offset`] is the origin of the rectangle, in pixels.\n\n* [`Self::extent`] is the size of the rectangle, in pixels.\n\n* [`Self::layer`] is the layer of the image.\n  For images with only one layer, the value of [`Self::layer`] **must** be 0.\n[](#_description)Description\n----------\n\nSome platforms allow the size of a surface to change, and then scale the\npixels of the image to fit the surface.[`crate::vk::RectLayerKHR`] specifies pixels of the swapchain’s image(s), which\nwill be constant for the life of the swapchain.\n\nValid Usage\n\n* []() VUID-VkRectLayerKHR-offset-04864  \n   The sum of [`Self::offset`] and [`Self::extent`], after being transformed\n  according to the `preTransform` member of the[`crate::vk::SwapchainCreateInfoKHR`] structure, **must** be no greater than the`imageExtent` member of the [`crate::vk::SwapchainCreateInfoKHR`] structure\n  passed to [`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* []() VUID-VkRectLayerKHR-layer-01262  \n  [`Self::layer`] **must** be less than the `imageArrayLayers` member of the[`crate::vk::SwapchainCreateInfoKHR`] structure passed to[`crate::vk::DeviceLoader::create_swapchain_khr`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::Offset2D`], [`crate::vk::PresentRegionKHR`]\n"]
#[doc(alias = "VkRectLayerKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RectLayerKHR {
    pub offset: crate::vk1_0::Offset2D,
    pub extent: crate::vk1_0::Extent2D,
    pub layer: u32,
}
impl Default for RectLayerKHR {
    fn default() -> Self {
        Self { offset: Default::default(), extent: Default::default(), layer: Default::default() }
    }
}
impl std::fmt::Debug for RectLayerKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RectLayerKHR").field("offset", &self.offset).field("extent", &self.extent).field("layer", &self.layer).finish()
    }
}
impl RectLayerKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> RectLayerKHRBuilder<'a> {
        RectLayerKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRectLayerKHR.html)) · Builder of [`RectLayerKHR`] <br/> VkRectLayerKHR - Structure containing a rectangle, including layer, changed by vkQueuePresentKHR for a given VkImage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::RectLayerKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_incremental_present\ntypedef struct VkRectLayerKHR {\n    VkOffset2D    offset;\n    VkExtent2D    extent;\n    uint32_t      layer;\n} VkRectLayerKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::offset`] is the origin of the rectangle, in pixels.\n\n* [`Self::extent`] is the size of the rectangle, in pixels.\n\n* [`Self::layer`] is the layer of the image.\n  For images with only one layer, the value of [`Self::layer`] **must** be 0.\n[](#_description)Description\n----------\n\nSome platforms allow the size of a surface to change, and then scale the\npixels of the image to fit the surface.[`crate::vk::RectLayerKHR`] specifies pixels of the swapchain’s image(s), which\nwill be constant for the life of the swapchain.\n\nValid Usage\n\n* []() VUID-VkRectLayerKHR-offset-04864  \n   The sum of [`Self::offset`] and [`Self::extent`], after being transformed\n  according to the `preTransform` member of the[`crate::vk::SwapchainCreateInfoKHR`] structure, **must** be no greater than the`imageExtent` member of the [`crate::vk::SwapchainCreateInfoKHR`] structure\n  passed to [`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* []() VUID-VkRectLayerKHR-layer-01262  \n  [`Self::layer`] **must** be less than the `imageArrayLayers` member of the[`crate::vk::SwapchainCreateInfoKHR`] structure passed to[`crate::vk::DeviceLoader::create_swapchain_khr`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::Offset2D`], [`crate::vk::PresentRegionKHR`]\n"]
#[repr(transparent)]
pub struct RectLayerKHRBuilder<'a>(RectLayerKHR, std::marker::PhantomData<&'a ()>);
impl<'a> RectLayerKHRBuilder<'a> {
    #[inline]
    pub fn new() -> RectLayerKHRBuilder<'a> {
        RectLayerKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn offset(mut self, offset: crate::vk1_0::Offset2D) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    pub fn extent(mut self, extent: crate::vk1_0::Extent2D) -> Self {
        self.0.extent = extent as _;
        self
    }
    #[inline]
    pub fn layer(mut self, layer: u32) -> Self {
        self.0.layer = layer as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RectLayerKHR {
        self.0
    }
}
impl<'a> std::default::Default for RectLayerKHRBuilder<'a> {
    fn default() -> RectLayerKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RectLayerKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RectLayerKHRBuilder<'a> {
    type Target = RectLayerKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RectLayerKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
