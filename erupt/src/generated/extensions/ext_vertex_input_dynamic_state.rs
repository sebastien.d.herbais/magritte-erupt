#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VERTEX_INPUT_DYNAMIC_STATE_SPEC_VERSION")]
pub const EXT_VERTEX_INPUT_DYNAMIC_STATE_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VERTEX_INPUT_DYNAMIC_STATE_EXTENSION_NAME")]
pub const EXT_VERTEX_INPUT_DYNAMIC_STATE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_vertex_input_dynamic_state");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_VERTEX_INPUT_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdSetVertexInputEXT");
#[doc = "Provided by [`crate::extensions::ext_vertex_input_dynamic_state`]"]
impl crate::vk1_0::DynamicState {
    pub const VERTEX_INPUT_EXT: Self = Self(1000352000);
}
#[doc = "Provided by [`crate::extensions::ext_vertex_input_dynamic_state`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT: Self = Self(1000352000);
    pub const VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT: Self = Self(1000352001);
    pub const VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT: Self = Self(1000352002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetVertexInputEXT.html)) · Function <br/> vkCmdSetVertexInputEXT - Set the dynamic vertex input state\n[](#_c_specification)C Specification\n----------\n\nAn alternative to specifying the vertex input attribute and vertex input\nbinding descriptions as part of graphics pipeline creation, the pipeline**can** be created with the [`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic\nstate enabled, and for that state to be set dynamically with:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\nvoid vkCmdSetVertexInputEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    vertexBindingDescriptionCount,\n    const VkVertexInputBindingDescription2EXT*  pVertexBindingDescriptions,\n    uint32_t                                    vertexAttributeDescriptionCount,\n    const VkVertexInputAttributeDescription2EXT* pVertexAttributeDescriptions);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::vertex_binding_description_count`] is the number of vertex binding\n  descriptions provided in [`Self::p_vertex_binding_descriptions`].\n\n* [`Self::p_vertex_binding_descriptions`] is a pointer to an array of[`crate::vk::VertexInputBindingDescription2EXT`] structures.\n\n* [`Self::vertex_attribute_description_count`] is the number of vertex attribute\n  descriptions provided in [`Self::p_vertex_attribute_descriptions`].\n\n* [`Self::p_vertex_attribute_descriptions`] is a pointer to an array of[`crate::vk::VertexInputAttributeDescription2EXT`] structures.\n[](#_description)Description\n----------\n\nThis command sets the vertex input attribute and vertex input binding\ndescriptions state for subsequent drawing commands.\n\nValid Usage\n\n* []() VUID-vkCmdSetVertexInputEXT-None-04790  \n   The [vertexInputDynamicState](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexInputDynamicState)feature **must** be enabled\n\n* []() VUID-vkCmdSetVertexInputEXT-vertexBindingDescriptionCount-04791  \n  [`Self::vertex_binding_description_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-vkCmdSetVertexInputEXT-vertexAttributeDescriptionCount-04792  \n  [`Self::vertex_attribute_description_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributes`\n\n* []() VUID-vkCmdSetVertexInputEXT-binding-04793  \n   For every `binding` specified by each element of[`Self::p_vertex_attribute_descriptions`], a[`crate::vk::VertexInputBindingDescription2EXT`] **must** exist in[`Self::p_vertex_binding_descriptions`] with the same value of `binding`\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexBindingDescriptions-04794  \n   All elements of [`Self::p_vertex_binding_descriptions`] **must** describe distinct\n  binding numbers\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexAttributeDescriptions-04795  \n   All elements of [`Self::p_vertex_attribute_descriptions`] **must** describe\n  distinct attribute locations\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexBindingDescriptions-parameter  \n   If [`Self::vertex_binding_description_count`] is not `0`, [`Self::p_vertex_binding_descriptions`] **must** be a valid pointer to an array of [`Self::vertex_binding_description_count`] valid [`crate::vk::VertexInputBindingDescription2EXT`] structures\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexAttributeDescriptions-parameter  \n   If [`Self::vertex_attribute_description_count`] is not `0`, [`Self::p_vertex_attribute_descriptions`] **must** be a valid pointer to an array of [`Self::vertex_attribute_description_count`] valid [`crate::vk::VertexInputAttributeDescription2EXT`] structures\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VertexInputAttributeDescription2EXT`], [`crate::vk::VertexInputBindingDescription2EXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetVertexInputEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, vertex_binding_description_count: u32, p_vertex_binding_descriptions: *const crate::extensions::ext_vertex_input_dynamic_state::VertexInputBindingDescription2EXT, vertex_attribute_description_count: u32, p_vertex_attribute_descriptions: *const crate::extensions::ext_vertex_input_dynamic_state::VertexInputAttributeDescription2EXT) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceVertexInputDynamicStateFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexInputDynamicStateFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT - Structure describing whether the dynamic vertex input state can be used\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           vertexInputDynamicState;\n} VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::vertex_input_dynamic_state`]indicates that the implementation supports the following dynamic states:\n\n  * [`crate::vk::DynamicState::VERTEX_INPUT_EXT`]\n\nIf the [`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub vertex_input_dynamic_state: crate::vk1_0::Bool32,
}
impl PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT;
}
impl Default for PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), vertex_input_dynamic_state: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceVertexInputDynamicStateFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("vertex_input_dynamic_state", &(self.vertex_input_dynamic_state != 0)).finish()
    }
}
impl PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
        PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT.html)) · Builder of [`PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] <br/> VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT - Structure describing whether the dynamic vertex input state can be used\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           vertexInputDynamicState;\n} VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::vertex_input_dynamic_state`]indicates that the implementation supports the following dynamic states:\n\n  * [`crate::vk::DynamicState::VERTEX_INPUT_EXT`]\n\nIf the [`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceVertexInputDynamicStateFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a>(PhysicalDeviceVertexInputDynamicStateFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
        PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn vertex_input_dynamic_state(mut self, vertex_input_dynamic_state: bool) -> Self {
        self.0.vertex_input_dynamic_state = vertex_input_dynamic_state as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceVertexInputDynamicStateFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceVertexInputDynamicStateFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputBindingDescription2EXT.html)) · Structure <br/> VkVertexInputBindingDescription2EXT - Structure specifying the extended vertex input binding description\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VertexInputBindingDescription2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkVertexInputBindingDescription2EXT {\n    VkStructureType      sType;\n    void*                pNext;\n    uint32_t             binding;\n    uint32_t             stride;\n    VkVertexInputRate    inputRate;\n    uint32_t             divisor;\n} VkVertexInputBindingDescription2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::binding`] is the binding number that this structure describes.\n\n* [`Self::stride`] is the byte stride between consecutive elements within the\n  buffer.\n\n* [`Self::input_rate`] is a [`crate::vk::VertexInputRate`] value specifying whether\n  vertex attribute addressing is a function of the vertex index or of the\n  instance index.\n\n* [`Self::divisor`] is the number of successive instances that will use the\n  same value of the vertex attribute when instanced rendering is enabled.\n  This member **can** be set to a value other than `1` if the[vertexAttributeInstanceRateDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor) feature is enabled.\n  For example, if the divisor is N, the same vertex attribute will be\n  applied to N successive instances before moving on to the next vertex\n  attribute.\n  The maximum value of [`Self::divisor`] is implementation-dependent and can\n  be queried using[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`.\n  A value of `0` **can** be used for the divisor if the[`vertexAttributeInstanceRateZeroDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor)feature is enabled.\n  In this case, the same vertex attribute will be applied to all\n  instances.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVertexInputBindingDescription2EXT-binding-04796  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-stride-04797  \n  [`Self::stride`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindingStride`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04798  \n   If the [vertexAttributeInstanceRateZeroDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor) feature is not enabled,[`Self::divisor`] **must** not be `0`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04799  \n   If the [vertexAttributeInstanceRateDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor) feature is not enabled,[`Self::divisor`] **must** be `1`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04800  \n  [`Self::divisor`] **must** be a value between `0` and[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`,\n  inclusive\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04801  \n   If [`Self::divisor`] is not `1` then [`Self::input_rate`] **must** be of type[`crate::vk::VertexInputRate::INSTANCE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkVertexInputBindingDescription2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT`]\n\n* []() VUID-VkVertexInputBindingDescription2EXT-inputRate-parameter  \n  [`Self::input_rate`] **must** be a valid [`crate::vk::VertexInputRate`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VertexInputRate`], [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`]\n"]
#[doc(alias = "VkVertexInputBindingDescription2EXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VertexInputBindingDescription2EXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub binding: u32,
    pub stride: u32,
    pub input_rate: crate::vk1_0::VertexInputRate,
    pub divisor: u32,
}
impl VertexInputBindingDescription2EXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT;
}
impl Default for VertexInputBindingDescription2EXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), binding: Default::default(), stride: Default::default(), input_rate: Default::default(), divisor: Default::default() }
    }
}
impl std::fmt::Debug for VertexInputBindingDescription2EXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VertexInputBindingDescription2EXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("binding", &self.binding).field("stride", &self.stride).field("input_rate", &self.input_rate).field("divisor", &self.divisor).finish()
    }
}
impl VertexInputBindingDescription2EXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VertexInputBindingDescription2EXTBuilder<'a> {
        VertexInputBindingDescription2EXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputBindingDescription2EXT.html)) · Builder of [`VertexInputBindingDescription2EXT`] <br/> VkVertexInputBindingDescription2EXT - Structure specifying the extended vertex input binding description\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VertexInputBindingDescription2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkVertexInputBindingDescription2EXT {\n    VkStructureType      sType;\n    void*                pNext;\n    uint32_t             binding;\n    uint32_t             stride;\n    VkVertexInputRate    inputRate;\n    uint32_t             divisor;\n} VkVertexInputBindingDescription2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::binding`] is the binding number that this structure describes.\n\n* [`Self::stride`] is the byte stride between consecutive elements within the\n  buffer.\n\n* [`Self::input_rate`] is a [`crate::vk::VertexInputRate`] value specifying whether\n  vertex attribute addressing is a function of the vertex index or of the\n  instance index.\n\n* [`Self::divisor`] is the number of successive instances that will use the\n  same value of the vertex attribute when instanced rendering is enabled.\n  This member **can** be set to a value other than `1` if the[vertexAttributeInstanceRateDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor) feature is enabled.\n  For example, if the divisor is N, the same vertex attribute will be\n  applied to N successive instances before moving on to the next vertex\n  attribute.\n  The maximum value of [`Self::divisor`] is implementation-dependent and can\n  be queried using[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`.\n  A value of `0` **can** be used for the divisor if the[`vertexAttributeInstanceRateZeroDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor)feature is enabled.\n  In this case, the same vertex attribute will be applied to all\n  instances.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVertexInputBindingDescription2EXT-binding-04796  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-stride-04797  \n  [`Self::stride`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindingStride`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04798  \n   If the [vertexAttributeInstanceRateZeroDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor) feature is not enabled,[`Self::divisor`] **must** not be `0`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04799  \n   If the [vertexAttributeInstanceRateDivisor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor) feature is not enabled,[`Self::divisor`] **must** be `1`\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04800  \n  [`Self::divisor`] **must** be a value between `0` and[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`,\n  inclusive\n\n* []() VUID-VkVertexInputBindingDescription2EXT-divisor-04801  \n   If [`Self::divisor`] is not `1` then [`Self::input_rate`] **must** be of type[`crate::vk::VertexInputRate::INSTANCE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkVertexInputBindingDescription2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT`]\n\n* []() VUID-VkVertexInputBindingDescription2EXT-inputRate-parameter  \n  [`Self::input_rate`] **must** be a valid [`crate::vk::VertexInputRate`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VertexInputRate`], [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`]\n"]
#[repr(transparent)]
pub struct VertexInputBindingDescription2EXTBuilder<'a>(VertexInputBindingDescription2EXT, std::marker::PhantomData<&'a ()>);
impl<'a> VertexInputBindingDescription2EXTBuilder<'a> {
    #[inline]
    pub fn new() -> VertexInputBindingDescription2EXTBuilder<'a> {
        VertexInputBindingDescription2EXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn binding(mut self, binding: u32) -> Self {
        self.0.binding = binding as _;
        self
    }
    #[inline]
    pub fn stride(mut self, stride: u32) -> Self {
        self.0.stride = stride as _;
        self
    }
    #[inline]
    pub fn input_rate(mut self, input_rate: crate::vk1_0::VertexInputRate) -> Self {
        self.0.input_rate = input_rate as _;
        self
    }
    #[inline]
    pub fn divisor(mut self, divisor: u32) -> Self {
        self.0.divisor = divisor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VertexInputBindingDescription2EXT {
        self.0
    }
}
impl<'a> std::default::Default for VertexInputBindingDescription2EXTBuilder<'a> {
    fn default() -> VertexInputBindingDescription2EXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VertexInputBindingDescription2EXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VertexInputBindingDescription2EXTBuilder<'a> {
    type Target = VertexInputBindingDescription2EXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VertexInputBindingDescription2EXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputAttributeDescription2EXT.html)) · Structure <br/> VkVertexInputAttributeDescription2EXT - Structure specifying the extended vertex input attribute description\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VertexInputAttributeDescription2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkVertexInputAttributeDescription2EXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           location;\n    uint32_t           binding;\n    VkFormat           format;\n    uint32_t           offset;\n} VkVertexInputAttributeDescription2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::location`] is the shader input location number for this attribute.\n\n* [`Self::binding`] is the binding number which this attribute takes its data\n  from.\n\n* [`Self::format`] is the size and type of the vertex attribute data.\n\n* [`Self::offset`] is a byte offset of this attribute relative to the start of\n  an element in the vertex input binding.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-location-04802  \n  [`Self::location`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributes`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-binding-04803  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-offset-04804  \n  [`Self::offset`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributeOffset`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-format-04805  \n  [`Self::format`] **must** be allowed as a vertex buffer format, as specified by\n  the [`crate::vk::FormatFeatureFlagBits::VERTEX_BUFFER`] flag in[`crate::vk::FormatProperties`]::`bufferFeatures` returned by[`crate::vk::PFN_vkGetPhysicalDeviceFormatProperties`]\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-vertexAttributeAccessBeyondStride-04806  \n   If the [VK_KHR_portability_subset](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_KHR_portability_subset.html) extension is enabled, and[`crate::vk::PhysicalDevicePortabilitySubsetFeaturesKHR::vertex_attribute_access_beyond_stride`]is [`crate::vk::FALSE`], the sum of [`Self::offset`] plus the size of the vertex\n  attribute data described by [`Self::format`] **must** not be greater than`stride` in the [`crate::vk::VertexInputBindingDescription2EXT`] referenced\n  in [`Self::binding`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT`]\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`]\n"]
#[doc(alias = "VkVertexInputAttributeDescription2EXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VertexInputAttributeDescription2EXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub location: u32,
    pub binding: u32,
    pub format: crate::vk1_0::Format,
    pub offset: u32,
}
impl VertexInputAttributeDescription2EXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT;
}
impl Default for VertexInputAttributeDescription2EXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), location: Default::default(), binding: Default::default(), format: Default::default(), offset: Default::default() }
    }
}
impl std::fmt::Debug for VertexInputAttributeDescription2EXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VertexInputAttributeDescription2EXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("location", &self.location).field("binding", &self.binding).field("format", &self.format).field("offset", &self.offset).finish()
    }
}
impl VertexInputAttributeDescription2EXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VertexInputAttributeDescription2EXTBuilder<'a> {
        VertexInputAttributeDescription2EXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputAttributeDescription2EXT.html)) · Builder of [`VertexInputAttributeDescription2EXT`] <br/> VkVertexInputAttributeDescription2EXT - Structure specifying the extended vertex input attribute description\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VertexInputAttributeDescription2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\ntypedef struct VkVertexInputAttributeDescription2EXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           location;\n    uint32_t           binding;\n    VkFormat           format;\n    uint32_t           offset;\n} VkVertexInputAttributeDescription2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::location`] is the shader input location number for this attribute.\n\n* [`Self::binding`] is the binding number which this attribute takes its data\n  from.\n\n* [`Self::format`] is the size and type of the vertex attribute data.\n\n* [`Self::offset`] is a byte offset of this attribute relative to the start of\n  an element in the vertex input binding.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-location-04802  \n  [`Self::location`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributes`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-binding-04803  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-offset-04804  \n  [`Self::offset`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributeOffset`\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-format-04805  \n  [`Self::format`] **must** be allowed as a vertex buffer format, as specified by\n  the [`crate::vk::FormatFeatureFlagBits::VERTEX_BUFFER`] flag in[`crate::vk::FormatProperties`]::`bufferFeatures` returned by[`crate::vk::PFN_vkGetPhysicalDeviceFormatProperties`]\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-vertexAttributeAccessBeyondStride-04806  \n   If the [VK_KHR_portability_subset](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_KHR_portability_subset.html) extension is enabled, and[`crate::vk::PhysicalDevicePortabilitySubsetFeaturesKHR::vertex_attribute_access_beyond_stride`]is [`crate::vk::FALSE`], the sum of [`Self::offset`] plus the size of the vertex\n  attribute data described by [`Self::format`] **must** not be greater than`stride` in the [`crate::vk::VertexInputBindingDescription2EXT`] referenced\n  in [`Self::binding`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT`]\n\n* []() VUID-VkVertexInputAttributeDescription2EXT-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`]\n"]
#[repr(transparent)]
pub struct VertexInputAttributeDescription2EXTBuilder<'a>(VertexInputAttributeDescription2EXT, std::marker::PhantomData<&'a ()>);
impl<'a> VertexInputAttributeDescription2EXTBuilder<'a> {
    #[inline]
    pub fn new() -> VertexInputAttributeDescription2EXTBuilder<'a> {
        VertexInputAttributeDescription2EXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn location(mut self, location: u32) -> Self {
        self.0.location = location as _;
        self
    }
    #[inline]
    pub fn binding(mut self, binding: u32) -> Self {
        self.0.binding = binding as _;
        self
    }
    #[inline]
    pub fn format(mut self, format: crate::vk1_0::Format) -> Self {
        self.0.format = format as _;
        self
    }
    #[inline]
    pub fn offset(mut self, offset: u32) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VertexInputAttributeDescription2EXT {
        self.0
    }
}
impl<'a> std::default::Default for VertexInputAttributeDescription2EXTBuilder<'a> {
    fn default() -> VertexInputAttributeDescription2EXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VertexInputAttributeDescription2EXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VertexInputAttributeDescription2EXTBuilder<'a> {
    type Target = VertexInputAttributeDescription2EXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VertexInputAttributeDescription2EXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_vertex_input_dynamic_state`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetVertexInputEXT.html)) · Function <br/> vkCmdSetVertexInputEXT - Set the dynamic vertex input state\n[](#_c_specification)C Specification\n----------\n\nAn alternative to specifying the vertex input attribute and vertex input\nbinding descriptions as part of graphics pipeline creation, the pipeline**can** be created with the [`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic\nstate enabled, and for that state to be set dynamically with:\n\n```\n// Provided by VK_EXT_vertex_input_dynamic_state\nvoid vkCmdSetVertexInputEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    vertexBindingDescriptionCount,\n    const VkVertexInputBindingDescription2EXT*  pVertexBindingDescriptions,\n    uint32_t                                    vertexAttributeDescriptionCount,\n    const VkVertexInputAttributeDescription2EXT* pVertexAttributeDescriptions);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::vertex_binding_description_count`] is the number of vertex binding\n  descriptions provided in [`Self::p_vertex_binding_descriptions`].\n\n* [`Self::p_vertex_binding_descriptions`] is a pointer to an array of[`crate::vk::VertexInputBindingDescription2EXT`] structures.\n\n* [`Self::vertex_attribute_description_count`] is the number of vertex attribute\n  descriptions provided in [`Self::p_vertex_attribute_descriptions`].\n\n* [`Self::p_vertex_attribute_descriptions`] is a pointer to an array of[`crate::vk::VertexInputAttributeDescription2EXT`] structures.\n[](#_description)Description\n----------\n\nThis command sets the vertex input attribute and vertex input binding\ndescriptions state for subsequent drawing commands.\n\nValid Usage\n\n* []() VUID-vkCmdSetVertexInputEXT-None-04790  \n   The [vertexInputDynamicState](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexInputDynamicState)feature **must** be enabled\n\n* []() VUID-vkCmdSetVertexInputEXT-vertexBindingDescriptionCount-04791  \n  [`Self::vertex_binding_description_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-vkCmdSetVertexInputEXT-vertexAttributeDescriptionCount-04792  \n  [`Self::vertex_attribute_description_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputAttributes`\n\n* []() VUID-vkCmdSetVertexInputEXT-binding-04793  \n   For every `binding` specified by each element of[`Self::p_vertex_attribute_descriptions`], a[`crate::vk::VertexInputBindingDescription2EXT`] **must** exist in[`Self::p_vertex_binding_descriptions`] with the same value of `binding`\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexBindingDescriptions-04794  \n   All elements of [`Self::p_vertex_binding_descriptions`] **must** describe distinct\n  binding numbers\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexAttributeDescriptions-04795  \n   All elements of [`Self::p_vertex_attribute_descriptions`] **must** describe\n  distinct attribute locations\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexBindingDescriptions-parameter  \n   If [`Self::vertex_binding_description_count`] is not `0`, [`Self::p_vertex_binding_descriptions`] **must** be a valid pointer to an array of [`Self::vertex_binding_description_count`] valid [`crate::vk::VertexInputBindingDescription2EXT`] structures\n\n* []() VUID-vkCmdSetVertexInputEXT-pVertexAttributeDescriptions-parameter  \n   If [`Self::vertex_attribute_description_count`] is not `0`, [`Self::p_vertex_attribute_descriptions`] **must** be a valid pointer to an array of [`Self::vertex_attribute_description_count`] valid [`crate::vk::VertexInputAttributeDescription2EXT`] structures\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetVertexInputEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VertexInputAttributeDescription2EXT`], [`crate::vk::VertexInputBindingDescription2EXT`]\n"]
    #[doc(alias = "vkCmdSetVertexInputEXT")]
    pub unsafe fn cmd_set_vertex_input_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, vertex_binding_descriptions: &[crate::extensions::ext_vertex_input_dynamic_state::VertexInputBindingDescription2EXTBuilder], vertex_attribute_descriptions: &[crate::extensions::ext_vertex_input_dynamic_state::VertexInputAttributeDescription2EXTBuilder]) -> () {
        let _function = self.cmd_set_vertex_input_ext.expect(crate::NOT_LOADED_MESSAGE);
        let vertex_binding_description_count = vertex_binding_descriptions.len();
        let vertex_attribute_description_count = vertex_attribute_descriptions.len();
        let _return = _function(command_buffer as _, vertex_binding_description_count as _, vertex_binding_descriptions.as_ptr() as _, vertex_attribute_description_count as _, vertex_attribute_descriptions.as_ptr() as _);
        ()
    }
}
