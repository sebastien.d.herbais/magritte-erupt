#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DIRECTFB_SURFACE_SPEC_VERSION")]
pub const EXT_DIRECTFB_SURFACE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DIRECTFB_SURFACE_EXTENSION_NAME")]
pub const EXT_DIRECTFB_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_directfb_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_DIRECT_FB_SURFACE_EXT: *const std::os::raw::c_char = crate::cstr!("vkCreateDirectFBSurfaceEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_DIRECT_FB_PRESENTATION_SUPPORT_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceDirectFBPresentationSupportEXT");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDirectFBSurfaceCreateFlagsEXT.html)) · Bitmask of [`DirectFBSurfaceCreateFlagBitsEXT`] <br/> VkDirectFBSurfaceCreateFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_directfb_surface\ntypedef VkFlags VkDirectFBSurfaceCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DirectFBSurfaceCreateFlagBitsEXT`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DirectFBSurfaceCreateInfoEXT`]\n"] # [doc (alias = "VkDirectFBSurfaceCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DirectFBSurfaceCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DirectFBSurfaceCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkDirectFBSurfaceCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DirectFBSurfaceCreateFlagBitsEXT(pub u32);
impl DirectFBSurfaceCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DirectFBSurfaceCreateFlagsEXT {
        DirectFBSurfaceCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DirectFBSurfaceCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_directfb_surface`]"]
impl crate::vk1_0::StructureType {
    pub const DIRECTFB_SURFACE_CREATE_INFO_EXT: Self = Self(1000346000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDirectFBSurfaceEXT.html)) · Function <br/> vkCreateDirectFBSurfaceEXT - Create a slink:VkSurfaceKHR object for a DirectFB surface\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a DirectFB surface, call:\n\n```\n// Provided by VK_EXT_directfb_surface\nVkResult vkCreateDirectFBSurfaceEXT(\n    VkInstance                                  instance,\n    const VkDirectFBSurfaceCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DirectFBSurfaceCreateInfoEXT`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DirectFBSurfaceCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DirectFBSurfaceCreateInfoEXT`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateDirectFBSurfaceEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::ext_directfb_surface::DirectFBSurfaceCreateInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDirectFBPresentationSupportEXT.html)) · Function <br/> vkGetPhysicalDeviceDirectFBPresentationSupportEXT - Query physical device for presentation with DirectFB\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation with DirectFB library, call:\n\n```\n// Provided by VK_EXT_directfb_surface\nVkBool32 vkGetPhysicalDeviceDirectFBPresentationSupportEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    IDirectFB*                                  dfb);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::dfb`] is a pointer to the `IDirectFB` main interface of DirectFB.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-queueFamilyIndex-04119  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-dfb-parameter  \n  [`Self::dfb`] **must** be a valid pointer to an `IDirectFB` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceDirectFBPresentationSupportEXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, dfb: *mut std::ffi::c_void) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDirectFBSurfaceCreateInfoEXT.html)) · Structure <br/> VkDirectFBSurfaceCreateInfoEXT - Structure specifying parameters of a newly created DirectFB surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DirectFBSurfaceCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_directfb_surface\ntypedef struct VkDirectFBSurfaceCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkDirectFBSurfaceCreateFlagsEXT    flags;\n    IDirectFB*                         dfb;\n    IDirectFBSurface*                  surface;\n} VkDirectFBSurfaceCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::dfb`] is a pointer to the `IDirectFB` main interface of DirectFB.\n\n* [`Self::surface`] is a pointer to a `IDirectFBSurface` surface interface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-dfb-04117  \n  [`Self::dfb`] **must** point to a valid DirectFB `IDirectFB`\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-surface-04118  \n  [`Self::surface`] **must** point to a valid DirectFB `IDirectFBSurface`\n\nValid Usage (Implicit)\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DIRECTFB_SURFACE_CREATE_INFO_EXT`]\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DirectFBSurfaceCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_direct_fb_surface_ext`]\n"]
#[doc(alias = "VkDirectFBSurfaceCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DirectFBSurfaceCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_directfb_surface::DirectFBSurfaceCreateFlagsEXT,
    pub dfb: *mut std::ffi::c_void,
    pub surface: *mut std::ffi::c_void,
}
impl DirectFBSurfaceCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DIRECTFB_SURFACE_CREATE_INFO_EXT;
}
impl Default for DirectFBSurfaceCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), dfb: std::ptr::null_mut(), surface: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for DirectFBSurfaceCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DirectFBSurfaceCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("dfb", &self.dfb).field("surface", &self.surface).finish()
    }
}
impl DirectFBSurfaceCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DirectFBSurfaceCreateInfoEXTBuilder<'a> {
        DirectFBSurfaceCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDirectFBSurfaceCreateInfoEXT.html)) · Builder of [`DirectFBSurfaceCreateInfoEXT`] <br/> VkDirectFBSurfaceCreateInfoEXT - Structure specifying parameters of a newly created DirectFB surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DirectFBSurfaceCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_directfb_surface\ntypedef struct VkDirectFBSurfaceCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkDirectFBSurfaceCreateFlagsEXT    flags;\n    IDirectFB*                         dfb;\n    IDirectFBSurface*                  surface;\n} VkDirectFBSurfaceCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::dfb`] is a pointer to the `IDirectFB` main interface of DirectFB.\n\n* [`Self::surface`] is a pointer to a `IDirectFBSurface` surface interface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-dfb-04117  \n  [`Self::dfb`] **must** point to a valid DirectFB `IDirectFB`\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-surface-04118  \n  [`Self::surface`] **must** point to a valid DirectFB `IDirectFBSurface`\n\nValid Usage (Implicit)\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DIRECTFB_SURFACE_CREATE_INFO_EXT`]\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDirectFBSurfaceCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DirectFBSurfaceCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_direct_fb_surface_ext`]\n"]
#[repr(transparent)]
pub struct DirectFBSurfaceCreateInfoEXTBuilder<'a>(DirectFBSurfaceCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DirectFBSurfaceCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DirectFBSurfaceCreateInfoEXTBuilder<'a> {
        DirectFBSurfaceCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_directfb_surface::DirectFBSurfaceCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn dfb(mut self, dfb: *mut std::ffi::c_void) -> Self {
        self.0.dfb = dfb;
        self
    }
    #[inline]
    pub fn surface(mut self, surface: *mut std::ffi::c_void) -> Self {
        self.0.surface = surface;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DirectFBSurfaceCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DirectFBSurfaceCreateInfoEXTBuilder<'a> {
    fn default() -> DirectFBSurfaceCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DirectFBSurfaceCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DirectFBSurfaceCreateInfoEXTBuilder<'a> {
    type Target = DirectFBSurfaceCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DirectFBSurfaceCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_directfb_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDirectFBSurfaceEXT.html)) · Function <br/> vkCreateDirectFBSurfaceEXT - Create a slink:VkSurfaceKHR object for a DirectFB surface\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a DirectFB surface, call:\n\n```\n// Provided by VK_EXT_directfb_surface\nVkResult vkCreateDirectFBSurfaceEXT(\n    VkInstance                                  instance,\n    const VkDirectFBSurfaceCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DirectFBSurfaceCreateInfoEXT`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DirectFBSurfaceCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDirectFBSurfaceEXT-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DirectFBSurfaceCreateInfoEXT`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateDirectFBSurfaceEXT")]
    pub unsafe fn create_direct_fb_surface_ext(&self, create_info: &crate::extensions::ext_directfb_surface::DirectFBSurfaceCreateInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_direct_fb_surface_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDirectFBPresentationSupportEXT.html)) · Function <br/> vkGetPhysicalDeviceDirectFBPresentationSupportEXT - Query physical device for presentation with DirectFB\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation with DirectFB library, call:\n\n```\n// Provided by VK_EXT_directfb_surface\nVkBool32 vkGetPhysicalDeviceDirectFBPresentationSupportEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    IDirectFB*                                  dfb);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::dfb`] is a pointer to the `IDirectFB` main interface of DirectFB.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-queueFamilyIndex-04119  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDirectFBPresentationSupportEXT-dfb-parameter  \n  [`Self::dfb`] **must** be a valid pointer to an `IDirectFB` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceDirectFBPresentationSupportEXT")]
    pub unsafe fn get_physical_device_direct_fb_presentation_support_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, dfb: *mut std::ffi::c_void) -> bool {
        let _function = self.get_physical_device_direct_fb_presentation_support_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _, dfb);
        _return != 0
    }
}
