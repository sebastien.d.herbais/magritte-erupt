#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_VIEWPORT_SWIZZLE_SPEC_VERSION")]
pub const NV_VIEWPORT_SWIZZLE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_VIEWPORT_SWIZZLE_EXTENSION_NAME")]
pub const NV_VIEWPORT_SWIZZLE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_viewport_swizzle");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportSwizzleStateCreateFlagsNV.html)) · Bitmask of [`PipelineViewportSwizzleStateCreateFlagBitsNV`] <br/> VkPipelineViewportSwizzleStateCreateFlagsNV - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef VkFlags VkPipelineViewportSwizzleStateCreateFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateFlagBitsNV`] is a bitmask type for\nsetting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`]\n"] # [doc (alias = "VkPipelineViewportSwizzleStateCreateFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct PipelineViewportSwizzleStateCreateFlagsNV : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineViewportSwizzleStateCreateFlagsNV`] <br/> "]
#[doc(alias = "VkPipelineViewportSwizzleStateCreateFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineViewportSwizzleStateCreateFlagBitsNV(pub u32);
impl PipelineViewportSwizzleStateCreateFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineViewportSwizzleStateCreateFlagsNV {
        PipelineViewportSwizzleStateCreateFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineViewportSwizzleStateCreateFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_viewport_swizzle`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV: Self = Self(1000098000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViewportCoordinateSwizzleNV.html)) · Enum <br/> VkViewportCoordinateSwizzleNV - Specify how a viewport coordinate is swizzled\n[](#_c_specification)C Specification\n----------\n\nPossible values of the [`crate::vk::ViewportSwizzleNV::x`], `y`, `z`,\nand `w` members, specifying swizzling of the corresponding components of\nprimitives, are:\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef enum VkViewportCoordinateSwizzleNV {\n    VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV = 0,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV = 1,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV = 2,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV = 3,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV = 4,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV = 5,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV = 6,\n    VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV = 7,\n} VkViewportCoordinateSwizzleNV;\n```\n[](#_description)Description\n----------\n\nThese values are described in detail in [Viewport Swizzle](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vertexpostproc-viewport-swizzle).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ViewportSwizzleNV`]\n"]
#[doc(alias = "VkViewportCoordinateSwizzleNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ViewportCoordinateSwizzleNV(pub i32);
impl std::fmt::Debug for ViewportCoordinateSwizzleNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::POSITIVE_X_NV => "POSITIVE_X_NV",
            &Self::NEGATIVE_X_NV => "NEGATIVE_X_NV",
            &Self::POSITIVE_Y_NV => "POSITIVE_Y_NV",
            &Self::NEGATIVE_Y_NV => "NEGATIVE_Y_NV",
            &Self::POSITIVE_Z_NV => "POSITIVE_Z_NV",
            &Self::NEGATIVE_Z_NV => "NEGATIVE_Z_NV",
            &Self::POSITIVE_W_NV => "POSITIVE_W_NV",
            &Self::NEGATIVE_W_NV => "NEGATIVE_W_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_viewport_swizzle`]"]
impl crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV {
    pub const POSITIVE_X_NV: Self = Self(0);
    pub const NEGATIVE_X_NV: Self = Self(1);
    pub const POSITIVE_Y_NV: Self = Self(2);
    pub const NEGATIVE_Y_NV: Self = Self(3);
    pub const POSITIVE_Z_NV: Self = Self(4);
    pub const NEGATIVE_Z_NV: Self = Self(5);
    pub const POSITIVE_W_NV: Self = Self(6);
    pub const NEGATIVE_W_NV: Self = Self(7);
}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportSwizzleStateCreateInfoNV> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportSwizzleStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViewportSwizzleNV.html)) · Structure <br/> VkViewportSwizzleNV - Structure specifying a viewport swizzle\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ViewportSwizzleNV`] structure is defined as:\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef struct VkViewportSwizzleNV {\n    VkViewportCoordinateSwizzleNV    x;\n    VkViewportCoordinateSwizzleNV    y;\n    VkViewportCoordinateSwizzleNV    z;\n    VkViewportCoordinateSwizzleNV    w;\n} VkViewportSwizzleNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::x`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the x component of the primitive\n\n* [`Self::y`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the y component of the primitive\n\n* [`Self::z`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the z component of the primitive\n\n* [`Self::w`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the w component of the primitive\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkViewportSwizzleNV-x-parameter  \n  [`Self::x`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-y-parameter  \n  [`Self::y`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-z-parameter  \n  [`Self::z`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-w-parameter  \n  [`Self::w`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`], [`crate::vk::ViewportCoordinateSwizzleNV`]\n"]
#[doc(alias = "VkViewportSwizzleNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ViewportSwizzleNV {
    pub x: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV,
    pub y: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV,
    pub z: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV,
    pub w: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV,
}
impl Default for ViewportSwizzleNV {
    fn default() -> Self {
        Self { x: Default::default(), y: Default::default(), z: Default::default(), w: Default::default() }
    }
}
impl std::fmt::Debug for ViewportSwizzleNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ViewportSwizzleNV").field("x", &self.x).field("y", &self.y).field("z", &self.z).field("w", &self.w).finish()
    }
}
impl ViewportSwizzleNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ViewportSwizzleNVBuilder<'a> {
        ViewportSwizzleNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViewportSwizzleNV.html)) · Builder of [`ViewportSwizzleNV`] <br/> VkViewportSwizzleNV - Structure specifying a viewport swizzle\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ViewportSwizzleNV`] structure is defined as:\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef struct VkViewportSwizzleNV {\n    VkViewportCoordinateSwizzleNV    x;\n    VkViewportCoordinateSwizzleNV    y;\n    VkViewportCoordinateSwizzleNV    z;\n    VkViewportCoordinateSwizzleNV    w;\n} VkViewportSwizzleNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::x`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the x component of the primitive\n\n* [`Self::y`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the y component of the primitive\n\n* [`Self::z`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the z component of the primitive\n\n* [`Self::w`] is a [`crate::vk::ViewportCoordinateSwizzleNV`] value specifying the\n  swizzle operation to apply to the w component of the primitive\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkViewportSwizzleNV-x-parameter  \n  [`Self::x`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-y-parameter  \n  [`Self::y`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-z-parameter  \n  [`Self::z`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n\n* []() VUID-VkViewportSwizzleNV-w-parameter  \n  [`Self::w`] **must** be a valid [`crate::vk::ViewportCoordinateSwizzleNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`], [`crate::vk::ViewportCoordinateSwizzleNV`]\n"]
#[repr(transparent)]
pub struct ViewportSwizzleNVBuilder<'a>(ViewportSwizzleNV, std::marker::PhantomData<&'a ()>);
impl<'a> ViewportSwizzleNVBuilder<'a> {
    #[inline]
    pub fn new() -> ViewportSwizzleNVBuilder<'a> {
        ViewportSwizzleNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn x(mut self, x: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV) -> Self {
        self.0.x = x as _;
        self
    }
    #[inline]
    pub fn y(mut self, y: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV) -> Self {
        self.0.y = y as _;
        self
    }
    #[inline]
    pub fn z(mut self, z: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV) -> Self {
        self.0.z = z as _;
        self
    }
    #[inline]
    pub fn w(mut self, w: crate::extensions::nv_viewport_swizzle::ViewportCoordinateSwizzleNV) -> Self {
        self.0.w = w as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ViewportSwizzleNV {
        self.0
    }
}
impl<'a> std::default::Default for ViewportSwizzleNVBuilder<'a> {
    fn default() -> ViewportSwizzleNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ViewportSwizzleNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ViewportSwizzleNVBuilder<'a> {
    type Target = ViewportSwizzleNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ViewportSwizzleNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportSwizzleStateCreateInfoNV.html)) · Structure <br/> VkPipelineViewportSwizzleStateCreateInfoNV - Structure specifying swizzle applied to primitive clip coordinates\n[](#_c_specification)C Specification\n----------\n\nEach primitive sent to a given viewport has a swizzle and **optional** negation\napplied to its clip coordinates.\nThe swizzle that is applied depends on the viewport index, and is controlled\nby the [`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`] pipeline state:\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef struct VkPipelineViewportSwizzleStateCreateInfoNV {\n    VkStructureType                                sType;\n    const void*                                    pNext;\n    VkPipelineViewportSwizzleStateCreateFlagsNV    flags;\n    uint32_t                                       viewportCount;\n    const VkViewportSwizzleNV*                     pViewportSwizzles;\n} VkPipelineViewportSwizzleStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::viewport_count`] is the number of viewport swizzles used by the\n  pipeline.\n\n* [`Self::p_viewport_swizzles`] is a pointer to an array of[`crate::vk::ViewportSwizzleNV`] structures, defining the viewport swizzles.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-viewportCount-01215  \n  [`Self::viewport_count`] **must** be greater than or equal to the[`Self::viewport_count`] set in [`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-pViewportSwizzles-parameter  \n  [`Self::p_viewport_swizzles`] **must** be a valid pointer to an array of [`Self::viewport_count`] valid [`crate::vk::ViewportSwizzleNV`] structures\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-viewportCount-arraylength  \n  [`Self::viewport_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateFlagBitsNV`], [`crate::vk::StructureType`], [`crate::vk::ViewportSwizzleNV`]\n"]
#[doc(alias = "VkPipelineViewportSwizzleStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineViewportSwizzleStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::nv_viewport_swizzle::PipelineViewportSwizzleStateCreateFlagsNV,
    pub viewport_count: u32,
    pub p_viewport_swizzles: *const crate::extensions::nv_viewport_swizzle::ViewportSwizzleNV,
}
impl PipelineViewportSwizzleStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV;
}
impl Default for PipelineViewportSwizzleStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), viewport_count: Default::default(), p_viewport_swizzles: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineViewportSwizzleStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineViewportSwizzleStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("viewport_count", &self.viewport_count).field("p_viewport_swizzles", &self.p_viewport_swizzles).finish()
    }
}
impl PipelineViewportSwizzleStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
        PipelineViewportSwizzleStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportSwizzleStateCreateInfoNV.html)) · Builder of [`PipelineViewportSwizzleStateCreateInfoNV`] <br/> VkPipelineViewportSwizzleStateCreateInfoNV - Structure specifying swizzle applied to primitive clip coordinates\n[](#_c_specification)C Specification\n----------\n\nEach primitive sent to a given viewport has a swizzle and **optional** negation\napplied to its clip coordinates.\nThe swizzle that is applied depends on the viewport index, and is controlled\nby the [`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`] pipeline state:\n\n```\n// Provided by VK_NV_viewport_swizzle\ntypedef struct VkPipelineViewportSwizzleStateCreateInfoNV {\n    VkStructureType                                sType;\n    const void*                                    pNext;\n    VkPipelineViewportSwizzleStateCreateFlagsNV    flags;\n    uint32_t                                       viewportCount;\n    const VkViewportSwizzleNV*                     pViewportSwizzles;\n} VkPipelineViewportSwizzleStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::viewport_count`] is the number of viewport swizzles used by the\n  pipeline.\n\n* [`Self::p_viewport_swizzles`] is a pointer to an array of[`crate::vk::ViewportSwizzleNV`] structures, defining the viewport swizzles.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-viewportCount-01215  \n  [`Self::viewport_count`] **must** be greater than or equal to the[`Self::viewport_count`] set in [`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-pViewportSwizzles-parameter  \n  [`Self::p_viewport_swizzles`] **must** be a valid pointer to an array of [`Self::viewport_count`] valid [`crate::vk::ViewportSwizzleNV`] structures\n\n* []() VUID-VkPipelineViewportSwizzleStateCreateInfoNV-viewportCount-arraylength  \n  [`Self::viewport_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportSwizzleStateCreateFlagBitsNV`], [`crate::vk::StructureType`], [`crate::vk::ViewportSwizzleNV`]\n"]
#[repr(transparent)]
pub struct PipelineViewportSwizzleStateCreateInfoNVBuilder<'a>(PipelineViewportSwizzleStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
        PipelineViewportSwizzleStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_viewport_swizzle::PipelineViewportSwizzleStateCreateFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn viewport_swizzles(mut self, viewport_swizzles: &'a [crate::extensions::nv_viewport_swizzle::ViewportSwizzleNVBuilder]) -> Self {
        self.0.p_viewport_swizzles = viewport_swizzles.as_ptr() as _;
        self.0.viewport_count = viewport_swizzles.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineViewportSwizzleStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
    type Target = PipelineViewportSwizzleStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineViewportSwizzleStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
