#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VALIDATION_FEATURES_SPEC_VERSION")]
pub const EXT_VALIDATION_FEATURES_SPEC_VERSION: u32 = 5;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME")]
pub const EXT_VALIDATION_FEATURES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_validation_features");
#[doc = "Provided by [`crate::extensions::ext_validation_features`]"]
impl crate::vk1_0::StructureType {
    pub const VALIDATION_FEATURES_EXT: Self = Self(1000247000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkValidationFeatureEnableEXT.html)) · Enum <br/> VkValidationFeatureEnableEXT - Specify validation features to enable\n[](#_c_specification)C Specification\n----------\n\nPossible values of elements of the[`crate::vk::ValidationFeaturesEXT::p_enabled_validation_features`] array,\nspecifying validation features to be enabled, are:\n\n```\n// Provided by VK_EXT_validation_features\ntypedef enum VkValidationFeatureEnableEXT {\n    VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT = 0,\n    VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT = 1,\n    VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT = 2,\n    VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT = 3,\n    VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT = 4,\n} VkValidationFeatureEnableEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::GPU_ASSISTED_EXT`] specifies that\n  GPU-assisted validation is enabled.\n  Activating this feature instruments shader programs to generate\n  additional diagnostic data.\n  This feature is disabled by default.\n\n* [`Self::GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT`]specifies that the validation layers reserve a descriptor set binding\n  slot for their own use.\n  The layer reports a value for[`crate::vk::PhysicalDeviceLimits::max_bound_descriptor_sets`] that is one\n  less than the value reported by the device.\n  If the device supports the binding of only one descriptor set, the\n  validation layer does not perform GPU-assisted validation.\n  This feature is disabled by default.\n\n* [`Self::BEST_PRACTICES_EXT`] specifies that\n  Vulkan best-practices validation is enabled.\n  Activating this feature enables the output of warnings related to common\n  misuse of the API, but which are not explicitly prohibited by the\n  specification.\n  This feature is disabled by default.\n\n* [`Self::DEBUG_PRINTF_EXT`] specifies that the\n  layers will process `debugPrintfEXT` operations in shaders and send\n  the resulting output to the debug callback.\n  This feature is disabled by default.\n\n* [`Self::SYNCHRONIZATION_VALIDATION_EXT`]specifies that Vulkan synchronization validation is enabled.\n  This feature reports resource access conflicts due to missing or\n  incorrect synchronization operations between actions (Draw, Copy,\n  Dispatch, Blit) reading or writing the same regions of memory.\n  This feature is disabled by default.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ValidationFeaturesEXT`]\n"]
#[doc(alias = "VkValidationFeatureEnableEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ValidationFeatureEnableEXT(pub i32);
impl std::fmt::Debug for ValidationFeatureEnableEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::GPU_ASSISTED_EXT => "GPU_ASSISTED_EXT",
            &Self::GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT => "GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT",
            &Self::BEST_PRACTICES_EXT => "BEST_PRACTICES_EXT",
            &Self::DEBUG_PRINTF_EXT => "DEBUG_PRINTF_EXT",
            &Self::SYNCHRONIZATION_VALIDATION_EXT => "SYNCHRONIZATION_VALIDATION_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_validation_features`]"]
impl crate::extensions::ext_validation_features::ValidationFeatureEnableEXT {
    pub const GPU_ASSISTED_EXT: Self = Self(0);
    pub const GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT: Self = Self(1);
    pub const BEST_PRACTICES_EXT: Self = Self(2);
    pub const DEBUG_PRINTF_EXT: Self = Self(3);
    pub const SYNCHRONIZATION_VALIDATION_EXT: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkValidationFeatureDisableEXT.html)) · Enum <br/> VkValidationFeatureDisableEXT - Specify validation features to disable\n[](#_c_specification)C Specification\n----------\n\nPossible values of elements of the[`crate::vk::ValidationFeaturesEXT::p_disabled_validation_features`] array,\nspecifying validation features to be disabled, are:\n\n```\n// Provided by VK_EXT_validation_features\ntypedef enum VkValidationFeatureDisableEXT {\n    VK_VALIDATION_FEATURE_DISABLE_ALL_EXT = 0,\n    VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT = 1,\n    VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT = 2,\n    VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT = 3,\n    VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT = 4,\n    VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT = 5,\n    VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT = 6,\n    VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT = 7,\n} VkValidationFeatureDisableEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::ALL_EXT`] specifies that all\n  validation checks are disabled.\n\n* [`Self::SHADERS_EXT`] specifies that shader\n  validation is disabled.\n  This feature is enabled by default.\n\n* [`Self::THREAD_SAFETY_EXT`] specifies that\n  thread safety validation is disabled.\n  This feature is enabled by default.\n\n* [`Self::API_PARAMETERS_EXT`] specifies that\n  stateless parameter validation is disabled.\n  This feature is enabled by default.\n\n* [`Self::OBJECT_LIFETIMES_EXT`] specifies that\n  object lifetime validation is disabled.\n  This feature is enabled by default.\n\n* [`Self::CORE_CHECKS_EXT`] specifies that core\n  validation checks are disabled.\n  This feature is enabled by default.\n  If this feature is disabled, the shader validation and GPU-assisted\n  validation features are also disabled.\n\n* [`Self::UNIQUE_HANDLES_EXT`] specifies that\n  protection against duplicate non-dispatchable object handles is\n  disabled.\n  This feature is enabled by default.\n\n* [`Self::SHADER_VALIDATION_CACHE_EXT`]specifies that there will be no caching of shader validation results and\n  every shader will be validated on every application execution.\n  Shader validation caching is enabled by default.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ValidationFeaturesEXT`]\n"]
#[doc(alias = "VkValidationFeatureDisableEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ValidationFeatureDisableEXT(pub i32);
impl std::fmt::Debug for ValidationFeatureDisableEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::ALL_EXT => "ALL_EXT",
            &Self::SHADERS_EXT => "SHADERS_EXT",
            &Self::THREAD_SAFETY_EXT => "THREAD_SAFETY_EXT",
            &Self::API_PARAMETERS_EXT => "API_PARAMETERS_EXT",
            &Self::OBJECT_LIFETIMES_EXT => "OBJECT_LIFETIMES_EXT",
            &Self::CORE_CHECKS_EXT => "CORE_CHECKS_EXT",
            &Self::UNIQUE_HANDLES_EXT => "UNIQUE_HANDLES_EXT",
            &Self::SHADER_VALIDATION_CACHE_EXT => "SHADER_VALIDATION_CACHE_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_validation_features`]"]
impl crate::extensions::ext_validation_features::ValidationFeatureDisableEXT {
    pub const ALL_EXT: Self = Self(0);
    pub const SHADERS_EXT: Self = Self(1);
    pub const THREAD_SAFETY_EXT: Self = Self(2);
    pub const API_PARAMETERS_EXT: Self = Self(3);
    pub const OBJECT_LIFETIMES_EXT: Self = Self(4);
    pub const CORE_CHECKS_EXT: Self = Self(5);
    pub const UNIQUE_HANDLES_EXT: Self = Self(6);
    pub const SHADER_VALIDATION_CACHE_EXT: Self = Self(7);
}
impl<'a> crate::ExtendableFromConst<'a, ValidationFeaturesEXT> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ValidationFeaturesEXTBuilder<'_>> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkValidationFeaturesEXT.html)) · Structure <br/> VkValidationFeaturesEXT - Specify validation features to enable or disable for a Vulkan instance\n[](#_c_specification)C Specification\n----------\n\nWhen creating a Vulkan instance for which you wish to enable or disable\nspecific validation features, add a [`crate::vk::ValidationFeaturesEXT`] structure\nto the [`Self::p_next`] chain of the [`crate::vk::InstanceCreateInfo`] structure,\nspecifying the features to be enabled or disabled.\n\n```\n// Provided by VK_EXT_validation_features\ntypedef struct VkValidationFeaturesEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    uint32_t                                enabledValidationFeatureCount;\n    const VkValidationFeatureEnableEXT*     pEnabledValidationFeatures;\n    uint32_t                                disabledValidationFeatureCount;\n    const VkValidationFeatureDisableEXT*    pDisabledValidationFeatures;\n} VkValidationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::enabled_validation_feature_count`] is the number of features to enable.\n\n* [`Self::p_enabled_validation_features`] is a pointer to an array of[`crate::vk::ValidationFeatureEnableEXT`] values specifying the validation\n  features to be enabled.\n\n* [`Self::disabled_validation_feature_count`] is the number of features to\n  disable.\n\n* [`Self::p_disabled_validation_features`] is a pointer to an array of[`crate::vk::ValidationFeatureDisableEXT`] values specifying the validation\n  features to be disabled.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-02967  \n   If the [`Self::p_enabled_validation_features`] array contains[`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT`],\n  then it **must** also contain[`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_EXT`]\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-02968  \n   If the [`Self::p_enabled_validation_features`] array contains[`crate::vk::ValidationFeatureEnableEXT::DEBUG_PRINTF_EXT`], then it **must** not\n  contain [`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkValidationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VALIDATION_FEATURES_EXT`]\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-parameter  \n   If [`Self::enabled_validation_feature_count`] is not `0`, [`Self::p_enabled_validation_features`] **must** be a valid pointer to an array of [`Self::enabled_validation_feature_count`] valid [`crate::vk::ValidationFeatureEnableEXT`] values\n\n* []() VUID-VkValidationFeaturesEXT-pDisabledValidationFeatures-parameter  \n   If [`Self::disabled_validation_feature_count`] is not `0`, [`Self::p_disabled_validation_features`] **must** be a valid pointer to an array of [`Self::disabled_validation_feature_count`] valid [`crate::vk::ValidationFeatureDisableEXT`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ValidationFeatureDisableEXT`], [`crate::vk::ValidationFeatureEnableEXT`]\n"]
#[doc(alias = "VkValidationFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ValidationFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub enabled_validation_feature_count: u32,
    pub p_enabled_validation_features: *const crate::extensions::ext_validation_features::ValidationFeatureEnableEXT,
    pub disabled_validation_feature_count: u32,
    pub p_disabled_validation_features: *const crate::extensions::ext_validation_features::ValidationFeatureDisableEXT,
}
impl ValidationFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VALIDATION_FEATURES_EXT;
}
impl Default for ValidationFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), enabled_validation_feature_count: Default::default(), p_enabled_validation_features: std::ptr::null(), disabled_validation_feature_count: Default::default(), p_disabled_validation_features: std::ptr::null() }
    }
}
impl std::fmt::Debug for ValidationFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ValidationFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("enabled_validation_feature_count", &self.enabled_validation_feature_count).field("p_enabled_validation_features", &self.p_enabled_validation_features).field("disabled_validation_feature_count", &self.disabled_validation_feature_count).field("p_disabled_validation_features", &self.p_disabled_validation_features).finish()
    }
}
impl ValidationFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ValidationFeaturesEXTBuilder<'a> {
        ValidationFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkValidationFeaturesEXT.html)) · Builder of [`ValidationFeaturesEXT`] <br/> VkValidationFeaturesEXT - Specify validation features to enable or disable for a Vulkan instance\n[](#_c_specification)C Specification\n----------\n\nWhen creating a Vulkan instance for which you wish to enable or disable\nspecific validation features, add a [`crate::vk::ValidationFeaturesEXT`] structure\nto the [`Self::p_next`] chain of the [`crate::vk::InstanceCreateInfo`] structure,\nspecifying the features to be enabled or disabled.\n\n```\n// Provided by VK_EXT_validation_features\ntypedef struct VkValidationFeaturesEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    uint32_t                                enabledValidationFeatureCount;\n    const VkValidationFeatureEnableEXT*     pEnabledValidationFeatures;\n    uint32_t                                disabledValidationFeatureCount;\n    const VkValidationFeatureDisableEXT*    pDisabledValidationFeatures;\n} VkValidationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::enabled_validation_feature_count`] is the number of features to enable.\n\n* [`Self::p_enabled_validation_features`] is a pointer to an array of[`crate::vk::ValidationFeatureEnableEXT`] values specifying the validation\n  features to be enabled.\n\n* [`Self::disabled_validation_feature_count`] is the number of features to\n  disable.\n\n* [`Self::p_disabled_validation_features`] is a pointer to an array of[`crate::vk::ValidationFeatureDisableEXT`] values specifying the validation\n  features to be disabled.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-02967  \n   If the [`Self::p_enabled_validation_features`] array contains[`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT`],\n  then it **must** also contain[`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_EXT`]\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-02968  \n   If the [`Self::p_enabled_validation_features`] array contains[`crate::vk::ValidationFeatureEnableEXT::DEBUG_PRINTF_EXT`], then it **must** not\n  contain [`crate::vk::ValidationFeatureEnableEXT::GPU_ASSISTED_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkValidationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VALIDATION_FEATURES_EXT`]\n\n* []() VUID-VkValidationFeaturesEXT-pEnabledValidationFeatures-parameter  \n   If [`Self::enabled_validation_feature_count`] is not `0`, [`Self::p_enabled_validation_features`] **must** be a valid pointer to an array of [`Self::enabled_validation_feature_count`] valid [`crate::vk::ValidationFeatureEnableEXT`] values\n\n* []() VUID-VkValidationFeaturesEXT-pDisabledValidationFeatures-parameter  \n   If [`Self::disabled_validation_feature_count`] is not `0`, [`Self::p_disabled_validation_features`] **must** be a valid pointer to an array of [`Self::disabled_validation_feature_count`] valid [`crate::vk::ValidationFeatureDisableEXT`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ValidationFeatureDisableEXT`], [`crate::vk::ValidationFeatureEnableEXT`]\n"]
#[repr(transparent)]
pub struct ValidationFeaturesEXTBuilder<'a>(ValidationFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ValidationFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ValidationFeaturesEXTBuilder<'a> {
        ValidationFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn enabled_validation_features(mut self, enabled_validation_features: &'a [crate::extensions::ext_validation_features::ValidationFeatureEnableEXT]) -> Self {
        self.0.p_enabled_validation_features = enabled_validation_features.as_ptr() as _;
        self.0.enabled_validation_feature_count = enabled_validation_features.len() as _;
        self
    }
    #[inline]
    pub fn disabled_validation_features(mut self, disabled_validation_features: &'a [crate::extensions::ext_validation_features::ValidationFeatureDisableEXT]) -> Self {
        self.0.p_disabled_validation_features = disabled_validation_features.as_ptr() as _;
        self.0.disabled_validation_feature_count = disabled_validation_features.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ValidationFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for ValidationFeaturesEXTBuilder<'a> {
    fn default() -> ValidationFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ValidationFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ValidationFeaturesEXTBuilder<'a> {
    type Target = ValidationFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ValidationFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
