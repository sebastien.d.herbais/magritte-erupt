//! ## Versioning Warning ⚠️
//!
//! This is a Vulkan **provisional/beta** extension and **must** be used with
//! caution. Its API/behaviour has not been finalized yet and _may_ therefore
//! change in ways that break backwards compatibility between revisions, and
//! before final release of a non-provisional version of this extension.
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_VIDEO_QUEUE_SPEC_VERSION")]
pub const KHR_VIDEO_QUEUE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_VIDEO_QUEUE_EXTENSION_NAME")]
pub const KHR_VIDEO_QUEUE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_video_queue");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_VIDEO_CAPABILITIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceVideoCapabilitiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_VIDEO_FORMAT_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceVideoFormatPropertiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_VIDEO_SESSION_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateVideoSessionKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_VIDEO_SESSION_KHR: *const std::os::raw::c_char = crate::cstr!("vkDestroyVideoSessionKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_VIDEO_SESSION_PARAMETERS_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateVideoSessionParametersKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_UPDATE_VIDEO_SESSION_PARAMETERS_KHR: *const std::os::raw::c_char = crate::cstr!("vkUpdateVideoSessionParametersKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_VIDEO_SESSION_PARAMETERS_KHR: *const std::os::raw::c_char = crate::cstr!("vkDestroyVideoSessionParametersKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_VIDEO_SESSION_MEMORY_REQUIREMENTS_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetVideoSessionMemoryRequirementsKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_BIND_VIDEO_SESSION_MEMORY_KHR: *const std::os::raw::c_char = crate::cstr!("vkBindVideoSessionMemoryKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BEGIN_VIDEO_CODING_KHR: *const std::os::raw::c_char = crate::cstr!("vkCmdBeginVideoCodingKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_CONTROL_VIDEO_CODING_KHR: *const std::os::raw::c_char = crate::cstr!("vkCmdControlVideoCodingKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_END_VIDEO_CODING_KHR: *const std::os::raw::c_char = crate::cstr!("vkCmdEndVideoCodingKHR");
crate::non_dispatchable_handle!(VideoSessionKHR, VIDEO_SESSION_KHR, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionKHR.html)) · Non-dispatchable Handle <br/> VkVideoSessionKHR - Opaque handle to a video session object\n[](#_c_specification)C Specification\n----------\n\nVideo session objects are abstracted and represented by[`crate::vk::VideoSessionKHR`] handles:\n\n```\n// Provided by VK_KHR_video_queue\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkVideoSessionKHR)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoBeginCodingInfoKHR`], [`crate::vk::VideoSessionParametersCreateInfoKHR`], [`crate::vk::DeviceLoader::bind_video_session_memory_khr`], [`crate::vk::DeviceLoader::create_video_session_khr`], [`crate::vk::DeviceLoader::destroy_video_session_khr`], [`crate::vk::DeviceLoader::get_video_session_memory_requirements_khr`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkVideoSessionKHR)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkVideoSessionKHR");
crate::non_dispatchable_handle!(VideoSessionParametersKHR, VIDEO_SESSION_PARAMETERS_KHR, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionParametersKHR.html)) · Non-dispatchable Handle <br/> VkVideoSessionParametersKHR - Opaque handle to a video video session parameters object\n[](#_c_specification)C Specification\n----------\n\nVideo session parameter objects are represented by[`crate::vk::VideoSessionParametersKHR`] handles:\n\n```\n// Provided by VK_KHR_video_queue\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkVideoSessionParametersKHR)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoBeginCodingInfoKHR`], [`crate::vk::VideoSessionParametersCreateInfoKHR`], [`crate::vk::DeviceLoader::create_video_session_parameters_khr`], [`crate::vk::DeviceLoader::destroy_video_session_parameters_khr`], [`crate::vk::DeviceLoader::update_video_session_parameters_khr`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkVideoSessionParametersKHR)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkVideoSessionParametersKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoBeginCodingFlagsKHR.html)) · Bitmask of [`VideoBeginCodingFlagBitsKHR`] <br/> "] # [doc (alias = "VkVideoBeginCodingFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoBeginCodingFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`VideoBeginCodingFlagsKHR`] <br/> "]
#[doc(alias = "VkVideoBeginCodingFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoBeginCodingFlagBitsKHR(pub u32);
impl VideoBeginCodingFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoBeginCodingFlagsKHR {
        VideoBeginCodingFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoBeginCodingFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoEndCodingFlagsKHR.html)) · Bitmask of [`VideoEndCodingFlagBitsKHR`] <br/> "] # [doc (alias = "VkVideoEndCodingFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoEndCodingFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`VideoEndCodingFlagsKHR`] <br/> "]
#[doc(alias = "VkVideoEndCodingFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoEndCodingFlagBitsKHR(pub u32);
impl VideoEndCodingFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoEndCodingFlagsKHR {
        VideoEndCodingFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoEndCodingFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::vk1_0::QueryResultFlagBits {
    pub const WITH_STATUS_KHR: Self = Self(16);
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::vk1_0::QueryType {
    pub const RESULT_STATUS_ONLY_KHR: Self = Self(1000023000);
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::vk1_0::StructureType {
    pub const VIDEO_PROFILE_KHR: Self = Self(1000023000);
    pub const VIDEO_CAPABILITIES_KHR: Self = Self(1000023001);
    pub const VIDEO_PICTURE_RESOURCE_KHR: Self = Self(1000023002);
    pub const VIDEO_GET_MEMORY_PROPERTIES_KHR: Self = Self(1000023003);
    pub const VIDEO_BIND_MEMORY_KHR: Self = Self(1000023004);
    pub const VIDEO_SESSION_CREATE_INFO_KHR: Self = Self(1000023005);
    pub const VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR: Self = Self(1000023006);
    pub const VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR: Self = Self(1000023007);
    pub const VIDEO_BEGIN_CODING_INFO_KHR: Self = Self(1000023008);
    pub const VIDEO_END_CODING_INFO_KHR: Self = Self(1000023009);
    pub const VIDEO_CODING_CONTROL_INFO_KHR: Self = Self(1000023010);
    pub const VIDEO_REFERENCE_SLOT_KHR: Self = Self(1000023011);
    pub const VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR: Self = Self(1000023012);
    pub const VIDEO_PROFILES_KHR: Self = Self(1000023013);
    pub const PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR: Self = Self(1000023014);
    pub const VIDEO_FORMAT_PROPERTIES_KHR: Self = Self(1000023015);
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::vk1_0::ObjectType {
    pub const VIDEO_SESSION_KHR: Self = Self(1000023000);
    pub const VIDEO_SESSION_PARAMETERS_KHR: Self = Self(1000023001);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagsKHR.html)) · Bitmask of [`VideoCodecOperationFlagBitsKHR`] <br/> VkVideoCodecOperationFlagsKHR - Bitmask specifying the Video codec operations supported\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoCodecOperationFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoCodecOperationFlagBitsKHR`] is a bitmask type for setting a mask of\nzero or more [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html), [`crate::vk::VideoQueueFamilyProperties2KHR`]\n"] # [doc (alias = "VkVideoCodecOperationFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoCodecOperationFlagsKHR : u32 { const INVALID_KHR = VideoCodecOperationFlagBitsKHR :: INVALID_KHR . 0 ; const ENCODE_H264_EXT = VideoCodecOperationFlagBitsKHR :: ENCODE_H264_EXT . 0 ; const DECODE_H264_EXT = VideoCodecOperationFlagBitsKHR :: DECODE_H264_EXT . 0 ; const DECODE_H265_EXT = VideoCodecOperationFlagBitsKHR :: DECODE_H265_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html)) · Bits enum of [`VideoCodecOperationFlagsKHR`] <br/> VkVideoCodecOperationFlagBitsKHR - Video codec operation types\n[](#_c_specification)C Specification\n----------\n\nThe codec operations are defined with the[VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) enum:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoCodecOperationFlagBitsKHR {\n    VK_VIDEO_CODEC_OPERATION_INVALID_BIT_KHR = 0,\n#ifdef VK_ENABLE_BETA_EXTENSIONS\n  // Provided by VK_EXT_video_encode_h264\n    VK_VIDEO_CODEC_OPERATION_ENCODE_H264_BIT_EXT = 0x00010000,\n#endif\n#ifdef VK_ENABLE_BETA_EXTENSIONS\n  // Provided by VK_EXT_video_decode_h264\n    VK_VIDEO_CODEC_OPERATION_DECODE_H264_BIT_EXT = 0x00000001,\n#endif\n#ifdef VK_ENABLE_BETA_EXTENSIONS\n  // Provided by VK_EXT_video_decode_h265\n    VK_VIDEO_CODEC_OPERATION_DECODE_H265_BIT_EXT = 0x00000002,\n#endif\n} VkVideoCodecOperationFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\nEach decode or encode codec-specific extension extends this enumeration with\nthe appropriate bit corresponding to the extension’s codec operation:\n\n* [`Self::INVALID_KHR`] - No video operations are\n  supported for this queue family.\n\n* [`Self::ENCODE_H264_EXT`] - H.264 video encode\n  operations are supported by this queue family.\n\n* [`Self::DECODE_H264_EXT`] - H.264 video decode\n  operations are supported by this queue family.\n\n* [`Self::DECODE_H265_EXT`] - H.265 video decode\n  operations are supported by this queue family.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoCodecOperationFlagBitsKHR`], [`crate::vk::VideoProfileKHR`]\n"]
#[doc(alias = "VkVideoCodecOperationFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoCodecOperationFlagBitsKHR(pub u32);
impl VideoCodecOperationFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoCodecOperationFlagsKHR {
        VideoCodecOperationFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoCodecOperationFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INVALID_KHR => "INVALID_KHR",
            &Self::ENCODE_H264_EXT => "ENCODE_H264_EXT",
            &Self::DECODE_H264_EXT => "DECODE_H264_EXT",
            &Self::DECODE_H265_EXT => "DECODE_H265_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoCodecOperationFlagBitsKHR {
    pub const INVALID_KHR: Self = Self(0);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagsKHR.html)) · Bitmask of [`VideoChromaSubsamplingFlagBitsKHR`] <br/> VkVideoChromaSubsamplingFlagsKHR - Bitmask specifying the Video format chroma subsampling\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoChromaSubsamplingFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoChromaSubsamplingFlagBitsKHR`] is a bitmask type for setting a mask\nof zero or more [VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html), [`crate::vk::VideoProfileKHR`]\n"] # [doc (alias = "VkVideoChromaSubsamplingFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoChromaSubsamplingFlagsKHR : u32 { const INVALID_KHR = VideoChromaSubsamplingFlagBitsKHR :: INVALID_KHR . 0 ; const MONOCHROME_KHR = VideoChromaSubsamplingFlagBitsKHR :: MONOCHROME_KHR . 0 ; const _420_KHR = VideoChromaSubsamplingFlagBitsKHR :: _420_KHR . 0 ; const _422_KHR = VideoChromaSubsamplingFlagBitsKHR :: _422_KHR . 0 ; const _444_KHR = VideoChromaSubsamplingFlagBitsKHR :: _444_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html)) · Bits enum of [`VideoChromaSubsamplingFlagsKHR`] <br/> VkVideoChromaSubsamplingFlagBitsKHR - Video chroma subsampling\n[](#_c_specification)C Specification\n----------\n\nThe video format chroma subsampling is defined with the following enums:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoChromaSubsamplingFlagBitsKHR {\n    VK_VIDEO_CHROMA_SUBSAMPLING_INVALID_BIT_KHR = 0,\n    VK_VIDEO_CHROMA_SUBSAMPLING_MONOCHROME_BIT_KHR = 0x00000001,\n    VK_VIDEO_CHROMA_SUBSAMPLING_420_BIT_KHR = 0x00000002,\n    VK_VIDEO_CHROMA_SUBSAMPLING_422_BIT_KHR = 0x00000004,\n    VK_VIDEO_CHROMA_SUBSAMPLING_444_BIT_KHR = 0x00000008,\n} VkVideoChromaSubsamplingFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::MONOCHROME_KHR`] - the format is\n  monochrome.\n\n* [`Self::_420_KHR`] - the format is 4:2:0\n  chroma subsampled.\n  The two chroma components are each subsampled at a factor of 2 both\n  horizontally and vertically.\n\n* [`Self::_422_KHR`] - the format is 4:2:2\n  chroma subsampled.\n  The two chroma components are sampled at half the sample rate of luma.\n  The horizontal chroma resolution is halved.\n\n* [`Self::_444_KHR`] - the format is 4:4:4\n  chroma sampled.\n  Each of the three YCbCr components have the same sample rate, thus there\n  is no chroma subsampling.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoChromaSubsamplingFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoChromaSubsamplingFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoChromaSubsamplingFlagBitsKHR(pub u32);
impl VideoChromaSubsamplingFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoChromaSubsamplingFlagsKHR {
        VideoChromaSubsamplingFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoChromaSubsamplingFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INVALID_KHR => "INVALID_KHR",
            &Self::MONOCHROME_KHR => "MONOCHROME_KHR",
            &Self::_420_KHR => "_420_KHR",
            &Self::_422_KHR => "_422_KHR",
            &Self::_444_KHR => "_444_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoChromaSubsamplingFlagBitsKHR {
    pub const INVALID_KHR: Self = Self(0);
    pub const MONOCHROME_KHR: Self = Self(1);
    pub const _420_KHR: Self = Self(2);
    pub const _422_KHR: Self = Self(4);
    pub const _444_KHR: Self = Self(8);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagsKHR.html)) · Bitmask of [`VideoComponentBitDepthFlagBitsKHR`] <br/> VkVideoComponentBitDepthFlagsKHR - Bitmask specifying the Video format component bit depth\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoComponentBitDepthFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoComponentBitDepthFlagBitsKHR`] is a bitmask type for setting a mask\nof zero or more [VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html), [`crate::vk::VideoProfileKHR`]\n"] # [doc (alias = "VkVideoComponentBitDepthFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoComponentBitDepthFlagsKHR : u32 { const VIDEO_COMPONENT_DEPTH_INVALID_KHR = VideoComponentBitDepthFlagBitsKHR :: VIDEO_COMPONENT_DEPTH_INVALID_KHR . 0 ; const VIDEO_COMPONENT_DEPTH_8_KHR = VideoComponentBitDepthFlagBitsKHR :: VIDEO_COMPONENT_DEPTH_8_KHR . 0 ; const VIDEO_COMPONENT_DEPTH_10_KHR = VideoComponentBitDepthFlagBitsKHR :: VIDEO_COMPONENT_DEPTH_10_KHR . 0 ; const VIDEO_COMPONENT_DEPTH_12_KHR = VideoComponentBitDepthFlagBitsKHR :: VIDEO_COMPONENT_DEPTH_12_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html)) · Bits enum of [`VideoComponentBitDepthFlagsKHR`] <br/> VkVideoComponentBitDepthFlagBitsKHR - Video component bit depth\n[](#_c_specification)C Specification\n----------\n\nThe video format component bit depth is defined with the following enums:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoComponentBitDepthFlagBitsKHR {\n    VK_VIDEO_COMPONENT_BIT_DEPTH_INVALID_KHR = 0,\n    VK_VIDEO_COMPONENT_BIT_DEPTH_8_BIT_KHR = 0x00000001,\n    VK_VIDEO_COMPONENT_BIT_DEPTH_10_BIT_KHR = 0x00000004,\n    VK_VIDEO_COMPONENT_BIT_DEPTH_12_BIT_KHR = 0x00000010,\n} VkVideoComponentBitDepthFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::VIDEO_COMPONENT_DEPTH_8_KHR`] - the format component bit\n  depth is 8 bits.\n\n* [`Self::VIDEO_COMPONENT_DEPTH_10_KHR`] - the format component bit\n  depth is 10 bits.\n\n* [`Self::VIDEO_COMPONENT_DEPTH_12_KHR`] - the format component bit\n  depth is 12 bits.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoComponentBitDepthFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoComponentBitDepthFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoComponentBitDepthFlagBitsKHR(pub u32);
impl VideoComponentBitDepthFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoComponentBitDepthFlagsKHR {
        VideoComponentBitDepthFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoComponentBitDepthFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VIDEO_COMPONENT_DEPTH_INVALID_KHR => "VIDEO_COMPONENT_DEPTH_INVALID_KHR",
            &Self::VIDEO_COMPONENT_DEPTH_8_KHR => "VIDEO_COMPONENT_DEPTH_8_KHR",
            &Self::VIDEO_COMPONENT_DEPTH_10_KHR => "VIDEO_COMPONENT_DEPTH_10_KHR",
            &Self::VIDEO_COMPONENT_DEPTH_12_KHR => "VIDEO_COMPONENT_DEPTH_12_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoComponentBitDepthFlagBitsKHR {
    pub const VIDEO_COMPONENT_DEPTH_INVALID_KHR: Self = Self(0);
    pub const VIDEO_COMPONENT_DEPTH_8_KHR: Self = Self(1);
    pub const VIDEO_COMPONENT_DEPTH_10_KHR: Self = Self(4);
    pub const VIDEO_COMPONENT_DEPTH_12_KHR: Self = Self(16);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagsKHR.html)) · Bitmask of [`VideoCapabilitiesFlagBitsKHR`] <br/> VkVideoCapabilitiesFlagsKHR - Bitmask specifying the Video Decode and Encode Capabilities Flags\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoCapabilitiesFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoCapabilitiesFlagBitsKHR`] is a bitmask type for setting a mask of\nzero or more [VkVideoCapabilitiesFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoCapabilitiesFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagBitsKHR.html), [`crate::vk::VideoCapabilitiesKHR`]\n"] # [doc (alias = "VkVideoCapabilitiesFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoCapabilitiesFlagsKHR : u32 { const PROTECTED_CONTENT_KHR = VideoCapabilitiesFlagBitsKHR :: PROTECTED_CONTENT_KHR . 0 ; const SEPARATE_REFERENCE_IMAGES_KHR = VideoCapabilitiesFlagBitsKHR :: SEPARATE_REFERENCE_IMAGES_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagBitsKHR.html)) · Bits enum of [`VideoCapabilitiesFlagsKHR`] <br/> VkVideoCapabilitiesFlagBitsKHR - Video Decode and Encode Capabilities Flags\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoCapabilitiesKHR`] flags are defined with the following\nenumeration:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoCapabilitiesFlagBitsKHR {\n    VK_VIDEO_CAPABILITIES_PROTECTED_CONTENT_BIT_KHR = 0x00000001,\n    VK_VIDEO_CAPABILITIES_SEPARATE_REFERENCE_IMAGES_BIT_KHR = 0x00000002,\n} VkVideoCapabilitiesFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::PROTECTED_CONTENT_KHR`] - the decode or\n  encode session supports protected content.\n\n* [`Self::SEPARATE_REFERENCE_IMAGES_KHR`] - the DPB\n  or Reconstructed Video Picture Resources for the video session **may** be\n  created as a separate [`crate::vk::Image`] for each DPB picture.\n  If not supported, the DPB **must** be created as single multi-layered image\n  where each layer represents one of the DPB Video Picture Resources.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoCapabilitiesFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoCapabilitiesFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoCapabilitiesFlagBitsKHR(pub u32);
impl VideoCapabilitiesFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoCapabilitiesFlagsKHR {
        VideoCapabilitiesFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoCapabilitiesFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::PROTECTED_CONTENT_KHR => "PROTECTED_CONTENT_KHR",
            &Self::SEPARATE_REFERENCE_IMAGES_KHR => "SEPARATE_REFERENCE_IMAGES_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoCapabilitiesFlagBitsKHR {
    pub const PROTECTED_CONTENT_KHR: Self = Self(1);
    pub const SEPARATE_REFERENCE_IMAGES_KHR: Self = Self(2);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagsKHR.html)) · Bitmask of [`VideoSessionCreateFlagBitsKHR`] <br/> VkVideoSessionCreateFlagsKHR - Bitmask specifying the video decode or encode video session creation flags\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoSessionCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoSessionCreateFlagBitsKHR`] is a bitmask type for setting a mask of\nzero or more [VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html), [`crate::vk::VideoSessionCreateInfoKHR`]\n"] # [doc (alias = "VkVideoSessionCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoSessionCreateFlagsKHR : u32 { const DEFAULT_KHR = VideoSessionCreateFlagBitsKHR :: DEFAULT_KHR . 0 ; const PROTECTED_CONTENT_KHR = VideoSessionCreateFlagBitsKHR :: PROTECTED_CONTENT_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html)) · Bits enum of [`VideoSessionCreateFlagsKHR`] <br/> VkVideoSessionCreateFlagBitsKHR - Video decode or encode video session creation flags\n[](#_c_specification)C Specification\n----------\n\nThe decode or encode session creation flags defined with the following\nenums:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoSessionCreateFlagBitsKHR {\n    VK_VIDEO_SESSION_CREATE_DEFAULT_KHR = 0,\n    VK_VIDEO_SESSION_CREATE_PROTECTED_CONTENT_BIT_KHR = 0x00000001,\n} VkVideoSessionCreateFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::PROTECTED_CONTENT_KHR`] - create the\n  video session for use with protected video content\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoSessionCreateFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoSessionCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoSessionCreateFlagBitsKHR(pub u32);
impl VideoSessionCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoSessionCreateFlagsKHR {
        VideoSessionCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoSessionCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_KHR => "DEFAULT_KHR",
            &Self::PROTECTED_CONTENT_KHR => "PROTECTED_CONTENT_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoSessionCreateFlagBitsKHR {
    pub const DEFAULT_KHR: Self = Self(0);
    pub const PROTECTED_CONTENT_KHR: Self = Self(1);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagsKHR.html)) · Bitmask of [`VideoCodingQualityPresetFlagBitsKHR`] <br/> VkVideoCodingQualityPresetFlagsKHR - Bitmask of flink:VkVideoCodingQualityPresetFlagBitsKHR flags\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoCodingQualityPresetFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoCodingQualityPresetFlagBitsKHR`] is a bitmask type for setting a\nmask of zero or more [VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoBeginCodingInfoKHR`], [VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html)\n"] # [doc (alias = "VkVideoCodingQualityPresetFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoCodingQualityPresetFlagsKHR : u32 { const DEFAULT_KHR = VideoCodingQualityPresetFlagBitsKHR :: DEFAULT_KHR . 0 ; const NORMAL_KHR = VideoCodingQualityPresetFlagBitsKHR :: NORMAL_KHR . 0 ; const POWER_KHR = VideoCodingQualityPresetFlagBitsKHR :: POWER_KHR . 0 ; const QUALITY_KHR = VideoCodingQualityPresetFlagBitsKHR :: QUALITY_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html)) · Bits enum of [`VideoCodingQualityPresetFlagsKHR`] <br/> VkVideoCodingQualityPresetFlagBitsKHR - Video codec profile types\n[](#_c_specification)C Specification\n----------\n\nThe decode preset types are defined with the following:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoCodingQualityPresetFlagBitsKHR {\n    VK_VIDEO_CODING_QUALITY_PRESET_DEFAULT_BIT_KHR = 0,\n    VK_VIDEO_CODING_QUALITY_PRESET_NORMAL_BIT_KHR = 0x00000001,\n    VK_VIDEO_CODING_QUALITY_PRESET_POWER_BIT_KHR = 0x00000002,\n    VK_VIDEO_CODING_QUALITY_PRESET_QUALITY_BIT_KHR = 0x00000004,\n} VkVideoCodingQualityPresetFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::NORMAL_KHR`] defines normal\n  decode case.\n\n* [`Self::POWER_KHR`] defines power\n  efficient case.\n\n* [`Self::QUALITY_KHR`] defines quality\n  focus case.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoCodingQualityPresetFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoCodingQualityPresetFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoCodingQualityPresetFlagBitsKHR(pub u32);
impl VideoCodingQualityPresetFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoCodingQualityPresetFlagsKHR {
        VideoCodingQualityPresetFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoCodingQualityPresetFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_KHR => "DEFAULT_KHR",
            &Self::NORMAL_KHR => "NORMAL_KHR",
            &Self::POWER_KHR => "POWER_KHR",
            &Self::QUALITY_KHR => "QUALITY_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoCodingQualityPresetFlagBitsKHR {
    pub const DEFAULT_KHR: Self = Self(0);
    pub const NORMAL_KHR: Self = Self(1);
    pub const POWER_KHR: Self = Self(2);
    pub const QUALITY_KHR: Self = Self(4);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagsKHR.html)) · Bitmask of [`VideoCodingControlFlagBitsKHR`] <br/> VkVideoCodingControlFlagsKHR - Bitmask specifying the Video Coding Control Command flink:vkCmdControlVideoCodingKHR flags\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_video_queue\ntypedef VkFlags VkVideoCodingControlFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::VideoCodingControlFlagBitsKHR`] is a bitmask type for setting a mask of\nzero or more [VkVideoCodingControlFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkVideoCodingControlFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagBitsKHR.html), [`crate::vk::VideoCodingControlInfoKHR`]\n"] # [doc (alias = "VkVideoCodingControlFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct VideoCodingControlFlagsKHR : u32 { const DEFAULT_KHR = VideoCodingControlFlagBitsKHR :: DEFAULT_KHR . 0 ; const RESET_KHR = VideoCodingControlFlagBitsKHR :: RESET_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagBitsKHR.html)) · Bits enum of [`VideoCodingControlFlagsKHR`] <br/> VkVideoCodingControlFlagBitsKHR - Video Coding Control Command Flags\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DeviceLoader::cmd_control_video_coding_khr`] flags are defined with the following\nenumeration:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkVideoCodingControlFlagBitsKHR {\n    VK_VIDEO_CODING_CONTROL_DEFAULT_KHR = 0,\n    VK_VIDEO_CODING_CONTROL_RESET_BIT_KHR = 0x00000001,\n} VkVideoCodingControlFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::RESET_KHR`] indicates a request for the\n  bound video session context to be reset to init state.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoCodingControlFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoCodingControlFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoCodingControlFlagBitsKHR(pub u32);
impl VideoCodingControlFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoCodingControlFlagsKHR {
        VideoCodingControlFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoCodingControlFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_KHR => "DEFAULT_KHR",
            &Self::RESET_KHR => "RESET_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::VideoCodingControlFlagBitsKHR {
    pub const DEFAULT_KHR: Self = Self(0);
    pub const RESET_KHR: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryResultStatusKHR.html)) · Enum <br/> VkQueryResultStatusKHR - Specific status codes for operations\n[](#_c_specification)C Specification\n----------\n\nSpecific status codes that **can** be returned from a query are:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef enum VkQueryResultStatusKHR {\n    VK_QUERY_RESULT_STATUS_ERROR_KHR = -1,\n    VK_QUERY_RESULT_STATUS_NOT_READY_KHR = 0,\n    VK_QUERY_RESULT_STATUS_COMPLETE_KHR = 1,\n} VkQueryResultStatusKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::NOT_READY_KHR`] specifies that the query\n  result is not yet available.\n\n* [`Self::ERROR_KHR`] specifies that operations did not\n  complete successfully.\n\n* [`Self::COMPLETE_KHR`] specifies that operations\n  completed successfully and the query result is available.\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[doc(alias = "VkQueryResultStatusKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct QueryResultStatusKHR(pub i32);
impl std::fmt::Debug for QueryResultStatusKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::ERROR_KHR => "ERROR_KHR",
            &Self::NOT_READY_KHR => "NOT_READY_KHR",
            &Self::COMPLETE_KHR => "COMPLETE_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::extensions::khr_video_queue::QueryResultStatusKHR {
    pub const ERROR_KHR: Self = Self(-1);
    pub const NOT_READY_KHR: Self = Self(0);
    pub const COMPLETE_KHR: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceVideoCapabilitiesKHR.html)) · Function <br/> vkGetPhysicalDeviceVideoCapabilitiesKHR - Query video decode or encode capabilities\n[](#_c_specification)C Specification\n----------\n\nTo query video decode or encode capabilities for a specific codec, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetPhysicalDeviceVideoCapabilitiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkVideoProfileKHR*                    pVideoProfile,\n    VkVideoCapabilitiesKHR*                     pCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device whose video decode or encode\n  capabilities will be queried.\n\n* [`Self::p_video_profile`] is a pointer to a [`crate::vk::VideoProfileKHR`] structure\n  with a chained codec-operation specific video profile structure.\n\n* [`Self::p_capabilities`] is a pointer to a [`crate::vk::VideoCapabilitiesKHR`]structure in which the capabilities are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-pVideoProfile-parameter  \n  [`Self::p_video_profile`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-pCapabilities-parameter  \n  [`Self::p_capabilities`] **must** be a valid pointer to a [`crate::vk::VideoCapabilitiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_EXTENSION_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::VideoCapabilitiesKHR`], [`crate::vk::VideoProfileKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceVideoCapabilitiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_video_profile: *const crate::extensions::khr_video_queue::VideoProfileKHR, p_capabilities: *mut crate::extensions::khr_video_queue::VideoCapabilitiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceVideoFormatPropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceVideoFormatPropertiesKHR - Query supported Video Decode and Encode image formats\n[](#_c_specification)C Specification\n----------\n\nTo enumerate the supported output, input and DPB image formats for a\nspecific codec operation and video profile, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetPhysicalDeviceVideoFormatPropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceVideoFormatInfoKHR*   pVideoFormatInfo,\n    uint32_t*                                   pVideoFormatPropertyCount,\n    VkVideoFormatPropertiesKHR*                 pVideoFormatProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device being queried.\n\n* [`Self::p_video_format_info`] is a pointer to a[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] structure specifying the codec\n  and video profile for which information is returned.\n\n* [`Self::p_video_format_property_count`] is a pointer to an integer related to\n  the number of video format properties available or queried, as described\n  below.\n\n* [`Self::p_video_format_properties`] is a pointer to an array of[`crate::vk::VideoFormatPropertiesKHR`] structures in which supported formats\n  are returned.\n[](#_description)Description\n----------\n\nIf [`Self::p_video_format_properties`] is `NULL`, then the number of video format\nproperties supported for the given [`Self::physical_device`] is returned in[`Self::p_video_format_property_count`].\nOtherwise, [`Self::p_video_format_property_count`] **must** point to a variable set by\nthe user to the number of elements in the [`Self::p_video_format_properties`]array, and on return the variable is overwritten with the number of values\nactually written to [`Self::p_video_format_properties`].\nIf the value of [`Self::p_video_format_property_count`] is less than the number of\nvideo format properties supported, at most [`Self::p_video_format_property_count`]values will be written to [`Self::p_video_format_properties`], and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the available values were returned.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-imageUsage-04844  \n   The `imageUsage` enum of [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`]**must** contain at least one of the following video image usage bit(s):[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DST_KHR`],[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DPB_KHR`],[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_SRC_KHR`], or[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_DPB_KHR`].\n\n|   |Note:<br/><br/>For most use cases, only decode or encode related usage flags are going to<br/>be specified.<br/>For a use case such as transcode, if the image were to be shared between<br/>decode and encode session(s), then both decode and encode related usage<br/>flags **can** be set.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatInfo-parameter  \n  [`Self::p_video_format_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatPropertyCount-parameter  \n  [`Self::p_video_format_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatProperties-parameter  \n   If the value referenced by [`Self::p_video_format_property_count`] is not `0`, and [`Self::p_video_format_properties`] is not `NULL`, [`Self::p_video_format_properties`] **must** be a valid pointer to an array of [`Self::p_video_format_property_count`] [`crate::vk::VideoFormatPropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_EXTENSION_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`], [`crate::vk::VideoFormatPropertiesKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceVideoFormatPropertiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_video_format_info: *const crate::extensions::khr_video_queue::PhysicalDeviceVideoFormatInfoKHR, p_video_format_property_count: *mut u32, p_video_format_properties: *mut crate::extensions::khr_video_queue::VideoFormatPropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateVideoSessionKHR.html)) · Function <br/> vkCreateVideoSessionKHR - Creates a video session object\n[](#_c_specification)C Specification\n----------\n\nTo create a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkCreateVideoSessionKHR(\n    VkDevice                                    device,\n    const VkVideoSessionCreateInfoKHR*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkVideoSessionKHR*                          pVideoSession);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that creates the decode or encode\n  session object.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::VideoSessionCreateInfoKHR`]structure containing parameters specifying the creation of the decode or\n  encode session.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_video_session`] is a pointer to a [`crate::vk::VideoSessionKHR`] structure\n  specifying the decode or encode video session object which will be\n  created by this function when it returns [`crate::vk::Result::SUCCESS`]\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateVideoSessionKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateVideoSessionKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionCreateInfoKHR`] structure\n\n* []() VUID-vkCreateVideoSessionKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateVideoSessionKHR-pVideoSession-parameter  \n  [`Self::p_video_session`] **must** be a valid pointer to a [`crate::vk::VideoSessionKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_INCOMPATIBLE_DRIVER`]\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionCreateInfoKHR`], [`crate::vk::VideoSessionKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateVideoSessionKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_create_info: *const crate::extensions::khr_video_queue::VideoSessionCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_video_session: *mut crate::extensions::khr_video_queue::VideoSessionKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyVideoSessionKHR.html)) · Function <br/> vkDestroyVideoSessionKHR - Destroy decode session object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a decode session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkDestroyVideoSessionKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that was used for the creation of the video\n  session.\n\n* [`Self::video_session`] is the decode or encode video session to be\n  destroyed.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyVideoSessionKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyVideoSessionKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkDestroyVideoSessionKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyVideoSessionKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyVideoSessionKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateVideoSessionParametersKHR.html)) · Function <br/> vkCreateVideoSessionParametersKHR - Creates video session video session parameter object\n[](#_c_specification)C Specification\n----------\n\nTo create a video session parameters object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkCreateVideoSessionParametersKHR(\n    VkDevice                                    device,\n    const VkVideoSessionParametersCreateInfoKHR* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkVideoSessionParametersKHR*                pVideoSessionParameters);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that was used for the creation of the\n  video session object.\n\n* [`Self::p_create_info`] is a pointer to[`crate::vk::VideoSessionParametersCreateInfoKHR`] structure specifying the\n  video session parameters.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_video_session_parameters`] is a pointer to a[`crate::vk::VideoSessionParametersKHR`] handle in which the video session\n  parameters object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionParametersCreateInfoKHR`] structure\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pVideoSessionParameters-parameter  \n  [`Self::p_video_session_parameters`] **must** be a valid pointer to a [`crate::vk::VideoSessionParametersKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionParametersCreateInfoKHR`], [`crate::vk::VideoSessionParametersKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateVideoSessionParametersKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_create_info: *const crate::extensions::khr_video_queue::VideoSessionParametersCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_video_session_parameters: *mut crate::extensions::khr_video_queue::VideoSessionParametersKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkUpdateVideoSessionParametersKHR.html)) · Function <br/> vkUpdateVideoSessionParametersKHR - Update video session video session parameter object\n[](#_c_specification)C Specification\n----------\n\nTo update, add, or remove video session parameters state, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkUpdateVideoSessionParametersKHR(\n    VkDevice                                    device,\n    VkVideoSessionParametersKHR                 videoSessionParameters,\n    const VkVideoSessionParametersUpdateInfoKHR* pUpdateInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that was used for the creation of the\n  video session object.\n\n* [`Self::video_session_parameters`] is the video session object that is going\n  to be updated.\n\n* [`Self::p_update_info`] is a pointer to a[`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure containing the\n  session parameters update information.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-videoSessionParameters-parameter  \n  [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-pUpdateInfo-parameter  \n  [`Self::p_update_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::VideoSessionParametersUpdateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkUpdateVideoSessionParametersKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR, p_update_info: *const crate::extensions::khr_video_queue::VideoSessionParametersUpdateInfoKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyVideoSessionParametersKHR.html)) · Function <br/> vkDestroyVideoSessionParametersKHR - Destroy video session parameters object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkDestroyVideoSessionParametersKHR(\n    VkDevice                                    device,\n    VkVideoSessionParametersKHR                 videoSessionParameters,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device the video session was created with.\n\n* [`Self::video_session_parameters`] is the video session parameters object to\n  be destroyed.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-videoSessionParameters-parameter  \n  [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionParametersKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyVideoSessionParametersKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetVideoSessionMemoryRequirementsKHR.html)) · Function <br/> vkGetVideoSessionMemoryRequirementsKHR - Get Memory Requirements\n[](#_c_specification)C Specification\n----------\n\nTo get memory requirements for a video session, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetVideoSessionMemoryRequirementsKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    uint32_t*                                   pVideoSessionMemoryRequirementsCount,\n    VkVideoGetMemoryPropertiesKHR*              pVideoSessionMemoryRequirements);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the video session.\n\n* [`Self::video_session`] is the video session to query.\n\n* [`Self::p_video_session_memory_requirements_count`] is a pointer to an integer\n  related to the number of memory heap requirements available or queried,\n  as described below.\n\n* [`Self::p_video_session_memory_requirements`] is `NULL` or a pointer to an array\n  of [`crate::vk::VideoGetMemoryPropertiesKHR`] structures in which the memory\n  heap requirements of the video session are returned.\n[](#_description)Description\n----------\n\nIf [`Self::p_video_session_memory_requirements`] is `NULL`, then the number of\nmemory heap types required for the video session is returned in[`Self::p_video_session_memory_requirements_count`].\nOtherwise, [`Self::p_video_session_memory_requirements_count`] **must** point to a\nvariable set by the user with the number of elements in the[`Self::p_video_session_memory_requirements`] array, and on return the variable is\noverwritten with the number of formats actually written to[`Self::p_video_session_memory_requirements`].\nIf [`Self::p_video_session_memory_requirements_count`] is less than the number of\nmemory heap types required for the video session, then at most[`Self::p_video_session_memory_requirements_count`] elements will be written to[`Self::p_video_session_memory_requirements`], and [`crate::vk::Result::INCOMPLETE`] will be\nreturned, instead of [`crate::vk::Result::SUCCESS`], to indicate that not all required\nmemory heap types were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-pVideoSessionMemoryRequirementsCount-parameter  \n  [`Self::p_video_session_memory_requirements_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-pVideoSessionMemoryRequirements-parameter  \n   If the value referenced by [`Self::p_video_session_memory_requirements_count`] is not `0`, and [`Self::p_video_session_memory_requirements`] is not `NULL`, [`Self::p_video_session_memory_requirements`] **must** be a valid pointer to an array of [`Self::p_video_session_memory_requirements_count`] [`crate::vk::VideoGetMemoryPropertiesKHR`] structures\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoGetMemoryPropertiesKHR`], [`crate::vk::VideoSessionKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetVideoSessionMemoryRequirementsKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, p_video_session_memory_requirements_count: *mut u32, p_video_session_memory_requirements: *mut crate::extensions::khr_video_queue::VideoGetMemoryPropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkBindVideoSessionMemoryKHR.html)) · Function <br/> vkBindVideoSessionMemoryKHR - Bind Video Memory\n[](#_c_specification)C Specification\n----------\n\nTo attach memory to a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkBindVideoSessionMemoryKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    uint32_t                                    videoSessionBindMemoryCount,\n    const VkVideoBindMemoryKHR*                 pVideoSessionBindMemories);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the video session’s memory.\n\n* [`Self::video_session`] is the video session to be bound with device memory.\n\n* [`Self::video_session_bind_memory_count`] is the number of[`Self::p_video_session_bind_memories`] to be bound.\n\n* [`Self::p_video_session_bind_memories`] is a pointer to an array of[`crate::vk::VideoBindMemoryKHR`] structures specifying memory regions to be\n  bound to a device memory heap.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkBindVideoSessionMemoryKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkBindVideoSessionMemoryKHR-pVideoSessionBindMemories-parameter  \n  [`Self::p_video_session_bind_memories`] **must** be a valid pointer to an array of [`Self::video_session_bind_memory_count`] valid [`crate::vk::VideoBindMemoryKHR`] structures\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSessionBindMemoryCount-arraylength  \n  [`Self::video_session_bind_memory_count`] **must** be greater than `0`\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoBindMemoryKHR`], [`crate::vk::VideoSessionKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkBindVideoSessionMemoryKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, video_session_bind_memory_count: u32, p_video_session_bind_memories: *const crate::extensions::khr_video_queue::VideoBindMemoryKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginVideoCodingKHR.html)) · Function <br/> vkCmdBeginVideoCodingKHR - Start decode jobs\n[](#_c_specification)C Specification\n----------\n\nTo start video decode or encode operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdBeginVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoBeginCodingInfoKHR*            pBeginInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be used when recording\n  commands for the video decode or encode operations.\n\n* [`Self::p_begin_info`] is a pointer to a [`crate::vk::VideoBeginCodingInfoKHR`]structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginVideoCodingKHR-pBeginInfo-parameter  \n  [`Self::p_begin_info`] **must** be a valid pointer to a valid [`crate::vk::VideoBeginCodingInfoKHR`] structure\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdBeginVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdBeginVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoBeginCodingInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBeginVideoCodingKHR = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_begin_info: *const crate::extensions::khr_video_queue::VideoBeginCodingInfoKHR) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdControlVideoCodingKHR.html)) · Function <br/> vkCmdControlVideoCodingKHR - Set encode rate control parameters\n[](#_c_specification)C Specification\n----------\n\nTo apply dynamic controls to video decode or video operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdControlVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoCodingControlInfoKHR*          pCodingControlInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be filled by this function\n  for setting encode rate control parameters.\n\n* [`Self::p_coding_control_info`] is a pointer to a[`crate::vk::VideoCodingControlInfoKHR`] structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdControlVideoCodingKHR-pCodingControlInfo-parameter  \n  [`Self::p_coding_control_info`] **must** be a valid pointer to a valid [`crate::vk::VideoCodingControlInfoKHR`] structure\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdControlVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdControlVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoCodingControlInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdControlVideoCodingKHR = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_coding_control_info: *const crate::extensions::khr_video_queue::VideoCodingControlInfoKHR) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndVideoCodingKHR.html)) · Function <br/> vkCmdEndVideoCodingKHR - End decode jobs\n[](#_c_specification)C Specification\n----------\n\nTo end video decode or encode operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdEndVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoEndCodingInfoKHR*              pEndCodingInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be filled by this function.\n\n* [`Self::p_end_coding_info`] is a pointer to a [`crate::vk::VideoEndCodingInfoKHR`]structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndVideoCodingKHR-pEndCodingInfo-parameter  \n  [`Self::p_end_coding_info`] **must** be a valid pointer to a valid [`crate::vk::VideoEndCodingInfoKHR`] structure\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdEndVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdEndVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoEndCodingInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdEndVideoCodingKHR = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_end_coding_info: *const crate::extensions::khr_video_queue::VideoEndCodingInfoKHR) -> ();
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHR> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHRBuilder<'_>> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHR> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHRBuilder<'_>> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHR> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHRBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHR> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHRBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHR> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfilesKHRBuilder<'_>> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHR> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHRBuilder<'_>> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHR> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoProfileKHRBuilder<'_>> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoProfilesKHR> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoProfilesKHRBuilder<'_>> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoProfileKHR> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoProfileKHRBuilder<'_>> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoQueueFamilyProperties2KHR> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoQueueFamilyProperties2KHRBuilder<'_>> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoQueueFamilyProperties2KHR.html)) · Structure <br/> VkVideoQueueFamilyProperties2KHR - Structure specifying the codec video operations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoQueueFamilyProperties2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoQueueFamilyProperties2KHR {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkVideoCodecOperationFlagsKHR    videoCodecOperations;\n} VkVideoQueueFamilyProperties2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_codec_operations`] is a bitmask of[VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) specifying supported video codec\n  operation(s).\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR`]\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-videoCodecOperations-parameter  \n  [`Self::video_codec_operations`] **must** be a valid combination of [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) values\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-videoCodecOperations-requiredbitmask  \n  [`Self::video_codec_operations`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoCodecOperationFlagBitsKHR`]\n"]
#[doc(alias = "VkVideoQueueFamilyProperties2KHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoQueueFamilyProperties2KHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub video_codec_operations: crate::extensions::khr_video_queue::VideoCodecOperationFlagsKHR,
}
impl VideoQueueFamilyProperties2KHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR;
}
impl Default for VideoQueueFamilyProperties2KHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), video_codec_operations: Default::default() }
    }
}
impl std::fmt::Debug for VideoQueueFamilyProperties2KHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoQueueFamilyProperties2KHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("video_codec_operations", &self.video_codec_operations).finish()
    }
}
impl VideoQueueFamilyProperties2KHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoQueueFamilyProperties2KHRBuilder<'a> {
        VideoQueueFamilyProperties2KHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoQueueFamilyProperties2KHR.html)) · Builder of [`VideoQueueFamilyProperties2KHR`] <br/> VkVideoQueueFamilyProperties2KHR - Structure specifying the codec video operations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoQueueFamilyProperties2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoQueueFamilyProperties2KHR {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkVideoCodecOperationFlagsKHR    videoCodecOperations;\n} VkVideoQueueFamilyProperties2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_codec_operations`] is a bitmask of[VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) specifying supported video codec\n  operation(s).\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR`]\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-videoCodecOperations-parameter  \n  [`Self::video_codec_operations`] **must** be a valid combination of [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) values\n\n* []() VUID-VkVideoQueueFamilyProperties2KHR-videoCodecOperations-requiredbitmask  \n  [`Self::video_codec_operations`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoCodecOperationFlagBitsKHR`]\n"]
#[repr(transparent)]
pub struct VideoQueueFamilyProperties2KHRBuilder<'a>(VideoQueueFamilyProperties2KHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoQueueFamilyProperties2KHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoQueueFamilyProperties2KHRBuilder<'a> {
        VideoQueueFamilyProperties2KHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn video_codec_operations(mut self, video_codec_operations: crate::extensions::khr_video_queue::VideoCodecOperationFlagsKHR) -> Self {
        self.0.video_codec_operations = video_codec_operations as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoQueueFamilyProperties2KHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoQueueFamilyProperties2KHRBuilder<'a> {
    fn default() -> VideoQueueFamilyProperties2KHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoQueueFamilyProperties2KHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoQueueFamilyProperties2KHRBuilder<'a> {
    type Target = VideoQueueFamilyProperties2KHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoQueueFamilyProperties2KHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoProfilesKHR.html)) · Structure <br/> VkVideoProfilesKHR - Structure enumerating the video profiles\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoProfilesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoProfilesKHR {\n    VkStructureType             sType;\n    void*                       pNext;\n    uint32_t                    profileCount;\n    const VkVideoProfileKHR*    pProfiles;\n} VkVideoProfilesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::profile_count`] is an integer which holds the number of video\n  profiles included in [`Self::p_profiles`].\n\n* [`Self::p_profiles`] is a pointer to an array of [`crate::vk::VideoProfileKHR`]structures.\n  Each [`crate::vk::VideoProfileKHR`] structure **must** chain the corresponding\n  codec-operation specific extension video profile structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoProfilesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PROFILES_KHR`]\n\n* []() VUID-VkVideoProfilesKHR-pProfiles-parameter  \n  [`Self::p_profiles`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`], [`crate::vk::StructureType`], [`crate::vk::VideoProfileKHR`]\n"]
#[doc(alias = "VkVideoProfilesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoProfilesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub profile_count: u32,
    pub p_profiles: *const crate::extensions::khr_video_queue::VideoProfileKHR,
}
impl VideoProfilesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_PROFILES_KHR;
}
impl Default for VideoProfilesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), profile_count: Default::default(), p_profiles: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoProfilesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoProfilesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("profile_count", &self.profile_count).field("p_profiles", &self.p_profiles).finish()
    }
}
impl VideoProfilesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoProfilesKHRBuilder<'a> {
        VideoProfilesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoProfilesKHR.html)) · Builder of [`VideoProfilesKHR`] <br/> VkVideoProfilesKHR - Structure enumerating the video profiles\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoProfilesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoProfilesKHR {\n    VkStructureType             sType;\n    void*                       pNext;\n    uint32_t                    profileCount;\n    const VkVideoProfileKHR*    pProfiles;\n} VkVideoProfilesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::profile_count`] is an integer which holds the number of video\n  profiles included in [`Self::p_profiles`].\n\n* [`Self::p_profiles`] is a pointer to an array of [`crate::vk::VideoProfileKHR`]structures.\n  Each [`crate::vk::VideoProfileKHR`] structure **must** chain the corresponding\n  codec-operation specific extension video profile structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoProfilesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PROFILES_KHR`]\n\n* []() VUID-VkVideoProfilesKHR-pProfiles-parameter  \n  [`Self::p_profiles`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`], [`crate::vk::StructureType`], [`crate::vk::VideoProfileKHR`]\n"]
#[repr(transparent)]
pub struct VideoProfilesKHRBuilder<'a>(VideoProfilesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoProfilesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoProfilesKHRBuilder<'a> {
        VideoProfilesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn profile_count(mut self, profile_count: u32) -> Self {
        self.0.profile_count = profile_count as _;
        self
    }
    #[inline]
    pub fn profiles(mut self, profiles: &'a crate::extensions::khr_video_queue::VideoProfileKHR) -> Self {
        self.0.p_profiles = profiles as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoProfilesKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoProfilesKHRBuilder<'a> {
    fn default() -> VideoProfilesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoProfilesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoProfilesKHRBuilder<'a> {
    type Target = VideoProfilesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoProfilesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVideoFormatInfoKHR.html)) · Structure <br/> VkPhysicalDeviceVideoFormatInfoKHR - Structure specifying the codec video format\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] input structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkPhysicalDeviceVideoFormatInfoKHR {\n    VkStructureType              sType;\n    void*                        pNext;\n    VkImageUsageFlags            imageUsage;\n    const VkVideoProfilesKHR*    pVideoProfiles;\n} VkPhysicalDeviceVideoFormatInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_usage`] is a bitmask of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html) specifying\n  intended video image usages.\n\n* [`Self::p_video_profiles`] is a pointer to a [`crate::vk::VideoProfilesKHR`]structure providing the video profile(s) of video session(s) that will\n  use the image.\n  For most use cases, the image is used by a single video session and a\n  single video profile is provided.\n  For a use case such as transcode, where a decode session output image**may** be used as encode input for one or more encode sessions, multiple\n  video profiles representing the video sessions that will share the image**may** be provided.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVideoFormatInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR`]\n\n* []() VUID-VkPhysicalDeviceVideoFormatInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ImageUsageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::VideoProfilesKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`]\n"]
#[doc(alias = "VkPhysicalDeviceVideoFormatInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceVideoFormatInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub image_usage: crate::vk1_0::ImageUsageFlags,
    pub p_video_profiles: *const crate::extensions::khr_video_queue::VideoProfilesKHR,
}
impl PhysicalDeviceVideoFormatInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR;
}
impl Default for PhysicalDeviceVideoFormatInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), image_usage: Default::default(), p_video_profiles: std::ptr::null() }
    }
}
impl std::fmt::Debug for PhysicalDeviceVideoFormatInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceVideoFormatInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("image_usage", &self.image_usage).field("p_video_profiles", &self.p_video_profiles).finish()
    }
}
impl PhysicalDeviceVideoFormatInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
        PhysicalDeviceVideoFormatInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVideoFormatInfoKHR.html)) · Builder of [`PhysicalDeviceVideoFormatInfoKHR`] <br/> VkPhysicalDeviceVideoFormatInfoKHR - Structure specifying the codec video format\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] input structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkPhysicalDeviceVideoFormatInfoKHR {\n    VkStructureType              sType;\n    void*                        pNext;\n    VkImageUsageFlags            imageUsage;\n    const VkVideoProfilesKHR*    pVideoProfiles;\n} VkPhysicalDeviceVideoFormatInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_usage`] is a bitmask of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html) specifying\n  intended video image usages.\n\n* [`Self::p_video_profiles`] is a pointer to a [`crate::vk::VideoProfilesKHR`]structure providing the video profile(s) of video session(s) that will\n  use the image.\n  For most use cases, the image is used by a single video session and a\n  single video profile is provided.\n  For a use case such as transcode, where a decode session output image**may** be used as encode input for one or more encode sessions, multiple\n  video profiles representing the video sessions that will share the image**may** be provided.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVideoFormatInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR`]\n\n* []() VUID-VkPhysicalDeviceVideoFormatInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ImageUsageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::VideoProfilesKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceVideoFormatInfoKHRBuilder<'a>(PhysicalDeviceVideoFormatInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
        PhysicalDeviceVideoFormatInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image_usage(mut self, image_usage: crate::vk1_0::ImageUsageFlags) -> Self {
        self.0.image_usage = image_usage as _;
        self
    }
    #[inline]
    pub fn video_profiles(mut self, video_profiles: &'a crate::extensions::khr_video_queue::VideoProfilesKHR) -> Self {
        self.0.p_video_profiles = video_profiles as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceVideoFormatInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
    fn default() -> PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
    type Target = PhysicalDeviceVideoFormatInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceVideoFormatInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoFormatPropertiesKHR.html)) · Structure <br/> VkVideoFormatPropertiesKHR - Structure enumerating the video image formats\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoFormatPropertiesKHR`] output structure for[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoFormatPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkFormat           format;\n} VkVideoFormatPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::format`] is one of the supported formats reported by the\n  implementation.\n[](#_description)Description\n----------\n\nIf the `pVideoProfiles` provided in input structure`pVideoFormatInfo` are not supported,[`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`] is returned.\nIf the implementation requires an opaque video decode or encode DPB, then\nwhen querying with the corresponding video decode or encode DPB image usage\nin `imageUsage`, only one image format is returned:[`crate::vk::Format::UNDEFINED`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoFormatPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_FORMAT_PROPERTIES_KHR`]\n\n* []() VUID-VkVideoFormatPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`]\n"]
#[doc(alias = "VkVideoFormatPropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoFormatPropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub format: crate::vk1_0::Format,
}
impl VideoFormatPropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_FORMAT_PROPERTIES_KHR;
}
impl Default for VideoFormatPropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), format: Default::default() }
    }
}
impl std::fmt::Debug for VideoFormatPropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoFormatPropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("format", &self.format).finish()
    }
}
impl VideoFormatPropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoFormatPropertiesKHRBuilder<'a> {
        VideoFormatPropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoFormatPropertiesKHR.html)) · Builder of [`VideoFormatPropertiesKHR`] <br/> VkVideoFormatPropertiesKHR - Structure enumerating the video image formats\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoFormatPropertiesKHR`] output structure for[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoFormatPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkFormat           format;\n} VkVideoFormatPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::format`] is one of the supported formats reported by the\n  implementation.\n[](#_description)Description\n----------\n\nIf the `pVideoProfiles` provided in input structure`pVideoFormatInfo` are not supported,[`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`] is returned.\nIf the implementation requires an opaque video decode or encode DPB, then\nwhen querying with the corresponding video decode or encode DPB image usage\nin `imageUsage`, only one image format is returned:[`crate::vk::Format::UNDEFINED`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoFormatPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_FORMAT_PROPERTIES_KHR`]\n\n* []() VUID-VkVideoFormatPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`]\n"]
#[repr(transparent)]
pub struct VideoFormatPropertiesKHRBuilder<'a>(VideoFormatPropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoFormatPropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoFormatPropertiesKHRBuilder<'a> {
        VideoFormatPropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn format(mut self, format: crate::vk1_0::Format) -> Self {
        self.0.format = format as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoFormatPropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoFormatPropertiesKHRBuilder<'a> {
    fn default() -> VideoFormatPropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoFormatPropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoFormatPropertiesKHRBuilder<'a> {
    type Target = VideoFormatPropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoFormatPropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoProfileKHR.html)) · Structure <br/> VkVideoProfileKHR - Structure specifying the codec video profile\n[](#_c_specification)C Specification\n----------\n\nA video profile is defined by [`crate::vk::VideoProfileKHR`] structure as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoProfileKHR {\n    VkStructureType                     sType;\n    void*                               pNext;\n    VkVideoCodecOperationFlagBitsKHR    videoCodecOperation;\n    VkVideoChromaSubsamplingFlagsKHR    chromaSubsampling;\n    VkVideoComponentBitDepthFlagsKHR    lumaBitDepth;\n    VkVideoComponentBitDepthFlagsKHR    chromaBitDepth;\n} VkVideoProfileKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_codec_operation`] is a [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html)value specifying a video codec operation.\n\n* [`Self::chroma_subsampling`] is a bitmask of[VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html) specifying video chroma\n  subsampling information.\n\n* [`Self::luma_bit_depth`] is a bitmask of[VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) specifying video luma bit\n  depth information.\n\n* [`Self::chroma_bit_depth`] is a bitmask of[VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) specifying video chroma bit\n  depth information.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoProfileKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PROFILE_KHR`]\n\n* []() VUID-VkVideoProfileKHR-videoCodecOperation-parameter  \n  [`Self::video_codec_operation`] **must** be a valid [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) value\n\n* []() VUID-VkVideoProfileKHR-chromaSubsampling-parameter  \n  [`Self::chroma_subsampling`] **must** be a valid combination of [VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-chromaSubsampling-requiredbitmask  \n  [`Self::chroma_subsampling`] **must** not be `0`\n\n* []() VUID-VkVideoProfileKHR-lumaBitDepth-parameter  \n  [`Self::luma_bit_depth`] **must** be a valid combination of [VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-lumaBitDepth-requiredbitmask  \n  [`Self::luma_bit_depth`] **must** not be `0`\n\n* []() VUID-VkVideoProfileKHR-chromaBitDepth-parameter  \n  [`Self::chroma_bit_depth`] **must** be a valid combination of [VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-chromaBitDepth-requiredbitmask  \n  [`Self::chroma_bit_depth`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoChromaSubsamplingFlagBitsKHR`], [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html), [`crate::vk::VideoComponentBitDepthFlagBitsKHR`], [`crate::vk::VideoProfilesKHR`], [`crate::vk::VideoSessionCreateInfoKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`]\n"]
#[doc(alias = "VkVideoProfileKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoProfileKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub video_codec_operation: crate::extensions::khr_video_queue::VideoCodecOperationFlagBitsKHR,
    pub chroma_subsampling: crate::extensions::khr_video_queue::VideoChromaSubsamplingFlagsKHR,
    pub luma_bit_depth: crate::extensions::khr_video_queue::VideoComponentBitDepthFlagsKHR,
    pub chroma_bit_depth: crate::extensions::khr_video_queue::VideoComponentBitDepthFlagsKHR,
}
impl VideoProfileKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_PROFILE_KHR;
}
impl Default for VideoProfileKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), video_codec_operation: Default::default(), chroma_subsampling: Default::default(), luma_bit_depth: Default::default(), chroma_bit_depth: Default::default() }
    }
}
impl std::fmt::Debug for VideoProfileKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoProfileKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("video_codec_operation", &self.video_codec_operation).field("chroma_subsampling", &self.chroma_subsampling).field("luma_bit_depth", &self.luma_bit_depth).field("chroma_bit_depth", &self.chroma_bit_depth).finish()
    }
}
impl VideoProfileKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoProfileKHRBuilder<'a> {
        VideoProfileKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoProfileKHR.html)) · Builder of [`VideoProfileKHR`] <br/> VkVideoProfileKHR - Structure specifying the codec video profile\n[](#_c_specification)C Specification\n----------\n\nA video profile is defined by [`crate::vk::VideoProfileKHR`] structure as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoProfileKHR {\n    VkStructureType                     sType;\n    void*                               pNext;\n    VkVideoCodecOperationFlagBitsKHR    videoCodecOperation;\n    VkVideoChromaSubsamplingFlagsKHR    chromaSubsampling;\n    VkVideoComponentBitDepthFlagsKHR    lumaBitDepth;\n    VkVideoComponentBitDepthFlagsKHR    chromaBitDepth;\n} VkVideoProfileKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_codec_operation`] is a [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html)value specifying a video codec operation.\n\n* [`Self::chroma_subsampling`] is a bitmask of[VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html) specifying video chroma\n  subsampling information.\n\n* [`Self::luma_bit_depth`] is a bitmask of[VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) specifying video luma bit\n  depth information.\n\n* [`Self::chroma_bit_depth`] is a bitmask of[VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) specifying video chroma bit\n  depth information.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoProfileKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PROFILE_KHR`]\n\n* []() VUID-VkVideoProfileKHR-videoCodecOperation-parameter  \n  [`Self::video_codec_operation`] **must** be a valid [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html) value\n\n* []() VUID-VkVideoProfileKHR-chromaSubsampling-parameter  \n  [`Self::chroma_subsampling`] **must** be a valid combination of [VkVideoChromaSubsamplingFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoChromaSubsamplingFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-chromaSubsampling-requiredbitmask  \n  [`Self::chroma_subsampling`] **must** not be `0`\n\n* []() VUID-VkVideoProfileKHR-lumaBitDepth-parameter  \n  [`Self::luma_bit_depth`] **must** be a valid combination of [VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-lumaBitDepth-requiredbitmask  \n  [`Self::luma_bit_depth`] **must** not be `0`\n\n* []() VUID-VkVideoProfileKHR-chromaBitDepth-parameter  \n  [`Self::chroma_bit_depth`] **must** be a valid combination of [VkVideoComponentBitDepthFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoComponentBitDepthFlagBitsKHR.html) values\n\n* []() VUID-VkVideoProfileKHR-chromaBitDepth-requiredbitmask  \n  [`Self::chroma_bit_depth`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoChromaSubsamplingFlagBitsKHR`], [VkVideoCodecOperationFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodecOperationFlagBitsKHR.html), [`crate::vk::VideoComponentBitDepthFlagBitsKHR`], [`crate::vk::VideoProfilesKHR`], [`crate::vk::VideoSessionCreateInfoKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`]\n"]
#[repr(transparent)]
pub struct VideoProfileKHRBuilder<'a>(VideoProfileKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoProfileKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoProfileKHRBuilder<'a> {
        VideoProfileKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn video_codec_operation(mut self, video_codec_operation: crate::extensions::khr_video_queue::VideoCodecOperationFlagBitsKHR) -> Self {
        self.0.video_codec_operation = video_codec_operation as _;
        self
    }
    #[inline]
    pub fn chroma_subsampling(mut self, chroma_subsampling: crate::extensions::khr_video_queue::VideoChromaSubsamplingFlagsKHR) -> Self {
        self.0.chroma_subsampling = chroma_subsampling as _;
        self
    }
    #[inline]
    pub fn luma_bit_depth(mut self, luma_bit_depth: crate::extensions::khr_video_queue::VideoComponentBitDepthFlagsKHR) -> Self {
        self.0.luma_bit_depth = luma_bit_depth as _;
        self
    }
    #[inline]
    pub fn chroma_bit_depth(mut self, chroma_bit_depth: crate::extensions::khr_video_queue::VideoComponentBitDepthFlagsKHR) -> Self {
        self.0.chroma_bit_depth = chroma_bit_depth as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoProfileKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoProfileKHRBuilder<'a> {
    fn default() -> VideoProfileKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoProfileKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoProfileKHRBuilder<'a> {
    type Target = VideoProfileKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoProfileKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesKHR.html)) · Structure <br/> VkVideoCapabilitiesKHR - Structure specifying parameters of video capabilities\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoCapabilitiesKHR {\n    VkStructureType                sType;\n    void*                          pNext;\n    VkVideoCapabilitiesFlagsKHR    capabilityFlags;\n    VkDeviceSize                   minBitstreamBufferOffsetAlignment;\n    VkDeviceSize                   minBitstreamBufferSizeAlignment;\n    VkExtent2D                     videoPictureExtentGranularity;\n    VkExtent2D                     minExtent;\n    VkExtent2D                     maxExtent;\n    uint32_t                       maxReferencePicturesSlotsCount;\n    uint32_t                       maxReferencePicturesActiveCount;\n} VkVideoCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::capability_flags`] is a bitmask of[VkVideoCapabilitiesFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagBitsKHR.html) specifying capability flags.\n\n* [`Self::min_bitstream_buffer_offset_alignment`] is the minimum alignment for the\n  input or output bitstream buffer offset.\n\n* [`Self::min_bitstream_buffer_size_alignment`] is the minimum alignment for the\n  input or output bitstream buffer size\n\n* [`Self::video_picture_extent_granularity`] is the minimum size alignment of the\n  extent with the required padding for the decoded or encoded video\n  images.\n\n* [`Self::min_extent`] is the minimum width and height of the decoded or\n  encoded video.\n\n* [`Self::max_extent`] is the maximum width and height of the decoded or\n  encoded video.\n\n* [`Self::max_reference_pictures_slots_count`] is the maximum number of DPB Slots\n  supported by the implementation for a single video session instance.\n\n* [`Self::max_reference_pictures_active_count`] is the maximum slots that can be\n  used as [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) with a single decode or\n  encode operation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoCapabilitiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_CAPABILITIES_KHR`]\n\n* []() VUID-VkVideoCapabilitiesKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264CapabilitiesEXT`], [`crate::vk::VideoDecodeH265CapabilitiesEXT`], or [`crate::vk::VideoEncodeH264CapabilitiesEXT`]\n\n* []() VUID-VkVideoCapabilitiesKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::Extent2D`], [`crate::vk::StructureType`], [`crate::vk::VideoCapabilitiesFlagBitsKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`]\n"]
#[doc(alias = "VkVideoCapabilitiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoCapabilitiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub capability_flags: crate::extensions::khr_video_queue::VideoCapabilitiesFlagsKHR,
    pub min_bitstream_buffer_offset_alignment: crate::vk1_0::DeviceSize,
    pub min_bitstream_buffer_size_alignment: crate::vk1_0::DeviceSize,
    pub video_picture_extent_granularity: crate::vk1_0::Extent2D,
    pub min_extent: crate::vk1_0::Extent2D,
    pub max_extent: crate::vk1_0::Extent2D,
    pub max_reference_pictures_slots_count: u32,
    pub max_reference_pictures_active_count: u32,
}
impl VideoCapabilitiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_CAPABILITIES_KHR;
}
impl Default for VideoCapabilitiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), capability_flags: Default::default(), min_bitstream_buffer_offset_alignment: Default::default(), min_bitstream_buffer_size_alignment: Default::default(), video_picture_extent_granularity: Default::default(), min_extent: Default::default(), max_extent: Default::default(), max_reference_pictures_slots_count: Default::default(), max_reference_pictures_active_count: Default::default() }
    }
}
impl std::fmt::Debug for VideoCapabilitiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoCapabilitiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("capability_flags", &self.capability_flags).field("min_bitstream_buffer_offset_alignment", &self.min_bitstream_buffer_offset_alignment).field("min_bitstream_buffer_size_alignment", &self.min_bitstream_buffer_size_alignment).field("video_picture_extent_granularity", &self.video_picture_extent_granularity).field("min_extent", &self.min_extent).field("max_extent", &self.max_extent).field("max_reference_pictures_slots_count", &self.max_reference_pictures_slots_count).field("max_reference_pictures_active_count", &self.max_reference_pictures_active_count).finish()
    }
}
impl VideoCapabilitiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoCapabilitiesKHRBuilder<'a> {
        VideoCapabilitiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesKHR.html)) · Builder of [`VideoCapabilitiesKHR`] <br/> VkVideoCapabilitiesKHR - Structure specifying parameters of video capabilities\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoCapabilitiesKHR {\n    VkStructureType                sType;\n    void*                          pNext;\n    VkVideoCapabilitiesFlagsKHR    capabilityFlags;\n    VkDeviceSize                   minBitstreamBufferOffsetAlignment;\n    VkDeviceSize                   minBitstreamBufferSizeAlignment;\n    VkExtent2D                     videoPictureExtentGranularity;\n    VkExtent2D                     minExtent;\n    VkExtent2D                     maxExtent;\n    uint32_t                       maxReferencePicturesSlotsCount;\n    uint32_t                       maxReferencePicturesActiveCount;\n} VkVideoCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::capability_flags`] is a bitmask of[VkVideoCapabilitiesFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCapabilitiesFlagBitsKHR.html) specifying capability flags.\n\n* [`Self::min_bitstream_buffer_offset_alignment`] is the minimum alignment for the\n  input or output bitstream buffer offset.\n\n* [`Self::min_bitstream_buffer_size_alignment`] is the minimum alignment for the\n  input or output bitstream buffer size\n\n* [`Self::video_picture_extent_granularity`] is the minimum size alignment of the\n  extent with the required padding for the decoded or encoded video\n  images.\n\n* [`Self::min_extent`] is the minimum width and height of the decoded or\n  encoded video.\n\n* [`Self::max_extent`] is the maximum width and height of the decoded or\n  encoded video.\n\n* [`Self::max_reference_pictures_slots_count`] is the maximum number of DPB Slots\n  supported by the implementation for a single video session instance.\n\n* [`Self::max_reference_pictures_active_count`] is the maximum slots that can be\n  used as [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) with a single decode or\n  encode operation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoCapabilitiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_CAPABILITIES_KHR`]\n\n* []() VUID-VkVideoCapabilitiesKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264CapabilitiesEXT`], [`crate::vk::VideoDecodeH265CapabilitiesEXT`], or [`crate::vk::VideoEncodeH264CapabilitiesEXT`]\n\n* []() VUID-VkVideoCapabilitiesKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::Extent2D`], [`crate::vk::StructureType`], [`crate::vk::VideoCapabilitiesFlagBitsKHR`], [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`]\n"]
#[repr(transparent)]
pub struct VideoCapabilitiesKHRBuilder<'a>(VideoCapabilitiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoCapabilitiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoCapabilitiesKHRBuilder<'a> {
        VideoCapabilitiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn capability_flags(mut self, capability_flags: crate::extensions::khr_video_queue::VideoCapabilitiesFlagsKHR) -> Self {
        self.0.capability_flags = capability_flags as _;
        self
    }
    #[inline]
    pub fn min_bitstream_buffer_offset_alignment(mut self, min_bitstream_buffer_offset_alignment: crate::vk1_0::DeviceSize) -> Self {
        self.0.min_bitstream_buffer_offset_alignment = min_bitstream_buffer_offset_alignment as _;
        self
    }
    #[inline]
    pub fn min_bitstream_buffer_size_alignment(mut self, min_bitstream_buffer_size_alignment: crate::vk1_0::DeviceSize) -> Self {
        self.0.min_bitstream_buffer_size_alignment = min_bitstream_buffer_size_alignment as _;
        self
    }
    #[inline]
    pub fn video_picture_extent_granularity(mut self, video_picture_extent_granularity: crate::vk1_0::Extent2D) -> Self {
        self.0.video_picture_extent_granularity = video_picture_extent_granularity as _;
        self
    }
    #[inline]
    pub fn min_extent(mut self, min_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.min_extent = min_extent as _;
        self
    }
    #[inline]
    pub fn max_extent(mut self, max_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.max_extent = max_extent as _;
        self
    }
    #[inline]
    pub fn max_reference_pictures_slots_count(mut self, max_reference_pictures_slots_count: u32) -> Self {
        self.0.max_reference_pictures_slots_count = max_reference_pictures_slots_count as _;
        self
    }
    #[inline]
    pub fn max_reference_pictures_active_count(mut self, max_reference_pictures_active_count: u32) -> Self {
        self.0.max_reference_pictures_active_count = max_reference_pictures_active_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoCapabilitiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoCapabilitiesKHRBuilder<'a> {
    fn default() -> VideoCapabilitiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoCapabilitiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoCapabilitiesKHRBuilder<'a> {
    type Target = VideoCapabilitiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoCapabilitiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoGetMemoryPropertiesKHR.html)) · Structure <br/> VkVideoGetMemoryPropertiesKHR - Structure specifying video session required memory heap type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoGetMemoryPropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoGetMemoryPropertiesKHR {\n    VkStructureType           sType;\n    const void*               pNext;\n    uint32_t                  memoryBindIndex;\n    VkMemoryRequirements2*    pMemoryRequirements;\n} VkVideoGetMemoryPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_bind_index`] is the memory bind index of the memory heap type\n  described by the information returned in [`Self::p_memory_requirements`].\n\n* [`Self::p_memory_requirements`] is a pointer to a [`crate::vk::MemoryRequirements2`]structure in which the requested memory heap requirements for the heap\n  with index [`Self::memory_bind_index`] are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_GET_MEMORY_PROPERTIES_KHR`]\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-pMemoryRequirements-parameter  \n  [`Self::p_memory_requirements`] **must** be a valid pointer to a [`crate::vk::MemoryRequirements2`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MemoryRequirements2`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_video_session_memory_requirements_khr`]\n"]
#[doc(alias = "VkVideoGetMemoryPropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoGetMemoryPropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory_bind_index: u32,
    pub p_memory_requirements: *mut crate::vk1_1::MemoryRequirements2,
}
impl VideoGetMemoryPropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_GET_MEMORY_PROPERTIES_KHR;
}
impl Default for VideoGetMemoryPropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory_bind_index: Default::default(), p_memory_requirements: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for VideoGetMemoryPropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoGetMemoryPropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_bind_index", &self.memory_bind_index).field("p_memory_requirements", &self.p_memory_requirements).finish()
    }
}
impl VideoGetMemoryPropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoGetMemoryPropertiesKHRBuilder<'a> {
        VideoGetMemoryPropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoGetMemoryPropertiesKHR.html)) · Builder of [`VideoGetMemoryPropertiesKHR`] <br/> VkVideoGetMemoryPropertiesKHR - Structure specifying video session required memory heap type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoGetMemoryPropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoGetMemoryPropertiesKHR {\n    VkStructureType           sType;\n    const void*               pNext;\n    uint32_t                  memoryBindIndex;\n    VkMemoryRequirements2*    pMemoryRequirements;\n} VkVideoGetMemoryPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_bind_index`] is the memory bind index of the memory heap type\n  described by the information returned in [`Self::p_memory_requirements`].\n\n* [`Self::p_memory_requirements`] is a pointer to a [`crate::vk::MemoryRequirements2`]structure in which the requested memory heap requirements for the heap\n  with index [`Self::memory_bind_index`] are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_GET_MEMORY_PROPERTIES_KHR`]\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoGetMemoryPropertiesKHR-pMemoryRequirements-parameter  \n  [`Self::p_memory_requirements`] **must** be a valid pointer to a [`crate::vk::MemoryRequirements2`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MemoryRequirements2`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_video_session_memory_requirements_khr`]\n"]
#[repr(transparent)]
pub struct VideoGetMemoryPropertiesKHRBuilder<'a>(VideoGetMemoryPropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoGetMemoryPropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoGetMemoryPropertiesKHRBuilder<'a> {
        VideoGetMemoryPropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_bind_index(mut self, memory_bind_index: u32) -> Self {
        self.0.memory_bind_index = memory_bind_index as _;
        self
    }
    #[inline]
    pub fn memory_requirements(mut self, memory_requirements: &'a mut crate::vk1_1::MemoryRequirements2) -> Self {
        self.0.p_memory_requirements = memory_requirements as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoGetMemoryPropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoGetMemoryPropertiesKHRBuilder<'a> {
    fn default() -> VideoGetMemoryPropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoGetMemoryPropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoGetMemoryPropertiesKHRBuilder<'a> {
    type Target = VideoGetMemoryPropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoGetMemoryPropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoBindMemoryKHR.html)) · Structure <br/> VkVideoBindMemoryKHR - Structure specifying device memory heap entry for video session object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoBindMemoryKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoBindMemoryKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           memoryBindIndex;\n    VkDeviceMemory     memory;\n    VkDeviceSize       memoryOffset;\n    VkDeviceSize       memorySize;\n} VkVideoBindMemoryKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_bind_index`] is the index of the device memory heap returned in[`crate::vk::VideoGetMemoryPropertiesKHR::memory_bind_index`] from[`crate::vk::DeviceLoader::get_video_session_memory_requirements_khr`].\n\n* [`Self::memory`] is the allocated device memory to be bound to the video\n  session’s heap with index [`Self::memory_bind_index`].\n\n* [`Self::memory_offset`] is the start offset of the region of [`Self::memory`]which is to be bound.\n\n* [`Self::memory_size`] is the size in bytes of the region of [`Self::memory`],\n  starting from [`Self::memory_offset`] bytes, to be bound.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoBindMemoryKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_BIND_MEMORY_KHR`]\n\n* []() VUID-VkVideoBindMemoryKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoBindMemoryKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::bind_video_session_memory_khr`]\n"]
#[doc(alias = "VkVideoBindMemoryKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoBindMemoryKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory_bind_index: u32,
    pub memory: crate::vk1_0::DeviceMemory,
    pub memory_offset: crate::vk1_0::DeviceSize,
    pub memory_size: crate::vk1_0::DeviceSize,
}
impl VideoBindMemoryKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_BIND_MEMORY_KHR;
}
impl Default for VideoBindMemoryKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory_bind_index: Default::default(), memory: Default::default(), memory_offset: Default::default(), memory_size: Default::default() }
    }
}
impl std::fmt::Debug for VideoBindMemoryKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoBindMemoryKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_bind_index", &self.memory_bind_index).field("memory", &self.memory).field("memory_offset", &self.memory_offset).field("memory_size", &self.memory_size).finish()
    }
}
impl VideoBindMemoryKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoBindMemoryKHRBuilder<'a> {
        VideoBindMemoryKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoBindMemoryKHR.html)) · Builder of [`VideoBindMemoryKHR`] <br/> VkVideoBindMemoryKHR - Structure specifying device memory heap entry for video session object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoBindMemoryKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoBindMemoryKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           memoryBindIndex;\n    VkDeviceMemory     memory;\n    VkDeviceSize       memoryOffset;\n    VkDeviceSize       memorySize;\n} VkVideoBindMemoryKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_bind_index`] is the index of the device memory heap returned in[`crate::vk::VideoGetMemoryPropertiesKHR::memory_bind_index`] from[`crate::vk::DeviceLoader::get_video_session_memory_requirements_khr`].\n\n* [`Self::memory`] is the allocated device memory to be bound to the video\n  session’s heap with index [`Self::memory_bind_index`].\n\n* [`Self::memory_offset`] is the start offset of the region of [`Self::memory`]which is to be bound.\n\n* [`Self::memory_size`] is the size in bytes of the region of [`Self::memory`],\n  starting from [`Self::memory_offset`] bytes, to be bound.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoBindMemoryKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_BIND_MEMORY_KHR`]\n\n* []() VUID-VkVideoBindMemoryKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoBindMemoryKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::bind_video_session_memory_khr`]\n"]
#[repr(transparent)]
pub struct VideoBindMemoryKHRBuilder<'a>(VideoBindMemoryKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoBindMemoryKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoBindMemoryKHRBuilder<'a> {
        VideoBindMemoryKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_bind_index(mut self, memory_bind_index: u32) -> Self {
        self.0.memory_bind_index = memory_bind_index as _;
        self
    }
    #[inline]
    pub fn memory(mut self, memory: crate::vk1_0::DeviceMemory) -> Self {
        self.0.memory = memory as _;
        self
    }
    #[inline]
    pub fn memory_offset(mut self, memory_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.memory_offset = memory_offset as _;
        self
    }
    #[inline]
    pub fn memory_size(mut self, memory_size: crate::vk1_0::DeviceSize) -> Self {
        self.0.memory_size = memory_size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoBindMemoryKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoBindMemoryKHRBuilder<'a> {
    fn default() -> VideoBindMemoryKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoBindMemoryKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoBindMemoryKHRBuilder<'a> {
    type Target = VideoBindMemoryKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoBindMemoryKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoPictureResourceKHR.html)) · Structure <br/> VkVideoPictureResourceKHR - Structure specifying the picture resources\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoPictureResourceKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoPictureResourceKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkOffset2D         codedOffset;\n    VkExtent2D         codedExtent;\n    uint32_t           baseArrayLayer;\n    VkImageView        imageViewBinding;\n} VkVideoPictureResourceKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::coded_offset`] is the offset to be used for the picture resource.\n\n* [`Self::coded_extent`] is the extent to be used for the picture resource.\n\n* [`Self::base_array_layer`] is the first array layer to be accessed for the\n  Decode or Encode Operations.\n\n* [`Self::image_view_binding`] is a [`crate::vk::ImageView`] image view representing\n  this picture resource.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoPictureResourceKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PICTURE_RESOURCE_KHR`]\n\n* []() VUID-VkVideoPictureResourceKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoPictureResourceKHR-imageViewBinding-parameter  \n  [`Self::image_view_binding`] **must** be a valid [`crate::vk::ImageView`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::ImageView`], [`crate::vk::Offset2D`], [`crate::vk::StructureType`], [`crate::vk::VideoDecodeInfoKHR`], [`crate::vk::VideoEncodeInfoKHR`], [`crate::vk::VideoReferenceSlotKHR`]\n"]
#[doc(alias = "VkVideoPictureResourceKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoPictureResourceKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub coded_offset: crate::vk1_0::Offset2D,
    pub coded_extent: crate::vk1_0::Extent2D,
    pub base_array_layer: u32,
    pub image_view_binding: crate::vk1_0::ImageView,
}
impl VideoPictureResourceKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_PICTURE_RESOURCE_KHR;
}
impl Default for VideoPictureResourceKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), coded_offset: Default::default(), coded_extent: Default::default(), base_array_layer: Default::default(), image_view_binding: Default::default() }
    }
}
impl std::fmt::Debug for VideoPictureResourceKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoPictureResourceKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("coded_offset", &self.coded_offset).field("coded_extent", &self.coded_extent).field("base_array_layer", &self.base_array_layer).field("image_view_binding", &self.image_view_binding).finish()
    }
}
impl VideoPictureResourceKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoPictureResourceKHRBuilder<'a> {
        VideoPictureResourceKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoPictureResourceKHR.html)) · Builder of [`VideoPictureResourceKHR`] <br/> VkVideoPictureResourceKHR - Structure specifying the picture resources\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoPictureResourceKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoPictureResourceKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkOffset2D         codedOffset;\n    VkExtent2D         codedExtent;\n    uint32_t           baseArrayLayer;\n    VkImageView        imageViewBinding;\n} VkVideoPictureResourceKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::coded_offset`] is the offset to be used for the picture resource.\n\n* [`Self::coded_extent`] is the extent to be used for the picture resource.\n\n* [`Self::base_array_layer`] is the first array layer to be accessed for the\n  Decode or Encode Operations.\n\n* [`Self::image_view_binding`] is a [`crate::vk::ImageView`] image view representing\n  this picture resource.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoPictureResourceKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_PICTURE_RESOURCE_KHR`]\n\n* []() VUID-VkVideoPictureResourceKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoPictureResourceKHR-imageViewBinding-parameter  \n  [`Self::image_view_binding`] **must** be a valid [`crate::vk::ImageView`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::ImageView`], [`crate::vk::Offset2D`], [`crate::vk::StructureType`], [`crate::vk::VideoDecodeInfoKHR`], [`crate::vk::VideoEncodeInfoKHR`], [`crate::vk::VideoReferenceSlotKHR`]\n"]
#[repr(transparent)]
pub struct VideoPictureResourceKHRBuilder<'a>(VideoPictureResourceKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoPictureResourceKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoPictureResourceKHRBuilder<'a> {
        VideoPictureResourceKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn coded_offset(mut self, coded_offset: crate::vk1_0::Offset2D) -> Self {
        self.0.coded_offset = coded_offset as _;
        self
    }
    #[inline]
    pub fn coded_extent(mut self, coded_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.coded_extent = coded_extent as _;
        self
    }
    #[inline]
    pub fn base_array_layer(mut self, base_array_layer: u32) -> Self {
        self.0.base_array_layer = base_array_layer as _;
        self
    }
    #[inline]
    pub fn image_view_binding(mut self, image_view_binding: crate::vk1_0::ImageView) -> Self {
        self.0.image_view_binding = image_view_binding as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoPictureResourceKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoPictureResourceKHRBuilder<'a> {
    fn default() -> VideoPictureResourceKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoPictureResourceKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoPictureResourceKHRBuilder<'a> {
    type Target = VideoPictureResourceKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoPictureResourceKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoReferenceSlotKHR.html)) · Structure <br/> VkVideoReferenceSlotKHR - Structure specifying the reference picture slot\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoReferenceSlotKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoReferenceSlotKHR {\n    VkStructureType                     sType;\n    const void*                         pNext;\n    int8_t                              slotIndex;\n    const VkVideoPictureResourceKHR*    pPictureResource;\n} VkVideoReferenceSlotKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::slot_index`] is the unique reference slot index used for the encode\n  or decode operation.\n\n* [`Self::p_picture_resource`] is a pointer to a [`crate::vk::VideoPictureResourceKHR`]structure describing the picture resource bound to this slot index.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoReferenceSlotKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_REFERENCE_SLOT_KHR`]\n\n* []() VUID-VkVideoReferenceSlotKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264DpbSlotInfoEXT`] or [`crate::vk::VideoDecodeH265DpbSlotInfoEXT`]\n\n* []() VUID-VkVideoReferenceSlotKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoReferenceSlotKHR-pPictureResource-parameter  \n  [`Self::p_picture_resource`] **must** be a valid pointer to a valid [`crate::vk::VideoPictureResourceKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoBeginCodingInfoKHR`], [`crate::vk::VideoDecodeInfoKHR`], [`crate::vk::VideoEncodeInfoKHR`], [`crate::vk::VideoPictureResourceKHR`]\n"]
#[doc(alias = "VkVideoReferenceSlotKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoReferenceSlotKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub slot_index: i8,
    pub p_picture_resource: *const crate::extensions::khr_video_queue::VideoPictureResourceKHR,
}
impl VideoReferenceSlotKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_REFERENCE_SLOT_KHR;
}
impl Default for VideoReferenceSlotKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), slot_index: Default::default(), p_picture_resource: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoReferenceSlotKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoReferenceSlotKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("slot_index", &self.slot_index).field("p_picture_resource", &self.p_picture_resource).finish()
    }
}
impl VideoReferenceSlotKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoReferenceSlotKHRBuilder<'a> {
        VideoReferenceSlotKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoReferenceSlotKHR.html)) · Builder of [`VideoReferenceSlotKHR`] <br/> VkVideoReferenceSlotKHR - Structure specifying the reference picture slot\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoReferenceSlotKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoReferenceSlotKHR {\n    VkStructureType                     sType;\n    const void*                         pNext;\n    int8_t                              slotIndex;\n    const VkVideoPictureResourceKHR*    pPictureResource;\n} VkVideoReferenceSlotKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::slot_index`] is the unique reference slot index used for the encode\n  or decode operation.\n\n* [`Self::p_picture_resource`] is a pointer to a [`crate::vk::VideoPictureResourceKHR`]structure describing the picture resource bound to this slot index.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoReferenceSlotKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_REFERENCE_SLOT_KHR`]\n\n* []() VUID-VkVideoReferenceSlotKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264DpbSlotInfoEXT`] or [`crate::vk::VideoDecodeH265DpbSlotInfoEXT`]\n\n* []() VUID-VkVideoReferenceSlotKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoReferenceSlotKHR-pPictureResource-parameter  \n  [`Self::p_picture_resource`] **must** be a valid pointer to a valid [`crate::vk::VideoPictureResourceKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoBeginCodingInfoKHR`], [`crate::vk::VideoDecodeInfoKHR`], [`crate::vk::VideoEncodeInfoKHR`], [`crate::vk::VideoPictureResourceKHR`]\n"]
#[repr(transparent)]
pub struct VideoReferenceSlotKHRBuilder<'a>(VideoReferenceSlotKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoReferenceSlotKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoReferenceSlotKHRBuilder<'a> {
        VideoReferenceSlotKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn slot_index(mut self, slot_index: i8) -> Self {
        self.0.slot_index = slot_index as _;
        self
    }
    #[inline]
    pub fn picture_resource(mut self, picture_resource: &'a crate::extensions::khr_video_queue::VideoPictureResourceKHR) -> Self {
        self.0.p_picture_resource = picture_resource as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoReferenceSlotKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoReferenceSlotKHRBuilder<'a> {
    fn default() -> VideoReferenceSlotKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoReferenceSlotKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoReferenceSlotKHRBuilder<'a> {
    type Target = VideoReferenceSlotKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoReferenceSlotKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateInfoKHR.html)) · Structure <br/> VkVideoSessionCreateInfoKHR - Structure specifying parameters of a newly created video decode session\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionCreateInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    uint32_t                        queueFamilyIndex;\n    VkVideoSessionCreateFlagsKHR    flags;\n    const VkVideoProfileKHR*        pVideoProfile;\n    VkFormat                        pictureFormat;\n    VkExtent2D                      maxCodedExtent;\n    VkFormat                        referencePicturesFormat;\n    uint32_t                        maxReferencePicturesSlotsCount;\n    uint32_t                        maxReferencePicturesActiveCount;\n} VkVideoSessionCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::queue_family_index`] is the queue family of the created video session.\n\n* [`Self::flags`] is a bitmask of [VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html)specifying creation flags.\n\n* [`Self::p_video_profile`] is a pointer to a [`crate::vk::VideoProfileKHR`] structure.\n\n* [`Self::picture_format`] is the format of the [image\n  views](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-image-views) representing decoded [Output](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#decoded-output-picture) or\n  encoded [Input](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#input-encode-picture) pictures.\n\n* [`Self::max_coded_extent`] is the maximum width and height of the coded\n  pictures that this instance will be able to support.\n\n* [`Self::reference_pictures_format`] is the format of the [DPB](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb) image\n  views representing the [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture).\n\n* [`Self::max_reference_pictures_slots_count`] is the maximum number of[DPB Slots](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb-slot) that can be activated with associated[Video Picture Resources](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#video-picture-resources) for the created\n  video session.\n\n* [`Self::max_reference_pictures_active_count`] is the maximum number of active[DPB Slots](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb-slot) that can be used as Dpb or Reconstructed[Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) within a single decode or\n  encode operation for the created video session.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pVideoProfile-04845  \n  [`Self::p_video_profile`] **must** be a pointer to a valid[`crate::vk::VideoProfileKHR`] structure whose [`Self::p_next`] chain **must** include\n  a valid codec-specific profile structure.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesSlotsCount-04846  \n   If [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) are required for use with\n  the created video session, the [`Self::max_reference_pictures_slots_count`]**must** be set to a value bigger than `0`.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesSlotsCount-04847  \n  [`Self::max_reference_pictures_slots_count`] **cannot** exceed the implementation\n  reported[`crate::vk::VideoCapabilitiesKHR::max_reference_pictures_slots_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04848  \n   If [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) are required for use with\n  the created video session, the [`Self::max_reference_pictures_active_count`]**must** be set to a value bigger than `0`.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04849  \n  [`Self::max_reference_pictures_active_count`] **cannot** exceed the implementation\n  reported[`crate::vk::VideoCapabilitiesKHR::max_reference_pictures_active_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04850  \n  [`Self::max_reference_pictures_active_count`] **cannot** exceed the[`Self::max_reference_pictures_slots_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxCodedExtent-04851  \n  [`Self::max_coded_extent`] **cannot** be smaller than[`crate::vk::VideoCapabilitiesKHR::min_extent`] and bigger than[`crate::vk::VideoCapabilitiesKHR::max_extent`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-referencePicturesFormat-04852  \n  [`Self::reference_pictures_format`] **must** be one of the supported formats in[`crate::vk::VideoFormatPropertiesKHR`] `format` returned by the[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when the[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DPB_KHR`] or[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_DPB_KHR`] depending on the session\n  codec operation.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-04853  \n  [`Self::picture_format`] for decode output **must** be one of the supported\n  formats in [`crate::vk::VideoFormatPropertiesKHR`] `format` returned by the[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when the[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DST_KHR`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-04854  \n  [`Self::picture_format`] targeting encode operations **must** be one of the\n  supported formats in [`crate::vk::VideoFormatPropertiesKHR`] `format`returned by the [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when\n  the [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_SRC_KHR`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_CREATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`], [`crate::vk::VideoDecodeH265SessionCreateInfoEXT`], or [`crate::vk::VideoEncodeH264SessionCreateInfoEXT`]\n\n* []() VUID-VkVideoSessionCreateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoSessionCreateInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html) values\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pVideoProfile-parameter  \n  [`Self::p_video_profile`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-parameter  \n  [`Self::picture_format`] **must** be a valid [`crate::vk::Format`] value\n\n* []() VUID-VkVideoSessionCreateInfoKHR-referencePicturesFormat-parameter  \n  [`Self::reference_pictures_format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::VideoProfileKHR`], [`crate::vk::VideoSessionCreateFlagBitsKHR`], [`crate::vk::DeviceLoader::create_video_session_khr`]\n"]
#[doc(alias = "VkVideoSessionCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoSessionCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub queue_family_index: u32,
    pub flags: crate::extensions::khr_video_queue::VideoSessionCreateFlagsKHR,
    pub p_video_profile: *const crate::extensions::khr_video_queue::VideoProfileKHR,
    pub picture_format: crate::vk1_0::Format,
    pub max_coded_extent: crate::vk1_0::Extent2D,
    pub reference_pictures_format: crate::vk1_0::Format,
    pub max_reference_pictures_slots_count: u32,
    pub max_reference_pictures_active_count: u32,
}
impl VideoSessionCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_SESSION_CREATE_INFO_KHR;
}
impl Default for VideoSessionCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), queue_family_index: Default::default(), flags: Default::default(), p_video_profile: std::ptr::null(), picture_format: Default::default(), max_coded_extent: Default::default(), reference_pictures_format: Default::default(), max_reference_pictures_slots_count: Default::default(), max_reference_pictures_active_count: Default::default() }
    }
}
impl std::fmt::Debug for VideoSessionCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoSessionCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("queue_family_index", &self.queue_family_index).field("flags", &self.flags).field("p_video_profile", &self.p_video_profile).field("picture_format", &self.picture_format).field("max_coded_extent", &self.max_coded_extent).field("reference_pictures_format", &self.reference_pictures_format).field("max_reference_pictures_slots_count", &self.max_reference_pictures_slots_count).field("max_reference_pictures_active_count", &self.max_reference_pictures_active_count).finish()
    }
}
impl VideoSessionCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoSessionCreateInfoKHRBuilder<'a> {
        VideoSessionCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateInfoKHR.html)) · Builder of [`VideoSessionCreateInfoKHR`] <br/> VkVideoSessionCreateInfoKHR - Structure specifying parameters of a newly created video decode session\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionCreateInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    uint32_t                        queueFamilyIndex;\n    VkVideoSessionCreateFlagsKHR    flags;\n    const VkVideoProfileKHR*        pVideoProfile;\n    VkFormat                        pictureFormat;\n    VkExtent2D                      maxCodedExtent;\n    VkFormat                        referencePicturesFormat;\n    uint32_t                        maxReferencePicturesSlotsCount;\n    uint32_t                        maxReferencePicturesActiveCount;\n} VkVideoSessionCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::queue_family_index`] is the queue family of the created video session.\n\n* [`Self::flags`] is a bitmask of [VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html)specifying creation flags.\n\n* [`Self::p_video_profile`] is a pointer to a [`crate::vk::VideoProfileKHR`] structure.\n\n* [`Self::picture_format`] is the format of the [image\n  views](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-image-views) representing decoded [Output](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#decoded-output-picture) or\n  encoded [Input](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#input-encode-picture) pictures.\n\n* [`Self::max_coded_extent`] is the maximum width and height of the coded\n  pictures that this instance will be able to support.\n\n* [`Self::reference_pictures_format`] is the format of the [DPB](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb) image\n  views representing the [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture).\n\n* [`Self::max_reference_pictures_slots_count`] is the maximum number of[DPB Slots](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb-slot) that can be activated with associated[Video Picture Resources](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#video-picture-resources) for the created\n  video session.\n\n* [`Self::max_reference_pictures_active_count`] is the maximum number of active[DPB Slots](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#dpb-slot) that can be used as Dpb or Reconstructed[Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) within a single decode or\n  encode operation for the created video session.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pVideoProfile-04845  \n  [`Self::p_video_profile`] **must** be a pointer to a valid[`crate::vk::VideoProfileKHR`] structure whose [`Self::p_next`] chain **must** include\n  a valid codec-specific profile structure.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesSlotsCount-04846  \n   If [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) are required for use with\n  the created video session, the [`Self::max_reference_pictures_slots_count`]**must** be set to a value bigger than `0`.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesSlotsCount-04847  \n  [`Self::max_reference_pictures_slots_count`] **cannot** exceed the implementation\n  reported[`crate::vk::VideoCapabilitiesKHR::max_reference_pictures_slots_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04848  \n   If [Reference Pictures](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#reference-picture) are required for use with\n  the created video session, the [`Self::max_reference_pictures_active_count`]**must** be set to a value bigger than `0`.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04849  \n  [`Self::max_reference_pictures_active_count`] **cannot** exceed the implementation\n  reported[`crate::vk::VideoCapabilitiesKHR::max_reference_pictures_active_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxReferencePicturesActiveCount-04850  \n  [`Self::max_reference_pictures_active_count`] **cannot** exceed the[`Self::max_reference_pictures_slots_count`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-maxCodedExtent-04851  \n  [`Self::max_coded_extent`] **cannot** be smaller than[`crate::vk::VideoCapabilitiesKHR::min_extent`] and bigger than[`crate::vk::VideoCapabilitiesKHR::max_extent`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-referencePicturesFormat-04852  \n  [`Self::reference_pictures_format`] **must** be one of the supported formats in[`crate::vk::VideoFormatPropertiesKHR`] `format` returned by the[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when the[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DPB_KHR`] or[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_DPB_KHR`] depending on the session\n  codec operation.\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-04853  \n  [`Self::picture_format`] for decode output **must** be one of the supported\n  formats in [`crate::vk::VideoFormatPropertiesKHR`] `format` returned by the[`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when the[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DST_KHR`].\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-04854  \n  [`Self::picture_format`] targeting encode operations **must** be one of the\n  supported formats in [`crate::vk::VideoFormatPropertiesKHR`] `format`returned by the [`crate::vk::DeviceLoader::get_physical_device_video_format_properties_khr`] when\n  the [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] `imageUsage` contains[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_SRC_KHR`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_CREATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`], [`crate::vk::VideoDecodeH265SessionCreateInfoEXT`], or [`crate::vk::VideoEncodeH264SessionCreateInfoEXT`]\n\n* []() VUID-VkVideoSessionCreateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoSessionCreateInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkVideoSessionCreateFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionCreateFlagBitsKHR.html) values\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pVideoProfile-parameter  \n  [`Self::p_video_profile`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n\n* []() VUID-VkVideoSessionCreateInfoKHR-pictureFormat-parameter  \n  [`Self::picture_format`] **must** be a valid [`crate::vk::Format`] value\n\n* []() VUID-VkVideoSessionCreateInfoKHR-referencePicturesFormat-parameter  \n  [`Self::reference_pictures_format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::Format`], [`crate::vk::StructureType`], [`crate::vk::VideoProfileKHR`], [`crate::vk::VideoSessionCreateFlagBitsKHR`], [`crate::vk::DeviceLoader::create_video_session_khr`]\n"]
#[repr(transparent)]
pub struct VideoSessionCreateInfoKHRBuilder<'a>(VideoSessionCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoSessionCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoSessionCreateInfoKHRBuilder<'a> {
        VideoSessionCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn queue_family_index(mut self, queue_family_index: u32) -> Self {
        self.0.queue_family_index = queue_family_index as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_video_queue::VideoSessionCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn video_profile(mut self, video_profile: &'a crate::extensions::khr_video_queue::VideoProfileKHR) -> Self {
        self.0.p_video_profile = video_profile as _;
        self
    }
    #[inline]
    pub fn picture_format(mut self, picture_format: crate::vk1_0::Format) -> Self {
        self.0.picture_format = picture_format as _;
        self
    }
    #[inline]
    pub fn max_coded_extent(mut self, max_coded_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.max_coded_extent = max_coded_extent as _;
        self
    }
    #[inline]
    pub fn reference_pictures_format(mut self, reference_pictures_format: crate::vk1_0::Format) -> Self {
        self.0.reference_pictures_format = reference_pictures_format as _;
        self
    }
    #[inline]
    pub fn max_reference_pictures_slots_count(mut self, max_reference_pictures_slots_count: u32) -> Self {
        self.0.max_reference_pictures_slots_count = max_reference_pictures_slots_count as _;
        self
    }
    #[inline]
    pub fn max_reference_pictures_active_count(mut self, max_reference_pictures_active_count: u32) -> Self {
        self.0.max_reference_pictures_active_count = max_reference_pictures_active_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoSessionCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoSessionCreateInfoKHRBuilder<'a> {
    fn default() -> VideoSessionCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoSessionCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoSessionCreateInfoKHRBuilder<'a> {
    type Target = VideoSessionCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoSessionCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionParametersCreateInfoKHR.html)) · Structure <br/> VkVideoSessionParametersCreateInfoKHR - Structure to set video session parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionParametersCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionParametersCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkVideoSessionParametersKHR    videoSessionParametersTemplate;\n    VkVideoSessionKHR              videoSession;\n} VkVideoSessionParametersCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_session_parameters_template`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a valid\n  handle to a [`crate::vk::VideoSessionParametersKHR`] object.\n  If this parameter represents a valid handle, then the underlying Video\n  Session Parameters object will be used as a template for constructing\n  the new video session parameters object.\n  All of the template object’s current parameters will be inherited by the\n  new object in such a case.\n  Optionally, some of the template’s parameters can be updated or new\n  parameters added to the newly constructed object via the\n  extension-specific parameters.\n\n* [`Self::video_session`] is the video session object against which the video\n  session parameters object is going to be created.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-04855  \n   If [`Self::video_session_parameters_template`] represents a valid handle, it**must** have been created against [`Self::video_session`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`], [`crate::vk::VideoDecodeH265SessionParametersCreateInfoEXT`], or [`crate::vk::VideoEncodeH264SessionParametersCreateInfoEXT`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-parameter  \n  [`Self::video_session_parameters_template`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-parent  \n  [`Self::video_session_parameters_template`] **must** have been created, allocated, or retrieved from [`Self::video_session`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-commonparent  \n   Both of [`Self::video_session`], and [`Self::video_session_parameters_template`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoSessionKHR`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::DeviceLoader::create_video_session_parameters_khr`]\n"]
#[doc(alias = "VkVideoSessionParametersCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoSessionParametersCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub video_session_parameters_template: crate::extensions::khr_video_queue::VideoSessionParametersKHR,
    pub video_session: crate::extensions::khr_video_queue::VideoSessionKHR,
}
impl VideoSessionParametersCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR;
}
impl Default for VideoSessionParametersCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), video_session_parameters_template: Default::default(), video_session: Default::default() }
    }
}
impl std::fmt::Debug for VideoSessionParametersCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoSessionParametersCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("video_session_parameters_template", &self.video_session_parameters_template).field("video_session", &self.video_session).finish()
    }
}
impl VideoSessionParametersCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoSessionParametersCreateInfoKHRBuilder<'a> {
        VideoSessionParametersCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionParametersCreateInfoKHR.html)) · Builder of [`VideoSessionParametersCreateInfoKHR`] <br/> VkVideoSessionParametersCreateInfoKHR - Structure to set video session parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionParametersCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionParametersCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkVideoSessionParametersKHR    videoSessionParametersTemplate;\n    VkVideoSessionKHR              videoSession;\n} VkVideoSessionParametersCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::video_session_parameters_template`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a valid\n  handle to a [`crate::vk::VideoSessionParametersKHR`] object.\n  If this parameter represents a valid handle, then the underlying Video\n  Session Parameters object will be used as a template for constructing\n  the new video session parameters object.\n  All of the template object’s current parameters will be inherited by the\n  new object in such a case.\n  Optionally, some of the template’s parameters can be updated or new\n  parameters added to the newly constructed object via the\n  extension-specific parameters.\n\n* [`Self::video_session`] is the video session object against which the video\n  session parameters object is going to be created.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-04855  \n   If [`Self::video_session_parameters_template`] represents a valid handle, it**must** have been created against [`Self::video_session`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`], [`crate::vk::VideoDecodeH265SessionParametersCreateInfoEXT`], or [`crate::vk::VideoEncodeH264SessionParametersCreateInfoEXT`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-parameter  \n  [`Self::video_session_parameters_template`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-videoSessionParametersTemplate-parent  \n  [`Self::video_session_parameters_template`] **must** have been created, allocated, or retrieved from [`Self::video_session`]\n\n* []() VUID-VkVideoSessionParametersCreateInfoKHR-commonparent  \n   Both of [`Self::video_session`], and [`Self::video_session_parameters_template`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoSessionKHR`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::DeviceLoader::create_video_session_parameters_khr`]\n"]
#[repr(transparent)]
pub struct VideoSessionParametersCreateInfoKHRBuilder<'a>(VideoSessionParametersCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoSessionParametersCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoSessionParametersCreateInfoKHRBuilder<'a> {
        VideoSessionParametersCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn video_session_parameters_template(mut self, video_session_parameters_template: crate::extensions::khr_video_queue::VideoSessionParametersKHR) -> Self {
        self.0.video_session_parameters_template = video_session_parameters_template as _;
        self
    }
    #[inline]
    pub fn video_session(mut self, video_session: crate::extensions::khr_video_queue::VideoSessionKHR) -> Self {
        self.0.video_session = video_session as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoSessionParametersCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoSessionParametersCreateInfoKHRBuilder<'a> {
    fn default() -> VideoSessionParametersCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoSessionParametersCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoSessionParametersCreateInfoKHRBuilder<'a> {
    type Target = VideoSessionParametersCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoSessionParametersCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionParametersUpdateInfoKHR.html)) · Structure <br/> VkVideoSessionParametersUpdateInfoKHR - Structure to update video session parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionParametersUpdateInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           updateSequenceCount;\n} VkVideoSessionParametersUpdateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::update_sequence_count`] is the sequence number of the object update\n  with parameters, starting from `1` and incrementing the value by one\n  with each subsequent update.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`], [`crate::vk::VideoDecodeH265SessionParametersAddInfoEXT`], or [`crate::vk::VideoEncodeH264SessionParametersAddInfoEXT`]\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::update_video_session_parameters_khr`]\n"]
#[doc(alias = "VkVideoSessionParametersUpdateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoSessionParametersUpdateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub update_sequence_count: u32,
}
impl VideoSessionParametersUpdateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR;
}
impl Default for VideoSessionParametersUpdateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), update_sequence_count: Default::default() }
    }
}
impl std::fmt::Debug for VideoSessionParametersUpdateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoSessionParametersUpdateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("update_sequence_count", &self.update_sequence_count).finish()
    }
}
impl VideoSessionParametersUpdateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoSessionParametersUpdateInfoKHRBuilder<'a> {
        VideoSessionParametersUpdateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoSessionParametersUpdateInfoKHR.html)) · Builder of [`VideoSessionParametersUpdateInfoKHR`] <br/> VkVideoSessionParametersUpdateInfoKHR - Structure to update video session parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoSessionParametersUpdateInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           updateSequenceCount;\n} VkVideoSessionParametersUpdateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::update_sequence_count`] is the sequence number of the object update\n  with parameters, starting from `1` and incrementing the value by one\n  with each subsequent update.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR`]\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`], [`crate::vk::VideoDecodeH265SessionParametersAddInfoEXT`], or [`crate::vk::VideoEncodeH264SessionParametersAddInfoEXT`]\n\n* []() VUID-VkVideoSessionParametersUpdateInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::update_video_session_parameters_khr`]\n"]
#[repr(transparent)]
pub struct VideoSessionParametersUpdateInfoKHRBuilder<'a>(VideoSessionParametersUpdateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoSessionParametersUpdateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoSessionParametersUpdateInfoKHRBuilder<'a> {
        VideoSessionParametersUpdateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn update_sequence_count(mut self, update_sequence_count: u32) -> Self {
        self.0.update_sequence_count = update_sequence_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoSessionParametersUpdateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoSessionParametersUpdateInfoKHRBuilder<'a> {
    fn default() -> VideoSessionParametersUpdateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoSessionParametersUpdateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoSessionParametersUpdateInfoKHRBuilder<'a> {
    type Target = VideoSessionParametersUpdateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoSessionParametersUpdateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoBeginCodingInfoKHR.html)) · Structure <br/> VkVideoBeginCodingInfoKHR - Structure specifying parameters of decode starts\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoBeginCodingInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoBeginCodingInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkVideoBeginCodingFlagsKHR            flags;\n    VkVideoCodingQualityPresetFlagsKHR    codecQualityPreset;\n    VkVideoSessionKHR                     videoSession;\n    VkVideoSessionParametersKHR           videoSessionParameters;\n    uint32_t                              referenceSlotCount;\n    const VkVideoReferenceSlotKHR*        pReferenceSlots;\n} VkVideoBeginCodingInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::codec_quality_preset`] is a bitmask of[VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html) specifying the Video Decode\n  or Encode quality preset.\n\n* [`Self::video_session`] is the video session object to be bound for the\n  processing of the video commands.\n\n* [`Self::video_session_parameters`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of a[`crate::vk::VideoSessionParametersKHR`] object to be used for the processing\n  of the video commands.\n  If [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), then no video session parameters apply to this\n  command buffer context.\n\n* [`Self::reference_slot_count`] is the number of reference slot entries\n  provided in [`Self::p_reference_slots`].\n\n* [`Self::p_reference_slots`] is a pointer to an array of[`crate::vk::VideoReferenceSlotKHR`] structures specifying reference slots,\n  used within the video command context between this[`crate::vk::DeviceLoader::cmd_begin_video_coding_khr`] command and the[`crate::vk::DeviceLoader::cmd_end_video_coding_khr`] commmand that follows.\n  Each reference slot provides a slot index and the[`crate::vk::VideoPictureResourceKHR`] specifying the reference picture\n  resource bound to this slot index.\n  A slot index **must** not appear more than once in [`Self::p_reference_slots`] in\n  a given command.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoBeginCodingInfoKHR-referenceSlotCount-04856  \n  [`crate::vk::VideoBeginCodingInfoKHR::reference_slot_count`] **must** not\n  exceed the value specified in[`crate::vk::VideoSessionCreateInfoKHR::max_reference_pictures_slots_count`]when creating the video session object that is being provided in[`Self::video_session`].\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-04857  \n   If [`Self::video_session_parameters`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must**have been created using [`Self::video_session`] as a parent object.\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoBeginCodingInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_BEGIN_CODING_INFO_KHR`]\n\n* []() VUID-VkVideoBeginCodingInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-codecQualityPreset-parameter  \n  [`Self::codec_quality_preset`] **must** be a valid combination of [VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html) values\n\n* []() VUID-VkVideoBeginCodingInfoKHR-codecQualityPreset-requiredbitmask  \n  [`Self::codec_quality_preset`] **must** not be `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-parameter  \n   If [`Self::video_session_parameters`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-VkVideoBeginCodingInfoKHR-pReferenceSlots-parameter  \n  [`Self::p_reference_slots`] **must** be a valid pointer to an array of [`Self::reference_slot_count`] valid [`crate::vk::VideoReferenceSlotKHR`] structures\n\n* []() VUID-VkVideoBeginCodingInfoKHR-referenceSlotCount-arraylength  \n  [`Self::reference_slot_count`] **must** be greater than `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-parent  \n   If [`Self::video_session_parameters`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::video_session`]\n\n* []() VUID-VkVideoBeginCodingInfoKHR-commonparent  \n   Both of [`Self::video_session`], and [`Self::video_session_parameters`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoBeginCodingFlagBitsKHR`], [`crate::vk::VideoCodingQualityPresetFlagBitsKHR`], [`crate::vk::VideoReferenceSlotKHR`], [`crate::vk::VideoSessionKHR`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::DeviceLoader::cmd_begin_video_coding_khr`]\n"]
#[doc(alias = "VkVideoBeginCodingInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoBeginCodingInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_video_queue::VideoBeginCodingFlagsKHR,
    pub codec_quality_preset: crate::extensions::khr_video_queue::VideoCodingQualityPresetFlagsKHR,
    pub video_session: crate::extensions::khr_video_queue::VideoSessionKHR,
    pub video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR,
    pub reference_slot_count: u32,
    pub p_reference_slots: *const crate::extensions::khr_video_queue::VideoReferenceSlotKHR,
}
impl VideoBeginCodingInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_BEGIN_CODING_INFO_KHR;
}
impl Default for VideoBeginCodingInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), codec_quality_preset: Default::default(), video_session: Default::default(), video_session_parameters: Default::default(), reference_slot_count: Default::default(), p_reference_slots: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoBeginCodingInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoBeginCodingInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("codec_quality_preset", &self.codec_quality_preset).field("video_session", &self.video_session).field("video_session_parameters", &self.video_session_parameters).field("reference_slot_count", &self.reference_slot_count).field("p_reference_slots", &self.p_reference_slots).finish()
    }
}
impl VideoBeginCodingInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoBeginCodingInfoKHRBuilder<'a> {
        VideoBeginCodingInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoBeginCodingInfoKHR.html)) · Builder of [`VideoBeginCodingInfoKHR`] <br/> VkVideoBeginCodingInfoKHR - Structure specifying parameters of decode starts\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoBeginCodingInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoBeginCodingInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkVideoBeginCodingFlagsKHR            flags;\n    VkVideoCodingQualityPresetFlagsKHR    codecQualityPreset;\n    VkVideoSessionKHR                     videoSession;\n    VkVideoSessionParametersKHR           videoSessionParameters;\n    uint32_t                              referenceSlotCount;\n    const VkVideoReferenceSlotKHR*        pReferenceSlots;\n} VkVideoBeginCodingInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::codec_quality_preset`] is a bitmask of[VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html) specifying the Video Decode\n  or Encode quality preset.\n\n* [`Self::video_session`] is the video session object to be bound for the\n  processing of the video commands.\n\n* [`Self::video_session_parameters`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of a[`crate::vk::VideoSessionParametersKHR`] object to be used for the processing\n  of the video commands.\n  If [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), then no video session parameters apply to this\n  command buffer context.\n\n* [`Self::reference_slot_count`] is the number of reference slot entries\n  provided in [`Self::p_reference_slots`].\n\n* [`Self::p_reference_slots`] is a pointer to an array of[`crate::vk::VideoReferenceSlotKHR`] structures specifying reference slots,\n  used within the video command context between this[`crate::vk::DeviceLoader::cmd_begin_video_coding_khr`] command and the[`crate::vk::DeviceLoader::cmd_end_video_coding_khr`] commmand that follows.\n  Each reference slot provides a slot index and the[`crate::vk::VideoPictureResourceKHR`] specifying the reference picture\n  resource bound to this slot index.\n  A slot index **must** not appear more than once in [`Self::p_reference_slots`] in\n  a given command.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoBeginCodingInfoKHR-referenceSlotCount-04856  \n  [`crate::vk::VideoBeginCodingInfoKHR::reference_slot_count`] **must** not\n  exceed the value specified in[`crate::vk::VideoSessionCreateInfoKHR::max_reference_pictures_slots_count`]when creating the video session object that is being provided in[`Self::video_session`].\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-04857  \n   If [`Self::video_session_parameters`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must**have been created using [`Self::video_session`] as a parent object.\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoBeginCodingInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_BEGIN_CODING_INFO_KHR`]\n\n* []() VUID-VkVideoBeginCodingInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-codecQualityPreset-parameter  \n  [`Self::codec_quality_preset`] **must** be a valid combination of [VkVideoCodingQualityPresetFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingQualityPresetFlagBitsKHR.html) values\n\n* []() VUID-VkVideoBeginCodingInfoKHR-codecQualityPreset-requiredbitmask  \n  [`Self::codec_quality_preset`] **must** not be `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-parameter  \n   If [`Self::video_session_parameters`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-VkVideoBeginCodingInfoKHR-pReferenceSlots-parameter  \n  [`Self::p_reference_slots`] **must** be a valid pointer to an array of [`Self::reference_slot_count`] valid [`crate::vk::VideoReferenceSlotKHR`] structures\n\n* []() VUID-VkVideoBeginCodingInfoKHR-referenceSlotCount-arraylength  \n  [`Self::reference_slot_count`] **must** be greater than `0`\n\n* []() VUID-VkVideoBeginCodingInfoKHR-videoSessionParameters-parent  \n   If [`Self::video_session_parameters`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::video_session`]\n\n* []() VUID-VkVideoBeginCodingInfoKHR-commonparent  \n   Both of [`Self::video_session`], and [`Self::video_session_parameters`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoBeginCodingFlagBitsKHR`], [`crate::vk::VideoCodingQualityPresetFlagBitsKHR`], [`crate::vk::VideoReferenceSlotKHR`], [`crate::vk::VideoSessionKHR`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::DeviceLoader::cmd_begin_video_coding_khr`]\n"]
#[repr(transparent)]
pub struct VideoBeginCodingInfoKHRBuilder<'a>(VideoBeginCodingInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoBeginCodingInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoBeginCodingInfoKHRBuilder<'a> {
        VideoBeginCodingInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_video_queue::VideoBeginCodingFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn codec_quality_preset(mut self, codec_quality_preset: crate::extensions::khr_video_queue::VideoCodingQualityPresetFlagsKHR) -> Self {
        self.0.codec_quality_preset = codec_quality_preset as _;
        self
    }
    #[inline]
    pub fn video_session(mut self, video_session: crate::extensions::khr_video_queue::VideoSessionKHR) -> Self {
        self.0.video_session = video_session as _;
        self
    }
    #[inline]
    pub fn video_session_parameters(mut self, video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR) -> Self {
        self.0.video_session_parameters = video_session_parameters as _;
        self
    }
    #[inline]
    pub fn reference_slots(mut self, reference_slots: &'a [crate::extensions::khr_video_queue::VideoReferenceSlotKHRBuilder]) -> Self {
        self.0.p_reference_slots = reference_slots.as_ptr() as _;
        self.0.reference_slot_count = reference_slots.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoBeginCodingInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoBeginCodingInfoKHRBuilder<'a> {
    fn default() -> VideoBeginCodingInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoBeginCodingInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoBeginCodingInfoKHRBuilder<'a> {
    type Target = VideoBeginCodingInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoBeginCodingInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoEndCodingInfoKHR.html)) · Structure <br/> VkVideoEndCodingInfoKHR - Structure specifying the end of decode encode commands sequence\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoEndCodingInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoEndCodingInfoKHR {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkVideoEndCodingFlagsKHR    flags;\n} VkVideoEndCodingInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoEndCodingInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_END_CODING_INFO_KHR`]\n\n* []() VUID-VkVideoEndCodingInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoEndCodingInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoEndCodingFlagBitsKHR`], [`crate::vk::DeviceLoader::cmd_end_video_coding_khr`]\n"]
#[doc(alias = "VkVideoEndCodingInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoEndCodingInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_video_queue::VideoEndCodingFlagsKHR,
}
impl VideoEndCodingInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_END_CODING_INFO_KHR;
}
impl Default for VideoEndCodingInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default() }
    }
}
impl std::fmt::Debug for VideoEndCodingInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoEndCodingInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).finish()
    }
}
impl VideoEndCodingInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoEndCodingInfoKHRBuilder<'a> {
        VideoEndCodingInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoEndCodingInfoKHR.html)) · Builder of [`VideoEndCodingInfoKHR`] <br/> VkVideoEndCodingInfoKHR - Structure specifying the end of decode encode commands sequence\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoEndCodingInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoEndCodingInfoKHR {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkVideoEndCodingFlagsKHR    flags;\n} VkVideoEndCodingInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoEndCodingInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_END_CODING_INFO_KHR`]\n\n* []() VUID-VkVideoEndCodingInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkVideoEndCodingInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoEndCodingFlagBitsKHR`], [`crate::vk::DeviceLoader::cmd_end_video_coding_khr`]\n"]
#[repr(transparent)]
pub struct VideoEndCodingInfoKHRBuilder<'a>(VideoEndCodingInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoEndCodingInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoEndCodingInfoKHRBuilder<'a> {
        VideoEndCodingInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_video_queue::VideoEndCodingFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoEndCodingInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoEndCodingInfoKHRBuilder<'a> {
    fn default() -> VideoEndCodingInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoEndCodingInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoEndCodingInfoKHRBuilder<'a> {
    type Target = VideoEndCodingInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoEndCodingInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlInfoKHR.html)) · Structure <br/> VkVideoCodingControlInfoKHR - Structure specifying parameters of decode starts\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoCodingControlInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoCodingControlInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkVideoCodingControlFlagsKHR    flags;\n} VkVideoCodingControlInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::VideoCodingControlFlagBitsKHR`]specifying control flags.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoCodingControlInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_CODING_CONTROL_INFO_KHR`]\n\n* []() VUID-VkVideoCodingControlInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL` or a pointer to a valid instance of [`crate::vk::VideoEncodeRateControlInfoKHR`]\n\n* []() VUID-VkVideoCodingControlInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoCodingControlInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkVideoCodingControlFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoCodingControlFlagBitsKHR`], [`crate::vk::DeviceLoader::cmd_control_video_coding_khr`]\n"]
#[doc(alias = "VkVideoCodingControlInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoCodingControlInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_video_queue::VideoCodingControlFlagsKHR,
}
impl VideoCodingControlInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_CODING_CONTROL_INFO_KHR;
}
impl Default for VideoCodingControlInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default() }
    }
}
impl std::fmt::Debug for VideoCodingControlInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoCodingControlInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).finish()
    }
}
impl VideoCodingControlInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoCodingControlInfoKHRBuilder<'a> {
        VideoCodingControlInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlInfoKHR.html)) · Builder of [`VideoCodingControlInfoKHR`] <br/> VkVideoCodingControlInfoKHR - Structure specifying parameters of decode starts\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoCodingControlInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_video_queue\ntypedef struct VkVideoCodingControlInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkVideoCodingControlFlagsKHR    flags;\n} VkVideoCodingControlInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::VideoCodingControlFlagBitsKHR`]specifying control flags.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoCodingControlInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_CODING_CONTROL_INFO_KHR`]\n\n* []() VUID-VkVideoCodingControlInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL` or a pointer to a valid instance of [`crate::vk::VideoEncodeRateControlInfoKHR`]\n\n* []() VUID-VkVideoCodingControlInfoKHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkVideoCodingControlInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkVideoCodingControlFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoCodingControlFlagBitsKHR.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoCodingControlFlagBitsKHR`], [`crate::vk::DeviceLoader::cmd_control_video_coding_khr`]\n"]
#[repr(transparent)]
pub struct VideoCodingControlInfoKHRBuilder<'a>(VideoCodingControlInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> VideoCodingControlInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> VideoCodingControlInfoKHRBuilder<'a> {
        VideoCodingControlInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_video_queue::VideoCodingControlFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoCodingControlInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for VideoCodingControlInfoKHRBuilder<'a> {
    fn default() -> VideoCodingControlInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoCodingControlInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoCodingControlInfoKHRBuilder<'a> {
    type Target = VideoCodingControlInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoCodingControlInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceVideoCapabilitiesKHR.html)) · Function <br/> vkGetPhysicalDeviceVideoCapabilitiesKHR - Query video decode or encode capabilities\n[](#_c_specification)C Specification\n----------\n\nTo query video decode or encode capabilities for a specific codec, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetPhysicalDeviceVideoCapabilitiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkVideoProfileKHR*                    pVideoProfile,\n    VkVideoCapabilitiesKHR*                     pCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device whose video decode or encode\n  capabilities will be queried.\n\n* [`Self::p_video_profile`] is a pointer to a [`crate::vk::VideoProfileKHR`] structure\n  with a chained codec-operation specific video profile structure.\n\n* [`Self::p_capabilities`] is a pointer to a [`crate::vk::VideoCapabilitiesKHR`]structure in which the capabilities are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-pVideoProfile-parameter  \n  [`Self::p_video_profile`] **must** be a valid pointer to a valid [`crate::vk::VideoProfileKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceVideoCapabilitiesKHR-pCapabilities-parameter  \n  [`Self::p_capabilities`] **must** be a valid pointer to a [`crate::vk::VideoCapabilitiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_EXTENSION_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::VideoCapabilitiesKHR`], [`crate::vk::VideoProfileKHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceVideoCapabilitiesKHR")]
    pub unsafe fn get_physical_device_video_capabilities_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, video_profile: &crate::extensions::khr_video_queue::VideoProfileKHR, capabilities: Option<crate::extensions::khr_video_queue::VideoCapabilitiesKHR>) -> crate::utils::VulkanResult<crate::extensions::khr_video_queue::VideoCapabilitiesKHR> {
        let _function = self.get_physical_device_video_capabilities_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut capabilities = match capabilities {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, video_profile as _, &mut capabilities);
        crate::utils::VulkanResult::new(_return, {
            capabilities.p_next = std::ptr::null_mut() as _;
            capabilities
        })
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceVideoFormatPropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceVideoFormatPropertiesKHR - Query supported Video Decode and Encode image formats\n[](#_c_specification)C Specification\n----------\n\nTo enumerate the supported output, input and DPB image formats for a\nspecific codec operation and video profile, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetPhysicalDeviceVideoFormatPropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceVideoFormatInfoKHR*   pVideoFormatInfo,\n    uint32_t*                                   pVideoFormatPropertyCount,\n    VkVideoFormatPropertiesKHR*                 pVideoFormatProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device being queried.\n\n* [`Self::p_video_format_info`] is a pointer to a[`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] structure specifying the codec\n  and video profile for which information is returned.\n\n* [`Self::p_video_format_property_count`] is a pointer to an integer related to\n  the number of video format properties available or queried, as described\n  below.\n\n* [`Self::p_video_format_properties`] is a pointer to an array of[`crate::vk::VideoFormatPropertiesKHR`] structures in which supported formats\n  are returned.\n[](#_description)Description\n----------\n\nIf [`Self::p_video_format_properties`] is `NULL`, then the number of video format\nproperties supported for the given [`Self::physical_device`] is returned in[`Self::p_video_format_property_count`].\nOtherwise, [`Self::p_video_format_property_count`] **must** point to a variable set by\nthe user to the number of elements in the [`Self::p_video_format_properties`]array, and on return the variable is overwritten with the number of values\nactually written to [`Self::p_video_format_properties`].\nIf the value of [`Self::p_video_format_property_count`] is less than the number of\nvideo format properties supported, at most [`Self::p_video_format_property_count`]values will be written to [`Self::p_video_format_properties`], and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the available values were returned.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-imageUsage-04844  \n   The `imageUsage` enum of [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`]**must** contain at least one of the following video image usage bit(s):[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DST_KHR`],[`crate::vk::ImageUsageFlagBits::VIDEO_DECODE_DPB_KHR`],[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_SRC_KHR`], or[`crate::vk::ImageUsageFlagBits::VIDEO_ENCODE_DPB_KHR`].\n\n|   |Note:<br/><br/>For most use cases, only decode or encode related usage flags are going to<br/>be specified.<br/>For a use case such as transcode, if the image were to be shared between<br/>decode and encode session(s), then both decode and encode related usage<br/>flags **can** be set.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatInfo-parameter  \n  [`Self::p_video_format_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatPropertyCount-parameter  \n  [`Self::p_video_format_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceVideoFormatPropertiesKHR-pVideoFormatProperties-parameter  \n   If the value referenced by [`Self::p_video_format_property_count`] is not `0`, and [`Self::p_video_format_properties`] is not `NULL`, [`Self::p_video_format_properties`] **must** be a valid pointer to an array of [`Self::p_video_format_property_count`] [`crate::vk::VideoFormatPropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_EXTENSION_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceVideoFormatInfoKHR`], [`crate::vk::VideoFormatPropertiesKHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceVideoFormatPropertiesKHR")]
    pub unsafe fn get_physical_device_video_format_properties_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, video_format_info: &crate::extensions::khr_video_queue::PhysicalDeviceVideoFormatInfoKHR, video_format_property_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_video_queue::VideoFormatPropertiesKHR>> {
        let _function = self.get_physical_device_video_format_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut video_format_property_count = match video_format_property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, video_format_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut video_format_properties = crate::SmallVec::from_elem(Default::default(), video_format_property_count as _);
        let _return = _function(physical_device as _, video_format_info as _, &mut video_format_property_count, video_format_properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, video_format_properties)
    }
}
#[doc = "Provided by [`crate::extensions::khr_video_queue`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateVideoSessionKHR.html)) · Function <br/> vkCreateVideoSessionKHR - Creates a video session object\n[](#_c_specification)C Specification\n----------\n\nTo create a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkCreateVideoSessionKHR(\n    VkDevice                                    device,\n    const VkVideoSessionCreateInfoKHR*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkVideoSessionKHR*                          pVideoSession);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that creates the decode or encode\n  session object.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::VideoSessionCreateInfoKHR`]structure containing parameters specifying the creation of the decode or\n  encode session.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_video_session`] is a pointer to a [`crate::vk::VideoSessionKHR`] structure\n  specifying the decode or encode video session object which will be\n  created by this function when it returns [`crate::vk::Result::SUCCESS`]\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateVideoSessionKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateVideoSessionKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionCreateInfoKHR`] structure\n\n* []() VUID-vkCreateVideoSessionKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateVideoSessionKHR-pVideoSession-parameter  \n  [`Self::p_video_session`] **must** be a valid pointer to a [`crate::vk::VideoSessionKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_INCOMPATIBLE_DRIVER`]\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionCreateInfoKHR`], [`crate::vk::VideoSessionKHR`]\n"]
    #[doc(alias = "vkCreateVideoSessionKHR")]
    pub unsafe fn create_video_session_khr(&self, create_info: &crate::extensions::khr_video_queue::VideoSessionCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_video_queue::VideoSessionKHR> {
        let _function = self.create_video_session_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut video_session = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut video_session,
        );
        crate::utils::VulkanResult::new(_return, video_session)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyVideoSessionKHR.html)) · Function <br/> vkDestroyVideoSessionKHR - Destroy decode session object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a decode session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkDestroyVideoSessionKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that was used for the creation of the video\n  session.\n\n* [`Self::video_session`] is the decode or encode video session to be\n  destroyed.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyVideoSessionKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyVideoSessionKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkDestroyVideoSessionKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyVideoSessionKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionKHR`]\n"]
    #[doc(alias = "vkDestroyVideoSessionKHR")]
    pub unsafe fn destroy_video_session_khr(&self, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_video_session_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            video_session as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateVideoSessionParametersKHR.html)) · Function <br/> vkCreateVideoSessionParametersKHR - Creates video session video session parameter object\n[](#_c_specification)C Specification\n----------\n\nTo create a video session parameters object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkCreateVideoSessionParametersKHR(\n    VkDevice                                    device,\n    const VkVideoSessionParametersCreateInfoKHR* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkVideoSessionParametersKHR*                pVideoSessionParameters);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that was used for the creation of the\n  video session object.\n\n* [`Self::p_create_info`] is a pointer to[`crate::vk::VideoSessionParametersCreateInfoKHR`] structure specifying the\n  video session parameters.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_video_session_parameters`] is a pointer to a[`crate::vk::VideoSessionParametersKHR`] handle in which the video session\n  parameters object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionParametersCreateInfoKHR`] structure\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateVideoSessionParametersKHR-pVideoSessionParameters-parameter  \n  [`Self::p_video_session_parameters`] **must** be a valid pointer to a [`crate::vk::VideoSessionParametersKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionParametersCreateInfoKHR`], [`crate::vk::VideoSessionParametersKHR`]\n"]
    #[doc(alias = "vkCreateVideoSessionParametersKHR")]
    pub unsafe fn create_video_session_parameters_khr(&self, create_info: &crate::extensions::khr_video_queue::VideoSessionParametersCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_video_queue::VideoSessionParametersKHR> {
        let _function = self.create_video_session_parameters_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut video_session_parameters = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut video_session_parameters,
        );
        crate::utils::VulkanResult::new(_return, video_session_parameters)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkUpdateVideoSessionParametersKHR.html)) · Function <br/> vkUpdateVideoSessionParametersKHR - Update video session video session parameter object\n[](#_c_specification)C Specification\n----------\n\nTo update, add, or remove video session parameters state, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkUpdateVideoSessionParametersKHR(\n    VkDevice                                    device,\n    VkVideoSessionParametersKHR                 videoSessionParameters,\n    const VkVideoSessionParametersUpdateInfoKHR* pUpdateInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that was used for the creation of the\n  video session object.\n\n* [`Self::video_session_parameters`] is the video session object that is going\n  to be updated.\n\n* [`Self::p_update_info`] is a pointer to a[`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure containing the\n  session parameters update information.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-videoSessionParameters-parameter  \n  [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-vkUpdateVideoSessionParametersKHR-pUpdateInfo-parameter  \n  [`Self::p_update_info`] **must** be a valid pointer to a valid [`crate::vk::VideoSessionParametersUpdateInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoSessionParametersKHR`], [`crate::vk::VideoSessionParametersUpdateInfoKHR`]\n"]
    #[doc(alias = "vkUpdateVideoSessionParametersKHR")]
    pub unsafe fn update_video_session_parameters_khr(&self, video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR, update_info: &crate::extensions::khr_video_queue::VideoSessionParametersUpdateInfoKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.update_video_session_parameters_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, video_session_parameters as _, update_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyVideoSessionParametersKHR.html)) · Function <br/> vkDestroyVideoSessionParametersKHR - Destroy video session parameters object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkDestroyVideoSessionParametersKHR(\n    VkDevice                                    device,\n    VkVideoSessionParametersKHR                 videoSessionParameters,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device the video session was created with.\n\n* [`Self::video_session_parameters`] is the video session parameters object to\n  be destroyed.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-videoSessionParameters-parameter  \n  [`Self::video_session_parameters`] **must** be a valid [`crate::vk::VideoSessionParametersKHR`] handle\n\n* []() VUID-vkDestroyVideoSessionParametersKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::VideoSessionParametersKHR`]\n"]
    #[doc(alias = "vkDestroyVideoSessionParametersKHR")]
    pub unsafe fn destroy_video_session_parameters_khr(&self, video_session_parameters: crate::extensions::khr_video_queue::VideoSessionParametersKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_video_session_parameters_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            video_session_parameters as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetVideoSessionMemoryRequirementsKHR.html)) · Function <br/> vkGetVideoSessionMemoryRequirementsKHR - Get Memory Requirements\n[](#_c_specification)C Specification\n----------\n\nTo get memory requirements for a video session, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkGetVideoSessionMemoryRequirementsKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    uint32_t*                                   pVideoSessionMemoryRequirementsCount,\n    VkVideoGetMemoryPropertiesKHR*              pVideoSessionMemoryRequirements);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the video session.\n\n* [`Self::video_session`] is the video session to query.\n\n* [`Self::p_video_session_memory_requirements_count`] is a pointer to an integer\n  related to the number of memory heap requirements available or queried,\n  as described below.\n\n* [`Self::p_video_session_memory_requirements`] is `NULL` or a pointer to an array\n  of [`crate::vk::VideoGetMemoryPropertiesKHR`] structures in which the memory\n  heap requirements of the video session are returned.\n[](#_description)Description\n----------\n\nIf [`Self::p_video_session_memory_requirements`] is `NULL`, then the number of\nmemory heap types required for the video session is returned in[`Self::p_video_session_memory_requirements_count`].\nOtherwise, [`Self::p_video_session_memory_requirements_count`] **must** point to a\nvariable set by the user with the number of elements in the[`Self::p_video_session_memory_requirements`] array, and on return the variable is\noverwritten with the number of formats actually written to[`Self::p_video_session_memory_requirements`].\nIf [`Self::p_video_session_memory_requirements_count`] is less than the number of\nmemory heap types required for the video session, then at most[`Self::p_video_session_memory_requirements_count`] elements will be written to[`Self::p_video_session_memory_requirements`], and [`crate::vk::Result::INCOMPLETE`] will be\nreturned, instead of [`crate::vk::Result::SUCCESS`], to indicate that not all required\nmemory heap types were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-pVideoSessionMemoryRequirementsCount-parameter  \n  [`Self::p_video_session_memory_requirements_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-pVideoSessionMemoryRequirements-parameter  \n   If the value referenced by [`Self::p_video_session_memory_requirements_count`] is not `0`, and [`Self::p_video_session_memory_requirements`] is not `NULL`, [`Self::p_video_session_memory_requirements`] **must** be a valid pointer to an array of [`Self::p_video_session_memory_requirements_count`] [`crate::vk::VideoGetMemoryPropertiesKHR`] structures\n\n* []() VUID-vkGetVideoSessionMemoryRequirementsKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoGetMemoryPropertiesKHR`], [`crate::vk::VideoSessionKHR`]\n"]
    #[doc(alias = "vkGetVideoSessionMemoryRequirementsKHR")]
    pub unsafe fn get_video_session_memory_requirements_khr(&self, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, video_session_memory_requirements_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_video_queue::VideoGetMemoryPropertiesKHR>> {
        let _function = self.get_video_session_memory_requirements_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut video_session_memory_requirements_count = match video_session_memory_requirements_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(self.handle, video_session as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut video_session_memory_requirements = crate::SmallVec::from_elem(Default::default(), video_session_memory_requirements_count as _);
        let _return = _function(self.handle, video_session as _, &mut video_session_memory_requirements_count, video_session_memory_requirements.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, video_session_memory_requirements)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkBindVideoSessionMemoryKHR.html)) · Function <br/> vkBindVideoSessionMemoryKHR - Bind Video Memory\n[](#_c_specification)C Specification\n----------\n\nTo attach memory to a video session object, call:\n\n```\n// Provided by VK_KHR_video_queue\nVkResult vkBindVideoSessionMemoryKHR(\n    VkDevice                                    device,\n    VkVideoSessionKHR                           videoSession,\n    uint32_t                                    videoSessionBindMemoryCount,\n    const VkVideoBindMemoryKHR*                 pVideoSessionBindMemories);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the video session’s memory.\n\n* [`Self::video_session`] is the video session to be bound with device memory.\n\n* [`Self::video_session_bind_memory_count`] is the number of[`Self::p_video_session_bind_memories`] to be bound.\n\n* [`Self::p_video_session_bind_memories`] is a pointer to an array of[`crate::vk::VideoBindMemoryKHR`] structures specifying memory regions to be\n  bound to a device memory heap.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkBindVideoSessionMemoryKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSession-parameter  \n  [`Self::video_session`] **must** be a valid [`crate::vk::VideoSessionKHR`] handle\n\n* []() VUID-vkBindVideoSessionMemoryKHR-pVideoSessionBindMemories-parameter  \n  [`Self::p_video_session_bind_memories`] **must** be a valid pointer to an array of [`Self::video_session_bind_memory_count`] valid [`crate::vk::VideoBindMemoryKHR`] structures\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSessionBindMemoryCount-arraylength  \n  [`Self::video_session_bind_memory_count`] **must** be greater than `0`\n\n* []() VUID-vkBindVideoSessionMemoryKHR-videoSession-parent  \n  [`Self::video_session`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::VideoBindMemoryKHR`], [`crate::vk::VideoSessionKHR`]\n"]
    #[doc(alias = "vkBindVideoSessionMemoryKHR")]
    pub unsafe fn bind_video_session_memory_khr(&self, video_session: crate::extensions::khr_video_queue::VideoSessionKHR, video_session_bind_memories: &[crate::extensions::khr_video_queue::VideoBindMemoryKHRBuilder]) -> crate::utils::VulkanResult<()> {
        let _function = self.bind_video_session_memory_khr.expect(crate::NOT_LOADED_MESSAGE);
        let video_session_bind_memory_count = video_session_bind_memories.len();
        let _return = _function(self.handle, video_session as _, video_session_bind_memory_count as _, video_session_bind_memories.as_ptr() as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginVideoCodingKHR.html)) · Function <br/> vkCmdBeginVideoCodingKHR - Start decode jobs\n[](#_c_specification)C Specification\n----------\n\nTo start video decode or encode operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdBeginVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoBeginCodingInfoKHR*            pBeginInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be used when recording\n  commands for the video decode or encode operations.\n\n* [`Self::p_begin_info`] is a pointer to a [`crate::vk::VideoBeginCodingInfoKHR`]structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginVideoCodingKHR-pBeginInfo-parameter  \n  [`Self::p_begin_info`] **must** be a valid pointer to a valid [`crate::vk::VideoBeginCodingInfoKHR`] structure\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdBeginVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdBeginVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoBeginCodingInfoKHR`]\n"]
    #[doc(alias = "vkCmdBeginVideoCodingKHR")]
    pub unsafe fn cmd_begin_video_coding_khr(&self, command_buffer: crate::vk1_0::CommandBuffer, begin_info: &crate::extensions::khr_video_queue::VideoBeginCodingInfoKHR) -> () {
        let _function = self.cmd_begin_video_coding_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, begin_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdControlVideoCodingKHR.html)) · Function <br/> vkCmdControlVideoCodingKHR - Set encode rate control parameters\n[](#_c_specification)C Specification\n----------\n\nTo apply dynamic controls to video decode or video operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdControlVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoCodingControlInfoKHR*          pCodingControlInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be filled by this function\n  for setting encode rate control parameters.\n\n* [`Self::p_coding_control_info`] is a pointer to a[`crate::vk::VideoCodingControlInfoKHR`] structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdControlVideoCodingKHR-pCodingControlInfo-parameter  \n  [`Self::p_coding_control_info`] **must** be a valid pointer to a valid [`crate::vk::VideoCodingControlInfoKHR`] structure\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdControlVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdControlVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdControlVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoCodingControlInfoKHR`]\n"]
    #[doc(alias = "vkCmdControlVideoCodingKHR")]
    pub unsafe fn cmd_control_video_coding_khr(&self, command_buffer: crate::vk1_0::CommandBuffer, coding_control_info: &crate::extensions::khr_video_queue::VideoCodingControlInfoKHR) -> () {
        let _function = self.cmd_control_video_coding_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, coding_control_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndVideoCodingKHR.html)) · Function <br/> vkCmdEndVideoCodingKHR - End decode jobs\n[](#_c_specification)C Specification\n----------\n\nTo end video decode or encode operations, call:\n\n```\n// Provided by VK_KHR_video_queue\nvoid vkCmdEndVideoCodingKHR(\n    VkCommandBuffer                             commandBuffer,\n    const VkVideoEndCodingInfoKHR*              pEndCodingInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer to be filled by this function.\n\n* [`Self::p_end_coding_info`] is a pointer to a [`crate::vk::VideoEndCodingInfoKHR`]structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndVideoCodingKHR-pEndCodingInfo-parameter  \n  [`Self::p_end_coding_info`] **must** be a valid pointer to a valid [`crate::vk::VideoEndCodingInfoKHR`] structure\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndVideoCodingKHR-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support decode, or encode operations\n\n* []() VUID-vkCmdEndVideoCodingKHR-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdEndVideoCodingKHR-bufferlevel  \n  [`Self::command_buffer`] **must** be a primary [`crate::vk::CommandBuffer`]\n\nHost Synchronization\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                   Primary                    |                 Outside                  |           Decode  <br/>Encode           |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::VideoEndCodingInfoKHR`]\n"]
    #[doc(alias = "vkCmdEndVideoCodingKHR")]
    pub unsafe fn cmd_end_video_coding_khr(&self, command_buffer: crate::vk1_0::CommandBuffer, end_coding_info: &crate::extensions::khr_video_queue::VideoEndCodingInfoKHR) -> () {
        let _function = self.cmd_end_video_coding_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, end_coding_info as _);
        ()
    }
}
