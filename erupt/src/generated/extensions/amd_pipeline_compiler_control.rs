#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_PIPELINE_COMPILER_CONTROL_SPEC_VERSION")]
pub const AMD_PIPELINE_COMPILER_CONTROL_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_PIPELINE_COMPILER_CONTROL_EXTENSION_NAME")]
pub const AMD_PIPELINE_COMPILER_CONTROL_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_AMD_pipeline_compiler_control");
#[doc = "Provided by [`crate::extensions::amd_pipeline_compiler_control`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD: Self = Self(1000183000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlFlagsAMD.html)) · Bitmask of [`PipelineCompilerControlFlagBitsAMD`] <br/> VkPipelineCompilerControlFlagsAMD - Bitmask of VkPipelineCompilerControlFlagBitsAMD\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_AMD_pipeline_compiler_control\ntypedef VkFlags VkPipelineCompilerControlFlagsAMD;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineCompilerControlFlagBitsAMD`] is a bitmask type for setting a mask\nof zero or more [VkPipelineCompilerControlFlagBitsAMD](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlFlagBitsAMD.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCompilerControlCreateInfoAMD`], [VkPipelineCompilerControlFlagBitsAMD](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlFlagBitsAMD.html)\n"] # [doc (alias = "VkPipelineCompilerControlFlagsAMD")] # [derive (Default)] # [repr (transparent)] pub struct PipelineCompilerControlFlagsAMD : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineCompilerControlFlagsAMD`] <br/> "]
#[doc(alias = "VkPipelineCompilerControlFlagBitsAMD")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineCompilerControlFlagBitsAMD(pub u32);
impl PipelineCompilerControlFlagBitsAMD {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineCompilerControlFlagsAMD {
        PipelineCompilerControlFlagsAMD::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineCompilerControlFlagBitsAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
impl<'a> crate::ExtendableFromConst<'a, PipelineCompilerControlCreateInfoAMD> for crate::vk1_0::ComputePipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCompilerControlCreateInfoAMDBuilder<'_>> for crate::vk1_0::ComputePipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCompilerControlCreateInfoAMD> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCompilerControlCreateInfoAMDBuilder<'_>> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlCreateInfoAMD.html)) · Structure <br/> VkPipelineCompilerControlCreateInfoAMD - Structure used to pass compilation control flags to a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe compilation of a pipeline **can** be tuned by adding a[`crate::vk::PipelineCompilerControlCreateInfoAMD`] structure to the [`Self::p_next`]chain of [`crate::vk::GraphicsPipelineCreateInfo`] or[`crate::vk::ComputePipelineCreateInfo`].\n\n```\n// Provided by VK_AMD_pipeline_compiler_control\ntypedef struct VkPipelineCompilerControlCreateInfoAMD {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkPipelineCompilerControlFlagsAMD    compilerControlFlags;\n} VkPipelineCompilerControlCreateInfoAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::compiler_control_flags`] is a bitmask of[VkPipelineCompilerControlFlagBitsAMD](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlFlagBitsAMD.html) affecting how the pipeline\n  will be compiled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCompilerControlCreateInfoAMD-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD`]\n\n* []() VUID-VkPipelineCompilerControlCreateInfoAMD-compilerControlFlags-zerobitmask  \n  [`Self::compiler_control_flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCompilerControlFlagBitsAMD`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineCompilerControlCreateInfoAMD")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineCompilerControlCreateInfoAMD {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub compiler_control_flags: crate::extensions::amd_pipeline_compiler_control::PipelineCompilerControlFlagsAMD,
}
impl PipelineCompilerControlCreateInfoAMD {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD;
}
impl Default for PipelineCompilerControlCreateInfoAMD {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), compiler_control_flags: Default::default() }
    }
}
impl std::fmt::Debug for PipelineCompilerControlCreateInfoAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineCompilerControlCreateInfoAMD").field("s_type", &self.s_type).field("p_next", &self.p_next).field("compiler_control_flags", &self.compiler_control_flags).finish()
    }
}
impl PipelineCompilerControlCreateInfoAMD {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineCompilerControlCreateInfoAMDBuilder<'a> {
        PipelineCompilerControlCreateInfoAMDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlCreateInfoAMD.html)) · Builder of [`PipelineCompilerControlCreateInfoAMD`] <br/> VkPipelineCompilerControlCreateInfoAMD - Structure used to pass compilation control flags to a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe compilation of a pipeline **can** be tuned by adding a[`crate::vk::PipelineCompilerControlCreateInfoAMD`] structure to the [`Self::p_next`]chain of [`crate::vk::GraphicsPipelineCreateInfo`] or[`crate::vk::ComputePipelineCreateInfo`].\n\n```\n// Provided by VK_AMD_pipeline_compiler_control\ntypedef struct VkPipelineCompilerControlCreateInfoAMD {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkPipelineCompilerControlFlagsAMD    compilerControlFlags;\n} VkPipelineCompilerControlCreateInfoAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::compiler_control_flags`] is a bitmask of[VkPipelineCompilerControlFlagBitsAMD](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCompilerControlFlagBitsAMD.html) affecting how the pipeline\n  will be compiled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCompilerControlCreateInfoAMD-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD`]\n\n* []() VUID-VkPipelineCompilerControlCreateInfoAMD-compilerControlFlags-zerobitmask  \n  [`Self::compiler_control_flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCompilerControlFlagBitsAMD`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineCompilerControlCreateInfoAMDBuilder<'a>(PipelineCompilerControlCreateInfoAMD, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineCompilerControlCreateInfoAMDBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineCompilerControlCreateInfoAMDBuilder<'a> {
        PipelineCompilerControlCreateInfoAMDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn compiler_control_flags(mut self, compiler_control_flags: crate::extensions::amd_pipeline_compiler_control::PipelineCompilerControlFlagsAMD) -> Self {
        self.0.compiler_control_flags = compiler_control_flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineCompilerControlCreateInfoAMD {
        self.0
    }
}
impl<'a> std::default::Default for PipelineCompilerControlCreateInfoAMDBuilder<'a> {
    fn default() -> PipelineCompilerControlCreateInfoAMDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineCompilerControlCreateInfoAMDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineCompilerControlCreateInfoAMDBuilder<'a> {
    type Target = PipelineCompilerControlCreateInfoAMD;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineCompilerControlCreateInfoAMDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
