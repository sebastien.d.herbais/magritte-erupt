#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEVICE_MEMORY_REPORT_SPEC_VERSION")]
pub const EXT_DEVICE_MEMORY_REPORT_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEVICE_MEMORY_REPORT_EXTENSION_NAME")]
pub const EXT_DEVICE_MEMORY_REPORT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_device_memory_report");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryReportFlagsEXT.html)) · Bitmask of [`DeviceMemoryReportFlagBitsEXT`] <br/> "] # [doc (alias = "VkDeviceMemoryReportFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DeviceMemoryReportFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DeviceMemoryReportFlagsEXT`] <br/> "]
#[doc(alias = "VkDeviceMemoryReportFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DeviceMemoryReportFlagBitsEXT(pub u32);
impl DeviceMemoryReportFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DeviceMemoryReportFlagsEXT {
        DeviceMemoryReportFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DeviceMemoryReportFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_device_memory_report`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT: Self = Self(1000284000);
    pub const DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT: Self = Self(1000284001);
    pub const DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT: Self = Self(1000284002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryReportEventTypeEXT.html)) · Enum <br/> VkDeviceMemoryReportEventTypeEXT - Events that can occur on a device memory object\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DeviceMemoryReportCallbackDataEXT::_type`],\nspecifying event types which cause the device driver to call the callback,\nare:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef enum VkDeviceMemoryReportEventTypeEXT {\n    VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATE_EXT = 0,\n    VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_FREE_EXT = 1,\n    VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_IMPORT_EXT = 2,\n    VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_UNIMPORT_EXT = 3,\n    VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATION_FAILED_EXT = 4,\n} VkDeviceMemoryReportEventTypeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::ALLOCATE_EXT`] specifies this\n  event corresponds to the allocation of an internal device memory object\n  or a [`crate::vk::DeviceMemory`].\n\n* [`Self::FREE_EXT`] specifies this event\n  corresponds to the deallocation of an internally-allocated device memory\n  object or a [`crate::vk::DeviceMemory`].\n\n* [`Self::IMPORT_EXT`] specifies this event\n  corresponds to the import of an external memory object.\n\n* [`Self::UNIMPORT_EXT`] specifies this\n  event is the release of an imported external memory object.\n\n* [`Self::ALLOCATION_FAILED_EXT`] specifies\n  this event corresponds to the failed allocation of an internal device\n  memory object or a [`crate::vk::DeviceMemory`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemoryReportCallbackDataEXT`]\n"]
#[doc(alias = "VkDeviceMemoryReportEventTypeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DeviceMemoryReportEventTypeEXT(pub i32);
impl std::fmt::Debug for DeviceMemoryReportEventTypeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::ALLOCATE_EXT => "ALLOCATE_EXT",
            &Self::FREE_EXT => "FREE_EXT",
            &Self::IMPORT_EXT => "IMPORT_EXT",
            &Self::UNIMPORT_EXT => "UNIMPORT_EXT",
            &Self::ALLOCATION_FAILED_EXT => "ALLOCATION_FAILED_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_device_memory_report`]"]
impl crate::extensions::ext_device_memory_report::DeviceMemoryReportEventTypeEXT {
    pub const ALLOCATE_EXT: Self = Self(0);
    pub const FREE_EXT: Self = Self(1);
    pub const IMPORT_EXT: Self = Self(2);
    pub const UNIMPORT_EXT: Self = Self(3);
    pub const ALLOCATION_FAILED_EXT: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/PFN_vkDeviceMemoryReportCallbackEXT.html)) · Function <br/> PFN\\_vkDeviceMemoryReportCallbackEXT - Application-defined device memory report callback function\n[](#_c_specification)C Specification\n----------\n\nThe prototype for the[`crate::vk::DeviceDeviceMemoryReportCreateInfoEXT::pfn_user_callback`]function implemented by the application is:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef void (VKAPI_PTR *PFN_vkDeviceMemoryReportCallbackEXT)(\n    const VkDeviceMemoryReportCallbackDataEXT*  pCallbackData,\n    void*                                       pUserData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::p_callback_data`] contains all the callback related data in the[`crate::vk::DeviceMemoryReportCallbackDataEXT`] structure.\n\n* [`Self::p_user_data`] is the user data provided when the[`crate::vk::DeviceDeviceMemoryReportCreateInfoEXT`] was created.\n[](#_description)Description\n----------\n\nThe callback **must** not make calls to any Vulkan commands.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceDeviceMemoryReportCreateInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDeviceMemoryReportCallbackEXT = unsafe extern "system" fn(p_callback_data: *const crate::extensions::ext_device_memory_report::DeviceMemoryReportCallbackDataEXT, p_user_data: *mut std::ffi::c_void) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceDeviceMemoryReportFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DeviceDeviceMemoryReportCreateInfoEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DeviceDeviceMemoryReportCreateInfoEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceMemoryReportFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceMemoryReportFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceDeviceMemoryReportFeaturesEXT - Structure describing whether device memory report callback can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkPhysicalDeviceDeviceMemoryReportFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           deviceMemoryReport;\n} VkPhysicalDeviceDeviceMemoryReportFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::device_memory_report`] indicates\n  whether the implementation supports the ability to register device\n  memory report callbacks.\n\nIf the [`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceMemoryReportFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceDeviceMemoryReportFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub device_memory_report: crate::vk1_0::Bool32,
}
impl PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT;
}
impl Default for PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), device_memory_report: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceDeviceMemoryReportFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("device_memory_report", &(self.device_memory_report != 0)).finish()
    }
}
impl PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
        PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceMemoryReportFeaturesEXT.html)) · Builder of [`PhysicalDeviceDeviceMemoryReportFeaturesEXT`] <br/> VkPhysicalDeviceDeviceMemoryReportFeaturesEXT - Structure describing whether device memory report callback can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkPhysicalDeviceDeviceMemoryReportFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           deviceMemoryReport;\n} VkPhysicalDeviceDeviceMemoryReportFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::device_memory_report`] indicates\n  whether the implementation supports the ability to register device\n  memory report callbacks.\n\nIf the [`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceDeviceMemoryReportFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceMemoryReportFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a>(PhysicalDeviceDeviceMemoryReportFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
        PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn device_memory_report(mut self, device_memory_report: bool) -> Self {
        self.0.device_memory_report = device_memory_report as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceDeviceMemoryReportFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceDeviceMemoryReportFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceDeviceMemoryReportFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceDeviceMemoryReportCreateInfoEXT.html)) · Structure <br/> VkDeviceDeviceMemoryReportCreateInfoEXT - Register device memory report callbacks for a Vulkan device\n[](#_c_specification)C Specification\n----------\n\nTo register callbacks for underlying device memory events of type[`crate::vk::DeviceMemoryReportEventTypeEXT`], add one or multiple[`crate::vk::DeviceDeviceMemoryReportCreateInfoEXT`] structures to the [`Self::p_next`]chain of the [`crate::vk::DeviceCreateInfo`] structure.\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkDeviceDeviceMemoryReportCreateInfoEXT {\n    VkStructureType                        sType;\n    const void*                            pNext;\n    VkDeviceMemoryReportFlagsEXT           flags;\n    PFN_vkDeviceMemoryReportCallbackEXT    pfnUserCallback;\n    void*                                  pUserData;\n} VkDeviceDeviceMemoryReportCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n\n* [`Self::pfn_user_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nThe callback **may** be called from multiple threads simultaneously.\n\nThe callback **must** be called only once by the implementation when a[`crate::vk::DeviceMemoryReportEventTypeEXT`] event occurs.\n\n|   |Note<br/><br/>The callback could be called from a background thread other than the thread<br/>calling the Vulkan commands.|\n|---|--------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT`]\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-pfnUserCallback-parameter  \n  [`Self::pfn_user_callback`] **must** be a valid [PFN\\_vkDeviceMemoryReportCallbackEXT](PFN_vkDeviceMemoryReportCallbackEXT.html) value\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-pUserData-parameter  \n  [`Self::p_user_data`] **must** be a pointer value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDeviceMemoryReportCallbackEXT](PFN_vkDeviceMemoryReportCallbackEXT.html), [`crate::vk::DeviceMemoryReportFlagBitsEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDeviceDeviceMemoryReportCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DeviceDeviceMemoryReportCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_device_memory_report::DeviceMemoryReportFlagsEXT,
    pub pfn_user_callback: Option<crate::extensions::ext_device_memory_report::PFN_vkDeviceMemoryReportCallbackEXT>,
    pub p_user_data: *mut std::ffi::c_void,
}
impl DeviceDeviceMemoryReportCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT;
}
impl Default for DeviceDeviceMemoryReportCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), pfn_user_callback: Default::default(), p_user_data: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for DeviceDeviceMemoryReportCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DeviceDeviceMemoryReportCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("pfn_user_callback", unsafe { &std::mem::transmute::<_, *const ()>(self.pfn_user_callback) }).field("p_user_data", &self.p_user_data).finish()
    }
}
impl DeviceDeviceMemoryReportCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
        DeviceDeviceMemoryReportCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceDeviceMemoryReportCreateInfoEXT.html)) · Builder of [`DeviceDeviceMemoryReportCreateInfoEXT`] <br/> VkDeviceDeviceMemoryReportCreateInfoEXT - Register device memory report callbacks for a Vulkan device\n[](#_c_specification)C Specification\n----------\n\nTo register callbacks for underlying device memory events of type[`crate::vk::DeviceMemoryReportEventTypeEXT`], add one or multiple[`crate::vk::DeviceDeviceMemoryReportCreateInfoEXT`] structures to the [`Self::p_next`]chain of the [`crate::vk::DeviceCreateInfo`] structure.\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkDeviceDeviceMemoryReportCreateInfoEXT {\n    VkStructureType                        sType;\n    const void*                            pNext;\n    VkDeviceMemoryReportFlagsEXT           flags;\n    PFN_vkDeviceMemoryReportCallbackEXT    pfnUserCallback;\n    void*                                  pUserData;\n} VkDeviceDeviceMemoryReportCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n\n* [`Self::pfn_user_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nThe callback **may** be called from multiple threads simultaneously.\n\nThe callback **must** be called only once by the implementation when a[`crate::vk::DeviceMemoryReportEventTypeEXT`] event occurs.\n\n|   |Note<br/><br/>The callback could be called from a background thread other than the thread<br/>calling the Vulkan commands.|\n|---|--------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT`]\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-pfnUserCallback-parameter  \n  [`Self::pfn_user_callback`] **must** be a valid [PFN\\_vkDeviceMemoryReportCallbackEXT](PFN_vkDeviceMemoryReportCallbackEXT.html) value\n\n* []() VUID-VkDeviceDeviceMemoryReportCreateInfoEXT-pUserData-parameter  \n  [`Self::p_user_data`] **must** be a pointer value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDeviceMemoryReportCallbackEXT](PFN_vkDeviceMemoryReportCallbackEXT.html), [`crate::vk::DeviceMemoryReportFlagBitsEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a>(DeviceDeviceMemoryReportCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
        DeviceDeviceMemoryReportCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_device_memory_report::DeviceMemoryReportFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn pfn_user_callback(mut self, pfn_user_callback: Option<crate::extensions::ext_device_memory_report::PFN_vkDeviceMemoryReportCallbackEXT>) -> Self {
        self.0.pfn_user_callback = pfn_user_callback as _;
        self
    }
    #[inline]
    pub fn user_data(mut self, user_data: *mut std::ffi::c_void) -> Self {
        self.0.p_user_data = user_data;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DeviceDeviceMemoryReportCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
    fn default() -> DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
    type Target = DeviceDeviceMemoryReportCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DeviceDeviceMemoryReportCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryReportCallbackDataEXT.html)) · Structure <br/> VkDeviceMemoryReportCallbackDataEXT - Structure specifying parameters returned to the callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DeviceMemoryReportCallbackDataEXT`] is:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkDeviceMemoryReportCallbackDataEXT {\n    VkStructureType                     sType;\n    void*                               pNext;\n    VkDeviceMemoryReportFlagsEXT        flags;\n    VkDeviceMemoryReportEventTypeEXT    type;\n    uint64_t                            memoryObjectId;\n    VkDeviceSize                        size;\n    VkObjectType                        objectType;\n    uint64_t                            objectHandle;\n    uint32_t                            heapIndex;\n} VkDeviceMemoryReportCallbackDataEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n\n* [`Self::_type`] is a [`crate::vk::DeviceMemoryReportEventTypeEXT`] type specifying\n  the type of event reported in this[`crate::vk::DeviceMemoryReportCallbackDataEXT`] structure.\n\n* [`Self::memory_object_id`] is the unique id for the underlying memory object\n  as described below.\n\n* [`Self::size`] is the size of the memory object in bytes.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::size`] is a valid [`crate::vk::DeviceSize`] value.\n  Otherwise, [`Self::size`] is undefined.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] value specifying the type of\n  the object associated with this device memory report event.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::FREE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::UNIMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::object_type`] is a valid [`crate::vk::ObjectType`] enum.\n  Otherwise, [`Self::object_type`] is undefined.\n\n* [`Self::object_handle`] is the object this device memory report event is\n  attributed to.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::FREE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::UNIMPORT_EXT`],[`Self::object_handle`] is a valid Vulkan handle of the type associated with[`Self::object_type`] as defined in the [VkObjectType and Vulkan Handle Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table.\n  Otherwise, [`Self::object_handle`] is undefined.\n\n* [`Self::heap_index`] describes which memory heap this device memory\n  allocation is made from.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`]or [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::heap_index`] corresponds to one of the valid heaps from the[`crate::vk::PhysicalDeviceMemoryProperties`] structure.\n  Otherwise, [`Self::heap_index`] is undefined.\n[](#_description)Description\n----------\n\n[`Self::memory_object_id`] is used to avoid double-counting on the same memory\nobject.\n\nIf an internally-allocated device memory object or a [`crate::vk::DeviceMemory`]**cannot** be exported, [`Self::memory_object_id`] **must** be unique in the[`crate::vk::Device`].\n\nIf an internally-allocated device memory object or a [`crate::vk::DeviceMemory`]supports being exported, [`Self::memory_object_id`] **must** be unique system wide.\n\nIf an internal device memory object or a [`crate::vk::DeviceMemory`] is backed by\nan imported external memory object, [`Self::memory_object_id`] **must** be unique\nsystem wide.\n\nImplementor’s Note\n\nIf the heap backing an internally-allocated device memory **cannot** be used to\nback [`crate::vk::DeviceMemory`], implementations **can** advertise that heap with no\ntypes.\n\n|   |Note<br/><br/>This structure should only be considered valid during the lifetime of the<br/>triggered callback.<br/><br/>For [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`] and[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] events,[`Self::object_handle`] usually will not yet exist when the application or tool<br/>receives the callback.[`Self::object_handle`] will only exist when the create or allocate call that<br/>triggered the event returns, and if the allocation or import ends up failing[`Self::object_handle`] will not ever exist.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceMemoryReportCallbackDataEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT`]\n\n* []() VUID-VkDeviceMemoryReportCallbackDataEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemoryReportEventTypeEXT`], [`crate::vk::DeviceMemoryReportFlagBitsEXT`], [`crate::vk::DeviceSize`], [`crate::vk::ObjectType`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDeviceMemoryReportCallbackDataEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DeviceMemoryReportCallbackDataEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub flags: crate::extensions::ext_device_memory_report::DeviceMemoryReportFlagsEXT,
    pub _type: crate::extensions::ext_device_memory_report::DeviceMemoryReportEventTypeEXT,
    pub memory_object_id: u64,
    pub size: crate::vk1_0::DeviceSize,
    pub object_type: crate::vk1_0::ObjectType,
    pub object_handle: u64,
    pub heap_index: u32,
}
impl DeviceMemoryReportCallbackDataEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT;
}
impl Default for DeviceMemoryReportCallbackDataEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), flags: Default::default(), _type: Default::default(), memory_object_id: Default::default(), size: Default::default(), object_type: Default::default(), object_handle: Default::default(), heap_index: Default::default() }
    }
}
impl std::fmt::Debug for DeviceMemoryReportCallbackDataEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DeviceMemoryReportCallbackDataEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("_type", &self._type).field("memory_object_id", &self.memory_object_id).field("size", &self.size).field("object_type", &self.object_type).field("object_handle", &self.object_handle).field("heap_index", &self.heap_index).finish()
    }
}
impl DeviceMemoryReportCallbackDataEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DeviceMemoryReportCallbackDataEXTBuilder<'a> {
        DeviceMemoryReportCallbackDataEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceMemoryReportCallbackDataEXT.html)) · Builder of [`DeviceMemoryReportCallbackDataEXT`] <br/> VkDeviceMemoryReportCallbackDataEXT - Structure specifying parameters returned to the callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DeviceMemoryReportCallbackDataEXT`] is:\n\n```\n// Provided by VK_EXT_device_memory_report\ntypedef struct VkDeviceMemoryReportCallbackDataEXT {\n    VkStructureType                     sType;\n    void*                               pNext;\n    VkDeviceMemoryReportFlagsEXT        flags;\n    VkDeviceMemoryReportEventTypeEXT    type;\n    uint64_t                            memoryObjectId;\n    VkDeviceSize                        size;\n    VkObjectType                        objectType;\n    uint64_t                            objectHandle;\n    uint32_t                            heapIndex;\n} VkDeviceMemoryReportCallbackDataEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is 0 and reserved for future use.\n\n* [`Self::_type`] is a [`crate::vk::DeviceMemoryReportEventTypeEXT`] type specifying\n  the type of event reported in this[`crate::vk::DeviceMemoryReportCallbackDataEXT`] structure.\n\n* [`Self::memory_object_id`] is the unique id for the underlying memory object\n  as described below.\n\n* [`Self::size`] is the size of the memory object in bytes.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::size`] is a valid [`crate::vk::DeviceSize`] value.\n  Otherwise, [`Self::size`] is undefined.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] value specifying the type of\n  the object associated with this device memory report event.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::FREE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::UNIMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::object_type`] is a valid [`crate::vk::ObjectType`] enum.\n  Otherwise, [`Self::object_type`] is undefined.\n\n* [`Self::object_handle`] is the object this device memory report event is\n  attributed to.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::FREE_EXT`],[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] or[`crate::vk::DeviceMemoryReportEventTypeEXT::UNIMPORT_EXT`],[`Self::object_handle`] is a valid Vulkan handle of the type associated with[`Self::object_type`] as defined in the [VkObjectType and Vulkan Handle Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table.\n  Otherwise, [`Self::object_handle`] is undefined.\n\n* [`Self::heap_index`] describes which memory heap this device memory\n  allocation is made from.\n  If [`Self::_type`] is [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`]or [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATION_FAILED_EXT`],[`Self::heap_index`] corresponds to one of the valid heaps from the[`crate::vk::PhysicalDeviceMemoryProperties`] structure.\n  Otherwise, [`Self::heap_index`] is undefined.\n[](#_description)Description\n----------\n\n[`Self::memory_object_id`] is used to avoid double-counting on the same memory\nobject.\n\nIf an internally-allocated device memory object or a [`crate::vk::DeviceMemory`]**cannot** be exported, [`Self::memory_object_id`] **must** be unique in the[`crate::vk::Device`].\n\nIf an internally-allocated device memory object or a [`crate::vk::DeviceMemory`]supports being exported, [`Self::memory_object_id`] **must** be unique system wide.\n\nIf an internal device memory object or a [`crate::vk::DeviceMemory`] is backed by\nan imported external memory object, [`Self::memory_object_id`] **must** be unique\nsystem wide.\n\nImplementor’s Note\n\nIf the heap backing an internally-allocated device memory **cannot** be used to\nback [`crate::vk::DeviceMemory`], implementations **can** advertise that heap with no\ntypes.\n\n|   |Note<br/><br/>This structure should only be considered valid during the lifetime of the<br/>triggered callback.<br/><br/>For [`crate::vk::DeviceMemoryReportEventTypeEXT::ALLOCATE_EXT`] and[`crate::vk::DeviceMemoryReportEventTypeEXT::IMPORT_EXT`] events,[`Self::object_handle`] usually will not yet exist when the application or tool<br/>receives the callback.[`Self::object_handle`] will only exist when the create or allocate call that<br/>triggered the event returns, and if the allocation or import ends up failing[`Self::object_handle`] will not ever exist.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceMemoryReportCallbackDataEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT`]\n\n* []() VUID-VkDeviceMemoryReportCallbackDataEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemoryReportEventTypeEXT`], [`crate::vk::DeviceMemoryReportFlagBitsEXT`], [`crate::vk::DeviceSize`], [`crate::vk::ObjectType`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DeviceMemoryReportCallbackDataEXTBuilder<'a>(DeviceMemoryReportCallbackDataEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DeviceMemoryReportCallbackDataEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DeviceMemoryReportCallbackDataEXTBuilder<'a> {
        DeviceMemoryReportCallbackDataEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_device_memory_report::DeviceMemoryReportFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn _type(mut self, _type: crate::extensions::ext_device_memory_report::DeviceMemoryReportEventTypeEXT) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn memory_object_id(mut self, memory_object_id: u64) -> Self {
        self.0.memory_object_id = memory_object_id as _;
        self
    }
    #[inline]
    pub fn size(mut self, size: crate::vk1_0::DeviceSize) -> Self {
        self.0.size = size as _;
        self
    }
    #[inline]
    pub fn object_type(mut self, object_type: crate::vk1_0::ObjectType) -> Self {
        self.0.object_type = object_type as _;
        self
    }
    #[inline]
    pub fn object_handle(mut self, object_handle: u64) -> Self {
        self.0.object_handle = object_handle as _;
        self
    }
    #[inline]
    pub fn heap_index(mut self, heap_index: u32) -> Self {
        self.0.heap_index = heap_index as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DeviceMemoryReportCallbackDataEXT {
        self.0
    }
}
impl<'a> std::default::Default for DeviceMemoryReportCallbackDataEXTBuilder<'a> {
    fn default() -> DeviceMemoryReportCallbackDataEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DeviceMemoryReportCallbackDataEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DeviceMemoryReportCallbackDataEXTBuilder<'a> {
    type Target = DeviceMemoryReportCallbackDataEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DeviceMemoryReportCallbackDataEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
