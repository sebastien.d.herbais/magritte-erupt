#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PRIVATE_DATA_SPEC_VERSION")]
pub const EXT_PRIVATE_DATA_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PRIVATE_DATA_EXTENSION_NAME")]
pub const EXT_PRIVATE_DATA_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_private_data");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_PRIVATE_DATA_SLOT_EXT: *const std::os::raw::c_char = crate::cstr!("vkCreatePrivateDataSlotEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_PRIVATE_DATA_SLOT_EXT: *const std::os::raw::c_char = crate::cstr!("vkDestroyPrivateDataSlotEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_SET_PRIVATE_DATA_EXT: *const std::os::raw::c_char = crate::cstr!("vkSetPrivateDataEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PRIVATE_DATA_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPrivateDataEXT");
crate::non_dispatchable_handle!(PrivateDataSlotEXT, PRIVATE_DATA_SLOT_EXT, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotEXT.html)) · Non-dispatchable Handle <br/> VkPrivateDataSlotEXT - Opaque handle to a private data slot object\n[](#_c_specification)C Specification\n----------\n\nPrivate data slots are represented by [`crate::vk::PrivateDataSlotEXT`] handles:\n\n```\n// Provided by VK_EXT_private_data\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkPrivateDataSlotEXT)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::create_private_data_slot_ext`], [`crate::vk::DeviceLoader::destroy_private_data_slot_ext`], [`crate::vk::DeviceLoader::get_private_data_ext`], [`crate::vk::DeviceLoader::set_private_data_ext`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPrivateDataSlotEXT)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkPrivateDataSlotEXT");
#[doc = "Provided by [`crate::extensions::ext_private_data`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT: Self = Self(1000295000);
    pub const DEVICE_PRIVATE_DATA_CREATE_INFO_EXT: Self = Self(1000295001);
    pub const PRIVATE_DATA_SLOT_CREATE_INFO_EXT: Self = Self(1000295002);
}
#[doc = "Provided by [`crate::extensions::ext_private_data`]"]
impl crate::vk1_0::ObjectType {
    pub const PRIVATE_DATA_SLOT_EXT: Self = Self(1000295000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotCreateFlagsEXT.html)) · Bitmask of [`PrivateDataSlotCreateFlagBitsEXT`] <br/> VkPrivateDataSlotCreateFlagsEXT - Bitmask of VkPrivateDataSlotCreateFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_private_data\ntypedef VkFlags VkPrivateDataSlotCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PrivateDataSlotCreateFlagBitsEXT`] is a bitmask type for setting a mask\nof zero or more [VkPrivateDataSlotCreateFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotCreateFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[VkPrivateDataSlotCreateFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotCreateFlagBitsEXT.html), [`crate::vk::PrivateDataSlotCreateInfoEXT`]\n"] # [doc (alias = "VkPrivateDataSlotCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct PrivateDataSlotCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PrivateDataSlotCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkPrivateDataSlotCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PrivateDataSlotCreateFlagBitsEXT(pub u32);
impl PrivateDataSlotCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PrivateDataSlotCreateFlagsEXT {
        PrivateDataSlotCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PrivateDataSlotCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreatePrivateDataSlotEXT.html)) · Function <br/> vkCreatePrivateDataSlotEXT - Create a slot for private data storage\n[](#_c_specification)C Specification\n----------\n\nTo create a private data slot, call:\n\n```\n// Provided by VK_EXT_private_data\nVkResult vkCreatePrivateDataSlotEXT(\n    VkDevice                                    device,\n    const VkPrivateDataSlotCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkPrivateDataSlotEXT*                       pPrivateDataSlot);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device associated with the creation of the\n  object(s) holding the private data slot.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::PrivateDataSlotCreateInfoEXT`]\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_private_data_slot`] is a pointer to a [`crate::vk::PrivateDataSlotEXT`]handle in which the resulting private data slot is returned\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCreatePrivateDataSlotEXT-privateData-04564  \n   The [`privateData`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-privateData) feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreatePrivateDataSlotEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::PrivateDataSlotCreateInfoEXT`] structure\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pPrivateDataSlot-parameter  \n  [`Self::p_private_data_slot`] **must** be a valid pointer to a [`crate::vk::PrivateDataSlotEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::PrivateDataSlotCreateInfoEXT`], [`crate::vk::PrivateDataSlotEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreatePrivateDataSlotEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_create_info: *const crate::extensions::ext_private_data::PrivateDataSlotCreateInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_private_data_slot: *mut crate::extensions::ext_private_data::PrivateDataSlotEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyPrivateDataSlotEXT.html)) · Function <br/> vkDestroyPrivateDataSlotEXT - Destroy a private data slot\n[](#_c_specification)C Specification\n----------\n\nTo destroy a private data slot, call:\n\n```\n// Provided by VK_EXT_private_data\nvoid vkDestroyPrivateDataSlotEXT(\n    VkDevice                                    device,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device associated with the creation of the\n  object(s) holding the private data slot.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::private_data_slot`] is the private data slot to destroy.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-04062  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::private_data_slot`]was created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-04063  \n   If no [`crate::vk::AllocationCallbacks`] were provided when[`Self::private_data_slot`] was created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-parameter  \n   If [`Self::private_data_slot`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-parent  \n   If [`Self::private_data_slot`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::private_data_slot`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::PrivateDataSlotEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyPrivateDataSlotEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, private_data_slot: crate::extensions::ext_private_data::PrivateDataSlotEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetPrivateDataEXT.html)) · Function <br/> vkSetPrivateDataEXT - Associate data with a Vulkan object\n[](#_c_specification)C Specification\n----------\n\nTo store user defined data in a slot associated with a Vulkan object, call:\n\n```\n// Provided by VK_EXT_private_data\nVkResult vkSetPrivateDataEXT(\n    VkDevice                                    device,\n    VkObjectType                                objectType,\n    uint64_t                                    objectHandle,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    uint64_t                                    data);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of object\n  to associate data with.\n\n* [`Self::object_handle`] is a handle to the object to associate data with.\n\n* [`Self::private_data_slot`] is a handle to a [`crate::vk::PrivateDataSlotEXT`]specifying location of private data storage.\n\n* [`Self::data`] is user defined data to associate the object with.\n  This data will be stored at [`Self::private_data_slot`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkSetPrivateDataEXT-objectHandle-04016  \n  [`Self::object_handle`] **must** be [`Self::device`] or a child of [`Self::device`]\n\n* []() VUID-vkSetPrivateDataEXT-objectHandle-04017  \n  [`Self::object_handle`] **must** be a valid handle to an object of type[`Self::object_type`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetPrivateDataEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetPrivateDataEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-vkSetPrivateDataEXT-privateDataSlot-parameter  \n  [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkSetPrivateDataEXT-privateDataSlot-parent  \n  [`Self::private_data_slot`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ObjectType`], [`crate::vk::PrivateDataSlotEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkSetPrivateDataEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, object_type: crate::vk1_0::ObjectType, object_handle: u64, private_data_slot: crate::extensions::ext_private_data::PrivateDataSlotEXT, data: u64) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPrivateDataEXT.html)) · Function <br/> vkGetPrivateDataEXT - Retrieve data associated with a Vulkan object\n[](#_c_specification)C Specification\n----------\n\nTo retrieve user defined data from a slot associated with a Vulkan object,\ncall:\n\n```\n// Provided by VK_EXT_private_data\nvoid vkGetPrivateDataEXT(\n    VkDevice                                    device,\n    VkObjectType                                objectType,\n    uint64_t                                    objectHandle,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    uint64_t*                                   pData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of object\n  data is associated with.\n\n* [`Self::object_handle`] is a handle to the object data is associated with.\n\n* [`Self::private_data_slot`] is a handle to a [`crate::vk::PrivateDataSlotEXT`]specifying location of private data pointer storage.\n\n* [`Self::p_data`] is a pointer to specify where user data is returned.`0` will be written in the absence of a previous call to[`crate::vk::DeviceLoader::set_private_data_ext`] using the object specified by[`Self::object_handle`].\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Due to platform details on Android, implementations might not be able to<br/>reliably return `0` from calls to [`crate::vk::DeviceLoader::get_private_data_ext`] for[`crate::vk::SwapchainKHR`] objects on which [`crate::vk::DeviceLoader::set_private_data_ext`] has not<br/>previously been called.<br/>This erratum is exclusive to the Android platform and objects of type[`crate::vk::SwapchainKHR`].|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-vkGetPrivateDataEXT-objectType-04018  \n  [`Self::object_type`] **must** be [`crate::vk::ObjectType::DEVICE`], or an object type\n  whose parent is [`crate::vk::Device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPrivateDataEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPrivateDataEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-vkGetPrivateDataEXT-privateDataSlot-parameter  \n  [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkGetPrivateDataEXT-pData-parameter  \n  [`Self::p_data`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetPrivateDataEXT-privateDataSlot-parent  \n  [`Self::private_data_slot`] **must** have been created, allocated, or retrieved from [`Self::device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ObjectType`], [`crate::vk::PrivateDataSlotEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPrivateDataEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, object_type: crate::vk1_0::ObjectType, object_handle: u64, private_data_slot: crate::extensions::ext_private_data::PrivateDataSlotEXT, p_data: *mut u64) -> ();
impl<'a> crate::ExtendableFromConst<'a, DevicePrivateDataCreateInfoEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DevicePrivateDataCreateInfoEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePrivateDataFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePrivateDataFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDevicePrivateDataCreateInfoEXT.html)) · Structure <br/> VkDevicePrivateDataCreateInfoEXT - Reserve private data slots\n[](#_c_specification)C Specification\n----------\n\nTo reserve private data storage slots, add a[`crate::vk::DevicePrivateDataCreateInfoEXT`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::DeviceCreateInfo`] structure.\nReserving slots in this manner is not strictly necessary, but doing so **may**improve performance.\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkDevicePrivateDataCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           privateDataSlotRequestCount;\n} VkDevicePrivateDataCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::private_data_slot_request_count`] is the amount of slots to reserve.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDevicePrivateDataCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_PRIVATE_DATA_CREATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDevicePrivateDataCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DevicePrivateDataCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub private_data_slot_request_count: u32,
}
impl DevicePrivateDataCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEVICE_PRIVATE_DATA_CREATE_INFO_EXT;
}
impl Default for DevicePrivateDataCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), private_data_slot_request_count: Default::default() }
    }
}
impl std::fmt::Debug for DevicePrivateDataCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DevicePrivateDataCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("private_data_slot_request_count", &self.private_data_slot_request_count).finish()
    }
}
impl DevicePrivateDataCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DevicePrivateDataCreateInfoEXTBuilder<'a> {
        DevicePrivateDataCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDevicePrivateDataCreateInfoEXT.html)) · Builder of [`DevicePrivateDataCreateInfoEXT`] <br/> VkDevicePrivateDataCreateInfoEXT - Reserve private data slots\n[](#_c_specification)C Specification\n----------\n\nTo reserve private data storage slots, add a[`crate::vk::DevicePrivateDataCreateInfoEXT`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::DeviceCreateInfo`] structure.\nReserving slots in this manner is not strictly necessary, but doing so **may**improve performance.\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkDevicePrivateDataCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           privateDataSlotRequestCount;\n} VkDevicePrivateDataCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::private_data_slot_request_count`] is the amount of slots to reserve.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDevicePrivateDataCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_PRIVATE_DATA_CREATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DevicePrivateDataCreateInfoEXTBuilder<'a>(DevicePrivateDataCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DevicePrivateDataCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DevicePrivateDataCreateInfoEXTBuilder<'a> {
        DevicePrivateDataCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn private_data_slot_request_count(mut self, private_data_slot_request_count: u32) -> Self {
        self.0.private_data_slot_request_count = private_data_slot_request_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DevicePrivateDataCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DevicePrivateDataCreateInfoEXTBuilder<'a> {
    fn default() -> DevicePrivateDataCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DevicePrivateDataCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DevicePrivateDataCreateInfoEXTBuilder<'a> {
    type Target = DevicePrivateDataCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DevicePrivateDataCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotCreateInfoEXT.html)) · Structure <br/> VkPrivateDataSlotCreateInfoEXT - Structure specifying the parameters of private data slot construction\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PrivateDataSlotCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkPrivateDataSlotCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkPrivateDataSlotCreateFlagsEXT    flags;\n} VkPrivateDataSlotCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::PrivateDataSlotCreateFlagBitsEXT`]specifying additional parameters of the new private data slot\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRIVATE_DATA_SLOT_CREATE_INFO_EXT`]\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PrivateDataSlotCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::create_private_data_slot_ext`]\n"]
#[doc(alias = "VkPrivateDataSlotCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PrivateDataSlotCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_private_data::PrivateDataSlotCreateFlagsEXT,
}
impl PrivateDataSlotCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PRIVATE_DATA_SLOT_CREATE_INFO_EXT;
}
impl Default for PrivateDataSlotCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default() }
    }
}
impl std::fmt::Debug for PrivateDataSlotCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PrivateDataSlotCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).finish()
    }
}
impl PrivateDataSlotCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PrivateDataSlotCreateInfoEXTBuilder<'a> {
        PrivateDataSlotCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPrivateDataSlotCreateInfoEXT.html)) · Builder of [`PrivateDataSlotCreateInfoEXT`] <br/> VkPrivateDataSlotCreateInfoEXT - Structure specifying the parameters of private data slot construction\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PrivateDataSlotCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkPrivateDataSlotCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkPrivateDataSlotCreateFlagsEXT    flags;\n} VkPrivateDataSlotCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::PrivateDataSlotCreateFlagBitsEXT`]specifying additional parameters of the new private data slot\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRIVATE_DATA_SLOT_CREATE_INFO_EXT`]\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPrivateDataSlotCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PrivateDataSlotCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::create_private_data_slot_ext`]\n"]
#[repr(transparent)]
pub struct PrivateDataSlotCreateInfoEXTBuilder<'a>(PrivateDataSlotCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PrivateDataSlotCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PrivateDataSlotCreateInfoEXTBuilder<'a> {
        PrivateDataSlotCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_private_data::PrivateDataSlotCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PrivateDataSlotCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PrivateDataSlotCreateInfoEXTBuilder<'a> {
    fn default() -> PrivateDataSlotCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PrivateDataSlotCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PrivateDataSlotCreateInfoEXTBuilder<'a> {
    type Target = PrivateDataSlotCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PrivateDataSlotCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePrivateDataFeaturesEXT.html)) · Structure <br/> VkPhysicalDevicePrivateDataFeaturesEXT - Structure specifying physical device support\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkPhysicalDevicePrivateDataFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           privateData;\n} VkPhysicalDevicePrivateDataFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::private_data`] indicates whether the\n  implementation supports private data.\n  See [Private Data](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#private-data).\n\nIf the [`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePrivateDataFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePrivateDataFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePrivateDataFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub private_data: crate::vk1_0::Bool32,
}
impl PhysicalDevicePrivateDataFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT;
}
impl Default for PhysicalDevicePrivateDataFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), private_data: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePrivateDataFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePrivateDataFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("private_data", &(self.private_data != 0)).finish()
    }
}
impl PhysicalDevicePrivateDataFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
        PhysicalDevicePrivateDataFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePrivateDataFeaturesEXT.html)) · Builder of [`PhysicalDevicePrivateDataFeaturesEXT`] <br/> VkPhysicalDevicePrivateDataFeaturesEXT - Structure specifying physical device support\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_private_data\ntypedef struct VkPhysicalDevicePrivateDataFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           privateData;\n} VkPhysicalDevicePrivateDataFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::private_data`] indicates whether the\n  implementation supports private data.\n  See [Private Data](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#private-data).\n\nIf the [`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePrivateDataFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePrivateDataFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePrivateDataFeaturesEXTBuilder<'a>(PhysicalDevicePrivateDataFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
        PhysicalDevicePrivateDataFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn private_data(mut self, private_data: bool) -> Self {
        self.0.private_data = private_data as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePrivateDataFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
    type Target = PhysicalDevicePrivateDataFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePrivateDataFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePrivateDataFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePrivateDataFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "Provided by [`crate::extensions::ext_private_data`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreatePrivateDataSlotEXT.html)) · Function <br/> vkCreatePrivateDataSlotEXT - Create a slot for private data storage\n[](#_c_specification)C Specification\n----------\n\nTo create a private data slot, call:\n\n```\n// Provided by VK_EXT_private_data\nVkResult vkCreatePrivateDataSlotEXT(\n    VkDevice                                    device,\n    const VkPrivateDataSlotCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkPrivateDataSlotEXT*                       pPrivateDataSlot);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device associated with the creation of the\n  object(s) holding the private data slot.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::PrivateDataSlotCreateInfoEXT`]\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_private_data_slot`] is a pointer to a [`crate::vk::PrivateDataSlotEXT`]handle in which the resulting private data slot is returned\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCreatePrivateDataSlotEXT-privateData-04564  \n   The [`privateData`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-privateData) feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreatePrivateDataSlotEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::PrivateDataSlotCreateInfoEXT`] structure\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreatePrivateDataSlotEXT-pPrivateDataSlot-parameter  \n  [`Self::p_private_data_slot`] **must** be a valid pointer to a [`crate::vk::PrivateDataSlotEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::PrivateDataSlotCreateInfoEXT`], [`crate::vk::PrivateDataSlotEXT`]\n"]
    #[doc(alias = "vkCreatePrivateDataSlotEXT")]
    pub unsafe fn create_private_data_slot_ext(&self, create_info: &crate::extensions::ext_private_data::PrivateDataSlotCreateInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::ext_private_data::PrivateDataSlotEXT> {
        let _function = self.create_private_data_slot_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut private_data_slot = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut private_data_slot,
        );
        crate::utils::VulkanResult::new(_return, private_data_slot)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyPrivateDataSlotEXT.html)) · Function <br/> vkDestroyPrivateDataSlotEXT - Destroy a private data slot\n[](#_c_specification)C Specification\n----------\n\nTo destroy a private data slot, call:\n\n```\n// Provided by VK_EXT_private_data\nvoid vkDestroyPrivateDataSlotEXT(\n    VkDevice                                    device,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device associated with the creation of the\n  object(s) holding the private data slot.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::private_data_slot`] is the private data slot to destroy.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-04062  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::private_data_slot`]was created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-04063  \n   If no [`crate::vk::AllocationCallbacks`] were provided when[`Self::private_data_slot`] was created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-parameter  \n   If [`Self::private_data_slot`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyPrivateDataSlotEXT-privateDataSlot-parent  \n   If [`Self::private_data_slot`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::private_data_slot`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::PrivateDataSlotEXT`]\n"]
    #[doc(alias = "vkDestroyPrivateDataSlotEXT")]
    pub unsafe fn destroy_private_data_slot_ext(&self, private_data_slot: Option<crate::extensions::ext_private_data::PrivateDataSlotEXT>, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_private_data_slot_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            match private_data_slot {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetPrivateDataEXT.html)) · Function <br/> vkSetPrivateDataEXT - Associate data with a Vulkan object\n[](#_c_specification)C Specification\n----------\n\nTo store user defined data in a slot associated with a Vulkan object, call:\n\n```\n// Provided by VK_EXT_private_data\nVkResult vkSetPrivateDataEXT(\n    VkDevice                                    device,\n    VkObjectType                                objectType,\n    uint64_t                                    objectHandle,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    uint64_t                                    data);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of object\n  to associate data with.\n\n* [`Self::object_handle`] is a handle to the object to associate data with.\n\n* [`Self::private_data_slot`] is a handle to a [`crate::vk::PrivateDataSlotEXT`]specifying location of private data storage.\n\n* [`Self::data`] is user defined data to associate the object with.\n  This data will be stored at [`Self::private_data_slot`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkSetPrivateDataEXT-objectHandle-04016  \n  [`Self::object_handle`] **must** be [`Self::device`] or a child of [`Self::device`]\n\n* []() VUID-vkSetPrivateDataEXT-objectHandle-04017  \n  [`Self::object_handle`] **must** be a valid handle to an object of type[`Self::object_type`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetPrivateDataEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetPrivateDataEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-vkSetPrivateDataEXT-privateDataSlot-parameter  \n  [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkSetPrivateDataEXT-privateDataSlot-parent  \n  [`Self::private_data_slot`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ObjectType`], [`crate::vk::PrivateDataSlotEXT`]\n"]
    #[doc(alias = "vkSetPrivateDataEXT")]
    pub unsafe fn set_private_data_ext(&self, object_type: crate::vk1_0::ObjectType, object_handle: u64, private_data_slot: crate::extensions::ext_private_data::PrivateDataSlotEXT, data: u64) -> crate::utils::VulkanResult<()> {
        let _function = self.set_private_data_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, object_type as _, object_handle as _, private_data_slot as _, data as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPrivateDataEXT.html)) · Function <br/> vkGetPrivateDataEXT - Retrieve data associated with a Vulkan object\n[](#_c_specification)C Specification\n----------\n\nTo retrieve user defined data from a slot associated with a Vulkan object,\ncall:\n\n```\n// Provided by VK_EXT_private_data\nvoid vkGetPrivateDataEXT(\n    VkDevice                                    device,\n    VkObjectType                                objectType,\n    uint64_t                                    objectHandle,\n    VkPrivateDataSlotEXT                        privateDataSlot,\n    uint64_t*                                   pData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of object\n  data is associated with.\n\n* [`Self::object_handle`] is a handle to the object data is associated with.\n\n* [`Self::private_data_slot`] is a handle to a [`crate::vk::PrivateDataSlotEXT`]specifying location of private data pointer storage.\n\n* [`Self::p_data`] is a pointer to specify where user data is returned.`0` will be written in the absence of a previous call to[`crate::vk::DeviceLoader::set_private_data_ext`] using the object specified by[`Self::object_handle`].\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Due to platform details on Android, implementations might not be able to<br/>reliably return `0` from calls to [`crate::vk::DeviceLoader::get_private_data_ext`] for[`crate::vk::SwapchainKHR`] objects on which [`crate::vk::DeviceLoader::set_private_data_ext`] has not<br/>previously been called.<br/>This erratum is exclusive to the Android platform and objects of type[`crate::vk::SwapchainKHR`].|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-vkGetPrivateDataEXT-objectType-04018  \n  [`Self::object_type`] **must** be [`crate::vk::ObjectType::DEVICE`], or an object type\n  whose parent is [`crate::vk::Device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPrivateDataEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPrivateDataEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-vkGetPrivateDataEXT-privateDataSlot-parameter  \n  [`Self::private_data_slot`] **must** be a valid [`crate::vk::PrivateDataSlotEXT`] handle\n\n* []() VUID-vkGetPrivateDataEXT-pData-parameter  \n  [`Self::p_data`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetPrivateDataEXT-privateDataSlot-parent  \n  [`Self::private_data_slot`] **must** have been created, allocated, or retrieved from [`Self::device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ObjectType`], [`crate::vk::PrivateDataSlotEXT`]\n"]
    #[doc(alias = "vkGetPrivateDataEXT")]
    pub unsafe fn get_private_data_ext(&self, object_type: crate::vk1_0::ObjectType, object_handle: u64, private_data_slot: crate::extensions::ext_private_data::PrivateDataSlotEXT) -> u64 {
        let _function = self.get_private_data_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut data = Default::default();
        let _return = _function(self.handle, object_type as _, object_handle as _, private_data_slot as _, &mut data);
        data
    }
}
