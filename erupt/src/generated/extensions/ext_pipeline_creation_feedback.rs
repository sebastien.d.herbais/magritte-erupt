#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PIPELINE_CREATION_FEEDBACK_SPEC_VERSION")]
pub const EXT_PIPELINE_CREATION_FEEDBACK_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PIPELINE_CREATION_FEEDBACK_EXTENSION_NAME")]
pub const EXT_PIPELINE_CREATION_FEEDBACK_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_pipeline_creation_feedback");
#[doc = "Provided by [`crate::extensions::ext_pipeline_creation_feedback`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT: Self = Self(1000192000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagsEXT.html)) · Bitmask of [`PipelineCreationFeedbackFlagBitsEXT`] <br/> VkPipelineCreationFeedbackFlagsEXT - Bitmask of VkPipelineCreationFeedbackFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef VkFlags VkPipelineCreationFeedbackFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineCreationFeedbackFlagBitsEXT`] is a bitmask type for providing\nzero or more [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCreationFeedbackEXT`], [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html)\n"] # [doc (alias = "VkPipelineCreationFeedbackFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct PipelineCreationFeedbackFlagsEXT : u32 { const VALID_EXT = PipelineCreationFeedbackFlagBitsEXT :: VALID_EXT . 0 ; const APPLICATION_PIPELINE_CACHE_HIT_EXT = PipelineCreationFeedbackFlagBitsEXT :: APPLICATION_PIPELINE_CACHE_HIT_EXT . 0 ; const BASE_PIPELINE_ACCELERATION_EXT = PipelineCreationFeedbackFlagBitsEXT :: BASE_PIPELINE_ACCELERATION_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html)) · Bits enum of [`PipelineCreationFeedbackFlagsEXT`] <br/> VkPipelineCreationFeedbackFlagBitsEXT - Bitmask specifying pipeline or pipeline stage creation feedback\n[](#_c_specification)C Specification\n----------\n\nPossible values of the `flags` member of[`crate::vk::PipelineCreationFeedbackEXT`] are:\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef enum VkPipelineCreationFeedbackFlagBitsEXT {\n    VK_PIPELINE_CREATION_FEEDBACK_VALID_BIT_EXT = 0x00000001,\n    VK_PIPELINE_CREATION_FEEDBACK_APPLICATION_PIPELINE_CACHE_HIT_BIT_EXT = 0x00000002,\n    VK_PIPELINE_CREATION_FEEDBACK_BASE_PIPELINE_ACCELERATION_BIT_EXT = 0x00000004,\n} VkPipelineCreationFeedbackFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::VALID_EXT`] indicates that the\n  feedback information is valid.\n\n* [`Self::APPLICATION_PIPELINE_CACHE_HIT_EXT`]indicates that a readily usable pipeline or pipeline stage was found in\n  the `pipelineCache` specified by the application in the pipeline\n  creation command.\n\n  An implementation **should** set the[`Self::APPLICATION_PIPELINE_CACHE_HIT_EXT`]bit if it was able to avoid the large majority of pipeline or pipeline stage\n  creation work by using the `pipelineCache` parameter of[`crate::vk::PFN_vkCreateGraphicsPipelines`],[`crate::vk::DeviceLoader::create_ray_tracing_pipelines_khr`],[`crate::vk::DeviceLoader::create_ray_tracing_pipelines_nv`],\n  or [`crate::vk::PFN_vkCreateComputePipelines`].\n  When an implementation sets this bit for the entire pipeline, it **may** leave\n  it unset for any stage.\n\n  |   |Note<br/><br/>Implementations are encouraged to provide a meaningful signal to<br/>applications using this bit.<br/>The intention is to communicate to the application that the pipeline or<br/>pipeline stage was created \"as fast as it gets\" using the pipeline cache<br/>provided by the application.<br/>If an implementation uses an internal cache, it is discouraged from setting<br/>this bit as the feedback would be unactionable.|\n  |---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\n* [`Self::BASE_PIPELINE_ACCELERATION_EXT`]indicates that the base pipeline specified by the`basePipelineHandle` or `basePipelineIndex` member of the`Vk*PipelineCreateInfo` structure was used to accelerate the\n  creation of the pipeline.\n\n  An implementation **should** set the[`Self::BASE_PIPELINE_ACCELERATION_EXT`] bit\n  if it was able to avoid a significant amount of work by using the base\n  pipeline.\n\n  |   |Note<br/><br/>While \"significant amount of work\" is subjective, implementations are<br/>encouraged to provide a meaningful signal to applications using this bit.<br/>For example, a 1% reduction in duration may not warrant setting this bit,<br/>while a 50% reduction would.|\n  |---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCreationFeedbackCreateInfoEXT`], [`crate::vk::PipelineCreationFeedbackEXT`], [`crate::vk::PipelineCreationFeedbackFlagBitsEXT`]\n"]
#[doc(alias = "VkPipelineCreationFeedbackFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineCreationFeedbackFlagBitsEXT(pub u32);
impl PipelineCreationFeedbackFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineCreationFeedbackFlagsEXT {
        PipelineCreationFeedbackFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineCreationFeedbackFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VALID_EXT => "VALID_EXT",
            &Self::APPLICATION_PIPELINE_CACHE_HIT_EXT => "APPLICATION_PIPELINE_CACHE_HIT_EXT",
            &Self::BASE_PIPELINE_ACCELERATION_EXT => "BASE_PIPELINE_ACCELERATION_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_pipeline_creation_feedback`]"]
impl crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackFlagBitsEXT {
    pub const VALID_EXT: Self = Self(1);
    pub const APPLICATION_PIPELINE_CACHE_HIT_EXT: Self = Self(2);
    pub const BASE_PIPELINE_ACCELERATION_EXT: Self = Self(4);
}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXT> for crate::vk1_0::ComputePipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXTBuilder<'_>> for crate::vk1_0::ComputePipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXT> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXTBuilder<'_>> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXT> for crate::extensions::nv_ray_tracing::RayTracingPipelineCreateInfoNVBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXTBuilder<'_>> for crate::extensions::nv_ray_tracing::RayTracingPipelineCreateInfoNVBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXT> for crate::extensions::khr_ray_tracing_pipeline::RayTracingPipelineCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCreationFeedbackCreateInfoEXTBuilder<'_>> for crate::extensions::khr_ray_tracing_pipeline::RayTracingPipelineCreateInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackEXT.html)) · Structure <br/> VkPipelineCreationFeedbackEXT - Feedback about the creation of a pipeline or pipeline stage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineCreationFeedbackEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef struct VkPipelineCreationFeedbackEXT {\n    VkPipelineCreationFeedbackFlagsEXT    flags;\n    uint64_t                              duration;\n} VkPipelineCreationFeedbackEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::flags`] is a bitmask of [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html)providing feedback about the creation of a pipeline or of a pipeline\n  stage.\n\n* [`Self::duration`] is the duration spent creating a pipeline or pipeline\n  stage in nanoseconds.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PipelineCreationFeedbackFlagBitsEXT::VALID_EXT`] is not set in[`Self::flags`], an implementation **must** not set any other bits in [`Self::flags`],\nand the values of all other [`crate::vk::PipelineCreationFeedbackEXT`] data members\nare undefined.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCreationFeedbackCreateInfoEXT`], [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html), [`crate::vk::PipelineCreationFeedbackFlagBitsEXT`]\n"]
#[doc(alias = "VkPipelineCreationFeedbackEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineCreationFeedbackEXT {
    pub flags: crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackFlagsEXT,
    pub duration: u64,
}
impl Default for PipelineCreationFeedbackEXT {
    fn default() -> Self {
        Self { flags: Default::default(), duration: Default::default() }
    }
}
impl std::fmt::Debug for PipelineCreationFeedbackEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineCreationFeedbackEXT").field("flags", &self.flags).field("duration", &self.duration).finish()
    }
}
impl PipelineCreationFeedbackEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineCreationFeedbackEXTBuilder<'a> {
        PipelineCreationFeedbackEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackEXT.html)) · Builder of [`PipelineCreationFeedbackEXT`] <br/> VkPipelineCreationFeedbackEXT - Feedback about the creation of a pipeline or pipeline stage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineCreationFeedbackEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef struct VkPipelineCreationFeedbackEXT {\n    VkPipelineCreationFeedbackFlagsEXT    flags;\n    uint64_t                              duration;\n} VkPipelineCreationFeedbackEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::flags`] is a bitmask of [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html)providing feedback about the creation of a pipeline or of a pipeline\n  stage.\n\n* [`Self::duration`] is the duration spent creating a pipeline or pipeline\n  stage in nanoseconds.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PipelineCreationFeedbackFlagBitsEXT::VALID_EXT`] is not set in[`Self::flags`], an implementation **must** not set any other bits in [`Self::flags`],\nand the values of all other [`crate::vk::PipelineCreationFeedbackEXT`] data members\nare undefined.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCreationFeedbackCreateInfoEXT`], [VkPipelineCreationFeedbackFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html), [`crate::vk::PipelineCreationFeedbackFlagBitsEXT`]\n"]
#[repr(transparent)]
pub struct PipelineCreationFeedbackEXTBuilder<'a>(PipelineCreationFeedbackEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineCreationFeedbackEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineCreationFeedbackEXTBuilder<'a> {
        PipelineCreationFeedbackEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn duration(mut self, duration: u64) -> Self {
        self.0.duration = duration as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineCreationFeedbackEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineCreationFeedbackEXTBuilder<'a> {
    fn default() -> PipelineCreationFeedbackEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineCreationFeedbackEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineCreationFeedbackEXTBuilder<'a> {
    type Target = PipelineCreationFeedbackEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineCreationFeedbackEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackCreateInfoEXT.html)) · Structure <br/> VkPipelineCreationFeedbackCreateInfoEXT - Request for feedback about the creation of a pipeline\n[](#_c_specification)C Specification\n----------\n\nFeedback about the creation of a particular pipeline object **can** be obtained\nby adding a [`crate::vk::PipelineCreationFeedbackCreateInfoEXT`] structure to the[`Self::p_next`] chain of [`crate::vk::GraphicsPipelineCreateInfo`],[`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::RayTracingPipelineCreateInfoNV`],\nor [`crate::vk::ComputePipelineCreateInfo`].\nThe [`crate::vk::PipelineCreationFeedbackCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef struct VkPipelineCreationFeedbackCreateInfoEXT {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkPipelineCreationFeedbackEXT*    pPipelineCreationFeedback;\n    uint32_t                          pipelineStageCreationFeedbackCount;\n    VkPipelineCreationFeedbackEXT*    pPipelineStageCreationFeedbacks;\n} VkPipelineCreationFeedbackCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_pipeline_creation_feedback`] is a pointer to a[`crate::vk::PipelineCreationFeedbackEXT`] structure.\n\n* [`Self::pipeline_stage_creation_feedback_count`] is the number of elements in[`Self::p_pipeline_stage_creation_feedbacks`].\n\n* [`Self::p_pipeline_stage_creation_feedbacks`] is a pointer to an array of[`Self::pipeline_stage_creation_feedback_count`][`crate::vk::PipelineCreationFeedbackEXT`] structures.\n[](#_description)Description\n----------\n\nAn implementation **should** write pipeline creation feedback to[`Self::p_pipeline_creation_feedback`] and **may** write pipeline stage creation\nfeedback to [`Self::p_pipeline_stage_creation_feedbacks`].\nAn implementation **must** set or clear the[`crate::vk::PipelineCreationFeedbackFlagBitsEXT::VALID_EXT`] in[`crate::vk::PipelineCreationFeedbackEXT::flags`] for[`Self::p_pipeline_creation_feedback`] and every element of[`Self::p_pipeline_stage_creation_feedbacks`].\n\n|   |Note<br/><br/>One common scenario for an implementation to skip per-stage feedback is when[`crate::vk::PipelineCreationFeedbackFlagBitsEXT::APPLICATION_PIPELINE_CACHE_HIT_EXT`]is set in [`Self::p_pipeline_creation_feedback`].|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nWhen chained to[`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::RayTracingPipelineCreateInfoNV`],\nor[`crate::vk::GraphicsPipelineCreateInfo`], the `i` element of[`Self::p_pipeline_stage_creation_feedbacks`] corresponds to the `i` element of[`crate::vk::RayTracingPipelineCreateInfoKHR::p_stages`],[`crate::vk::RayTracingPipelineCreateInfoNV::p_stages`],\nor[`crate::vk::GraphicsPipelineCreateInfo::p_stages`].\nWhen chained to [`crate::vk::ComputePipelineCreateInfo`], the first element of[`Self::p_pipeline_stage_creation_feedbacks`] corresponds to[`crate::vk::ComputePipelineCreateInfo::stage`].\n\nValid Usage\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02668  \n   When chained to [`crate::vk::GraphicsPipelineCreateInfo`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::GraphicsPipelineCreateInfo::stage_count`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02669  \n   When chained to [`crate::vk::ComputePipelineCreateInfo`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal 1\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02670  \n   When chained to [`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::RayTracingPipelineCreateInfoKHR::stage_count`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02969  \n   When chained to [`crate::vk::RayTracingPipelineCreateInfoNV`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::RayTracingPipelineCreateInfoNV::stage_count`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pPipelineCreationFeedback-parameter  \n  [`Self::p_pipeline_creation_feedback`] **must** be a valid pointer to a [`crate::vk::PipelineCreationFeedbackEXT`] structure\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pPipelineStageCreationFeedbacks-parameter  \n  [`Self::p_pipeline_stage_creation_feedbacks`] **must** be a valid pointer to an array of [`Self::pipeline_stage_creation_feedback_count`] [`crate::vk::PipelineCreationFeedbackEXT`] structures\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-arraylength  \n  [`Self::pipeline_stage_creation_feedback_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ComputePipelineCreateInfo`], [`crate::vk::GraphicsPipelineCreateInfo`], [`crate::vk::PipelineCreationFeedbackEXT`], [`crate::vk::RayTracingPipelineCreateInfoKHR`], [`crate::vk::RayTracingPipelineCreateInfoNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineCreationFeedbackCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineCreationFeedbackCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_pipeline_creation_feedback: *mut crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackEXT,
    pub pipeline_stage_creation_feedback_count: u32,
    pub p_pipeline_stage_creation_feedbacks: *mut crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackEXT,
}
impl PipelineCreationFeedbackCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT;
}
impl Default for PipelineCreationFeedbackCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_pipeline_creation_feedback: std::ptr::null_mut(), pipeline_stage_creation_feedback_count: Default::default(), p_pipeline_stage_creation_feedbacks: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for PipelineCreationFeedbackCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineCreationFeedbackCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_pipeline_creation_feedback", &self.p_pipeline_creation_feedback).field("pipeline_stage_creation_feedback_count", &self.pipeline_stage_creation_feedback_count).field("p_pipeline_stage_creation_feedbacks", &self.p_pipeline_stage_creation_feedbacks).finish()
    }
}
impl PipelineCreationFeedbackCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
        PipelineCreationFeedbackCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCreationFeedbackCreateInfoEXT.html)) · Builder of [`PipelineCreationFeedbackCreateInfoEXT`] <br/> VkPipelineCreationFeedbackCreateInfoEXT - Request for feedback about the creation of a pipeline\n[](#_c_specification)C Specification\n----------\n\nFeedback about the creation of a particular pipeline object **can** be obtained\nby adding a [`crate::vk::PipelineCreationFeedbackCreateInfoEXT`] structure to the[`Self::p_next`] chain of [`crate::vk::GraphicsPipelineCreateInfo`],[`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::RayTracingPipelineCreateInfoNV`],\nor [`crate::vk::ComputePipelineCreateInfo`].\nThe [`crate::vk::PipelineCreationFeedbackCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_pipeline_creation_feedback\ntypedef struct VkPipelineCreationFeedbackCreateInfoEXT {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkPipelineCreationFeedbackEXT*    pPipelineCreationFeedback;\n    uint32_t                          pipelineStageCreationFeedbackCount;\n    VkPipelineCreationFeedbackEXT*    pPipelineStageCreationFeedbacks;\n} VkPipelineCreationFeedbackCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_pipeline_creation_feedback`] is a pointer to a[`crate::vk::PipelineCreationFeedbackEXT`] structure.\n\n* [`Self::pipeline_stage_creation_feedback_count`] is the number of elements in[`Self::p_pipeline_stage_creation_feedbacks`].\n\n* [`Self::p_pipeline_stage_creation_feedbacks`] is a pointer to an array of[`Self::pipeline_stage_creation_feedback_count`][`crate::vk::PipelineCreationFeedbackEXT`] structures.\n[](#_description)Description\n----------\n\nAn implementation **should** write pipeline creation feedback to[`Self::p_pipeline_creation_feedback`] and **may** write pipeline stage creation\nfeedback to [`Self::p_pipeline_stage_creation_feedbacks`].\nAn implementation **must** set or clear the[`crate::vk::PipelineCreationFeedbackFlagBitsEXT::VALID_EXT`] in[`crate::vk::PipelineCreationFeedbackEXT::flags`] for[`Self::p_pipeline_creation_feedback`] and every element of[`Self::p_pipeline_stage_creation_feedbacks`].\n\n|   |Note<br/><br/>One common scenario for an implementation to skip per-stage feedback is when[`crate::vk::PipelineCreationFeedbackFlagBitsEXT::APPLICATION_PIPELINE_CACHE_HIT_EXT`]is set in [`Self::p_pipeline_creation_feedback`].|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nWhen chained to[`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::RayTracingPipelineCreateInfoNV`],\nor[`crate::vk::GraphicsPipelineCreateInfo`], the `i` element of[`Self::p_pipeline_stage_creation_feedbacks`] corresponds to the `i` element of[`crate::vk::RayTracingPipelineCreateInfoKHR::p_stages`],[`crate::vk::RayTracingPipelineCreateInfoNV::p_stages`],\nor[`crate::vk::GraphicsPipelineCreateInfo::p_stages`].\nWhen chained to [`crate::vk::ComputePipelineCreateInfo`], the first element of[`Self::p_pipeline_stage_creation_feedbacks`] corresponds to[`crate::vk::ComputePipelineCreateInfo::stage`].\n\nValid Usage\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02668  \n   When chained to [`crate::vk::GraphicsPipelineCreateInfo`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::GraphicsPipelineCreateInfo::stage_count`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02669  \n   When chained to [`crate::vk::ComputePipelineCreateInfo`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal 1\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02670  \n   When chained to [`crate::vk::RayTracingPipelineCreateInfoKHR`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::RayTracingPipelineCreateInfoKHR::stage_count`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-02969  \n   When chained to [`crate::vk::RayTracingPipelineCreateInfoNV`],[`crate::vk::PipelineCreationFeedbackEXT::pipeline_stage_creation_feedback_count`]**must** equal [`crate::vk::RayTracingPipelineCreateInfoNV::stage_count`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pPipelineCreationFeedback-parameter  \n  [`Self::p_pipeline_creation_feedback`] **must** be a valid pointer to a [`crate::vk::PipelineCreationFeedbackEXT`] structure\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pPipelineStageCreationFeedbacks-parameter  \n  [`Self::p_pipeline_stage_creation_feedbacks`] **must** be a valid pointer to an array of [`Self::pipeline_stage_creation_feedback_count`] [`crate::vk::PipelineCreationFeedbackEXT`] structures\n\n* []() VUID-VkPipelineCreationFeedbackCreateInfoEXT-pipelineStageCreationFeedbackCount-arraylength  \n  [`Self::pipeline_stage_creation_feedback_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ComputePipelineCreateInfo`], [`crate::vk::GraphicsPipelineCreateInfo`], [`crate::vk::PipelineCreationFeedbackEXT`], [`crate::vk::RayTracingPipelineCreateInfoKHR`], [`crate::vk::RayTracingPipelineCreateInfoNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineCreationFeedbackCreateInfoEXTBuilder<'a>(PipelineCreationFeedbackCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
        PipelineCreationFeedbackCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline_creation_feedback(mut self, pipeline_creation_feedback: &'a mut crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackEXT) -> Self {
        self.0.p_pipeline_creation_feedback = pipeline_creation_feedback as _;
        self
    }
    #[inline]
    pub fn pipeline_stage_creation_feedbacks(mut self, pipeline_stage_creation_feedbacks: &'a mut [crate::extensions::ext_pipeline_creation_feedback::PipelineCreationFeedbackEXTBuilder]) -> Self {
        self.0.p_pipeline_stage_creation_feedbacks = pipeline_stage_creation_feedbacks.as_ptr() as _;
        self.0.pipeline_stage_creation_feedback_count = pipeline_stage_creation_feedbacks.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineCreationFeedbackCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
    type Target = PipelineCreationFeedbackCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineCreationFeedbackCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
