#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CUSTOM_BORDER_COLOR_SPEC_VERSION")]
pub const EXT_CUSTOM_BORDER_COLOR_SPEC_VERSION: u32 = 12;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CUSTOM_BORDER_COLOR_EXTENSION_NAME")]
pub const EXT_CUSTOM_BORDER_COLOR_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_custom_border_color");
#[doc = "Provided by [`crate::extensions::ext_custom_border_color`]"]
impl crate::vk1_0::BorderColor {
    pub const FLOAT_CUSTOM_EXT: Self = Self(1000287003);
    pub const INT_CUSTOM_EXT: Self = Self(1000287004);
}
#[doc = "Provided by [`crate::extensions::ext_custom_border_color`]"]
impl crate::vk1_0::StructureType {
    pub const SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT: Self = Self(1000287000);
    pub const PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT: Self = Self(1000287001);
    pub const PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT: Self = Self(1000287002);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCustomBorderColorFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SamplerCustomBorderColorCreateInfoEXT> for crate::vk1_0::SamplerCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SamplerCustomBorderColorCreateInfoEXTBuilder<'_>> for crate::vk1_0::SamplerCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCustomBorderColorFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCustomBorderColorPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerCustomBorderColorCreateInfoEXT.html)) · Structure <br/> VkSamplerCustomBorderColorCreateInfoEXT - Structure specifying custom border color\n[](#_c_specification)C Specification\n----------\n\nIn addition to the predefined border color values, applications **can** provide\na custom border color value by including the[`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure in the[`crate::vk::SamplerCreateInfo::p_next`] chain.\n\nThe [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkSamplerCustomBorderColorCreateInfoEXT {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkClearColorValue    customBorderColor;\n    VkFormat             format;\n} VkSamplerCustomBorderColorCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::custom_border_color`] is a [`crate::vk::ClearColorValue`] representing the\n  desired custom sampler border color.\n\n* [`Self::format`] is a [`crate::vk::Format`] representing the format of the sampled\n  image view(s).\n  This field may be [`crate::vk::Format::UNDEFINED`] if the[customBorderColorWithoutFormat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-customBorderColorWithoutFormat)feature is enabled.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04013  \n   If provided [`Self::format`] is not [`crate::vk::Format::UNDEFINED`] then the[`crate::vk::SamplerCreateInfo::border_color`] type **must** match the\n  sampled type of the provided [`Self::format`], as shown in the *SPIR-V\n  Sampled Type* column of the [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-numericformat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-numericformat) table\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04014  \n   If the[customBorderColorWithoutFormat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-customBorderColorWithoutFormat)feature is not enabled then [`Self::format`] **must** not be[`crate::vk::Format::UNDEFINED`]\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04015  \n   If the sampler is used to sample an image view of[`crate::vk::Format::B4G4R4A4_UNORM_PACK16`],[`crate::vk::Format::B5G6R5_UNORM_PACK16`], or[`crate::vk::Format::B5G5R5A1_UNORM_PACK16`] format then [`Self::format`] **must** not\n  be [`crate::vk::Format::UNDEFINED`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT`]\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ClearColorValue`], [`crate::vk::Format`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkSamplerCustomBorderColorCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SamplerCustomBorderColorCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub custom_border_color: crate::vk1_0::ClearColorValue,
    pub format: crate::vk1_0::Format,
}
impl SamplerCustomBorderColorCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT;
}
impl Default for SamplerCustomBorderColorCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), custom_border_color: Default::default(), format: Default::default() }
    }
}
impl std::fmt::Debug for SamplerCustomBorderColorCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SamplerCustomBorderColorCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("custom_border_color", &self.custom_border_color).field("format", &self.format).finish()
    }
}
impl SamplerCustomBorderColorCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
        SamplerCustomBorderColorCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSamplerCustomBorderColorCreateInfoEXT.html)) · Builder of [`SamplerCustomBorderColorCreateInfoEXT`] <br/> VkSamplerCustomBorderColorCreateInfoEXT - Structure specifying custom border color\n[](#_c_specification)C Specification\n----------\n\nIn addition to the predefined border color values, applications **can** provide\na custom border color value by including the[`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure in the[`crate::vk::SamplerCreateInfo::p_next`] chain.\n\nThe [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkSamplerCustomBorderColorCreateInfoEXT {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkClearColorValue    customBorderColor;\n    VkFormat             format;\n} VkSamplerCustomBorderColorCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::custom_border_color`] is a [`crate::vk::ClearColorValue`] representing the\n  desired custom sampler border color.\n\n* [`Self::format`] is a [`crate::vk::Format`] representing the format of the sampled\n  image view(s).\n  This field may be [`crate::vk::Format::UNDEFINED`] if the[customBorderColorWithoutFormat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-customBorderColorWithoutFormat)feature is enabled.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04013  \n   If provided [`Self::format`] is not [`crate::vk::Format::UNDEFINED`] then the[`crate::vk::SamplerCreateInfo::border_color`] type **must** match the\n  sampled type of the provided [`Self::format`], as shown in the *SPIR-V\n  Sampled Type* column of the [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-numericformat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-numericformat) table\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04014  \n   If the[customBorderColorWithoutFormat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-customBorderColorWithoutFormat)feature is not enabled then [`Self::format`] **must** not be[`crate::vk::Format::UNDEFINED`]\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-04015  \n   If the sampler is used to sample an image view of[`crate::vk::Format::B4G4R4A4_UNORM_PACK16`],[`crate::vk::Format::B5G6R5_UNORM_PACK16`], or[`crate::vk::Format::B5G5R5A1_UNORM_PACK16`] format then [`Self::format`] **must** not\n  be [`crate::vk::Format::UNDEFINED`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT`]\n\n* []() VUID-VkSamplerCustomBorderColorCreateInfoEXT-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ClearColorValue`], [`crate::vk::Format`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct SamplerCustomBorderColorCreateInfoEXTBuilder<'a>(SamplerCustomBorderColorCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
        SamplerCustomBorderColorCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn custom_border_color(mut self, custom_border_color: crate::vk1_0::ClearColorValue) -> Self {
        self.0.custom_border_color = custom_border_color as _;
        self
    }
    #[inline]
    pub fn format(mut self, format: crate::vk1_0::Format) -> Self {
        self.0.format = format as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SamplerCustomBorderColorCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
    fn default() -> SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
    type Target = SamplerCustomBorderColorCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SamplerCustomBorderColorCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCustomBorderColorPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceCustomBorderColorPropertiesEXT - Structure describing whether custom border colors can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCustomBorderColorPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkPhysicalDeviceCustomBorderColorPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxCustomBorderColorSamplers;\n} VkPhysicalDeviceCustomBorderColorPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* []()[`Self::max_custom_border_color_samplers`] indicates the maximum number of\n  samplers with custom border colors which **can** simultaneously exist on a\n  device.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceCustomBorderColorPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCustomBorderColorPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceCustomBorderColorPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceCustomBorderColorPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_custom_border_color_samplers: u32,
}
impl PhysicalDeviceCustomBorderColorPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceCustomBorderColorPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_custom_border_color_samplers: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceCustomBorderColorPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceCustomBorderColorPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_custom_border_color_samplers", &self.max_custom_border_color_samplers).finish()
    }
}
impl PhysicalDeviceCustomBorderColorPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
        PhysicalDeviceCustomBorderColorPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCustomBorderColorPropertiesEXT.html)) · Builder of [`PhysicalDeviceCustomBorderColorPropertiesEXT`] <br/> VkPhysicalDeviceCustomBorderColorPropertiesEXT - Structure describing whether custom border colors can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCustomBorderColorPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkPhysicalDeviceCustomBorderColorPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxCustomBorderColorSamplers;\n} VkPhysicalDeviceCustomBorderColorPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* []()[`Self::max_custom_border_color_samplers`] indicates the maximum number of\n  samplers with custom border colors which **can** simultaneously exist on a\n  device.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceCustomBorderColorPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCustomBorderColorPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a>(PhysicalDeviceCustomBorderColorPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
        PhysicalDeviceCustomBorderColorPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_custom_border_color_samplers(mut self, max_custom_border_color_samplers: u32) -> Self {
        self.0.max_custom_border_color_samplers = max_custom_border_color_samplers as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceCustomBorderColorPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceCustomBorderColorPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceCustomBorderColorPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCustomBorderColorFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceCustomBorderColorFeaturesEXT - Structure describing whether custom border colors can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkPhysicalDeviceCustomBorderColorFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           customBorderColors;\n    VkBool32           customBorderColorWithoutFormat;\n} VkPhysicalDeviceCustomBorderColorFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::custom_border_colors`] indicates that\n  the implementation supports providing a `borderColor` value with one\n  of the following values at sampler creation time:\n\n  * [`crate::vk::BorderColor::FLOAT_CUSTOM_EXT`]\n\n  * [`crate::vk::BorderColor::INT_CUSTOM_EXT`]\n\n* []()[`Self::custom_border_color_without_format`] indicates that explicit formats are\n  not required for custom border colors and the value of the `format`member of the [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure**may** be [`crate::vk::Format::UNDEFINED`].\n  If this feature bit is not set, applications **must** provide the[`crate::vk::Format`] of the image view(s) being sampled by this sampler in the`format` member of the [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`]structure.\n\nIf the [`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCustomBorderColorFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceCustomBorderColorFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceCustomBorderColorFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub custom_border_colors: crate::vk1_0::Bool32,
    pub custom_border_color_without_format: crate::vk1_0::Bool32,
}
impl PhysicalDeviceCustomBorderColorFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT;
}
impl Default for PhysicalDeviceCustomBorderColorFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), custom_border_colors: Default::default(), custom_border_color_without_format: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceCustomBorderColorFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceCustomBorderColorFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("custom_border_colors", &(self.custom_border_colors != 0)).field("custom_border_color_without_format", &(self.custom_border_color_without_format != 0)).finish()
    }
}
impl PhysicalDeviceCustomBorderColorFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
        PhysicalDeviceCustomBorderColorFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCustomBorderColorFeaturesEXT.html)) · Builder of [`PhysicalDeviceCustomBorderColorFeaturesEXT`] <br/> VkPhysicalDeviceCustomBorderColorFeaturesEXT - Structure describing whether custom border colors can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_custom_border_color\ntypedef struct VkPhysicalDeviceCustomBorderColorFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           customBorderColors;\n    VkBool32           customBorderColorWithoutFormat;\n} VkPhysicalDeviceCustomBorderColorFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::custom_border_colors`] indicates that\n  the implementation supports providing a `borderColor` value with one\n  of the following values at sampler creation time:\n\n  * [`crate::vk::BorderColor::FLOAT_CUSTOM_EXT`]\n\n  * [`crate::vk::BorderColor::INT_CUSTOM_EXT`]\n\n* []()[`Self::custom_border_color_without_format`] indicates that explicit formats are\n  not required for custom border colors and the value of the `format`member of the [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`] structure**may** be [`crate::vk::Format::UNDEFINED`].\n  If this feature bit is not set, applications **must** provide the[`crate::vk::Format`] of the image view(s) being sampled by this sampler in the`format` member of the [`crate::vk::SamplerCustomBorderColorCreateInfoEXT`]structure.\n\nIf the [`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCustomBorderColorFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCustomBorderColorFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a>(PhysicalDeviceCustomBorderColorFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
        PhysicalDeviceCustomBorderColorFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn custom_border_colors(mut self, custom_border_colors: bool) -> Self {
        self.0.custom_border_colors = custom_border_colors as _;
        self
    }
    #[inline]
    pub fn custom_border_color_without_format(mut self, custom_border_color_without_format: bool) -> Self {
        self.0.custom_border_color_without_format = custom_border_color_without_format as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceCustomBorderColorFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceCustomBorderColorFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceCustomBorderColorFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
