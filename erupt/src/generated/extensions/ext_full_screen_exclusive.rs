#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FULL_SCREEN_EXCLUSIVE_SPEC_VERSION")]
pub const EXT_FULL_SCREEN_EXCLUSIVE_SPEC_VERSION: u32 = 4;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FULL_SCREEN_EXCLUSIVE_EXTENSION_NAME")]
pub const EXT_FULL_SCREEN_EXCLUSIVE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_full_screen_exclusive");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SURFACE_PRESENT_MODES2_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceSurfacePresentModes2EXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_DEVICE_GROUP_SURFACE_PRESENT_MODES2_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetDeviceGroupSurfacePresentModes2EXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_ACQUIRE_FULL_SCREEN_EXCLUSIVE_MODE_EXT: *const std::os::raw::c_char = crate::cstr!("vkAcquireFullScreenExclusiveModeEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_RELEASE_FULL_SCREEN_EXCLUSIVE_MODE_EXT: *const std::os::raw::c_char = crate::cstr!("vkReleaseFullScreenExclusiveModeEXT");
#[doc = "Provided by [`crate::extensions::ext_full_screen_exclusive`]"]
impl crate::vk1_0::Result {
    pub const ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: Self = Self(-1000255000);
}
#[doc = "Provided by [`crate::extensions::ext_full_screen_exclusive`]"]
impl crate::vk1_0::StructureType {
    pub const SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT: Self = Self(1000255000);
    pub const SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT: Self = Self(1000255002);
    pub const SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT: Self = Self(1000255001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFullScreenExclusiveEXT.html)) · Enum <br/> VkFullScreenExclusiveEXT - Hint values an application can specify affecting full-screen transition behavior\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`]::`fullScreenExclusive` are:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\ntypedef enum VkFullScreenExclusiveEXT {\n    VK_FULL_SCREEN_EXCLUSIVE_DEFAULT_EXT = 0,\n    VK_FULL_SCREEN_EXCLUSIVE_ALLOWED_EXT = 1,\n    VK_FULL_SCREEN_EXCLUSIVE_DISALLOWED_EXT = 2,\n    VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT = 3,\n} VkFullScreenExclusiveEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::DEFAULT_EXT`] indicates the implementation**should** determine the appropriate full-screen method by whatever means\n  it deems appropriate.\n\n* [`Self::ALLOWED_EXT`] indicates the implementation**may** use full-screen exclusive mechanisms when available.\n  Such mechanisms **may** result in better performance and/or the\n  availability of different presentation capabilities, but **may** require a\n  more disruptive transition during swapchain initialization, first\n  presentation and/or destruction.\n\n* [`Self::DISALLOWED_EXT`] indicates the\n  implementation **should** avoid using full-screen mechanisms which rely on\n  disruptive transitions.\n\n* [`Self::APPLICATION_CONTROLLED_EXT`] indicates the\n  application will manage full-screen exclusive mode by using the[`crate::vk::DeviceLoader::acquire_full_screen_exclusive_mode_ext`] and[`crate::vk::DeviceLoader::release_full_screen_exclusive_mode_ext`] commands.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`]\n"]
#[doc(alias = "VkFullScreenExclusiveEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct FullScreenExclusiveEXT(pub i32);
impl std::fmt::Debug for FullScreenExclusiveEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_EXT => "DEFAULT_EXT",
            &Self::ALLOWED_EXT => "ALLOWED_EXT",
            &Self::DISALLOWED_EXT => "DISALLOWED_EXT",
            &Self::APPLICATION_CONTROLLED_EXT => "APPLICATION_CONTROLLED_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_full_screen_exclusive`]"]
impl crate::extensions::ext_full_screen_exclusive::FullScreenExclusiveEXT {
    pub const DEFAULT_EXT: Self = Self(0);
    pub const ALLOWED_EXT: Self = Self(1);
    pub const DISALLOWED_EXT: Self = Self(2);
    pub const APPLICATION_CONTROLLED_EXT: Self = Self(3);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfacePresentModes2EXT.html)) · Function <br/> vkGetPhysicalDeviceSurfacePresentModes2EXT - Query supported presentation modes\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to query the supported presentation modes for a surface\ncombined with select other fixed swapchain creation parameters, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkGetPhysicalDeviceSurfacePresentModes2EXT(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    uint32_t*                                   pPresentModeCount,\n    VkPresentModeKHR*                           pPresentModes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_present_mode_count`] is a pointer to an integer related to the number\n  of presentation modes available or queried, as described below.\n\n* [`Self::p_present_modes`] is either `NULL` or a pointer to an array of[`crate::vk::PresentModeKHR`] values, indicating the supported presentation\n  modes.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::get_physical_device_surface_present_modes2_ext`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_present_modes_khr`], with the ability to specify\nextended inputs via chained input structures.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pPresentModeCount-parameter  \n  [`Self::p_present_mode_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pPresentModes-parameter  \n   If the value referenced by [`Self::p_present_mode_count`] is not `0`, and [`Self::p_present_modes`] is not `NULL`, [`Self::p_present_modes`] **must** be a valid pointer to an array of [`Self::p_present_mode_count`] [`crate::vk::PresentModeKHR`] values\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::PresentModeKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSurfacePresentModes2EXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_surface_info: *const crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, p_present_mode_count: *mut u32, p_present_modes: *mut crate::extensions::khr_surface::PresentModeKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceGroupSurfacePresentModes2EXT.html)) · Function <br/> vkGetDeviceGroupSurfacePresentModes2EXT - Query device group present capabilities for a surface\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to query the supported device group presentation modes for a\nsurface combined with select other fixed swapchain creation parameters,\ncall:\n\n```\n// Provided by VK_EXT_full_screen_exclusive with VK_KHR_device_group, VK_EXT_full_screen_exclusive with VK_VERSION_1_1\nVkResult vkGetDeviceGroupSurfacePresentModes2EXT(\n    VkDevice                                    device,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    VkDeviceGroupPresentModeFlagsKHR*           pModes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device.\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_modes`] is a pointer to a [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`] in\n  which the supported device group present modes for the surface are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::get_device_group_surface_present_modes2_ext`] behaves similarly to[`crate::vk::DeviceLoader::get_device_group_surface_present_modes_khr`], with the ability to specify\nextended inputs via chained input structures.\n\nValid Usage\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pSurfaceInfo-06213  \n  `pSurfaceInfo->surface` **must** be supported by all physical devices\n  associated with [`Self::device`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an equivalent\n  platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pModes-parameter  \n  [`Self::p_modes`] **must** be a valid pointer to a [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDeviceGroupSurfacePresentModes2EXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_surface_info: *const crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, p_modes: *mut crate::extensions::khr_swapchain::DeviceGroupPresentModeFlagsKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquireFullScreenExclusiveModeEXT.html)) · Function <br/> vkAcquireFullScreenExclusiveModeEXT - Acquire full-screen exclusive mode for a swapchain\n[](#_c_specification)C Specification\n----------\n\nTo acquire exclusive full-screen access for a swapchain, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkAcquireFullScreenExclusiveModeEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to acquire exclusive full-screen access\n  for.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02674  \n  [`Self::swapchain`] **must** not be in the retired state\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02675  \n  [`Self::swapchain`] **must** be a swapchain created with a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, with`fullScreenExclusive` set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`]\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02676  \n  [`Self::swapchain`] **must** not currently have exclusive full-screen access\n\nA return value of [`crate::vk::Result::SUCCESS`] indicates that the [`Self::swapchain`]successfully acquired exclusive full-screen access.\nThe swapchain will retain this exclusivity until either the application\nreleases exclusive full-screen access with[`crate::vk::DeviceLoader::release_full_screen_exclusive_mode_ext`], destroys the swapchain, or if any\nof the swapchain commands return[`crate::vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT`] indicating that the mode\nwas lost because of platform-specific changes.\n\nIf the swapchain was unable to acquire exclusive full-screen access to the\ndisplay then [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`] is returned.\nAn application **can** attempt to acquire exclusive full-screen access again\nfor the same swapchain even if this command fails, or if[`crate::vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT`] has been returned by a\nswapchain command.\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkAcquireFullScreenExclusiveModeEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseFullScreenExclusiveModeEXT.html)) · Function <br/> vkReleaseFullScreenExclusiveModeEXT - Release full-screen exclusive mode from a swapchain\n[](#_c_specification)C Specification\n----------\n\nTo release exclusive full-screen access from a swapchain, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkReleaseFullScreenExclusiveModeEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to release exclusive full-screen access\n  from.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Applications will not be able to present to [`Self::swapchain`] after this call<br/>until exclusive full-screen access is reacquired.<br/>This is usually useful to handle when an application is minimised or<br/>otherwise intends to stop presenting for a time.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-vkReleaseFullScreenExclusiveModeEXT-swapchain-02677  \n  [`Self::swapchain`] **must** not be in the retired state\n\n* []() VUID-vkReleaseFullScreenExclusiveModeEXT-swapchain-02678  \n  [`Self::swapchain`] **must** be a swapchain created with a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, with`fullScreenExclusive` set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkReleaseFullScreenExclusiveModeEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveInfoEXT> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveInfoEXTBuilder<'_>> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveWin32InfoEXT> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'_>> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveInfoEXT> for crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveInfoEXTBuilder<'_>> for crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveWin32InfoEXT> for crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'_>> for crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, SurfaceCapabilitiesFullScreenExclusiveEXT> for crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'_>> for crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFullScreenExclusiveInfoEXT.html)) · Structure <br/> VkSurfaceFullScreenExclusiveInfoEXT - Structure specifying the preferred full-screen transition behavior\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::SwapchainCreateInfoKHR`] includes a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, then that structure\nspecifies the application’s preferred full-screen transition behavior.\n\nThe [`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\ntypedef struct VkSurfaceFullScreenExclusiveInfoEXT {\n    VkStructureType             sType;\n    void*                       pNext;\n    VkFullScreenExclusiveEXT    fullScreenExclusive;\n} VkSurfaceFullScreenExclusiveInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::full_screen_exclusive`] is a [`crate::vk::FullScreenExclusiveEXT`] value\n  specifying the preferred full-screen transition behavior.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::full_screen_exclusive`] is considered to\nbe [`crate::vk::FullScreenExclusiveEXT::DEFAULT_EXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFullScreenExclusiveInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT`]\n\n* []() VUID-VkSurfaceFullScreenExclusiveInfoEXT-fullScreenExclusive-parameter  \n  [`Self::full_screen_exclusive`] **must** be a valid [`crate::vk::FullScreenExclusiveEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FullScreenExclusiveEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkSurfaceFullScreenExclusiveInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceFullScreenExclusiveInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub full_screen_exclusive: crate::extensions::ext_full_screen_exclusive::FullScreenExclusiveEXT,
}
impl SurfaceFullScreenExclusiveInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT;
}
impl Default for SurfaceFullScreenExclusiveInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), full_screen_exclusive: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceFullScreenExclusiveInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceFullScreenExclusiveInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("full_screen_exclusive", &self.full_screen_exclusive).finish()
    }
}
impl SurfaceFullScreenExclusiveInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
        SurfaceFullScreenExclusiveInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFullScreenExclusiveInfoEXT.html)) · Builder of [`SurfaceFullScreenExclusiveInfoEXT`] <br/> VkSurfaceFullScreenExclusiveInfoEXT - Structure specifying the preferred full-screen transition behavior\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::SwapchainCreateInfoKHR`] includes a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, then that structure\nspecifies the application’s preferred full-screen transition behavior.\n\nThe [`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\ntypedef struct VkSurfaceFullScreenExclusiveInfoEXT {\n    VkStructureType             sType;\n    void*                       pNext;\n    VkFullScreenExclusiveEXT    fullScreenExclusive;\n} VkSurfaceFullScreenExclusiveInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::full_screen_exclusive`] is a [`crate::vk::FullScreenExclusiveEXT`] value\n  specifying the preferred full-screen transition behavior.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::full_screen_exclusive`] is considered to\nbe [`crate::vk::FullScreenExclusiveEXT::DEFAULT_EXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFullScreenExclusiveInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT`]\n\n* []() VUID-VkSurfaceFullScreenExclusiveInfoEXT-fullScreenExclusive-parameter  \n  [`Self::full_screen_exclusive`] **must** be a valid [`crate::vk::FullScreenExclusiveEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FullScreenExclusiveEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct SurfaceFullScreenExclusiveInfoEXTBuilder<'a>(SurfaceFullScreenExclusiveInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
        SurfaceFullScreenExclusiveInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn full_screen_exclusive(mut self, full_screen_exclusive: crate::extensions::ext_full_screen_exclusive::FullScreenExclusiveEXT) -> Self {
        self.0.full_screen_exclusive = full_screen_exclusive as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceFullScreenExclusiveInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
    fn default() -> SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
    type Target = SurfaceFullScreenExclusiveInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceFullScreenExclusiveInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFullScreenExclusiveWin32InfoEXT.html)) · Structure <br/> VkSurfaceFullScreenExclusiveWin32InfoEXT - Structure specifying additional creation parameters specific to Win32 fullscreen exclusive mode\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_full_screen_exclusive with VK_KHR_win32_surface\ntypedef struct VkSurfaceFullScreenExclusiveWin32InfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    HMONITOR           hmonitor;\n} VkSurfaceFullScreenExclusiveWin32InfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::hmonitor`] is the Win32 `HMONITOR` handle identifying the display\n  to create the surface with.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>If [`Self::hmonitor`] is invalidated (e.g. the monitor is unplugged) during the<br/>lifetime of a swapchain created with this structure, operations on that<br/>swapchain will return [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`].|\n|---|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\n|   |Note<br/><br/>It is the responsibility of the application to change the display settings<br/>of the targeted Win32 display using the appropriate platform APIs.<br/>Such changes **may** alter the surface capabilities reported for the created<br/>surface.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkSurfaceFullScreenExclusiveWin32InfoEXT-hmonitor-02673  \n  [`Self::hmonitor`] **must** be a valid `HMONITOR`\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFullScreenExclusiveWin32InfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkSurfaceFullScreenExclusiveWin32InfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceFullScreenExclusiveWin32InfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub hmonitor: *mut std::ffi::c_void,
}
impl SurfaceFullScreenExclusiveWin32InfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT;
}
impl Default for SurfaceFullScreenExclusiveWin32InfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), hmonitor: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for SurfaceFullScreenExclusiveWin32InfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceFullScreenExclusiveWin32InfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("hmonitor", &self.hmonitor).finish()
    }
}
impl SurfaceFullScreenExclusiveWin32InfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
        SurfaceFullScreenExclusiveWin32InfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFullScreenExclusiveWin32InfoEXT.html)) · Builder of [`SurfaceFullScreenExclusiveWin32InfoEXT`] <br/> VkSurfaceFullScreenExclusiveWin32InfoEXT - Structure specifying additional creation parameters specific to Win32 fullscreen exclusive mode\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_full_screen_exclusive with VK_KHR_win32_surface\ntypedef struct VkSurfaceFullScreenExclusiveWin32InfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    HMONITOR           hmonitor;\n} VkSurfaceFullScreenExclusiveWin32InfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::hmonitor`] is the Win32 `HMONITOR` handle identifying the display\n  to create the surface with.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>If [`Self::hmonitor`] is invalidated (e.g. the monitor is unplugged) during the<br/>lifetime of a swapchain created with this structure, operations on that<br/>swapchain will return [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`].|\n|---|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\n|   |Note<br/><br/>It is the responsibility of the application to change the display settings<br/>of the targeted Win32 display using the appropriate platform APIs.<br/>Such changes **may** alter the surface capabilities reported for the created<br/>surface.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkSurfaceFullScreenExclusiveWin32InfoEXT-hmonitor-02673  \n  [`Self::hmonitor`] **must** be a valid `HMONITOR`\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFullScreenExclusiveWin32InfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a>(SurfaceFullScreenExclusiveWin32InfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
        SurfaceFullScreenExclusiveWin32InfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn hmonitor(mut self, hmonitor: *mut std::ffi::c_void) -> Self {
        self.0.hmonitor = hmonitor;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceFullScreenExclusiveWin32InfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
    fn default() -> SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
    type Target = SurfaceFullScreenExclusiveWin32InfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceFullScreenExclusiveWin32InfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilitiesFullScreenExclusiveEXT.html)) · Structure <br/> VkSurfaceCapabilitiesFullScreenExclusiveEXT - Structure describing full screen exclusive capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\ntypedef struct VkSurfaceCapabilitiesFullScreenExclusiveEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fullScreenExclusiveSupported;\n} VkSurfaceCapabilitiesFullScreenExclusiveEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `fullScreenExclusiveControlSupported` is a boolean describing\n  whether the surface is able to make use of exclusive full-screen access.\n[](#_description)Description\n----------\n\nThis structure **can** be included in the [`Self::p_next`] chain of[`crate::vk::SurfaceCapabilities2KHR`] to determine support for exclusive\nfull-screen access.\nIf [`Self::full_screen_exclusive_supported`] is [`crate::vk::FALSE`], it indicates that\nexclusive full-screen access is not obtainable for this surface.\n\nApplications **must** not attempt to create swapchains with[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`] set if[`Self::full_screen_exclusive_supported`] is [`crate::vk::FALSE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilitiesFullScreenExclusiveEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkSurfaceCapabilitiesFullScreenExclusiveEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceCapabilitiesFullScreenExclusiveEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub full_screen_exclusive_supported: crate::vk1_0::Bool32,
}
impl SurfaceCapabilitiesFullScreenExclusiveEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT;
}
impl Default for SurfaceCapabilitiesFullScreenExclusiveEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), full_screen_exclusive_supported: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceCapabilitiesFullScreenExclusiveEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceCapabilitiesFullScreenExclusiveEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("full_screen_exclusive_supported", &(self.full_screen_exclusive_supported != 0)).finish()
    }
}
impl SurfaceCapabilitiesFullScreenExclusiveEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
        SurfaceCapabilitiesFullScreenExclusiveEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilitiesFullScreenExclusiveEXT.html)) · Builder of [`SurfaceCapabilitiesFullScreenExclusiveEXT`] <br/> VkSurfaceCapabilitiesFullScreenExclusiveEXT - Structure describing full screen exclusive capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\ntypedef struct VkSurfaceCapabilitiesFullScreenExclusiveEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fullScreenExclusiveSupported;\n} VkSurfaceCapabilitiesFullScreenExclusiveEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `fullScreenExclusiveControlSupported` is a boolean describing\n  whether the surface is able to make use of exclusive full-screen access.\n[](#_description)Description\n----------\n\nThis structure **can** be included in the [`Self::p_next`] chain of[`crate::vk::SurfaceCapabilities2KHR`] to determine support for exclusive\nfull-screen access.\nIf [`Self::full_screen_exclusive_supported`] is [`crate::vk::FALSE`], it indicates that\nexclusive full-screen access is not obtainable for this surface.\n\nApplications **must** not attempt to create swapchains with[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`] set if[`Self::full_screen_exclusive_supported`] is [`crate::vk::FALSE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilitiesFullScreenExclusiveEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a>(SurfaceCapabilitiesFullScreenExclusiveEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
        SurfaceCapabilitiesFullScreenExclusiveEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn full_screen_exclusive_supported(mut self, full_screen_exclusive_supported: bool) -> Self {
        self.0.full_screen_exclusive_supported = full_screen_exclusive_supported as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceCapabilitiesFullScreenExclusiveEXT {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
    fn default() -> SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
    type Target = SurfaceCapabilitiesFullScreenExclusiveEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceCapabilitiesFullScreenExclusiveEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_full_screen_exclusive`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfacePresentModes2EXT.html)) · Function <br/> vkGetPhysicalDeviceSurfacePresentModes2EXT - Query supported presentation modes\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to query the supported presentation modes for a surface\ncombined with select other fixed swapchain creation parameters, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkGetPhysicalDeviceSurfacePresentModes2EXT(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    uint32_t*                                   pPresentModeCount,\n    VkPresentModeKHR*                           pPresentModes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_present_mode_count`] is a pointer to an integer related to the number\n  of presentation modes available or queried, as described below.\n\n* [`Self::p_present_modes`] is either `NULL` or a pointer to an array of[`crate::vk::PresentModeKHR`] values, indicating the supported presentation\n  modes.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::get_physical_device_surface_present_modes2_ext`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_present_modes_khr`], with the ability to specify\nextended inputs via chained input structures.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pPresentModeCount-parameter  \n  [`Self::p_present_mode_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSurfacePresentModes2EXT-pPresentModes-parameter  \n   If the value referenced by [`Self::p_present_mode_count`] is not `0`, and [`Self::p_present_modes`] is not `NULL`, [`Self::p_present_modes`] **must** be a valid pointer to an array of [`Self::p_present_mode_count`] [`crate::vk::PresentModeKHR`] values\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::PresentModeKHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceSurfacePresentModes2EXT")]
    pub unsafe fn get_physical_device_surface_present_modes2_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, surface_info: &crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, present_mode_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_surface::PresentModeKHR>> {
        let _function = self.get_physical_device_surface_present_modes2_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut present_mode_count = match present_mode_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, surface_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut present_modes = crate::SmallVec::from_elem(Default::default(), present_mode_count as _);
        let _return = _function(physical_device as _, surface_info as _, &mut present_mode_count, present_modes.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, present_modes)
    }
}
#[doc = "Provided by [`crate::extensions::ext_full_screen_exclusive`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceGroupSurfacePresentModes2EXT.html)) · Function <br/> vkGetDeviceGroupSurfacePresentModes2EXT - Query device group present capabilities for a surface\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to query the supported device group presentation modes for a\nsurface combined with select other fixed swapchain creation parameters,\ncall:\n\n```\n// Provided by VK_EXT_full_screen_exclusive with VK_KHR_device_group, VK_EXT_full_screen_exclusive with VK_VERSION_1_1\nVkResult vkGetDeviceGroupSurfacePresentModes2EXT(\n    VkDevice                                    device,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    VkDeviceGroupPresentModeFlagsKHR*           pModes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device.\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_modes`] is a pointer to a [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`] in\n  which the supported device group present modes for the surface are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::get_device_group_surface_present_modes2_ext`] behaves similarly to[`crate::vk::DeviceLoader::get_device_group_surface_present_modes_khr`], with the ability to specify\nextended inputs via chained input structures.\n\nValid Usage\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pSurfaceInfo-06213  \n  `pSurfaceInfo->surface` **must** be supported by all physical devices\n  associated with [`Self::device`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an equivalent\n  platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetDeviceGroupSurfacePresentModes2EXT-pModes-parameter  \n  [`Self::p_modes`] **must** be a valid pointer to a [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DeviceGroupPresentModeFlagBitsKHR`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`]\n"]
    #[doc(alias = "vkGetDeviceGroupSurfacePresentModes2EXT")]
    pub unsafe fn get_device_group_surface_present_modes2_ext(&self, surface_info: &crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, modes: &mut crate::extensions::khr_swapchain::DeviceGroupPresentModeFlagsKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.get_device_group_surface_present_modes2_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, surface_info as _, modes as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquireFullScreenExclusiveModeEXT.html)) · Function <br/> vkAcquireFullScreenExclusiveModeEXT - Acquire full-screen exclusive mode for a swapchain\n[](#_c_specification)C Specification\n----------\n\nTo acquire exclusive full-screen access for a swapchain, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkAcquireFullScreenExclusiveModeEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to acquire exclusive full-screen access\n  for.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02674  \n  [`Self::swapchain`] **must** not be in the retired state\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02675  \n  [`Self::swapchain`] **must** be a swapchain created with a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, with`fullScreenExclusive` set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`]\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-02676  \n  [`Self::swapchain`] **must** not currently have exclusive full-screen access\n\nA return value of [`crate::vk::Result::SUCCESS`] indicates that the [`Self::swapchain`]successfully acquired exclusive full-screen access.\nThe swapchain will retain this exclusivity until either the application\nreleases exclusive full-screen access with[`crate::vk::DeviceLoader::release_full_screen_exclusive_mode_ext`], destroys the swapchain, or if any\nof the swapchain commands return[`crate::vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT`] indicating that the mode\nwas lost because of platform-specific changes.\n\nIf the swapchain was unable to acquire exclusive full-screen access to the\ndisplay then [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`] is returned.\nAn application **can** attempt to acquire exclusive full-screen access again\nfor the same swapchain even if this command fails, or if[`crate::vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT`] has been returned by a\nswapchain command.\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkAcquireFullScreenExclusiveModeEXT-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkAcquireFullScreenExclusiveModeEXT")]
    pub unsafe fn acquire_full_screen_exclusive_mode_ext(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.acquire_full_screen_exclusive_mode_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, swapchain as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseFullScreenExclusiveModeEXT.html)) · Function <br/> vkReleaseFullScreenExclusiveModeEXT - Release full-screen exclusive mode from a swapchain\n[](#_c_specification)C Specification\n----------\n\nTo release exclusive full-screen access from a swapchain, call:\n\n```\n// Provided by VK_EXT_full_screen_exclusive\nVkResult vkReleaseFullScreenExclusiveModeEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to release exclusive full-screen access\n  from.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Applications will not be able to present to [`Self::swapchain`] after this call<br/>until exclusive full-screen access is reacquired.<br/>This is usually useful to handle when an application is minimised or<br/>otherwise intends to stop presenting for a time.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-vkReleaseFullScreenExclusiveModeEXT-swapchain-02677  \n  [`Self::swapchain`] **must** not be in the retired state\n\n* []() VUID-vkReleaseFullScreenExclusiveModeEXT-swapchain-02678  \n  [`Self::swapchain`] **must** be a swapchain created with a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure, with`fullScreenExclusive` set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkReleaseFullScreenExclusiveModeEXT")]
    pub unsafe fn release_full_screen_exclusive_mode_ext(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.release_full_screen_exclusive_mode_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, swapchain as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
