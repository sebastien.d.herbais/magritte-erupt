#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PIPELINE_EXECUTABLE_PROPERTIES_SPEC_VERSION")]
pub const KHR_PIPELINE_EXECUTABLE_PROPERTIES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PIPELINE_EXECUTABLE_PROPERTIES_EXTENSION_NAME")]
pub const KHR_PIPELINE_EXECUTABLE_PROPERTIES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_pipeline_executable_properties");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PIPELINE_EXECUTABLE_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPipelineExecutablePropertiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PIPELINE_EXECUTABLE_STATISTICS_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPipelineExecutableStatisticsKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATIONS_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPipelineExecutableInternalRepresentationsKHR");
#[doc = "Provided by [`crate::extensions::khr_pipeline_executable_properties`]"]
impl crate::vk1_0::PipelineCreateFlagBits {
    pub const CAPTURE_STATISTICS_KHR: Self = Self(64);
    pub const CAPTURE_INTERNAL_REPRESENTATIONS_KHR: Self = Self(128);
}
#[doc = "Provided by [`crate::extensions::khr_pipeline_executable_properties`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR: Self = Self(1000269000);
    pub const PIPELINE_INFO_KHR: Self = Self(1000269001);
    pub const PIPELINE_EXECUTABLE_PROPERTIES_KHR: Self = Self(1000269002);
    pub const PIPELINE_EXECUTABLE_INFO_KHR: Self = Self(1000269003);
    pub const PIPELINE_EXECUTABLE_STATISTIC_KHR: Self = Self(1000269004);
    pub const PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR: Self = Self(1000269005);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableStatisticFormatKHR.html)) · Enum <br/> VkPipelineExecutableStatisticFormatKHR - Enum describing a pipeline executable statistic\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableStatisticFormatKHR`] enum is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef enum VkPipelineExecutableStatisticFormatKHR {\n    VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR = 0,\n    VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR = 1,\n    VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR = 2,\n    VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR = 3,\n} VkPipelineExecutableStatisticFormatKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::BOOL32_KHR`] specifies that\n  the statistic is returned as a 32-bit boolean value which **must** be\n  either [`crate::vk::TRUE`] or [`crate::vk::FALSE`] and **should** be read from the`b32` field of [`crate::vk::PipelineExecutableStatisticValueKHR`].\n\n* [`Self::INT64_KHR`] specifies that\n  the statistic is returned as a signed 64-bit integer and **should** be read\n  from the `i64` field of [`crate::vk::PipelineExecutableStatisticValueKHR`].\n\n* [`Self::UINT64_KHR`] specifies that\n  the statistic is returned as an unsigned 64-bit integer and **should** be\n  read from the `u64` field of[`crate::vk::PipelineExecutableStatisticValueKHR`].\n\n* [`Self::FLOAT64_KHR`] specifies that\n  the statistic is returned as a 64-bit floating-point value and **should**be read from the `f64` field of[`crate::vk::PipelineExecutableStatisticValueKHR`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineExecutableStatisticKHR`]\n"]
#[doc(alias = "VkPipelineExecutableStatisticFormatKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineExecutableStatisticFormatKHR(pub i32);
impl std::fmt::Debug for PipelineExecutableStatisticFormatKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::BOOL32_KHR => "BOOL32_KHR",
            &Self::INT64_KHR => "INT64_KHR",
            &Self::UINT64_KHR => "UINT64_KHR",
            &Self::FLOAT64_KHR => "FLOAT64_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_pipeline_executable_properties`]"]
impl crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticFormatKHR {
    pub const BOOL32_KHR: Self = Self(0);
    pub const INT64_KHR: Self = Self(1);
    pub const UINT64_KHR: Self = Self(2);
    pub const FLOAT64_KHR: Self = Self(3);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutablePropertiesKHR.html)) · Function <br/> vkGetPipelineExecutablePropertiesKHR - Get the executables associated with a pipeline\n[](#_c_specification)C Specification\n----------\n\nWhen a pipeline is created, its state and shaders are compiled into zero or\nmore device-specific executables, which are used when executing commands\nagainst that pipeline.\nTo query the properties of these pipeline executables, call:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutablePropertiesKHR(\n    VkDevice                                    device,\n    const VkPipelineInfoKHR*                    pPipelineInfo,\n    uint32_t*                                   pExecutableCount,\n    VkPipelineExecutablePropertiesKHR*          pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_pipeline_info`] describes the pipeline being queried.\n\n* [`Self::p_executable_count`] is a pointer to an integer related to the number\n  of pipeline executables available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::PipelineExecutablePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of pipeline executables\nassociated with the pipeline is returned in [`Self::p_executable_count`].\nOtherwise, [`Self::p_executable_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf [`Self::p_executable_count`] is less than the number of pipeline executables\nassociated with the pipeline, at most [`Self::p_executable_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available properties were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pipelineExecutableInfo-03270  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pipeline-03271  \n  `pipeline` member of [`Self::p_pipeline_info`] **must** have been created\n  with [`Self::device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pPipelineInfo-parameter  \n  [`Self::p_pipeline_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pExecutableCount-parameter  \n  [`Self::p_executable_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_executable_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_executable_count`] [`crate::vk::PipelineExecutablePropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutablePropertiesKHR`], [`crate::vk::PipelineInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPipelineExecutablePropertiesKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_pipeline_info: *const crate::extensions::khr_pipeline_executable_properties::PipelineInfoKHR, p_executable_count: *mut u32, p_properties: *mut crate::extensions::khr_pipeline_executable_properties::PipelineExecutablePropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutableStatisticsKHR.html)) · Function <br/> vkGetPipelineExecutableStatisticsKHR - Get compile time statistics associated with a pipeline executable\n[](#_c_specification)C Specification\n----------\n\nEach pipeline executable **may** have a set of statistics associated with it\nthat are generated by the pipeline compilation process.\nThese statistics **may** include things such as instruction counts, amount of\nspilling (if any), maximum number of simultaneous threads, or anything else\nwhich **may** aid developers in evaluating the expected performance of a\nshader.\nTo query the compile-time statistics associated with a pipeline executable,\ncall:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutableStatisticsKHR(\n    VkDevice                                    device,\n    const VkPipelineExecutableInfoKHR*          pExecutableInfo,\n    uint32_t*                                   pStatisticCount,\n    VkPipelineExecutableStatisticKHR*           pStatistics);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_executable_info`] describes the pipeline executable being queried.\n\n* [`Self::p_statistic_count`] is a pointer to an integer related to the number\n  of statistics available or queried, as described below.\n\n* [`Self::p_statistics`] is either `NULL` or a pointer to an array of[`crate::vk::PipelineExecutableStatisticKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_statistics`] is `NULL`, then the number of statistics associated\nwith the pipeline executable is returned in [`Self::p_statistic_count`].\nOtherwise, [`Self::p_statistic_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_statistics`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_statistics`].\nIf [`Self::p_statistic_count`] is less than the number of statistics associated\nwith the pipeline executable, at most [`Self::p_statistic_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available statistics were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipelineExecutableInfo-03272  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipeline-03273  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`Self::device`]\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipeline-03274  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`crate::vk::PipelineCreateFlagBits::CAPTURE_STATISTICS_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pExecutableInfo-parameter  \n  [`Self::p_executable_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineExecutableInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pStatisticCount-parameter  \n  [`Self::p_statistic_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pStatistics-parameter  \n   If the value referenced by [`Self::p_statistic_count`] is not `0`, and [`Self::p_statistics`] is not `NULL`, [`Self::p_statistics`] **must** be a valid pointer to an array of [`Self::p_statistic_count`] [`crate::vk::PipelineExecutableStatisticKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutableInfoKHR`], [`crate::vk::PipelineExecutableStatisticKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPipelineExecutableStatisticsKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_executable_info: *const crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInfoKHR, p_statistic_count: *mut u32, p_statistics: *mut crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutableInternalRepresentationsKHR.html)) · Function <br/> vkGetPipelineExecutableInternalRepresentationsKHR - Get internal representations of the pipeline executable\n[](#_c_specification)C Specification\n----------\n\nEach pipeline executable **may** have one or more text or binary internal\nrepresentations associated with it which are generated as part of the\ncompile process.\nThese **may** include the final shader assembly, a binary form of the compiled\nshader, or the shader compiler’s internal representation at any number of\nintermediate compile steps.\nTo query the internal representations associated with a pipeline executable,\ncall:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutableInternalRepresentationsKHR(\n    VkDevice                                    device,\n    const VkPipelineExecutableInfoKHR*          pExecutableInfo,\n    uint32_t*                                   pInternalRepresentationCount,\n    VkPipelineExecutableInternalRepresentationKHR* pInternalRepresentations);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_executable_info`] describes the pipeline executable being queried.\n\n* [`Self::p_internal_representation_count`] is a pointer to an integer related to\n  the number of internal representations available or queried, as\n  described below.\n\n* [`Self::p_internal_representations`] is either `NULL` or a pointer to an array\n  of [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_internal_representations`] is `NULL`, then the number of internal\nrepresentations associated with the pipeline executable is returned in[`Self::p_internal_representation_count`].\nOtherwise, [`Self::p_internal_representation_count`] **must** point to a variable set\nby the user to the number of elements in the [`Self::p_internal_representations`]array, and on return the variable is overwritten with the number of\nstructures actually written to [`Self::p_internal_representations`].\nIf [`Self::p_internal_representation_count`] is less than the number of internal\nrepresentations associated with the pipeline executable, at most[`Self::p_internal_representation_count`] structures will be written, and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the available representations were returned.\n\nWhile the details of the internal representations remain\nimplementation-dependent, the implementation **should** order the internal\nrepresentations in the order in which they occur in the compiled pipeline\nwith the final shader assembly (if any) last.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipelineExecutableInfo-03276  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipeline-03277  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`Self::device`]\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipeline-03278  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`crate::vk::PipelineCreateFlagBits::CAPTURE_INTERNAL_REPRESENTATIONS_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pExecutableInfo-parameter  \n  [`Self::p_executable_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineExecutableInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pInternalRepresentationCount-parameter  \n  [`Self::p_internal_representation_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pInternalRepresentations-parameter  \n   If the value referenced by [`Self::p_internal_representation_count`] is not `0`, and [`Self::p_internal_representations`] is not `NULL`, [`Self::p_internal_representations`] **must** be a valid pointer to an array of [`Self::p_internal_representation_count`] [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutableInfoKHR`], [`crate::vk::PipelineExecutableInternalRepresentationKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPipelineExecutableInternalRepresentationsKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_executable_info: *const crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInfoKHR, p_internal_representation_count: *mut u32, p_internal_representations: *mut crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInternalRepresentationKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePipelineExecutablePropertiesFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePipelineExecutablePropertiesFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR.html)) · Structure <br/> VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR - Structure describing whether pipeline executable properties are available\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           pipelineExecutableInfo;\n} VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::pipeline_executable_info`]indicates that the implementation supports reporting properties and\n  statistics about the pipeline executables associated with a compiled\n  pipeline.\n\nIf the [`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub pipeline_executable_info: crate::vk1_0::Bool32,
}
impl PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR;
}
impl Default for PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), pipeline_executable_info: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePipelineExecutablePropertiesFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("pipeline_executable_info", &(self.pipeline_executable_info != 0)).finish()
    }
}
impl PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
        PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR.html)) · Builder of [`PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] <br/> VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR - Structure describing whether pipeline executable properties are available\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           pipelineExecutableInfo;\n} VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::pipeline_executable_info`]indicates that the implementation supports reporting properties and\n  statistics about the pipeline executables associated with a compiled\n  pipeline.\n\nIf the [`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePipelineExecutablePropertiesFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a>(PhysicalDevicePipelineExecutablePropertiesFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
        PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline_executable_info(mut self, pipeline_executable_info: bool) -> Self {
        self.0.pipeline_executable_info = pipeline_executable_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
    type Target = PhysicalDevicePipelineExecutablePropertiesFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePipelineExecutablePropertiesFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineInfoKHR.html)) · Structure <br/> VkPipelineInfoKHR - Structure describing a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkPipeline         pipeline;\n} VkPipelineInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline`] is a [`crate::vk::Pipeline`] handle.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_INFO_KHR`]\n\n* []() VUID-VkPipelineInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPipelineInfoKHR-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Pipeline`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n"]
#[doc(alias = "VkPipelineInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub pipeline: crate::vk1_0::Pipeline,
}
impl PipelineInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_INFO_KHR;
}
impl Default for PipelineInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), pipeline: Default::default() }
    }
}
impl std::fmt::Debug for PipelineInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("pipeline", &self.pipeline).finish()
    }
}
impl PipelineInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineInfoKHRBuilder<'a> {
        PipelineInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineInfoKHR.html)) · Builder of [`PipelineInfoKHR`] <br/> VkPipelineInfoKHR - Structure describing a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkPipeline         pipeline;\n} VkPipelineInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline`] is a [`crate::vk::Pipeline`] handle.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_INFO_KHR`]\n\n* []() VUID-VkPipelineInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPipelineInfoKHR-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Pipeline`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n"]
#[repr(transparent)]
pub struct PipelineInfoKHRBuilder<'a>(PipelineInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineInfoKHRBuilder<'a> {
        PipelineInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline(mut self, pipeline: crate::vk1_0::Pipeline) -> Self {
        self.0.pipeline = pipeline as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for PipelineInfoKHRBuilder<'a> {
    fn default() -> PipelineInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineInfoKHRBuilder<'a> {
    type Target = PipelineInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutablePropertiesKHR.html)) · Structure <br/> VkPipelineExecutablePropertiesKHR - Structure describing a pipeline executable\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutablePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutablePropertiesKHR {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkShaderStageFlags    stages;\n    char                  name[VK_MAX_DESCRIPTION_SIZE];\n    char                  description[VK_MAX_DESCRIPTION_SIZE];\n    uint32_t              subgroupSize;\n} VkPipelineExecutablePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stages`] is a bitmask of zero or more [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html)indicating which shader stages (if any) were principally used as inputs\n  to compile this pipeline executable.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this pipeline executable.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this pipeline executable.\n\n* [`Self::subgroup_size`] is the subgroup size with which this pipeline\n  executable is dispatched.\n[](#_description)Description\n----------\n\nNot all implementations have a 1:1 mapping between shader stages and\npipeline executables and some implementations **may** reduce a given shader\nstage to fixed function hardware programming such that no pipeline\nexecutable is available.\nNo guarantees are provided about the mapping between shader stages and\npipeline executables and [`Self::stages`] **should** be considered a best effort\nhint.\nBecause the application **cannot** rely on the [`Self::stages`] field to provide an\nexact description, [`Self::name`] and [`Self::description`] provide a human readable\nname and description which more accurately describes the given pipeline\nexecutable.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutablePropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_PROPERTIES_KHR`]\n\n* []() VUID-VkPipelineExecutablePropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderStageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n"]
#[doc(alias = "VkPipelineExecutablePropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineExecutablePropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub stages: crate::vk1_0::ShaderStageFlags,
    pub name: [std::os::raw::c_char; 256],
    pub description: [std::os::raw::c_char; 256],
    pub subgroup_size: u32,
}
impl PipelineExecutablePropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_EXECUTABLE_PROPERTIES_KHR;
}
impl Default for PipelineExecutablePropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), stages: Default::default(), name: unsafe { std::mem::zeroed() }, description: unsafe { std::mem::zeroed() }, subgroup_size: Default::default() }
    }
}
impl std::fmt::Debug for PipelineExecutablePropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineExecutablePropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("stages", &self.stages).field("name", unsafe { &std::ffi::CStr::from_ptr(self.name.as_ptr()) }).field("description", unsafe { &std::ffi::CStr::from_ptr(self.description.as_ptr()) }).field("subgroup_size", &self.subgroup_size).finish()
    }
}
impl PipelineExecutablePropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineExecutablePropertiesKHRBuilder<'a> {
        PipelineExecutablePropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutablePropertiesKHR.html)) · Builder of [`PipelineExecutablePropertiesKHR`] <br/> VkPipelineExecutablePropertiesKHR - Structure describing a pipeline executable\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutablePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutablePropertiesKHR {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkShaderStageFlags    stages;\n    char                  name[VK_MAX_DESCRIPTION_SIZE];\n    char                  description[VK_MAX_DESCRIPTION_SIZE];\n    uint32_t              subgroupSize;\n} VkPipelineExecutablePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stages`] is a bitmask of zero or more [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html)indicating which shader stages (if any) were principally used as inputs\n  to compile this pipeline executable.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this pipeline executable.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this pipeline executable.\n\n* [`Self::subgroup_size`] is the subgroup size with which this pipeline\n  executable is dispatched.\n[](#_description)Description\n----------\n\nNot all implementations have a 1:1 mapping between shader stages and\npipeline executables and some implementations **may** reduce a given shader\nstage to fixed function hardware programming such that no pipeline\nexecutable is available.\nNo guarantees are provided about the mapping between shader stages and\npipeline executables and [`Self::stages`] **should** be considered a best effort\nhint.\nBecause the application **cannot** rely on the [`Self::stages`] field to provide an\nexact description, [`Self::name`] and [`Self::description`] provide a human readable\nname and description which more accurately describes the given pipeline\nexecutable.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutablePropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_PROPERTIES_KHR`]\n\n* []() VUID-VkPipelineExecutablePropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderStageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n"]
#[repr(transparent)]
pub struct PipelineExecutablePropertiesKHRBuilder<'a>(PipelineExecutablePropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineExecutablePropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineExecutablePropertiesKHRBuilder<'a> {
        PipelineExecutablePropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn stages(mut self, stages: crate::vk1_0::ShaderStageFlags) -> Self {
        self.0.stages = stages as _;
        self
    }
    #[inline]
    pub fn name(mut self, name: [std::os::raw::c_char; 256]) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    pub fn description(mut self, description: [std::os::raw::c_char; 256]) -> Self {
        self.0.description = description as _;
        self
    }
    #[inline]
    pub fn subgroup_size(mut self, subgroup_size: u32) -> Self {
        self.0.subgroup_size = subgroup_size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineExecutablePropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PipelineExecutablePropertiesKHRBuilder<'a> {
    fn default() -> PipelineExecutablePropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineExecutablePropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineExecutablePropertiesKHRBuilder<'a> {
    type Target = PipelineExecutablePropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineExecutablePropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableInfoKHR.html)) · Structure <br/> VkPipelineExecutableInfoKHR - Structure describing a pipeline executable to query for associated statistics or internal representations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkPipeline         pipeline;\n    uint32_t           executableIndex;\n} VkPipelineExecutableInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline`] is the pipeline to query.\n\n* [`Self::executable_index`] is the index of the pipeline executable to query\n  in the array of executable properties returned by[`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineExecutableInfoKHR-executableIndex-03275  \n  [`Self::executable_index`] **must** be less than the number of pipeline\n  executables associated with [`Self::pipeline`] as returned in the`pExecutableCount` parameter of[`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_INFO_KHR`]\n\n* []() VUID-VkPipelineExecutableInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPipelineExecutableInfoKHR-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Pipeline`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_internal_representations_khr`], [`crate::vk::DeviceLoader::get_pipeline_executable_statistics_khr`]\n"]
#[doc(alias = "VkPipelineExecutableInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineExecutableInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub pipeline: crate::vk1_0::Pipeline,
    pub executable_index: u32,
}
impl PipelineExecutableInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_EXECUTABLE_INFO_KHR;
}
impl Default for PipelineExecutableInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), pipeline: Default::default(), executable_index: Default::default() }
    }
}
impl std::fmt::Debug for PipelineExecutableInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineExecutableInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("pipeline", &self.pipeline).field("executable_index", &self.executable_index).finish()
    }
}
impl PipelineExecutableInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineExecutableInfoKHRBuilder<'a> {
        PipelineExecutableInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableInfoKHR.html)) · Builder of [`PipelineExecutableInfoKHR`] <br/> VkPipelineExecutableInfoKHR - Structure describing a pipeline executable to query for associated statistics or internal representations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkPipeline         pipeline;\n    uint32_t           executableIndex;\n} VkPipelineExecutableInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline`] is the pipeline to query.\n\n* [`Self::executable_index`] is the index of the pipeline executable to query\n  in the array of executable properties returned by[`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineExecutableInfoKHR-executableIndex-03275  \n  [`Self::executable_index`] **must** be less than the number of pipeline\n  executables associated with [`Self::pipeline`] as returned in the`pExecutableCount` parameter of[`crate::vk::DeviceLoader::get_pipeline_executable_properties_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_INFO_KHR`]\n\n* []() VUID-VkPipelineExecutableInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPipelineExecutableInfoKHR-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Pipeline`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_internal_representations_khr`], [`crate::vk::DeviceLoader::get_pipeline_executable_statistics_khr`]\n"]
#[repr(transparent)]
pub struct PipelineExecutableInfoKHRBuilder<'a>(PipelineExecutableInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineExecutableInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineExecutableInfoKHRBuilder<'a> {
        PipelineExecutableInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline(mut self, pipeline: crate::vk1_0::Pipeline) -> Self {
        self.0.pipeline = pipeline as _;
        self
    }
    #[inline]
    pub fn executable_index(mut self, executable_index: u32) -> Self {
        self.0.executable_index = executable_index as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineExecutableInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for PipelineExecutableInfoKHRBuilder<'a> {
    fn default() -> PipelineExecutableInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineExecutableInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineExecutableInfoKHRBuilder<'a> {
    type Target = PipelineExecutableInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineExecutableInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableStatisticValueKHR.html)) · Structure <br/> VkPipelineExecutableStatisticValueKHR - A union describing a pipeline executable statistic\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableStatisticValueKHR`] union is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef union VkPipelineExecutableStatisticValueKHR {\n    VkBool32    b32;\n    int64_t     i64;\n    uint64_t    u64;\n    double      f64;\n} VkPipelineExecutableStatisticValueKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::b32`] is the 32-bit boolean value if the[`crate::vk::PipelineExecutableStatisticFormatKHR`] is[`crate::vk::PipelineExecutableStatisticFormatKHR::BOOL32_KHR`].\n\n* [`Self::i64`] is the signed 64-bit integer value if the[`crate::vk::PipelineExecutableStatisticFormatKHR`] is[`crate::vk::PipelineExecutableStatisticFormatKHR::INT64_KHR`].\n\n* [`Self::u64`] is the unsigned 64-bit integer value if the[`crate::vk::PipelineExecutableStatisticFormatKHR`] is[`crate::vk::PipelineExecutableStatisticFormatKHR::UINT64_KHR`].\n\n* [`Self::f64`] is the 64-bit floating-point value if the[`crate::vk::PipelineExecutableStatisticFormatKHR`] is[`crate::vk::PipelineExecutableStatisticFormatKHR::FLOAT64_KHR`].\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::PipelineExecutableStatisticKHR`]\n"]
#[doc(alias = "VkPipelineExecutableStatisticValueKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub union PipelineExecutableStatisticValueKHR {
    pub b32: crate::vk1_0::Bool32,
    pub i64: i64,
    pub u64: u64,
    pub f64: std::os::raw::c_double,
}
impl Default for PipelineExecutableStatisticValueKHR {
    fn default() -> Self {
        unsafe { std::mem::zeroed() }
    }
}
impl std::fmt::Debug for PipelineExecutableStatisticValueKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineExecutableStatisticValueKHR").finish()
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableStatisticKHR.html)) · Structure <br/> VkPipelineExecutableStatisticKHR - Structure describing a compile-time pipeline executable statistic\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableStatisticKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableStatisticKHR {\n    VkStructureType                           sType;\n    void*                                     pNext;\n    char                                      name[VK_MAX_DESCRIPTION_SIZE];\n    char                                      description[VK_MAX_DESCRIPTION_SIZE];\n    VkPipelineExecutableStatisticFormatKHR    format;\n    VkPipelineExecutableStatisticValueKHR     value;\n} VkPipelineExecutableStatisticKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this statistic.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this statistic.\n\n* [`Self::format`] is a [`crate::vk::PipelineExecutableStatisticFormatKHR`] value\n  specifying the format of the data found in [`Self::value`].\n\n* [`Self::value`] is the value of this statistic.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableStatisticKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_STATISTIC_KHR`]\n\n* []() VUID-VkPipelineExecutableStatisticKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineExecutableStatisticFormatKHR`], [`crate::vk::PipelineExecutableStatisticValueKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_statistics_khr`]\n"]
#[doc(alias = "VkPipelineExecutableStatisticKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineExecutableStatisticKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub name: [std::os::raw::c_char; 256],
    pub description: [std::os::raw::c_char; 256],
    pub format: crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticFormatKHR,
    pub value: crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticValueKHR,
}
impl PipelineExecutableStatisticKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_EXECUTABLE_STATISTIC_KHR;
}
impl Default for PipelineExecutableStatisticKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), name: unsafe { std::mem::zeroed() }, description: unsafe { std::mem::zeroed() }, format: Default::default(), value: Default::default() }
    }
}
impl std::fmt::Debug for PipelineExecutableStatisticKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineExecutableStatisticKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("name", unsafe { &std::ffi::CStr::from_ptr(self.name.as_ptr()) }).field("description", unsafe { &std::ffi::CStr::from_ptr(self.description.as_ptr()) }).field("format", &self.format).field("value", &self.value).finish()
    }
}
impl PipelineExecutableStatisticKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineExecutableStatisticKHRBuilder<'a> {
        PipelineExecutableStatisticKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableStatisticKHR.html)) · Builder of [`PipelineExecutableStatisticKHR`] <br/> VkPipelineExecutableStatisticKHR - Structure describing a compile-time pipeline executable statistic\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableStatisticKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableStatisticKHR {\n    VkStructureType                           sType;\n    void*                                     pNext;\n    char                                      name[VK_MAX_DESCRIPTION_SIZE];\n    char                                      description[VK_MAX_DESCRIPTION_SIZE];\n    VkPipelineExecutableStatisticFormatKHR    format;\n    VkPipelineExecutableStatisticValueKHR     value;\n} VkPipelineExecutableStatisticKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this statistic.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this statistic.\n\n* [`Self::format`] is a [`crate::vk::PipelineExecutableStatisticFormatKHR`] value\n  specifying the format of the data found in [`Self::value`].\n\n* [`Self::value`] is the value of this statistic.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableStatisticKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_STATISTIC_KHR`]\n\n* []() VUID-VkPipelineExecutableStatisticKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineExecutableStatisticFormatKHR`], [`crate::vk::PipelineExecutableStatisticValueKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_statistics_khr`]\n"]
#[repr(transparent)]
pub struct PipelineExecutableStatisticKHRBuilder<'a>(PipelineExecutableStatisticKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineExecutableStatisticKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineExecutableStatisticKHRBuilder<'a> {
        PipelineExecutableStatisticKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn name(mut self, name: [std::os::raw::c_char; 256]) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    pub fn description(mut self, description: [std::os::raw::c_char; 256]) -> Self {
        self.0.description = description as _;
        self
    }
    #[inline]
    pub fn format(mut self, format: crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticFormatKHR) -> Self {
        self.0.format = format as _;
        self
    }
    #[inline]
    pub fn value(mut self, value: crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticValueKHR) -> Self {
        self.0.value = value as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineExecutableStatisticKHR {
        self.0
    }
}
impl<'a> std::default::Default for PipelineExecutableStatisticKHRBuilder<'a> {
    fn default() -> PipelineExecutableStatisticKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineExecutableStatisticKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineExecutableStatisticKHRBuilder<'a> {
    type Target = PipelineExecutableStatisticKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineExecutableStatisticKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableInternalRepresentationKHR.html)) · Structure <br/> VkPipelineExecutableInternalRepresentationKHR - Structure describing the textual form of a pipeline executable internal representation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableInternalRepresentationKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    char               name[VK_MAX_DESCRIPTION_SIZE];\n    char               description[VK_MAX_DESCRIPTION_SIZE];\n    VkBool32           isText;\n    size_t             dataSize;\n    void*              pData;\n} VkPipelineExecutableInternalRepresentationKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this internal representation.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this internal representation.\n\n* [`Self::is_text`] specifies whether the returned data is text or opaque data.\n  If [`Self::is_text`] is [`crate::vk::TRUE`] then the data returned in [`Self::p_data`]is text and is guaranteed to be a null-terminated UTF-8 string.\n\n* [`Self::data_size`] is an integer related to the size, in bytes, of the\n  internal representation’s data, as described below.\n\n* [`Self::p_data`] is either `NULL` or a pointer to a block of data into which\n  the implementation will write the internal representation.\n[](#_description)Description\n----------\n\nIf [`Self::p_data`] is `NULL`, then the size, in bytes, of the internal\nrepresentation data is returned in [`Self::data_size`].\nOtherwise, [`Self::data_size`] must be the size of the buffer, in bytes, pointed\nto by [`Self::p_data`] and on return [`Self::data_size`] is overwritten with the\nnumber of bytes of data actually written to [`Self::p_data`] including any\ntrailing null character.\nIf [`Self::data_size`] is less than the size, in bytes, of the internal\nrepresentation’s data, at most [`Self::data_size`] bytes of data will be written\nto [`Self::p_data`], and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available representation was\nreturned.\n\nIf [`Self::is_text`] is [`crate::vk::TRUE`] and [`Self::p_data`] is not `NULL` and[`Self::data_size`] is not zero, the last byte written to [`Self::p_data`] will be a\nnull character.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableInternalRepresentationKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR`]\n\n* []() VUID-VkPipelineExecutableInternalRepresentationKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_internal_representations_khr`]\n"]
#[doc(alias = "VkPipelineExecutableInternalRepresentationKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineExecutableInternalRepresentationKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub name: [std::os::raw::c_char; 256],
    pub description: [std::os::raw::c_char; 256],
    pub is_text: crate::vk1_0::Bool32,
    pub data_size: usize,
    pub p_data: *mut std::ffi::c_void,
}
impl PipelineExecutableInternalRepresentationKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR;
}
impl Default for PipelineExecutableInternalRepresentationKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), name: unsafe { std::mem::zeroed() }, description: unsafe { std::mem::zeroed() }, is_text: Default::default(), data_size: Default::default(), p_data: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for PipelineExecutableInternalRepresentationKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineExecutableInternalRepresentationKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("name", unsafe { &std::ffi::CStr::from_ptr(self.name.as_ptr()) }).field("description", unsafe { &std::ffi::CStr::from_ptr(self.description.as_ptr()) }).field("is_text", &(self.is_text != 0)).field("data_size", &self.data_size).field("p_data", &self.p_data).finish()
    }
}
impl PipelineExecutableInternalRepresentationKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineExecutableInternalRepresentationKHRBuilder<'a> {
        PipelineExecutableInternalRepresentationKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineExecutableInternalRepresentationKHR.html)) · Builder of [`PipelineExecutableInternalRepresentationKHR`] <br/> VkPipelineExecutableInternalRepresentationKHR - Structure describing the textual form of a pipeline executable internal representation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\ntypedef struct VkPipelineExecutableInternalRepresentationKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    char               name[VK_MAX_DESCRIPTION_SIZE];\n    char               description[VK_MAX_DESCRIPTION_SIZE];\n    VkBool32           isText;\n    size_t             dataSize;\n    void*              pData;\n} VkPipelineExecutableInternalRepresentationKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a short human\n  readable name for this internal representation.\n\n* [`Self::description`] is an array of [`crate::vk::MAX_DESCRIPTION_SIZE`] `char`containing a null-terminated UTF-8 string which is a human readable\n  description for this internal representation.\n\n* [`Self::is_text`] specifies whether the returned data is text or opaque data.\n  If [`Self::is_text`] is [`crate::vk::TRUE`] then the data returned in [`Self::p_data`]is text and is guaranteed to be a null-terminated UTF-8 string.\n\n* [`Self::data_size`] is an integer related to the size, in bytes, of the\n  internal representation’s data, as described below.\n\n* [`Self::p_data`] is either `NULL` or a pointer to a block of data into which\n  the implementation will write the internal representation.\n[](#_description)Description\n----------\n\nIf [`Self::p_data`] is `NULL`, then the size, in bytes, of the internal\nrepresentation data is returned in [`Self::data_size`].\nOtherwise, [`Self::data_size`] must be the size of the buffer, in bytes, pointed\nto by [`Self::p_data`] and on return [`Self::data_size`] is overwritten with the\nnumber of bytes of data actually written to [`Self::p_data`] including any\ntrailing null character.\nIf [`Self::data_size`] is less than the size, in bytes, of the internal\nrepresentation’s data, at most [`Self::data_size`] bytes of data will be written\nto [`Self::p_data`], and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available representation was\nreturned.\n\nIf [`Self::is_text`] is [`crate::vk::TRUE`] and [`Self::p_data`] is not `NULL` and[`Self::data_size`] is not zero, the last byte written to [`Self::p_data`] will be a\nnull character.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineExecutableInternalRepresentationKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR`]\n\n* []() VUID-VkPipelineExecutableInternalRepresentationKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_pipeline_executable_internal_representations_khr`]\n"]
#[repr(transparent)]
pub struct PipelineExecutableInternalRepresentationKHRBuilder<'a>(PipelineExecutableInternalRepresentationKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineExecutableInternalRepresentationKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineExecutableInternalRepresentationKHRBuilder<'a> {
        PipelineExecutableInternalRepresentationKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn name(mut self, name: [std::os::raw::c_char; 256]) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    pub fn description(mut self, description: [std::os::raw::c_char; 256]) -> Self {
        self.0.description = description as _;
        self
    }
    #[inline]
    pub fn is_text(mut self, is_text: bool) -> Self {
        self.0.is_text = is_text as _;
        self
    }
    #[inline]
    pub fn data_size(mut self, data_size: usize) -> Self {
        self.0.data_size = data_size;
        self
    }
    #[inline]
    pub fn data(mut self, data: *mut std::ffi::c_void) -> Self {
        self.0.p_data = data;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineExecutableInternalRepresentationKHR {
        self.0
    }
}
impl<'a> std::default::Default for PipelineExecutableInternalRepresentationKHRBuilder<'a> {
    fn default() -> PipelineExecutableInternalRepresentationKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineExecutableInternalRepresentationKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineExecutableInternalRepresentationKHRBuilder<'a> {
    type Target = PipelineExecutableInternalRepresentationKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineExecutableInternalRepresentationKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_pipeline_executable_properties`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutablePropertiesKHR.html)) · Function <br/> vkGetPipelineExecutablePropertiesKHR - Get the executables associated with a pipeline\n[](#_c_specification)C Specification\n----------\n\nWhen a pipeline is created, its state and shaders are compiled into zero or\nmore device-specific executables, which are used when executing commands\nagainst that pipeline.\nTo query the properties of these pipeline executables, call:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutablePropertiesKHR(\n    VkDevice                                    device,\n    const VkPipelineInfoKHR*                    pPipelineInfo,\n    uint32_t*                                   pExecutableCount,\n    VkPipelineExecutablePropertiesKHR*          pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_pipeline_info`] describes the pipeline being queried.\n\n* [`Self::p_executable_count`] is a pointer to an integer related to the number\n  of pipeline executables available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::PipelineExecutablePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of pipeline executables\nassociated with the pipeline is returned in [`Self::p_executable_count`].\nOtherwise, [`Self::p_executable_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf [`Self::p_executable_count`] is less than the number of pipeline executables\nassociated with the pipeline, at most [`Self::p_executable_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available properties were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pipelineExecutableInfo-03270  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pipeline-03271  \n  `pipeline` member of [`Self::p_pipeline_info`] **must** have been created\n  with [`Self::device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pPipelineInfo-parameter  \n  [`Self::p_pipeline_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pExecutableCount-parameter  \n  [`Self::p_executable_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutablePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_executable_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_executable_count`] [`crate::vk::PipelineExecutablePropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutablePropertiesKHR`], [`crate::vk::PipelineInfoKHR`]\n"]
    #[doc(alias = "vkGetPipelineExecutablePropertiesKHR")]
    pub unsafe fn get_pipeline_executable_properties_khr(&self, pipeline_info: &crate::extensions::khr_pipeline_executable_properties::PipelineInfoKHR, executable_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_pipeline_executable_properties::PipelineExecutablePropertiesKHR>> {
        let _function = self.get_pipeline_executable_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut executable_count = match executable_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(self.handle, pipeline_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut properties = crate::SmallVec::from_elem(Default::default(), executable_count as _);
        let _return = _function(self.handle, pipeline_info as _, &mut executable_count, properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, properties)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutableStatisticsKHR.html)) · Function <br/> vkGetPipelineExecutableStatisticsKHR - Get compile time statistics associated with a pipeline executable\n[](#_c_specification)C Specification\n----------\n\nEach pipeline executable **may** have a set of statistics associated with it\nthat are generated by the pipeline compilation process.\nThese statistics **may** include things such as instruction counts, amount of\nspilling (if any), maximum number of simultaneous threads, or anything else\nwhich **may** aid developers in evaluating the expected performance of a\nshader.\nTo query the compile-time statistics associated with a pipeline executable,\ncall:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutableStatisticsKHR(\n    VkDevice                                    device,\n    const VkPipelineExecutableInfoKHR*          pExecutableInfo,\n    uint32_t*                                   pStatisticCount,\n    VkPipelineExecutableStatisticKHR*           pStatistics);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_executable_info`] describes the pipeline executable being queried.\n\n* [`Self::p_statistic_count`] is a pointer to an integer related to the number\n  of statistics available or queried, as described below.\n\n* [`Self::p_statistics`] is either `NULL` or a pointer to an array of[`crate::vk::PipelineExecutableStatisticKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_statistics`] is `NULL`, then the number of statistics associated\nwith the pipeline executable is returned in [`Self::p_statistic_count`].\nOtherwise, [`Self::p_statistic_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_statistics`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_statistics`].\nIf [`Self::p_statistic_count`] is less than the number of statistics associated\nwith the pipeline executable, at most [`Self::p_statistic_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available statistics were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipelineExecutableInfo-03272  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipeline-03273  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`Self::device`]\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pipeline-03274  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`crate::vk::PipelineCreateFlagBits::CAPTURE_STATISTICS_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pExecutableInfo-parameter  \n  [`Self::p_executable_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineExecutableInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pStatisticCount-parameter  \n  [`Self::p_statistic_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutableStatisticsKHR-pStatistics-parameter  \n   If the value referenced by [`Self::p_statistic_count`] is not `0`, and [`Self::p_statistics`] is not `NULL`, [`Self::p_statistics`] **must** be a valid pointer to an array of [`Self::p_statistic_count`] [`crate::vk::PipelineExecutableStatisticKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutableInfoKHR`], [`crate::vk::PipelineExecutableStatisticKHR`]\n"]
    #[doc(alias = "vkGetPipelineExecutableStatisticsKHR")]
    pub unsafe fn get_pipeline_executable_statistics_khr(&self, executable_info: &crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInfoKHR, statistic_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_pipeline_executable_properties::PipelineExecutableStatisticKHR>> {
        let _function = self.get_pipeline_executable_statistics_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut statistic_count = match statistic_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(self.handle, executable_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut statistics = crate::SmallVec::from_elem(Default::default(), statistic_count as _);
        let _return = _function(self.handle, executable_info as _, &mut statistic_count, statistics.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, statistics)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPipelineExecutableInternalRepresentationsKHR.html)) · Function <br/> vkGetPipelineExecutableInternalRepresentationsKHR - Get internal representations of the pipeline executable\n[](#_c_specification)C Specification\n----------\n\nEach pipeline executable **may** have one or more text or binary internal\nrepresentations associated with it which are generated as part of the\ncompile process.\nThese **may** include the final shader assembly, a binary form of the compiled\nshader, or the shader compiler’s internal representation at any number of\nintermediate compile steps.\nTo query the internal representations associated with a pipeline executable,\ncall:\n\n```\n// Provided by VK_KHR_pipeline_executable_properties\nVkResult vkGetPipelineExecutableInternalRepresentationsKHR(\n    VkDevice                                    device,\n    const VkPipelineExecutableInfoKHR*          pExecutableInfo,\n    uint32_t*                                   pInternalRepresentationCount,\n    VkPipelineExecutableInternalRepresentationKHR* pInternalRepresentations);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the pipeline.\n\n* [`Self::p_executable_info`] describes the pipeline executable being queried.\n\n* [`Self::p_internal_representation_count`] is a pointer to an integer related to\n  the number of internal representations available or queried, as\n  described below.\n\n* [`Self::p_internal_representations`] is either `NULL` or a pointer to an array\n  of [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_internal_representations`] is `NULL`, then the number of internal\nrepresentations associated with the pipeline executable is returned in[`Self::p_internal_representation_count`].\nOtherwise, [`Self::p_internal_representation_count`] **must** point to a variable set\nby the user to the number of elements in the [`Self::p_internal_representations`]array, and on return the variable is overwritten with the number of\nstructures actually written to [`Self::p_internal_representations`].\nIf [`Self::p_internal_representation_count`] is less than the number of internal\nrepresentations associated with the pipeline executable, at most[`Self::p_internal_representation_count`] structures will be written, and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the available representations were returned.\n\nWhile the details of the internal representations remain\nimplementation-dependent, the implementation **should** order the internal\nrepresentations in the order in which they occur in the compiled pipeline\nwith the final shader assembly (if any) last.\n\nValid Usage\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipelineExecutableInfo-03276  \n  [`pipelineExecutableInfo`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-pipelineExecutableInfo) **must**be enabled\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipeline-03277  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`Self::device`]\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pipeline-03278  \n  `pipeline` member of [`Self::p_executable_info`] **must** have been created\n  with [`crate::vk::PipelineCreateFlagBits::CAPTURE_INTERNAL_REPRESENTATIONS_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pExecutableInfo-parameter  \n  [`Self::p_executable_info`] **must** be a valid pointer to a valid [`crate::vk::PipelineExecutableInfoKHR`] structure\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pInternalRepresentationCount-parameter  \n  [`Self::p_internal_representation_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPipelineExecutableInternalRepresentationsKHR-pInternalRepresentations-parameter  \n   If the value referenced by [`Self::p_internal_representation_count`] is not `0`, and [`Self::p_internal_representations`] is not `NULL`, [`Self::p_internal_representations`] **must** be a valid pointer to an array of [`Self::p_internal_representation_count`] [`crate::vk::PipelineExecutableInternalRepresentationKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PipelineExecutableInfoKHR`], [`crate::vk::PipelineExecutableInternalRepresentationKHR`]\n"]
    #[doc(alias = "vkGetPipelineExecutableInternalRepresentationsKHR")]
    pub unsafe fn get_pipeline_executable_internal_representations_khr(&self, executable_info: &crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInfoKHR, internal_representation_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_pipeline_executable_properties::PipelineExecutableInternalRepresentationKHR>> {
        let _function = self.get_pipeline_executable_internal_representations_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut internal_representation_count = match internal_representation_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(self.handle, executable_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut internal_representations = crate::SmallVec::from_elem(Default::default(), internal_representation_count as _);
        let _return = _function(self.handle, executable_info as _, &mut internal_representation_count, internal_representations.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, internal_representations)
    }
}
