#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_DISPLAY_SPEC_VERSION")]
pub const KHR_DISPLAY_SPEC_VERSION: u32 = 23;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_DISPLAY_EXTENSION_NAME")]
pub const KHR_DISPLAY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_display");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_DISPLAY_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceDisplayPropertiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_DISPLAY_PLANE_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceDisplayPlanePropertiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_DISPLAY_PLANE_SUPPORTED_DISPLAYS_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetDisplayPlaneSupportedDisplaysKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_DISPLAY_MODE_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetDisplayModePropertiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_DISPLAY_MODE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateDisplayModeKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_DISPLAY_PLANE_CAPABILITIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetDisplayPlaneCapabilitiesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_DISPLAY_PLANE_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateDisplayPlaneSurfaceKHR");
crate::non_dispatchable_handle!(DisplayKHR, DISPLAY_KHR, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayKHR.html)) · Non-dispatchable Handle <br/> VkDisplayKHR - Opaque handle to a display object\n[](#_c_specification)C Specification\n----------\n\nDisplays are represented by [`crate::vk::DisplayKHR`] handles:\n\n```\n// Provided by VK_KHR_display\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkDisplayKHR)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlanePropertiesKHR`], [`crate::vk::DisplayPropertiesKHR`], [`crate::vk::InstanceLoader::acquire_drm_display_ext`], [`crate::vk::DeviceLoader::acquire_winrt_display_nv`], [`crate::vk::InstanceLoader::acquire_xlib_display_ext`], [`crate::vk::InstanceLoader::create_display_mode_khr`], [`crate::vk::DeviceLoader::display_power_control_ext`], [`crate::vk::InstanceLoader::get_display_mode_properties2_khr`], [`crate::vk::InstanceLoader::get_display_mode_properties_khr`], [`crate::vk::InstanceLoader::get_display_plane_supported_displays_khr`], [`crate::vk::InstanceLoader::get_drm_display_ext`], [`crate::vk::InstanceLoader::get_rand_r_output_display_ext`], [`crate::vk::DeviceLoader::get_winrt_display_nv`], [`crate::vk::DeviceLoader::register_display_event_ext`], [`crate::vk::InstanceLoader::release_display_ext`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkDisplayKHR)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkDisplayKHR");
crate::non_dispatchable_handle!(DisplayModeKHR, DISPLAY_MODE_KHR, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeKHR.html)) · Non-dispatchable Handle <br/> VkDisplayModeKHR - Opaque handle to a display mode object\n[](#_c_specification)C Specification\n----------\n\nDisplay modes are represented by [`crate::vk::DisplayModeKHR`] handles:\n\n```\n// Provided by VK_KHR_display\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkDisplayModeKHR)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModePropertiesKHR`], [`crate::vk::DisplayPlaneInfo2KHR`], [`crate::vk::DisplaySurfaceCreateInfoKHR`], [`crate::vk::InstanceLoader::create_display_mode_khr`], [`crate::vk::InstanceLoader::get_display_plane_capabilities_khr`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkDisplayModeKHR)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkDisplayModeKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeCreateFlagsKHR.html)) · Bitmask of [`DisplayModeCreateFlagBitsKHR`] <br/> VkDisplayModeCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_display\ntypedef VkFlags VkDisplayModeCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DisplayModeCreateFlagBitsKHR`] is a bitmask type for setting a mask, but\nis currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeCreateInfoKHR`]\n"] # [doc (alias = "VkDisplayModeCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct DisplayModeCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DisplayModeCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkDisplayModeCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DisplayModeCreateFlagBitsKHR(pub u32);
impl DisplayModeCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DisplayModeCreateFlagsKHR {
        DisplayModeCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DisplayModeCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplaySurfaceCreateFlagsKHR.html)) · Bitmask of [`DisplaySurfaceCreateFlagBitsKHR`] <br/> VkDisplaySurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_display\ntypedef VkFlags VkDisplaySurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DisplaySurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplaySurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkDisplaySurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct DisplaySurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DisplaySurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkDisplaySurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DisplaySurfaceCreateFlagBitsKHR(pub u32);
impl DisplaySurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DisplaySurfaceCreateFlagsKHR {
        DisplaySurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DisplaySurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_display`]"]
impl crate::vk1_0::StructureType {
    pub const DISPLAY_MODE_CREATE_INFO_KHR: Self = Self(1000002000);
    pub const DISPLAY_SURFACE_CREATE_INFO_KHR: Self = Self(1000002001);
}
#[doc = "Provided by [`crate::extensions::khr_display`]"]
impl crate::vk1_0::ObjectType {
    pub const DISPLAY_KHR: Self = Self(1000002000);
    pub const DISPLAY_MODE_KHR: Self = Self(1000002001);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagsKHR.html)) · Bitmask of [`DisplayPlaneAlphaFlagBitsKHR`] <br/> VkDisplayPlaneAlphaFlagsKHR - Bitmask of VkDisplayPlaneAlphaFlagBitsKHR\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_display\ntypedef VkFlags VkDisplayPlaneAlphaFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DisplayPlaneAlphaFlagBitsKHR`] is a bitmask type for setting a mask of\nzero or more [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html), [`crate::vk::DisplayPlaneCapabilitiesKHR`]\n"] # [doc (alias = "VkDisplayPlaneAlphaFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct DisplayPlaneAlphaFlagsKHR : u32 { const OPAQUE_KHR = DisplayPlaneAlphaFlagBitsKHR :: OPAQUE_KHR . 0 ; const GLOBAL_KHR = DisplayPlaneAlphaFlagBitsKHR :: GLOBAL_KHR . 0 ; const PER_PIXEL_KHR = DisplayPlaneAlphaFlagBitsKHR :: PER_PIXEL_KHR . 0 ; const PER_PIXEL_PREMULTIPLIED_KHR = DisplayPlaneAlphaFlagBitsKHR :: PER_PIXEL_PREMULTIPLIED_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html)) · Bits enum of [`DisplayPlaneAlphaFlagsKHR`] <br/> VkDisplayPlaneAlphaFlagBitsKHR - Alpha blending type\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DisplaySurfaceCreateInfoKHR::alpha_mode`],\nspecifying the type of alpha blending to use on a display, are:\n\n```\n// Provided by VK_KHR_display\ntypedef enum VkDisplayPlaneAlphaFlagBitsKHR {\n    VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR = 0x00000001,\n    VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR = 0x00000002,\n    VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR = 0x00000004,\n    VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR = 0x00000008,\n} VkDisplayPlaneAlphaFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::OPAQUE_KHR`] specifies that the source\n  image will be treated as opaque.\n\n* [`Self::GLOBAL_KHR`] specifies that a global\n  alpha value **must** be specified that will be applied to all pixels in the\n  source image.\n\n* [`Self::PER_PIXEL_KHR`] specifies that the alpha\n  value will be determined by the alpha channel of the source image’s\n  pixels.\n  If the source format contains no alpha values, no blending will be\n  applied.\n  The source alpha values are not premultiplied into the source image’s\n  other color channels.\n\n* [`Self::PER_PIXEL_PREMULTIPLIED_KHR`] is\n  equivalent to [`Self::PER_PIXEL_KHR`], except the\n  source alpha values are assumed to be premultiplied into the source\n  image’s other color channels.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlaneAlphaFlagBitsKHR`], [`crate::vk::DisplaySurfaceCreateInfoKHR`]\n"]
#[doc(alias = "VkDisplayPlaneAlphaFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DisplayPlaneAlphaFlagBitsKHR(pub u32);
impl DisplayPlaneAlphaFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DisplayPlaneAlphaFlagsKHR {
        DisplayPlaneAlphaFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DisplayPlaneAlphaFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::OPAQUE_KHR => "OPAQUE_KHR",
            &Self::GLOBAL_KHR => "GLOBAL_KHR",
            &Self::PER_PIXEL_KHR => "PER_PIXEL_KHR",
            &Self::PER_PIXEL_PREMULTIPLIED_KHR => "PER_PIXEL_PREMULTIPLIED_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_display`]"]
impl crate::extensions::khr_display::DisplayPlaneAlphaFlagBitsKHR {
    pub const OPAQUE_KHR: Self = Self(1);
    pub const GLOBAL_KHR: Self = Self(2);
    pub const PER_PIXEL_KHR: Self = Self(4);
    pub const PER_PIXEL_PREMULTIPLIED_KHR: Self = Self(8);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDisplayPropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceDisplayPropertiesKHR - Query information about the available displays\n[](#_c_specification)C Specification\n----------\n\nVarious functions are provided for enumerating the available display devices\npresent on a Vulkan physical device.\nTo query information about the available displays, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetPhysicalDeviceDisplayPropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayPropertiesKHR*                     pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display devices available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayPropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display devices available\nfor [`Self::physical_device`] is returned in [`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\ndevices for [`Self::physical_device`], at most [`Self::p_property_count`] structures\nwill be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available properties were\nreturned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayPropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceDisplayPropertiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_property_count: *mut u32, p_properties: *mut crate::extensions::khr_display::DisplayPropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDisplayPlanePropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceDisplayPlanePropertiesKHR - Query the plane properties\n[](#_c_specification)C Specification\n----------\n\nImages are presented to individual planes on a display.\nDevices **must** support at least one plane on each display.\nPlanes **can** be stacked and blended to composite multiple images on one\ndisplay.\nDevices **may** support only a fixed stacking order and fixed mapping between\nplanes and displays, or they **may** allow arbitrary application specified\nstacking orders and mappings between planes and displays.\nTo query the properties of device display planes, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetPhysicalDeviceDisplayPlanePropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayPlanePropertiesKHR*                pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display planes available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayPlanePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display planes available\nfor [`Self::physical_device`] is returned in [`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\nplanes for [`Self::physical_device`], at most [`Self::p_property_count`] structures\nwill be written.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayPlanePropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlanePropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceDisplayPlanePropertiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_property_count: *mut u32, p_properties: *mut crate::extensions::khr_display::DisplayPlanePropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayPlaneSupportedDisplaysKHR.html)) · Function <br/> vkGetDisplayPlaneSupportedDisplaysKHR - Query the list of displays a plane supports\n[](#_c_specification)C Specification\n----------\n\nTo determine which displays a plane is usable with, call\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayPlaneSupportedDisplaysKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    planeIndex,\n    uint32_t*                                   pDisplayCount,\n    VkDisplayKHR*                               pDisplays);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::plane_index`] is the plane which the application wishes to use, and**must** be in the range [0, physical device plane count - 1].\n\n* [`Self::p_display_count`] is a pointer to an integer related to the number of\n  displays available or queried, as described below.\n\n* [`Self::p_displays`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayKHR`] handles.\n[](#_description)Description\n----------\n\nIf [`Self::p_displays`] is `NULL`, then the number of displays usable with the\nspecified [`Self::plane_index`] for [`Self::physical_device`] is returned in[`Self::p_display_count`].\nOtherwise, [`Self::p_display_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_displays`] array, and on return the\nvariable is overwritten with the number of handles actually written to[`Self::p_displays`].\nIf the value of [`Self::p_display_count`] is less than the number of usable\ndisplay-plane pairs for [`Self::physical_device`], at most [`Self::p_display_count`]handles will be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available pairs were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-planeIndex-01249  \n  [`Self::plane_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-pDisplayCount-parameter  \n  [`Self::p_display_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-pDisplays-parameter  \n   If the value referenced by [`Self::p_display_count`] is not `0`, and [`Self::p_displays`] is not `NULL`, [`Self::p_displays`] **must** be a valid pointer to an array of [`Self::p_display_count`] [`crate::vk::DisplayKHR`] handles\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDisplayPlaneSupportedDisplaysKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, plane_index: u32, p_display_count: *mut u32, p_displays: *mut crate::extensions::khr_display::DisplayKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayModePropertiesKHR.html)) · Function <br/> vkGetDisplayModePropertiesKHR - Query the set of mode properties supported by the display\n[](#_c_specification)C Specification\n----------\n\nEach display has one or more supported modes associated with it by default.\nThese built-in modes are queried by calling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayModePropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayModePropertiesKHR*                 pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with[`Self::display`].\n\n* [`Self::display`] is the display to query.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display modes available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayModePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display modes available\non the specified [`Self::display`] for [`Self::physical_device`] is returned in[`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\nmodes for [`Self::physical_device`], at most [`Self::p_property_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available display modes were\nreturned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayModePropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayModePropertiesKHR-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkGetDisplayModePropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetDisplayModePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayModePropertiesKHR`] structures\n\n* []() VUID-vkGetDisplayModePropertiesKHR-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::DisplayModePropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDisplayModePropertiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR, p_property_count: *mut u32, p_properties: *mut crate::extensions::khr_display::DisplayModePropertiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDisplayModeKHR.html)) · Function <br/> vkCreateDisplayModeKHR - Create a display mode\n[](#_c_specification)C Specification\n----------\n\nAdditional modes **may** also be created by calling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkCreateDisplayModeKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display,\n    const VkDisplayModeCreateInfoKHR*           pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDisplayModeKHR*                           pMode);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with[`Self::display`].\n\n* [`Self::display`] is the display to create an additional mode for.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DisplayModeCreateInfoKHR`]structure describing the new mode to create.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  display mode object when there is no more specific allocator available\n  (see [Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_mode`] is a pointer to a [`crate::vk::DisplayModeKHR`] handle in which the\n  mode created is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDisplayModeKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayModeCreateInfoKHR`] structure\n\n* []() VUID-vkCreateDisplayModeKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDisplayModeKHR-pMode-parameter  \n  [`Self::p_mode`] **must** be a valid pointer to a [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nHost Synchronization\n\n* Host access to [`Self::display`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayModeCreateInfoKHR`], [`crate::vk::DisplayModeKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateDisplayModeKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR, p_create_info: *const crate::extensions::khr_display::DisplayModeCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_mode: *mut crate::extensions::khr_display::DisplayModeKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayPlaneCapabilitiesKHR.html)) · Function <br/> vkGetDisplayPlaneCapabilitiesKHR - Query capabilities of a mode and plane combination\n[](#_c_specification)C Specification\n----------\n\nApplications that wish to present directly to a display **must** select which\nlayer, or “plane” of the display they wish to target, and a mode to use\nwith the display.\nEach display supports at least one plane.\nThe capabilities of a given mode and plane combination are determined by\ncalling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayPlaneCapabilitiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayModeKHR                            mode,\n    uint32_t                                    planeIndex,\n    VkDisplayPlaneCapabilitiesKHR*              pCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with the display\n  specified by [`Self::mode`]\n\n* [`Self::mode`] is the display mode the application intends to program when\n  using the specified plane.\n  Note this parameter also implicitly specifies a display.\n\n* [`Self::plane_index`] is the plane which the application intends to use with\n  the display, and is less than the number of display planes supported by\n  the device.\n\n* [`Self::p_capabilities`] is a pointer to a[`crate::vk::DisplayPlaneCapabilitiesKHR`] structure in which the capabilities\n  are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-mode-parameter  \n  [`Self::mode`] **must** be a valid [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-pCapabilities-parameter  \n  [`Self::p_capabilities`] **must** be a valid pointer to a [`crate::vk::DisplayPlaneCapabilitiesKHR`] structure\n\nHost Synchronization\n\n* Host access to [`Self::mode`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [`crate::vk::DisplayPlaneCapabilitiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetDisplayPlaneCapabilitiesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, mode: crate::extensions::khr_display::DisplayModeKHR, plane_index: u32, p_capabilities: *mut crate::extensions::khr_display::DisplayPlaneCapabilitiesKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDisplayPlaneSurfaceKHR.html)) · Function <br/> vkCreateDisplayPlaneSurfaceKHR - Create a slink:VkSurfaceKHR structure representing a display plane and mode\n[](#_c_specification)C Specification\n----------\n\nA complete display configuration includes a mode, one or more display planes\nand any parameters describing their behavior, and parameters describing some\naspects of the images associated with those planes.\nDisplay surfaces describe the configuration of a single plane within a\ncomplete display configuration.\nTo create a [`crate::vk::SurfaceKHR`] object for a display plane, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkCreateDisplayPlaneSurfaceKHR(\n    VkInstance                                  instance,\n    const VkDisplaySurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance corresponding to the physical device the\n  targeted display is on.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DisplaySurfaceCreateInfoKHR`]structure specifying which mode, plane, and other parameters to use, as\n  described below.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DisplaySurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DisplaySurfaceCreateInfoKHR`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateDisplayPlaneSurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_display::DisplaySurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPropertiesKHR.html)) · Structure <br/> VkDisplayPropertiesKHR - Structure describing an available display device\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPropertiesKHR {\n    VkDisplayKHR                  display;\n    const char*                   displayName;\n    VkExtent2D                    physicalDimensions;\n    VkExtent2D                    physicalResolution;\n    VkSurfaceTransformFlagsKHR    supportedTransforms;\n    VkBool32                      planeReorderPossible;\n    VkBool32                      persistentContent;\n} VkDisplayPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::display`] is a handle that is used to refer to the display described\n  here.\n  This handle will be valid for the lifetime of the Vulkan instance.\n\n* [`Self::display_name`] is `NULL` or a pointer to a null-terminated UTF-8\n  string containing the name of the display.\n  Generally, this will be the name provided by the display’s EDID.\n  If `NULL`, no suitable name is available.\n  If not `NULL`, the string pointed to **must** remain accessible and\n  unmodified as long as [`Self::display`] is valid.\n\n* [`Self::physical_dimensions`] describes the physical width and height of the\n  visible portion of the display, in millimeters.\n\n* [`Self::physical_resolution`] describes the physical, native, or preferred\n  resolution of the display.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>For devices which have no natural value to return here, implementations**should** return the maximum resolution supported.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------|\n\n* [`Self::supported_transforms`] is a bitmask of[VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) describing which transforms are\n  supported by this display.\n\n* [`Self::plane_reorder_possible`] tells whether the planes on this display **can**have their z order changed.\n  If this is [`crate::vk::TRUE`], the application **can** re-arrange the planes on\n  this display in any order relative to each other.\n\n* [`Self::persistent_content`] tells whether the display supports\n  self-refresh/internal buffering.\n  If this is true, the application **can** submit persistent present\n  operations on swapchains created against this display.\n\n|   |Note<br/><br/>Persistent presents **may** have higher latency, and **may** use less power when<br/>the screen content is updated infrequently, or when only a portion of the<br/>screen needs to be updated in most frames.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayProperties2KHR`], [`crate::vk::Extent2D`], [`crate::vk::SurfaceTransformFlagBitsKHR`], [`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`]\n"]
#[doc(alias = "VkDisplayPropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayPropertiesKHR {
    pub display: crate::extensions::khr_display::DisplayKHR,
    pub display_name: *const std::os::raw::c_char,
    pub physical_dimensions: crate::vk1_0::Extent2D,
    pub physical_resolution: crate::vk1_0::Extent2D,
    pub supported_transforms: crate::extensions::khr_surface::SurfaceTransformFlagsKHR,
    pub plane_reorder_possible: crate::vk1_0::Bool32,
    pub persistent_content: crate::vk1_0::Bool32,
}
impl Default for DisplayPropertiesKHR {
    fn default() -> Self {
        Self { display: Default::default(), display_name: std::ptr::null(), physical_dimensions: Default::default(), physical_resolution: Default::default(), supported_transforms: Default::default(), plane_reorder_possible: Default::default(), persistent_content: Default::default() }
    }
}
impl std::fmt::Debug for DisplayPropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayPropertiesKHR").field("display", &self.display).field("display_name", &self.display_name).field("physical_dimensions", &self.physical_dimensions).field("physical_resolution", &self.physical_resolution).field("supported_transforms", &self.supported_transforms).field("plane_reorder_possible", &(self.plane_reorder_possible != 0)).field("persistent_content", &(self.persistent_content != 0)).finish()
    }
}
impl DisplayPropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayPropertiesKHRBuilder<'a> {
        DisplayPropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPropertiesKHR.html)) · Builder of [`DisplayPropertiesKHR`] <br/> VkDisplayPropertiesKHR - Structure describing an available display device\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPropertiesKHR {\n    VkDisplayKHR                  display;\n    const char*                   displayName;\n    VkExtent2D                    physicalDimensions;\n    VkExtent2D                    physicalResolution;\n    VkSurfaceTransformFlagsKHR    supportedTransforms;\n    VkBool32                      planeReorderPossible;\n    VkBool32                      persistentContent;\n} VkDisplayPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::display`] is a handle that is used to refer to the display described\n  here.\n  This handle will be valid for the lifetime of the Vulkan instance.\n\n* [`Self::display_name`] is `NULL` or a pointer to a null-terminated UTF-8\n  string containing the name of the display.\n  Generally, this will be the name provided by the display’s EDID.\n  If `NULL`, no suitable name is available.\n  If not `NULL`, the string pointed to **must** remain accessible and\n  unmodified as long as [`Self::display`] is valid.\n\n* [`Self::physical_dimensions`] describes the physical width and height of the\n  visible portion of the display, in millimeters.\n\n* [`Self::physical_resolution`] describes the physical, native, or preferred\n  resolution of the display.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>For devices which have no natural value to return here, implementations**should** return the maximum resolution supported.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------|\n\n* [`Self::supported_transforms`] is a bitmask of[VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) describing which transforms are\n  supported by this display.\n\n* [`Self::plane_reorder_possible`] tells whether the planes on this display **can**have their z order changed.\n  If this is [`crate::vk::TRUE`], the application **can** re-arrange the planes on\n  this display in any order relative to each other.\n\n* [`Self::persistent_content`] tells whether the display supports\n  self-refresh/internal buffering.\n  If this is true, the application **can** submit persistent present\n  operations on swapchains created against this display.\n\n|   |Note<br/><br/>Persistent presents **may** have higher latency, and **may** use less power when<br/>the screen content is updated infrequently, or when only a portion of the<br/>screen needs to be updated in most frames.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayProperties2KHR`], [`crate::vk::Extent2D`], [`crate::vk::SurfaceTransformFlagBitsKHR`], [`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`]\n"]
#[repr(transparent)]
pub struct DisplayPropertiesKHRBuilder<'a>(DisplayPropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayPropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayPropertiesKHRBuilder<'a> {
        DisplayPropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn display(mut self, display: crate::extensions::khr_display::DisplayKHR) -> Self {
        self.0.display = display as _;
        self
    }
    #[inline]
    pub fn display_name(mut self, display_name: &'a std::ffi::CStr) -> Self {
        self.0.display_name = display_name.as_ptr();
        self
    }
    #[inline]
    pub fn physical_dimensions(mut self, physical_dimensions: crate::vk1_0::Extent2D) -> Self {
        self.0.physical_dimensions = physical_dimensions as _;
        self
    }
    #[inline]
    pub fn physical_resolution(mut self, physical_resolution: crate::vk1_0::Extent2D) -> Self {
        self.0.physical_resolution = physical_resolution as _;
        self
    }
    #[inline]
    pub fn supported_transforms(mut self, supported_transforms: crate::extensions::khr_surface::SurfaceTransformFlagsKHR) -> Self {
        self.0.supported_transforms = supported_transforms as _;
        self
    }
    #[inline]
    pub fn plane_reorder_possible(mut self, plane_reorder_possible: bool) -> Self {
        self.0.plane_reorder_possible = plane_reorder_possible as _;
        self
    }
    #[inline]
    pub fn persistent_content(mut self, persistent_content: bool) -> Self {
        self.0.persistent_content = persistent_content as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayPropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayPropertiesKHRBuilder<'a> {
    fn default() -> DisplayPropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayPropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayPropertiesKHRBuilder<'a> {
    type Target = DisplayPropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayPropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlanePropertiesKHR.html)) · Structure <br/> VkDisplayPlanePropertiesKHR - Structure describing display plane properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPlanePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPlanePropertiesKHR {\n    VkDisplayKHR    currentDisplay;\n    uint32_t        currentStackIndex;\n} VkDisplayPlanePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::current_display`] is the handle of the display the plane is currently\n  associated with.\n  If the plane is not currently attached to any displays, this will be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html).\n\n* [`Self::current_stack_index`] is the current z-order of the plane.\n  This will be between 0 and the value returned by[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`] in`pPropertyCount`.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::DisplayPlaneProperties2KHR`], [`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n"]
#[doc(alias = "VkDisplayPlanePropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayPlanePropertiesKHR {
    pub current_display: crate::extensions::khr_display::DisplayKHR,
    pub current_stack_index: u32,
}
impl Default for DisplayPlanePropertiesKHR {
    fn default() -> Self {
        Self { current_display: Default::default(), current_stack_index: Default::default() }
    }
}
impl std::fmt::Debug for DisplayPlanePropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayPlanePropertiesKHR").field("current_display", &self.current_display).field("current_stack_index", &self.current_stack_index).finish()
    }
}
impl DisplayPlanePropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayPlanePropertiesKHRBuilder<'a> {
        DisplayPlanePropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlanePropertiesKHR.html)) · Builder of [`DisplayPlanePropertiesKHR`] <br/> VkDisplayPlanePropertiesKHR - Structure describing display plane properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPlanePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPlanePropertiesKHR {\n    VkDisplayKHR    currentDisplay;\n    uint32_t        currentStackIndex;\n} VkDisplayPlanePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::current_display`] is the handle of the display the plane is currently\n  associated with.\n  If the plane is not currently attached to any displays, this will be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html).\n\n* [`Self::current_stack_index`] is the current z-order of the plane.\n  This will be between 0 and the value returned by[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`] in`pPropertyCount`.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::DisplayPlaneProperties2KHR`], [`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n"]
#[repr(transparent)]
pub struct DisplayPlanePropertiesKHRBuilder<'a>(DisplayPlanePropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayPlanePropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayPlanePropertiesKHRBuilder<'a> {
        DisplayPlanePropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn current_display(mut self, current_display: crate::extensions::khr_display::DisplayKHR) -> Self {
        self.0.current_display = current_display as _;
        self
    }
    #[inline]
    pub fn current_stack_index(mut self, current_stack_index: u32) -> Self {
        self.0.current_stack_index = current_stack_index as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayPlanePropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayPlanePropertiesKHRBuilder<'a> {
    fn default() -> DisplayPlanePropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayPlanePropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayPlanePropertiesKHRBuilder<'a> {
    type Target = DisplayPlanePropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayPlanePropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeParametersKHR.html)) · Structure <br/> VkDisplayModeParametersKHR - Structure describing display parameters associated with a display mode\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModeParametersKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModeParametersKHR {\n    VkExtent2D    visibleRegion;\n    uint32_t      refreshRate;\n} VkDisplayModeParametersKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::visible_region`] is the 2D extents of the visible region.\n\n* [`Self::refresh_rate`] is a `uint32_t` that is the number of times the\n  display is refreshed each second multiplied by 1000.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>For example, a 60Hz display mode would report a [`Self::refresh_rate`] of 60,000.|\n|---|--------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDisplayModeParametersKHR-width-01990  \n   The `width` member of [`Self::visible_region`] **must** be greater than `0`\n\n* []() VUID-VkDisplayModeParametersKHR-height-01991  \n   The `height` member of [`Self::visible_region`] **must** be greater than `0`\n\n* []() VUID-VkDisplayModeParametersKHR-refreshRate-01992  \n  [`Self::refresh_rate`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeCreateInfoKHR`], [`crate::vk::DisplayModePropertiesKHR`], [`crate::vk::Extent2D`]\n"]
#[doc(alias = "VkDisplayModeParametersKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayModeParametersKHR {
    pub visible_region: crate::vk1_0::Extent2D,
    pub refresh_rate: u32,
}
impl Default for DisplayModeParametersKHR {
    fn default() -> Self {
        Self { visible_region: Default::default(), refresh_rate: Default::default() }
    }
}
impl std::fmt::Debug for DisplayModeParametersKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayModeParametersKHR").field("visible_region", &self.visible_region).field("refresh_rate", &self.refresh_rate).finish()
    }
}
impl DisplayModeParametersKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayModeParametersKHRBuilder<'a> {
        DisplayModeParametersKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeParametersKHR.html)) · Builder of [`DisplayModeParametersKHR`] <br/> VkDisplayModeParametersKHR - Structure describing display parameters associated with a display mode\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModeParametersKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModeParametersKHR {\n    VkExtent2D    visibleRegion;\n    uint32_t      refreshRate;\n} VkDisplayModeParametersKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::visible_region`] is the 2D extents of the visible region.\n\n* [`Self::refresh_rate`] is a `uint32_t` that is the number of times the\n  display is refreshed each second multiplied by 1000.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>For example, a 60Hz display mode would report a [`Self::refresh_rate`] of 60,000.|\n|---|--------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDisplayModeParametersKHR-width-01990  \n   The `width` member of [`Self::visible_region`] **must** be greater than `0`\n\n* []() VUID-VkDisplayModeParametersKHR-height-01991  \n   The `height` member of [`Self::visible_region`] **must** be greater than `0`\n\n* []() VUID-VkDisplayModeParametersKHR-refreshRate-01992  \n  [`Self::refresh_rate`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeCreateInfoKHR`], [`crate::vk::DisplayModePropertiesKHR`], [`crate::vk::Extent2D`]\n"]
#[repr(transparent)]
pub struct DisplayModeParametersKHRBuilder<'a>(DisplayModeParametersKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayModeParametersKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayModeParametersKHRBuilder<'a> {
        DisplayModeParametersKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn visible_region(mut self, visible_region: crate::vk1_0::Extent2D) -> Self {
        self.0.visible_region = visible_region as _;
        self
    }
    #[inline]
    pub fn refresh_rate(mut self, refresh_rate: u32) -> Self {
        self.0.refresh_rate = refresh_rate as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayModeParametersKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayModeParametersKHRBuilder<'a> {
    fn default() -> DisplayModeParametersKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayModeParametersKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayModeParametersKHRBuilder<'a> {
    type Target = DisplayModeParametersKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayModeParametersKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModePropertiesKHR.html)) · Structure <br/> VkDisplayModePropertiesKHR - Structure describing display mode properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModePropertiesKHR {\n    VkDisplayModeKHR              displayMode;\n    VkDisplayModeParametersKHR    parameters;\n} VkDisplayModePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::display_mode`] is a handle to the display mode described in this\n  structure.\n  This handle will be valid for the lifetime of the Vulkan instance.\n\n* [`Self::parameters`] is a [`crate::vk::DisplayModeParametersKHR`] structure\n  describing the display parameters associated with [`Self::display_mode`].\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [`crate::vk::DisplayModeParametersKHR`], [`crate::vk::DisplayModeProperties2KHR`], [`crate::vk::InstanceLoader::get_display_mode_properties_khr`]\n"]
#[doc(alias = "VkDisplayModePropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayModePropertiesKHR {
    pub display_mode: crate::extensions::khr_display::DisplayModeKHR,
    pub parameters: crate::extensions::khr_display::DisplayModeParametersKHR,
}
impl Default for DisplayModePropertiesKHR {
    fn default() -> Self {
        Self { display_mode: Default::default(), parameters: Default::default() }
    }
}
impl std::fmt::Debug for DisplayModePropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayModePropertiesKHR").field("display_mode", &self.display_mode).field("parameters", &self.parameters).finish()
    }
}
impl DisplayModePropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayModePropertiesKHRBuilder<'a> {
        DisplayModePropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModePropertiesKHR.html)) · Builder of [`DisplayModePropertiesKHR`] <br/> VkDisplayModePropertiesKHR - Structure describing display mode properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModePropertiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModePropertiesKHR {\n    VkDisplayModeKHR              displayMode;\n    VkDisplayModeParametersKHR    parameters;\n} VkDisplayModePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::display_mode`] is a handle to the display mode described in this\n  structure.\n  This handle will be valid for the lifetime of the Vulkan instance.\n\n* [`Self::parameters`] is a [`crate::vk::DisplayModeParametersKHR`] structure\n  describing the display parameters associated with [`Self::display_mode`].\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [`crate::vk::DisplayModeParametersKHR`], [`crate::vk::DisplayModeProperties2KHR`], [`crate::vk::InstanceLoader::get_display_mode_properties_khr`]\n"]
#[repr(transparent)]
pub struct DisplayModePropertiesKHRBuilder<'a>(DisplayModePropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayModePropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayModePropertiesKHRBuilder<'a> {
        DisplayModePropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn display_mode(mut self, display_mode: crate::extensions::khr_display::DisplayModeKHR) -> Self {
        self.0.display_mode = display_mode as _;
        self
    }
    #[inline]
    pub fn parameters(mut self, parameters: crate::extensions::khr_display::DisplayModeParametersKHR) -> Self {
        self.0.parameters = parameters as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayModePropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayModePropertiesKHRBuilder<'a> {
    fn default() -> DisplayModePropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayModePropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayModePropertiesKHRBuilder<'a> {
    type Target = DisplayModePropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayModePropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeCreateInfoKHR.html)) · Structure <br/> VkDisplayModeCreateInfoKHR - Structure specifying parameters of a newly created display mode object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModeCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModeCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkDisplayModeCreateFlagsKHR    flags;\n    VkDisplayModeParametersKHR     parameters;\n} VkDisplayModeCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use, and **must** be zero.\n\n* [`Self::parameters`] is a [`crate::vk::DisplayModeParametersKHR`] structure\n  describing the display parameters to use in creating the new mode.\n  If the parameters are not compatible with the specified display, the\n  implementation **must** return [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayModeCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_MODE_CREATE_INFO_KHR`]\n\n* []() VUID-VkDisplayModeCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayModeCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDisplayModeCreateInfoKHR-parameters-parameter  \n  [`Self::parameters`] **must** be a valid [`crate::vk::DisplayModeParametersKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeCreateFlagBitsKHR`], [`crate::vk::DisplayModeParametersKHR`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_display_mode_khr`]\n"]
#[doc(alias = "VkDisplayModeCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayModeCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_display::DisplayModeCreateFlagsKHR,
    pub parameters: crate::extensions::khr_display::DisplayModeParametersKHR,
}
impl DisplayModeCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DISPLAY_MODE_CREATE_INFO_KHR;
}
impl Default for DisplayModeCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), parameters: Default::default() }
    }
}
impl std::fmt::Debug for DisplayModeCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayModeCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("parameters", &self.parameters).finish()
    }
}
impl DisplayModeCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayModeCreateInfoKHRBuilder<'a> {
        DisplayModeCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayModeCreateInfoKHR.html)) · Builder of [`DisplayModeCreateInfoKHR`] <br/> VkDisplayModeCreateInfoKHR - Structure specifying parameters of a newly created display mode object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayModeCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayModeCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkDisplayModeCreateFlagsKHR    flags;\n    VkDisplayModeParametersKHR     parameters;\n} VkDisplayModeCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use, and **must** be zero.\n\n* [`Self::parameters`] is a [`crate::vk::DisplayModeParametersKHR`] structure\n  describing the display parameters to use in creating the new mode.\n  If the parameters are not compatible with the specified display, the\n  implementation **must** return [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayModeCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_MODE_CREATE_INFO_KHR`]\n\n* []() VUID-VkDisplayModeCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayModeCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDisplayModeCreateInfoKHR-parameters-parameter  \n  [`Self::parameters`] **must** be a valid [`crate::vk::DisplayModeParametersKHR`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeCreateFlagBitsKHR`], [`crate::vk::DisplayModeParametersKHR`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_display_mode_khr`]\n"]
#[repr(transparent)]
pub struct DisplayModeCreateInfoKHRBuilder<'a>(DisplayModeCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayModeCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayModeCreateInfoKHRBuilder<'a> {
        DisplayModeCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_display::DisplayModeCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn parameters(mut self, parameters: crate::extensions::khr_display::DisplayModeParametersKHR) -> Self {
        self.0.parameters = parameters as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayModeCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayModeCreateInfoKHRBuilder<'a> {
    fn default() -> DisplayModeCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayModeCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayModeCreateInfoKHRBuilder<'a> {
    type Target = DisplayModeCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayModeCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneCapabilitiesKHR.html)) · Structure <br/> VkDisplayPlaneCapabilitiesKHR - Structure describing capabilities of a mode and plane combination\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPlaneCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPlaneCapabilitiesKHR {\n    VkDisplayPlaneAlphaFlagsKHR    supportedAlpha;\n    VkOffset2D                     minSrcPosition;\n    VkOffset2D                     maxSrcPosition;\n    VkExtent2D                     minSrcExtent;\n    VkExtent2D                     maxSrcExtent;\n    VkOffset2D                     minDstPosition;\n    VkOffset2D                     maxDstPosition;\n    VkExtent2D                     minDstExtent;\n    VkExtent2D                     maxDstExtent;\n} VkDisplayPlaneCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::supported_alpha`] is a bitmask of[VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) describing the supported alpha\n  blending modes.\n\n* [`Self::min_src_position`] is the minimum source rectangle offset supported by\n  this plane using the specified mode.\n\n* [`Self::max_src_position`] is the maximum source rectangle offset supported by\n  this plane using the specified mode.\n  The `x` and `y` components of [`Self::max_src_position`] **must** each be\n  greater than or equal to the `x` and `y` components of[`Self::min_src_position`], respectively.\n\n* [`Self::min_src_extent`] is the minimum source rectangle size supported by\n  this plane using the specified mode.\n\n* [`Self::max_src_extent`] is the maximum source rectangle size supported by\n  this plane using the specified mode.\n\n* [`Self::min_dst_position`], [`Self::max_dst_position`], [`Self::min_dst_extent`],[`Self::max_dst_extent`] all have similar semantics to their corresponding`*Src*` equivalents, but apply to the output region within the mode\n  rather than the input region within the source image.\n  Unlike the `*Src*` offsets, [`Self::min_dst_position`] and[`Self::max_dst_position`] **may** contain negative values.\n[](#_description)Description\n----------\n\nThe minimum and maximum position and extent fields describe the\nimplementation limits, if any, as they apply to the specified display mode\nand plane.\nVendors **may** support displaying a subset of a swapchain’s presentable images\non the specified display plane.\nThis is expressed by returning [`Self::min_src_position`], [`Self::max_src_position`],[`Self::min_src_extent`], and [`Self::max_src_extent`] values that indicate a range of\npossible positions and sizes which **may** be used to specify the region within\nthe presentable images that source pixels will be read from when creating a\nswapchain on the specified display mode and plane.\n\nVendors **may** also support mapping the presentable images’ content to a\nsubset or superset of the visible region in the specified display mode.\nThis is expressed by returning [`Self::min_dst_position`], [`Self::max_dst_position`],[`Self::min_dst_extent`] and [`Self::max_dst_extent`] values that indicate a range of\npossible positions and sizes which **may** be used to describe the region\nwithin the display mode that the source pixels will be mapped to.\n\nOther vendors **may** support only a 1-1 mapping between pixels in the\npresentable images and the display mode.\nThis **may** be indicated by returning (0,0) for [`Self::min_src_position`],[`Self::max_src_position`], [`Self::min_dst_position`], and [`Self::max_dst_position`], and\n(display mode width, display mode height) for [`Self::min_src_extent`],[`Self::max_src_extent`], [`Self::min_dst_extent`], and [`Self::max_dst_extent`].\n\nThe value [`Self::supported_alpha`] **must** contain at least one valid[VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) bit.\n\nThese values indicate the limits of the implementation’s individual fields.\nNot all combinations of values within the offset and extent ranges returned\nin [`crate::vk::DisplayPlaneCapabilitiesKHR`] are guaranteed to be supported.\nPresentation requests specifying unsupported combinations **may** fail.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlaneAlphaFlagBitsKHR`], [`crate::vk::DisplayPlaneCapabilities2KHR`], [`crate::vk::Extent2D`], [`crate::vk::Offset2D`], [`crate::vk::InstanceLoader::get_display_plane_capabilities_khr`]\n"]
#[doc(alias = "VkDisplayPlaneCapabilitiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayPlaneCapabilitiesKHR {
    pub supported_alpha: crate::extensions::khr_display::DisplayPlaneAlphaFlagsKHR,
    pub min_src_position: crate::vk1_0::Offset2D,
    pub max_src_position: crate::vk1_0::Offset2D,
    pub min_src_extent: crate::vk1_0::Extent2D,
    pub max_src_extent: crate::vk1_0::Extent2D,
    pub min_dst_position: crate::vk1_0::Offset2D,
    pub max_dst_position: crate::vk1_0::Offset2D,
    pub min_dst_extent: crate::vk1_0::Extent2D,
    pub max_dst_extent: crate::vk1_0::Extent2D,
}
impl Default for DisplayPlaneCapabilitiesKHR {
    fn default() -> Self {
        Self { supported_alpha: Default::default(), min_src_position: Default::default(), max_src_position: Default::default(), min_src_extent: Default::default(), max_src_extent: Default::default(), min_dst_position: Default::default(), max_dst_position: Default::default(), min_dst_extent: Default::default(), max_dst_extent: Default::default() }
    }
}
impl std::fmt::Debug for DisplayPlaneCapabilitiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayPlaneCapabilitiesKHR").field("supported_alpha", &self.supported_alpha).field("min_src_position", &self.min_src_position).field("max_src_position", &self.max_src_position).field("min_src_extent", &self.min_src_extent).field("max_src_extent", &self.max_src_extent).field("min_dst_position", &self.min_dst_position).field("max_dst_position", &self.max_dst_position).field("min_dst_extent", &self.min_dst_extent).field("max_dst_extent", &self.max_dst_extent).finish()
    }
}
impl DisplayPlaneCapabilitiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayPlaneCapabilitiesKHRBuilder<'a> {
        DisplayPlaneCapabilitiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneCapabilitiesKHR.html)) · Builder of [`DisplayPlaneCapabilitiesKHR`] <br/> VkDisplayPlaneCapabilitiesKHR - Structure describing capabilities of a mode and plane combination\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPlaneCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplayPlaneCapabilitiesKHR {\n    VkDisplayPlaneAlphaFlagsKHR    supportedAlpha;\n    VkOffset2D                     minSrcPosition;\n    VkOffset2D                     maxSrcPosition;\n    VkExtent2D                     minSrcExtent;\n    VkExtent2D                     maxSrcExtent;\n    VkOffset2D                     minDstPosition;\n    VkOffset2D                     maxDstPosition;\n    VkExtent2D                     minDstExtent;\n    VkExtent2D                     maxDstExtent;\n} VkDisplayPlaneCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::supported_alpha`] is a bitmask of[VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) describing the supported alpha\n  blending modes.\n\n* [`Self::min_src_position`] is the minimum source rectangle offset supported by\n  this plane using the specified mode.\n\n* [`Self::max_src_position`] is the maximum source rectangle offset supported by\n  this plane using the specified mode.\n  The `x` and `y` components of [`Self::max_src_position`] **must** each be\n  greater than or equal to the `x` and `y` components of[`Self::min_src_position`], respectively.\n\n* [`Self::min_src_extent`] is the minimum source rectangle size supported by\n  this plane using the specified mode.\n\n* [`Self::max_src_extent`] is the maximum source rectangle size supported by\n  this plane using the specified mode.\n\n* [`Self::min_dst_position`], [`Self::max_dst_position`], [`Self::min_dst_extent`],[`Self::max_dst_extent`] all have similar semantics to their corresponding`*Src*` equivalents, but apply to the output region within the mode\n  rather than the input region within the source image.\n  Unlike the `*Src*` offsets, [`Self::min_dst_position`] and[`Self::max_dst_position`] **may** contain negative values.\n[](#_description)Description\n----------\n\nThe minimum and maximum position and extent fields describe the\nimplementation limits, if any, as they apply to the specified display mode\nand plane.\nVendors **may** support displaying a subset of a swapchain’s presentable images\non the specified display plane.\nThis is expressed by returning [`Self::min_src_position`], [`Self::max_src_position`],[`Self::min_src_extent`], and [`Self::max_src_extent`] values that indicate a range of\npossible positions and sizes which **may** be used to specify the region within\nthe presentable images that source pixels will be read from when creating a\nswapchain on the specified display mode and plane.\n\nVendors **may** also support mapping the presentable images’ content to a\nsubset or superset of the visible region in the specified display mode.\nThis is expressed by returning [`Self::min_dst_position`], [`Self::max_dst_position`],[`Self::min_dst_extent`] and [`Self::max_dst_extent`] values that indicate a range of\npossible positions and sizes which **may** be used to describe the region\nwithin the display mode that the source pixels will be mapped to.\n\nOther vendors **may** support only a 1-1 mapping between pixels in the\npresentable images and the display mode.\nThis **may** be indicated by returning (0,0) for [`Self::min_src_position`],[`Self::max_src_position`], [`Self::min_dst_position`], and [`Self::max_dst_position`], and\n(display mode width, display mode height) for [`Self::min_src_extent`],[`Self::max_src_extent`], [`Self::min_dst_extent`], and [`Self::max_dst_extent`].\n\nThe value [`Self::supported_alpha`] **must** contain at least one valid[VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) bit.\n\nThese values indicate the limits of the implementation’s individual fields.\nNot all combinations of values within the offset and extent ranges returned\nin [`crate::vk::DisplayPlaneCapabilitiesKHR`] are guaranteed to be supported.\nPresentation requests specifying unsupported combinations **may** fail.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlaneAlphaFlagBitsKHR`], [`crate::vk::DisplayPlaneCapabilities2KHR`], [`crate::vk::Extent2D`], [`crate::vk::Offset2D`], [`crate::vk::InstanceLoader::get_display_plane_capabilities_khr`]\n"]
#[repr(transparent)]
pub struct DisplayPlaneCapabilitiesKHRBuilder<'a>(DisplayPlaneCapabilitiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayPlaneCapabilitiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayPlaneCapabilitiesKHRBuilder<'a> {
        DisplayPlaneCapabilitiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn supported_alpha(mut self, supported_alpha: crate::extensions::khr_display::DisplayPlaneAlphaFlagsKHR) -> Self {
        self.0.supported_alpha = supported_alpha as _;
        self
    }
    #[inline]
    pub fn min_src_position(mut self, min_src_position: crate::vk1_0::Offset2D) -> Self {
        self.0.min_src_position = min_src_position as _;
        self
    }
    #[inline]
    pub fn max_src_position(mut self, max_src_position: crate::vk1_0::Offset2D) -> Self {
        self.0.max_src_position = max_src_position as _;
        self
    }
    #[inline]
    pub fn min_src_extent(mut self, min_src_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.min_src_extent = min_src_extent as _;
        self
    }
    #[inline]
    pub fn max_src_extent(mut self, max_src_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.max_src_extent = max_src_extent as _;
        self
    }
    #[inline]
    pub fn min_dst_position(mut self, min_dst_position: crate::vk1_0::Offset2D) -> Self {
        self.0.min_dst_position = min_dst_position as _;
        self
    }
    #[inline]
    pub fn max_dst_position(mut self, max_dst_position: crate::vk1_0::Offset2D) -> Self {
        self.0.max_dst_position = max_dst_position as _;
        self
    }
    #[inline]
    pub fn min_dst_extent(mut self, min_dst_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.min_dst_extent = min_dst_extent as _;
        self
    }
    #[inline]
    pub fn max_dst_extent(mut self, max_dst_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.max_dst_extent = max_dst_extent as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayPlaneCapabilitiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplayPlaneCapabilitiesKHRBuilder<'a> {
    fn default() -> DisplayPlaneCapabilitiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayPlaneCapabilitiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayPlaneCapabilitiesKHRBuilder<'a> {
    type Target = DisplayPlaneCapabilitiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayPlaneCapabilitiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplaySurfaceCreateInfoKHR.html)) · Structure <br/> VkDisplaySurfaceCreateInfoKHR - Structure specifying parameters of a newly created display plane surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplaySurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplaySurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkDisplaySurfaceCreateFlagsKHR    flags;\n    VkDisplayModeKHR                  displayMode;\n    uint32_t                          planeIndex;\n    uint32_t                          planeStackIndex;\n    VkSurfaceTransformFlagBitsKHR     transform;\n    float                             globalAlpha;\n    VkDisplayPlaneAlphaFlagBitsKHR    alphaMode;\n    VkExtent2D                        imageExtent;\n} VkDisplaySurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use, and **must** be zero.\n\n* [`Self::display_mode`] is a [`crate::vk::DisplayModeKHR`] handle specifying the mode\n  to use when displaying this surface.\n\n* [`Self::plane_index`] is the plane on which this surface appears.\n\n* [`Self::plane_stack_index`] is the z-order of the plane.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  specifying the transformation to apply to images as part of the scanout\n  operation.\n\n* [`Self::global_alpha`] is the global alpha value.\n  This value is ignored if [`Self::alpha_mode`] is not[`crate::vk::DisplayPlaneAlphaFlagBitsKHR::GLOBAL_KHR`].\n\n* [`Self::alpha_mode`] is a [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) value\n  specifying the type of alpha blending to use.\n\n* [`Self::image_extent`] is the size of the presentable images to use with the\n  surface.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Creating a display surface **must** not modify the state of the displays,<br/>planes, or other resources it names.<br/>For example, it **must** not apply the specified mode to be set on the<br/>associated display.<br/>Application of display configuration occurs as a side effect of presenting<br/>to a display surface.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-planeIndex-01252  \n  [`Self::plane_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-planeReorderPossible-01253  \n   If the `planeReorderPossible` member of the[`crate::vk::DisplayPropertiesKHR`] structure returned by[`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`] for the display\n  corresponding to [`Self::display_mode`] is [`crate::vk::TRUE`] then[`Self::plane_stack_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]; otherwise[`Self::plane_stack_index`] **must** equal the `currentStackIndex` member of[`crate::vk::DisplayPlanePropertiesKHR`] returned by[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`] for the display plane\n  corresponding to [`Self::display_mode`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-01254  \n   If [`Self::alpha_mode`] is [`crate::vk::DisplayPlaneAlphaFlagBitsKHR::GLOBAL_KHR`] then[`Self::global_alpha`] **must** be between `0` and `1`, inclusive\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-01255  \n  [`Self::alpha_mode`] **must** be one of the bits present in the`supportedAlpha` member of [`crate::vk::DisplayPlaneCapabilitiesKHR`] for\n  the display plane corresponding to [`Self::display_mode`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-width-01256  \n   The `width` and `height` members of [`Self::image_extent`] **must** be\n  less than or equal to[`crate::vk::PhysicalDeviceLimits::max_image_dimension2_d`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-displayMode-parameter  \n  [`Self::display_mode`] **must** be a valid [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-transform-parameter  \n  [`Self::transform`] **must** be a valid [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-parameter  \n  [`Self::alpha_mode`] **must** be a valid [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html), [`crate::vk::DisplaySurfaceCreateFlagBitsKHR`], [`crate::vk::Extent2D`], [`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html), [`crate::vk::InstanceLoader::create_display_plane_surface_khr`]\n"]
#[doc(alias = "VkDisplaySurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplaySurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_display::DisplaySurfaceCreateFlagsKHR,
    pub display_mode: crate::extensions::khr_display::DisplayModeKHR,
    pub plane_index: u32,
    pub plane_stack_index: u32,
    pub transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR,
    pub global_alpha: std::os::raw::c_float,
    pub alpha_mode: crate::extensions::khr_display::DisplayPlaneAlphaFlagBitsKHR,
    pub image_extent: crate::vk1_0::Extent2D,
}
impl DisplaySurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DISPLAY_SURFACE_CREATE_INFO_KHR;
}
impl Default for DisplaySurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), display_mode: Default::default(), plane_index: Default::default(), plane_stack_index: Default::default(), transform: Default::default(), global_alpha: Default::default(), alpha_mode: Default::default(), image_extent: Default::default() }
    }
}
impl std::fmt::Debug for DisplaySurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplaySurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("display_mode", &self.display_mode).field("plane_index", &self.plane_index).field("plane_stack_index", &self.plane_stack_index).field("transform", &self.transform).field("global_alpha", &self.global_alpha).field("alpha_mode", &self.alpha_mode).field("image_extent", &self.image_extent).finish()
    }
}
impl DisplaySurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplaySurfaceCreateInfoKHRBuilder<'a> {
        DisplaySurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplaySurfaceCreateInfoKHR.html)) · Builder of [`DisplaySurfaceCreateInfoKHR`] <br/> VkDisplaySurfaceCreateInfoKHR - Structure specifying parameters of a newly created display plane surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplaySurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_display\ntypedef struct VkDisplaySurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkDisplaySurfaceCreateFlagsKHR    flags;\n    VkDisplayModeKHR                  displayMode;\n    uint32_t                          planeIndex;\n    uint32_t                          planeStackIndex;\n    VkSurfaceTransformFlagBitsKHR     transform;\n    float                             globalAlpha;\n    VkDisplayPlaneAlphaFlagBitsKHR    alphaMode;\n    VkExtent2D                        imageExtent;\n} VkDisplaySurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use, and **must** be zero.\n\n* [`Self::display_mode`] is a [`crate::vk::DisplayModeKHR`] handle specifying the mode\n  to use when displaying this surface.\n\n* [`Self::plane_index`] is the plane on which this surface appears.\n\n* [`Self::plane_stack_index`] is the z-order of the plane.\n\n* [`Self::transform`] is a [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  specifying the transformation to apply to images as part of the scanout\n  operation.\n\n* [`Self::global_alpha`] is the global alpha value.\n  This value is ignored if [`Self::alpha_mode`] is not[`crate::vk::DisplayPlaneAlphaFlagBitsKHR::GLOBAL_KHR`].\n\n* [`Self::alpha_mode`] is a [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) value\n  specifying the type of alpha blending to use.\n\n* [`Self::image_extent`] is the size of the presentable images to use with the\n  surface.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Creating a display surface **must** not modify the state of the displays,<br/>planes, or other resources it names.<br/>For example, it **must** not apply the specified mode to be set on the<br/>associated display.<br/>Application of display configuration occurs as a side effect of presenting<br/>to a display surface.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-planeIndex-01252  \n  [`Self::plane_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-planeReorderPossible-01253  \n   If the `planeReorderPossible` member of the[`crate::vk::DisplayPropertiesKHR`] structure returned by[`crate::vk::InstanceLoader::get_physical_device_display_properties_khr`] for the display\n  corresponding to [`Self::display_mode`] is [`crate::vk::TRUE`] then[`Self::plane_stack_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]; otherwise[`Self::plane_stack_index`] **must** equal the `currentStackIndex` member of[`crate::vk::DisplayPlanePropertiesKHR`] returned by[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`] for the display plane\n  corresponding to [`Self::display_mode`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-01254  \n   If [`Self::alpha_mode`] is [`crate::vk::DisplayPlaneAlphaFlagBitsKHR::GLOBAL_KHR`] then[`Self::global_alpha`] **must** be between `0` and `1`, inclusive\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-01255  \n  [`Self::alpha_mode`] **must** be one of the bits present in the`supportedAlpha` member of [`crate::vk::DisplayPlaneCapabilitiesKHR`] for\n  the display plane corresponding to [`Self::display_mode`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-width-01256  \n   The `width` and `height` members of [`Self::image_extent`] **must** be\n  less than or equal to[`crate::vk::PhysicalDeviceLimits::max_image_dimension2_d`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-displayMode-parameter  \n  [`Self::display_mode`] **must** be a valid [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-transform-parameter  \n  [`Self::transform`] **must** be a valid [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n\n* []() VUID-VkDisplaySurfaceCreateInfoKHR-alphaMode-parameter  \n  [`Self::alpha_mode`] **must** be a valid [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [VkDisplayPlaneAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPlaneAlphaFlagBitsKHR.html), [`crate::vk::DisplaySurfaceCreateFlagBitsKHR`], [`crate::vk::Extent2D`], [`crate::vk::StructureType`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html), [`crate::vk::InstanceLoader::create_display_plane_surface_khr`]\n"]
#[repr(transparent)]
pub struct DisplaySurfaceCreateInfoKHRBuilder<'a>(DisplaySurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> DisplaySurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> DisplaySurfaceCreateInfoKHRBuilder<'a> {
        DisplaySurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_display::DisplaySurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn display_mode(mut self, display_mode: crate::extensions::khr_display::DisplayModeKHR) -> Self {
        self.0.display_mode = display_mode as _;
        self
    }
    #[inline]
    pub fn plane_index(mut self, plane_index: u32) -> Self {
        self.0.plane_index = plane_index as _;
        self
    }
    #[inline]
    pub fn plane_stack_index(mut self, plane_stack_index: u32) -> Self {
        self.0.plane_stack_index = plane_stack_index as _;
        self
    }
    #[inline]
    pub fn transform(mut self, transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR) -> Self {
        self.0.transform = transform as _;
        self
    }
    #[inline]
    pub fn global_alpha(mut self, global_alpha: std::os::raw::c_float) -> Self {
        self.0.global_alpha = global_alpha as _;
        self
    }
    #[inline]
    pub fn alpha_mode(mut self, alpha_mode: crate::extensions::khr_display::DisplayPlaneAlphaFlagBitsKHR) -> Self {
        self.0.alpha_mode = alpha_mode as _;
        self
    }
    #[inline]
    pub fn image_extent(mut self, image_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.image_extent = image_extent as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplaySurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for DisplaySurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> DisplaySurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplaySurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplaySurfaceCreateInfoKHRBuilder<'a> {
    type Target = DisplaySurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplaySurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_display`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDisplayPropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceDisplayPropertiesKHR - Query information about the available displays\n[](#_c_specification)C Specification\n----------\n\nVarious functions are provided for enumerating the available display devices\npresent on a Vulkan physical device.\nTo query information about the available displays, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetPhysicalDeviceDisplayPropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayPropertiesKHR*                     pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display devices available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayPropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display devices available\nfor [`Self::physical_device`] is returned in [`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\ndevices for [`Self::physical_device`], at most [`Self::p_property_count`] structures\nwill be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available properties were\nreturned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceDisplayPropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayPropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceDisplayPropertiesKHR")]
    pub unsafe fn get_physical_device_display_properties_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, property_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_display::DisplayPropertiesKHR>> {
        let _function = self.get_physical_device_display_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut property_count = match property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut properties = crate::SmallVec::from_elem(Default::default(), property_count as _);
        let _return = _function(physical_device as _, &mut property_count, properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, properties)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceDisplayPlanePropertiesKHR.html)) · Function <br/> vkGetPhysicalDeviceDisplayPlanePropertiesKHR - Query the plane properties\n[](#_c_specification)C Specification\n----------\n\nImages are presented to individual planes on a display.\nDevices **must** support at least one plane on each display.\nPlanes **can** be stacked and blended to composite multiple images on one\ndisplay.\nDevices **may** support only a fixed stacking order and fixed mapping between\nplanes and displays, or they **may** allow arbitrary application specified\nstacking orders and mappings between planes and displays.\nTo query the properties of device display planes, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetPhysicalDeviceDisplayPlanePropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayPlanePropertiesKHR*                pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display planes available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayPlanePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display planes available\nfor [`Self::physical_device`] is returned in [`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\nplanes for [`Self::physical_device`], at most [`Self::p_property_count`] structures\nwill be written.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceDisplayPlanePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayPlanePropertiesKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPlanePropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceDisplayPlanePropertiesKHR")]
    pub unsafe fn get_physical_device_display_plane_properties_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, property_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_display::DisplayPlanePropertiesKHR>> {
        let _function = self.get_physical_device_display_plane_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut property_count = match property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut properties = crate::SmallVec::from_elem(Default::default(), property_count as _);
        let _return = _function(physical_device as _, &mut property_count, properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, properties)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayPlaneSupportedDisplaysKHR.html)) · Function <br/> vkGetDisplayPlaneSupportedDisplaysKHR - Query the list of displays a plane supports\n[](#_c_specification)C Specification\n----------\n\nTo determine which displays a plane is usable with, call\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayPlaneSupportedDisplaysKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    planeIndex,\n    uint32_t*                                   pDisplayCount,\n    VkDisplayKHR*                               pDisplays);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is a physical device.\n\n* [`Self::plane_index`] is the plane which the application wishes to use, and**must** be in the range [0, physical device plane count - 1].\n\n* [`Self::p_display_count`] is a pointer to an integer related to the number of\n  displays available or queried, as described below.\n\n* [`Self::p_displays`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayKHR`] handles.\n[](#_description)Description\n----------\n\nIf [`Self::p_displays`] is `NULL`, then the number of displays usable with the\nspecified [`Self::plane_index`] for [`Self::physical_device`] is returned in[`Self::p_display_count`].\nOtherwise, [`Self::p_display_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_displays`] array, and on return the\nvariable is overwritten with the number of handles actually written to[`Self::p_displays`].\nIf the value of [`Self::p_display_count`] is less than the number of usable\ndisplay-plane pairs for [`Self::physical_device`], at most [`Self::p_display_count`]handles will be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available pairs were\nreturned.\n\nValid Usage\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-planeIndex-01249  \n  [`Self::plane_index`] **must** be less than the number of display planes\n  supported by the device as determined by calling[`crate::vk::InstanceLoader::get_physical_device_display_plane_properties_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-pDisplayCount-parameter  \n  [`Self::p_display_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetDisplayPlaneSupportedDisplaysKHR-pDisplays-parameter  \n   If the value referenced by [`Self::p_display_count`] is not `0`, and [`Self::p_displays`] is not `NULL`, [`Self::p_displays`] **must** be a valid pointer to an array of [`Self::p_display_count`] [`crate::vk::DisplayKHR`] handles\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetDisplayPlaneSupportedDisplaysKHR")]
    pub unsafe fn get_display_plane_supported_displays_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, plane_index: u32, display_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_display::DisplayKHR>> {
        let _function = self.get_display_plane_supported_displays_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut display_count = match display_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, plane_index as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut displays = crate::SmallVec::from_elem(Default::default(), display_count as _);
        let _return = _function(physical_device as _, plane_index as _, &mut display_count, displays.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, displays)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayModePropertiesKHR.html)) · Function <br/> vkGetDisplayModePropertiesKHR - Query the set of mode properties supported by the display\n[](#_c_specification)C Specification\n----------\n\nEach display has one or more supported modes associated with it by default.\nThese built-in modes are queried by calling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayModePropertiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display,\n    uint32_t*                                   pPropertyCount,\n    VkDisplayModePropertiesKHR*                 pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with[`Self::display`].\n\n* [`Self::display`] is the display to query.\n\n* [`Self::p_property_count`] is a pointer to an integer related to the number of\n  display modes available or queried, as described below.\n\n* [`Self::p_properties`] is either `NULL` or a pointer to an array of[`crate::vk::DisplayModePropertiesKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_properties`] is `NULL`, then the number of display modes available\non the specified [`Self::display`] for [`Self::physical_device`] is returned in[`Self::p_property_count`].\nOtherwise, [`Self::p_property_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_properties`].\nIf the value of [`Self::p_property_count`] is less than the number of display\nmodes for [`Self::physical_device`], at most [`Self::p_property_count`] structures will\nbe written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available display modes were\nreturned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayModePropertiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayModePropertiesKHR-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkGetDisplayModePropertiesKHR-pPropertyCount-parameter  \n  [`Self::p_property_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetDisplayModePropertiesKHR-pProperties-parameter  \n   If the value referenced by [`Self::p_property_count`] is not `0`, and [`Self::p_properties`] is not `NULL`, [`Self::p_properties`] **must** be a valid pointer to an array of [`Self::p_property_count`] [`crate::vk::DisplayModePropertiesKHR`] structures\n\n* []() VUID-vkGetDisplayModePropertiesKHR-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::DisplayModePropertiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetDisplayModePropertiesKHR")]
    pub unsafe fn get_display_mode_properties_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR, property_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_display::DisplayModePropertiesKHR>> {
        let _function = self.get_display_mode_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut property_count = match property_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, display as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut properties = crate::SmallVec::from_elem(Default::default(), property_count as _);
        let _return = _function(physical_device as _, display as _, &mut property_count, properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, properties)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDisplayModeKHR.html)) · Function <br/> vkCreateDisplayModeKHR - Create a display mode\n[](#_c_specification)C Specification\n----------\n\nAdditional modes **may** also be created by calling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkCreateDisplayModeKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display,\n    const VkDisplayModeCreateInfoKHR*           pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDisplayModeKHR*                           pMode);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with[`Self::display`].\n\n* [`Self::display`] is the display to create an additional mode for.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DisplayModeCreateInfoKHR`]structure describing the new mode to create.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  display mode object when there is no more specific allocator available\n  (see [Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_mode`] is a pointer to a [`crate::vk::DisplayModeKHR`] handle in which the\n  mode created is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDisplayModeKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayModeCreateInfoKHR`] structure\n\n* []() VUID-vkCreateDisplayModeKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDisplayModeKHR-pMode-parameter  \n  [`Self::p_mode`] **must** be a valid pointer to a [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-vkCreateDisplayModeKHR-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nHost Synchronization\n\n* Host access to [`Self::display`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayModeCreateInfoKHR`], [`crate::vk::DisplayModeKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkCreateDisplayModeKHR")]
    pub unsafe fn create_display_mode_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR, create_info: &crate::extensions::khr_display::DisplayModeCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_display::DisplayModeKHR> {
        let _function = self.create_display_mode_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut mode = Default::default();
        let _return = _function(
            physical_device as _,
            display as _,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut mode,
        );
        crate::utils::VulkanResult::new(_return, mode)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDisplayPlaneCapabilitiesKHR.html)) · Function <br/> vkGetDisplayPlaneCapabilitiesKHR - Query capabilities of a mode and plane combination\n[](#_c_specification)C Specification\n----------\n\nApplications that wish to present directly to a display **must** select which\nlayer, or “plane” of the display they wish to target, and a mode to use\nwith the display.\nEach display supports at least one plane.\nThe capabilities of a given mode and plane combination are determined by\ncalling:\n\n```\n// Provided by VK_KHR_display\nVkResult vkGetDisplayPlaneCapabilitiesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayModeKHR                            mode,\n    uint32_t                                    planeIndex,\n    VkDisplayPlaneCapabilitiesKHR*              pCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device associated with the display\n  specified by [`Self::mode`]\n\n* [`Self::mode`] is the display mode the application intends to program when\n  using the specified plane.\n  Note this parameter also implicitly specifies a display.\n\n* [`Self::plane_index`] is the plane which the application intends to use with\n  the display, and is less than the number of display planes supported by\n  the device.\n\n* [`Self::p_capabilities`] is a pointer to a[`crate::vk::DisplayPlaneCapabilitiesKHR`] structure in which the capabilities\n  are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-mode-parameter  \n  [`Self::mode`] **must** be a valid [`crate::vk::DisplayModeKHR`] handle\n\n* []() VUID-vkGetDisplayPlaneCapabilitiesKHR-pCapabilities-parameter  \n  [`Self::p_capabilities`] **must** be a valid pointer to a [`crate::vk::DisplayPlaneCapabilitiesKHR`] structure\n\nHost Synchronization\n\n* Host access to [`Self::mode`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayModeKHR`], [`crate::vk::DisplayPlaneCapabilitiesKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetDisplayPlaneCapabilitiesKHR")]
    pub unsafe fn get_display_plane_capabilities_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, mode: crate::extensions::khr_display::DisplayModeKHR, plane_index: u32) -> crate::utils::VulkanResult<crate::extensions::khr_display::DisplayPlaneCapabilitiesKHR> {
        let _function = self.get_display_plane_capabilities_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut capabilities = Default::default();
        let _return = _function(physical_device as _, mode as _, plane_index as _, &mut capabilities);
        crate::utils::VulkanResult::new(_return, capabilities)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDisplayPlaneSurfaceKHR.html)) · Function <br/> vkCreateDisplayPlaneSurfaceKHR - Create a slink:VkSurfaceKHR structure representing a display plane and mode\n[](#_c_specification)C Specification\n----------\n\nA complete display configuration includes a mode, one or more display planes\nand any parameters describing their behavior, and parameters describing some\naspects of the images associated with those planes.\nDisplay surfaces describe the configuration of a single plane within a\ncomplete display configuration.\nTo create a [`crate::vk::SurfaceKHR`] object for a display plane, call:\n\n```\n// Provided by VK_KHR_display\nVkResult vkCreateDisplayPlaneSurfaceKHR(\n    VkInstance                                  instance,\n    const VkDisplaySurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance corresponding to the physical device the\n  targeted display is on.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::DisplaySurfaceCreateInfoKHR`]structure specifying which mode, plane, and other parameters to use, as\n  described below.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DisplaySurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDisplayPlaneSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DisplaySurfaceCreateInfoKHR`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateDisplayPlaneSurfaceKHR")]
    pub unsafe fn create_display_plane_surface_khr(&self, create_info: &crate::extensions::khr_display::DisplaySurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_display_plane_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
