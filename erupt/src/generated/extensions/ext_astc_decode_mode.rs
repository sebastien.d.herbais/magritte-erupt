#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_ASTC_DECODE_MODE_SPEC_VERSION")]
pub const EXT_ASTC_DECODE_MODE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_ASTC_DECODE_MODE_EXTENSION_NAME")]
pub const EXT_ASTC_DECODE_MODE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_astc_decode_mode");
#[doc = "Provided by [`crate::extensions::ext_astc_decode_mode`]"]
impl crate::vk1_0::StructureType {
    pub const IMAGE_VIEW_ASTC_DECODE_MODE_EXT: Self = Self(1000067000);
    pub const PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT: Self = Self(1000067001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceASTCDecodeFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImageViewASTCDecodeModeEXT> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImageViewASTCDecodeModeEXTBuilder<'_>> for crate::vk1_0::ImageViewCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceASTCDecodeFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewASTCDecodeModeEXT.html)) · Structure <br/> VkImageViewASTCDecodeModeEXT - Structure describing the ASTC decode mode for an image view\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::ImageViewASTCDecodeModeEXT`]structure, then that structure includes a parameter specifying the decode\nmode for image views using ASTC compressed formats.\n\nThe [`crate::vk::ImageViewASTCDecodeModeEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_astc_decode_mode\ntypedef struct VkImageViewASTCDecodeModeEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkFormat           decodeMode;\n} VkImageViewASTCDecodeModeEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::decode_mode`] is the intermediate format used to decode ASTC\n  compressed formats.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02230  \n  [`Self::decode_mode`] **must** be one of [`crate::vk::Format::R16G16B16A16_SFLOAT`],[`crate::vk::Format::R8G8B8A8_UNORM`], or[`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02231  \n   If the [`decodeModeSharedExponent`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-astc-decodeModeSharedExponent) feature is not enabled,[`Self::decode_mode`] **must** not be [`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02232  \n   If [`Self::decode_mode`] is [`crate::vk::Format::R8G8B8A8_UNORM`] the image view**must** not include blocks using any of the ASTC HDR modes\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-format-04084  \n  `format` of the image view **must** be one of the[ASTC Compressed Image Formats](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#appendix-compressedtex-astc)\n\nIf `format` uses sRGB encoding then the [`Self::decode_mode`] has no effect.\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_ASTC_DECODE_MODE_EXT`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-parameter  \n  [`Self::decode_mode`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImageViewASTCDecodeModeEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageViewASTCDecodeModeEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub decode_mode: crate::vk1_0::Format,
}
impl ImageViewASTCDecodeModeEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_VIEW_ASTC_DECODE_MODE_EXT;
}
impl Default for ImageViewASTCDecodeModeEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), decode_mode: Default::default() }
    }
}
impl std::fmt::Debug for ImageViewASTCDecodeModeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageViewASTCDecodeModeEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("decode_mode", &self.decode_mode).finish()
    }
}
impl ImageViewASTCDecodeModeEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageViewASTCDecodeModeEXTBuilder<'a> {
        ImageViewASTCDecodeModeEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewASTCDecodeModeEXT.html)) · Builder of [`ImageViewASTCDecodeModeEXT`] <br/> VkImageViewASTCDecodeModeEXT - Structure describing the ASTC decode mode for an image view\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::ImageViewASTCDecodeModeEXT`]structure, then that structure includes a parameter specifying the decode\nmode for image views using ASTC compressed formats.\n\nThe [`crate::vk::ImageViewASTCDecodeModeEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_astc_decode_mode\ntypedef struct VkImageViewASTCDecodeModeEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkFormat           decodeMode;\n} VkImageViewASTCDecodeModeEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::decode_mode`] is the intermediate format used to decode ASTC\n  compressed formats.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02230  \n  [`Self::decode_mode`] **must** be one of [`crate::vk::Format::R16G16B16A16_SFLOAT`],[`crate::vk::Format::R8G8B8A8_UNORM`], or[`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02231  \n   If the [`decodeModeSharedExponent`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-astc-decodeModeSharedExponent) feature is not enabled,[`Self::decode_mode`] **must** not be [`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-02232  \n   If [`Self::decode_mode`] is [`crate::vk::Format::R8G8B8A8_UNORM`] the image view**must** not include blocks using any of the ASTC HDR modes\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-format-04084  \n  `format` of the image view **must** be one of the[ASTC Compressed Image Formats](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#appendix-compressedtex-astc)\n\nIf `format` uses sRGB encoding then the [`Self::decode_mode`] has no effect.\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_ASTC_DECODE_MODE_EXT`]\n\n* []() VUID-VkImageViewASTCDecodeModeEXT-decodeMode-parameter  \n  [`Self::decode_mode`] **must** be a valid [`crate::vk::Format`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Format`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImageViewASTCDecodeModeEXTBuilder<'a>(ImageViewASTCDecodeModeEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ImageViewASTCDecodeModeEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ImageViewASTCDecodeModeEXTBuilder<'a> {
        ImageViewASTCDecodeModeEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn decode_mode(mut self, decode_mode: crate::vk1_0::Format) -> Self {
        self.0.decode_mode = decode_mode as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageViewASTCDecodeModeEXT {
        self.0
    }
}
impl<'a> std::default::Default for ImageViewASTCDecodeModeEXTBuilder<'a> {
    fn default() -> ImageViewASTCDecodeModeEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageViewASTCDecodeModeEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageViewASTCDecodeModeEXTBuilder<'a> {
    type Target = ImageViewASTCDecodeModeEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageViewASTCDecodeModeEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceASTCDecodeFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceASTCDecodeFeaturesEXT - Structure describing ASTC decode mode features\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_astc_decode_mode\ntypedef struct VkPhysicalDeviceASTCDecodeFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           decodeModeSharedExponent;\n} VkPhysicalDeviceASTCDecodeFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::decode_mode_shared_exponent`] indicates whether the implementation\n  supports decoding ASTC compressed formats to[`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`] internal precision.\n\nIf the [`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceASTCDecodeFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceASTCDecodeFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceASTCDecodeFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub decode_mode_shared_exponent: crate::vk1_0::Bool32,
}
impl PhysicalDeviceASTCDecodeFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT;
}
impl Default for PhysicalDeviceASTCDecodeFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), decode_mode_shared_exponent: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceASTCDecodeFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceASTCDecodeFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("decode_mode_shared_exponent", &(self.decode_mode_shared_exponent != 0)).finish()
    }
}
impl PhysicalDeviceASTCDecodeFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
        PhysicalDeviceASTCDecodeFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceASTCDecodeFeaturesEXT.html)) · Builder of [`PhysicalDeviceASTCDecodeFeaturesEXT`] <br/> VkPhysicalDeviceASTCDecodeFeaturesEXT - Structure describing ASTC decode mode features\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_astc_decode_mode\ntypedef struct VkPhysicalDeviceASTCDecodeFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           decodeModeSharedExponent;\n} VkPhysicalDeviceASTCDecodeFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::decode_mode_shared_exponent`] indicates whether the implementation\n  supports decoding ASTC compressed formats to[`crate::vk::Format::E5B9G9R9_UFLOAT_PACK32`] internal precision.\n\nIf the [`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceASTCDecodeFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceASTCDecodeFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a>(PhysicalDeviceASTCDecodeFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
        PhysicalDeviceASTCDecodeFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn decode_mode_shared_exponent(mut self, decode_mode_shared_exponent: bool) -> Self {
        self.0.decode_mode_shared_exponent = decode_mode_shared_exponent as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceASTCDecodeFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceASTCDecodeFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceASTCDecodeFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
