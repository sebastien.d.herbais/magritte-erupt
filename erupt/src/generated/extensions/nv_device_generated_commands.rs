#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEVICE_GENERATED_COMMANDS_SPEC_VERSION")]
pub const NV_DEVICE_GENERATED_COMMANDS_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEVICE_GENERATED_COMMANDS_EXTENSION_NAME")]
pub const NV_DEVICE_GENERATED_COMMANDS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_device_generated_commands");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_EXECUTE_GENERATED_COMMANDS_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdExecuteGeneratedCommandsNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_PREPROCESS_GENERATED_COMMANDS_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdPreprocessGeneratedCommandsNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BIND_PIPELINE_SHADER_GROUP_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdBindPipelineShaderGroupNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_NV: *const std::os::raw::c_char = crate::cstr!("vkGetGeneratedCommandsMemoryRequirementsNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_INDIRECT_COMMANDS_LAYOUT_NV: *const std::os::raw::c_char = crate::cstr!("vkCreateIndirectCommandsLayoutNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_INDIRECT_COMMANDS_LAYOUT_NV: *const std::os::raw::c_char = crate::cstr!("vkDestroyIndirectCommandsLayoutNV");
crate::non_dispatchable_handle!(IndirectCommandsLayoutNV, INDIRECT_COMMANDS_LAYOUT_NV, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutNV.html)) · Non-dispatchable Handle <br/> VkIndirectCommandsLayoutNV - Opaque handle to an indirect commands layout object\n[](#_c_specification)C Specification\n----------\n\nThe device-side command generation happens through an iterative processing\nof an atomic sequence comprised of command tokens, which are represented by:\n\n```\n// Provided by VK_NV_device_generated_commands\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkIndirectCommandsLayoutNV)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::GeneratedCommandsInfoNV`], [`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`], [`crate::vk::DeviceLoader::create_indirect_commands_layout_nv`], [`crate::vk::DeviceLoader::destroy_indirect_commands_layout_nv`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkIndirectCommandsLayoutNV)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkIndirectCommandsLayoutNV");
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::vk1_0::AccessFlagBits {
    pub const COMMAND_PREPROCESS_READ_NV: Self = Self(131072);
    pub const COMMAND_PREPROCESS_WRITE_NV: Self = Self(262144);
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::vk1_0::PipelineCreateFlagBits {
    pub const INDIRECT_BINDABLE_NV: Self = Self(262144);
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV: Self = Self(1000277000);
    pub const GRAPHICS_SHADER_GROUP_CREATE_INFO_NV: Self = Self(1000277001);
    pub const GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV: Self = Self(1000277002);
    pub const INDIRECT_COMMANDS_LAYOUT_TOKEN_NV: Self = Self(1000277003);
    pub const INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV: Self = Self(1000277004);
    pub const GENERATED_COMMANDS_INFO_NV: Self = Self(1000277005);
    pub const GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV: Self = Self(1000277006);
    pub const PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV: Self = Self(1000277007);
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::vk1_0::PipelineStageFlagBits {
    pub const COMMAND_PREPROCESS_NV: Self = Self(131072);
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::vk1_0::ObjectType {
    pub const INDIRECT_COMMANDS_LAYOUT_NV: Self = Self(1000277000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagsNV.html)) · Bitmask of [`IndirectCommandsLayoutUsageFlagBitsNV`] <br/> VkIndirectCommandsLayoutUsageFlagsNV - Bitmask of VkIndirectCommandsLayoutUsageFlagBitsNV\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef VkFlags VkIndirectCommandsLayoutUsageFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV`] is a bitmask type for setting a\nmask of zero or more [VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutCreateInfoNV`], [VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html)\n"] # [doc (alias = "VkIndirectCommandsLayoutUsageFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct IndirectCommandsLayoutUsageFlagsNV : u32 { const EXPLICIT_PREPROCESS_NV = IndirectCommandsLayoutUsageFlagBitsNV :: EXPLICIT_PREPROCESS_NV . 0 ; const INDEXED_SEQUENCES_NV = IndirectCommandsLayoutUsageFlagBitsNV :: INDEXED_SEQUENCES_NV . 0 ; const UNORDERED_SEQUENCES_NV = IndirectCommandsLayoutUsageFlagBitsNV :: UNORDERED_SEQUENCES_NV . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html)) · Bits enum of [`IndirectCommandsLayoutUsageFlagsNV`] <br/> VkIndirectCommandsLayoutUsageFlagBitsNV - Bitmask specifying allowed usage of an indirect commands layout\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::IndirectCommandsLayoutCreateInfoNV::flags`], specifying usage\nhints of an indirect command layout, are:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef enum VkIndirectCommandsLayoutUsageFlagBitsNV {\n    VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EXPLICIT_PREPROCESS_BIT_NV = 0x00000001,\n    VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV = 0x00000002,\n    VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV = 0x00000004,\n} VkIndirectCommandsLayoutUsageFlagBitsNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::EXPLICIT_PREPROCESS_NV`]specifies that the layout is always used with the manual preprocessing\n  step through calling [`crate::vk::DeviceLoader::cmd_preprocess_generated_commands_nv`] and\n  executed by [`crate::vk::DeviceLoader::cmd_execute_generated_commands_nv`] with `isPreprocessed`set to [`crate::vk::TRUE`].\n\n* [`Self::INDEXED_SEQUENCES_NV`]specifies that the input data for the sequences is not implicitly\n  indexed from 0..sequencesUsed but a user provided [`crate::vk::Buffer`]encoding the index is provided.\n\n* [`Self::UNORDERED_SEQUENCES_NV`]specifies that the processing of sequences **can** happen at an\n  implementation-dependent order, which is not: guaranteed to be coherent\n  using the same input data.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV`]\n"]
#[doc(alias = "VkIndirectCommandsLayoutUsageFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct IndirectCommandsLayoutUsageFlagBitsNV(pub u32);
impl IndirectCommandsLayoutUsageFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> IndirectCommandsLayoutUsageFlagsNV {
        IndirectCommandsLayoutUsageFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for IndirectCommandsLayoutUsageFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::EXPLICIT_PREPROCESS_NV => "EXPLICIT_PREPROCESS_NV",
            &Self::INDEXED_SEQUENCES_NV => "INDEXED_SEQUENCES_NV",
            &Self::UNORDERED_SEQUENCES_NV => "UNORDERED_SEQUENCES_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutUsageFlagBitsNV {
    pub const EXPLICIT_PREPROCESS_NV: Self = Self(1);
    pub const INDEXED_SEQUENCES_NV: Self = Self(2);
    pub const UNORDERED_SEQUENCES_NV: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsTokenTypeNV.html)) · Enum <br/> VkIndirectCommandsTokenTypeNV - Enum specifying token commands\n[](#_c_specification)C Specification\n----------\n\nPossible values of those elements of the[`crate::vk::IndirectCommandsLayoutCreateInfoNV::p_tokens`] array which\nspecify command tokens (other elements of the array specify command\nparameters) are:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef enum VkIndirectCommandsTokenTypeNV {\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_SHADER_GROUP_NV = 0,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_STATE_FLAGS_NV = 1,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NV = 2,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NV = 3,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NV = 4,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NV = 5,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NV = 6,\n    VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_TASKS_NV = 7,\n} VkIndirectCommandsTokenTypeNV;\n```\n[](#_description)Description\n----------\n\n|                    Token type                    |                         Equivalent command                          |\n|--------------------------------------------------|---------------------------------------------------------------------|\n|[`Self::SHADER_GROUP_NV`] |[`crate::vk::DeviceLoader::cmd_bind_pipeline_shader_group_nv`]|\n| [`Self::STATE_FLAGS_NV`] |                                 \\-                                  |\n|[`Self::INDEX_BUFFER_NV`] |          [`crate::vk::PFN_vkCmdBindIndexBuffer`]          |\n|[`Self::VERTEX_BUFFER_NV`]|        [`crate::vk::PFN_vkCmdBindVertexBuffers`]        |\n|[`Self::PUSH_CONSTANT_NV`]|            [`crate::vk::PFN_vkCmdPushConstants`]            |\n|[`Self::DRAW_INDEXED_NV`] |      [`crate::vk::PFN_vkCmdDrawIndexedIndirect`]      |\n|    [`Self::DRAW_NV`]     |             [`crate::vk::PFN_vkCmdDrawIndirect`]             |\n| [`Self::DRAW_TASKS_NV`]  |  [`crate::vk::DeviceLoader::cmd_draw_mesh_tasks_indirect_nv`]  |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutTokenNV`]\n"]
#[doc(alias = "VkIndirectCommandsTokenTypeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct IndirectCommandsTokenTypeNV(pub i32);
impl std::fmt::Debug for IndirectCommandsTokenTypeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::SHADER_GROUP_NV => "SHADER_GROUP_NV",
            &Self::STATE_FLAGS_NV => "STATE_FLAGS_NV",
            &Self::INDEX_BUFFER_NV => "INDEX_BUFFER_NV",
            &Self::VERTEX_BUFFER_NV => "VERTEX_BUFFER_NV",
            &Self::PUSH_CONSTANT_NV => "PUSH_CONSTANT_NV",
            &Self::DRAW_INDEXED_NV => "DRAW_INDEXED_NV",
            &Self::DRAW_NV => "DRAW_NV",
            &Self::DRAW_TASKS_NV => "DRAW_TASKS_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::extensions::nv_device_generated_commands::IndirectCommandsTokenTypeNV {
    pub const SHADER_GROUP_NV: Self = Self(0);
    pub const STATE_FLAGS_NV: Self = Self(1);
    pub const INDEX_BUFFER_NV: Self = Self(2);
    pub const VERTEX_BUFFER_NV: Self = Self(3);
    pub const PUSH_CONSTANT_NV: Self = Self(4);
    pub const DRAW_INDEXED_NV: Self = Self(5);
    pub const DRAW_NV: Self = Self(6);
    pub const DRAW_TASKS_NV: Self = Self(7);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagsNV.html)) · Bitmask of [`IndirectStateFlagBitsNV`] <br/> VkIndirectStateFlagsNV - Bitmask of VkIndirectStateFlagBitsNV\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef VkFlags VkIndirectStateFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::IndirectStateFlagBitsNV`] is a bitmask type for setting a mask of zero or\nmore [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutTokenNV`], [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html)\n"] # [doc (alias = "VkIndirectStateFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct IndirectStateFlagsNV : u32 { const FLAG_FRONTFACE_NV = IndirectStateFlagBitsNV :: FLAG_FRONTFACE_NV . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html)) · Bits enum of [`IndirectStateFlagsNV`] <br/> VkIndirectStateFlagBitsNV - Bitmask specifiying state that can be altered on the device\n[](#_c_specification)C Specification\n----------\n\nA subset of the graphics pipeline state **can** be altered using indirect state\nflags:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef enum VkIndirectStateFlagBitsNV {\n    VK_INDIRECT_STATE_FLAG_FRONTFACE_BIT_NV = 0x00000001,\n} VkIndirectStateFlagBitsNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::FLAG_FRONTFACE_NV`] allows to toggle the[`crate::vk::FrontFace`] rasterization state for subsequent draw operations.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectStateFlagBitsNV`]\n"]
#[doc(alias = "VkIndirectStateFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct IndirectStateFlagBitsNV(pub u32);
impl IndirectStateFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> IndirectStateFlagsNV {
        IndirectStateFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for IndirectStateFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::FLAG_FRONTFACE_NV => "FLAG_FRONTFACE_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::extensions::nv_device_generated_commands::IndirectStateFlagBitsNV {
    pub const FLAG_FRONTFACE_NV: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdExecuteGeneratedCommandsNV.html)) · Function <br/> vkCmdExecuteGeneratedCommandsNV - Generate and execute commands on the device\n[](#_c_specification)C Specification\n----------\n\nThe actual generation of commands as well as their execution on the device\nis handled as single action with:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdExecuteGeneratedCommandsNV(\n    VkCommandBuffer                             commandBuffer,\n    VkBool32                                    isPreprocessed,\n    const VkGeneratedCommandsInfoNV*            pGeneratedCommandsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::is_preprocessed`] represents whether the input data has already been\n  preprocessed on the device.\n  If it is [`crate::vk::FALSE`] this command will implicitly trigger the\n  preprocessing step, otherwise not.\n\n* [`Self::p_generated_commands_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsInfoNV`] structure containing parameters\n  affecting the generation of commands.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-magFilter-04553  \n   If a [`crate::vk::Sampler`] created with `magFilter` or `minFilter`equal to [`crate::vk::Filter::LINEAR`] and `compareEnable` equal to[`crate::vk::FALSE`] is used to sample a [`crate::vk::ImageView`] as a result of this\n  command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-mipmapMode-04770  \n   If a [`crate::vk::Sampler`] created with `mipmapMode` equal to[`crate::vk::SamplerMipmapMode::LINEAR`] and `compareEnable` equal to[`crate::vk::FALSE`] is used to sample a [`crate::vk::ImageView`] as a result of this\n  command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02691  \n   If a [`crate::vk::ImageView`] is accessed using atomic operations as a result\n  of this command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::STORAGE_IMAGE_ATOMIC`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02692  \n   If a [`crate::vk::ImageView`] is sampled with [`crate::vk::Filter::CUBIC_EXT`] as a\n  result of this command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_CUBIC_EXT`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-filterCubic-02694  \n   Any [`crate::vk::ImageView`] being sampled with [`crate::vk::Filter::CUBIC_EXT`] as a\n  result of this command **must** have a [`crate::vk::ImageViewType`] and format\n  that supports cubic filtering, as specified by[`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]::`filterCubic`returned by [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-filterCubicMinmax-02695  \n   Any [`crate::vk::ImageView`] being sampled with [`crate::vk::Filter::CUBIC_EXT`] with\n  a reduction mode of either [`crate::vk::SamplerReductionMode::MIN`] or[`crate::vk::SamplerReductionMode::MAX`] as a result of this command **must**have a [`crate::vk::ImageViewType`] and format that supports cubic filtering\n  together with minmax filtering, as specified by[`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]::`filterCubicMinmax`returned by [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-flags-02696  \n   Any [`crate::vk::Image`] created with a [`crate::vk::ImageCreateInfo::flags`]containing [`crate::vk::ImageCreateFlagBits::CORNER_SAMPLED_NV`] sampled as a\n  result of this command **must** only be sampled using a[`crate::vk::SamplerAddressMode`] of[`crate::vk::SamplerAddressMode::CLAMP_TO_EDGE`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02697  \n   For each set *n* that is statically used by the [`crate::vk::Pipeline`] bound\n  to the pipeline bind point used by this command, a descriptor set **must**have been bound to *n* at the same pipeline bind point, with a[`crate::vk::PipelineLayout`] that is compatible for set *n*, with the[`crate::vk::PipelineLayout`] used to create the current [`crate::vk::Pipeline`], as\n  described in [[descriptorsets-compatibility]](#descriptorsets-compatibility)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02698  \n   For each push constant that is statically used by the [`crate::vk::Pipeline`]bound to the pipeline bind point used by this command, a push constant\n  value **must** have been set for the same pipeline bind point, with a[`crate::vk::PipelineLayout`] that is compatible for push constants, with the[`crate::vk::PipelineLayout`] used to create the current [`crate::vk::Pipeline`], as\n  described in [[descriptorsets-compatibility]](#descriptorsets-compatibility)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02699  \n   Descriptors in each bound descriptor set, specified via[`crate::vk::PFN_vkCmdBindDescriptorSets`], **must** be valid if they are statically\n  used by the [`crate::vk::Pipeline`] bound to the pipeline bind point used by\n  this command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02700  \n   A valid pipeline **must** be bound to the pipeline bind point used by this\n  command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02701  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command requires any dynamic state, that state **must** have been set\n  or inherited (if the `[[VK_NV_inherited_viewport_scissor]](#VK_NV_inherited_viewport_scissor)` extension is\n  enabled) for [`Self::command_buffer`], and done so after any previously bound\n  pipeline with the corresponding state not specified as dynamic\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02859  \n   There **must** not have been any calls to dynamic state setting commands\n  for any state not specified as dynamic in the [`crate::vk::Pipeline`] object\n  bound to the pipeline bind point used by this command, since that\n  pipeline was bound\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02702  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used to sample from any[`crate::vk::Image`] with a [`crate::vk::ImageView`] of the type[`crate::vk::ImageViewType::_3D`], [`crate::vk::ImageViewType::CUBE`],[`crate::vk::ImageViewType::_1D_ARRAY`], [`crate::vk::ImageViewType::_2D_ARRAY`] or[`crate::vk::ImageViewType::CUBE_ARRAY`], in any shader stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02703  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used with any of the SPIR-V`OpImageSample*` or `OpImageSparseSample*` instructions with`ImplicitLod`, `Dref` or `Proj` in their name, in any shader\n  stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02704  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used with any of the SPIR-V`OpImageSample*` or `OpImageSparseSample*` instructions that includes a\n  LOD bias or any offset values, in any shader stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02705  \n   If the [robust buffer access](#features-robustBufferAccess) feature is\n  not enabled, and if the [`crate::vk::Pipeline`] object bound to the pipeline\n  bind point used by this command accesses a uniform buffer, it **must** not\n  access values outside of the range of the buffer as specified in the\n  descriptor set bound to the same pipeline bind point\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02706  \n   If the [robust buffer access](#features-robustBufferAccess) feature is\n  not enabled, and if the [`crate::vk::Pipeline`] object bound to the pipeline\n  bind point used by this command accesses a storage buffer, it **must** not\n  access values outside of the range of the buffer as specified in the\n  descriptor set bound to the same pipeline bind point\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02707  \n   If [`Self::command_buffer`] is an unprotected command buffer, any resource\n  accessed by the [`crate::vk::Pipeline`] object bound to the pipeline bind point\n  used by this command **must** not be a protected resource\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04115  \n   If a [`crate::vk::ImageView`] is accessed using `OpImageWrite` as a result\n  of this command, then the `Type` of the `Texel` operand of that\n  instruction **must** have at least as many components as the image view’s\n  format.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-OpImageWrite-04469  \n   If a [`crate::vk::BufferView`] is accessed using `OpImageWrite` as a result\n  of this command, then the `Type` of the `Texel` operand of that\n  instruction **must** have at least as many components as the buffer view’s\n  format.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04470  \n   If a [`crate::vk::ImageView`] with a [`crate::vk::Format`] that has a 64-bit channel\n  width is accessed as a result of this command, the `SampledType` of\n  the `OpTypeImage` operand of that instruction **must** have a `Width`of 64.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04471  \n   If a [`crate::vk::ImageView`] with a [`crate::vk::Format`] that has a channel width\n  less than 64-bit is accessed as a result of this command, the`SampledType` of the `OpTypeImage` operand of that instruction**must** have a `Width` of 32.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04472  \n   If a [`crate::vk::BufferView`] with a [`crate::vk::Format`] that has a 64-bit channel\n  width is accessed as a result of this command, the `SampledType` of\n  the `OpTypeImage` operand of that instruction **must** have a `Width`of 64.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04473  \n   If a [`crate::vk::BufferView`] with a [`crate::vk::Format`] that has a channel width\n  less than 64-bit is accessed as a result of this command, the`SampledType` of the `OpTypeImage` operand of that instruction**must** have a `Width` of 32.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sparseImageInt64Atomics-04474  \n   If the[`sparseImageInt64Atomics`](#features-sparseImageInt64Atomics)feature is not enabled, [`crate::vk::Image`] objects created with the[`crate::vk::ImageCreateFlagBits::SPARSE_RESIDENCY`] flag **must** not be accessed by\n  atomic instructions through an `OpTypeImage` with a `SampledType`with a `Width` of 64 by this command.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sparseImageInt64Atomics-04475  \n   If the[`sparseImageInt64Atomics`](#features-sparseImageInt64Atomics)feature is not enabled, [`crate::vk::Buffer`] objects created with the[`crate::vk::BufferCreateFlagBits::SPARSE_RESIDENCY`] flag **must** not be accessed\n  by atomic instructions through an `OpTypeImage` with a`SampledType` with a `Width` of 64 by this command.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-renderPass-02684  \n   The current render pass **must** be [compatible](#renderpass-compatibility)with the `renderPass` member of the[`crate::vk::GraphicsPipelineCreateInfo`] structure specified when creating the[`crate::vk::Pipeline`] bound to [`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-subpass-02685  \n   The subpass index of the current render pass **must** be equal to the`subpass` member of the [`crate::vk::GraphicsPipelineCreateInfo`] structure\n  specified when creating the [`crate::vk::Pipeline`] bound to[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02686  \n   Every input attachment used by the current subpass **must** be bound to the\n  pipeline via a descriptor set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04584  \n   Image subresources used as attachments in the current render pass **must**not be accessed in any way other than as an attachment by this command,\n  except for cases involving read-only access to depth/stencil attachments\n  as described in the [Render Pass](#renderpass-attachment-nonattachment)chapter\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-maxMultiviewInstanceIndex-02688  \n   If the draw is recorded in a render pass instance with multiview\n  enabled, the maximum instance index **must** be less than or equal to[`crate::vk::PhysicalDeviceMultiviewProperties::max_multiview_instance_index`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sampleLocationsEnable-02689  \n   If the bound graphics pipeline was created with[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT::sample_locations_enable`]set to [`crate::vk::TRUE`] and the current subpass has a depth/stencil\n  attachment, then that attachment **must** have been created with the[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] bit set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-03417  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] dynamic state\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] **must** have been called\n  in the current command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** match the[`crate::vk::PipelineViewportStateCreateInfo`]::`scissorCount` of the\n  pipeline\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-scissorCount-03418  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] dynamic state enabled, but\n  not the [`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must** have been called\n  in the current command buffer prior to this drawing command, and the`scissorCount` parameter of [`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must**match the [`crate::vk::PipelineViewportStateCreateInfo`]::`viewportCount`of the pipeline\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-03419  \n   If the bound graphics pipeline state was created with both the[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic states enabled\n  then both [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] and[`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** match the `scissorCount` parameter of[`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04137  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::VIEWPORT_W_SCALING_NV`] dynamic state\n  enabled, then the bound graphics pipeline **must** have been created with[`crate::vk::PipelineViewportWScalingStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04138  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_W_SCALING_NV`] dynamic states enabled then\n  the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_w_scaling_nv`] **must** be greater than or equal to the`viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04139  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`]dynamic state enabled, then the bound graphics pipeline **must** have been\n  created with[`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04140  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`] dynamic states\n  enabled then the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_shading_rate_palette_nv`] **must** be greater than or\n  equal to the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-VkPipelineVieportCreateInfo-04141  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled and\n  an instance of [`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`] chained\n  from `VkPipelineVieportCreateInfo`, then the bound graphics pipeline**must** have been created with[`crate::vk::PipelineViewportSwizzleStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-VkPipelineVieportCreateInfo-04142  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled and\n  an instance of [`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV`]chained from `VkPipelineVieportCreateInfo`, then the bound graphics\n  pipeline **must** have been created with[`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV::exclusive_scissor_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-primitiveTopology-03420  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PRIMITIVE_TOPOLOGY_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_primitive_topology_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`primitiveTopology` parameter of [`crate::vk::DeviceLoader::cmd_set_primitive_topology_ext`]**must** be of the same [topology\n  class](#drawing-primitive-topology-class) as the pipeline[`crate::vk::PipelineInputAssemblyStateCreateInfo::topology`] state\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04875  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PATCH_CONTROL_POINTS_EXT`] dynamic state enabled\n  then [`crate::vk::DeviceLoader::cmd_set_patch_control_points_ext`] **must** have been called in the\n  current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04876  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::RASTERIZER_DISCARD_ENABLE_EXT`] dynamic state\n  enabled then [`crate::vk::DeviceLoader::cmd_set_rasterizer_discard_enable_ext`] **must** have been\n  called in the current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04877  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::DEPTH_BIAS_ENABLE_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_depth_bias_enable_ext`] **must** have been called in the current\n  command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-logicOp-04878  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::LOGIC_OP_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_logic_op_ext`] **must** have been called in the current command\n  buffer prior to this drawing command and the `logicOp` **must** be a\n  valid [`crate::vk::LogicOp`] value\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04879  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PRIMITIVE_RESTART_ENABLE_EXT`] dynamic state\n  enabled then [`crate::vk::DeviceLoader::cmd_set_primitive_restart_enable_ext`] **must** have been\n  called in the current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-primitiveFragmentShadingRateWithMultipleViewports-04552  \n   If the [`primitiveFragmentShadingRateWithMultipleViewports`](#limits-primitiveFragmentShadingRateWithMultipleViewports) limit is not\n  supported, the bound graphics pipeline was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  and any of the shader stages of the bound graphics pipeline write to the`PrimitiveShadingRateKHR` built-in, then[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** be `1`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-blendEnable-04727  \n   If rasterization is not disabled in the bound graphics pipeline, then\n  for each color attachment in the subpass, if the corresponding image\n  view’s [format features](#resources-image-view-format-features) do not\n  contain [`crate::vk::FormatFeatureFlagBits::COLOR_ATTACHMENT_BLEND`], then the`blendEnable` member of the corresponding element of the`pAttachments` member of `pColorBlendState` **must** be[`crate::vk::FALSE`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-rasterizationSamples-04740  \n   If rasterization is not disabled in the bound graphics pipeline, and\n  neither the `[[VK_AMD_mixed_attachment_samples]](#VK_AMD_mixed_attachment_samples)` nor the`[[VK_NV_framebuffer_mixed_samples]](#VK_NV_framebuffer_mixed_samples)` extensions are enabled, then[`crate::vk::PipelineMultisampleStateCreateInfo::rasterization_samples`]**must** be the same as the current subpass color and/or depth/stencil\n  attachments\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04912  \n   If the bound graphics pipeline was created with both the[`crate::vk::DynamicState::VERTEX_INPUT_EXT`] and[`crate::vk::DynamicState::VERTEX_INPUT_BINDING_STRIDE_EXT`] dynamic states\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`] **must** have been called in the\n  current command buffer prior to this draw command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pStrides-04913  \n   If the bound graphics pipeline was created with the[`crate::vk::DynamicState::VERTEX_INPUT_BINDING_STRIDE_EXT`] dynamic state\n  enabled, but not the [`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic\n  state enabled, then [`crate::vk::DeviceLoader::cmd_bind_vertex_buffers2_ext`] **must** have been\n  called in the current command buffer prior to this draw command, and the`pStrides` parameter of [`crate::vk::DeviceLoader::cmd_bind_vertex_buffers2_ext`] **must** not\n  be `NULL`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04914  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic state enabled, then[`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`] **must** have been called in the current\n  command buffer prior to this draw command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04007  \n   All vertex input bindings accessed via vertex input variables declared\n  in the vertex shader entry point’s interface **must** have either valid or[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) buffers bound\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04008  \n   If the [nullDescriptor](#features-nullDescriptor) feature is not\n  enabled, all vertex input bindings accessed via vertex input variables\n  declared in the vertex shader entry point’s interface **must** not be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02721  \n   For a given vertex buffer binding, any attribute data fetched **must** be\n  entirely contained within the corresponding vertex buffer binding, as\n  described in [[fxvertex-input]](#fxvertex-input)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02970  \n  [`Self::command_buffer`] **must** not be a protected command buffer\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-isPreprocessed-02908  \n   If [`Self::is_preprocessed`] is [`crate::vk::TRUE`] then[`crate::vk::DeviceLoader::cmd_preprocess_generated_commands_nv`] **must** have already been\n  executed on the device, using the same [`Self::p_generated_commands_info`]content as well as the content of the input buffers it references (all\n  except [`crate::vk::GeneratedCommandsInfoNV::preprocess_buffer`]).\n  Furthermore [`Self::p_generated_commands_info`]`s `indirectCommandsLayout`**must** have been created with the[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::EXPLICIT_PREPROCESS_NV`] bit\n  set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pipeline-02909  \n  [`crate::vk::GeneratedCommandsInfoNV`]::`pipeline` **must** match the current\n  bound pipeline at[`crate::vk::GeneratedCommandsInfoNV`]::`pipelineBindPoint`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02910  \n   Transform feedback **must** not be active\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-deviceGeneratedCommands-02911  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pGeneratedCommandsInfo-parameter  \n  [`Self::p_generated_commands_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsInfoNV`] structure\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-renderpass  \n   This command **must** only be called inside of a render pass instance\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                  Inside                  |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CommandBuffer`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdExecuteGeneratedCommandsNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, is_preprocessed: crate::vk1_0::Bool32, p_generated_commands_info: *const crate::extensions::nv_device_generated_commands::GeneratedCommandsInfoNV) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdPreprocessGeneratedCommandsNV.html)) · Function <br/> vkCmdPreprocessGeneratedCommandsNV - Performs preprocessing for generated commands\n[](#_c_specification)C Specification\n----------\n\nCommands **can** be preprocessed prior execution using the following command:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdPreprocessGeneratedCommandsNV(\n    VkCommandBuffer                             commandBuffer,\n    const VkGeneratedCommandsInfoNV*            pGeneratedCommandsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer which does the preprocessing.\n\n* [`Self::p_generated_commands_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsInfoNV`] structure containing parameters\n  affecting the preprocessing step.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-02974  \n  [`Self::command_buffer`] **must** not be a protected command buffer\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-pGeneratedCommandsInfo-02927  \n  [`Self::p_generated_commands_info`]`s `indirectCommandsLayout` **must** have\n  been created with the[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::EXPLICIT_PREPROCESS_NV`] bit\n  set\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-deviceGeneratedCommands-02928  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-pGeneratedCommandsInfo-parameter  \n  [`Self::p_generated_commands_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsInfoNV`] structure\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-renderpass  \n   This command **must** only be called outside of a render pass instance\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                 Outside                  |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdPreprocessGeneratedCommandsNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_generated_commands_info: *const crate::extensions::nv_device_generated_commands::GeneratedCommandsInfoNV) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindPipelineShaderGroupNV.html)) · Function <br/> vkCmdBindPipelineShaderGroupNV - Bind a pipeline object\n[](#_c_specification)C Specification\n----------\n\nFor pipelines that were created with the support of multiple shader groups\n(see [Graphics Pipeline Shader Groups](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#graphics-shadergroups)), the regular[`crate::vk::PFN_vkCmdBindPipeline`] command will bind Shader Group `0`.\nTo explicitly bind a shader group use:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdBindPipelineShaderGroupNV(\n    VkCommandBuffer                             commandBuffer,\n    VkPipelineBindPoint                         pipelineBindPoint,\n    VkPipeline                                  pipeline,\n    uint32_t                                    groupIndex);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer that the pipeline will be\n  bound to.\n\n* [`Self::pipeline_bind_point`] is a [`crate::vk::PipelineBindPoint`] value specifying\n  the bind point to which the pipeline will be bound.\n\n* [`Self::pipeline`] is the pipeline to be bound.\n\n* [`Self::group_index`] is the shader group to be bound.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-groupIndex-02893  \n  [`Self::group_index`] **must** be `0` or less than the effective[`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV::group_count`]including the referenced pipelines\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipelineBindPoint-02894  \n   The [`Self::pipeline_bind_point`] **must** be[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-groupIndex-02895  \n   The same restrictions as [`crate::vk::PFN_vkCmdBindPipeline`] apply as if the bound\n  pipeline was created only with the Shader Group from the[`Self::group_index`] information\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-deviceGeneratedCommands-02896  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::pipeline`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBindPipelineShaderGroupNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, pipeline_bind_point: crate::vk1_0::PipelineBindPoint, pipeline: crate::vk1_0::Pipeline, group_index: u32) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetGeneratedCommandsMemoryRequirementsNV.html)) · Function <br/> vkGetGeneratedCommandsMemoryRequirementsNV - Retrieve the buffer allocation requirements for generated commands\n[](#_c_specification)C Specification\n----------\n\nThe generation of commands on the device requires a `preprocess` buffer.\nTo retrieve the memory size and alignment requirements of a particular\nexecution state call:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkGetGeneratedCommandsMemoryRequirementsNV(\n    VkDevice                                    device,\n    const VkGeneratedCommandsMemoryRequirementsInfoNV* pInfo,\n    VkMemoryRequirements2*                      pMemoryRequirements);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the buffer.\n\n* [`Self::p_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`] structure containing\n  parameters required for the memory requirements query.\n\n* [`Self::p_memory_requirements`] is a pointer to a [`crate::vk::MemoryRequirements2`]structure in which the memory requirements of the buffer object are\n  returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-deviceGeneratedCommands-02906  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`] structure\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-pMemoryRequirements-parameter  \n  [`Self::p_memory_requirements`] **must** be a valid pointer to a [`crate::vk::MemoryRequirements2`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`], [`crate::vk::MemoryRequirements2`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetGeneratedCommandsMemoryRequirementsNV = unsafe extern "system" fn(device: crate::vk1_0::Device, p_info: *const crate::extensions::nv_device_generated_commands::GeneratedCommandsMemoryRequirementsInfoNV, p_memory_requirements: *mut crate::vk1_1::MemoryRequirements2) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateIndirectCommandsLayoutNV.html)) · Function <br/> vkCreateIndirectCommandsLayoutNV - Create an indirect command layout object\n[](#_c_specification)C Specification\n----------\n\nIndirect command layouts are created by:\n\n```\n// Provided by VK_NV_device_generated_commands\nVkResult vkCreateIndirectCommandsLayoutNV(\n    VkDevice                                    device,\n    const VkIndirectCommandsLayoutCreateInfoNV* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkIndirectCommandsLayoutNV*                 pIndirectCommandsLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that creates the indirect command\n  layout.\n\n* [`Self::p_create_info`] is a pointer to an instance of the[`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure containing\n  parameters affecting creation of the indirect command layout.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_indirect_commands_layout`] is a pointer to a[`crate::vk::IndirectCommandsLayoutNV`] handle in which the resulting indirect\n  command layout is returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-deviceGeneratedCommands-02929  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pIndirectCommandsLayout-parameter  \n  [`Self::p_indirect_commands_layout`] **must** be a valid pointer to a [`crate::vk::IndirectCommandsLayoutNV`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::IndirectCommandsLayoutCreateInfoNV`], [`crate::vk::IndirectCommandsLayoutNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateIndirectCommandsLayoutNV = unsafe extern "system" fn(device: crate::vk1_0::Device, p_create_info: *const crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutCreateInfoNV, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_indirect_commands_layout: *mut crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyIndirectCommandsLayoutNV.html)) · Function <br/> vkDestroyIndirectCommandsLayoutNV - Destroy an indirect commands layout\n[](#_c_specification)C Specification\n----------\n\nIndirect command layouts are destroyed by:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkDestroyIndirectCommandsLayoutNV(\n    VkDevice                                    device,\n    VkIndirectCommandsLayoutNV                  indirectCommandsLayout,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that destroys the layout.\n\n* [`Self::indirect_commands_layout`] is the layout to destroy.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02938  \n   All submitted commands that refer to [`Self::indirect_commands_layout`] **must**have completed execution\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02939  \n   If [`crate::vk::AllocationCallbacks`] were provided when[`Self::indirect_commands_layout`] was created, a compatible set of callbacks**must** be provided here\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02940  \n   If no [`crate::vk::AllocationCallbacks`] were provided when[`Self::indirect_commands_layout`] was created, [`Self::p_allocator`] **must** be`NULL`\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-deviceGeneratedCommands-02941  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-parameter  \n   If [`Self::indirect_commands_layout`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-parent  \n   If [`Self::indirect_commands_layout`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::indirect_commands_layout`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::IndirectCommandsLayoutNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyIndirectCommandsLayoutNV = unsafe extern "system" fn(device: crate::vk1_0::Device, indirect_commands_layout: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceDeviceGeneratedCommandsFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, GraphicsPipelineShaderGroupsCreateInfoNV> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'_>> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV - Structure describing the device-generated commands features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           deviceGeneratedCommands;\n} VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::device_generated_commands`]indicates whether the implementation supports functionality to generate\n  commands on the device.\n  See [Device-Generated Commands](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#device-generated-commands).\n\nIf the [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub device_generated_commands: crate::vk1_0::Bool32,
}
impl PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV;
}
impl Default for PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), device_generated_commands: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceDeviceGeneratedCommandsFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("device_generated_commands", &(self.device_generated_commands != 0)).finish()
    }
}
impl PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
        PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV.html)) · Builder of [`PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] <br/> VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV - Structure describing the device-generated commands features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           deviceGeneratedCommands;\n} VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::device_generated_commands`]indicates whether the implementation supports functionality to generate\n  commands on the device.\n  See [Device-Generated Commands](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#device-generated-commands).\n\nIf the [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a>(PhysicalDeviceDeviceGeneratedCommandsFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
        PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn device_generated_commands(mut self, device_generated_commands: bool) -> Self {
        self.0.device_generated_commands = device_generated_commands as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceDeviceGeneratedCommandsFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV.html)) · Structure <br/> VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV - Structure describing push descriptor limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxGraphicsShaderGroupCount;\n    uint32_t           maxIndirectSequenceCount;\n    uint32_t           maxIndirectCommandsTokenCount;\n    uint32_t           maxIndirectCommandsStreamCount;\n    uint32_t           maxIndirectCommandsTokenOffset;\n    uint32_t           maxIndirectCommandsStreamStride;\n    uint32_t           minSequencesCountBufferOffsetAlignment;\n    uint32_t           minSequencesIndexBufferOffsetAlignment;\n    uint32_t           minIndirectCommandsBufferOffsetAlignment;\n} VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_graphics_shader_group_count`] is the maximum number of shader groups\n  in [`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`].\n\n* [`Self::max_indirect_sequence_count`] is the maximum number of sequences in[`crate::vk::GeneratedCommandsInfoNV`] and in[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`].\n\n* `maxIndirectCommandsLayoutTokenCount` is the maximum number of\n  tokens in [`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* [`Self::max_indirect_commands_stream_count`] is the maximum number of streams in[`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* [`Self::max_indirect_commands_token_offset`] is the maximum offset in[`crate::vk::IndirectCommandsLayoutTokenNV`].\n\n* [`Self::max_indirect_commands_stream_stride`] is the maximum stream stride in[`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* `minSequenceCountBufferOffsetAlignment` is the minimum alignment for\n  memory addresses which **can** be used in [`crate::vk::GeneratedCommandsInfoNV`].\n\n* `minSequenceIndexBufferOffsetAlignment` is the minimum alignment for\n  memory addresses which **can** be used in [`crate::vk::GeneratedCommandsInfoNV`].\n\n* [`Self::min_indirect_commands_buffer_offset_alignment`] is the minimum alignment\n  for memory addresses used in [`crate::vk::IndirectCommandsStreamNV`], and as\n  preprocess buffer in [`crate::vk::GeneratedCommandsInfoNV`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_graphics_shader_group_count: u32,
    pub max_indirect_sequence_count: u32,
    pub max_indirect_commands_token_count: u32,
    pub max_indirect_commands_stream_count: u32,
    pub max_indirect_commands_token_offset: u32,
    pub max_indirect_commands_stream_stride: u32,
    pub min_sequences_count_buffer_offset_alignment: u32,
    pub min_sequences_index_buffer_offset_alignment: u32,
    pub min_indirect_commands_buffer_offset_alignment: u32,
}
impl PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV;
}
impl Default for PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_graphics_shader_group_count: Default::default(), max_indirect_sequence_count: Default::default(), max_indirect_commands_token_count: Default::default(), max_indirect_commands_stream_count: Default::default(), max_indirect_commands_token_offset: Default::default(), max_indirect_commands_stream_stride: Default::default(), min_sequences_count_buffer_offset_alignment: Default::default(), min_sequences_index_buffer_offset_alignment: Default::default(), min_indirect_commands_buffer_offset_alignment: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceDeviceGeneratedCommandsPropertiesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_graphics_shader_group_count", &self.max_graphics_shader_group_count).field("max_indirect_sequence_count", &self.max_indirect_sequence_count).field("max_indirect_commands_token_count", &self.max_indirect_commands_token_count).field("max_indirect_commands_stream_count", &self.max_indirect_commands_stream_count).field("max_indirect_commands_token_offset", &self.max_indirect_commands_token_offset).field("max_indirect_commands_stream_stride", &self.max_indirect_commands_stream_stride).field("min_sequences_count_buffer_offset_alignment", &self.min_sequences_count_buffer_offset_alignment).field("min_sequences_index_buffer_offset_alignment", &self.min_sequences_index_buffer_offset_alignment).field("min_indirect_commands_buffer_offset_alignment", &self.min_indirect_commands_buffer_offset_alignment).finish()
    }
}
impl PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
        PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV.html)) · Builder of [`PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`] <br/> VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV - Structure describing push descriptor limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxGraphicsShaderGroupCount;\n    uint32_t           maxIndirectSequenceCount;\n    uint32_t           maxIndirectCommandsTokenCount;\n    uint32_t           maxIndirectCommandsStreamCount;\n    uint32_t           maxIndirectCommandsTokenOffset;\n    uint32_t           maxIndirectCommandsStreamStride;\n    uint32_t           minSequencesCountBufferOffsetAlignment;\n    uint32_t           minSequencesIndexBufferOffsetAlignment;\n    uint32_t           minIndirectCommandsBufferOffsetAlignment;\n} VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_graphics_shader_group_count`] is the maximum number of shader groups\n  in [`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`].\n\n* [`Self::max_indirect_sequence_count`] is the maximum number of sequences in[`crate::vk::GeneratedCommandsInfoNV`] and in[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`].\n\n* `maxIndirectCommandsLayoutTokenCount` is the maximum number of\n  tokens in [`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* [`Self::max_indirect_commands_stream_count`] is the maximum number of streams in[`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* [`Self::max_indirect_commands_token_offset`] is the maximum offset in[`crate::vk::IndirectCommandsLayoutTokenNV`].\n\n* [`Self::max_indirect_commands_stream_stride`] is the maximum stream stride in[`crate::vk::IndirectCommandsLayoutCreateInfoNV`].\n\n* `minSequenceCountBufferOffsetAlignment` is the minimum alignment for\n  memory addresses which **can** be used in [`crate::vk::GeneratedCommandsInfoNV`].\n\n* `minSequenceIndexBufferOffsetAlignment` is the minimum alignment for\n  memory addresses which **can** be used in [`crate::vk::GeneratedCommandsInfoNV`].\n\n* [`Self::min_indirect_commands_buffer_offset_alignment`] is the minimum alignment\n  for memory addresses used in [`crate::vk::IndirectCommandsStreamNV`], and as\n  preprocess buffer in [`crate::vk::GeneratedCommandsInfoNV`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a>(PhysicalDeviceDeviceGeneratedCommandsPropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
        PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_graphics_shader_group_count(mut self, max_graphics_shader_group_count: u32) -> Self {
        self.0.max_graphics_shader_group_count = max_graphics_shader_group_count as _;
        self
    }
    #[inline]
    pub fn max_indirect_sequence_count(mut self, max_indirect_sequence_count: u32) -> Self {
        self.0.max_indirect_sequence_count = max_indirect_sequence_count as _;
        self
    }
    #[inline]
    pub fn max_indirect_commands_token_count(mut self, max_indirect_commands_token_count: u32) -> Self {
        self.0.max_indirect_commands_token_count = max_indirect_commands_token_count as _;
        self
    }
    #[inline]
    pub fn max_indirect_commands_stream_count(mut self, max_indirect_commands_stream_count: u32) -> Self {
        self.0.max_indirect_commands_stream_count = max_indirect_commands_stream_count as _;
        self
    }
    #[inline]
    pub fn max_indirect_commands_token_offset(mut self, max_indirect_commands_token_offset: u32) -> Self {
        self.0.max_indirect_commands_token_offset = max_indirect_commands_token_offset as _;
        self
    }
    #[inline]
    pub fn max_indirect_commands_stream_stride(mut self, max_indirect_commands_stream_stride: u32) -> Self {
        self.0.max_indirect_commands_stream_stride = max_indirect_commands_stream_stride as _;
        self
    }
    #[inline]
    pub fn min_sequences_count_buffer_offset_alignment(mut self, min_sequences_count_buffer_offset_alignment: u32) -> Self {
        self.0.min_sequences_count_buffer_offset_alignment = min_sequences_count_buffer_offset_alignment as _;
        self
    }
    #[inline]
    pub fn min_sequences_index_buffer_offset_alignment(mut self, min_sequences_index_buffer_offset_alignment: u32) -> Self {
        self.0.min_sequences_index_buffer_offset_alignment = min_sequences_index_buffer_offset_alignment as _;
        self
    }
    #[inline]
    pub fn min_indirect_commands_buffer_offset_alignment(mut self, min_indirect_commands_buffer_offset_alignment: u32) -> Self {
        self.0.min_indirect_commands_buffer_offset_alignment = min_indirect_commands_buffer_offset_alignment as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
    fn default() -> PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
    type Target = PhysicalDeviceDeviceGeneratedCommandsPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGraphicsShaderGroupCreateInfoNV.html)) · Structure <br/> VkGraphicsShaderGroupCreateInfoNV - Structure specifying override parameters for each shader group\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::GraphicsShaderGroupCreateInfoNV`] structure provides the state\noverrides for each shader group.\nEach shader group behaves like a pipeline that was created from its state as\nwell as the remaining parent’s state.\nIt is defined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGraphicsShaderGroupCreateInfoNV {\n    VkStructureType                                 sType;\n    const void*                                     pNext;\n    uint32_t                                        stageCount;\n    const VkPipelineShaderStageCreateInfo*          pStages;\n    const VkPipelineVertexInputStateCreateInfo*     pVertexInputState;\n    const VkPipelineTessellationStateCreateInfo*    pTessellationState;\n} VkGraphicsShaderGroupCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stage_count`] is the number of entries in the [`Self::p_stages`] array.\n\n* [`Self::p_stages`] is a pointer to an array[`crate::vk::PipelineShaderStageCreateInfo`] structures specifying the set of\n  the shader stages to be included in this shader group.\n\n* [`Self::p_vertex_input_state`] is a pointer to a[`crate::vk::PipelineVertexInputStateCreateInfo`] structure.\n\n* [`Self::p_tessellation_state`] is a pointer to a[`crate::vk::PipelineTessellationStateCreateInfo`] structure, and is ignored if\n  the shader group does not include a tessellation control shader stage\n  and tessellation evaluation shader stage.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-stageCount-02888  \n   For [`Self::stage_count`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::stage_count`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pStages-02889  \n   For [`Self::p_stages`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_stages`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pVertexInputState-02890  \n   For [`Self::p_vertex_input_state`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_vertex_input_state`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pTessellationState-02891  \n   For [`Self::p_tessellation_state`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_tessellation_state`] apply\n\nValid Usage (Implicit)\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GRAPHICS_SHADER_GROUP_CREATE_INFO_NV`]\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pStages-parameter  \n  [`Self::p_stages`] **must** be a valid pointer to an array of [`Self::stage_count`] valid [`crate::vk::PipelineShaderStageCreateInfo`] structures\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-stageCount-arraylength  \n  [`Self::stage_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`], [`crate::vk::PipelineShaderStageCreateInfo`], [`crate::vk::PipelineTessellationStateCreateInfo`], [`crate::vk::PipelineVertexInputStateCreateInfo`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkGraphicsShaderGroupCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GraphicsShaderGroupCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub stage_count: u32,
    pub p_stages: *const crate::vk1_0::PipelineShaderStageCreateInfo,
    pub p_vertex_input_state: *const crate::vk1_0::PipelineVertexInputStateCreateInfo,
    pub p_tessellation_state: *const crate::vk1_0::PipelineTessellationStateCreateInfo,
}
impl GraphicsShaderGroupCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::GRAPHICS_SHADER_GROUP_CREATE_INFO_NV;
}
impl Default for GraphicsShaderGroupCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), stage_count: Default::default(), p_stages: std::ptr::null(), p_vertex_input_state: std::ptr::null(), p_tessellation_state: std::ptr::null() }
    }
}
impl std::fmt::Debug for GraphicsShaderGroupCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GraphicsShaderGroupCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("stage_count", &self.stage_count).field("p_stages", &self.p_stages).field("p_vertex_input_state", &self.p_vertex_input_state).field("p_tessellation_state", &self.p_tessellation_state).finish()
    }
}
impl GraphicsShaderGroupCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GraphicsShaderGroupCreateInfoNVBuilder<'a> {
        GraphicsShaderGroupCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGraphicsShaderGroupCreateInfoNV.html)) · Builder of [`GraphicsShaderGroupCreateInfoNV`] <br/> VkGraphicsShaderGroupCreateInfoNV - Structure specifying override parameters for each shader group\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::GraphicsShaderGroupCreateInfoNV`] structure provides the state\noverrides for each shader group.\nEach shader group behaves like a pipeline that was created from its state as\nwell as the remaining parent’s state.\nIt is defined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGraphicsShaderGroupCreateInfoNV {\n    VkStructureType                                 sType;\n    const void*                                     pNext;\n    uint32_t                                        stageCount;\n    const VkPipelineShaderStageCreateInfo*          pStages;\n    const VkPipelineVertexInputStateCreateInfo*     pVertexInputState;\n    const VkPipelineTessellationStateCreateInfo*    pTessellationState;\n} VkGraphicsShaderGroupCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stage_count`] is the number of entries in the [`Self::p_stages`] array.\n\n* [`Self::p_stages`] is a pointer to an array[`crate::vk::PipelineShaderStageCreateInfo`] structures specifying the set of\n  the shader stages to be included in this shader group.\n\n* [`Self::p_vertex_input_state`] is a pointer to a[`crate::vk::PipelineVertexInputStateCreateInfo`] structure.\n\n* [`Self::p_tessellation_state`] is a pointer to a[`crate::vk::PipelineTessellationStateCreateInfo`] structure, and is ignored if\n  the shader group does not include a tessellation control shader stage\n  and tessellation evaluation shader stage.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-stageCount-02888  \n   For [`Self::stage_count`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::stage_count`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pStages-02889  \n   For [`Self::p_stages`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_stages`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pVertexInputState-02890  \n   For [`Self::p_vertex_input_state`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_vertex_input_state`] apply\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pTessellationState-02891  \n   For [`Self::p_tessellation_state`], the same restrictions as in[`crate::vk::GraphicsPipelineCreateInfo::p_tessellation_state`] apply\n\nValid Usage (Implicit)\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GRAPHICS_SHADER_GROUP_CREATE_INFO_NV`]\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-pStages-parameter  \n  [`Self::p_stages`] **must** be a valid pointer to an array of [`Self::stage_count`] valid [`crate::vk::PipelineShaderStageCreateInfo`] structures\n\n* []() VUID-VkGraphicsShaderGroupCreateInfoNV-stageCount-arraylength  \n  [`Self::stage_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`], [`crate::vk::PipelineShaderStageCreateInfo`], [`crate::vk::PipelineTessellationStateCreateInfo`], [`crate::vk::PipelineVertexInputStateCreateInfo`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct GraphicsShaderGroupCreateInfoNVBuilder<'a>(GraphicsShaderGroupCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> GraphicsShaderGroupCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> GraphicsShaderGroupCreateInfoNVBuilder<'a> {
        GraphicsShaderGroupCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn stages(mut self, stages: &'a [crate::vk1_0::PipelineShaderStageCreateInfoBuilder]) -> Self {
        self.0.p_stages = stages.as_ptr() as _;
        self.0.stage_count = stages.len() as _;
        self
    }
    #[inline]
    pub fn vertex_input_state(mut self, vertex_input_state: &'a crate::vk1_0::PipelineVertexInputStateCreateInfo) -> Self {
        self.0.p_vertex_input_state = vertex_input_state as _;
        self
    }
    #[inline]
    pub fn tessellation_state(mut self, tessellation_state: &'a crate::vk1_0::PipelineTessellationStateCreateInfo) -> Self {
        self.0.p_tessellation_state = tessellation_state as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GraphicsShaderGroupCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for GraphicsShaderGroupCreateInfoNVBuilder<'a> {
    fn default() -> GraphicsShaderGroupCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GraphicsShaderGroupCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GraphicsShaderGroupCreateInfoNVBuilder<'a> {
    type Target = GraphicsShaderGroupCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GraphicsShaderGroupCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGraphicsPipelineShaderGroupsCreateInfoNV.html)) · Structure <br/> VkGraphicsPipelineShaderGroupsCreateInfoNV - Structure specifying parameters of a newly created multi shader group pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGraphicsPipelineShaderGroupsCreateInfoNV {\n    VkStructureType                             sType;\n    const void*                                 pNext;\n    uint32_t                                    groupCount;\n    const VkGraphicsShaderGroupCreateInfoNV*    pGroups;\n    uint32_t                                    pipelineCount;\n    const VkPipeline*                           pPipelines;\n} VkGraphicsPipelineShaderGroupsCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::group_count`] is the number of elements in the [`Self::p_groups`] array.\n\n* [`Self::p_groups`] is a pointer to an array of[`crate::vk::GraphicsShaderGroupCreateInfoNV`] structures specifying which\n  state of the original [`crate::vk::GraphicsPipelineCreateInfo`] each shader\n  group overrides.\n\n* [`Self::pipeline_count`] is the number of elements in the [`Self::p_pipelines`]array.\n\n* [`Self::p_pipelines`] is a pointer to an array of graphics [`crate::vk::Pipeline`]structures which are referenced within the created pipeline, including\n  all their shader groups.\n[](#_description)Description\n----------\n\nWhen referencing shader groups by index, groups defined in the referenced\npipelines are treated as if they were defined as additional entries in[`Self::p_groups`].\nThey are appended in the order they appear in the [`Self::p_pipelines`] array and\nin the [`Self::p_groups`] array when those pipelines were defined.\n\nThe application **must** maintain the lifetime of all such referenced pipelines\nbased on the pipelines that make use of them.\n\nValid Usage\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-02879  \n  [`Self::group_count`] **must** be at least `1` and as maximum[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxGraphicsShaderGroupCount`\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-02880  \n   The sum of [`Self::group_count`] including those groups added from referenced[`Self::p_pipelines`] **must** also be as maximum[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxGraphicsShaderGroupCount`\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02881  \n   The state of the first element of [`Self::p_groups`] **must** match its\n  equivalent within the parent’s [`crate::vk::GraphicsPipelineCreateInfo`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02882  \n   Each element of [`Self::p_groups`] **must** in combination with the rest of the\n  pipeline state yield a valid state configuration\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02884  \n   All elements of [`Self::p_groups`] **must** use the same shader stage\n  combinations unless any mesh shader stage is used, then either\n  combination of task and mesh or just mesh shader is valid\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02885  \n   Mesh and regular primitive shading stages cannot be mixed across[`Self::p_groups`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pPipelines-02886  \n   Each element of [`Self::p_pipelines`] **must** have been created with identical\n  state to the pipeline currently created except the state that can be\n  overridden by [`crate::vk::GraphicsShaderGroupCreateInfoNV`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-deviceGeneratedCommands-02887  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-parameter  \n  [`Self::p_groups`] **must** be a valid pointer to an array of [`Self::group_count`] valid [`crate::vk::GraphicsShaderGroupCreateInfoNV`] structures\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pPipelines-parameter  \n   If [`Self::pipeline_count`] is not `0`, [`Self::p_pipelines`] **must** be a valid pointer to an array of [`Self::pipeline_count`] valid [`crate::vk::Pipeline`] handles\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-arraylength  \n  [`Self::group_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::GraphicsShaderGroupCreateInfoNV`], [`crate::vk::Pipeline`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkGraphicsPipelineShaderGroupsCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GraphicsPipelineShaderGroupsCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub group_count: u32,
    pub p_groups: *const crate::extensions::nv_device_generated_commands::GraphicsShaderGroupCreateInfoNV,
    pub pipeline_count: u32,
    pub p_pipelines: *const crate::vk1_0::Pipeline,
}
impl GraphicsPipelineShaderGroupsCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV;
}
impl Default for GraphicsPipelineShaderGroupsCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), group_count: Default::default(), p_groups: std::ptr::null(), pipeline_count: Default::default(), p_pipelines: std::ptr::null() }
    }
}
impl std::fmt::Debug for GraphicsPipelineShaderGroupsCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GraphicsPipelineShaderGroupsCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("group_count", &self.group_count).field("p_groups", &self.p_groups).field("pipeline_count", &self.pipeline_count).field("p_pipelines", &self.p_pipelines).finish()
    }
}
impl GraphicsPipelineShaderGroupsCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
        GraphicsPipelineShaderGroupsCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGraphicsPipelineShaderGroupsCreateInfoNV.html)) · Builder of [`GraphicsPipelineShaderGroupsCreateInfoNV`] <br/> VkGraphicsPipelineShaderGroupsCreateInfoNV - Structure specifying parameters of a newly created multi shader group pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGraphicsPipelineShaderGroupsCreateInfoNV {\n    VkStructureType                             sType;\n    const void*                                 pNext;\n    uint32_t                                    groupCount;\n    const VkGraphicsShaderGroupCreateInfoNV*    pGroups;\n    uint32_t                                    pipelineCount;\n    const VkPipeline*                           pPipelines;\n} VkGraphicsPipelineShaderGroupsCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::group_count`] is the number of elements in the [`Self::p_groups`] array.\n\n* [`Self::p_groups`] is a pointer to an array of[`crate::vk::GraphicsShaderGroupCreateInfoNV`] structures specifying which\n  state of the original [`crate::vk::GraphicsPipelineCreateInfo`] each shader\n  group overrides.\n\n* [`Self::pipeline_count`] is the number of elements in the [`Self::p_pipelines`]array.\n\n* [`Self::p_pipelines`] is a pointer to an array of graphics [`crate::vk::Pipeline`]structures which are referenced within the created pipeline, including\n  all their shader groups.\n[](#_description)Description\n----------\n\nWhen referencing shader groups by index, groups defined in the referenced\npipelines are treated as if they were defined as additional entries in[`Self::p_groups`].\nThey are appended in the order they appear in the [`Self::p_pipelines`] array and\nin the [`Self::p_groups`] array when those pipelines were defined.\n\nThe application **must** maintain the lifetime of all such referenced pipelines\nbased on the pipelines that make use of them.\n\nValid Usage\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-02879  \n  [`Self::group_count`] **must** be at least `1` and as maximum[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxGraphicsShaderGroupCount`\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-02880  \n   The sum of [`Self::group_count`] including those groups added from referenced[`Self::p_pipelines`] **must** also be as maximum[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxGraphicsShaderGroupCount`\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02881  \n   The state of the first element of [`Self::p_groups`] **must** match its\n  equivalent within the parent’s [`crate::vk::GraphicsPipelineCreateInfo`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02882  \n   Each element of [`Self::p_groups`] **must** in combination with the rest of the\n  pipeline state yield a valid state configuration\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02884  \n   All elements of [`Self::p_groups`] **must** use the same shader stage\n  combinations unless any mesh shader stage is used, then either\n  combination of task and mesh or just mesh shader is valid\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-02885  \n   Mesh and regular primitive shading stages cannot be mixed across[`Self::p_groups`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pPipelines-02886  \n   Each element of [`Self::p_pipelines`] **must** have been created with identical\n  state to the pipeline currently created except the state that can be\n  overridden by [`crate::vk::GraphicsShaderGroupCreateInfoNV`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-deviceGeneratedCommands-02887  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV`]\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pGroups-parameter  \n  [`Self::p_groups`] **must** be a valid pointer to an array of [`Self::group_count`] valid [`crate::vk::GraphicsShaderGroupCreateInfoNV`] structures\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-pPipelines-parameter  \n   If [`Self::pipeline_count`] is not `0`, [`Self::p_pipelines`] **must** be a valid pointer to an array of [`Self::pipeline_count`] valid [`crate::vk::Pipeline`] handles\n\n* []() VUID-VkGraphicsPipelineShaderGroupsCreateInfoNV-groupCount-arraylength  \n  [`Self::group_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::GraphicsShaderGroupCreateInfoNV`], [`crate::vk::Pipeline`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a>(GraphicsPipelineShaderGroupsCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
        GraphicsPipelineShaderGroupsCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn groups(mut self, groups: &'a [crate::extensions::nv_device_generated_commands::GraphicsShaderGroupCreateInfoNVBuilder]) -> Self {
        self.0.p_groups = groups.as_ptr() as _;
        self.0.group_count = groups.len() as _;
        self
    }
    #[inline]
    pub fn pipelines(mut self, pipelines: &'a [crate::vk1_0::Pipeline]) -> Self {
        self.0.p_pipelines = pipelines.as_ptr() as _;
        self.0.pipeline_count = pipelines.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GraphicsPipelineShaderGroupsCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
    fn default() -> GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
    type Target = GraphicsPipelineShaderGroupsCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GraphicsPipelineShaderGroupsCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindShaderGroupIndirectCommandNV.html)) · Structure <br/> VkBindShaderGroupIndirectCommandNV - Structure specifying input data for a single shader group command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindShaderGroupIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindShaderGroupIndirectCommandNV {\n    uint32_t    groupIndex;\n} VkBindShaderGroupIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* `index` specifies which shader group of the current bound graphics\n  pipeline is used.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindShaderGroupIndirectCommandNV-None-02944  \n   The current bound graphics pipeline, as well as the pipelines it may\n  reference, **must** have been created with[`crate::vk::PipelineCreateFlagBits::INDIRECT_BINDABLE_NV`]\n\n* []() VUID-VkBindShaderGroupIndirectCommandNV-index-02945  \n   The `index` **must** be within range of the accessible shader groups of\n  the current bound graphics pipeline.\n  See [`crate::vk::DeviceLoader::cmd_bind_pipeline_shader_group_nv`] for further details\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[doc(alias = "VkBindShaderGroupIndirectCommandNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct BindShaderGroupIndirectCommandNV {
    pub group_index: u32,
}
impl Default for BindShaderGroupIndirectCommandNV {
    fn default() -> Self {
        Self { group_index: Default::default() }
    }
}
impl std::fmt::Debug for BindShaderGroupIndirectCommandNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("BindShaderGroupIndirectCommandNV").field("group_index", &self.group_index).finish()
    }
}
impl BindShaderGroupIndirectCommandNV {
    #[inline]
    pub fn into_builder<'a>(self) -> BindShaderGroupIndirectCommandNVBuilder<'a> {
        BindShaderGroupIndirectCommandNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindShaderGroupIndirectCommandNV.html)) · Builder of [`BindShaderGroupIndirectCommandNV`] <br/> VkBindShaderGroupIndirectCommandNV - Structure specifying input data for a single shader group command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindShaderGroupIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindShaderGroupIndirectCommandNV {\n    uint32_t    groupIndex;\n} VkBindShaderGroupIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* `index` specifies which shader group of the current bound graphics\n  pipeline is used.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindShaderGroupIndirectCommandNV-None-02944  \n   The current bound graphics pipeline, as well as the pipelines it may\n  reference, **must** have been created with[`crate::vk::PipelineCreateFlagBits::INDIRECT_BINDABLE_NV`]\n\n* []() VUID-VkBindShaderGroupIndirectCommandNV-index-02945  \n   The `index` **must** be within range of the accessible shader groups of\n  the current bound graphics pipeline.\n  See [`crate::vk::DeviceLoader::cmd_bind_pipeline_shader_group_nv`] for further details\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[repr(transparent)]
pub struct BindShaderGroupIndirectCommandNVBuilder<'a>(BindShaderGroupIndirectCommandNV, std::marker::PhantomData<&'a ()>);
impl<'a> BindShaderGroupIndirectCommandNVBuilder<'a> {
    #[inline]
    pub fn new() -> BindShaderGroupIndirectCommandNVBuilder<'a> {
        BindShaderGroupIndirectCommandNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn group_index(mut self, group_index: u32) -> Self {
        self.0.group_index = group_index as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> BindShaderGroupIndirectCommandNV {
        self.0
    }
}
impl<'a> std::default::Default for BindShaderGroupIndirectCommandNVBuilder<'a> {
    fn default() -> BindShaderGroupIndirectCommandNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for BindShaderGroupIndirectCommandNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for BindShaderGroupIndirectCommandNVBuilder<'a> {
    type Target = BindShaderGroupIndirectCommandNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for BindShaderGroupIndirectCommandNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindIndexBufferIndirectCommandNV.html)) · Structure <br/> VkBindIndexBufferIndirectCommandNV - Structure specifying input data for a single index buffer command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindIndexBufferIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::INDEX_BUFFER_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindIndexBufferIndirectCommandNV {\n    VkDeviceAddress    bufferAddress;\n    uint32_t           size;\n    VkIndexType        indexType;\n} VkBindIndexBufferIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer_address`] specifies a physical address of the [`crate::vk::Buffer`]used as index buffer.\n\n* [`Self::size`] is the byte size range which is available for this operation\n  from the provided address.\n\n* [`Self::index_type`] is a [`crate::vk::IndexType`] value specifying how indices are\n  treated.\n  Instead of the Vulkan enum values, a custom `uint32_t` value **can** be\n  mapped to an [`crate::vk::IndexType`] by specifying the[`crate::vk::IndirectCommandsLayoutTokenNV`]::`pIndexTypes` and[`crate::vk::IndirectCommandsLayoutTokenNV`]::`pIndexTypeValues` arrays.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-None-02946  \n   The buffer’s usage flag from which the address was acquired **must** have\n  the [`crate::vk::BufferUsageFlagBits::INDEX_BUFFER`] bit set\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-bufferAddress-02947  \n   The [`Self::buffer_address`] **must** be aligned to the [`Self::index_type`] used\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-None-02948  \n   Each element of the buffer from which the address was acquired and that\n  is non-sparse **must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-indexType-parameter  \n  [`Self::index_type`] **must** be a valid [`crate::vk::IndexType`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::IndexType`]\n"]
#[doc(alias = "VkBindIndexBufferIndirectCommandNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct BindIndexBufferIndirectCommandNV {
    pub buffer_address: crate::vk1_0::DeviceAddress,
    pub size: u32,
    pub index_type: crate::vk1_0::IndexType,
}
impl Default for BindIndexBufferIndirectCommandNV {
    fn default() -> Self {
        Self { buffer_address: Default::default(), size: Default::default(), index_type: Default::default() }
    }
}
impl std::fmt::Debug for BindIndexBufferIndirectCommandNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("BindIndexBufferIndirectCommandNV").field("buffer_address", &self.buffer_address).field("size", &self.size).field("index_type", &self.index_type).finish()
    }
}
impl BindIndexBufferIndirectCommandNV {
    #[inline]
    pub fn into_builder<'a>(self) -> BindIndexBufferIndirectCommandNVBuilder<'a> {
        BindIndexBufferIndirectCommandNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindIndexBufferIndirectCommandNV.html)) · Builder of [`BindIndexBufferIndirectCommandNV`] <br/> VkBindIndexBufferIndirectCommandNV - Structure specifying input data for a single index buffer command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindIndexBufferIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::INDEX_BUFFER_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindIndexBufferIndirectCommandNV {\n    VkDeviceAddress    bufferAddress;\n    uint32_t           size;\n    VkIndexType        indexType;\n} VkBindIndexBufferIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer_address`] specifies a physical address of the [`crate::vk::Buffer`]used as index buffer.\n\n* [`Self::size`] is the byte size range which is available for this operation\n  from the provided address.\n\n* [`Self::index_type`] is a [`crate::vk::IndexType`] value specifying how indices are\n  treated.\n  Instead of the Vulkan enum values, a custom `uint32_t` value **can** be\n  mapped to an [`crate::vk::IndexType`] by specifying the[`crate::vk::IndirectCommandsLayoutTokenNV`]::`pIndexTypes` and[`crate::vk::IndirectCommandsLayoutTokenNV`]::`pIndexTypeValues` arrays.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-None-02946  \n   The buffer’s usage flag from which the address was acquired **must** have\n  the [`crate::vk::BufferUsageFlagBits::INDEX_BUFFER`] bit set\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-bufferAddress-02947  \n   The [`Self::buffer_address`] **must** be aligned to the [`Self::index_type`] used\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-None-02948  \n   Each element of the buffer from which the address was acquired and that\n  is non-sparse **must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkBindIndexBufferIndirectCommandNV-indexType-parameter  \n  [`Self::index_type`] **must** be a valid [`crate::vk::IndexType`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::IndexType`]\n"]
#[repr(transparent)]
pub struct BindIndexBufferIndirectCommandNVBuilder<'a>(BindIndexBufferIndirectCommandNV, std::marker::PhantomData<&'a ()>);
impl<'a> BindIndexBufferIndirectCommandNVBuilder<'a> {
    #[inline]
    pub fn new() -> BindIndexBufferIndirectCommandNVBuilder<'a> {
        BindIndexBufferIndirectCommandNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer_address(mut self, buffer_address: crate::vk1_0::DeviceAddress) -> Self {
        self.0.buffer_address = buffer_address as _;
        self
    }
    #[inline]
    pub fn size(mut self, size: u32) -> Self {
        self.0.size = size as _;
        self
    }
    #[inline]
    pub fn index_type(mut self, index_type: crate::vk1_0::IndexType) -> Self {
        self.0.index_type = index_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> BindIndexBufferIndirectCommandNV {
        self.0
    }
}
impl<'a> std::default::Default for BindIndexBufferIndirectCommandNVBuilder<'a> {
    fn default() -> BindIndexBufferIndirectCommandNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for BindIndexBufferIndirectCommandNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for BindIndexBufferIndirectCommandNVBuilder<'a> {
    type Target = BindIndexBufferIndirectCommandNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for BindIndexBufferIndirectCommandNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindVertexBufferIndirectCommandNV.html)) · Structure <br/> VkBindVertexBufferIndirectCommandNV - Structure specifying input data for a single vertex buffer command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindVertexBufferIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::VERTEX_BUFFER_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindVertexBufferIndirectCommandNV {\n    VkDeviceAddress    bufferAddress;\n    uint32_t           size;\n    uint32_t           stride;\n} VkBindVertexBufferIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer_address`] specifies a physical address of the [`crate::vk::Buffer`]used as vertex input binding.\n\n* [`Self::size`] is the byte size range which is available for this operation\n  from the provided address.\n\n* [`Self::stride`] is the byte size stride for this vertex input binding as in[`crate::vk::VertexInputBindingDescription`]::[`Self::stride`].\n  It is only used if[`crate::vk::IndirectCommandsLayoutTokenNV`]::`vertexDynamicStride` was\n  set, otherwise the stride is inherited from the current bound graphics\n  pipeline.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindVertexBufferIndirectCommandNV-None-02949  \n   The buffer’s usage flag from which the address was acquired **must** have\n  the [`crate::vk::BufferUsageFlagBits::VERTEX_BUFFER`] bit set\n\n* []() VUID-VkBindVertexBufferIndirectCommandNV-None-02950  \n   Each element of the buffer from which the address was acquired and that\n  is non-sparse **must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`]\n"]
#[doc(alias = "VkBindVertexBufferIndirectCommandNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct BindVertexBufferIndirectCommandNV {
    pub buffer_address: crate::vk1_0::DeviceAddress,
    pub size: u32,
    pub stride: u32,
}
impl Default for BindVertexBufferIndirectCommandNV {
    fn default() -> Self {
        Self { buffer_address: Default::default(), size: Default::default(), stride: Default::default() }
    }
}
impl std::fmt::Debug for BindVertexBufferIndirectCommandNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("BindVertexBufferIndirectCommandNV").field("buffer_address", &self.buffer_address).field("size", &self.size).field("stride", &self.stride).finish()
    }
}
impl BindVertexBufferIndirectCommandNV {
    #[inline]
    pub fn into_builder<'a>(self) -> BindVertexBufferIndirectCommandNVBuilder<'a> {
        BindVertexBufferIndirectCommandNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBindVertexBufferIndirectCommandNV.html)) · Builder of [`BindVertexBufferIndirectCommandNV`] <br/> VkBindVertexBufferIndirectCommandNV - Structure specifying input data for a single vertex buffer command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BindVertexBufferIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::VERTEX_BUFFER_NV`] token.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkBindVertexBufferIndirectCommandNV {\n    VkDeviceAddress    bufferAddress;\n    uint32_t           size;\n    uint32_t           stride;\n} VkBindVertexBufferIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer_address`] specifies a physical address of the [`crate::vk::Buffer`]used as vertex input binding.\n\n* [`Self::size`] is the byte size range which is available for this operation\n  from the provided address.\n\n* [`Self::stride`] is the byte size stride for this vertex input binding as in[`crate::vk::VertexInputBindingDescription`]::[`Self::stride`].\n  It is only used if[`crate::vk::IndirectCommandsLayoutTokenNV`]::`vertexDynamicStride` was\n  set, otherwise the stride is inherited from the current bound graphics\n  pipeline.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBindVertexBufferIndirectCommandNV-None-02949  \n   The buffer’s usage flag from which the address was acquired **must** have\n  the [`crate::vk::BufferUsageFlagBits::VERTEX_BUFFER`] bit set\n\n* []() VUID-VkBindVertexBufferIndirectCommandNV-None-02950  \n   Each element of the buffer from which the address was acquired and that\n  is non-sparse **must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`]\n"]
#[repr(transparent)]
pub struct BindVertexBufferIndirectCommandNVBuilder<'a>(BindVertexBufferIndirectCommandNV, std::marker::PhantomData<&'a ()>);
impl<'a> BindVertexBufferIndirectCommandNVBuilder<'a> {
    #[inline]
    pub fn new() -> BindVertexBufferIndirectCommandNVBuilder<'a> {
        BindVertexBufferIndirectCommandNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer_address(mut self, buffer_address: crate::vk1_0::DeviceAddress) -> Self {
        self.0.buffer_address = buffer_address as _;
        self
    }
    #[inline]
    pub fn size(mut self, size: u32) -> Self {
        self.0.size = size as _;
        self
    }
    #[inline]
    pub fn stride(mut self, stride: u32) -> Self {
        self.0.stride = stride as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> BindVertexBufferIndirectCommandNV {
        self.0
    }
}
impl<'a> std::default::Default for BindVertexBufferIndirectCommandNVBuilder<'a> {
    fn default() -> BindVertexBufferIndirectCommandNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for BindVertexBufferIndirectCommandNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for BindVertexBufferIndirectCommandNVBuilder<'a> {
    type Target = BindVertexBufferIndirectCommandNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for BindVertexBufferIndirectCommandNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSetStateFlagsIndirectCommandNV.html)) · Structure <br/> VkSetStateFlagsIndirectCommandNV - Structure specifying input data for a single state flag command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SetStateFlagsIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`] token.\nWhich state is changed depends on the [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html)specified at [`crate::vk::IndirectCommandsLayoutNV`] creation time.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkSetStateFlagsIndirectCommandNV {\n    uint32_t    data;\n} VkSetStateFlagsIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::data`] encodes packed state that this command alters.\n\n  * Bit `0`: If set represents [`crate::vk::FrontFace::CLOCKWISE`], otherwise[`crate::vk::FrontFace::COUNTER_CLOCKWISE`]\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[doc(alias = "VkSetStateFlagsIndirectCommandNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SetStateFlagsIndirectCommandNV {
    pub data: u32,
}
impl Default for SetStateFlagsIndirectCommandNV {
    fn default() -> Self {
        Self { data: Default::default() }
    }
}
impl std::fmt::Debug for SetStateFlagsIndirectCommandNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SetStateFlagsIndirectCommandNV").field("data", &self.data).finish()
    }
}
impl SetStateFlagsIndirectCommandNV {
    #[inline]
    pub fn into_builder<'a>(self) -> SetStateFlagsIndirectCommandNVBuilder<'a> {
        SetStateFlagsIndirectCommandNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSetStateFlagsIndirectCommandNV.html)) · Builder of [`SetStateFlagsIndirectCommandNV`] <br/> VkSetStateFlagsIndirectCommandNV - Structure specifying input data for a single state flag command token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SetStateFlagsIndirectCommandNV`] structure specifies the input\ndata for the [`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`] token.\nWhich state is changed depends on the [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html)specified at [`crate::vk::IndirectCommandsLayoutNV`] creation time.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkSetStateFlagsIndirectCommandNV {\n    uint32_t    data;\n} VkSetStateFlagsIndirectCommandNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::data`] encodes packed state that this command alters.\n\n  * Bit `0`: If set represents [`crate::vk::FrontFace::CLOCKWISE`], otherwise[`crate::vk::FrontFace::COUNTER_CLOCKWISE`]\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[repr(transparent)]
pub struct SetStateFlagsIndirectCommandNVBuilder<'a>(SetStateFlagsIndirectCommandNV, std::marker::PhantomData<&'a ()>);
impl<'a> SetStateFlagsIndirectCommandNVBuilder<'a> {
    #[inline]
    pub fn new() -> SetStateFlagsIndirectCommandNVBuilder<'a> {
        SetStateFlagsIndirectCommandNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn data(mut self, data: u32) -> Self {
        self.0.data = data as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SetStateFlagsIndirectCommandNV {
        self.0
    }
}
impl<'a> std::default::Default for SetStateFlagsIndirectCommandNVBuilder<'a> {
    fn default() -> SetStateFlagsIndirectCommandNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SetStateFlagsIndirectCommandNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SetStateFlagsIndirectCommandNVBuilder<'a> {
    type Target = SetStateFlagsIndirectCommandNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SetStateFlagsIndirectCommandNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsStreamNV.html)) · Structure <br/> VkIndirectCommandsStreamNV - Structure specifying input streams for generated command tokens\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsStreamNV`] structure specifies the input data for\none or more tokens at processing time.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsStreamNV {\n    VkBuffer        buffer;\n    VkDeviceSize    offset;\n} VkIndirectCommandsStreamNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer`] specifies the [`crate::vk::Buffer`] storing the functional\n  arguments for each sequence.\n  These arguments **can** be written by the device.\n\n* [`Self::offset`] specified an offset into [`Self::buffer`] where the arguments\n  start.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-02942  \n   The [`Self::buffer`]’s usage flag **must** have the[`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkIndirectCommandsStreamNV-offset-02943  \n   The [`Self::offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minIndirectCommandsBufferOffsetAlignment`\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-02975  \n   If [`Self::buffer`] is non-sparse then it **must** be bound completely and\n  contiguously to a single [`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-parameter  \n  [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::DeviceSize`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
#[doc(alias = "VkIndirectCommandsStreamNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct IndirectCommandsStreamNV {
    pub buffer: crate::vk1_0::Buffer,
    pub offset: crate::vk1_0::DeviceSize,
}
impl Default for IndirectCommandsStreamNV {
    fn default() -> Self {
        Self { buffer: Default::default(), offset: Default::default() }
    }
}
impl std::fmt::Debug for IndirectCommandsStreamNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("IndirectCommandsStreamNV").field("buffer", &self.buffer).field("offset", &self.offset).finish()
    }
}
impl IndirectCommandsStreamNV {
    #[inline]
    pub fn into_builder<'a>(self) -> IndirectCommandsStreamNVBuilder<'a> {
        IndirectCommandsStreamNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsStreamNV.html)) · Builder of [`IndirectCommandsStreamNV`] <br/> VkIndirectCommandsStreamNV - Structure specifying input streams for generated command tokens\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsStreamNV`] structure specifies the input data for\none or more tokens at processing time.\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsStreamNV {\n    VkBuffer        buffer;\n    VkDeviceSize    offset;\n} VkIndirectCommandsStreamNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::buffer`] specifies the [`crate::vk::Buffer`] storing the functional\n  arguments for each sequence.\n  These arguments **can** be written by the device.\n\n* [`Self::offset`] specified an offset into [`Self::buffer`] where the arguments\n  start.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-02942  \n   The [`Self::buffer`]’s usage flag **must** have the[`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkIndirectCommandsStreamNV-offset-02943  \n   The [`Self::offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minIndirectCommandsBufferOffsetAlignment`\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-02975  \n   If [`Self::buffer`] is non-sparse then it **must** be bound completely and\n  contiguously to a single [`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsStreamNV-buffer-parameter  \n  [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::DeviceSize`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
#[repr(transparent)]
pub struct IndirectCommandsStreamNVBuilder<'a>(IndirectCommandsStreamNV, std::marker::PhantomData<&'a ()>);
impl<'a> IndirectCommandsStreamNVBuilder<'a> {
    #[inline]
    pub fn new() -> IndirectCommandsStreamNVBuilder<'a> {
        IndirectCommandsStreamNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer(mut self, buffer: crate::vk1_0::Buffer) -> Self {
        self.0.buffer = buffer as _;
        self
    }
    #[inline]
    pub fn offset(mut self, offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> IndirectCommandsStreamNV {
        self.0
    }
}
impl<'a> std::default::Default for IndirectCommandsStreamNVBuilder<'a> {
    fn default() -> IndirectCommandsStreamNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for IndirectCommandsStreamNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for IndirectCommandsStreamNVBuilder<'a> {
    type Target = IndirectCommandsStreamNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for IndirectCommandsStreamNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutTokenNV.html)) · Structure <br/> VkIndirectCommandsLayoutTokenNV - Struct specifying the details of an indirect command layout token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsLayoutTokenNV`] structure specifies details to the\nfunction arguments that need to be known at layout creation time:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsLayoutTokenNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkIndirectCommandsTokenTypeNV    tokenType;\n    uint32_t                         stream;\n    uint32_t                         offset;\n    uint32_t                         vertexBindingUnit;\n    VkBool32                         vertexDynamicStride;\n    VkPipelineLayout                 pushconstantPipelineLayout;\n    VkShaderStageFlags               pushconstantShaderStageFlags;\n    uint32_t                         pushconstantOffset;\n    uint32_t                         pushconstantSize;\n    VkIndirectStateFlagsNV           indirectStateFlags;\n    uint32_t                         indexTypeCount;\n    const VkIndexType*               pIndexTypes;\n    const uint32_t*                  pIndexTypeValues;\n} VkIndirectCommandsLayoutTokenNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::token_type`] specifies the token command type.\n\n* [`Self::stream`] is the index of the input stream that contains the token\n  argument data.\n\n* [`Self::offset`] is a relative starting offset within the input stream\n  memory for the token argument data.\n\n* [`Self::vertex_binding_unit`] is used for the vertex buffer binding command.\n\n* [`Self::vertex_dynamic_stride`] sets if the vertex buffer stride is provided\n  by the binding command rather than the current bound graphics pipeline\n  state.\n\n* [`Self::pushconstant_pipeline_layout`] is the [`crate::vk::PipelineLayout`] used for\n  the push constant command.\n\n* [`Self::pushconstant_shader_stage_flags`] are the shader stage flags used for\n  the push constant command.\n\n* [`Self::pushconstant_offset`] is the offset used for the push constant\n  command.\n\n* [`Self::pushconstant_size`] is the size used for the push constant command.\n\n* [`Self::indirect_state_flags`] are the active states for the state flag\n  command.\n\n* [`Self::index_type_count`] is the optional size of the [`Self::p_index_types`] and[`Self::p_index_type_values`] array pairings.\n  If not zero, it allows to register a custom `uint32_t` value to be\n  treated as specific [`crate::vk::IndexType`].\n\n* [`Self::p_index_types`] is the used [`crate::vk::IndexType`] for the corresponding`uint32_t` value entry in [`Self::p_index_type_values`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-stream-02951  \n  [`Self::stream`] **must** be smaller than[`crate::vk::IndirectCommandsLayoutCreateInfoNV`]::`streamCount`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-offset-02952  \n  [`Self::offset`] **must** be less than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsTokenOffset`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02976  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::VERTEX_BUFFER_NV`],[`Self::vertex_binding_unit`] **must** stay within device supported limits for\n  the appropriate commands\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02977  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_pipeline_layout`] **must** be valid\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02978  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_offset`] **must** be a multiple of `4`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02979  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_size`] **must** be a multiple of `4`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02980  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_offset`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxPushConstantsSize`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02981  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_size`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxPushConstantsSize` minus[`Self::pushconstant_offset`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02982  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], for each byte in\n  the range specified by [`Self::pushconstant_offset`] and[`Self::pushconstant_size`] and for each shader stage in[`Self::pushconstant_shader_stage_flags`], there **must** be a push constant range\n  in [`Self::pushconstant_pipeline_layout`] that includes that byte and that\n  stage\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02983  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], for each byte in\n  the range specified by [`Self::pushconstant_offset`] and[`Self::pushconstant_size`] and for each push constant range that overlaps\n  that byte, [`Self::pushconstant_shader_stage_flags`] **must** include all stages\n  in that push constant range’s[`crate::vk::PushConstantRange::stage_flags`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02984  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`],[`Self::indirect_state_flags`] **must** not be `0`\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INDIRECT_COMMANDS_LAYOUT_TOKEN_NV`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-parameter  \n  [`Self::token_type`] **must** be a valid [`crate::vk::IndirectCommandsTokenTypeNV`] value\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pushconstantPipelineLayout-parameter  \n   If [`Self::pushconstant_pipeline_layout`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::pushconstant_pipeline_layout`] **must** be a valid [`crate::vk::PipelineLayout`] handle\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pushconstantShaderStageFlags-parameter  \n  [`Self::pushconstant_shader_stage_flags`] **must** be a valid combination of [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-indirectStateFlags-parameter  \n  [`Self::indirect_state_flags`] **must** be a valid combination of [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html) values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pIndexTypes-parameter  \n   If [`Self::index_type_count`] is not `0`, [`Self::p_index_types`] **must** be a valid pointer to an array of [`Self::index_type_count`] valid [`crate::vk::IndexType`] values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pIndexTypeValues-parameter  \n   If [`Self::index_type_count`] is not `0`, [`Self::p_index_type_values`] **must** be a valid pointer to an array of [`Self::index_type_count`] `uint32_t` values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::IndexType`], [`crate::vk::IndirectCommandsLayoutCreateInfoNV`], [`crate::vk::IndirectCommandsTokenTypeNV`], [`crate::vk::IndirectStateFlagBitsNV`], [`crate::vk::PipelineLayout`], [`crate::vk::ShaderStageFlagBits`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkIndirectCommandsLayoutTokenNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct IndirectCommandsLayoutTokenNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub token_type: crate::extensions::nv_device_generated_commands::IndirectCommandsTokenTypeNV,
    pub stream: u32,
    pub offset: u32,
    pub vertex_binding_unit: u32,
    pub vertex_dynamic_stride: crate::vk1_0::Bool32,
    pub pushconstant_pipeline_layout: crate::vk1_0::PipelineLayout,
    pub pushconstant_shader_stage_flags: crate::vk1_0::ShaderStageFlags,
    pub pushconstant_offset: u32,
    pub pushconstant_size: u32,
    pub indirect_state_flags: crate::extensions::nv_device_generated_commands::IndirectStateFlagsNV,
    pub index_type_count: u32,
    pub p_index_types: *const crate::vk1_0::IndexType,
    pub p_index_type_values: *const u32,
}
impl IndirectCommandsLayoutTokenNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::INDIRECT_COMMANDS_LAYOUT_TOKEN_NV;
}
impl Default for IndirectCommandsLayoutTokenNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), token_type: Default::default(), stream: Default::default(), offset: Default::default(), vertex_binding_unit: Default::default(), vertex_dynamic_stride: Default::default(), pushconstant_pipeline_layout: Default::default(), pushconstant_shader_stage_flags: Default::default(), pushconstant_offset: Default::default(), pushconstant_size: Default::default(), indirect_state_flags: Default::default(), index_type_count: Default::default(), p_index_types: std::ptr::null(), p_index_type_values: std::ptr::null() }
    }
}
impl std::fmt::Debug for IndirectCommandsLayoutTokenNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("IndirectCommandsLayoutTokenNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("token_type", &self.token_type).field("stream", &self.stream).field("offset", &self.offset).field("vertex_binding_unit", &self.vertex_binding_unit).field("vertex_dynamic_stride", &(self.vertex_dynamic_stride != 0)).field("pushconstant_pipeline_layout", &self.pushconstant_pipeline_layout).field("pushconstant_shader_stage_flags", &self.pushconstant_shader_stage_flags).field("pushconstant_offset", &self.pushconstant_offset).field("pushconstant_size", &self.pushconstant_size).field("indirect_state_flags", &self.indirect_state_flags).field("index_type_count", &self.index_type_count).field("p_index_types", &self.p_index_types).field("p_index_type_values", &self.p_index_type_values).finish()
    }
}
impl IndirectCommandsLayoutTokenNV {
    #[inline]
    pub fn into_builder<'a>(self) -> IndirectCommandsLayoutTokenNVBuilder<'a> {
        IndirectCommandsLayoutTokenNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutTokenNV.html)) · Builder of [`IndirectCommandsLayoutTokenNV`] <br/> VkIndirectCommandsLayoutTokenNV - Struct specifying the details of an indirect command layout token\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsLayoutTokenNV`] structure specifies details to the\nfunction arguments that need to be known at layout creation time:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsLayoutTokenNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkIndirectCommandsTokenTypeNV    tokenType;\n    uint32_t                         stream;\n    uint32_t                         offset;\n    uint32_t                         vertexBindingUnit;\n    VkBool32                         vertexDynamicStride;\n    VkPipelineLayout                 pushconstantPipelineLayout;\n    VkShaderStageFlags               pushconstantShaderStageFlags;\n    uint32_t                         pushconstantOffset;\n    uint32_t                         pushconstantSize;\n    VkIndirectStateFlagsNV           indirectStateFlags;\n    uint32_t                         indexTypeCount;\n    const VkIndexType*               pIndexTypes;\n    const uint32_t*                  pIndexTypeValues;\n} VkIndirectCommandsLayoutTokenNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::token_type`] specifies the token command type.\n\n* [`Self::stream`] is the index of the input stream that contains the token\n  argument data.\n\n* [`Self::offset`] is a relative starting offset within the input stream\n  memory for the token argument data.\n\n* [`Self::vertex_binding_unit`] is used for the vertex buffer binding command.\n\n* [`Self::vertex_dynamic_stride`] sets if the vertex buffer stride is provided\n  by the binding command rather than the current bound graphics pipeline\n  state.\n\n* [`Self::pushconstant_pipeline_layout`] is the [`crate::vk::PipelineLayout`] used for\n  the push constant command.\n\n* [`Self::pushconstant_shader_stage_flags`] are the shader stage flags used for\n  the push constant command.\n\n* [`Self::pushconstant_offset`] is the offset used for the push constant\n  command.\n\n* [`Self::pushconstant_size`] is the size used for the push constant command.\n\n* [`Self::indirect_state_flags`] are the active states for the state flag\n  command.\n\n* [`Self::index_type_count`] is the optional size of the [`Self::p_index_types`] and[`Self::p_index_type_values`] array pairings.\n  If not zero, it allows to register a custom `uint32_t` value to be\n  treated as specific [`crate::vk::IndexType`].\n\n* [`Self::p_index_types`] is the used [`crate::vk::IndexType`] for the corresponding`uint32_t` value entry in [`Self::p_index_type_values`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-stream-02951  \n  [`Self::stream`] **must** be smaller than[`crate::vk::IndirectCommandsLayoutCreateInfoNV`]::`streamCount`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-offset-02952  \n  [`Self::offset`] **must** be less than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsTokenOffset`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02976  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::VERTEX_BUFFER_NV`],[`Self::vertex_binding_unit`] **must** stay within device supported limits for\n  the appropriate commands\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02977  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_pipeline_layout`] **must** be valid\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02978  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_offset`] **must** be a multiple of `4`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02979  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_size`] **must** be a multiple of `4`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02980  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_offset`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxPushConstantsSize`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02981  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`],[`Self::pushconstant_size`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxPushConstantsSize` minus[`Self::pushconstant_offset`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02982  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], for each byte in\n  the range specified by [`Self::pushconstant_offset`] and[`Self::pushconstant_size`] and for each shader stage in[`Self::pushconstant_shader_stage_flags`], there **must** be a push constant range\n  in [`Self::pushconstant_pipeline_layout`] that includes that byte and that\n  stage\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02983  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], for each byte in\n  the range specified by [`Self::pushconstant_offset`] and[`Self::pushconstant_size`] and for each push constant range that overlaps\n  that byte, [`Self::pushconstant_shader_stage_flags`] **must** include all stages\n  in that push constant range’s[`crate::vk::PushConstantRange::stage_flags`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-02984  \n   If [`Self::token_type`] is[`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`],[`Self::indirect_state_flags`] **must** not be `0`\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INDIRECT_COMMANDS_LAYOUT_TOKEN_NV`]\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-tokenType-parameter  \n  [`Self::token_type`] **must** be a valid [`crate::vk::IndirectCommandsTokenTypeNV`] value\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pushconstantPipelineLayout-parameter  \n   If [`Self::pushconstant_pipeline_layout`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::pushconstant_pipeline_layout`] **must** be a valid [`crate::vk::PipelineLayout`] handle\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pushconstantShaderStageFlags-parameter  \n  [`Self::pushconstant_shader_stage_flags`] **must** be a valid combination of [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-indirectStateFlags-parameter  \n  [`Self::indirect_state_flags`] **must** be a valid combination of [VkIndirectStateFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectStateFlagBitsNV.html) values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pIndexTypes-parameter  \n   If [`Self::index_type_count`] is not `0`, [`Self::p_index_types`] **must** be a valid pointer to an array of [`Self::index_type_count`] valid [`crate::vk::IndexType`] values\n\n* []() VUID-VkIndirectCommandsLayoutTokenNV-pIndexTypeValues-parameter  \n   If [`Self::index_type_count`] is not `0`, [`Self::p_index_type_values`] **must** be a valid pointer to an array of [`Self::index_type_count`] `uint32_t` values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::IndexType`], [`crate::vk::IndirectCommandsLayoutCreateInfoNV`], [`crate::vk::IndirectCommandsTokenTypeNV`], [`crate::vk::IndirectStateFlagBitsNV`], [`crate::vk::PipelineLayout`], [`crate::vk::ShaderStageFlagBits`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct IndirectCommandsLayoutTokenNVBuilder<'a>(IndirectCommandsLayoutTokenNV, std::marker::PhantomData<&'a ()>);
impl<'a> IndirectCommandsLayoutTokenNVBuilder<'a> {
    #[inline]
    pub fn new() -> IndirectCommandsLayoutTokenNVBuilder<'a> {
        IndirectCommandsLayoutTokenNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn token_type(mut self, token_type: crate::extensions::nv_device_generated_commands::IndirectCommandsTokenTypeNV) -> Self {
        self.0.token_type = token_type as _;
        self
    }
    #[inline]
    pub fn stream(mut self, stream: u32) -> Self {
        self.0.stream = stream as _;
        self
    }
    #[inline]
    pub fn offset(mut self, offset: u32) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    pub fn vertex_binding_unit(mut self, vertex_binding_unit: u32) -> Self {
        self.0.vertex_binding_unit = vertex_binding_unit as _;
        self
    }
    #[inline]
    pub fn vertex_dynamic_stride(mut self, vertex_dynamic_stride: bool) -> Self {
        self.0.vertex_dynamic_stride = vertex_dynamic_stride as _;
        self
    }
    #[inline]
    pub fn pushconstant_pipeline_layout(mut self, pushconstant_pipeline_layout: crate::vk1_0::PipelineLayout) -> Self {
        self.0.pushconstant_pipeline_layout = pushconstant_pipeline_layout as _;
        self
    }
    #[inline]
    pub fn pushconstant_shader_stage_flags(mut self, pushconstant_shader_stage_flags: crate::vk1_0::ShaderStageFlags) -> Self {
        self.0.pushconstant_shader_stage_flags = pushconstant_shader_stage_flags as _;
        self
    }
    #[inline]
    pub fn pushconstant_offset(mut self, pushconstant_offset: u32) -> Self {
        self.0.pushconstant_offset = pushconstant_offset as _;
        self
    }
    #[inline]
    pub fn pushconstant_size(mut self, pushconstant_size: u32) -> Self {
        self.0.pushconstant_size = pushconstant_size as _;
        self
    }
    #[inline]
    pub fn indirect_state_flags(mut self, indirect_state_flags: crate::extensions::nv_device_generated_commands::IndirectStateFlagsNV) -> Self {
        self.0.indirect_state_flags = indirect_state_flags as _;
        self
    }
    #[inline]
    pub fn index_types(mut self, index_types: &'a [crate::vk1_0::IndexType]) -> Self {
        self.0.p_index_types = index_types.as_ptr() as _;
        self.0.index_type_count = index_types.len() as _;
        self
    }
    #[inline]
    pub fn index_type_values(mut self, index_type_values: &'a [u32]) -> Self {
        self.0.p_index_type_values = index_type_values.as_ptr() as _;
        self.0.index_type_count = index_type_values.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> IndirectCommandsLayoutTokenNV {
        self.0
    }
}
impl<'a> std::default::Default for IndirectCommandsLayoutTokenNVBuilder<'a> {
    fn default() -> IndirectCommandsLayoutTokenNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for IndirectCommandsLayoutTokenNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for IndirectCommandsLayoutTokenNVBuilder<'a> {
    type Target = IndirectCommandsLayoutTokenNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for IndirectCommandsLayoutTokenNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutCreateInfoNV.html)) · Structure <br/> VkIndirectCommandsLayoutCreateInfoNV - Structure specifying the parameters of a newly created indirect commands layout object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsLayoutCreateInfoNV {\n    VkStructureType                           sType;\n    const void*                               pNext;\n    VkIndirectCommandsLayoutUsageFlagsNV      flags;\n    VkPipelineBindPoint                       pipelineBindPoint;\n    uint32_t                                  tokenCount;\n    const VkIndirectCommandsLayoutTokenNV*    pTokens;\n    uint32_t                                  streamCount;\n    const uint32_t*                           pStreamStrides;\n} VkIndirectCommandsLayoutCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] that this\n  layout targets.\n\n* [`Self::flags`] is a bitmask of[VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html) specifying usage hints of\n  this layout.\n\n* [`Self::token_count`] is the length of the individual command sequence.\n\n* [`Self::p_tokens`] is an array describing each command token in detail.\n  See [`crate::vk::IndirectCommandsTokenTypeNV`] and[`crate::vk::IndirectCommandsLayoutTokenNV`] below for details.\n\n* [`Self::stream_count`] is the number of streams used to provide the token\n  inputs.\n\n* [`Self::p_stream_strides`] is an array defining the byte stride for each input\n  stream.\n[](#_description)Description\n----------\n\nThe following code illustrates some of the flags:\n\n```\nvoid cmdProcessAllSequences(cmd, pipeline, indirectCommandsLayout, pIndirectCommandsTokens, sequencesCount, indexbuffer, indexbufferOffset)\n{\n  for (s = 0; s < sequencesCount; s++)\n  {\n    sUsed = s;\n\n    if (indirectCommandsLayout.flags & VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV) {\n      sUsed = indexbuffer.load_uint32( sUsed * sizeof(uint32_t) + indexbufferOffset);\n    }\n\n    if (indirectCommandsLayout.flags & VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV) {\n      sUsed = incoherent_implementation_dependent_permutation[ sUsed ];\n    }\n\n    cmdProcessSequence( cmd, pipeline, indirectCommandsLayout, pIndirectCommandsTokens, sUsed );\n  }\n}\n```\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pipelineBindPoint-02930  \n   The [`Self::pipeline_bind_point`] **must** be[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-tokenCount-02931  \n  [`Self::token_count`] **must** be greater than `0` and less than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsTokenCount`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02932  \n   If [`Self::p_tokens`] contains an entry of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`] it **must** be the\n  first element of the array and there **must** be only a single element of\n  such token type\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02933  \n   If [`Self::p_tokens`] contains an entry of[`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`] there **must** be only\n  a single element of such token type\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02934  \n   All state tokens in [`Self::p_tokens`] **must** occur prior work provoking\n  tokens ([`crate::vk::IndirectCommandsTokenTypeNV::DRAW_NV`],[`crate::vk::IndirectCommandsTokenTypeNV::DRAW_INDEXED_NV`],[`crate::vk::IndirectCommandsTokenTypeNV::DRAW_TASKS_NV`])\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02935  \n   The content of [`Self::p_tokens`] **must** include one single work provoking\n  token that is compatible with the [`Self::pipeline_bind_point`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-streamCount-02936  \n  [`Self::stream_count`] **must** be greater than `0` and less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsStreamCount`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pStreamStrides-02937  \n   each element of [`Self::p_stream_strides`] **must** be greater than `0`and less\n  than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsStreamStride`.\n  Furthermore the alignment of each token input **must** be ensured\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html) values\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-parameter  \n  [`Self::p_tokens`] **must** be a valid pointer to an array of [`Self::token_count`] valid [`crate::vk::IndirectCommandsLayoutTokenNV`] structures\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pStreamStrides-parameter  \n  [`Self::p_stream_strides`] **must** be a valid pointer to an array of [`Self::stream_count`] `uint32_t` values\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-tokenCount-arraylength  \n  [`Self::token_count`] **must** be greater than `0`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-streamCount-arraylength  \n  [`Self::stream_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutTokenNV`], [`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::create_indirect_commands_layout_nv`]\n"]
#[doc(alias = "VkIndirectCommandsLayoutCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct IndirectCommandsLayoutCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutUsageFlagsNV,
    pub pipeline_bind_point: crate::vk1_0::PipelineBindPoint,
    pub token_count: u32,
    pub p_tokens: *const crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutTokenNV,
    pub stream_count: u32,
    pub p_stream_strides: *const u32,
}
impl IndirectCommandsLayoutCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV;
}
impl Default for IndirectCommandsLayoutCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), pipeline_bind_point: Default::default(), token_count: Default::default(), p_tokens: std::ptr::null(), stream_count: Default::default(), p_stream_strides: std::ptr::null() }
    }
}
impl std::fmt::Debug for IndirectCommandsLayoutCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("IndirectCommandsLayoutCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("pipeline_bind_point", &self.pipeline_bind_point).field("token_count", &self.token_count).field("p_tokens", &self.p_tokens).field("stream_count", &self.stream_count).field("p_stream_strides", &self.p_stream_strides).finish()
    }
}
impl IndirectCommandsLayoutCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
        IndirectCommandsLayoutCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutCreateInfoNV.html)) · Builder of [`IndirectCommandsLayoutCreateInfoNV`] <br/> VkIndirectCommandsLayoutCreateInfoNV - Structure specifying the parameters of a newly created indirect commands layout object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkIndirectCommandsLayoutCreateInfoNV {\n    VkStructureType                           sType;\n    const void*                               pNext;\n    VkIndirectCommandsLayoutUsageFlagsNV      flags;\n    VkPipelineBindPoint                       pipelineBindPoint;\n    uint32_t                                  tokenCount;\n    const VkIndirectCommandsLayoutTokenNV*    pTokens;\n    uint32_t                                  streamCount;\n    const uint32_t*                           pStreamStrides;\n} VkIndirectCommandsLayoutCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] that this\n  layout targets.\n\n* [`Self::flags`] is a bitmask of[VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html) specifying usage hints of\n  this layout.\n\n* [`Self::token_count`] is the length of the individual command sequence.\n\n* [`Self::p_tokens`] is an array describing each command token in detail.\n  See [`crate::vk::IndirectCommandsTokenTypeNV`] and[`crate::vk::IndirectCommandsLayoutTokenNV`] below for details.\n\n* [`Self::stream_count`] is the number of streams used to provide the token\n  inputs.\n\n* [`Self::p_stream_strides`] is an array defining the byte stride for each input\n  stream.\n[](#_description)Description\n----------\n\nThe following code illustrates some of the flags:\n\n```\nvoid cmdProcessAllSequences(cmd, pipeline, indirectCommandsLayout, pIndirectCommandsTokens, sequencesCount, indexbuffer, indexbufferOffset)\n{\n  for (s = 0; s < sequencesCount; s++)\n  {\n    sUsed = s;\n\n    if (indirectCommandsLayout.flags & VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV) {\n      sUsed = indexbuffer.load_uint32( sUsed * sizeof(uint32_t) + indexbufferOffset);\n    }\n\n    if (indirectCommandsLayout.flags & VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV) {\n      sUsed = incoherent_implementation_dependent_permutation[ sUsed ];\n    }\n\n    cmdProcessSequence( cmd, pipeline, indirectCommandsLayout, pIndirectCommandsTokens, sUsed );\n  }\n}\n```\n\nValid Usage\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pipelineBindPoint-02930  \n   The [`Self::pipeline_bind_point`] **must** be[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-tokenCount-02931  \n  [`Self::token_count`] **must** be greater than `0` and less than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsTokenCount`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02932  \n   If [`Self::p_tokens`] contains an entry of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`] it **must** be the\n  first element of the array and there **must** be only a single element of\n  such token type\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02933  \n   If [`Self::p_tokens`] contains an entry of[`crate::vk::IndirectCommandsTokenTypeNV::STATE_FLAGS_NV`] there **must** be only\n  a single element of such token type\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02934  \n   All state tokens in [`Self::p_tokens`] **must** occur prior work provoking\n  tokens ([`crate::vk::IndirectCommandsTokenTypeNV::DRAW_NV`],[`crate::vk::IndirectCommandsTokenTypeNV::DRAW_INDEXED_NV`],[`crate::vk::IndirectCommandsTokenTypeNV::DRAW_TASKS_NV`])\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-02935  \n   The content of [`Self::p_tokens`] **must** include one single work provoking\n  token that is compatible with the [`Self::pipeline_bind_point`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-streamCount-02936  \n  [`Self::stream_count`] **must** be greater than `0` and less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsStreamCount`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pStreamStrides-02937  \n   each element of [`Self::p_stream_strides`] **must** be greater than `0`and less\n  than or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`maxIndirectCommandsStreamStride`.\n  Furthermore the alignment of each token input **must** be ensured\n\nValid Usage (Implicit)\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV`]\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkIndirectCommandsLayoutUsageFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html) values\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pTokens-parameter  \n  [`Self::p_tokens`] **must** be a valid pointer to an array of [`Self::token_count`] valid [`crate::vk::IndirectCommandsLayoutTokenNV`] structures\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-pStreamStrides-parameter  \n  [`Self::p_stream_strides`] **must** be a valid pointer to an array of [`Self::stream_count`] `uint32_t` values\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-tokenCount-arraylength  \n  [`Self::token_count`] **must** be greater than `0`\n\n* []() VUID-VkIndirectCommandsLayoutCreateInfoNV-streamCount-arraylength  \n  [`Self::stream_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutTokenNV`], [`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::create_indirect_commands_layout_nv`]\n"]
#[repr(transparent)]
pub struct IndirectCommandsLayoutCreateInfoNVBuilder<'a>(IndirectCommandsLayoutCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
        IndirectCommandsLayoutCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutUsageFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn pipeline_bind_point(mut self, pipeline_bind_point: crate::vk1_0::PipelineBindPoint) -> Self {
        self.0.pipeline_bind_point = pipeline_bind_point as _;
        self
    }
    #[inline]
    pub fn tokens(mut self, tokens: &'a [crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutTokenNVBuilder]) -> Self {
        self.0.p_tokens = tokens.as_ptr() as _;
        self.0.token_count = tokens.len() as _;
        self
    }
    #[inline]
    pub fn stream_strides(mut self, stream_strides: &'a [u32]) -> Self {
        self.0.p_stream_strides = stream_strides.as_ptr() as _;
        self.0.stream_count = stream_strides.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> IndirectCommandsLayoutCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
    fn default() -> IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
    type Target = IndirectCommandsLayoutCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for IndirectCommandsLayoutCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeneratedCommandsInfoNV.html)) · Structure <br/> VkGeneratedCommandsInfoNV - Structure specifying parameters for the generation of commands\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGeneratedCommandsInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkPipelineBindPoint                  pipelineBindPoint;\n    VkPipeline                           pipeline;\n    VkIndirectCommandsLayoutNV           indirectCommandsLayout;\n    uint32_t                             streamCount;\n    const VkIndirectCommandsStreamNV*    pStreams;\n    uint32_t                             sequencesCount;\n    VkBuffer                             preprocessBuffer;\n    VkDeviceSize                         preprocessOffset;\n    VkDeviceSize                         preprocessSize;\n    VkBuffer                             sequencesCountBuffer;\n    VkDeviceSize                         sequencesCountOffset;\n    VkBuffer                             sequencesIndexBuffer;\n    VkDeviceSize                         sequencesIndexOffset;\n} VkGeneratedCommandsInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] used for the[`Self::pipeline`].\n\n* [`Self::pipeline`] is the [`crate::vk::Pipeline`] used in the generation and\n  execution process.\n\n* [`Self::indirect_commands_layout`] is the [`crate::vk::IndirectCommandsLayoutNV`]that provides the command sequence to generate.\n\n* [`Self::stream_count`] defines the number of input streams\n\n* [`Self::p_streams`] is a pointer to an array of [`Self::stream_count`][`crate::vk::IndirectCommandsStreamNV`] structures providing the input data for\n  the tokens used in [`Self::indirect_commands_layout`].\n\n* [`Self::sequences_count`] is the maximum number of sequences to reserve.\n  If [`Self::sequences_count_buffer`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), this is also the\n  actual number of sequences generated.\n\n* [`Self::preprocess_buffer`] is the [`crate::vk::Buffer`] that is used for\n  preprocessing the input data for execution.\n  If this structure is used with [`crate::vk::DeviceLoader::cmd_execute_generated_commands_nv`]with its `isPreprocessed` set to [`crate::vk::TRUE`], then the preprocessing\n  step is skipped and data is only read from this buffer.\n\n* [`Self::preprocess_offset`] is the byte offset into [`Self::preprocess_buffer`]where the preprocessed data is stored.\n\n* [`Self::preprocess_size`] is the maximum byte size within the[`Self::preprocess_buffer`] after the [`Self::preprocess_offset`] that is\n  available for preprocessing.\n\n* [`Self::sequences_count_buffer`] is a [`crate::vk::Buffer`] in which the actual\n  number of sequences is provided as single `uint32_t` value.\n\n* [`Self::sequences_count_offset`] is the byte offset into[`Self::sequences_count_buffer`] where the count value is stored.\n\n* [`Self::sequences_index_buffer`] is a [`crate::vk::Buffer`] that encodes the used\n  sequence indices as `uint32_t` array.\n\n* [`Self::sequences_index_offset`] is the byte offset into[`Self::sequences_index_buffer`] where the index values start.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipeline-02912  \n   The provided [`Self::pipeline`] **must** match the pipeline bound at execution\n  time\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02913  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`], then the[`Self::pipeline`] **must** have been created with multiple shader groups\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02914  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`], then the[`Self::pipeline`] **must** have been created with[`crate::vk::PipelineCreateFlagBits::INDIRECT_BINDABLE_NV`] set in[`crate::vk::GraphicsPipelineCreateInfo`]::`flags`\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02915  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], then the[`Self::pipeline`]`s [`crate::vk::PipelineLayout`] **must** match the[`crate::vk::IndirectCommandsLayoutTokenNV::pushconstant_pipeline_layout`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-streamCount-02916  \n  [`Self::stream_count`] **must** match the [`Self::indirect_commands_layout`]’s[`Self::stream_count`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCount-02917  \n  [`Self::sequences_count`] **must** be less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::max_indirect_sequence_count`]and[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV::max_sequences_count`]that was used to determine the [`Self::preprocess_size`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-02918  \n  [`Self::preprocess_buffer`] **must** have the[`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set in its usage flag\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessOffset-02919  \n  [`Self::preprocess_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::min_indirect_commands_buffer_offset_alignment`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-02971  \n   If [`Self::preprocess_buffer`] is non-sparse then it **must** be bound\n  completely and contiguously to a single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessSize-02920  \n  [`Self::preprocess_size`] **must** be at least equal to the memory requirement`s\n  size returned by [`crate::vk::DeviceLoader::get_generated_commands_memory_requirements_nv`] using\n  the matching inputs ([`Self::indirect_commands_layout`], …\u{200b}) as within this\n  structure\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02921  \n  [`Self::sequences_count_buffer`] **can** be set if the actual used count of\n  sequences is sourced from the provided buffer.\n  In that case the [`Self::sequences_count`] serves as upper bound\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02922  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), its usage\n  flag **must** have the [`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02923  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`Self::sequences_count_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minSequencesCountBufferOffsetAlignment`\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02972  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and is\n  non-sparse then it **must** be bound completely and contiguously to a\n  single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02924  \n   If [`Self::indirect_commands_layout`]’s[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::INDEXED_SEQUENCES_NV`] is set,[`Self::sequences_index_buffer`] **must** be set otherwise it **must** be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02925  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), its usage\n  flag **must** have the [`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02926  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`Self::sequences_index_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minSequencesIndexBufferOffsetAlignment`\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02973  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and is\n  non-sparse then it **must** be bound completely and contiguously to a\n  single [`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkGeneratedCommandsInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GENERATED_COMMANDS_INFO_NV`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-parameter  \n  [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-pStreams-parameter  \n  [`Self::p_streams`] **must** be a valid pointer to an array of [`Self::stream_count`] valid [`crate::vk::IndirectCommandsStreamNV`] structures\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-parameter  \n  [`Self::preprocess_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-parameter  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sequences_count_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-parameter  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sequences_index_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-streamCount-arraylength  \n  [`Self::stream_count`] **must** be greater than `0`\n\n* []() VUID-VkGeneratedCommandsInfoNV-commonparent  \n   Each of [`Self::indirect_commands_layout`], [`Self::pipeline`], [`Self::preprocess_buffer`], [`Self::sequences_count_buffer`], and [`Self::sequences_index_buffer`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::DeviceSize`], [`crate::vk::IndirectCommandsLayoutNV`], [`crate::vk::IndirectCommandsStreamNV`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_execute_generated_commands_nv`], [`crate::vk::DeviceLoader::cmd_preprocess_generated_commands_nv`]\n"]
#[doc(alias = "VkGeneratedCommandsInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeneratedCommandsInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub pipeline_bind_point: crate::vk1_0::PipelineBindPoint,
    pub pipeline: crate::vk1_0::Pipeline,
    pub indirect_commands_layout: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV,
    pub stream_count: u32,
    pub p_streams: *const crate::extensions::nv_device_generated_commands::IndirectCommandsStreamNV,
    pub sequences_count: u32,
    pub preprocess_buffer: crate::vk1_0::Buffer,
    pub preprocess_offset: crate::vk1_0::DeviceSize,
    pub preprocess_size: crate::vk1_0::DeviceSize,
    pub sequences_count_buffer: crate::vk1_0::Buffer,
    pub sequences_count_offset: crate::vk1_0::DeviceSize,
    pub sequences_index_buffer: crate::vk1_0::Buffer,
    pub sequences_index_offset: crate::vk1_0::DeviceSize,
}
impl GeneratedCommandsInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::GENERATED_COMMANDS_INFO_NV;
}
impl Default for GeneratedCommandsInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), pipeline_bind_point: Default::default(), pipeline: Default::default(), indirect_commands_layout: Default::default(), stream_count: Default::default(), p_streams: std::ptr::null(), sequences_count: Default::default(), preprocess_buffer: Default::default(), preprocess_offset: Default::default(), preprocess_size: Default::default(), sequences_count_buffer: Default::default(), sequences_count_offset: Default::default(), sequences_index_buffer: Default::default(), sequences_index_offset: Default::default() }
    }
}
impl std::fmt::Debug for GeneratedCommandsInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeneratedCommandsInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("pipeline_bind_point", &self.pipeline_bind_point).field("pipeline", &self.pipeline).field("indirect_commands_layout", &self.indirect_commands_layout).field("stream_count", &self.stream_count).field("p_streams", &self.p_streams).field("sequences_count", &self.sequences_count).field("preprocess_buffer", &self.preprocess_buffer).field("preprocess_offset", &self.preprocess_offset).field("preprocess_size", &self.preprocess_size).field("sequences_count_buffer", &self.sequences_count_buffer).field("sequences_count_offset", &self.sequences_count_offset).field("sequences_index_buffer", &self.sequences_index_buffer).field("sequences_index_offset", &self.sequences_index_offset).finish()
    }
}
impl GeneratedCommandsInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeneratedCommandsInfoNVBuilder<'a> {
        GeneratedCommandsInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeneratedCommandsInfoNV.html)) · Builder of [`GeneratedCommandsInfoNV`] <br/> VkGeneratedCommandsInfoNV - Structure specifying parameters for the generation of commands\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGeneratedCommandsInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkPipelineBindPoint                  pipelineBindPoint;\n    VkPipeline                           pipeline;\n    VkIndirectCommandsLayoutNV           indirectCommandsLayout;\n    uint32_t                             streamCount;\n    const VkIndirectCommandsStreamNV*    pStreams;\n    uint32_t                             sequencesCount;\n    VkBuffer                             preprocessBuffer;\n    VkDeviceSize                         preprocessOffset;\n    VkDeviceSize                         preprocessSize;\n    VkBuffer                             sequencesCountBuffer;\n    VkDeviceSize                         sequencesCountOffset;\n    VkBuffer                             sequencesIndexBuffer;\n    VkDeviceSize                         sequencesIndexOffset;\n} VkGeneratedCommandsInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] used for the[`Self::pipeline`].\n\n* [`Self::pipeline`] is the [`crate::vk::Pipeline`] used in the generation and\n  execution process.\n\n* [`Self::indirect_commands_layout`] is the [`crate::vk::IndirectCommandsLayoutNV`]that provides the command sequence to generate.\n\n* [`Self::stream_count`] defines the number of input streams\n\n* [`Self::p_streams`] is a pointer to an array of [`Self::stream_count`][`crate::vk::IndirectCommandsStreamNV`] structures providing the input data for\n  the tokens used in [`Self::indirect_commands_layout`].\n\n* [`Self::sequences_count`] is the maximum number of sequences to reserve.\n  If [`Self::sequences_count_buffer`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), this is also the\n  actual number of sequences generated.\n\n* [`Self::preprocess_buffer`] is the [`crate::vk::Buffer`] that is used for\n  preprocessing the input data for execution.\n  If this structure is used with [`crate::vk::DeviceLoader::cmd_execute_generated_commands_nv`]with its `isPreprocessed` set to [`crate::vk::TRUE`], then the preprocessing\n  step is skipped and data is only read from this buffer.\n\n* [`Self::preprocess_offset`] is the byte offset into [`Self::preprocess_buffer`]where the preprocessed data is stored.\n\n* [`Self::preprocess_size`] is the maximum byte size within the[`Self::preprocess_buffer`] after the [`Self::preprocess_offset`] that is\n  available for preprocessing.\n\n* [`Self::sequences_count_buffer`] is a [`crate::vk::Buffer`] in which the actual\n  number of sequences is provided as single `uint32_t` value.\n\n* [`Self::sequences_count_offset`] is the byte offset into[`Self::sequences_count_buffer`] where the count value is stored.\n\n* [`Self::sequences_index_buffer`] is a [`crate::vk::Buffer`] that encodes the used\n  sequence indices as `uint32_t` array.\n\n* [`Self::sequences_index_offset`] is the byte offset into[`Self::sequences_index_buffer`] where the index values start.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipeline-02912  \n   The provided [`Self::pipeline`] **must** match the pipeline bound at execution\n  time\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02913  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`], then the[`Self::pipeline`] **must** have been created with multiple shader groups\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02914  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::SHADER_GROUP_NV`], then the[`Self::pipeline`] **must** have been created with[`crate::vk::PipelineCreateFlagBits::INDIRECT_BINDABLE_NV`] set in[`crate::vk::GraphicsPipelineCreateInfo`]::`flags`\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-02915  \n   If the [`Self::indirect_commands_layout`] uses a token of[`crate::vk::IndirectCommandsTokenTypeNV::PUSH_CONSTANT_NV`], then the[`Self::pipeline`]`s [`crate::vk::PipelineLayout`] **must** match the[`crate::vk::IndirectCommandsLayoutTokenNV::pushconstant_pipeline_layout`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-streamCount-02916  \n  [`Self::stream_count`] **must** match the [`Self::indirect_commands_layout`]’s[`Self::stream_count`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCount-02917  \n  [`Self::sequences_count`] **must** be less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::max_indirect_sequence_count`]and[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV::max_sequences_count`]that was used to determine the [`Self::preprocess_size`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-02918  \n  [`Self::preprocess_buffer`] **must** have the[`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set in its usage flag\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessOffset-02919  \n  [`Self::preprocess_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::min_indirect_commands_buffer_offset_alignment`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-02971  \n   If [`Self::preprocess_buffer`] is non-sparse then it **must** be bound\n  completely and contiguously to a single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessSize-02920  \n  [`Self::preprocess_size`] **must** be at least equal to the memory requirement`s\n  size returned by [`crate::vk::DeviceLoader::get_generated_commands_memory_requirements_nv`] using\n  the matching inputs ([`Self::indirect_commands_layout`], …\u{200b}) as within this\n  structure\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02921  \n  [`Self::sequences_count_buffer`] **can** be set if the actual used count of\n  sequences is sourced from the provided buffer.\n  In that case the [`Self::sequences_count`] serves as upper bound\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02922  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), its usage\n  flag **must** have the [`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02923  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`Self::sequences_count_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minSequencesCountBufferOffsetAlignment`\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-02972  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and is\n  non-sparse then it **must** be bound completely and contiguously to a\n  single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02924  \n   If [`Self::indirect_commands_layout`]’s[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::INDEXED_SEQUENCES_NV`] is set,[`Self::sequences_index_buffer`] **must** be set otherwise it **must** be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02925  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), its usage\n  flag **must** have the [`crate::vk::BufferUsageFlagBits::INDIRECT_BUFFER`] bit set\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02926  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`Self::sequences_index_offset`] **must** be aligned to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV`]::`minSequencesIndexBufferOffsetAlignment`\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-02973  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and is\n  non-sparse then it **must** be bound completely and contiguously to a\n  single [`crate::vk::DeviceMemory`] object\n\nValid Usage (Implicit)\n\n* []() VUID-VkGeneratedCommandsInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GENERATED_COMMANDS_INFO_NV`]\n\n* []() VUID-VkGeneratedCommandsInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkGeneratedCommandsInfoNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-indirectCommandsLayout-parameter  \n  [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-pStreams-parameter  \n  [`Self::p_streams`] **must** be a valid pointer to an array of [`Self::stream_count`] valid [`crate::vk::IndirectCommandsStreamNV`] structures\n\n* []() VUID-VkGeneratedCommandsInfoNV-preprocessBuffer-parameter  \n  [`Self::preprocess_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesCountBuffer-parameter  \n   If [`Self::sequences_count_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sequences_count_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-sequencesIndexBuffer-parameter  \n   If [`Self::sequences_index_buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sequences_index_buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkGeneratedCommandsInfoNV-streamCount-arraylength  \n  [`Self::stream_count`] **must** be greater than `0`\n\n* []() VUID-VkGeneratedCommandsInfoNV-commonparent  \n   Each of [`Self::indirect_commands_layout`], [`Self::pipeline`], [`Self::preprocess_buffer`], [`Self::sequences_count_buffer`], and [`Self::sequences_index_buffer`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::DeviceSize`], [`crate::vk::IndirectCommandsLayoutNV`], [`crate::vk::IndirectCommandsStreamNV`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_execute_generated_commands_nv`], [`crate::vk::DeviceLoader::cmd_preprocess_generated_commands_nv`]\n"]
#[repr(transparent)]
pub struct GeneratedCommandsInfoNVBuilder<'a>(GeneratedCommandsInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeneratedCommandsInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeneratedCommandsInfoNVBuilder<'a> {
        GeneratedCommandsInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline_bind_point(mut self, pipeline_bind_point: crate::vk1_0::PipelineBindPoint) -> Self {
        self.0.pipeline_bind_point = pipeline_bind_point as _;
        self
    }
    #[inline]
    pub fn pipeline(mut self, pipeline: crate::vk1_0::Pipeline) -> Self {
        self.0.pipeline = pipeline as _;
        self
    }
    #[inline]
    pub fn indirect_commands_layout(mut self, indirect_commands_layout: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV) -> Self {
        self.0.indirect_commands_layout = indirect_commands_layout as _;
        self
    }
    #[inline]
    pub fn streams(mut self, streams: &'a [crate::extensions::nv_device_generated_commands::IndirectCommandsStreamNVBuilder]) -> Self {
        self.0.p_streams = streams.as_ptr() as _;
        self.0.stream_count = streams.len() as _;
        self
    }
    #[inline]
    pub fn sequences_count(mut self, sequences_count: u32) -> Self {
        self.0.sequences_count = sequences_count as _;
        self
    }
    #[inline]
    pub fn preprocess_buffer(mut self, preprocess_buffer: crate::vk1_0::Buffer) -> Self {
        self.0.preprocess_buffer = preprocess_buffer as _;
        self
    }
    #[inline]
    pub fn preprocess_offset(mut self, preprocess_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.preprocess_offset = preprocess_offset as _;
        self
    }
    #[inline]
    pub fn preprocess_size(mut self, preprocess_size: crate::vk1_0::DeviceSize) -> Self {
        self.0.preprocess_size = preprocess_size as _;
        self
    }
    #[inline]
    pub fn sequences_count_buffer(mut self, sequences_count_buffer: crate::vk1_0::Buffer) -> Self {
        self.0.sequences_count_buffer = sequences_count_buffer as _;
        self
    }
    #[inline]
    pub fn sequences_count_offset(mut self, sequences_count_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.sequences_count_offset = sequences_count_offset as _;
        self
    }
    #[inline]
    pub fn sequences_index_buffer(mut self, sequences_index_buffer: crate::vk1_0::Buffer) -> Self {
        self.0.sequences_index_buffer = sequences_index_buffer as _;
        self
    }
    #[inline]
    pub fn sequences_index_offset(mut self, sequences_index_offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.sequences_index_offset = sequences_index_offset as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeneratedCommandsInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for GeneratedCommandsInfoNVBuilder<'a> {
    fn default() -> GeneratedCommandsInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeneratedCommandsInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeneratedCommandsInfoNVBuilder<'a> {
    type Target = GeneratedCommandsInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeneratedCommandsInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeneratedCommandsMemoryRequirementsInfoNV.html)) · Structure <br/> VkGeneratedCommandsMemoryRequirementsInfoNV - Structure specifying parameters for the reservation of preprocess buffer space\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGeneratedCommandsMemoryRequirementsInfoNV {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkPipelineBindPoint           pipelineBindPoint;\n    VkPipeline                    pipeline;\n    VkIndirectCommandsLayoutNV    indirectCommandsLayout;\n    uint32_t                      maxSequencesCount;\n} VkGeneratedCommandsMemoryRequirementsInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] of the[`Self::pipeline`] that this buffer memory is intended to be used with\n  during the execution.\n\n* [`Self::pipeline`] is the [`crate::vk::Pipeline`] that this buffer memory is\n  intended to be used with during the execution.\n\n* [`Self::indirect_commands_layout`] is the [`crate::vk::IndirectCommandsLayoutNV`]that this buffer memory is intended to be used with.\n\n* [`Self::max_sequences_count`] is the maximum number of sequences that this\n  buffer memory in combination with the other state provided **can** be used\n  with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-maxSequencesCount-02907  \n  [`Self::max_sequences_count`] **must** be less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::max_indirect_sequence_count`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV`]\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-indirectCommandsLayout-parameter  \n  [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-commonparent  \n   Both of [`Self::indirect_commands_layout`], and [`Self::pipeline`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutNV`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_generated_commands_memory_requirements_nv`]\n"]
#[doc(alias = "VkGeneratedCommandsMemoryRequirementsInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct GeneratedCommandsMemoryRequirementsInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub pipeline_bind_point: crate::vk1_0::PipelineBindPoint,
    pub pipeline: crate::vk1_0::Pipeline,
    pub indirect_commands_layout: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV,
    pub max_sequences_count: u32,
}
impl GeneratedCommandsMemoryRequirementsInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV;
}
impl Default for GeneratedCommandsMemoryRequirementsInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), pipeline_bind_point: Default::default(), pipeline: Default::default(), indirect_commands_layout: Default::default(), max_sequences_count: Default::default() }
    }
}
impl std::fmt::Debug for GeneratedCommandsMemoryRequirementsInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("GeneratedCommandsMemoryRequirementsInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("pipeline_bind_point", &self.pipeline_bind_point).field("pipeline", &self.pipeline).field("indirect_commands_layout", &self.indirect_commands_layout).field("max_sequences_count", &self.max_sequences_count).finish()
    }
}
impl GeneratedCommandsMemoryRequirementsInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
        GeneratedCommandsMemoryRequirementsInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkGeneratedCommandsMemoryRequirementsInfoNV.html)) · Builder of [`GeneratedCommandsMemoryRequirementsInfoNV`] <br/> VkGeneratedCommandsMemoryRequirementsInfoNV - Structure specifying parameters for the reservation of preprocess buffer space\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_device_generated_commands\ntypedef struct VkGeneratedCommandsMemoryRequirementsInfoNV {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkPipelineBindPoint           pipelineBindPoint;\n    VkPipeline                    pipeline;\n    VkIndirectCommandsLayoutNV    indirectCommandsLayout;\n    uint32_t                      maxSequencesCount;\n} VkGeneratedCommandsMemoryRequirementsInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::pipeline_bind_point`] is the [`crate::vk::PipelineBindPoint`] of the[`Self::pipeline`] that this buffer memory is intended to be used with\n  during the execution.\n\n* [`Self::pipeline`] is the [`crate::vk::Pipeline`] that this buffer memory is\n  intended to be used with during the execution.\n\n* [`Self::indirect_commands_layout`] is the [`crate::vk::IndirectCommandsLayoutNV`]that this buffer memory is intended to be used with.\n\n* [`Self::max_sequences_count`] is the maximum number of sequences that this\n  buffer memory in combination with the other state provided **can** be used\n  with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-maxSequencesCount-02907  \n  [`Self::max_sequences_count`] **must** be less or equal to[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsPropertiesNV::max_indirect_sequence_count`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV`]\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-indirectCommandsLayout-parameter  \n  [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-VkGeneratedCommandsMemoryRequirementsInfoNV-commonparent  \n   Both of [`Self::indirect_commands_layout`], and [`Self::pipeline`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::IndirectCommandsLayoutNV`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_generated_commands_memory_requirements_nv`]\n"]
#[repr(transparent)]
pub struct GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a>(GeneratedCommandsMemoryRequirementsInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
        GeneratedCommandsMemoryRequirementsInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pipeline_bind_point(mut self, pipeline_bind_point: crate::vk1_0::PipelineBindPoint) -> Self {
        self.0.pipeline_bind_point = pipeline_bind_point as _;
        self
    }
    #[inline]
    pub fn pipeline(mut self, pipeline: crate::vk1_0::Pipeline) -> Self {
        self.0.pipeline = pipeline as _;
        self
    }
    #[inline]
    pub fn indirect_commands_layout(mut self, indirect_commands_layout: crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV) -> Self {
        self.0.indirect_commands_layout = indirect_commands_layout as _;
        self
    }
    #[inline]
    pub fn max_sequences_count(mut self, max_sequences_count: u32) -> Self {
        self.0.max_sequences_count = max_sequences_count as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> GeneratedCommandsMemoryRequirementsInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
    fn default() -> GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
    type Target = GeneratedCommandsMemoryRequirementsInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for GeneratedCommandsMemoryRequirementsInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceGeneratedCommandsFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceGeneratedCommandsFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceGeneratedCommandsPropertiesNV> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDeviceGeneratedCommandsPropertiesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "Provided by [`crate::extensions::nv_device_generated_commands`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdExecuteGeneratedCommandsNV.html)) · Function <br/> vkCmdExecuteGeneratedCommandsNV - Generate and execute commands on the device\n[](#_c_specification)C Specification\n----------\n\nThe actual generation of commands as well as their execution on the device\nis handled as single action with:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdExecuteGeneratedCommandsNV(\n    VkCommandBuffer                             commandBuffer,\n    VkBool32                                    isPreprocessed,\n    const VkGeneratedCommandsInfoNV*            pGeneratedCommandsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::is_preprocessed`] represents whether the input data has already been\n  preprocessed on the device.\n  If it is [`crate::vk::FALSE`] this command will implicitly trigger the\n  preprocessing step, otherwise not.\n\n* [`Self::p_generated_commands_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsInfoNV`] structure containing parameters\n  affecting the generation of commands.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-magFilter-04553  \n   If a [`crate::vk::Sampler`] created with `magFilter` or `minFilter`equal to [`crate::vk::Filter::LINEAR`] and `compareEnable` equal to[`crate::vk::FALSE`] is used to sample a [`crate::vk::ImageView`] as a result of this\n  command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-mipmapMode-04770  \n   If a [`crate::vk::Sampler`] created with `mipmapMode` equal to[`crate::vk::SamplerMipmapMode::LINEAR`] and `compareEnable` equal to[`crate::vk::FALSE`] is used to sample a [`crate::vk::ImageView`] as a result of this\n  command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02691  \n   If a [`crate::vk::ImageView`] is accessed using atomic operations as a result\n  of this command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::STORAGE_IMAGE_ATOMIC`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02692  \n   If a [`crate::vk::ImageView`] is sampled with [`crate::vk::Filter::CUBIC_EXT`] as a\n  result of this command, then the image view’s[format features](#resources-image-view-format-features) **must** contain[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_CUBIC_EXT`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-filterCubic-02694  \n   Any [`crate::vk::ImageView`] being sampled with [`crate::vk::Filter::CUBIC_EXT`] as a\n  result of this command **must** have a [`crate::vk::ImageViewType`] and format\n  that supports cubic filtering, as specified by[`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]::`filterCubic`returned by [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-filterCubicMinmax-02695  \n   Any [`crate::vk::ImageView`] being sampled with [`crate::vk::Filter::CUBIC_EXT`] with\n  a reduction mode of either [`crate::vk::SamplerReductionMode::MIN`] or[`crate::vk::SamplerReductionMode::MAX`] as a result of this command **must**have a [`crate::vk::ImageViewType`] and format that supports cubic filtering\n  together with minmax filtering, as specified by[`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]::`filterCubicMinmax`returned by [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-flags-02696  \n   Any [`crate::vk::Image`] created with a [`crate::vk::ImageCreateInfo::flags`]containing [`crate::vk::ImageCreateFlagBits::CORNER_SAMPLED_NV`] sampled as a\n  result of this command **must** only be sampled using a[`crate::vk::SamplerAddressMode`] of[`crate::vk::SamplerAddressMode::CLAMP_TO_EDGE`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02697  \n   For each set *n* that is statically used by the [`crate::vk::Pipeline`] bound\n  to the pipeline bind point used by this command, a descriptor set **must**have been bound to *n* at the same pipeline bind point, with a[`crate::vk::PipelineLayout`] that is compatible for set *n*, with the[`crate::vk::PipelineLayout`] used to create the current [`crate::vk::Pipeline`], as\n  described in [[descriptorsets-compatibility]](#descriptorsets-compatibility)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02698  \n   For each push constant that is statically used by the [`crate::vk::Pipeline`]bound to the pipeline bind point used by this command, a push constant\n  value **must** have been set for the same pipeline bind point, with a[`crate::vk::PipelineLayout`] that is compatible for push constants, with the[`crate::vk::PipelineLayout`] used to create the current [`crate::vk::Pipeline`], as\n  described in [[descriptorsets-compatibility]](#descriptorsets-compatibility)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02699  \n   Descriptors in each bound descriptor set, specified via[`crate::vk::PFN_vkCmdBindDescriptorSets`], **must** be valid if they are statically\n  used by the [`crate::vk::Pipeline`] bound to the pipeline bind point used by\n  this command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02700  \n   A valid pipeline **must** be bound to the pipeline bind point used by this\n  command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02701  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command requires any dynamic state, that state **must** have been set\n  or inherited (if the `[[VK_NV_inherited_viewport_scissor]](#VK_NV_inherited_viewport_scissor)` extension is\n  enabled) for [`Self::command_buffer`], and done so after any previously bound\n  pipeline with the corresponding state not specified as dynamic\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02859  \n   There **must** not have been any calls to dynamic state setting commands\n  for any state not specified as dynamic in the [`crate::vk::Pipeline`] object\n  bound to the pipeline bind point used by this command, since that\n  pipeline was bound\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02702  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used to sample from any[`crate::vk::Image`] with a [`crate::vk::ImageView`] of the type[`crate::vk::ImageViewType::_3D`], [`crate::vk::ImageViewType::CUBE`],[`crate::vk::ImageViewType::_1D_ARRAY`], [`crate::vk::ImageViewType::_2D_ARRAY`] or[`crate::vk::ImageViewType::CUBE_ARRAY`], in any shader stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02703  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used with any of the SPIR-V`OpImageSample*` or `OpImageSparseSample*` instructions with`ImplicitLod`, `Dref` or `Proj` in their name, in any shader\n  stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02704  \n   If the [`crate::vk::Pipeline`] object bound to the pipeline bind point used by\n  this command accesses a [`crate::vk::Sampler`] object that uses unnormalized\n  coordinates, that sampler **must** not be used with any of the SPIR-V`OpImageSample*` or `OpImageSparseSample*` instructions that includes a\n  LOD bias or any offset values, in any shader stage\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02705  \n   If the [robust buffer access](#features-robustBufferAccess) feature is\n  not enabled, and if the [`crate::vk::Pipeline`] object bound to the pipeline\n  bind point used by this command accesses a uniform buffer, it **must** not\n  access values outside of the range of the buffer as specified in the\n  descriptor set bound to the same pipeline bind point\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02706  \n   If the [robust buffer access](#features-robustBufferAccess) feature is\n  not enabled, and if the [`crate::vk::Pipeline`] object bound to the pipeline\n  bind point used by this command accesses a storage buffer, it **must** not\n  access values outside of the range of the buffer as specified in the\n  descriptor set bound to the same pipeline bind point\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02707  \n   If [`Self::command_buffer`] is an unprotected command buffer, any resource\n  accessed by the [`crate::vk::Pipeline`] object bound to the pipeline bind point\n  used by this command **must** not be a protected resource\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04115  \n   If a [`crate::vk::ImageView`] is accessed using `OpImageWrite` as a result\n  of this command, then the `Type` of the `Texel` operand of that\n  instruction **must** have at least as many components as the image view’s\n  format.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-OpImageWrite-04469  \n   If a [`crate::vk::BufferView`] is accessed using `OpImageWrite` as a result\n  of this command, then the `Type` of the `Texel` operand of that\n  instruction **must** have at least as many components as the buffer view’s\n  format.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04470  \n   If a [`crate::vk::ImageView`] with a [`crate::vk::Format`] that has a 64-bit channel\n  width is accessed as a result of this command, the `SampledType` of\n  the `OpTypeImage` operand of that instruction **must** have a `Width`of 64.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04471  \n   If a [`crate::vk::ImageView`] with a [`crate::vk::Format`] that has a channel width\n  less than 64-bit is accessed as a result of this command, the`SampledType` of the `OpTypeImage` operand of that instruction**must** have a `Width` of 32.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04472  \n   If a [`crate::vk::BufferView`] with a [`crate::vk::Format`] that has a 64-bit channel\n  width is accessed as a result of this command, the `SampledType` of\n  the `OpTypeImage` operand of that instruction **must** have a `Width`of 64.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-SampledType-04473  \n   If a [`crate::vk::BufferView`] with a [`crate::vk::Format`] that has a channel width\n  less than 64-bit is accessed as a result of this command, the`SampledType` of the `OpTypeImage` operand of that instruction**must** have a `Width` of 32.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sparseImageInt64Atomics-04474  \n   If the[`sparseImageInt64Atomics`](#features-sparseImageInt64Atomics)feature is not enabled, [`crate::vk::Image`] objects created with the[`crate::vk::ImageCreateFlagBits::SPARSE_RESIDENCY`] flag **must** not be accessed by\n  atomic instructions through an `OpTypeImage` with a `SampledType`with a `Width` of 64 by this command.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sparseImageInt64Atomics-04475  \n   If the[`sparseImageInt64Atomics`](#features-sparseImageInt64Atomics)feature is not enabled, [`crate::vk::Buffer`] objects created with the[`crate::vk::BufferCreateFlagBits::SPARSE_RESIDENCY`] flag **must** not be accessed\n  by atomic instructions through an `OpTypeImage` with a`SampledType` with a `Width` of 64 by this command.\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-renderPass-02684  \n   The current render pass **must** be [compatible](#renderpass-compatibility)with the `renderPass` member of the[`crate::vk::GraphicsPipelineCreateInfo`] structure specified when creating the[`crate::vk::Pipeline`] bound to [`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-subpass-02685  \n   The subpass index of the current render pass **must** be equal to the`subpass` member of the [`crate::vk::GraphicsPipelineCreateInfo`] structure\n  specified when creating the [`crate::vk::Pipeline`] bound to[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02686  \n   Every input attachment used by the current subpass **must** be bound to the\n  pipeline via a descriptor set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04584  \n   Image subresources used as attachments in the current render pass **must**not be accessed in any way other than as an attachment by this command,\n  except for cases involving read-only access to depth/stencil attachments\n  as described in the [Render Pass](#renderpass-attachment-nonattachment)chapter\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-maxMultiviewInstanceIndex-02688  \n   If the draw is recorded in a render pass instance with multiview\n  enabled, the maximum instance index **must** be less than or equal to[`crate::vk::PhysicalDeviceMultiviewProperties::max_multiview_instance_index`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-sampleLocationsEnable-02689  \n   If the bound graphics pipeline was created with[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT::sample_locations_enable`]set to [`crate::vk::TRUE`] and the current subpass has a depth/stencil\n  attachment, then that attachment **must** have been created with the[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] bit set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-03417  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] dynamic state\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] **must** have been called\n  in the current command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** match the[`crate::vk::PipelineViewportStateCreateInfo`]::`scissorCount` of the\n  pipeline\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-scissorCount-03418  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] dynamic state enabled, but\n  not the [`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must** have been called\n  in the current command buffer prior to this drawing command, and the`scissorCount` parameter of [`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must**match the [`crate::vk::PipelineViewportStateCreateInfo`]::`viewportCount`of the pipeline\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-03419  \n   If the bound graphics pipeline state was created with both the[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic states enabled\n  then both [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] and[`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** match the `scissorCount` parameter of[`crate::vk::DeviceLoader::cmd_set_scissor_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04137  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::VIEWPORT_W_SCALING_NV`] dynamic state\n  enabled, then the bound graphics pipeline **must** have been created with[`crate::vk::PipelineViewportWScalingStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04138  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_W_SCALING_NV`] dynamic states enabled then\n  the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_w_scaling_nv`] **must** be greater than or equal to the`viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04139  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  but not the [`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`]dynamic state enabled, then the bound graphics pipeline **must** have been\n  created with[`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-viewportCount-04140  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] and[`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`] dynamic states\n  enabled then the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_shading_rate_palette_nv`] **must** be greater than or\n  equal to the `viewportCount` parameter in the last call to[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-VkPipelineVieportCreateInfo-04141  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled and\n  an instance of [`crate::vk::PipelineViewportSwizzleStateCreateInfoNV`] chained\n  from `VkPipelineVieportCreateInfo`, then the bound graphics pipeline**must** have been created with[`crate::vk::PipelineViewportSwizzleStateCreateInfoNV::viewport_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-VkPipelineVieportCreateInfo-04142  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled and\n  an instance of [`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV`]chained from `VkPipelineVieportCreateInfo`, then the bound graphics\n  pipeline **must** have been created with[`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV::exclusive_scissor_count`]greater or equal to the `viewportCount` parameter in the last call\n  to [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-primitiveTopology-03420  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PRIMITIVE_TOPOLOGY_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_primitive_topology_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`primitiveTopology` parameter of [`crate::vk::DeviceLoader::cmd_set_primitive_topology_ext`]**must** be of the same [topology\n  class](#drawing-primitive-topology-class) as the pipeline[`crate::vk::PipelineInputAssemblyStateCreateInfo::topology`] state\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04875  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PATCH_CONTROL_POINTS_EXT`] dynamic state enabled\n  then [`crate::vk::DeviceLoader::cmd_set_patch_control_points_ext`] **must** have been called in the\n  current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04876  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::RASTERIZER_DISCARD_ENABLE_EXT`] dynamic state\n  enabled then [`crate::vk::DeviceLoader::cmd_set_rasterizer_discard_enable_ext`] **must** have been\n  called in the current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04877  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::DEPTH_BIAS_ENABLE_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_depth_bias_enable_ext`] **must** have been called in the current\n  command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-logicOp-04878  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::LOGIC_OP_EXT`] dynamic state enabled then[`crate::vk::DeviceLoader::cmd_set_logic_op_ext`] **must** have been called in the current command\n  buffer prior to this drawing command and the `logicOp` **must** be a\n  valid [`crate::vk::LogicOp`] value\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04879  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::PRIMITIVE_RESTART_ENABLE_EXT`] dynamic state\n  enabled then [`crate::vk::DeviceLoader::cmd_set_primitive_restart_enable_ext`] **must** have been\n  called in the current command buffer prior to this drawing command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-primitiveFragmentShadingRateWithMultipleViewports-04552  \n   If the [`primitiveFragmentShadingRateWithMultipleViewports`](#limits-primitiveFragmentShadingRateWithMultipleViewports) limit is not\n  supported, the bound graphics pipeline was created with the[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`] dynamic state enabled,\n  and any of the shader stages of the bound graphics pipeline write to the`PrimitiveShadingRateKHR` built-in, then[`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`] **must** have been called in the current\n  command buffer prior to this drawing command, and the`viewportCount` parameter of [`crate::vk::DeviceLoader::cmd_set_viewport_with_count_ext`]**must** be `1`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-blendEnable-04727  \n   If rasterization is not disabled in the bound graphics pipeline, then\n  for each color attachment in the subpass, if the corresponding image\n  view’s [format features](#resources-image-view-format-features) do not\n  contain [`crate::vk::FormatFeatureFlagBits::COLOR_ATTACHMENT_BLEND`], then the`blendEnable` member of the corresponding element of the`pAttachments` member of `pColorBlendState` **must** be[`crate::vk::FALSE`]\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-rasterizationSamples-04740  \n   If rasterization is not disabled in the bound graphics pipeline, and\n  neither the `[[VK_AMD_mixed_attachment_samples]](#VK_AMD_mixed_attachment_samples)` nor the`[[VK_NV_framebuffer_mixed_samples]](#VK_NV_framebuffer_mixed_samples)` extensions are enabled, then[`crate::vk::PipelineMultisampleStateCreateInfo::rasterization_samples`]**must** be the same as the current subpass color and/or depth/stencil\n  attachments\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04912  \n   If the bound graphics pipeline was created with both the[`crate::vk::DynamicState::VERTEX_INPUT_EXT`] and[`crate::vk::DynamicState::VERTEX_INPUT_BINDING_STRIDE_EXT`] dynamic states\n  enabled, then [`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`] **must** have been called in the\n  current command buffer prior to this draw command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pStrides-04913  \n   If the bound graphics pipeline was created with the[`crate::vk::DynamicState::VERTEX_INPUT_BINDING_STRIDE_EXT`] dynamic state\n  enabled, but not the [`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic\n  state enabled, then [`crate::vk::DeviceLoader::cmd_bind_vertex_buffers2_ext`] **must** have been\n  called in the current command buffer prior to this draw command, and the`pStrides` parameter of [`crate::vk::DeviceLoader::cmd_bind_vertex_buffers2_ext`] **must** not\n  be `NULL`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04914  \n   If the bound graphics pipeline state was created with the[`crate::vk::DynamicState::VERTEX_INPUT_EXT`] dynamic state enabled, then[`crate::vk::DeviceLoader::cmd_set_vertex_input_ext`] **must** have been called in the current\n  command buffer prior to this draw command\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04007  \n   All vertex input bindings accessed via vertex input variables declared\n  in the vertex shader entry point’s interface **must** have either valid or[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) buffers bound\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-04008  \n   If the [nullDescriptor](#features-nullDescriptor) feature is not\n  enabled, all vertex input bindings accessed via vertex input variables\n  declared in the vertex shader entry point’s interface **must** not be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02721  \n   For a given vertex buffer binding, any attribute data fetched **must** be\n  entirely contained within the corresponding vertex buffer binding, as\n  described in [[fxvertex-input]](#fxvertex-input)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-02970  \n  [`Self::command_buffer`] **must** not be a protected command buffer\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-isPreprocessed-02908  \n   If [`Self::is_preprocessed`] is [`crate::vk::TRUE`] then[`crate::vk::DeviceLoader::cmd_preprocess_generated_commands_nv`] **must** have already been\n  executed on the device, using the same [`Self::p_generated_commands_info`]content as well as the content of the input buffers it references (all\n  except [`crate::vk::GeneratedCommandsInfoNV::preprocess_buffer`]).\n  Furthermore [`Self::p_generated_commands_info`]`s `indirectCommandsLayout`**must** have been created with the[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::EXPLICIT_PREPROCESS_NV`] bit\n  set\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pipeline-02909  \n  [`crate::vk::GeneratedCommandsInfoNV`]::`pipeline` **must** match the current\n  bound pipeline at[`crate::vk::GeneratedCommandsInfoNV`]::`pipelineBindPoint`\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-None-02910  \n   Transform feedback **must** not be active\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-deviceGeneratedCommands-02911  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-pGeneratedCommandsInfo-parameter  \n  [`Self::p_generated_commands_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsInfoNV`] structure\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdExecuteGeneratedCommandsNV-renderpass  \n   This command **must** only be called inside of a render pass instance\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                  Inside                  |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CommandBuffer`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
    #[doc(alias = "vkCmdExecuteGeneratedCommandsNV")]
    pub unsafe fn cmd_execute_generated_commands_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, is_preprocessed: bool, generated_commands_info: &crate::extensions::nv_device_generated_commands::GeneratedCommandsInfoNV) -> () {
        let _function = self.cmd_execute_generated_commands_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, is_preprocessed as _, generated_commands_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdPreprocessGeneratedCommandsNV.html)) · Function <br/> vkCmdPreprocessGeneratedCommandsNV - Performs preprocessing for generated commands\n[](#_c_specification)C Specification\n----------\n\nCommands **can** be preprocessed prior execution using the following command:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdPreprocessGeneratedCommandsNV(\n    VkCommandBuffer                             commandBuffer,\n    const VkGeneratedCommandsInfoNV*            pGeneratedCommandsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer which does the preprocessing.\n\n* [`Self::p_generated_commands_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsInfoNV`] structure containing parameters\n  affecting the preprocessing step.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-02974  \n  [`Self::command_buffer`] **must** not be a protected command buffer\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-pGeneratedCommandsInfo-02927  \n  [`Self::p_generated_commands_info`]`s `indirectCommandsLayout` **must** have\n  been created with the[`crate::vk::IndirectCommandsLayoutUsageFlagBitsNV::EXPLICIT_PREPROCESS_NV`] bit\n  set\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-deviceGeneratedCommands-02928  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-pGeneratedCommandsInfo-parameter  \n  [`Self::p_generated_commands_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsInfoNV`] structure\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdPreprocessGeneratedCommandsNV-renderpass  \n   This command **must** only be called outside of a render pass instance\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                 Outside                  |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::GeneratedCommandsInfoNV`]\n"]
    #[doc(alias = "vkCmdPreprocessGeneratedCommandsNV")]
    pub unsafe fn cmd_preprocess_generated_commands_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, generated_commands_info: &crate::extensions::nv_device_generated_commands::GeneratedCommandsInfoNV) -> () {
        let _function = self.cmd_preprocess_generated_commands_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, generated_commands_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindPipelineShaderGroupNV.html)) · Function <br/> vkCmdBindPipelineShaderGroupNV - Bind a pipeline object\n[](#_c_specification)C Specification\n----------\n\nFor pipelines that were created with the support of multiple shader groups\n(see [Graphics Pipeline Shader Groups](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#graphics-shadergroups)), the regular[`crate::vk::PFN_vkCmdBindPipeline`] command will bind Shader Group `0`.\nTo explicitly bind a shader group use:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkCmdBindPipelineShaderGroupNV(\n    VkCommandBuffer                             commandBuffer,\n    VkPipelineBindPoint                         pipelineBindPoint,\n    VkPipeline                                  pipeline,\n    uint32_t                                    groupIndex);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer that the pipeline will be\n  bound to.\n\n* [`Self::pipeline_bind_point`] is a [`crate::vk::PipelineBindPoint`] value specifying\n  the bind point to which the pipeline will be bound.\n\n* [`Self::pipeline`] is the pipeline to be bound.\n\n* [`Self::group_index`] is the shader group to be bound.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-groupIndex-02893  \n  [`Self::group_index`] **must** be `0` or less than the effective[`crate::vk::GraphicsPipelineShaderGroupsCreateInfoNV::group_count`]including the referenced pipelines\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipelineBindPoint-02894  \n   The [`Self::pipeline_bind_point`] **must** be[`crate::vk::PipelineBindPoint::GRAPHICS`]\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-groupIndex-02895  \n   The same restrictions as [`crate::vk::PFN_vkCmdBindPipeline`] apply as if the bound\n  pipeline was created only with the Shader Group from the[`Self::group_index`] information\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-deviceGeneratedCommands-02896  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipelineBindPoint-parameter  \n  [`Self::pipeline_bind_point`] **must** be a valid [`crate::vk::PipelineBindPoint`] value\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\n* []() VUID-vkCmdBindPipelineShaderGroupNV-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::pipeline`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Pipeline`], [`crate::vk::PipelineBindPoint`]\n"]
    #[doc(alias = "vkCmdBindPipelineShaderGroupNV")]
    pub unsafe fn cmd_bind_pipeline_shader_group_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, pipeline_bind_point: crate::vk1_0::PipelineBindPoint, pipeline: crate::vk1_0::Pipeline, group_index: u32) -> () {
        let _function = self.cmd_bind_pipeline_shader_group_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, pipeline_bind_point as _, pipeline as _, group_index as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetGeneratedCommandsMemoryRequirementsNV.html)) · Function <br/> vkGetGeneratedCommandsMemoryRequirementsNV - Retrieve the buffer allocation requirements for generated commands\n[](#_c_specification)C Specification\n----------\n\nThe generation of commands on the device requires a `preprocess` buffer.\nTo retrieve the memory size and alignment requirements of a particular\nexecution state call:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkGetGeneratedCommandsMemoryRequirementsNV(\n    VkDevice                                    device,\n    const VkGeneratedCommandsMemoryRequirementsInfoNV* pInfo,\n    VkMemoryRequirements2*                      pMemoryRequirements);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the buffer.\n\n* [`Self::p_info`] is a pointer to an instance of the[`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`] structure containing\n  parameters required for the memory requirements query.\n\n* [`Self::p_memory_requirements`] is a pointer to a [`crate::vk::MemoryRequirements2`]structure in which the memory requirements of the buffer object are\n  returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-deviceGeneratedCommands-02906  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`] structure\n\n* []() VUID-vkGetGeneratedCommandsMemoryRequirementsNV-pMemoryRequirements-parameter  \n  [`Self::p_memory_requirements`] **must** be a valid pointer to a [`crate::vk::MemoryRequirements2`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::GeneratedCommandsMemoryRequirementsInfoNV`], [`crate::vk::MemoryRequirements2`]\n"]
    #[doc(alias = "vkGetGeneratedCommandsMemoryRequirementsNV")]
    pub unsafe fn get_generated_commands_memory_requirements_nv(&self, info: &crate::extensions::nv_device_generated_commands::GeneratedCommandsMemoryRequirementsInfoNV, memory_requirements: Option<crate::vk1_1::MemoryRequirements2>) -> crate::vk1_1::MemoryRequirements2 {
        let _function = self.get_generated_commands_memory_requirements_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut memory_requirements = match memory_requirements {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, info as _, &mut memory_requirements);
        {
            memory_requirements.p_next = std::ptr::null_mut() as _;
            memory_requirements
        }
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateIndirectCommandsLayoutNV.html)) · Function <br/> vkCreateIndirectCommandsLayoutNV - Create an indirect command layout object\n[](#_c_specification)C Specification\n----------\n\nIndirect command layouts are created by:\n\n```\n// Provided by VK_NV_device_generated_commands\nVkResult vkCreateIndirectCommandsLayoutNV(\n    VkDevice                                    device,\n    const VkIndirectCommandsLayoutCreateInfoNV* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkIndirectCommandsLayoutNV*                 pIndirectCommandsLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that creates the indirect command\n  layout.\n\n* [`Self::p_create_info`] is a pointer to an instance of the[`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure containing\n  parameters affecting creation of the indirect command layout.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_indirect_commands_layout`] is a pointer to a[`crate::vk::IndirectCommandsLayoutNV`] handle in which the resulting indirect\n  command layout is returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-deviceGeneratedCommands-02929  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::IndirectCommandsLayoutCreateInfoNV`] structure\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateIndirectCommandsLayoutNV-pIndirectCommandsLayout-parameter  \n  [`Self::p_indirect_commands_layout`] **must** be a valid pointer to a [`crate::vk::IndirectCommandsLayoutNV`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::IndirectCommandsLayoutCreateInfoNV`], [`crate::vk::IndirectCommandsLayoutNV`]\n"]
    #[doc(alias = "vkCreateIndirectCommandsLayoutNV")]
    pub unsafe fn create_indirect_commands_layout_nv(&self, create_info: &crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutCreateInfoNV, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV> {
        let _function = self.create_indirect_commands_layout_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut indirect_commands_layout = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut indirect_commands_layout,
        );
        crate::utils::VulkanResult::new(_return, indirect_commands_layout)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyIndirectCommandsLayoutNV.html)) · Function <br/> vkDestroyIndirectCommandsLayoutNV - Destroy an indirect commands layout\n[](#_c_specification)C Specification\n----------\n\nIndirect command layouts are destroyed by:\n\n```\n// Provided by VK_NV_device_generated_commands\nvoid vkDestroyIndirectCommandsLayoutNV(\n    VkDevice                                    device,\n    VkIndirectCommandsLayoutNV                  indirectCommandsLayout,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that destroys the layout.\n\n* [`Self::indirect_commands_layout`] is the layout to destroy.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02938  \n   All submitted commands that refer to [`Self::indirect_commands_layout`] **must**have completed execution\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02939  \n   If [`crate::vk::AllocationCallbacks`] were provided when[`Self::indirect_commands_layout`] was created, a compatible set of callbacks**must** be provided here\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-02940  \n   If no [`crate::vk::AllocationCallbacks`] were provided when[`Self::indirect_commands_layout`] was created, [`Self::p_allocator`] **must** be`NULL`\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-deviceGeneratedCommands-02941  \n   The [[`crate::vk::PhysicalDeviceDeviceGeneratedCommandsFeaturesNV`]::`deviceGeneratedCommands`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-deviceGeneratedCommands)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-parameter  \n   If [`Self::indirect_commands_layout`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::indirect_commands_layout`] **must** be a valid [`crate::vk::IndirectCommandsLayoutNV`] handle\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyIndirectCommandsLayoutNV-indirectCommandsLayout-parent  \n   If [`Self::indirect_commands_layout`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::indirect_commands_layout`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::IndirectCommandsLayoutNV`]\n"]
    #[doc(alias = "vkDestroyIndirectCommandsLayoutNV")]
    pub unsafe fn destroy_indirect_commands_layout_nv(&self, indirect_commands_layout: Option<crate::extensions::nv_device_generated_commands::IndirectCommandsLayoutNV>, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_indirect_commands_layout_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            match indirect_commands_layout {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
}
