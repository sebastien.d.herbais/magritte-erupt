#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_QNX_SCREEN_SURFACE_SPEC_VERSION")]
pub const QNX_SCREEN_SURFACE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_QNX_SCREEN_SURFACE_EXTENSION_NAME")]
pub const QNX_SCREEN_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_QNX_screen_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_SCREEN_SURFACE_QNX: *const std::os::raw::c_char = crate::cstr!("vkCreateScreenSurfaceQNX");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SCREEN_PRESENTATION_SUPPORT_QNX: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceScreenPresentationSupportQNX");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkScreenSurfaceCreateFlagsQNX.html)) · Bitmask of [`ScreenSurfaceCreateFlagBitsQNX`] <br/> VkScreenSurfaceCreateFlagsQNX - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_QNX_screen_surface\ntypedef VkFlags VkScreenSurfaceCreateFlagsQNX;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ScreenSurfaceCreateFlagBitsQNX`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ScreenSurfaceCreateInfoQNX`]\n"] # [doc (alias = "VkScreenSurfaceCreateFlagsQNX")] # [derive (Default)] # [repr (transparent)] pub struct ScreenSurfaceCreateFlagsQNX : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`ScreenSurfaceCreateFlagsQNX`] <br/> "]
#[doc(alias = "VkScreenSurfaceCreateFlagBitsQNX")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ScreenSurfaceCreateFlagBitsQNX(pub u32);
impl ScreenSurfaceCreateFlagBitsQNX {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ScreenSurfaceCreateFlagsQNX {
        ScreenSurfaceCreateFlagsQNX::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ScreenSurfaceCreateFlagBitsQNX {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::qnx_screen_surface`]"]
impl crate::vk1_0::StructureType {
    pub const SCREEN_SURFACE_CREATE_INFO_QNX: Self = Self(1000378000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateScreenSurfaceQNX.html)) · Function <br/> vkCreateScreenSurfaceQNX - Create a slink:VkSurfaceKHR object for a QNX Screen window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a QNX Screen surface, call:\n\n```\n// Provided by VK_QNX_screen_surface\nVkResult vkCreateScreenSurfaceQNX(\n    VkInstance                                  instance,\n    const VkScreenSurfaceCreateInfoQNX*         pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::ScreenSurfaceCreateInfoQNX`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateScreenSurfaceQNX-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateScreenSurfaceQNX-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::ScreenSurfaceCreateInfoQNX`] structure\n\n* []() VUID-vkCreateScreenSurfaceQNX-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateScreenSurfaceQNX-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::ScreenSurfaceCreateInfoQNX`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateScreenSurfaceQNX = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::qnx_screen_surface::ScreenSurfaceCreateInfoQNX, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceScreenPresentationSupportQNX.html)) · Function <br/> vkGetPhysicalDeviceScreenPresentationSupportQNX - Query physical device for presentation to QNX Screen\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to a QNX Screen compositor, call:\n\n```\n// Provided by VK_QNX_screen_surface\nVkBool32 vkGetPhysicalDeviceScreenPresentationSupportQNX(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    struct _screen_window*                      window);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::window`] is the QNX Screen [`Self::window`] object.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-queueFamilyIndex-04743  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-window-parameter  \n  [`Self::window`] **must** be a valid pointer to a `_screen_window` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceScreenPresentationSupportQNX = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, window: *mut std::ffi::c_void) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkScreenSurfaceCreateInfoQNX.html)) · Structure <br/> VkScreenSurfaceCreateInfoQNX - Structure specifying parameters of a newly created QNX Screen surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ScreenSurfaceCreateInfoQNX`] structure is defined as:\n\n```\n// Provided by VK_QNX_screen_surface\ntypedef struct VkScreenSurfaceCreateInfoQNX {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkScreenSurfaceCreateFlagsQNX    flags;\n    struct _screen_context*          context;\n    struct _screen_window*           window;\n} VkScreenSurfaceCreateInfoQNX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::context`] and [`Self::window`] are QNX Screen [`Self::context`] and[`Self::window`] to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-context-04741  \n  [`Self::context`] **must** point to a valid QNX Screen `struct`\\_screen\\_context\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-window-04742  \n  [`Self::window`] **must** point to a valid QNX Screen `struct`\\_screen\\_window\n\nValid Usage (Implicit)\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SCREEN_SURFACE_CREATE_INFO_QNX`]\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ScreenSurfaceCreateFlagBitsQNX`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_screen_surface_qnx`]\n"]
#[doc(alias = "VkScreenSurfaceCreateInfoQNX")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ScreenSurfaceCreateInfoQNX {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::qnx_screen_surface::ScreenSurfaceCreateFlagsQNX,
    pub context: *mut std::ffi::c_void,
    pub window: *mut std::ffi::c_void,
}
impl ScreenSurfaceCreateInfoQNX {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SCREEN_SURFACE_CREATE_INFO_QNX;
}
impl Default for ScreenSurfaceCreateInfoQNX {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), context: std::ptr::null_mut(), window: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ScreenSurfaceCreateInfoQNX {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ScreenSurfaceCreateInfoQNX").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("context", &self.context).field("window", &self.window).finish()
    }
}
impl ScreenSurfaceCreateInfoQNX {
    #[inline]
    pub fn into_builder<'a>(self) -> ScreenSurfaceCreateInfoQNXBuilder<'a> {
        ScreenSurfaceCreateInfoQNXBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkScreenSurfaceCreateInfoQNX.html)) · Builder of [`ScreenSurfaceCreateInfoQNX`] <br/> VkScreenSurfaceCreateInfoQNX - Structure specifying parameters of a newly created QNX Screen surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ScreenSurfaceCreateInfoQNX`] structure is defined as:\n\n```\n// Provided by VK_QNX_screen_surface\ntypedef struct VkScreenSurfaceCreateInfoQNX {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkScreenSurfaceCreateFlagsQNX    flags;\n    struct _screen_context*          context;\n    struct _screen_window*           window;\n} VkScreenSurfaceCreateInfoQNX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::context`] and [`Self::window`] are QNX Screen [`Self::context`] and[`Self::window`] to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-context-04741  \n  [`Self::context`] **must** point to a valid QNX Screen `struct`\\_screen\\_context\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-window-04742  \n  [`Self::window`] **must** point to a valid QNX Screen `struct`\\_screen\\_window\n\nValid Usage (Implicit)\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SCREEN_SURFACE_CREATE_INFO_QNX`]\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkScreenSurfaceCreateInfoQNX-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ScreenSurfaceCreateFlagBitsQNX`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_screen_surface_qnx`]\n"]
#[repr(transparent)]
pub struct ScreenSurfaceCreateInfoQNXBuilder<'a>(ScreenSurfaceCreateInfoQNX, std::marker::PhantomData<&'a ()>);
impl<'a> ScreenSurfaceCreateInfoQNXBuilder<'a> {
    #[inline]
    pub fn new() -> ScreenSurfaceCreateInfoQNXBuilder<'a> {
        ScreenSurfaceCreateInfoQNXBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::qnx_screen_surface::ScreenSurfaceCreateFlagsQNX) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn context(mut self, context: *mut std::ffi::c_void) -> Self {
        self.0.context = context;
        self
    }
    #[inline]
    pub fn window(mut self, window: *mut std::ffi::c_void) -> Self {
        self.0.window = window;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ScreenSurfaceCreateInfoQNX {
        self.0
    }
}
impl<'a> std::default::Default for ScreenSurfaceCreateInfoQNXBuilder<'a> {
    fn default() -> ScreenSurfaceCreateInfoQNXBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ScreenSurfaceCreateInfoQNXBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ScreenSurfaceCreateInfoQNXBuilder<'a> {
    type Target = ScreenSurfaceCreateInfoQNX;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ScreenSurfaceCreateInfoQNXBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::qnx_screen_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateScreenSurfaceQNX.html)) · Function <br/> vkCreateScreenSurfaceQNX - Create a slink:VkSurfaceKHR object for a QNX Screen window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a QNX Screen surface, call:\n\n```\n// Provided by VK_QNX_screen_surface\nVkResult vkCreateScreenSurfaceQNX(\n    VkInstance                                  instance,\n    const VkScreenSurfaceCreateInfoQNX*         pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::ScreenSurfaceCreateInfoQNX`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateScreenSurfaceQNX-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateScreenSurfaceQNX-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::ScreenSurfaceCreateInfoQNX`] structure\n\n* []() VUID-vkCreateScreenSurfaceQNX-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateScreenSurfaceQNX-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::ScreenSurfaceCreateInfoQNX`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateScreenSurfaceQNX")]
    pub unsafe fn create_screen_surface_qnx(&self, create_info: &crate::extensions::qnx_screen_surface::ScreenSurfaceCreateInfoQNX, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_screen_surface_qnx.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceScreenPresentationSupportQNX.html)) · Function <br/> vkGetPhysicalDeviceScreenPresentationSupportQNX - Query physical device for presentation to QNX Screen\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to a QNX Screen compositor, call:\n\n```\n// Provided by VK_QNX_screen_surface\nVkBool32 vkGetPhysicalDeviceScreenPresentationSupportQNX(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    struct _screen_window*                      window);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::window`] is the QNX Screen [`Self::window`] object.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-queueFamilyIndex-04743  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceScreenPresentationSupportQNX-window-parameter  \n  [`Self::window`] **must** be a valid pointer to a `_screen_window` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceScreenPresentationSupportQNX")]
    pub unsafe fn get_physical_device_screen_presentation_support_qnx(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, window: *mut std::ffi::c_void) -> bool {
        let _function = self.get_physical_device_screen_presentation_support_qnx.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _, window);
        _return != 0
    }
}
