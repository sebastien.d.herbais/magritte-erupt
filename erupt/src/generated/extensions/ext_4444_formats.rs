#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_4444_FORMATS_SPEC_VERSION")]
pub const EXT_4444_FORMATS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_4444_FORMATS_EXTENSION_NAME")]
pub const EXT_4444_FORMATS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_4444_formats");
#[doc = "Provided by [`crate::extensions::ext_4444_formats`]"]
impl crate::vk1_0::Format {
    pub const A4R4G4B4_UNORM_PACK16_EXT: Self = Self(1000340000);
    pub const A4B4G4R4_UNORM_PACK16_EXT: Self = Self(1000340001);
}
#[doc = "Provided by [`crate::extensions::ext_4444_formats`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT: Self = Self(1000340000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevice4444FormatsFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevice4444FormatsFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevice4444FormatsFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevice4444FormatsFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice4444FormatsFeaturesEXT.html)) · Structure <br/> VkPhysicalDevice4444FormatsFeaturesEXT - Structure describing additional 4444 formats supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_4444_formats\ntypedef struct VkPhysicalDevice4444FormatsFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           formatA4R4G4B4;\n    VkBool32           formatA4B4G4R4;\n} VkPhysicalDevice4444FormatsFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::format_a4r4g4b4`] indicates that the\n  implementation **must** support using a [`crate::vk::Format`] of[`crate::vk::Format::A4R4G4B4_UNORM_PACK16_EXT`] with at least the following[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html):\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`]\n\n  * [`crate::vk::FormatFeatureFlagBits::BLIT_SRC`]\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() [`Self::format_a4b4g4r4`] indicates that the\n  implementation **must** support using a [`crate::vk::Format`] of[`crate::vk::Format::A4B4G4R4_UNORM_PACK16_EXT`] with at least the following[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html):\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`]\n\n  * [`crate::vk::FormatFeatureFlagBits::BLIT_SRC`]\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\nIf the [`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevice4444FormatsFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevice4444FormatsFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevice4444FormatsFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub format_a4r4g4b4: crate::vk1_0::Bool32,
    pub format_a4b4g4r4: crate::vk1_0::Bool32,
}
impl PhysicalDevice4444FormatsFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT;
}
impl Default for PhysicalDevice4444FormatsFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), format_a4r4g4b4: Default::default(), format_a4b4g4r4: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevice4444FormatsFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevice4444FormatsFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("format_a4r4g4b4", &(self.format_a4r4g4b4 != 0)).field("format_a4b4g4r4", &(self.format_a4b4g4r4 != 0)).finish()
    }
}
impl PhysicalDevice4444FormatsFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
        PhysicalDevice4444FormatsFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevice4444FormatsFeaturesEXT.html)) · Builder of [`PhysicalDevice4444FormatsFeaturesEXT`] <br/> VkPhysicalDevice4444FormatsFeaturesEXT - Structure describing additional 4444 formats supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_4444_formats\ntypedef struct VkPhysicalDevice4444FormatsFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           formatA4R4G4B4;\n    VkBool32           formatA4B4G4R4;\n} VkPhysicalDevice4444FormatsFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::format_a4r4g4b4`] indicates that the\n  implementation **must** support using a [`crate::vk::Format`] of[`crate::vk::Format::A4R4G4B4_UNORM_PACK16_EXT`] with at least the following[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html):\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`]\n\n  * [`crate::vk::FormatFeatureFlagBits::BLIT_SRC`]\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\n* []() [`Self::format_a4b4g4r4`] indicates that the\n  implementation **must** support using a [`crate::vk::Format`] of[`crate::vk::Format::A4B4G4R4_UNORM_PACK16_EXT`] with at least the following[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html):\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`]\n\n  * [`crate::vk::FormatFeatureFlagBits::BLIT_SRC`]\n\n  * [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`]\n\nIf the [`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevice4444FormatsFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevice4444FormatsFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevice4444FormatsFeaturesEXTBuilder<'a>(PhysicalDevice4444FormatsFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
        PhysicalDevice4444FormatsFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn format_a4r4g4b4(mut self, format_a4r4g4b4: bool) -> Self {
        self.0.format_a4r4g4b4 = format_a4r4g4b4 as _;
        self
    }
    #[inline]
    pub fn format_a4b4g4r4(mut self, format_a4b4g4r4: bool) -> Self {
        self.0.format_a4b4g4r4 = format_a4b4g4r4 as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevice4444FormatsFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
    type Target = PhysicalDevice4444FormatsFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevice4444FormatsFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
