#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_SEMAPHORE_WIN32_SPEC_VERSION")]
pub const KHR_EXTERNAL_SEMAPHORE_WIN32_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_SEMAPHORE_WIN32_EXTENSION_NAME")]
pub const KHR_EXTERNAL_SEMAPHORE_WIN32_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_external_semaphore_win32");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_SEMAPHORE_WIN32_HANDLE_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetSemaphoreWin32HandleKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_IMPORT_SEMAPHORE_WIN32_HANDLE_KHR: *const std::os::raw::c_char = crate::cstr!("vkImportSemaphoreWin32HandleKHR");
#[doc = "Provided by [`crate::extensions::khr_external_semaphore_win32`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: Self = Self(1000078000);
    pub const EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: Self = Self(1000078001);
    pub const D3D12_FENCE_SUBMIT_INFO_KHR: Self = Self(1000078002);
    pub const SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR: Self = Self(1000078003);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSemaphoreWin32HandleKHR.html)) · Function <br/> vkGetSemaphoreWin32HandleKHR - Get a Windows HANDLE for a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the payload of a semaphore, call:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\nVkResult vkGetSemaphoreWin32HandleKHR(\n    VkDevice                                    device,\n    const VkSemaphoreGetWin32HandleInfoKHR*     pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure containing parameters\n  of the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the semaphore\n  state.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`] are owned by the application.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\nExporting a Windows handle from a semaphore **may** have side effects depending\non the transference of the specified handle type, as described in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SemaphoreGetWin32HandleInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetSemaphoreWin32HandleKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_win32_handle_info: *const crate::extensions::khr_external_semaphore_win32::SemaphoreGetWin32HandleInfoKHR, p_handle: *mut *mut std::ffi::c_void) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportSemaphoreWin32HandleKHR.html)) · Function <br/> vkImportSemaphoreWin32HandleKHR - Import a semaphore from a Windows HANDLE\n[](#_c_specification)C Specification\n----------\n\nTo import a semaphore payload from a Windows handle, call:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\nVkResult vkImportSemaphoreWin32HandleKHR(\n    VkDevice                                    device,\n    const VkImportSemaphoreWin32HandleInfoKHR*  pImportSemaphoreWin32HandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore.\n\n* [`Self::p_import_semaphore_win32_handle_info`] is a pointer to a[`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure specifying the\n  semaphore and import parameters.\n[](#_description)Description\n----------\n\nImporting a semaphore payload from Windows handles does not transfer\nownership of the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\n\nApplications **can** import the same semaphore payload into multiple instances\nof Vulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportSemaphoreWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportSemaphoreWin32HandleKHR-pImportSemaphoreWin32HandleInfo-parameter  \n  [`Self::p_import_semaphore_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkImportSemaphoreWin32HandleKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_import_semaphore_win32_handle_info: *const crate::extensions::khr_external_semaphore_win32::ImportSemaphoreWin32HandleInfoKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ExportSemaphoreWin32HandleInfoKHR> for crate::vk1_0::SemaphoreCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportSemaphoreWin32HandleInfoKHRBuilder<'_>> for crate::vk1_0::SemaphoreCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, D3D12FenceSubmitInfoKHR> for crate::vk1_0::SubmitInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, D3D12FenceSubmitInfoKHRBuilder<'_>> for crate::vk1_0::SubmitInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportSemaphoreWin32HandleInfoKHR.html)) · Structure <br/> VkImportSemaphoreWin32HandleInfoKHR - Structure specifying Windows handle to import to a semaphore\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkImportSemaphoreWin32HandleInfoKHR {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkSemaphoreImportFlags                   flags;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n    HANDLE                                   handle;\n    LPCWSTR                                  name;\n} VkImportSemaphoreWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore into which the payload will be\n  imported.\n\n* [`Self::flags`] is a bitmask of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) specifying\n  additional parameters for the semaphore payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  underlying synchronization primitive to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                      Handle Type                       |Transference|Permanence Supported|\n|--------------------------------------------------------|------------|--------------------|\n|  [`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`]  | Reference  |Temporary,Permanent |\n|[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`]| Reference  |Temporary,Permanent |\n|  [`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`]   | Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01140  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportSemaphoreWin32HandleInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphore-handletypes-win32) table\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01466  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`], [`Self::name`]**must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01467  \n   If [`Self::handle`] is `NULL`, [`Self::name`] **must** name a valid synchronization\n  primitive of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01468  \n   If [`Self::name`] is `NULL`, [`Self::handle`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handle-01469  \n   If [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handle-01542  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-name-01543  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-03261  \n   If [`Self::handle_type`] is[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`], the[`crate::vk::SemaphoreCreateInfo::flags`] field **must** match that of the\n  semaphore from which [`Self::handle`] or [`Self::name`] was exported\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-03262  \n   If [`Self::handle_type`] is[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`], the[`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field **must** match\n  that of the semaphore from which [`Self::handle`] or [`Self::name`] was exported\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-flags-03322  \n   If [`Self::flags`] contains [`crate::vk::SemaphoreImportFlagBits::TEMPORARY`], the[`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field of the\n  semaphore from which [`Self::handle`] or [`Self::name`] was exported **must** not\n  be [`crate::vk::SemaphoreType::TIMELINE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) values\n\nHost Synchronization\n\n* Host access to [`Self::semaphore`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::SemaphoreImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_semaphore_win32_handle_khr`]\n"]
#[doc(alias = "VkImportSemaphoreWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportSemaphoreWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub semaphore: crate::vk1_0::Semaphore,
    pub flags: crate::vk1_1::SemaphoreImportFlags,
    pub handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits,
    pub handle: *mut std::ffi::c_void,
    pub name: *const u16,
}
impl ImportSemaphoreWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR;
}
impl Default for ImportSemaphoreWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), semaphore: Default::default(), flags: Default::default(), handle_type: Default::default(), handle: std::ptr::null_mut(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ImportSemaphoreWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportSemaphoreWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("semaphore", &self.semaphore).field("flags", &self.flags).field("handle_type", &self.handle_type).field("handle", &self.handle).field("name", &self.name).finish()
    }
}
impl ImportSemaphoreWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        ImportSemaphoreWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportSemaphoreWin32HandleInfoKHR.html)) · Builder of [`ImportSemaphoreWin32HandleInfoKHR`] <br/> VkImportSemaphoreWin32HandleInfoKHR - Structure specifying Windows handle to import to a semaphore\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkImportSemaphoreWin32HandleInfoKHR {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkSemaphoreImportFlags                   flags;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n    HANDLE                                   handle;\n    LPCWSTR                                  name;\n} VkImportSemaphoreWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore into which the payload will be\n  imported.\n\n* [`Self::flags`] is a bitmask of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) specifying\n  additional parameters for the semaphore payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  underlying synchronization primitive to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                      Handle Type                       |Transference|Permanence Supported|\n|--------------------------------------------------------|------------|--------------------|\n|  [`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`]  | Reference  |Temporary,Permanent |\n|[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`]| Reference  |Temporary,Permanent |\n|  [`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`]   | Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01140  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportSemaphoreWin32HandleInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphore-handletypes-win32) table\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01466  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`], [`Self::name`]**must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01467  \n   If [`Self::handle`] is `NULL`, [`Self::name`] **must** name a valid synchronization\n  primitive of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-01468  \n   If [`Self::name`] is `NULL`, [`Self::handle`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handle-01469  \n   If [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handle-01542  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-name-01543  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-03261  \n   If [`Self::handle_type`] is[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`], the[`crate::vk::SemaphoreCreateInfo::flags`] field **must** match that of the\n  semaphore from which [`Self::handle`] or [`Self::name`] was exported\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-handleType-03262  \n   If [`Self::handle_type`] is[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32_KMT`], the[`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field **must** match\n  that of the semaphore from which [`Self::handle`] or [`Self::name`] was exported\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-flags-03322  \n   If [`Self::flags`] contains [`crate::vk::SemaphoreImportFlagBits::TEMPORARY`], the[`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field of the\n  semaphore from which [`Self::handle`] or [`Self::name`] was exported **must** not\n  be [`crate::vk::SemaphoreType::TIMELINE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkImportSemaphoreWin32HandleInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) values\n\nHost Synchronization\n\n* Host access to [`Self::semaphore`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::SemaphoreImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_semaphore_win32_handle_khr`]\n"]
#[repr(transparent)]
pub struct ImportSemaphoreWin32HandleInfoKHRBuilder<'a>(ImportSemaphoreWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        ImportSemaphoreWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn semaphore(mut self, semaphore: crate::vk1_0::Semaphore) -> Self {
        self.0.semaphore = semaphore as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::vk1_1::SemaphoreImportFlags) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn handle(mut self, handle: *mut std::ffi::c_void) -> Self {
        self.0.handle = handle;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportSemaphoreWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    type Target = ImportSemaphoreWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportSemaphoreWin32HandleInfoKHR.html)) · Structure <br/> VkExportSemaphoreWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a semaphore,\nadd a [`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] structure to the [`Self::p_next`]chain of the [`crate::vk::SemaphoreCreateInfo`] structure.\nThe [`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkExportSemaphoreWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportSemaphoreWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  underlying synchronization primitive referenced by NT handles exported\n  from the created semaphore.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportSemaphoreCreateInfo`] is not included in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportSemaphoreCreateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::SemaphoreCreateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights used depend on\nthe handle type.\n\nFor handles of the following types:\n\n[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`]\n\nThe implementation **must** ensure the access rights allow both signal and wait\noperations on the semaphore.\n\nFor handles of the following types:\n\n[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`]\n\nThe access rights **must** be:\n\n`GENERIC_ALL`\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-handleTypes-01125  \n   If [`crate::vk::ExportSemaphoreCreateInfo::handle_types`] does not include[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`],[`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] **must** not be included in the[`Self::p_next`] chain of [`crate::vk::SemaphoreCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExportSemaphoreWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExportSemaphoreWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_attributes: *const std::ffi::c_void,
    pub dw_access: u32,
    pub name: *const u16,
}
impl ExportSemaphoreWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR;
}
impl Default for ExportSemaphoreWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_attributes: std::ptr::null(), dw_access: Default::default(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ExportSemaphoreWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExportSemaphoreWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_attributes", &self.p_attributes).field("dw_access", &self.dw_access).field("name", &self.name).finish()
    }
}
impl ExportSemaphoreWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        ExportSemaphoreWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportSemaphoreWin32HandleInfoKHR.html)) · Builder of [`ExportSemaphoreWin32HandleInfoKHR`] <br/> VkExportSemaphoreWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a semaphore,\nadd a [`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] structure to the [`Self::p_next`]chain of the [`crate::vk::SemaphoreCreateInfo`] structure.\nThe [`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkExportSemaphoreWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportSemaphoreWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  underlying synchronization primitive referenced by NT handles exported\n  from the created semaphore.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportSemaphoreCreateInfo`] is not included in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportSemaphoreCreateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::SemaphoreCreateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights used depend on\nthe handle type.\n\nFor handles of the following types:\n\n[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`]\n\nThe implementation **must** ensure the access rights allow both signal and wait\noperations on the semaphore.\n\nFor handles of the following types:\n\n[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`]\n\nThe access rights **must** be:\n\n`GENERIC_ALL`\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-handleTypes-01125  \n   If [`crate::vk::ExportSemaphoreCreateInfo::handle_types`] does not include[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::OPAQUE_WIN32`] or[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`],[`crate::vk::ExportSemaphoreWin32HandleInfoKHR`] **must** not be included in the[`Self::p_next`] chain of [`crate::vk::SemaphoreCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportSemaphoreWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExportSemaphoreWin32HandleInfoKHRBuilder<'a>(ExportSemaphoreWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        ExportSemaphoreWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attributes(mut self, attributes: *const std::ffi::c_void) -> Self {
        self.0.p_attributes = attributes;
        self
    }
    #[inline]
    pub fn dw_access(mut self, dw_access: u32) -> Self {
        self.0.dw_access = dw_access as _;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExportSemaphoreWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    type Target = ExportSemaphoreWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExportSemaphoreWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkD3D12FenceSubmitInfoKHR.html)) · Structure <br/> VkD3D12FenceSubmitInfoKHR - Structure specifying values for Direct3D 12 fence-backed semaphores\n[](#_c_specification)C Specification\n----------\n\nTo specify the values to use when waiting for and signaling semaphores whose[current payload](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing) refers to a\nDirect3D 12 fence, add a [`crate::vk::D3D12FenceSubmitInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::SubmitInfo`] structure.\nThe [`crate::vk::D3D12FenceSubmitInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkD3D12FenceSubmitInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           waitSemaphoreValuesCount;\n    const uint64_t*    pWaitSemaphoreValues;\n    uint32_t           signalSemaphoreValuesCount;\n    const uint64_t*    pSignalSemaphoreValues;\n} VkD3D12FenceSubmitInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::wait_semaphore_values_count`] is the number of semaphore wait values\n  specified in [`Self::p_wait_semaphore_values`].\n\n* [`Self::p_wait_semaphore_values`] is a pointer to an array of[`Self::wait_semaphore_values_count`] values for the corresponding semaphores\n  in [`crate::vk::SubmitInfo::p_wait_semaphores`] to wait for.\n\n* [`Self::signal_semaphore_values_count`] is the number of semaphore signal\n  values specified in [`Self::p_signal_semaphore_values`].\n\n* [`Self::p_signal_semaphore_values`] is a pointer to an array of[`Self::signal_semaphore_values_count`] values for the corresponding semaphores\n  in [`crate::vk::SubmitInfo::p_signal_semaphores`] to set when signaled.\n[](#_description)Description\n----------\n\nIf the semaphore in [`crate::vk::SubmitInfo::p_wait_semaphores`] or[`crate::vk::SubmitInfo::p_signal_semaphores`] corresponding to an entry in[`Self::p_wait_semaphore_values`] or [`Self::p_signal_semaphore_values`] respectively does\nnot currently have a [payload](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-payloads)referring to a Direct3D 12 fence, the implementation **must** ignore the value\nin the [`Self::p_wait_semaphore_values`] or [`Self::p_signal_semaphore_values`] entry.\n\n|   |Note<br/><br/>As the introduction of the external semaphore handle type[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`] predates that of<br/>timeline semaphores, support for importing semaphore payloads from external<br/>handles of that type into semaphores created (implicitly or explicitly) with<br/>a [`crate::vk::SemaphoreType`] of [`crate::vk::SemaphoreType::BINARY`] is preserved for<br/>backwards compatibility.<br/>However, applications **should** prefer importing such handle types into<br/>semaphores created with a [`crate::vk::SemaphoreType`] of[`crate::vk::SemaphoreType::TIMELINE`], and use the[`crate::vk::TimelineSemaphoreSubmitInfo`] structure instead of the[`crate::vk::D3D12FenceSubmitInfoKHR`] structure to specify the values to use when<br/>waiting for and signaling such semaphores.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-waitSemaphoreValuesCount-00079  \n  [`Self::wait_semaphore_values_count`] **must** be the same value as[`crate::vk::SubmitInfo`]::`waitSemaphoreCount`, where [`crate::vk::SubmitInfo`]is in the [`Self::p_next`] chain of this [`crate::vk::D3D12FenceSubmitInfoKHR`]structure.\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-signalSemaphoreValuesCount-00080  \n  [`Self::signal_semaphore_values_count`] **must** be the same value as[`crate::vk::SubmitInfo`]::`signalSemaphoreCount`, where [`crate::vk::SubmitInfo`]is in the [`Self::p_next`] chain of this [`crate::vk::D3D12FenceSubmitInfoKHR`]structure.\n\nValid Usage (Implicit)\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::D3D12_FENCE_SUBMIT_INFO_KHR`]\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-pWaitSemaphoreValues-parameter  \n   If [`Self::wait_semaphore_values_count`] is not `0`, and [`Self::p_wait_semaphore_values`] is not `NULL`, [`Self::p_wait_semaphore_values`] **must** be a valid pointer to an array of [`Self::wait_semaphore_values_count`] `uint64_t` values\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-pSignalSemaphoreValues-parameter  \n   If [`Self::signal_semaphore_values_count`] is not `0`, and [`Self::p_signal_semaphore_values`] is not `NULL`, [`Self::p_signal_semaphore_values`] **must** be a valid pointer to an array of [`Self::signal_semaphore_values_count`] `uint64_t` values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkD3D12FenceSubmitInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct D3D12FenceSubmitInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub wait_semaphore_values_count: u32,
    pub p_wait_semaphore_values: *const u64,
    pub signal_semaphore_values_count: u32,
    pub p_signal_semaphore_values: *const u64,
}
impl D3D12FenceSubmitInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::D3D12_FENCE_SUBMIT_INFO_KHR;
}
impl Default for D3D12FenceSubmitInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), wait_semaphore_values_count: Default::default(), p_wait_semaphore_values: std::ptr::null(), signal_semaphore_values_count: Default::default(), p_signal_semaphore_values: std::ptr::null() }
    }
}
impl std::fmt::Debug for D3D12FenceSubmitInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("D3D12FenceSubmitInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("wait_semaphore_values_count", &self.wait_semaphore_values_count).field("p_wait_semaphore_values", &self.p_wait_semaphore_values).field("signal_semaphore_values_count", &self.signal_semaphore_values_count).field("p_signal_semaphore_values", &self.p_signal_semaphore_values).finish()
    }
}
impl D3D12FenceSubmitInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> D3D12FenceSubmitInfoKHRBuilder<'a> {
        D3D12FenceSubmitInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkD3D12FenceSubmitInfoKHR.html)) · Builder of [`D3D12FenceSubmitInfoKHR`] <br/> VkD3D12FenceSubmitInfoKHR - Structure specifying values for Direct3D 12 fence-backed semaphores\n[](#_c_specification)C Specification\n----------\n\nTo specify the values to use when waiting for and signaling semaphores whose[current payload](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing) refers to a\nDirect3D 12 fence, add a [`crate::vk::D3D12FenceSubmitInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::SubmitInfo`] structure.\nThe [`crate::vk::D3D12FenceSubmitInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkD3D12FenceSubmitInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           waitSemaphoreValuesCount;\n    const uint64_t*    pWaitSemaphoreValues;\n    uint32_t           signalSemaphoreValuesCount;\n    const uint64_t*    pSignalSemaphoreValues;\n} VkD3D12FenceSubmitInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::wait_semaphore_values_count`] is the number of semaphore wait values\n  specified in [`Self::p_wait_semaphore_values`].\n\n* [`Self::p_wait_semaphore_values`] is a pointer to an array of[`Self::wait_semaphore_values_count`] values for the corresponding semaphores\n  in [`crate::vk::SubmitInfo::p_wait_semaphores`] to wait for.\n\n* [`Self::signal_semaphore_values_count`] is the number of semaphore signal\n  values specified in [`Self::p_signal_semaphore_values`].\n\n* [`Self::p_signal_semaphore_values`] is a pointer to an array of[`Self::signal_semaphore_values_count`] values for the corresponding semaphores\n  in [`crate::vk::SubmitInfo::p_signal_semaphores`] to set when signaled.\n[](#_description)Description\n----------\n\nIf the semaphore in [`crate::vk::SubmitInfo::p_wait_semaphores`] or[`crate::vk::SubmitInfo::p_signal_semaphores`] corresponding to an entry in[`Self::p_wait_semaphore_values`] or [`Self::p_signal_semaphore_values`] respectively does\nnot currently have a [payload](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-payloads)referring to a Direct3D 12 fence, the implementation **must** ignore the value\nin the [`Self::p_wait_semaphore_values`] or [`Self::p_signal_semaphore_values`] entry.\n\n|   |Note<br/><br/>As the introduction of the external semaphore handle type[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::D3D12_FENCE`] predates that of<br/>timeline semaphores, support for importing semaphore payloads from external<br/>handles of that type into semaphores created (implicitly or explicitly) with<br/>a [`crate::vk::SemaphoreType`] of [`crate::vk::SemaphoreType::BINARY`] is preserved for<br/>backwards compatibility.<br/>However, applications **should** prefer importing such handle types into<br/>semaphores created with a [`crate::vk::SemaphoreType`] of[`crate::vk::SemaphoreType::TIMELINE`], and use the[`crate::vk::TimelineSemaphoreSubmitInfo`] structure instead of the[`crate::vk::D3D12FenceSubmitInfoKHR`] structure to specify the values to use when<br/>waiting for and signaling such semaphores.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-waitSemaphoreValuesCount-00079  \n  [`Self::wait_semaphore_values_count`] **must** be the same value as[`crate::vk::SubmitInfo`]::`waitSemaphoreCount`, where [`crate::vk::SubmitInfo`]is in the [`Self::p_next`] chain of this [`crate::vk::D3D12FenceSubmitInfoKHR`]structure.\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-signalSemaphoreValuesCount-00080  \n  [`Self::signal_semaphore_values_count`] **must** be the same value as[`crate::vk::SubmitInfo`]::`signalSemaphoreCount`, where [`crate::vk::SubmitInfo`]is in the [`Self::p_next`] chain of this [`crate::vk::D3D12FenceSubmitInfoKHR`]structure.\n\nValid Usage (Implicit)\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::D3D12_FENCE_SUBMIT_INFO_KHR`]\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-pWaitSemaphoreValues-parameter  \n   If [`Self::wait_semaphore_values_count`] is not `0`, and [`Self::p_wait_semaphore_values`] is not `NULL`, [`Self::p_wait_semaphore_values`] **must** be a valid pointer to an array of [`Self::wait_semaphore_values_count`] `uint64_t` values\n\n* []() VUID-VkD3D12FenceSubmitInfoKHR-pSignalSemaphoreValues-parameter  \n   If [`Self::signal_semaphore_values_count`] is not `0`, and [`Self::p_signal_semaphore_values`] is not `NULL`, [`Self::p_signal_semaphore_values`] **must** be a valid pointer to an array of [`Self::signal_semaphore_values_count`] `uint64_t` values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct D3D12FenceSubmitInfoKHRBuilder<'a>(D3D12FenceSubmitInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> D3D12FenceSubmitInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> D3D12FenceSubmitInfoKHRBuilder<'a> {
        D3D12FenceSubmitInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn wait_semaphore_values(mut self, wait_semaphore_values: &'a [u64]) -> Self {
        self.0.p_wait_semaphore_values = wait_semaphore_values.as_ptr() as _;
        self.0.wait_semaphore_values_count = wait_semaphore_values.len() as _;
        self
    }
    #[inline]
    pub fn signal_semaphore_values(mut self, signal_semaphore_values: &'a [u64]) -> Self {
        self.0.p_signal_semaphore_values = signal_semaphore_values.as_ptr() as _;
        self.0.signal_semaphore_values_count = signal_semaphore_values.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> D3D12FenceSubmitInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for D3D12FenceSubmitInfoKHRBuilder<'a> {
    fn default() -> D3D12FenceSubmitInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for D3D12FenceSubmitInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for D3D12FenceSubmitInfoKHRBuilder<'a> {
    type Target = D3D12FenceSubmitInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for D3D12FenceSubmitInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreGetWin32HandleInfoKHR.html)) · Structure <br/> VkSemaphoreGetWin32HandleInfoKHR - Structure describing a Win32 handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkSemaphoreGetWin32HandleInfoKHR {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n} VkSemaphoreGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) for a description of the\nproperties of the defined external semaphore handle types.\n\nValid Usage\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01126  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportSemaphoreCreateInfo::handle_types`] when the[`Self::semaphore`]’s current payload was created\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01127  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::semaphore`] and [`Self::handle_type`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-semaphore-01128  \n  [`Self::semaphore`] **must** not currently have its payload replaced by an\n  imported payload as described below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing)unless that imported payload’s handle type was included in[`crate::vk::ExternalSemaphoreProperties::export_from_imported_handle_types`]for [`Self::handle_type`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01129  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, as defined below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing),\n  there **must** be no queue waiting on [`Self::semaphore`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01130  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::semaphore`] **must** be signaled, or have an\n  associated [semaphore signal\n  operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-signaling) pending execution\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01131  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`]\n"]
#[doc(alias = "VkSemaphoreGetWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SemaphoreGetWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub semaphore: crate::vk1_0::Semaphore,
    pub handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits,
}
impl SemaphoreGetWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR;
}
impl Default for SemaphoreGetWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), semaphore: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for SemaphoreGetWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SemaphoreGetWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("semaphore", &self.semaphore).field("handle_type", &self.handle_type).finish()
    }
}
impl SemaphoreGetWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
        SemaphoreGetWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreGetWin32HandleInfoKHR.html)) · Builder of [`SemaphoreGetWin32HandleInfoKHR`] <br/> VkSemaphoreGetWin32HandleInfoKHR - Structure describing a Win32 handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\ntypedef struct VkSemaphoreGetWin32HandleInfoKHR {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n} VkSemaphoreGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) for a description of the\nproperties of the defined external semaphore handle types.\n\nValid Usage\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01126  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportSemaphoreCreateInfo::handle_types`] when the[`Self::semaphore`]’s current payload was created\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01127  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::semaphore`] and [`Self::handle_type`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-semaphore-01128  \n  [`Self::semaphore`] **must** not currently have its payload replaced by an\n  imported payload as described below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing)unless that imported payload’s handle type was included in[`crate::vk::ExternalSemaphoreProperties::export_from_imported_handle_types`]for [`Self::handle_type`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01129  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, as defined below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing),\n  there **must** be no queue waiting on [`Self::semaphore`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01130  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::semaphore`] **must** be signaled, or have an\n  associated [semaphore signal\n  operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-signaling) pending execution\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-01131  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkSemaphoreGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`]\n"]
#[repr(transparent)]
pub struct SemaphoreGetWin32HandleInfoKHRBuilder<'a>(SemaphoreGetWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
        SemaphoreGetWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn semaphore(mut self, semaphore: crate::vk1_0::Semaphore) -> Self {
        self.0.semaphore = semaphore as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SemaphoreGetWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
    fn default() -> SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
    type Target = SemaphoreGetWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SemaphoreGetWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_external_semaphore_win32`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSemaphoreWin32HandleKHR.html)) · Function <br/> vkGetSemaphoreWin32HandleKHR - Get a Windows HANDLE for a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the payload of a semaphore, call:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\nVkResult vkGetSemaphoreWin32HandleKHR(\n    VkDevice                                    device,\n    const VkSemaphoreGetWin32HandleInfoKHR*     pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure containing parameters\n  of the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the semaphore\n  state.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_semaphore_win32_handle_khr`] are owned by the application.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\nExporting a Windows handle from a semaphore **may** have side effects depending\non the transference of the specified handle type, as described in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::SemaphoreGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetSemaphoreWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SemaphoreGetWin32HandleInfoKHR`]\n"]
    #[doc(alias = "vkGetSemaphoreWin32HandleKHR")]
    pub unsafe fn get_semaphore_win32_handle_khr(&self, get_win32_handle_info: &crate::extensions::khr_external_semaphore_win32::SemaphoreGetWin32HandleInfoKHR, handle: *mut *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_semaphore_win32_handle_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, get_win32_handle_info as _, handle);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportSemaphoreWin32HandleKHR.html)) · Function <br/> vkImportSemaphoreWin32HandleKHR - Import a semaphore from a Windows HANDLE\n[](#_c_specification)C Specification\n----------\n\nTo import a semaphore payload from a Windows handle, call:\n\n```\n// Provided by VK_KHR_external_semaphore_win32\nVkResult vkImportSemaphoreWin32HandleKHR(\n    VkDevice                                    device,\n    const VkImportSemaphoreWin32HandleInfoKHR*  pImportSemaphoreWin32HandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore.\n\n* [`Self::p_import_semaphore_win32_handle_info`] is a pointer to a[`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure specifying the\n  semaphore and import parameters.\n[](#_description)Description\n----------\n\nImporting a semaphore payload from Windows handles does not transfer\nownership of the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\n\nApplications **can** import the same semaphore payload into multiple instances\nof Vulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportSemaphoreWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportSemaphoreWin32HandleKHR-pImportSemaphoreWin32HandleInfo-parameter  \n  [`Self::p_import_semaphore_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportSemaphoreWin32HandleInfoKHR`]\n"]
    #[doc(alias = "vkImportSemaphoreWin32HandleKHR")]
    pub unsafe fn import_semaphore_win32_handle_khr(&self, import_semaphore_win32_handle_info: &crate::extensions::khr_external_semaphore_win32::ImportSemaphoreWin32HandleInfoKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.import_semaphore_win32_handle_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, import_semaphore_win32_handle_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
