#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_MEMORY_BUDGET_SPEC_VERSION")]
pub const EXT_MEMORY_BUDGET_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_MEMORY_BUDGET_EXTENSION_NAME")]
pub const EXT_MEMORY_BUDGET_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_memory_budget");
#[doc = "Provided by [`crate::extensions::ext_memory_budget`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT: Self = Self(1000237000);
}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMemoryBudgetPropertiesEXT> for crate::vk1_1::PhysicalDeviceMemoryProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceMemoryProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryBudgetPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceMemoryBudgetPropertiesEXT - Structure specifying physical device memory budget and usage\n[](#_c_specification)C Specification\n----------\n\nIf the [`crate::vk::PhysicalDeviceMemoryBudgetPropertiesEXT`] structure is included\nin the [`Self::p_next`] chain of [`crate::vk::PhysicalDeviceMemoryProperties2`], it is\nfilled with the current memory budgets and usages.\n\nThe [`crate::vk::PhysicalDeviceMemoryBudgetPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_budget\ntypedef struct VkPhysicalDeviceMemoryBudgetPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       heapBudget[VK_MAX_MEMORY_HEAPS];\n    VkDeviceSize       heapUsage[VK_MAX_MEMORY_HEAPS];\n} VkPhysicalDeviceMemoryBudgetPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::heap_budget`] is an array of [`crate::vk::MAX_MEMORY_HEAPS`][`crate::vk::DeviceSize`] values in which memory budgets are returned, with\n  one element for each memory heap.\n  A heap’s budget is a rough estimate of how much memory the process **can**allocate from that heap before allocations **may** fail or cause\n  performance degradation.\n  The budget includes any currently allocated device memory.\n\n* [`Self::heap_usage`] is an array of [`crate::vk::MAX_MEMORY_HEAPS`][`crate::vk::DeviceSize`] values in which memory usages are returned, with\n  one element for each memory heap.\n  A heap’s usage is an estimate of how much memory the process is\n  currently using in that heap.\n[](#_description)Description\n----------\n\nThe values returned in this structure are not invariant.\nThe [`Self::heap_budget`] and [`Self::heap_usage`] values **must** be zero for array\nelements greater than or equal to[`crate::vk::PhysicalDeviceMemoryProperties::memory_heap_count`].\nThe [`Self::heap_budget`] value **must** be non-zero for array elements less than[`crate::vk::PhysicalDeviceMemoryProperties::memory_heap_count`].\nThe [`Self::heap_budget`] value **must** be less than or equal to[`crate::vk::MemoryHeap::size`] for each heap.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMemoryBudgetPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceMemoryBudgetPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceMemoryBudgetPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub heap_budget: [crate::vk1_0::DeviceSize; 16],
    pub heap_usage: [crate::vk1_0::DeviceSize; 16],
}
impl PhysicalDeviceMemoryBudgetPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceMemoryBudgetPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), heap_budget: unsafe { std::mem::zeroed() }, heap_usage: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for PhysicalDeviceMemoryBudgetPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceMemoryBudgetPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("heap_budget", &self.heap_budget).field("heap_usage", &self.heap_usage).finish()
    }
}
impl PhysicalDeviceMemoryBudgetPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
        PhysicalDeviceMemoryBudgetPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMemoryBudgetPropertiesEXT.html)) · Builder of [`PhysicalDeviceMemoryBudgetPropertiesEXT`] <br/> VkPhysicalDeviceMemoryBudgetPropertiesEXT - Structure specifying physical device memory budget and usage\n[](#_c_specification)C Specification\n----------\n\nIf the [`crate::vk::PhysicalDeviceMemoryBudgetPropertiesEXT`] structure is included\nin the [`Self::p_next`] chain of [`crate::vk::PhysicalDeviceMemoryProperties2`], it is\nfilled with the current memory budgets and usages.\n\nThe [`crate::vk::PhysicalDeviceMemoryBudgetPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_memory_budget\ntypedef struct VkPhysicalDeviceMemoryBudgetPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       heapBudget[VK_MAX_MEMORY_HEAPS];\n    VkDeviceSize       heapUsage[VK_MAX_MEMORY_HEAPS];\n} VkPhysicalDeviceMemoryBudgetPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::heap_budget`] is an array of [`crate::vk::MAX_MEMORY_HEAPS`][`crate::vk::DeviceSize`] values in which memory budgets are returned, with\n  one element for each memory heap.\n  A heap’s budget is a rough estimate of how much memory the process **can**allocate from that heap before allocations **may** fail or cause\n  performance degradation.\n  The budget includes any currently allocated device memory.\n\n* [`Self::heap_usage`] is an array of [`crate::vk::MAX_MEMORY_HEAPS`][`crate::vk::DeviceSize`] values in which memory usages are returned, with\n  one element for each memory heap.\n  A heap’s usage is an estimate of how much memory the process is\n  currently using in that heap.\n[](#_description)Description\n----------\n\nThe values returned in this structure are not invariant.\nThe [`Self::heap_budget`] and [`Self::heap_usage`] values **must** be zero for array\nelements greater than or equal to[`crate::vk::PhysicalDeviceMemoryProperties::memory_heap_count`].\nThe [`Self::heap_budget`] value **must** be non-zero for array elements less than[`crate::vk::PhysicalDeviceMemoryProperties::memory_heap_count`].\nThe [`Self::heap_budget`] value **must** be less than or equal to[`crate::vk::MemoryHeap::size`] for each heap.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMemoryBudgetPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a>(PhysicalDeviceMemoryBudgetPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
        PhysicalDeviceMemoryBudgetPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn heap_budget(mut self, heap_budget: [crate::vk1_0::DeviceSize; 16]) -> Self {
        self.0.heap_budget = heap_budget as _;
        self
    }
    #[inline]
    pub fn heap_usage(mut self, heap_usage: [crate::vk1_0::DeviceSize; 16]) -> Self {
        self.0.heap_usage = heap_usage as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceMemoryBudgetPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceMemoryBudgetPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceMemoryBudgetPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
