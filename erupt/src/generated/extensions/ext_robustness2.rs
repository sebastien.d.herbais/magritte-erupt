#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_ROBUSTNESS_2_SPEC_VERSION")]
pub const EXT_ROBUSTNESS_2_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_ROBUSTNESS_2_EXTENSION_NAME")]
pub const EXT_ROBUSTNESS_2_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_robustness2");
#[doc = "Provided by [`crate::extensions::ext_robustness2`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT: Self = Self(1000286000);
    pub const PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT: Self = Self(1000286001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRobustness2FeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRobustness2FeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRobustness2FeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRobustness2FeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRobustness2PropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRobustness2PropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRobustness2FeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceRobustness2FeaturesEXT - Structure describing the out-of-bounds behavior for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_robustness2\ntypedef struct VkPhysicalDeviceRobustness2FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           robustBufferAccess2;\n    VkBool32           robustImageAccess2;\n    VkBool32           nullDescriptor;\n} VkPhysicalDeviceRobustness2FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::robust_buffer_access2`] indicates\n  whether buffer accesses are tightly bounds-checked against the range of\n  the descriptor.\n  Uniform buffers **must** be bounds-checked to the range of the descriptor,\n  where the range is rounded up to a multiple of[robustUniformBufferAccessSizeAlignment](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-robustUniformBufferAccessSizeAlignment).\n  Storage buffers **must** be bounds-checked to the range of the descriptor,\n  where the range is rounded up to a multiple of[robustStorageBufferAccessSizeAlignment](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-robustStorageBufferAccessSizeAlignment).\n  Out of bounds buffer loads will return zero values, and formatted loads\n  will have (0,0,1) values inserted for missing G, B, or A\n  components based on the format.\n\n* []() [`Self::robust_image_access2`] indicates\n  whether image accesses are tightly bounds-checked against the dimensions\n  of the image view.\n  Out of bounds image loads will return zero values, with (0,0,1)values [inserted for missing G, B, or A\n  components](#textures-conversion-to-rgba) based on the format.\n\n* []() [`Self::null_descriptor`] indicates whether\n  descriptors **can** be written with a [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) resource or\n  view, which are considered valid to access and act as if the descriptor\n  were bound to nothing.\n\nIf the [`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceRobustness2FeaturesEXT-robustBufferAccess2-04000  \n   If [`Self::robust_buffer_access2`] is enabled then[`robustBufferAccess`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess) **must** also be\n  enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRobustness2FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceRobustness2FeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceRobustness2FeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub robust_buffer_access2: crate::vk1_0::Bool32,
    pub robust_image_access2: crate::vk1_0::Bool32,
    pub null_descriptor: crate::vk1_0::Bool32,
}
impl PhysicalDeviceRobustness2FeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT;
}
impl Default for PhysicalDeviceRobustness2FeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), robust_buffer_access2: Default::default(), robust_image_access2: Default::default(), null_descriptor: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceRobustness2FeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceRobustness2FeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("robust_buffer_access2", &(self.robust_buffer_access2 != 0)).field("robust_image_access2", &(self.robust_image_access2 != 0)).field("null_descriptor", &(self.null_descriptor != 0)).finish()
    }
}
impl PhysicalDeviceRobustness2FeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
        PhysicalDeviceRobustness2FeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRobustness2FeaturesEXT.html)) · Builder of [`PhysicalDeviceRobustness2FeaturesEXT`] <br/> VkPhysicalDeviceRobustness2FeaturesEXT - Structure describing the out-of-bounds behavior for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_robustness2\ntypedef struct VkPhysicalDeviceRobustness2FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           robustBufferAccess2;\n    VkBool32           robustImageAccess2;\n    VkBool32           nullDescriptor;\n} VkPhysicalDeviceRobustness2FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::robust_buffer_access2`] indicates\n  whether buffer accesses are tightly bounds-checked against the range of\n  the descriptor.\n  Uniform buffers **must** be bounds-checked to the range of the descriptor,\n  where the range is rounded up to a multiple of[robustUniformBufferAccessSizeAlignment](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-robustUniformBufferAccessSizeAlignment).\n  Storage buffers **must** be bounds-checked to the range of the descriptor,\n  where the range is rounded up to a multiple of[robustStorageBufferAccessSizeAlignment](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-robustStorageBufferAccessSizeAlignment).\n  Out of bounds buffer loads will return zero values, and formatted loads\n  will have (0,0,1) values inserted for missing G, B, or A\n  components based on the format.\n\n* []() [`Self::robust_image_access2`] indicates\n  whether image accesses are tightly bounds-checked against the dimensions\n  of the image view.\n  Out of bounds image loads will return zero values, with (0,0,1)values [inserted for missing G, B, or A\n  components](#textures-conversion-to-rgba) based on the format.\n\n* []() [`Self::null_descriptor`] indicates whether\n  descriptors **can** be written with a [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) resource or\n  view, which are considered valid to access and act as if the descriptor\n  were bound to nothing.\n\nIf the [`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRobustness2FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceRobustness2FeaturesEXT-robustBufferAccess2-04000  \n   If [`Self::robust_buffer_access2`] is enabled then[`robustBufferAccess`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess) **must** also be\n  enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRobustness2FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceRobustness2FeaturesEXTBuilder<'a>(PhysicalDeviceRobustness2FeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
        PhysicalDeviceRobustness2FeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn robust_buffer_access2(mut self, robust_buffer_access2: bool) -> Self {
        self.0.robust_buffer_access2 = robust_buffer_access2 as _;
        self
    }
    #[inline]
    pub fn robust_image_access2(mut self, robust_image_access2: bool) -> Self {
        self.0.robust_image_access2 = robust_image_access2 as _;
        self
    }
    #[inline]
    pub fn null_descriptor(mut self, null_descriptor: bool) -> Self {
        self.0.null_descriptor = null_descriptor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceRobustness2FeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceRobustness2FeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceRobustness2FeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRobustness2PropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceRobustness2PropertiesEXT - Structure describing robust buffer access properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRobustness2PropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_robustness2\ntypedef struct VkPhysicalDeviceRobustness2PropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       robustStorageBufferAccessSizeAlignment;\n    VkDeviceSize       robustUniformBufferAccessSizeAlignment;\n} VkPhysicalDeviceRobustness2PropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::robust_storage_buffer_access_size_alignment`] is the number of bytes that\n  the range of a storage buffer descriptor is rounded up to when used for\n  bounds-checking when[`robustBufferAccess2`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess2) is enabled.\n  This value is either 1 or 4.\n\n* []()[`Self::robust_uniform_buffer_access_size_alignment`] is the number of bytes that\n  the range of a uniform buffer descriptor is rounded up to when used for\n  bounds-checking when[`robustBufferAccess2`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess2) is enabled.\n  This value is a power of two in the range [1, 256].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceRobustness2PropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRobustness2PropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceRobustness2PropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceRobustness2PropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub robust_storage_buffer_access_size_alignment: crate::vk1_0::DeviceSize,
    pub robust_uniform_buffer_access_size_alignment: crate::vk1_0::DeviceSize,
}
impl PhysicalDeviceRobustness2PropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceRobustness2PropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), robust_storage_buffer_access_size_alignment: Default::default(), robust_uniform_buffer_access_size_alignment: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceRobustness2PropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceRobustness2PropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("robust_storage_buffer_access_size_alignment", &self.robust_storage_buffer_access_size_alignment).field("robust_uniform_buffer_access_size_alignment", &self.robust_uniform_buffer_access_size_alignment).finish()
    }
}
impl PhysicalDeviceRobustness2PropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
        PhysicalDeviceRobustness2PropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRobustness2PropertiesEXT.html)) · Builder of [`PhysicalDeviceRobustness2PropertiesEXT`] <br/> VkPhysicalDeviceRobustness2PropertiesEXT - Structure describing robust buffer access properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRobustness2PropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_robustness2\ntypedef struct VkPhysicalDeviceRobustness2PropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       robustStorageBufferAccessSizeAlignment;\n    VkDeviceSize       robustUniformBufferAccessSizeAlignment;\n} VkPhysicalDeviceRobustness2PropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::robust_storage_buffer_access_size_alignment`] is the number of bytes that\n  the range of a storage buffer descriptor is rounded up to when used for\n  bounds-checking when[`robustBufferAccess2`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess2) is enabled.\n  This value is either 1 or 4.\n\n* []()[`Self::robust_uniform_buffer_access_size_alignment`] is the number of bytes that\n  the range of a uniform buffer descriptor is rounded up to when used for\n  bounds-checking when[`robustBufferAccess2`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-robustBufferAccess2) is enabled.\n  This value is a power of two in the range [1, 256].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceRobustness2PropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRobustness2PropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceRobustness2PropertiesEXTBuilder<'a>(PhysicalDeviceRobustness2PropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
        PhysicalDeviceRobustness2PropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn robust_storage_buffer_access_size_alignment(mut self, robust_storage_buffer_access_size_alignment: crate::vk1_0::DeviceSize) -> Self {
        self.0.robust_storage_buffer_access_size_alignment = robust_storage_buffer_access_size_alignment as _;
        self
    }
    #[inline]
    pub fn robust_uniform_buffer_access_size_alignment(mut self, robust_uniform_buffer_access_size_alignment: crate::vk1_0::DeviceSize) -> Self {
        self.0.robust_uniform_buffer_access_size_alignment = robust_uniform_buffer_access_size_alignment as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceRobustness2PropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceRobustness2PropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceRobustness2PropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
