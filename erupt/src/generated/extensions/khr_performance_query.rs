#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PERFORMANCE_QUERY_SPEC_VERSION")]
pub const KHR_PERFORMANCE_QUERY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PERFORMANCE_QUERY_EXTENSION_NAME")]
pub const KHR_PERFORMANCE_QUERY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_performance_query");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_ENUMERATE_PHYSICAL_DEVICE_QUEUE_FAMILY_PERFORMANCE_QUERY_COUNTERS_KHR: *const std::os::raw::c_char = crate::cstr!("vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_QUEUE_FAMILY_PERFORMANCE_QUERY_PASSES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_ACQUIRE_PROFILING_LOCK_KHR: *const std::os::raw::c_char = crate::cstr!("vkAcquireProfilingLockKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_RELEASE_PROFILING_LOCK_KHR: *const std::os::raw::c_char = crate::cstr!("vkReleaseProfilingLockKHR");
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::vk1_0::QueryType {
    pub const PERFORMANCE_QUERY_KHR: Self = Self(1000116000);
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR: Self = Self(1000116000);
    pub const PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR: Self = Self(1000116001);
    pub const QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR: Self = Self(1000116002);
    pub const PERFORMANCE_QUERY_SUBMIT_INFO_KHR: Self = Self(1000116003);
    pub const ACQUIRE_PROFILING_LOCK_INFO_KHR: Self = Self(1000116004);
    pub const PERFORMANCE_COUNTER_KHR: Self = Self(1000116005);
    pub const PERFORMANCE_COUNTER_DESCRIPTION_KHR: Self = Self(1000116006);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterScopeKHR.html)) · Enum <br/> VkPerformanceCounterScopeKHR - Supported counter scope types\n[](#_c_specification)C Specification\n----------\n\nPerformance counters have an associated scope.\nThis scope describes the granularity of a performance counter.\n\nThe performance counter scope types which **may** be returned in[`crate::vk::PerformanceCounterKHR::scope`] are:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef enum VkPerformanceCounterScopeKHR {\n    VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR = 0,\n    VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR = 1,\n    VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR = 2,\n    VK_QUERY_SCOPE_COMMAND_BUFFER_KHR = VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR,\n    VK_QUERY_SCOPE_RENDER_PASS_KHR = VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR,\n    VK_QUERY_SCOPE_COMMAND_KHR = VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR,\n} VkPerformanceCounterScopeKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::COMMAND_BUFFER_KHR`] - the performance\n  counter scope is a single complete command buffer.\n\n* [`Self::RENDER_PASS_KHR`] - the performance\n  counter scope is zero or more complete render passes.\n  The performance query containing the performance counter **must** begin and\n  end outside a render pass instance.\n\n* [`Self::COMMAND_KHR`] - the performance counter\n  scope is zero or more commands.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterKHR`]\n"]
#[doc(alias = "VkPerformanceCounterScopeKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceCounterScopeKHR(pub i32);
impl std::fmt::Debug for PerformanceCounterScopeKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::COMMAND_BUFFER_KHR => "COMMAND_BUFFER_KHR",
            &Self::RENDER_PASS_KHR => "RENDER_PASS_KHR",
            &Self::COMMAND_KHR => "COMMAND_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::extensions::khr_performance_query::PerformanceCounterScopeKHR {
    pub const COMMAND_BUFFER_KHR: Self = Self(0);
    pub const RENDER_PASS_KHR: Self = Self(1);
    pub const COMMAND_KHR: Self = Self(2);
    pub const QUERY_SCOPE_COMMAND_BUFFER_KHR: Self = Self::COMMAND_BUFFER_KHR;
    pub const QUERY_SCOPE_RENDER_PASS_KHR: Self = Self::RENDER_PASS_KHR;
    pub const QUERY_SCOPE_COMMAND_KHR: Self = Self::COMMAND_KHR;
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterUnitKHR.html)) · Enum <br/> VkPerformanceCounterUnitKHR - Supported counter unit types\n[](#_c_specification)C Specification\n----------\n\nPerformance counters have an associated unit.\nThis unit describes how to interpret the performance counter result.\n\nThe performance counter unit types which **may** be returned in[`crate::vk::PerformanceCounterKHR::unit`] are:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef enum VkPerformanceCounterUnitKHR {\n    VK_PERFORMANCE_COUNTER_UNIT_GENERIC_KHR = 0,\n    VK_PERFORMANCE_COUNTER_UNIT_PERCENTAGE_KHR = 1,\n    VK_PERFORMANCE_COUNTER_UNIT_NANOSECONDS_KHR = 2,\n    VK_PERFORMANCE_COUNTER_UNIT_BYTES_KHR = 3,\n    VK_PERFORMANCE_COUNTER_UNIT_BYTES_PER_SECOND_KHR = 4,\n    VK_PERFORMANCE_COUNTER_UNIT_KELVIN_KHR = 5,\n    VK_PERFORMANCE_COUNTER_UNIT_WATTS_KHR = 6,\n    VK_PERFORMANCE_COUNTER_UNIT_VOLTS_KHR = 7,\n    VK_PERFORMANCE_COUNTER_UNIT_AMPS_KHR = 8,\n    VK_PERFORMANCE_COUNTER_UNIT_HERTZ_KHR = 9,\n    VK_PERFORMANCE_COUNTER_UNIT_CYCLES_KHR = 10,\n} VkPerformanceCounterUnitKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::GENERIC_KHR`] - the performance counter\n  unit is a generic data point.\n\n* [`Self::PERCENTAGE_KHR`] - the performance\n  counter unit is a percentage (%).\n\n* [`Self::NANOSECONDS_KHR`] - the performance\n  counter unit is a value of nanoseconds (ns).\n\n* [`Self::BYTES_KHR`] - the performance counter\n  unit is a value of bytes.\n\n* [`Self::BYTES_PER_SECOND_KHR`] - the performance\n  counter unit is a value of bytes/s.\n\n* [`Self::KELVIN_KHR`] - the performance counter\n  unit is a temperature reported in Kelvin.\n\n* [`Self::WATTS_KHR`] - the performance counter\n  unit is a value of watts (W).\n\n* [`Self::VOLTS_KHR`] - the performance counter\n  unit is a value of volts (V).\n\n* [`Self::AMPS_KHR`] - the performance counter\n  unit is a value of amps (A).\n\n* [`Self::HERTZ_KHR`] - the performance counter\n  unit is a value of hertz (Hz).\n\n* [`Self::CYCLES_KHR`] - the performance counter\n  unit is a value of cycles.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterKHR`]\n"]
#[doc(alias = "VkPerformanceCounterUnitKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceCounterUnitKHR(pub i32);
impl std::fmt::Debug for PerformanceCounterUnitKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::GENERIC_KHR => "GENERIC_KHR",
            &Self::PERCENTAGE_KHR => "PERCENTAGE_KHR",
            &Self::NANOSECONDS_KHR => "NANOSECONDS_KHR",
            &Self::BYTES_KHR => "BYTES_KHR",
            &Self::BYTES_PER_SECOND_KHR => "BYTES_PER_SECOND_KHR",
            &Self::KELVIN_KHR => "KELVIN_KHR",
            &Self::WATTS_KHR => "WATTS_KHR",
            &Self::VOLTS_KHR => "VOLTS_KHR",
            &Self::AMPS_KHR => "AMPS_KHR",
            &Self::HERTZ_KHR => "HERTZ_KHR",
            &Self::CYCLES_KHR => "CYCLES_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::extensions::khr_performance_query::PerformanceCounterUnitKHR {
    pub const GENERIC_KHR: Self = Self(0);
    pub const PERCENTAGE_KHR: Self = Self(1);
    pub const NANOSECONDS_KHR: Self = Self(2);
    pub const BYTES_KHR: Self = Self(3);
    pub const BYTES_PER_SECOND_KHR: Self = Self(4);
    pub const KELVIN_KHR: Self = Self(5);
    pub const WATTS_KHR: Self = Self(6);
    pub const VOLTS_KHR: Self = Self(7);
    pub const AMPS_KHR: Self = Self(8);
    pub const HERTZ_KHR: Self = Self(9);
    pub const CYCLES_KHR: Self = Self(10);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterStorageKHR.html)) · Enum <br/> VkPerformanceCounterStorageKHR - Supported counter storage types\n[](#_c_specification)C Specification\n----------\n\nPerformance counters have an associated storage.\nThis storage describes the payload of a counter result.\n\nThe performance counter storage types which **may** be returned in[`crate::vk::PerformanceCounterKHR::storage`] are:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef enum VkPerformanceCounterStorageKHR {\n    VK_PERFORMANCE_COUNTER_STORAGE_INT32_KHR = 0,\n    VK_PERFORMANCE_COUNTER_STORAGE_INT64_KHR = 1,\n    VK_PERFORMANCE_COUNTER_STORAGE_UINT32_KHR = 2,\n    VK_PERFORMANCE_COUNTER_STORAGE_UINT64_KHR = 3,\n    VK_PERFORMANCE_COUNTER_STORAGE_FLOAT32_KHR = 4,\n    VK_PERFORMANCE_COUNTER_STORAGE_FLOAT64_KHR = 5,\n} VkPerformanceCounterStorageKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::INT32_KHR`] - the performance counter\n  storage is a 32-bit signed integer.\n\n* [`Self::INT64_KHR`] - the performance counter\n  storage is a 64-bit signed integer.\n\n* [`Self::UINT32_KHR`] - the performance\n  counter storage is a 32-bit unsigned integer.\n\n* [`Self::UINT64_KHR`] - the performance\n  counter storage is a 64-bit unsigned integer.\n\n* [`Self::FLOAT32_KHR`] - the performance\n  counter storage is a 32-bit floating-point.\n\n* [`Self::FLOAT64_KHR`] - the performance\n  counter storage is a 64-bit floating-point.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterKHR`]\n"]
#[doc(alias = "VkPerformanceCounterStorageKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceCounterStorageKHR(pub i32);
impl std::fmt::Debug for PerformanceCounterStorageKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INT32_KHR => "INT32_KHR",
            &Self::INT64_KHR => "INT64_KHR",
            &Self::UINT32_KHR => "UINT32_KHR",
            &Self::UINT64_KHR => "UINT64_KHR",
            &Self::FLOAT32_KHR => "FLOAT32_KHR",
            &Self::FLOAT64_KHR => "FLOAT64_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::extensions::khr_performance_query::PerformanceCounterStorageKHR {
    pub const INT32_KHR: Self = Self(0);
    pub const INT64_KHR: Self = Self(1);
    pub const UINT32_KHR: Self = Self(2);
    pub const UINT64_KHR: Self = Self(3);
    pub const FLOAT32_KHR: Self = Self(4);
    pub const FLOAT64_KHR: Self = Self(5);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagsKHR.html)) · Bitmask of [`PerformanceCounterDescriptionFlagBitsKHR`] <br/> VkPerformanceCounterDescriptionFlagsKHR - Bitmask of VkPerformanceCounterDescriptionFlagBitsKHR\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_performance_query\ntypedef VkFlags VkPerformanceCounterDescriptionFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PerformanceCounterDescriptionFlagBitsKHR`] is a bitmask type for setting\na mask of zero or more [VkPerformanceCounterDescriptionFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagBitsKHR.html).\n[](#_see_also)See Also\n----------\n\n[VkPerformanceCounterDescriptionFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagBitsKHR.html), [`crate::vk::PerformanceCounterDescriptionKHR`]\n"] # [doc (alias = "VkPerformanceCounterDescriptionFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct PerformanceCounterDescriptionFlagsKHR : u32 { const PERFORMANCE_IMPACTING_KHR = PerformanceCounterDescriptionFlagBitsKHR :: PERFORMANCE_IMPACTING_KHR . 0 ; const CONCURRENTLY_IMPACTED_KHR = PerformanceCounterDescriptionFlagBitsKHR :: CONCURRENTLY_IMPACTED_KHR . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagBitsKHR.html)) · Bits enum of [`PerformanceCounterDescriptionFlagsKHR`] <br/> VkPerformanceCounterDescriptionFlagBitsKHR - Bitmask specifying usage behavior for a counter\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::PerformanceCounterDescriptionKHR::flags`] to specify usage\nbehavior for a performance counter are:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef enum VkPerformanceCounterDescriptionFlagBitsKHR {\n    VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR = 0x00000001,\n    VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR = 0x00000002,\n    VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_KHR = VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR,\n    VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_KHR = VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR,\n} VkPerformanceCounterDescriptionFlagBitsKHR;\n```\n[](#_description)Description\n----------\n\n* [`Self::PERFORMANCE_IMPACTING_KHR`]specifies that recording the counter **may** have a noticeable performance\n  impact.\n\n* [`Self::CONCURRENTLY_IMPACTED_KHR`]specifies that concurrently recording the counter while other submitted\n  command buffers are running **may** impact the accuracy of the recording.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterDescriptionFlagBitsKHR`]\n"]
#[doc(alias = "VkPerformanceCounterDescriptionFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceCounterDescriptionFlagBitsKHR(pub u32);
impl PerformanceCounterDescriptionFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PerformanceCounterDescriptionFlagsKHR {
        PerformanceCounterDescriptionFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PerformanceCounterDescriptionFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::PERFORMANCE_IMPACTING_KHR => "PERFORMANCE_IMPACTING_KHR",
            &Self::CONCURRENTLY_IMPACTED_KHR => "CONCURRENTLY_IMPACTED_KHR",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::extensions::khr_performance_query::PerformanceCounterDescriptionFlagBitsKHR {
    pub const PERFORMANCE_IMPACTING_KHR: Self = Self(1);
    pub const CONCURRENTLY_IMPACTED_KHR: Self = Self(2);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAcquireProfilingLockFlagsKHR.html)) · Bitmask of [`AcquireProfilingLockFlagBitsKHR`] <br/> VkAcquireProfilingLockFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_performance_query\ntypedef VkFlags VkAcquireProfilingLockFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::AcquireProfilingLockFlagBitsKHR`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[VkAcquireProfilingLockFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAcquireProfilingLockFlagBitsKHR.html), [`crate::vk::AcquireProfilingLockInfoKHR`]\n"] # [doc (alias = "VkAcquireProfilingLockFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct AcquireProfilingLockFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`AcquireProfilingLockFlagsKHR`] <br/> "]
#[doc(alias = "VkAcquireProfilingLockFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct AcquireProfilingLockFlagBitsKHR(pub u32);
impl AcquireProfilingLockFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> AcquireProfilingLockFlagsKHR {
        AcquireProfilingLockFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for AcquireProfilingLockFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR.html)) · Function <br/> vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR - Reports properties of the performance query counters available on a queue family of a device\n[](#_c_specification)C Specification\n----------\n\nTo enumerate the performance query counters available on a queue family of a\nphysical device, call:\n\n```\n// Provided by VK_KHR_performance_query\nVkResult vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    uint32_t*                                   pCounterCount,\n    VkPerformanceCounterKHR*                    pCounters,\n    VkPerformanceCounterDescriptionKHR*         pCounterDescriptions);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device whose queue\n  family performance query counter properties will be queried.\n\n* [`Self::queue_family_index`] is the index into the queue family of the\n  physical device we want to get properties for.\n\n* [`Self::p_counter_count`] is a pointer to an integer related to the number of\n  counters available or queried, as described below.\n\n* [`Self::p_counters`] is either `NULL` or a pointer to an array of[`crate::vk::PerformanceCounterKHR`] structures.\n\n* [`Self::p_counter_descriptions`] is either `NULL` or a pointer to an array of[`crate::vk::PerformanceCounterDescriptionKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_counters`] is `NULL` and [`Self::p_counter_descriptions`] is `NULL`, then\nthe number of counters available is returned in [`Self::p_counter_count`].\nOtherwise, [`Self::p_counter_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_counters`], [`Self::p_counter_descriptions`],\nor both arrays and on return the variable is overwritten with the number of\nstructures actually written out.\nIf [`Self::p_counter_count`] is less than the number of counters available, at\nmost [`Self::p_counter_count`] structures will be written, and [`crate::vk::Result::INCOMPLETE`]will be returned instead of [`crate::vk::Result::SUCCESS`], to indicate that not all the\navailable counters were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounterCount-parameter  \n  [`Self::p_counter_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounters-parameter  \n   If the value referenced by [`Self::p_counter_count`] is not `0`, and [`Self::p_counters`] is not `NULL`, [`Self::p_counters`] **must** be a valid pointer to an array of [`Self::p_counter_count`] [`crate::vk::PerformanceCounterKHR`] structures\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounterDescriptions-parameter  \n   If the value referenced by [`Self::p_counter_count`] is not `0`, and [`Self::p_counter_descriptions`] is not `NULL`, [`Self::p_counter_descriptions`] **must** be a valid pointer to an array of [`Self::p_counter_count`] [`crate::vk::PerformanceCounterDescriptionKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterDescriptionKHR`], [`crate::vk::PerformanceCounterKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, p_counter_count: *mut u32, p_counters: *mut crate::extensions::khr_performance_query::PerformanceCounterKHR, p_counter_descriptions: *mut crate::extensions::khr_performance_query::PerformanceCounterDescriptionKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR.html)) · Function <br/> vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR - Reports the number of passes require for a performance query pool type\n[](#_c_specification)C Specification\n----------\n\nTo query the number of passes required to query a performance query pool on\na physical device, call:\n\n```\n// Provided by VK_KHR_performance_query\nvoid vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkQueryPoolPerformanceCreateInfoKHR*  pPerformanceQueryCreateInfo,\n    uint32_t*                                   pNumPasses);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device whose queue\n  family performance query counter properties will be queried.\n\n* [`Self::p_performance_query_create_info`] is a pointer to a[`crate::vk::QueryPoolPerformanceCreateInfoKHR`] of the performance query that\n  is to be created.\n\n* [`Self::p_num_passes`] is a pointer to an integer related to the number of\n  passes required to query the performance query pool, as described below.\n[](#_description)Description\n----------\n\nThe [`Self::p_performance_query_create_info`] member[`crate::vk::QueryPoolPerformanceCreateInfoKHR`]::`queueFamilyIndex` **must** be a\nqueue family of [`Self::physical_device`].\nThe number of passes required to capture the counters specified in the[`Self::p_performance_query_create_info`] member[`crate::vk::QueryPoolPerformanceCreateInfoKHR`]::`pCounters` is returned in[`Self::p_num_passes`].\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-pPerformanceQueryCreateInfo-parameter  \n  [`Self::p_performance_query_create_info`] **must** be a valid pointer to a valid [`crate::vk::QueryPoolPerformanceCreateInfoKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-pNumPasses-parameter  \n  [`Self::p_num_passes`] **must** be a valid pointer to a `uint32_t` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::QueryPoolPerformanceCreateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_performance_query_create_info: *const crate::extensions::khr_performance_query::QueryPoolPerformanceCreateInfoKHR, p_num_passes: *mut u32) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquireProfilingLockKHR.html)) · Function <br/> vkAcquireProfilingLockKHR - Acquires the profiling lock\n[](#_c_specification)C Specification\n----------\n\nTo record and submit a command buffer that contains a performance query pool\nthe profiling lock **must** be held.\nThe profiling lock **must** be acquired prior to any call to[`crate::vk::PFN_vkBeginCommandBuffer`] that will be using a performance query pool.\nThe profiling lock **must** be held while any command buffer that contains a\nperformance query pool is in the *recording*, *executable*, or *pending\nstate*.\nTo acquire the profiling lock, call:\n\n```\n// Provided by VK_KHR_performance_query\nVkResult vkAcquireProfilingLockKHR(\n    VkDevice                                    device,\n    const VkAcquireProfilingLockInfoKHR*        pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to profile.\n\n* [`Self::p_info`] is a pointer to a [`crate::vk::AcquireProfilingLockInfoKHR`]structure which contains information about how the profiling is to be\n  acquired.\n[](#_description)Description\n----------\n\nImplementations **may** allow multiple actors to hold the profiling lock\nconcurrently.\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquireProfilingLockKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquireProfilingLockKHR-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::AcquireProfilingLockInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::TIMEOUT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AcquireProfilingLockInfoKHR`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkAcquireProfilingLockKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_info: *const crate::extensions::khr_performance_query::AcquireProfilingLockInfoKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseProfilingLockKHR.html)) · Function <br/> vkReleaseProfilingLockKHR - Releases the profiling lock\n[](#_c_specification)C Specification\n----------\n\nTo release the profiling lock, call:\n\n```\n// Provided by VK_KHR_performance_query\nvoid vkReleaseProfilingLockKHR(\n    VkDevice                                    device);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to cease profiling on.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkReleaseProfilingLockKHR-device-03235  \n   The profiling lock of [`Self::device`] **must** have been held via a previous\n  successful call to [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleaseProfilingLockKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkReleaseProfilingLockKHR = unsafe extern "system" fn(device: crate::vk1_0::Device) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePerformanceQueryFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, QueryPoolPerformanceCreateInfoKHR> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, QueryPoolPerformanceCreateInfoKHRBuilder<'_>> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PerformanceQuerySubmitInfoKHR> for crate::vk1_0::SubmitInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PerformanceQuerySubmitInfoKHRBuilder<'_>> for crate::vk1_0::SubmitInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePerformanceQueryFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePerformanceQueryPropertiesKHR> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePerformanceQueryFeaturesKHR.html)) · Structure <br/> VkPhysicalDevicePerformanceQueryFeaturesKHR - Structure describing performance query support for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPhysicalDevicePerformanceQueryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           performanceCounterQueryPools;\n    VkBool32           performanceCounterMultipleQueryPools;\n} VkPhysicalDevicePerformanceQueryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::performance_counter_query_pools`] indicates whether the implementation\n  supports performance counter query pools.\n\n* []()[`Self::performance_counter_multiple_query_pools`] indicates whether the\n  implementation supports using multiple performance query pools in a\n  primary command buffer and secondary command buffers executed within it.\n\nIf the [`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePerformanceQueryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePerformanceQueryFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePerformanceQueryFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub performance_counter_query_pools: crate::vk1_0::Bool32,
    pub performance_counter_multiple_query_pools: crate::vk1_0::Bool32,
}
impl PhysicalDevicePerformanceQueryFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR;
}
impl Default for PhysicalDevicePerformanceQueryFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), performance_counter_query_pools: Default::default(), performance_counter_multiple_query_pools: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePerformanceQueryFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePerformanceQueryFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("performance_counter_query_pools", &(self.performance_counter_query_pools != 0)).field("performance_counter_multiple_query_pools", &(self.performance_counter_multiple_query_pools != 0)).finish()
    }
}
impl PhysicalDevicePerformanceQueryFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
        PhysicalDevicePerformanceQueryFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePerformanceQueryFeaturesKHR.html)) · Builder of [`PhysicalDevicePerformanceQueryFeaturesKHR`] <br/> VkPhysicalDevicePerformanceQueryFeaturesKHR - Structure describing performance query support for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPhysicalDevicePerformanceQueryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           performanceCounterQueryPools;\n    VkBool32           performanceCounterMultipleQueryPools;\n} VkPhysicalDevicePerformanceQueryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::performance_counter_query_pools`] indicates whether the implementation\n  supports performance counter query pools.\n\n* []()[`Self::performance_counter_multiple_query_pools`] indicates whether the\n  implementation supports using multiple performance query pools in a\n  primary command buffer and secondary command buffers executed within it.\n\nIf the [`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePerformanceQueryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePerformanceQueryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a>(PhysicalDevicePerformanceQueryFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
        PhysicalDevicePerformanceQueryFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn performance_counter_query_pools(mut self, performance_counter_query_pools: bool) -> Self {
        self.0.performance_counter_query_pools = performance_counter_query_pools as _;
        self
    }
    #[inline]
    pub fn performance_counter_multiple_query_pools(mut self, performance_counter_multiple_query_pools: bool) -> Self {
        self.0.performance_counter_multiple_query_pools = performance_counter_multiple_query_pools as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePerformanceQueryFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
    type Target = PhysicalDevicePerformanceQueryFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePerformanceQueryFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePerformanceQueryPropertiesKHR.html)) · Structure <br/> VkPhysicalDevicePerformanceQueryPropertiesKHR - Structure describing performance query properties for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePerformanceQueryPropertiesKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPhysicalDevicePerformanceQueryPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           allowCommandBufferQueryCopies;\n} VkPhysicalDevicePerformanceQueryPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::allow_command_buffer_query_copies`] is [`crate::vk::TRUE`] if the performance\n  query pools are allowed to be used with [`crate::vk::PFN_vkCmdCopyQueryPoolResults`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDevicePerformanceQueryPropertiesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePerformanceQueryPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePerformanceQueryPropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePerformanceQueryPropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub allow_command_buffer_query_copies: crate::vk1_0::Bool32,
}
impl PhysicalDevicePerformanceQueryPropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR;
}
impl Default for PhysicalDevicePerformanceQueryPropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), allow_command_buffer_query_copies: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePerformanceQueryPropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePerformanceQueryPropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("allow_command_buffer_query_copies", &(self.allow_command_buffer_query_copies != 0)).finish()
    }
}
impl PhysicalDevicePerformanceQueryPropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
        PhysicalDevicePerformanceQueryPropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePerformanceQueryPropertiesKHR.html)) · Builder of [`PhysicalDevicePerformanceQueryPropertiesKHR`] <br/> VkPhysicalDevicePerformanceQueryPropertiesKHR - Structure describing performance query properties for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePerformanceQueryPropertiesKHR`] structure is defined\nas:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPhysicalDevicePerformanceQueryPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           allowCommandBufferQueryCopies;\n} VkPhysicalDevicePerformanceQueryPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::allow_command_buffer_query_copies`] is [`crate::vk::TRUE`] if the performance\n  query pools are allowed to be used with [`crate::vk::PFN_vkCmdCopyQueryPoolResults`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDevicePerformanceQueryPropertiesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePerformanceQueryPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a>(PhysicalDevicePerformanceQueryPropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
        PhysicalDevicePerformanceQueryPropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn allow_command_buffer_query_copies(mut self, allow_command_buffer_query_copies: bool) -> Self {
        self.0.allow_command_buffer_query_copies = allow_command_buffer_query_copies as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePerformanceQueryPropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
    fn default() -> PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
    type Target = PhysicalDevicePerformanceQueryPropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePerformanceQueryPropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterKHR.html)) · Structure <br/> VkPerformanceCounterKHR - Structure providing information about a counter\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceCounterKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceCounterKHR {\n    VkStructureType                   sType;\n    void*                             pNext;\n    VkPerformanceCounterUnitKHR       unit;\n    VkPerformanceCounterScopeKHR      scope;\n    VkPerformanceCounterStorageKHR    storage;\n    uint8_t                           uuid[VK_UUID_SIZE];\n} VkPerformanceCounterKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::unit`] is a [`crate::vk::PerformanceCounterUnitKHR`] specifying the unit\n  that the counter data will record.\n\n* [`Self::scope`] is a [`crate::vk::PerformanceCounterScopeKHR`] specifying the scope\n  that the counter belongs to.\n\n* [`Self::storage`] is a [`crate::vk::PerformanceCounterStorageKHR`] specifying the\n  storage type that the counter’s data uses.\n\n* [`Self::uuid`] is an array of size [`crate::vk::UUID_SIZE`], containing 8-bit\n  values that represent a universally unique identifier for the counter of\n  the physical device.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceCounterKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_COUNTER_KHR`]\n\n* []() VUID-VkPerformanceCounterKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterScopeKHR`], [`crate::vk::PerformanceCounterStorageKHR`], [`crate::vk::PerformanceCounterUnitKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]\n"]
#[doc(alias = "VkPerformanceCounterKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceCounterKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub unit: crate::extensions::khr_performance_query::PerformanceCounterUnitKHR,
    pub scope: crate::extensions::khr_performance_query::PerformanceCounterScopeKHR,
    pub storage: crate::extensions::khr_performance_query::PerformanceCounterStorageKHR,
    pub uuid: [u8; 16],
}
impl PerformanceCounterKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_COUNTER_KHR;
}
impl Default for PerformanceCounterKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), unit: Default::default(), scope: Default::default(), storage: Default::default(), uuid: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for PerformanceCounterKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceCounterKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("unit", &self.unit).field("scope", &self.scope).field("storage", &self.storage).field("uuid", &self.uuid).finish()
    }
}
impl PerformanceCounterKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceCounterKHRBuilder<'a> {
        PerformanceCounterKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterKHR.html)) · Builder of [`PerformanceCounterKHR`] <br/> VkPerformanceCounterKHR - Structure providing information about a counter\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceCounterKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceCounterKHR {\n    VkStructureType                   sType;\n    void*                             pNext;\n    VkPerformanceCounterUnitKHR       unit;\n    VkPerformanceCounterScopeKHR      scope;\n    VkPerformanceCounterStorageKHR    storage;\n    uint8_t                           uuid[VK_UUID_SIZE];\n} VkPerformanceCounterKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::unit`] is a [`crate::vk::PerformanceCounterUnitKHR`] specifying the unit\n  that the counter data will record.\n\n* [`Self::scope`] is a [`crate::vk::PerformanceCounterScopeKHR`] specifying the scope\n  that the counter belongs to.\n\n* [`Self::storage`] is a [`crate::vk::PerformanceCounterStorageKHR`] specifying the\n  storage type that the counter’s data uses.\n\n* [`Self::uuid`] is an array of size [`crate::vk::UUID_SIZE`], containing 8-bit\n  values that represent a universally unique identifier for the counter of\n  the physical device.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceCounterKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_COUNTER_KHR`]\n\n* []() VUID-VkPerformanceCounterKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterScopeKHR`], [`crate::vk::PerformanceCounterStorageKHR`], [`crate::vk::PerformanceCounterUnitKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]\n"]
#[repr(transparent)]
pub struct PerformanceCounterKHRBuilder<'a>(PerformanceCounterKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceCounterKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceCounterKHRBuilder<'a> {
        PerformanceCounterKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn unit(mut self, unit: crate::extensions::khr_performance_query::PerformanceCounterUnitKHR) -> Self {
        self.0.unit = unit as _;
        self
    }
    #[inline]
    pub fn scope(mut self, scope: crate::extensions::khr_performance_query::PerformanceCounterScopeKHR) -> Self {
        self.0.scope = scope as _;
        self
    }
    #[inline]
    pub fn storage(mut self, storage: crate::extensions::khr_performance_query::PerformanceCounterStorageKHR) -> Self {
        self.0.storage = storage as _;
        self
    }
    #[inline]
    pub fn uuid(mut self, uuid: [u8; 16]) -> Self {
        self.0.uuid = uuid as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceCounterKHR {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceCounterKHRBuilder<'a> {
    fn default() -> PerformanceCounterKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceCounterKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceCounterKHRBuilder<'a> {
    type Target = PerformanceCounterKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceCounterKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionKHR.html)) · Structure <br/> VkPerformanceCounterDescriptionKHR - Structure providing more detailed information about a counter\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceCounterDescriptionKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceCounterDescriptionKHR {\n    VkStructureType                            sType;\n    void*                                      pNext;\n    VkPerformanceCounterDescriptionFlagsKHR    flags;\n    char                                       name[VK_MAX_DESCRIPTION_SIZE];\n    char                                       category[VK_MAX_DESCRIPTION_SIZE];\n    char                                       description[VK_MAX_DESCRIPTION_SIZE];\n} VkPerformanceCounterDescriptionKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of[VkPerformanceCounterDescriptionFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagBitsKHR.html) indicating the usage\n  behavior for the counter.\n\n* [`Self::name`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`], containing\n  a null-terminated UTF-8 string specifying the name of the counter.\n\n* [`Self::category`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`],\n  containing a null-terminated UTF-8 string specifying the category of the\n  counter.\n\n* [`Self::description`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`],\n  containing a null-terminated UTF-8 string specifying the description of\n  the counter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceCounterDescriptionKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_COUNTER_DESCRIPTION_KHR`]\n\n* []() VUID-VkPerformanceCounterDescriptionKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterDescriptionFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]\n"]
#[doc(alias = "VkPerformanceCounterDescriptionKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceCounterDescriptionKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub flags: crate::extensions::khr_performance_query::PerformanceCounterDescriptionFlagsKHR,
    pub name: [std::os::raw::c_char; 256],
    pub category: [std::os::raw::c_char; 256],
    pub description: [std::os::raw::c_char; 256],
}
impl PerformanceCounterDescriptionKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_COUNTER_DESCRIPTION_KHR;
}
impl Default for PerformanceCounterDescriptionKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), flags: Default::default(), name: unsafe { std::mem::zeroed() }, category: unsafe { std::mem::zeroed() }, description: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for PerformanceCounterDescriptionKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceCounterDescriptionKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("name", unsafe { &std::ffi::CStr::from_ptr(self.name.as_ptr()) }).field("category", unsafe { &std::ffi::CStr::from_ptr(self.category.as_ptr()) }).field("description", unsafe { &std::ffi::CStr::from_ptr(self.description.as_ptr()) }).finish()
    }
}
impl PerformanceCounterDescriptionKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceCounterDescriptionKHRBuilder<'a> {
        PerformanceCounterDescriptionKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionKHR.html)) · Builder of [`PerformanceCounterDescriptionKHR`] <br/> VkPerformanceCounterDescriptionKHR - Structure providing more detailed information about a counter\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceCounterDescriptionKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceCounterDescriptionKHR {\n    VkStructureType                            sType;\n    void*                                      pNext;\n    VkPerformanceCounterDescriptionFlagsKHR    flags;\n    char                                       name[VK_MAX_DESCRIPTION_SIZE];\n    char                                       category[VK_MAX_DESCRIPTION_SIZE];\n    char                                       description[VK_MAX_DESCRIPTION_SIZE];\n} VkPerformanceCounterDescriptionKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of[VkPerformanceCounterDescriptionFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterDescriptionFlagBitsKHR.html) indicating the usage\n  behavior for the counter.\n\n* [`Self::name`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`], containing\n  a null-terminated UTF-8 string specifying the name of the counter.\n\n* [`Self::category`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`],\n  containing a null-terminated UTF-8 string specifying the category of the\n  counter.\n\n* [`Self::description`] is an array of size [`crate::vk::MAX_DESCRIPTION_SIZE`],\n  containing a null-terminated UTF-8 string specifying the description of\n  the counter.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceCounterDescriptionKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_COUNTER_DESCRIPTION_KHR`]\n\n* []() VUID-VkPerformanceCounterDescriptionKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterDescriptionFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]\n"]
#[repr(transparent)]
pub struct PerformanceCounterDescriptionKHRBuilder<'a>(PerformanceCounterDescriptionKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceCounterDescriptionKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceCounterDescriptionKHRBuilder<'a> {
        PerformanceCounterDescriptionKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_performance_query::PerformanceCounterDescriptionFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn name(mut self, name: [std::os::raw::c_char; 256]) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    pub fn category(mut self, category: [std::os::raw::c_char; 256]) -> Self {
        self.0.category = category as _;
        self
    }
    #[inline]
    pub fn description(mut self, description: [std::os::raw::c_char; 256]) -> Self {
        self.0.description = description as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceCounterDescriptionKHR {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceCounterDescriptionKHRBuilder<'a> {
    fn default() -> PerformanceCounterDescriptionKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceCounterDescriptionKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceCounterDescriptionKHRBuilder<'a> {
    type Target = PerformanceCounterDescriptionKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceCounterDescriptionKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolPerformanceCreateInfoKHR.html)) · Structure <br/> VkQueryPoolPerformanceCreateInfoKHR - Structure specifying parameters of a newly created performance query pool\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkQueryPoolPerformanceCreateInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           queueFamilyIndex;\n    uint32_t           counterIndexCount;\n    const uint32_t*    pCounterIndices;\n} VkQueryPoolPerformanceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::queue_family_index`] is the queue family index to create this\n  performance query pool for.\n\n* [`Self::counter_index_count`] is the length of the [`Self::p_counter_indices`]array.\n\n* [`Self::p_counter_indices`] is a pointer to an array of indices into the[`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr::p_counters`]to enable in this performance query pool.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-queueFamilyIndex-03236  \n  [`Self::queue_family_index`] **must** be a valid queue family index of the device\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-performanceCounterQueryPools-03237  \n   The [`performanceCounterQueryPools`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-performanceCounterQueryPools) feature **must** be enabled\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-pCounterIndices-03321  \n   Each element of [`Self::p_counter_indices`] **must** be in the range of counters\n  reported by[`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]for the queue family specified in [`Self::queue_family_index`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR`]\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-pCounterIndices-parameter  \n  [`Self::p_counter_indices`] **must** be a valid pointer to an array of [`Self::counter_index_count`] `uint32_t` values\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-counterIndexCount-arraylength  \n  [`Self::counter_index_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_queue_family_performance_query_passes_khr`]\n"]
#[doc(alias = "VkQueryPoolPerformanceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct QueryPoolPerformanceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub queue_family_index: u32,
    pub counter_index_count: u32,
    pub p_counter_indices: *const u32,
}
impl QueryPoolPerformanceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR;
}
impl Default for QueryPoolPerformanceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), queue_family_index: Default::default(), counter_index_count: Default::default(), p_counter_indices: std::ptr::null() }
    }
}
impl std::fmt::Debug for QueryPoolPerformanceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("QueryPoolPerformanceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("queue_family_index", &self.queue_family_index).field("counter_index_count", &self.counter_index_count).field("p_counter_indices", &self.p_counter_indices).finish()
    }
}
impl QueryPoolPerformanceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
        QueryPoolPerformanceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolPerformanceCreateInfoKHR.html)) · Builder of [`QueryPoolPerformanceCreateInfoKHR`] <br/> VkQueryPoolPerformanceCreateInfoKHR - Structure specifying parameters of a newly created performance query pool\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkQueryPoolPerformanceCreateInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           queueFamilyIndex;\n    uint32_t           counterIndexCount;\n    const uint32_t*    pCounterIndices;\n} VkQueryPoolPerformanceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::queue_family_index`] is the queue family index to create this\n  performance query pool for.\n\n* [`Self::counter_index_count`] is the length of the [`Self::p_counter_indices`]array.\n\n* [`Self::p_counter_indices`] is a pointer to an array of indices into the[`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr::p_counters`]to enable in this performance query pool.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-queueFamilyIndex-03236  \n  [`Self::queue_family_index`] **must** be a valid queue family index of the device\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-performanceCounterQueryPools-03237  \n   The [`performanceCounterQueryPools`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-performanceCounterQueryPools) feature **must** be enabled\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-pCounterIndices-03321  \n   Each element of [`Self::p_counter_indices`] **must** be in the range of counters\n  reported by[`crate::vk::DeviceLoader::enumerate_physical_device_queue_family_performance_query_counters_khr`]for the queue family specified in [`Self::queue_family_index`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR`]\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-pCounterIndices-parameter  \n  [`Self::p_counter_indices`] **must** be a valid pointer to an array of [`Self::counter_index_count`] `uint32_t` values\n\n* []() VUID-VkQueryPoolPerformanceCreateInfoKHR-counterIndexCount-arraylength  \n  [`Self::counter_index_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_queue_family_performance_query_passes_khr`]\n"]
#[repr(transparent)]
pub struct QueryPoolPerformanceCreateInfoKHRBuilder<'a>(QueryPoolPerformanceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
        QueryPoolPerformanceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn queue_family_index(mut self, queue_family_index: u32) -> Self {
        self.0.queue_family_index = queue_family_index as _;
        self
    }
    #[inline]
    pub fn counter_indices(mut self, counter_indices: &'a [u32]) -> Self {
        self.0.p_counter_indices = counter_indices.as_ptr() as _;
        self.0.counter_index_count = counter_indices.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> QueryPoolPerformanceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
    fn default() -> QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
    type Target = QueryPoolPerformanceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for QueryPoolPerformanceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceCounterResultKHR.html)) · Structure <br/> VkPerformanceCounterResultKHR - Union containing a performance counter result\n[](#_c_specification)C Specification\n----------\n\nPerformance query results are returned in an array of[`crate::vk::PerformanceCounterResultKHR`] unions containing the data associated\nwith each counter in the query, stored in the same order as the counters\nsupplied in `pCounterIndices` when creating the performance query.\nThe [`crate::vk::PerformanceCounterKHR::unit`] enumeration specifies how to\nparse the counter data.\n\n```\n// Provided by VK_KHR_performance_query\ntypedef union VkPerformanceCounterResultKHR {\n    int32_t     int32;\n    int64_t     int64;\n    uint32_t    uint32;\n    uint64_t    uint64;\n    float       float32;\n    double      float64;\n} VkPerformanceCounterResultKHR;\n```\n[](#_members)Members\n----------\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
#[doc(alias = "VkPerformanceCounterResultKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub union PerformanceCounterResultKHR {
    pub int32: i32,
    pub int64: i64,
    pub uint32: u32,
    pub uint64: u64,
    pub float32: std::os::raw::c_float,
    pub float64: std::os::raw::c_double,
}
impl Default for PerformanceCounterResultKHR {
    fn default() -> Self {
        unsafe { std::mem::zeroed() }
    }
}
impl std::fmt::Debug for PerformanceCounterResultKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceCounterResultKHR").finish()
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAcquireProfilingLockInfoKHR.html)) · Structure <br/> VkAcquireProfilingLockInfoKHR - Structure specifying parameters to acquire the profiling lock\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AcquireProfilingLockInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkAcquireProfilingLockInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkAcquireProfilingLockFlagsKHR    flags;\n    uint64_t                          timeout;\n} VkAcquireProfilingLockInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::timeout`] indicates how long the function waits, in nanoseconds, if\n  the profiling lock is not available.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACQUIRE_PROFILING_LOCK_INFO_KHR`]\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\nIf [`Self::timeout`] is 0, [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`] will not block while\nattempting to acquire the profling lock.\nIf [`Self::timeout`] is `UINT64_MAX`, the function will not return until the\nprofiling lock was acquired.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AcquireProfilingLockFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`]\n"]
#[doc(alias = "VkAcquireProfilingLockInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AcquireProfilingLockInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_performance_query::AcquireProfilingLockFlagsKHR,
    pub timeout: u64,
}
impl AcquireProfilingLockInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ACQUIRE_PROFILING_LOCK_INFO_KHR;
}
impl Default for AcquireProfilingLockInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), timeout: Default::default() }
    }
}
impl std::fmt::Debug for AcquireProfilingLockInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AcquireProfilingLockInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("timeout", &self.timeout).finish()
    }
}
impl AcquireProfilingLockInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> AcquireProfilingLockInfoKHRBuilder<'a> {
        AcquireProfilingLockInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAcquireProfilingLockInfoKHR.html)) · Builder of [`AcquireProfilingLockInfoKHR`] <br/> VkAcquireProfilingLockInfoKHR - Structure specifying parameters to acquire the profiling lock\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AcquireProfilingLockInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkAcquireProfilingLockInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkAcquireProfilingLockFlagsKHR    flags;\n    uint64_t                          timeout;\n} VkAcquireProfilingLockInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::timeout`] indicates how long the function waits, in nanoseconds, if\n  the profiling lock is not available.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ACQUIRE_PROFILING_LOCK_INFO_KHR`]\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkAcquireProfilingLockInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\nIf [`Self::timeout`] is 0, [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`] will not block while\nattempting to acquire the profling lock.\nIf [`Self::timeout`] is `UINT64_MAX`, the function will not return until the\nprofiling lock was acquired.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AcquireProfilingLockFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`]\n"]
#[repr(transparent)]
pub struct AcquireProfilingLockInfoKHRBuilder<'a>(AcquireProfilingLockInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> AcquireProfilingLockInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> AcquireProfilingLockInfoKHRBuilder<'a> {
        AcquireProfilingLockInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_performance_query::AcquireProfilingLockFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn timeout(mut self, timeout: u64) -> Self {
        self.0.timeout = timeout as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AcquireProfilingLockInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for AcquireProfilingLockInfoKHRBuilder<'a> {
    fn default() -> AcquireProfilingLockInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AcquireProfilingLockInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AcquireProfilingLockInfoKHRBuilder<'a> {
    type Target = AcquireProfilingLockInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AcquireProfilingLockInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceQuerySubmitInfoKHR.html)) · Structure <br/> VkPerformanceQuerySubmitInfoKHR - Structure indicating which counter pass index is active for performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceQuerySubmitInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceQuerySubmitInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           counterPassIndex;\n} VkPerformanceQuerySubmitInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::counter_pass_index`] specifies which counter pass index is active.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::SubmitInfo`]::[`Self::p_next`] chain does not include this\nstructure, the batch defaults to use counter pass index 0.\n\nValid Usage\n\n* []() VUID-VkPerformanceQuerySubmitInfoKHR-counterPassIndex-03221  \n  [`Self::counter_pass_index`] **must** be less than the number of counter passes\n  required by any queries within the batch.\n  The required number of counter passes for a performance query is\n  obtained by calling[`crate::vk::DeviceLoader::get_physical_device_queue_family_performance_query_passes_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceQuerySubmitInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_QUERY_SUBMIT_INFO_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPerformanceQuerySubmitInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceQuerySubmitInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub counter_pass_index: u32,
}
impl PerformanceQuerySubmitInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_QUERY_SUBMIT_INFO_KHR;
}
impl Default for PerformanceQuerySubmitInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), counter_pass_index: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceQuerySubmitInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceQuerySubmitInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("counter_pass_index", &self.counter_pass_index).finish()
    }
}
impl PerformanceQuerySubmitInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceQuerySubmitInfoKHRBuilder<'a> {
        PerformanceQuerySubmitInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceQuerySubmitInfoKHR.html)) · Builder of [`PerformanceQuerySubmitInfoKHR`] <br/> VkPerformanceQuerySubmitInfoKHR - Structure indicating which counter pass index is active for performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceQuerySubmitInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_performance_query\ntypedef struct VkPerformanceQuerySubmitInfoKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           counterPassIndex;\n} VkPerformanceQuerySubmitInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::counter_pass_index`] specifies which counter pass index is active.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::SubmitInfo`]::[`Self::p_next`] chain does not include this\nstructure, the batch defaults to use counter pass index 0.\n\nValid Usage\n\n* []() VUID-VkPerformanceQuerySubmitInfoKHR-counterPassIndex-03221  \n  [`Self::counter_pass_index`] **must** be less than the number of counter passes\n  required by any queries within the batch.\n  The required number of counter passes for a performance query is\n  obtained by calling[`crate::vk::DeviceLoader::get_physical_device_queue_family_performance_query_passes_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceQuerySubmitInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_QUERY_SUBMIT_INFO_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PerformanceQuerySubmitInfoKHRBuilder<'a>(PerformanceQuerySubmitInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceQuerySubmitInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceQuerySubmitInfoKHRBuilder<'a> {
        PerformanceQuerySubmitInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn counter_pass_index(mut self, counter_pass_index: u32) -> Self {
        self.0.counter_pass_index = counter_pass_index as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceQuerySubmitInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceQuerySubmitInfoKHRBuilder<'a> {
    fn default() -> PerformanceQuerySubmitInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceQuerySubmitInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceQuerySubmitInfoKHRBuilder<'a> {
    type Target = PerformanceQuerySubmitInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceQuerySubmitInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromConst<'a, PerformanceQuerySubmitInfoKHR> for crate::extensions::khr_synchronization2::SubmitInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PerformanceQuerySubmitInfoKHRBuilder<'_>> for crate::extensions::khr_synchronization2::SubmitInfo2KHRBuilder<'a> {}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR.html)) · Function <br/> vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR - Reports properties of the performance query counters available on a queue family of a device\n[](#_c_specification)C Specification\n----------\n\nTo enumerate the performance query counters available on a queue family of a\nphysical device, call:\n\n```\n// Provided by VK_KHR_performance_query\nVkResult vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    uint32_t*                                   pCounterCount,\n    VkPerformanceCounterKHR*                    pCounters,\n    VkPerformanceCounterDescriptionKHR*         pCounterDescriptions);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device whose queue\n  family performance query counter properties will be queried.\n\n* [`Self::queue_family_index`] is the index into the queue family of the\n  physical device we want to get properties for.\n\n* [`Self::p_counter_count`] is a pointer to an integer related to the number of\n  counters available or queried, as described below.\n\n* [`Self::p_counters`] is either `NULL` or a pointer to an array of[`crate::vk::PerformanceCounterKHR`] structures.\n\n* [`Self::p_counter_descriptions`] is either `NULL` or a pointer to an array of[`crate::vk::PerformanceCounterDescriptionKHR`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_counters`] is `NULL` and [`Self::p_counter_descriptions`] is `NULL`, then\nthe number of counters available is returned in [`Self::p_counter_count`].\nOtherwise, [`Self::p_counter_count`] **must** point to a variable set by the user to\nthe number of elements in the [`Self::p_counters`], [`Self::p_counter_descriptions`],\nor both arrays and on return the variable is overwritten with the number of\nstructures actually written out.\nIf [`Self::p_counter_count`] is less than the number of counters available, at\nmost [`Self::p_counter_count`] structures will be written, and [`crate::vk::Result::INCOMPLETE`]will be returned instead of [`crate::vk::Result::SUCCESS`], to indicate that not all the\navailable counters were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounterCount-parameter  \n  [`Self::p_counter_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounters-parameter  \n   If the value referenced by [`Self::p_counter_count`] is not `0`, and [`Self::p_counters`] is not `NULL`, [`Self::p_counters`] **must** be a valid pointer to an array of [`Self::p_counter_count`] [`crate::vk::PerformanceCounterKHR`] structures\n\n* []() VUID-vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR-pCounterDescriptions-parameter  \n   If the value referenced by [`Self::p_counter_count`] is not `0`, and [`Self::p_counter_descriptions`] is not `NULL`, [`Self::p_counter_descriptions`] **must** be a valid pointer to an array of [`Self::p_counter_count`] [`crate::vk::PerformanceCounterDescriptionKHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INITIALIZATION_FAILED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceCounterDescriptionKHR`], [`crate::vk::PerformanceCounterKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR")]
    pub unsafe fn enumerate_physical_device_queue_family_performance_query_counters_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, counter_count: Option<u32>) -> crate::utils::VulkanResult<(crate::SmallVec<crate::extensions::khr_performance_query::PerformanceCounterKHR>, crate::SmallVec<crate::extensions::khr_performance_query::PerformanceCounterDescriptionKHR>)> {
        let _function = self.enumerate_physical_device_queue_family_performance_query_counters_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut counter_count = match counter_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, queue_family_index as _, &mut v, std::ptr::null_mut(), std::ptr::null_mut());
                v
            }
        };
        let mut counters = crate::SmallVec::from_elem(Default::default(), counter_count as _);
        let mut counter_descriptions = crate::SmallVec::from_elem(Default::default(), counter_count as _);
        let _return = _function(physical_device as _, queue_family_index as _, &mut counter_count, counters.as_mut_ptr(), counter_descriptions.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, (counters, counter_descriptions))
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR.html)) · Function <br/> vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR - Reports the number of passes require for a performance query pool type\n[](#_c_specification)C Specification\n----------\n\nTo query the number of passes required to query a performance query pool on\na physical device, call:\n\n```\n// Provided by VK_KHR_performance_query\nvoid vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkQueryPoolPerformanceCreateInfoKHR*  pPerformanceQueryCreateInfo,\n    uint32_t*                                   pNumPasses);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device whose queue\n  family performance query counter properties will be queried.\n\n* [`Self::p_performance_query_create_info`] is a pointer to a[`crate::vk::QueryPoolPerformanceCreateInfoKHR`] of the performance query that\n  is to be created.\n\n* [`Self::p_num_passes`] is a pointer to an integer related to the number of\n  passes required to query the performance query pool, as described below.\n[](#_description)Description\n----------\n\nThe [`Self::p_performance_query_create_info`] member[`crate::vk::QueryPoolPerformanceCreateInfoKHR`]::`queueFamilyIndex` **must** be a\nqueue family of [`Self::physical_device`].\nThe number of passes required to capture the counters specified in the[`Self::p_performance_query_create_info`] member[`crate::vk::QueryPoolPerformanceCreateInfoKHR`]::`pCounters` is returned in[`Self::p_num_passes`].\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-pPerformanceQueryCreateInfo-parameter  \n  [`Self::p_performance_query_create_info`] **must** be a valid pointer to a valid [`crate::vk::QueryPoolPerformanceCreateInfoKHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR-pNumPasses-parameter  \n  [`Self::p_num_passes`] **must** be a valid pointer to a `uint32_t` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::QueryPoolPerformanceCreateInfoKHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR")]
    pub unsafe fn get_physical_device_queue_family_performance_query_passes_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, performance_query_create_info: &crate::extensions::khr_performance_query::QueryPoolPerformanceCreateInfoKHR) -> u32 {
        let _function = self.get_physical_device_queue_family_performance_query_passes_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut num_passes = Default::default();
        let _return = _function(physical_device as _, performance_query_create_info as _, &mut num_passes);
        num_passes
    }
}
#[doc = "Provided by [`crate::extensions::khr_performance_query`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquireProfilingLockKHR.html)) · Function <br/> vkAcquireProfilingLockKHR - Acquires the profiling lock\n[](#_c_specification)C Specification\n----------\n\nTo record and submit a command buffer that contains a performance query pool\nthe profiling lock **must** be held.\nThe profiling lock **must** be acquired prior to any call to[`crate::vk::PFN_vkBeginCommandBuffer`] that will be using a performance query pool.\nThe profiling lock **must** be held while any command buffer that contains a\nperformance query pool is in the *recording*, *executable*, or *pending\nstate*.\nTo acquire the profiling lock, call:\n\n```\n// Provided by VK_KHR_performance_query\nVkResult vkAcquireProfilingLockKHR(\n    VkDevice                                    device,\n    const VkAcquireProfilingLockInfoKHR*        pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to profile.\n\n* [`Self::p_info`] is a pointer to a [`crate::vk::AcquireProfilingLockInfoKHR`]structure which contains information about how the profiling is to be\n  acquired.\n[](#_description)Description\n----------\n\nImplementations **may** allow multiple actors to hold the profiling lock\nconcurrently.\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquireProfilingLockKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquireProfilingLockKHR-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::AcquireProfilingLockInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::TIMEOUT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AcquireProfilingLockInfoKHR`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkAcquireProfilingLockKHR")]
    pub unsafe fn acquire_profiling_lock_khr(&self, info: &crate::extensions::khr_performance_query::AcquireProfilingLockInfoKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.acquire_profiling_lock_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseProfilingLockKHR.html)) · Function <br/> vkReleaseProfilingLockKHR - Releases the profiling lock\n[](#_c_specification)C Specification\n----------\n\nTo release the profiling lock, call:\n\n```\n// Provided by VK_KHR_performance_query\nvoid vkReleaseProfilingLockKHR(\n    VkDevice                                    device);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to cease profiling on.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkReleaseProfilingLockKHR-device-03235  \n   The profiling lock of [`Self::device`] **must** have been held via a previous\n  successful call to [`crate::vk::DeviceLoader::acquire_profiling_lock_khr`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleaseProfilingLockKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`]\n"]
    #[doc(alias = "vkReleaseProfilingLockKHR")]
    pub unsafe fn release_profiling_lock_khr(&self) -> () {
        let _function = self.release_profiling_lock_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle);
        ()
    }
}
