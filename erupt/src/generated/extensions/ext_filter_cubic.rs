#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FILTER_CUBIC_SPEC_VERSION")]
pub const EXT_FILTER_CUBIC_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FILTER_CUBIC_EXTENSION_NAME")]
pub const EXT_FILTER_CUBIC_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_filter_cubic");
#[doc = "Provided by [`crate::extensions::ext_filter_cubic`]"]
impl crate::vk1_0::FormatFeatureFlagBits {
    pub const SAMPLED_IMAGE_FILTER_CUBIC_EXT: Self = Self::SAMPLED_IMAGE_FILTER_CUBIC_IMG;
}
#[doc = "Provided by [`crate::extensions::ext_filter_cubic`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT: Self = Self(1000170000);
    pub const FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT: Self = Self(1000170001);
}
#[doc = "Provided by [`crate::extensions::ext_filter_cubic`]"]
impl crate::vk1_0::Filter {
    pub const CUBIC_EXT: Self = Self::CUBIC_IMG;
}
impl<'a> crate::ExtendableFromMut<'a, FilterCubicImageViewImageFormatPropertiesEXT> for crate::vk1_1::ImageFormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, FilterCubicImageViewImageFormatPropertiesEXTBuilder<'_>> for crate::vk1_1::ImageFormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceImageViewImageFormatInfoEXT> for crate::vk1_1::PhysicalDeviceImageFormatInfo2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceImageFormatInfo2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageViewImageFormatInfoEXT.html)) · Structure <br/> VkPhysicalDeviceImageViewImageFormatInfoEXT - Structure for providing image view type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceImageViewImageFormatInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_filter_cubic\ntypedef struct VkPhysicalDeviceImageViewImageFormatInfoEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkImageViewType    imageViewType;\n} VkPhysicalDeviceImageViewImageFormatInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_view_type`] is a [`crate::vk::ImageViewType`] value specifying the type\n  of the image view.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceImageViewImageFormatInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT`]\n\n* []() VUID-VkPhysicalDeviceImageViewImageFormatInfoEXT-imageViewType-parameter  \n  [`Self::image_view_type`] **must** be a valid [`crate::vk::ImageViewType`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ImageViewType`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceImageViewImageFormatInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceImageViewImageFormatInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub image_view_type: crate::vk1_0::ImageViewType,
}
impl PhysicalDeviceImageViewImageFormatInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT;
}
impl Default for PhysicalDeviceImageViewImageFormatInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), image_view_type: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceImageViewImageFormatInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceImageViewImageFormatInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("image_view_type", &self.image_view_type).finish()
    }
}
impl PhysicalDeviceImageViewImageFormatInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
        PhysicalDeviceImageViewImageFormatInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageViewImageFormatInfoEXT.html)) · Builder of [`PhysicalDeviceImageViewImageFormatInfoEXT`] <br/> VkPhysicalDeviceImageViewImageFormatInfoEXT - Structure for providing image view type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceImageViewImageFormatInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_filter_cubic\ntypedef struct VkPhysicalDeviceImageViewImageFormatInfoEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkImageViewType    imageViewType;\n} VkPhysicalDeviceImageViewImageFormatInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_view_type`] is a [`crate::vk::ImageViewType`] value specifying the type\n  of the image view.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceImageViewImageFormatInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT`]\n\n* []() VUID-VkPhysicalDeviceImageViewImageFormatInfoEXT-imageViewType-parameter  \n  [`Self::image_view_type`] **must** be a valid [`crate::vk::ImageViewType`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ImageViewType`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a>(PhysicalDeviceImageViewImageFormatInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
        PhysicalDeviceImageViewImageFormatInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image_view_type(mut self, image_view_type: crate::vk1_0::ImageViewType) -> Self {
        self.0.image_view_type = image_view_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceImageViewImageFormatInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
    fn default() -> PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
    type Target = PhysicalDeviceImageViewImageFormatInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceImageViewImageFormatInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFilterCubicImageViewImageFormatPropertiesEXT.html)) · Structure <br/> VkFilterCubicImageViewImageFormatPropertiesEXT - Structure for querying cubic filtering capabilities of an image view type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_filter_cubic\ntypedef struct VkFilterCubicImageViewImageFormatPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           filterCubic;\n    VkBool32           filterCubicMinmax;\n} VkFilterCubicImageViewImageFormatPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::filter_cubic`] tells if image format, image type and image view type**can** be used with cubic filtering.\n  This field is set by the implementation.\n  User-specified value is ignored.\n\n* [`Self::filter_cubic_minmax`] tells if image format, image type and image view\n  type **can** be used with cubic filtering and minmax filtering.\n  This field is set by the implementation.\n  User-specified value is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkFilterCubicImageViewImageFormatPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT`]\n\nValid Usage\n\n* []() VUID-VkFilterCubicImageViewImageFormatPropertiesEXT-pNext-02627  \n   If the [`Self::p_next`] chain of the [`crate::vk::ImageFormatProperties2`] structure\n  includes a [`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]structure, the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceImageFormatInfo2`] structure **must** include a[`crate::vk::PhysicalDeviceImageViewImageFormatInfoEXT`] structure with an`imageViewType` that is compatible with `imageType`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkFilterCubicImageViewImageFormatPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct FilterCubicImageViewImageFormatPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub filter_cubic: crate::vk1_0::Bool32,
    pub filter_cubic_minmax: crate::vk1_0::Bool32,
}
impl FilterCubicImageViewImageFormatPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT;
}
impl Default for FilterCubicImageViewImageFormatPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), filter_cubic: Default::default(), filter_cubic_minmax: Default::default() }
    }
}
impl std::fmt::Debug for FilterCubicImageViewImageFormatPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("FilterCubicImageViewImageFormatPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("filter_cubic", &(self.filter_cubic != 0)).field("filter_cubic_minmax", &(self.filter_cubic_minmax != 0)).finish()
    }
}
impl FilterCubicImageViewImageFormatPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
        FilterCubicImageViewImageFormatPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFilterCubicImageViewImageFormatPropertiesEXT.html)) · Builder of [`FilterCubicImageViewImageFormatPropertiesEXT`] <br/> VkFilterCubicImageViewImageFormatPropertiesEXT - Structure for querying cubic filtering capabilities of an image view type\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_filter_cubic\ntypedef struct VkFilterCubicImageViewImageFormatPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           filterCubic;\n    VkBool32           filterCubicMinmax;\n} VkFilterCubicImageViewImageFormatPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::filter_cubic`] tells if image format, image type and image view type**can** be used with cubic filtering.\n  This field is set by the implementation.\n  User-specified value is ignored.\n\n* [`Self::filter_cubic_minmax`] tells if image format, image type and image view\n  type **can** be used with cubic filtering and minmax filtering.\n  This field is set by the implementation.\n  User-specified value is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkFilterCubicImageViewImageFormatPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT`]\n\nValid Usage\n\n* []() VUID-VkFilterCubicImageViewImageFormatPropertiesEXT-pNext-02627  \n   If the [`Self::p_next`] chain of the [`crate::vk::ImageFormatProperties2`] structure\n  includes a [`crate::vk::FilterCubicImageViewImageFormatPropertiesEXT`]structure, the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceImageFormatInfo2`] structure **must** include a[`crate::vk::PhysicalDeviceImageViewImageFormatInfoEXT`] structure with an`imageViewType` that is compatible with `imageType`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a>(FilterCubicImageViewImageFormatPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
        FilterCubicImageViewImageFormatPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn filter_cubic(mut self, filter_cubic: bool) -> Self {
        self.0.filter_cubic = filter_cubic as _;
        self
    }
    #[inline]
    pub fn filter_cubic_minmax(mut self, filter_cubic_minmax: bool) -> Self {
        self.0.filter_cubic_minmax = filter_cubic_minmax as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> FilterCubicImageViewImageFormatPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
    fn default() -> FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
    type Target = FilterCubicImageViewImageFormatPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for FilterCubicImageViewImageFormatPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
