#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_FRAMEBUFFER_MIXED_SAMPLES_SPEC_VERSION")]
pub const NV_FRAMEBUFFER_MIXED_SAMPLES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_FRAMEBUFFER_MIXED_SAMPLES_EXTENSION_NAME")]
pub const NV_FRAMEBUFFER_MIXED_SAMPLES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_framebuffer_mixed_samples");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageModulationStateCreateFlagsNV.html)) · Bitmask of [`PipelineCoverageModulationStateCreateFlagBitsNV`] <br/> VkPipelineCoverageModulationStateCreateFlagsNV - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_framebuffer_mixed_samples\ntypedef VkFlags VkPipelineCoverageModulationStateCreateFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineCoverageModulationStateCreateFlagBitsNV`] is a bitmask type for\nsetting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCoverageModulationStateCreateInfoNV`]\n"] # [doc (alias = "VkPipelineCoverageModulationStateCreateFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct PipelineCoverageModulationStateCreateFlagsNV : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineCoverageModulationStateCreateFlagsNV`] <br/> "]
#[doc(alias = "VkPipelineCoverageModulationStateCreateFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineCoverageModulationStateCreateFlagBitsNV(pub u32);
impl PipelineCoverageModulationStateCreateFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineCoverageModulationStateCreateFlagsNV {
        PipelineCoverageModulationStateCreateFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineCoverageModulationStateCreateFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_framebuffer_mixed_samples`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV: Self = Self(1000152000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoverageModulationModeNV.html)) · Enum <br/> VkCoverageModulationModeNV - Specify the coverage modulation mode\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineCoverageModulationStateCreateInfoNV::coverage_modulation_mode`],\nspecifying which color components are modulated, are:\n\n```\n// Provided by VK_NV_framebuffer_mixed_samples\ntypedef enum VkCoverageModulationModeNV {\n    VK_COVERAGE_MODULATION_MODE_NONE_NV = 0,\n    VK_COVERAGE_MODULATION_MODE_RGB_NV = 1,\n    VK_COVERAGE_MODULATION_MODE_ALPHA_NV = 2,\n    VK_COVERAGE_MODULATION_MODE_RGBA_NV = 3,\n} VkCoverageModulationModeNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::NONE_NV`] specifies that no components\n  are multiplied by the modulation factor.\n\n* [`Self::RGB_NV`] specifies that the red, green,\n  and blue components are multiplied by the modulation factor.\n\n* [`Self::ALPHA_NV`] specifies that the alpha\n  component is multiplied by the modulation factor.\n\n* [`Self::RGBA_NV`] specifies that all components\n  are multiplied by the modulation factor.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCoverageModulationStateCreateInfoNV`]\n"]
#[doc(alias = "VkCoverageModulationModeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct CoverageModulationModeNV(pub i32);
impl std::fmt::Debug for CoverageModulationModeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::NONE_NV => "NONE_NV",
            &Self::RGB_NV => "RGB_NV",
            &Self::ALPHA_NV => "ALPHA_NV",
            &Self::RGBA_NV => "RGBA_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_framebuffer_mixed_samples`]"]
impl crate::extensions::nv_framebuffer_mixed_samples::CoverageModulationModeNV {
    pub const NONE_NV: Self = Self(0);
    pub const RGB_NV: Self = Self(1);
    pub const ALPHA_NV: Self = Self(2);
    pub const RGBA_NV: Self = Self(3);
}
impl<'a> crate::ExtendableFromConst<'a, PipelineCoverageModulationStateCreateInfoNV> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCoverageModulationStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageModulationStateCreateInfoNV.html)) · Structure <br/> VkPipelineCoverageModulationStateCreateInfoNV - Structure specifying parameters controlling coverage modulation\n[](#_c_specification)C Specification\n----------\n\nAs part of coverage reduction, fragment color values **can** also be modulated\n(multiplied) by a value that is a function of fraction of covered\nrasterization samples associated with that color sample.\n\nPipeline state controlling coverage modulation is specified through the\nmembers of the [`crate::vk::PipelineCoverageModulationStateCreateInfoNV`]structure.\n\nThe [`crate::vk::PipelineCoverageModulationStateCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_framebuffer_mixed_samples\ntypedef struct VkPipelineCoverageModulationStateCreateInfoNV {\n    VkStructureType                                   sType;\n    const void*                                       pNext;\n    VkPipelineCoverageModulationStateCreateFlagsNV    flags;\n    VkCoverageModulationModeNV                        coverageModulationMode;\n    VkBool32                                          coverageModulationTableEnable;\n    uint32_t                                          coverageModulationTableCount;\n    const float*                                      pCoverageModulationTable;\n} VkPipelineCoverageModulationStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::coverage_modulation_mode`] is a [`crate::vk::CoverageModulationModeNV`] value\n  controlling which color components are modulated.\n\n* [`Self::coverage_modulation_table_enable`] controls whether the modulation\n  factor is looked up from a table in [`Self::p_coverage_modulation_table`].\n\n* [`Self::coverage_modulation_table_count`] is the number of elements in[`Self::p_coverage_modulation_table`].\n\n* [`Self::p_coverage_modulation_table`] is a table of modulation factors\n  containing a value for each number of covered samples.\n[](#_description)Description\n----------\n\nIf [`Self::coverage_modulation_table_enable`] is [`crate::vk::FALSE`], then for each\ncolor sample the associated bits of the pixel coverage are counted and\ndivided by the number of associated bits to produce a modulation factorR in the range (0,1] (a value of zero would have been killed due\nto a color coverage of 0).\nSpecifically:\n\n* N = value of `rasterizationSamples`\n\n* M = value of [`crate::vk::AttachmentDescription::samples`] for any\n  color attachments\n\n* R = popcount(associated coverage bits) / (N / M)\n\nIf [`Self::coverage_modulation_table_enable`] is [`crate::vk::TRUE`], the value Ris computed using a programmable lookup table.\nThe lookup table has N / M elements, and the element of the table is\nselected by:\n\n* R = [`Self::p_coverage_modulation_table`][popcount(associated coverage\n  bits)-1]\n\nNote that the table does not have an entry for popcount(associated\ncoverage bits) = 0, because such samples would have been killed.\n\nThe values of [`Self::p_coverage_modulation_table`] **may** be rounded to an\nimplementation-dependent precision, which is at least as fine as 1 /\nN, and clamped to [0,1].\n\nFor each color attachment with a floating point or normalized color format,\neach fragment output color value is replicated to M values which **can**each be modulated (multiplied) by that color sample’s associated value ofR.\nWhich components are modulated is controlled by[`Self::coverage_modulation_mode`].\n\nIf this structure is not included in the [`Self::p_next`] chain, it is as if[`Self::coverage_modulation_mode`] is [`crate::vk::CoverageModulationModeNV::NONE_NV`].\n\nIf the [coverage reduction mode](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-coverage-reduction) is[`crate::vk::CoverageReductionModeNV::TRUNCATE_NV`], each color sample is\nassociated with only a single coverage sample.\nIn this case, it is as if [`Self::coverage_modulation_mode`] is[`crate::vk::CoverageModulationModeNV::NONE_NV`].\n\nValid Usage\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-coverageModulationTableEnable-01405  \n   If [`Self::coverage_modulation_table_enable`] is [`crate::vk::TRUE`],[`Self::coverage_modulation_table_count`] **must** be equal to the number of\n  rasterization samples divided by the number of color samples in the\n  subpass\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-coverageModulationMode-parameter  \n  [`Self::coverage_modulation_mode`] **must** be a valid [`crate::vk::CoverageModulationModeNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CoverageModulationModeNV`], [`crate::vk::PipelineCoverageModulationStateCreateFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineCoverageModulationStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineCoverageModulationStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::nv_framebuffer_mixed_samples::PipelineCoverageModulationStateCreateFlagsNV,
    pub coverage_modulation_mode: crate::extensions::nv_framebuffer_mixed_samples::CoverageModulationModeNV,
    pub coverage_modulation_table_enable: crate::vk1_0::Bool32,
    pub coverage_modulation_table_count: u32,
    pub p_coverage_modulation_table: *const std::os::raw::c_float,
}
impl PipelineCoverageModulationStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV;
}
impl Default for PipelineCoverageModulationStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), coverage_modulation_mode: Default::default(), coverage_modulation_table_enable: Default::default(), coverage_modulation_table_count: Default::default(), p_coverage_modulation_table: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineCoverageModulationStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineCoverageModulationStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("coverage_modulation_mode", &self.coverage_modulation_mode).field("coverage_modulation_table_enable", &(self.coverage_modulation_table_enable != 0)).field("coverage_modulation_table_count", &self.coverage_modulation_table_count).field("p_coverage_modulation_table", &self.p_coverage_modulation_table).finish()
    }
}
impl PipelineCoverageModulationStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
        PipelineCoverageModulationStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageModulationStateCreateInfoNV.html)) · Builder of [`PipelineCoverageModulationStateCreateInfoNV`] <br/> VkPipelineCoverageModulationStateCreateInfoNV - Structure specifying parameters controlling coverage modulation\n[](#_c_specification)C Specification\n----------\n\nAs part of coverage reduction, fragment color values **can** also be modulated\n(multiplied) by a value that is a function of fraction of covered\nrasterization samples associated with that color sample.\n\nPipeline state controlling coverage modulation is specified through the\nmembers of the [`crate::vk::PipelineCoverageModulationStateCreateInfoNV`]structure.\n\nThe [`crate::vk::PipelineCoverageModulationStateCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_framebuffer_mixed_samples\ntypedef struct VkPipelineCoverageModulationStateCreateInfoNV {\n    VkStructureType                                   sType;\n    const void*                                       pNext;\n    VkPipelineCoverageModulationStateCreateFlagsNV    flags;\n    VkCoverageModulationModeNV                        coverageModulationMode;\n    VkBool32                                          coverageModulationTableEnable;\n    uint32_t                                          coverageModulationTableCount;\n    const float*                                      pCoverageModulationTable;\n} VkPipelineCoverageModulationStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::coverage_modulation_mode`] is a [`crate::vk::CoverageModulationModeNV`] value\n  controlling which color components are modulated.\n\n* [`Self::coverage_modulation_table_enable`] controls whether the modulation\n  factor is looked up from a table in [`Self::p_coverage_modulation_table`].\n\n* [`Self::coverage_modulation_table_count`] is the number of elements in[`Self::p_coverage_modulation_table`].\n\n* [`Self::p_coverage_modulation_table`] is a table of modulation factors\n  containing a value for each number of covered samples.\n[](#_description)Description\n----------\n\nIf [`Self::coverage_modulation_table_enable`] is [`crate::vk::FALSE`], then for each\ncolor sample the associated bits of the pixel coverage are counted and\ndivided by the number of associated bits to produce a modulation factorR in the range (0,1] (a value of zero would have been killed due\nto a color coverage of 0).\nSpecifically:\n\n* N = value of `rasterizationSamples`\n\n* M = value of [`crate::vk::AttachmentDescription::samples`] for any\n  color attachments\n\n* R = popcount(associated coverage bits) / (N / M)\n\nIf [`Self::coverage_modulation_table_enable`] is [`crate::vk::TRUE`], the value Ris computed using a programmable lookup table.\nThe lookup table has N / M elements, and the element of the table is\nselected by:\n\n* R = [`Self::p_coverage_modulation_table`][popcount(associated coverage\n  bits)-1]\n\nNote that the table does not have an entry for popcount(associated\ncoverage bits) = 0, because such samples would have been killed.\n\nThe values of [`Self::p_coverage_modulation_table`] **may** be rounded to an\nimplementation-dependent precision, which is at least as fine as 1 /\nN, and clamped to [0,1].\n\nFor each color attachment with a floating point or normalized color format,\neach fragment output color value is replicated to M values which **can**each be modulated (multiplied) by that color sample’s associated value ofR.\nWhich components are modulated is controlled by[`Self::coverage_modulation_mode`].\n\nIf this structure is not included in the [`Self::p_next`] chain, it is as if[`Self::coverage_modulation_mode`] is [`crate::vk::CoverageModulationModeNV::NONE_NV`].\n\nIf the [coverage reduction mode](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-coverage-reduction) is[`crate::vk::CoverageReductionModeNV::TRUNCATE_NV`], each color sample is\nassociated with only a single coverage sample.\nIn this case, it is as if [`Self::coverage_modulation_mode`] is[`crate::vk::CoverageModulationModeNV::NONE_NV`].\n\nValid Usage\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-coverageModulationTableEnable-01405  \n   If [`Self::coverage_modulation_table_enable`] is [`crate::vk::TRUE`],[`Self::coverage_modulation_table_count`] **must** be equal to the number of\n  rasterization samples divided by the number of color samples in the\n  subpass\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineCoverageModulationStateCreateInfoNV-coverageModulationMode-parameter  \n  [`Self::coverage_modulation_mode`] **must** be a valid [`crate::vk::CoverageModulationModeNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CoverageModulationModeNV`], [`crate::vk::PipelineCoverageModulationStateCreateFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineCoverageModulationStateCreateInfoNVBuilder<'a>(PipelineCoverageModulationStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
        PipelineCoverageModulationStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_framebuffer_mixed_samples::PipelineCoverageModulationStateCreateFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn coverage_modulation_mode(mut self, coverage_modulation_mode: crate::extensions::nv_framebuffer_mixed_samples::CoverageModulationModeNV) -> Self {
        self.0.coverage_modulation_mode = coverage_modulation_mode as _;
        self
    }
    #[inline]
    pub fn coverage_modulation_table_enable(mut self, coverage_modulation_table_enable: bool) -> Self {
        self.0.coverage_modulation_table_enable = coverage_modulation_table_enable as _;
        self
    }
    #[inline]
    pub fn coverage_modulation_table(mut self, coverage_modulation_table: &'a [std::os::raw::c_float]) -> Self {
        self.0.p_coverage_modulation_table = coverage_modulation_table.as_ptr() as _;
        self.0.coverage_modulation_table_count = coverage_modulation_table.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineCoverageModulationStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
    type Target = PipelineCoverageModulationStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineCoverageModulationStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
