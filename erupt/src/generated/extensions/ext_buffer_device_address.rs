#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_BUFFER_DEVICE_ADDRESS_SPEC_VERSION")]
pub const EXT_BUFFER_DEVICE_ADDRESS_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME")]
pub const EXT_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_buffer_device_address");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_BUFFER_DEVICE_ADDRESS_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetBufferDeviceAddressEXT");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferAddressFeaturesEXT.html)) · Alias <br/> VkPhysicalDeviceBufferDeviceAddressFeaturesEXT - Structure describing buffer address features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkPhysicalDeviceBufferDeviceAddressFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           bufferDeviceAddress;\n    VkBool32           bufferDeviceAddressCaptureReplay;\n    VkBool32           bufferDeviceAddressMultiDevice;\n} VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() `bufferDeviceAddress` indicates\n  that the implementation supports accessing buffer memory in shaders as\n  storage buffers via an address queried from[`crate::vk::PFN_vkGetBufferDeviceAddress`].\n\n* []()`bufferDeviceAddressCaptureReplay` indicates that the implementation\n  supports saving and reusing buffer addresses, e.g. for trace capture and\n  replay.\n\n* []()`bufferDeviceAddressMultiDevice` indicates that the implementation\n  supports the `bufferDeviceAddress` feature for logical devices\n  created with multiple physical devices.\n  If this feature is not supported, buffer addresses **must** not be queried\n  on a logical device created with more than one physical device.\n\nIf the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is included in the `pNext` chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] **can** also be used in the `pNext` chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\n|   |Note<br/><br/>The [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure has the<br/>same members as the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`]structure, but the functionality indicated by the members is expressed<br/>differently.<br/>The features indicated by the[`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`] structure requires<br/>additional flags to be passed at memory allocation time, and the capture and<br/>replay mechanism is built around opaque capture addresses for buffer and<br/>memory objects.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBufferDeviceAddressFeaturesEXT-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceBufferAddressFeaturesEXT")]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceBufferAddressFeaturesEXT = crate::extensions::ext_buffer_device_address::PhysicalDeviceBufferDeviceAddressFeaturesEXT;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferAddressFeaturesEXT.html)) · Alias <br/> VkPhysicalDeviceBufferDeviceAddressFeaturesEXT - Structure describing buffer address features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkPhysicalDeviceBufferDeviceAddressFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           bufferDeviceAddress;\n    VkBool32           bufferDeviceAddressCaptureReplay;\n    VkBool32           bufferDeviceAddressMultiDevice;\n} VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() `bufferDeviceAddress` indicates\n  that the implementation supports accessing buffer memory in shaders as\n  storage buffers via an address queried from[`crate::vk::PFN_vkGetBufferDeviceAddress`].\n\n* []()`bufferDeviceAddressCaptureReplay` indicates that the implementation\n  supports saving and reusing buffer addresses, e.g. for trace capture and\n  replay.\n\n* []()`bufferDeviceAddressMultiDevice` indicates that the implementation\n  supports the `bufferDeviceAddress` feature for logical devices\n  created with multiple physical devices.\n  If this feature is not supported, buffer addresses **must** not be queried\n  on a logical device created with more than one physical device.\n\nIf the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is included in the `pNext` chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] **can** also be used in the `pNext` chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\n|   |Note<br/><br/>The [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure has the<br/>same members as the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`]structure, but the functionality indicated by the members is expressed<br/>differently.<br/>The features indicated by the[`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`] structure requires<br/>additional flags to be passed at memory allocation time, and the capture and<br/>replay mechanism is built around opaque capture addresses for buffer and<br/>memory objects.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBufferDeviceAddressFeaturesEXT-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceBufferAddressFeaturesEXT")]
#[allow(non_camel_case_types)]
pub type PhysicalDeviceBufferAddressFeaturesEXTBuilder<'a> = crate::extensions::ext_buffer_device_address::PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressInfoEXT.html)) · Alias <br/> VkBufferDeviceAddressInfo - Structure specifying the buffer to query an address for\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BufferDeviceAddressInfo`] structure is defined as:\n\n```\n// Provided by VK_VERSION_1_2\ntypedef struct VkBufferDeviceAddressInfo {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBuffer           buffer;\n} VkBufferDeviceAddressInfo;\n```\n\nor the equivalent\n\n```\n// Provided by VK_KHR_buffer_device_address\ntypedef VkBufferDeviceAddressInfo VkBufferDeviceAddressInfoKHR;\n```\n\nor the equivalent\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef VkBufferDeviceAddressInfo VkBufferDeviceAddressInfoEXT;\n```\n[](#_members)Members\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `buffer` specifies the buffer whose address is being queried.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-02600  \n   If `buffer` is non-sparse and was not created with the[`crate::vk::BufferCreateFlagBits::DEVICE_ADDRESS_CAPTURE_REPLAY`] flag, then it**must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-02601  \n  `buffer` **must** have been created with[`crate::vk::BufferUsageFlagBits::SHADER_DEVICE_ADDRESS`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkBufferDeviceAddressInfo-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::BUFFER_DEVICE_ADDRESS_INFO`]\n\n* []() VUID-VkBufferDeviceAddressInfo-pNext-pNext  \n  `pNext` **must** be `NULL`\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-parameter  \n  `buffer` **must** be a valid [`crate::vk::Buffer`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::StructureType`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferOpaqueCaptureAddress`], [`crate::vk::PFN_vkGetBufferOpaqueCaptureAddress`]\n"]
#[doc(alias = "VkBufferDeviceAddressInfoEXT")]
#[allow(non_camel_case_types)]
pub type BufferDeviceAddressInfoEXT = crate::vk1_2::BufferDeviceAddressInfo;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressInfoEXT.html)) · Alias <br/> VkBufferDeviceAddressInfo - Structure specifying the buffer to query an address for\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::BufferDeviceAddressInfo`] structure is defined as:\n\n```\n// Provided by VK_VERSION_1_2\ntypedef struct VkBufferDeviceAddressInfo {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBuffer           buffer;\n} VkBufferDeviceAddressInfo;\n```\n\nor the equivalent\n\n```\n// Provided by VK_KHR_buffer_device_address\ntypedef VkBufferDeviceAddressInfo VkBufferDeviceAddressInfoKHR;\n```\n\nor the equivalent\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef VkBufferDeviceAddressInfo VkBufferDeviceAddressInfoEXT;\n```\n[](#_members)Members\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `buffer` specifies the buffer whose address is being queried.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-02600  \n   If `buffer` is non-sparse and was not created with the[`crate::vk::BufferCreateFlagBits::DEVICE_ADDRESS_CAPTURE_REPLAY`] flag, then it**must** be bound completely and contiguously to a single[`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-02601  \n  `buffer` **must** have been created with[`crate::vk::BufferUsageFlagBits::SHADER_DEVICE_ADDRESS`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkBufferDeviceAddressInfo-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::BUFFER_DEVICE_ADDRESS_INFO`]\n\n* []() VUID-VkBufferDeviceAddressInfo-pNext-pNext  \n  `pNext` **must** be `NULL`\n\n* []() VUID-VkBufferDeviceAddressInfo-buffer-parameter  \n  `buffer` **must** be a valid [`crate::vk::Buffer`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::StructureType`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferDeviceAddress`], [`crate::vk::PFN_vkGetBufferOpaqueCaptureAddress`], [`crate::vk::PFN_vkGetBufferOpaqueCaptureAddress`]\n"]
#[doc(alias = "VkBufferDeviceAddressInfoEXT")]
#[allow(non_camel_case_types)]
pub type BufferDeviceAddressInfoEXTBuilder<'a> = crate::vk1_2::BufferDeviceAddressInfoBuilder<'a>;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferDeviceAddressEXT.html)) · Alias <br/> vkGetBufferDeviceAddress - Query an address of a buffer\n[](#_c_specification)C Specification\n----------\n\nTo query a 64-bit buffer device address value through which buffer memory**can** be accessed in a shader, call:\n\n```\n// Provided by VK_VERSION_1_2\nVkDeviceAddress vkGetBufferDeviceAddress(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n\nor the equivalent command\n\n```\n// Provided by VK_KHR_buffer_device_address\nVkDeviceAddress vkGetBufferDeviceAddressKHR(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n\nor the equivalent command\n\n```\n// Provided by VK_EXT_buffer_device_address\nVkDeviceAddress vkGetBufferDeviceAddressEXT(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* `device` is the logical device that the buffer was created on.\n\n* `pInfo` is a pointer to a [`crate::vk::BufferDeviceAddressInfo`] structure\n  specifying the buffer to retrieve an address for.\n[](#_description)Description\n----------\n\nThe 64-bit return value is an address of the start of `pInfo->buffer`.\nThe address range starting at this value and whose size is the size of the\nbuffer **can** be used in a shader to access the memory bound to that buffer,\nusing the`SPV_KHR_physical_storage_buffer` extension\nor the equivalent`SPV_EXT_physical_storage_buffer` extension\nand the `PhysicalStorageBuffer` storage class.\nFor example, this value **can** be stored in a uniform buffer, and the shader**can** read the value from the uniform buffer and use it to do a dependent\nread/write to this buffer.\nA value of zero is reserved as a “null” pointer and **must** not be returned\nas a valid buffer device address.\nAll loads, stores, and atomics in a shader through`PhysicalStorageBuffer` pointers **must** access addresses in the address\nrange of some buffer.\n\nIf the buffer was created with a non-zero value of[`crate::vk::BufferOpaqueCaptureAddressCreateInfo::opaque_capture_address`]or[`crate::vk::BufferDeviceAddressCreateInfoEXT::device_address`]the return value will be the same address that was returned at capture time.\n\nValid Usage\n\n* []() VUID-vkGetBufferDeviceAddress-bufferDeviceAddress-03324  \n   The [bufferDeviceAddress](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddress) or[[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`]::`bufferDeviceAddress`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressEXT)feature **must** be enabled\n\n* []() VUID-vkGetBufferDeviceAddress-device-03325  \n   If `device` was created with multiple physical devices, then the[bufferDeviceAddressMultiDevice](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressMultiDevice)or[[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`]::`bufferDeviceAddressMultiDevice`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressMultiDeviceEXT)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetBufferDeviceAddress-device-parameter  \n  `device` **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetBufferDeviceAddress-pInfo-parameter  \n  `pInfo` **must** be a valid pointer to a valid [`crate::vk::BufferDeviceAddressInfo`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::BufferDeviceAddressInfo`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetBufferDeviceAddressEXT = crate::vk1_2::PFN_vkGetBufferDeviceAddress;
#[doc = "Provided by [`crate::extensions::ext_buffer_device_address`]"]
impl crate::vk1_0::BufferCreateFlagBits {
    pub const DEVICE_ADDRESS_CAPTURE_REPLAY_EXT: Self = Self::DEVICE_ADDRESS_CAPTURE_REPLAY;
}
#[doc = "Provided by [`crate::extensions::ext_buffer_device_address`]"]
impl crate::vk1_0::BufferUsageFlagBits {
    pub const SHADER_DEVICE_ADDRESS_EXT: Self = Self::SHADER_DEVICE_ADDRESS;
}
#[doc = "Provided by [`crate::extensions::ext_buffer_device_address`]"]
impl crate::vk1_0::Result {
    pub const ERROR_INVALID_DEVICE_ADDRESS_EXT: Self = Self::ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS;
}
#[doc = "Provided by [`crate::extensions::ext_buffer_device_address`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT: Self = Self(1000244000);
    pub const BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT: Self = Self(1000244002);
    pub const PHYSICAL_DEVICE_BUFFER_ADDRESS_FEATURES_EXT: Self = Self::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT;
    pub const BUFFER_DEVICE_ADDRESS_INFO_EXT: Self = Self::BUFFER_DEVICE_ADDRESS_INFO;
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceBufferDeviceAddressFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, BufferDeviceAddressCreateInfoEXT> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, BufferDeviceAddressCreateInfoEXTBuilder<'_>> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBufferDeviceAddressFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferDeviceAddressFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceBufferDeviceAddressFeaturesEXT - Structure describing buffer address features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkPhysicalDeviceBufferDeviceAddressFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           bufferDeviceAddress;\n    VkBool32           bufferDeviceAddressCaptureReplay;\n    VkBool32           bufferDeviceAddressMultiDevice;\n} VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::buffer_device_address`] indicates\n  that the implementation supports accessing buffer memory in shaders as\n  storage buffers via an address queried from[`crate::vk::PFN_vkGetBufferDeviceAddress`].\n\n* []()[`Self::buffer_device_address_capture_replay`] indicates that the implementation\n  supports saving and reusing buffer addresses, e.g. for trace capture and\n  replay.\n\n* []()[`Self::buffer_device_address_multi_device`] indicates that the implementation\n  supports the [`Self::buffer_device_address`] feature for logical devices\n  created with multiple physical devices.\n  If this feature is not supported, buffer addresses **must** not be queried\n  on a logical device created with more than one physical device.\n\nIf the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\n|   |Note<br/><br/>The [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure has the<br/>same members as the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`]structure, but the functionality indicated by the members is expressed<br/>differently.<br/>The features indicated by the[`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`] structure requires<br/>additional flags to be passed at memory allocation time, and the capture and<br/>replay mechanism is built around opaque capture addresses for buffer and<br/>memory objects.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBufferDeviceAddressFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceBufferDeviceAddressFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub buffer_device_address: crate::vk1_0::Bool32,
    pub buffer_device_address_capture_replay: crate::vk1_0::Bool32,
    pub buffer_device_address_multi_device: crate::vk1_0::Bool32,
}
impl PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT;
}
impl Default for PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), buffer_device_address: Default::default(), buffer_device_address_capture_replay: Default::default(), buffer_device_address_multi_device: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceBufferDeviceAddressFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("buffer_device_address", &(self.buffer_device_address != 0)).field("buffer_device_address_capture_replay", &(self.buffer_device_address_capture_replay != 0)).field("buffer_device_address_multi_device", &(self.buffer_device_address_multi_device != 0)).finish()
    }
}
impl PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
        PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBufferDeviceAddressFeaturesEXT.html)) · Builder of [`PhysicalDeviceBufferDeviceAddressFeaturesEXT`] <br/> VkPhysicalDeviceBufferDeviceAddressFeaturesEXT - Structure describing buffer address features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkPhysicalDeviceBufferDeviceAddressFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           bufferDeviceAddress;\n    VkBool32           bufferDeviceAddressCaptureReplay;\n    VkBool32           bufferDeviceAddressMultiDevice;\n} VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::buffer_device_address`] indicates\n  that the implementation supports accessing buffer memory in shaders as\n  storage buffers via an address queried from[`crate::vk::PFN_vkGetBufferDeviceAddress`].\n\n* []()[`Self::buffer_device_address_capture_replay`] indicates that the implementation\n  supports saving and reusing buffer addresses, e.g. for trace capture and\n  replay.\n\n* []()[`Self::buffer_device_address_multi_device`] indicates that the implementation\n  supports the [`Self::buffer_device_address`] feature for logical devices\n  created with multiple physical devices.\n  If this feature is not supported, buffer addresses **must** not be queried\n  on a logical device created with more than one physical device.\n\nIf the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\n|   |Note<br/><br/>The [`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`] structure has the<br/>same members as the [`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`]structure, but the functionality indicated by the members is expressed<br/>differently.<br/>The features indicated by the[`crate::vk::PhysicalDeviceBufferDeviceAddressFeatures`] structure requires<br/>additional flags to be passed at memory allocation time, and the capture and<br/>replay mechanism is built around opaque capture addresses for buffer and<br/>memory objects.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBufferDeviceAddressFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a>(PhysicalDeviceBufferDeviceAddressFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
        PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer_device_address(mut self, buffer_device_address: bool) -> Self {
        self.0.buffer_device_address = buffer_device_address as _;
        self
    }
    #[inline]
    pub fn buffer_device_address_capture_replay(mut self, buffer_device_address_capture_replay: bool) -> Self {
        self.0.buffer_device_address_capture_replay = buffer_device_address_capture_replay as _;
        self
    }
    #[inline]
    pub fn buffer_device_address_multi_device(mut self, buffer_device_address_multi_device: bool) -> Self {
        self.0.buffer_device_address_multi_device = buffer_device_address_multi_device as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceBufferDeviceAddressFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceBufferDeviceAddressFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceBufferDeviceAddressFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressCreateInfoEXT.html)) · Structure <br/> VkBufferDeviceAddressCreateInfoEXT - Request a specific address for a buffer\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to\nrequest a specific device address for a buffer, add a[`crate::vk::BufferDeviceAddressCreateInfoEXT`] structure to the [`Self::p_next`] chain\nof the [`crate::vk::BufferCreateInfo`] structure.\nThe [`crate::vk::BufferDeviceAddressCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkBufferDeviceAddressCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkDeviceAddress    deviceAddress;\n} VkBufferDeviceAddressCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::device_address`] is the device address requested for the buffer.\n[](#_description)Description\n----------\n\nIf [`Self::device_address`] is zero, no specific address is requested.\n\nIf [`Self::device_address`] is not zero, then it **must** be an address retrieved\nfrom an identically created buffer on the same implementation.\nThe buffer **must** also be bound to an identically created[`crate::vk::DeviceMemory`] object.\n\nIf this structure is not present, it is as if [`Self::device_address`] is zero.\n\nApps **should** avoid creating buffers with app-provided addresses and\nimplementation-provided addresses in the same process, to reduce the\nlikelihood of [`crate::vk::Result::ERROR_INVALID_DEVICE_ADDRESS_EXT`] errors.\n\nValid Usage (Implicit)\n\n* []() VUID-VkBufferDeviceAddressCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkBufferDeviceAddressCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct BufferDeviceAddressCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub device_address: crate::vk1_0::DeviceAddress,
}
impl BufferDeviceAddressCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT;
}
impl Default for BufferDeviceAddressCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), device_address: Default::default() }
    }
}
impl std::fmt::Debug for BufferDeviceAddressCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("BufferDeviceAddressCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("device_address", &self.device_address).finish()
    }
}
impl BufferDeviceAddressCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> BufferDeviceAddressCreateInfoEXTBuilder<'a> {
        BufferDeviceAddressCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBufferDeviceAddressCreateInfoEXT.html)) · Builder of [`BufferDeviceAddressCreateInfoEXT`] <br/> VkBufferDeviceAddressCreateInfoEXT - Request a specific address for a buffer\n[](#_c_specification)C Specification\n----------\n\nAlternatively, to\nrequest a specific device address for a buffer, add a[`crate::vk::BufferDeviceAddressCreateInfoEXT`] structure to the [`Self::p_next`] chain\nof the [`crate::vk::BufferCreateInfo`] structure.\nThe [`crate::vk::BufferDeviceAddressCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_buffer_device_address\ntypedef struct VkBufferDeviceAddressCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkDeviceAddress    deviceAddress;\n} VkBufferDeviceAddressCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::device_address`] is the device address requested for the buffer.\n[](#_description)Description\n----------\n\nIf [`Self::device_address`] is zero, no specific address is requested.\n\nIf [`Self::device_address`] is not zero, then it **must** be an address retrieved\nfrom an identically created buffer on the same implementation.\nThe buffer **must** also be bound to an identically created[`crate::vk::DeviceMemory`] object.\n\nIf this structure is not present, it is as if [`Self::device_address`] is zero.\n\nApps **should** avoid creating buffers with app-provided addresses and\nimplementation-provided addresses in the same process, to reduce the\nlikelihood of [`crate::vk::Result::ERROR_INVALID_DEVICE_ADDRESS_EXT`] errors.\n\nValid Usage (Implicit)\n\n* []() VUID-VkBufferDeviceAddressCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct BufferDeviceAddressCreateInfoEXTBuilder<'a>(BufferDeviceAddressCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> BufferDeviceAddressCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> BufferDeviceAddressCreateInfoEXTBuilder<'a> {
        BufferDeviceAddressCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn device_address(mut self, device_address: crate::vk1_0::DeviceAddress) -> Self {
        self.0.device_address = device_address as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> BufferDeviceAddressCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for BufferDeviceAddressCreateInfoEXTBuilder<'a> {
    fn default() -> BufferDeviceAddressCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for BufferDeviceAddressCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for BufferDeviceAddressCreateInfoEXTBuilder<'a> {
    type Target = BufferDeviceAddressCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for BufferDeviceAddressCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_buffer_device_address`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetBufferDeviceAddressEXT.html)) · Function <br/> vkGetBufferDeviceAddress - Query an address of a buffer\n[](#_c_specification)C Specification\n----------\n\nTo query a 64-bit buffer device address value through which buffer memory**can** be accessed in a shader, call:\n\n```\n// Provided by VK_VERSION_1_2\nVkDeviceAddress vkGetBufferDeviceAddress(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n\nor the equivalent command\n\n```\n// Provided by VK_KHR_buffer_device_address\nVkDeviceAddress vkGetBufferDeviceAddressKHR(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n\nor the equivalent command\n\n```\n// Provided by VK_EXT_buffer_device_address\nVkDeviceAddress vkGetBufferDeviceAddressEXT(\n    VkDevice                                    device,\n    const VkBufferDeviceAddressInfo*            pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* `device` is the logical device that the buffer was created on.\n\n* `pInfo` is a pointer to a [`crate::vk::BufferDeviceAddressInfo`] structure\n  specifying the buffer to retrieve an address for.\n[](#_description)Description\n----------\n\nThe 64-bit return value is an address of the start of `pInfo->buffer`.\nThe address range starting at this value and whose size is the size of the\nbuffer **can** be used in a shader to access the memory bound to that buffer,\nusing the`SPV_KHR_physical_storage_buffer` extension\nor the equivalent`SPV_EXT_physical_storage_buffer` extension\nand the `PhysicalStorageBuffer` storage class.\nFor example, this value **can** be stored in a uniform buffer, and the shader**can** read the value from the uniform buffer and use it to do a dependent\nread/write to this buffer.\nA value of zero is reserved as a “null” pointer and **must** not be returned\nas a valid buffer device address.\nAll loads, stores, and atomics in a shader through`PhysicalStorageBuffer` pointers **must** access addresses in the address\nrange of some buffer.\n\nIf the buffer was created with a non-zero value of[`crate::vk::BufferOpaqueCaptureAddressCreateInfo::opaque_capture_address`]or[`crate::vk::BufferDeviceAddressCreateInfoEXT::device_address`]the return value will be the same address that was returned at capture time.\n\nValid Usage\n\n* []() VUID-vkGetBufferDeviceAddress-bufferDeviceAddress-03324  \n   The [bufferDeviceAddress](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddress) or[[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`]::`bufferDeviceAddress`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressEXT)feature **must** be enabled\n\n* []() VUID-vkGetBufferDeviceAddress-device-03325  \n   If `device` was created with multiple physical devices, then the[bufferDeviceAddressMultiDevice](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressMultiDevice)or[[`crate::vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT`]::`bufferDeviceAddressMultiDevice`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-bufferDeviceAddressMultiDeviceEXT)feature **must** be enabled\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetBufferDeviceAddress-device-parameter  \n  `device` **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetBufferDeviceAddress-pInfo-parameter  \n  `pInfo` **must** be a valid pointer to a valid [`crate::vk::BufferDeviceAddressInfo`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::BufferDeviceAddressInfo`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkGetBufferDeviceAddressEXT")]
    pub unsafe fn get_buffer_device_address_ext(&self, info: &crate::vk1_2::BufferDeviceAddressInfo) -> crate::vk1_0::DeviceAddress {
        let _function = self.get_buffer_device_address_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, info as _);
        _return
    }
}
