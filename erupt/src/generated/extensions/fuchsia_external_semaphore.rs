#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_FUCHSIA_EXTERNAL_SEMAPHORE_SPEC_VERSION")]
pub const FUCHSIA_EXTERNAL_SEMAPHORE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_FUCHSIA_EXTERNAL_SEMAPHORE_EXTENSION_NAME")]
pub const FUCHSIA_EXTERNAL_SEMAPHORE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_FUCHSIA_external_semaphore");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_SEMAPHORE_ZIRCON_HANDLE_FUCHSIA: *const std::os::raw::c_char = crate::cstr!("vkGetSemaphoreZirconHandleFUCHSIA");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_IMPORT_SEMAPHORE_ZIRCON_HANDLE_FUCHSIA: *const std::os::raw::c_char = crate::cstr!("vkImportSemaphoreZirconHandleFUCHSIA");
#[doc = "Provided by [`crate::extensions::fuchsia_external_semaphore`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA: Self = Self(1000365000);
    pub const SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA: Self = Self(1000365001);
}
#[doc = "Provided by [`crate::extensions::fuchsia_external_semaphore`]"]
impl crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits {
    pub const ZIRCON_EVENT_FUCHSIA: Self = Self(128);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSemaphoreZirconHandleFUCHSIA.html)) · Function <br/> vkGetSemaphoreZirconHandleFUCHSIA - Get a Zircon event handle for a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo export a Zircon event handle representing the payload of a semaphore,\ncall:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\nVkResult vkGetSemaphoreZirconHandleFUCHSIA(\n    VkDevice                                    device,\n    const VkSemaphoreGetZirconHandleInfoFUCHSIA* pGetZirconHandleInfo,\n    zx_handle_t*                                pZirconHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore being\n  exported.\n\n* [`Self::p_get_zircon_handle_info`] is a pointer to a[`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure containing\n  parameters of the export operation.\n\n* [`Self::p_zircon_handle`] will return the Zircon event handle representing the\n  semaphore payload.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_semaphore_zircon_handle_fuchsia`] **must** create a Zircon\nevent handle and transfer ownership of it to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nZircon event handle when it is no longer needed.\n\n|   |Note<br/><br/>Ownership can be released in many ways.<br/>For example, the application can call zx\\_handle\\_close() on the file<br/>descriptor, or transfer ownership back to Vulkan by using the file<br/>descriptor to import a semaphore payload.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nExporting a Zircon event handle from a semaphore **may** have side effects\ndepending on the transference of the specified handle type, as described in[Importing Semaphore State](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-pGetZirconHandleInfo-parameter  \n  [`Self::p_get_zircon_handle_info`] **must** be a valid pointer to a valid [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-pZirconHandle-parameter  \n  [`Self::p_zircon_handle`] **must** be a valid pointer to a `zx_handle_t` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetSemaphoreZirconHandleFUCHSIA = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_zircon_handle_info: *const crate::extensions::fuchsia_external_semaphore::SemaphoreGetZirconHandleInfoFUCHSIA, p_zircon_handle: *mut *mut std::ffi::c_void) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportSemaphoreZirconHandleFUCHSIA.html)) · Function <br/> vkImportSemaphoreZirconHandleFUCHSIA - Import a semaphore from a Zircon event handle\n[](#_c_specification)C Specification\n----------\n\nTo import a semaphore payload from a Zircon event handle, call:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\nVkResult vkImportSemaphoreZirconHandleFUCHSIA(\n    VkDevice                                    device,\n    const VkImportSemaphoreZirconHandleInfoFUCHSIA* pImportSemaphoreZirconHandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore.\n\n* [`Self::p_import_semaphore_zircon_handle_info`] is a pointer to a[`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure specifying the\n  semaphore and import parameters.\n[](#_description)Description\n----------\n\nImporting a semaphore payload from a Zircon event handle transfers ownership\nof the handle from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the handle after a\nsuccessful import.\n\nApplications **can** import the same semaphore payload into multiple instances\nof Vulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-semaphore-04764  \n  `semaphore` **must** not be associated with any queue command that has\n  not yet completed execution on that queue.\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-pImportSemaphoreZirconHandleInfo-parameter  \n  [`Self::p_import_semaphore_zircon_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkImportSemaphoreZirconHandleFUCHSIA = unsafe extern "system" fn(device: crate::vk1_0::Device, p_import_semaphore_zircon_handle_info: *const crate::extensions::fuchsia_external_semaphore::ImportSemaphoreZirconHandleInfoFUCHSIA) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportSemaphoreZirconHandleInfoFUCHSIA.html)) · Structure <br/> VkImportSemaphoreZirconHandleInfoFUCHSIA - Structure specifying Zircon event handle to import to a semaphore\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure is defined as:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\ntypedef struct VkImportSemaphoreZirconHandleInfoFUCHSIA {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkSemaphoreImportFlags                   flags;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n    zx_handle_t                              zirconHandle;\n} VkImportSemaphoreZirconHandleInfoFUCHSIA;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore into which the payload will be\n  imported.\n\n* [`Self::flags`] is a bitmask of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) specifying\n  additional parameters for the semaphore payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of [`Self::zircon_handle`].\n\n* [`Self::zircon_handle`] is the external handle to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                        Handle Type                         |Transference|Permanence Supported|\n|------------------------------------------------------------|------------|--------------------|\n|[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::ZIRCON_EVENT_FUCHSIA`]| Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-handleType-04765  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types\n  Supported by [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphore-handletypes-zircon-handle) table.\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-zirconHandle-04766  \n  [`Self::zircon_handle`] **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility).\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-zirconHandle-04767  \n  [`Self::zircon_handle`] **must** have `ZX_RIGHTS_BASIC` and`ZX_RIGHTS_SIGNAL` rights.\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-semaphoreType-04768  \n   The [`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field **must** not\n  be [`crate::vk::SemaphoreType::TIMELINE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA`]\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) values\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n\nHost Synchronization\n\n* Host access to [`Self::semaphore`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::SemaphoreImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_semaphore_zircon_handle_fuchsia`]\n"]
#[doc(alias = "VkImportSemaphoreZirconHandleInfoFUCHSIA")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportSemaphoreZirconHandleInfoFUCHSIA {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub semaphore: crate::vk1_0::Semaphore,
    pub flags: crate::vk1_1::SemaphoreImportFlags,
    pub handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits,
    pub zircon_handle: *mut std::ffi::c_void,
}
impl ImportSemaphoreZirconHandleInfoFUCHSIA {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA;
}
impl Default for ImportSemaphoreZirconHandleInfoFUCHSIA {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), semaphore: Default::default(), flags: Default::default(), handle_type: Default::default(), zircon_handle: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ImportSemaphoreZirconHandleInfoFUCHSIA {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportSemaphoreZirconHandleInfoFUCHSIA").field("s_type", &self.s_type).field("p_next", &self.p_next).field("semaphore", &self.semaphore).field("flags", &self.flags).field("handle_type", &self.handle_type).field("zircon_handle", &self.zircon_handle).finish()
    }
}
impl ImportSemaphoreZirconHandleInfoFUCHSIA {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
        ImportSemaphoreZirconHandleInfoFUCHSIABuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportSemaphoreZirconHandleInfoFUCHSIA.html)) · Builder of [`ImportSemaphoreZirconHandleInfoFUCHSIA`] <br/> VkImportSemaphoreZirconHandleInfoFUCHSIA - Structure specifying Zircon event handle to import to a semaphore\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure is defined as:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\ntypedef struct VkImportSemaphoreZirconHandleInfoFUCHSIA {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkSemaphoreImportFlags                   flags;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n    zx_handle_t                              zirconHandle;\n} VkImportSemaphoreZirconHandleInfoFUCHSIA;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore into which the payload will be\n  imported.\n\n* [`Self::flags`] is a bitmask of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) specifying\n  additional parameters for the semaphore payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of [`Self::zircon_handle`].\n\n* [`Self::zircon_handle`] is the external handle to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                        Handle Type                         |Transference|Permanence Supported|\n|------------------------------------------------------------|------------|--------------------|\n|[`crate::vk::ExternalSemaphoreHandleTypeFlagBits::ZIRCON_EVENT_FUCHSIA`]| Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-handleType-04765  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types\n  Supported by [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphore-handletypes-zircon-handle) table.\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-zirconHandle-04766  \n  [`Self::zircon_handle`] **must** obey any requirements listed for[`Self::handle_type`] in[external semaphore\n  handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-semaphore-handle-types-compatibility).\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-zirconHandle-04767  \n  [`Self::zircon_handle`] **must** have `ZX_RIGHTS_BASIC` and`ZX_RIGHTS_SIGNAL` rights.\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-semaphoreType-04768  \n   The [`crate::vk::SemaphoreTypeCreateInfo::semaphore_type`] field **must** not\n  be [`crate::vk::SemaphoreType::TIMELINE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA`]\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkSemaphoreImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreImportFlagBits.html) values\n\n* []() VUID-VkImportSemaphoreZirconHandleInfoFUCHSIA-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n\nHost Synchronization\n\n* Host access to [`Self::semaphore`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::SemaphoreImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_semaphore_zircon_handle_fuchsia`]\n"]
#[repr(transparent)]
pub struct ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a>(ImportSemaphoreZirconHandleInfoFUCHSIA, std::marker::PhantomData<&'a ()>);
impl<'a> ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
    #[inline]
    pub fn new() -> ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
        ImportSemaphoreZirconHandleInfoFUCHSIABuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn semaphore(mut self, semaphore: crate::vk1_0::Semaphore) -> Self {
        self.0.semaphore = semaphore as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::vk1_1::SemaphoreImportFlags) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn zircon_handle(mut self, zircon_handle: *mut std::ffi::c_void) -> Self {
        self.0.zircon_handle = zircon_handle;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportSemaphoreZirconHandleInfoFUCHSIA {
        self.0
    }
}
impl<'a> std::default::Default for ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
    fn default() -> ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
    type Target = ImportSemaphoreZirconHandleInfoFUCHSIA;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportSemaphoreZirconHandleInfoFUCHSIABuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreGetZirconHandleInfoFUCHSIA.html)) · Structure <br/> VkSemaphoreGetZirconHandleInfoFUCHSIA - Structure describing a Zircon event handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure is defined as:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\ntypedef struct VkSemaphoreGetZirconHandleInfoFUCHSIA {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n} VkSemaphoreGetZirconHandleInfoFUCHSIA;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the Zircon event handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) for a description of the\nproperties of the defined external semaphore handle types.\n\nValid Usage\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04758  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportSemaphoreCreateInfo::handle_types`] when[`Self::semaphore`]’s current payload was created.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-04759  \n  [`Self::semaphore`] **must** not currently have its payload replaced by an\n  imported payload as described below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing)unless that imported payload’s handle type was included in[`crate::vk::ExternalSemaphoreProperties::export_from_imported_handle_types`]for [`Self::handle_type`].\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04760  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, as defined below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing),\n  there **must** be no queue waiting on [`Self::semaphore`].\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04761  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::semaphore`] **must** be signaled, or have an\n  associated [semaphore signal\n  operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-signaling) pending execution.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04762  \n  [`Self::handle_type`] **must** be defined as a Zircon event handle.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-04763  \n  [`Self::semaphore`] **must** have been created with a [`crate::vk::SemaphoreType`] of[`crate::vk::SemaphoreType::BINARY`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA`]\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_semaphore_zircon_handle_fuchsia`]\n"]
#[doc(alias = "VkSemaphoreGetZirconHandleInfoFUCHSIA")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SemaphoreGetZirconHandleInfoFUCHSIA {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub semaphore: crate::vk1_0::Semaphore,
    pub handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits,
}
impl SemaphoreGetZirconHandleInfoFUCHSIA {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA;
}
impl Default for SemaphoreGetZirconHandleInfoFUCHSIA {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), semaphore: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for SemaphoreGetZirconHandleInfoFUCHSIA {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SemaphoreGetZirconHandleInfoFUCHSIA").field("s_type", &self.s_type).field("p_next", &self.p_next).field("semaphore", &self.semaphore).field("handle_type", &self.handle_type).finish()
    }
}
impl SemaphoreGetZirconHandleInfoFUCHSIA {
    #[inline]
    pub fn into_builder<'a>(self) -> SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
        SemaphoreGetZirconHandleInfoFUCHSIABuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSemaphoreGetZirconHandleInfoFUCHSIA.html)) · Builder of [`SemaphoreGetZirconHandleInfoFUCHSIA`] <br/> VkSemaphoreGetZirconHandleInfoFUCHSIA - Structure describing a Zircon event handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure is defined as:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\ntypedef struct VkSemaphoreGetZirconHandleInfoFUCHSIA {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    VkSemaphore                              semaphore;\n    VkExternalSemaphoreHandleTypeFlagBits    handleType;\n} VkSemaphoreGetZirconHandleInfoFUCHSIA;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::semaphore`] is the semaphore from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the Zircon event handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) for a description of the\nproperties of the defined external semaphore handle types.\n\nValid Usage\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04758  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportSemaphoreCreateInfo::handle_types`] when[`Self::semaphore`]’s current payload was created.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-04759  \n  [`Self::semaphore`] **must** not currently have its payload replaced by an\n  imported payload as described below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing)unless that imported payload’s handle type was included in[`crate::vk::ExternalSemaphoreProperties::export_from_imported_handle_types`]for [`Self::handle_type`].\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04760  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, as defined below in[Importing Semaphore Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing),\n  there **must** be no queue waiting on [`Self::semaphore`].\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04761  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::semaphore`] **must** be signaled, or have an\n  associated [semaphore signal\n  operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-signaling) pending execution.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-04762  \n  [`Self::handle_type`] **must** be defined as a Zircon event handle.\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-04763  \n  [`Self::semaphore`] **must** have been created with a [`crate::vk::SemaphoreType`] of[`crate::vk::SemaphoreType::BINARY`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA`]\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-semaphore-parameter  \n  [`Self::semaphore`] **must** be a valid [`crate::vk::Semaphore`] handle\n\n* []() VUID-VkSemaphoreGetZirconHandleInfoFUCHSIA-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalSemaphoreHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalSemaphoreHandleTypeFlagBits.html), [`crate::vk::Semaphore`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_semaphore_zircon_handle_fuchsia`]\n"]
#[repr(transparent)]
pub struct SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a>(SemaphoreGetZirconHandleInfoFUCHSIA, std::marker::PhantomData<&'a ()>);
impl<'a> SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
    #[inline]
    pub fn new() -> SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
        SemaphoreGetZirconHandleInfoFUCHSIABuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn semaphore(mut self, semaphore: crate::vk1_0::Semaphore) -> Self {
        self.0.semaphore = semaphore as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalSemaphoreHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SemaphoreGetZirconHandleInfoFUCHSIA {
        self.0
    }
}
impl<'a> std::default::Default for SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
    fn default() -> SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
    type Target = SemaphoreGetZirconHandleInfoFUCHSIA;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SemaphoreGetZirconHandleInfoFUCHSIABuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::fuchsia_external_semaphore`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSemaphoreZirconHandleFUCHSIA.html)) · Function <br/> vkGetSemaphoreZirconHandleFUCHSIA - Get a Zircon event handle for a semaphore\n[](#_c_specification)C Specification\n----------\n\nTo export a Zircon event handle representing the payload of a semaphore,\ncall:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\nVkResult vkGetSemaphoreZirconHandleFUCHSIA(\n    VkDevice                                    device,\n    const VkSemaphoreGetZirconHandleInfoFUCHSIA* pGetZirconHandleInfo,\n    zx_handle_t*                                pZirconHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore being\n  exported.\n\n* [`Self::p_get_zircon_handle_info`] is a pointer to a[`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure containing\n  parameters of the export operation.\n\n* [`Self::p_zircon_handle`] will return the Zircon event handle representing the\n  semaphore payload.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_semaphore_zircon_handle_fuchsia`] **must** create a Zircon\nevent handle and transfer ownership of it to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nZircon event handle when it is no longer needed.\n\n|   |Note<br/><br/>Ownership can be released in many ways.<br/>For example, the application can call zx\\_handle\\_close() on the file<br/>descriptor, or transfer ownership back to Vulkan by using the file<br/>descriptor to import a semaphore payload.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nExporting a Zircon event handle from a semaphore **may** have side effects\ndepending on the transference of the specified handle type, as described in[Importing Semaphore State](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-semaphores-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-pGetZirconHandleInfo-parameter  \n  [`Self::p_get_zircon_handle_info`] **must** be a valid pointer to a valid [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`] structure\n\n* []() VUID-vkGetSemaphoreZirconHandleFUCHSIA-pZirconHandle-parameter  \n  [`Self::p_zircon_handle`] **must** be a valid pointer to a `zx_handle_t` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SemaphoreGetZirconHandleInfoFUCHSIA`]\n"]
    #[doc(alias = "vkGetSemaphoreZirconHandleFUCHSIA")]
    pub unsafe fn get_semaphore_zircon_handle_fuchsia(&self, get_zircon_handle_info: &crate::extensions::fuchsia_external_semaphore::SemaphoreGetZirconHandleInfoFUCHSIA, zircon_handle: *mut *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_semaphore_zircon_handle_fuchsia.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, get_zircon_handle_info as _, zircon_handle);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportSemaphoreZirconHandleFUCHSIA.html)) · Function <br/> vkImportSemaphoreZirconHandleFUCHSIA - Import a semaphore from a Zircon event handle\n[](#_c_specification)C Specification\n----------\n\nTo import a semaphore payload from a Zircon event handle, call:\n\n```\n// Provided by VK_FUCHSIA_external_semaphore\nVkResult vkImportSemaphoreZirconHandleFUCHSIA(\n    VkDevice                                    device,\n    const VkImportSemaphoreZirconHandleInfoFUCHSIA* pImportSemaphoreZirconHandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the semaphore.\n\n* [`Self::p_import_semaphore_zircon_handle_info`] is a pointer to a[`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure specifying the\n  semaphore and import parameters.\n[](#_description)Description\n----------\n\nImporting a semaphore payload from a Zircon event handle transfers ownership\nof the handle from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the handle after a\nsuccessful import.\n\nApplications **can** import the same semaphore payload into multiple instances\nof Vulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-semaphore-04764  \n  `semaphore` **must** not be associated with any queue command that has\n  not yet completed execution on that queue.\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportSemaphoreZirconHandleFUCHSIA-pImportSemaphoreZirconHandleInfo-parameter  \n  [`Self::p_import_semaphore_zircon_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportSemaphoreZirconHandleInfoFUCHSIA`]\n"]
    #[doc(alias = "vkImportSemaphoreZirconHandleFUCHSIA")]
    pub unsafe fn import_semaphore_zircon_handle_fuchsia(&self, import_semaphore_zircon_handle_info: &crate::extensions::fuchsia_external_semaphore::ImportSemaphoreZirconHandleInfoFUCHSIA) -> crate::utils::VulkanResult<()> {
        let _function = self.import_semaphore_zircon_handle_fuchsia.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, import_semaphore_zircon_handle_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
