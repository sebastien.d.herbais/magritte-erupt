#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADER_IMAGE_FOOTPRINT_SPEC_VERSION")]
pub const NV_SHADER_IMAGE_FOOTPRINT_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADER_IMAGE_FOOTPRINT_EXTENSION_NAME")]
pub const NV_SHADER_IMAGE_FOOTPRINT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_shader_image_footprint");
#[doc = "Provided by [`crate::extensions::nv_shader_image_footprint`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV: Self = Self(1000204000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderImageFootprintFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderImageFootprintFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderImageFootprintFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceShaderImageFootprintFeaturesNV - Structure describing shader image footprint features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shader_image_footprint\ntypedef struct VkPhysicalDeviceShaderImageFootprintFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           imageFootprint;\n} VkPhysicalDeviceShaderImageFootprintFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::image_footprint`] specifies whether the\n  implementation supports the `ImageFootprintNV` SPIR-V capability.\n\nSee [Texel Footprint Evaluation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#textures-footprint) for more information.\n\nIf the [`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderImageFootprintFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShaderImageFootprintFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShaderImageFootprintFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub image_footprint: crate::vk1_0::Bool32,
}
impl PhysicalDeviceShaderImageFootprintFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV;
}
impl Default for PhysicalDeviceShaderImageFootprintFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), image_footprint: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShaderImageFootprintFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShaderImageFootprintFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("image_footprint", &(self.image_footprint != 0)).finish()
    }
}
impl PhysicalDeviceShaderImageFootprintFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
        PhysicalDeviceShaderImageFootprintFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderImageFootprintFeaturesNV.html)) · Builder of [`PhysicalDeviceShaderImageFootprintFeaturesNV`] <br/> VkPhysicalDeviceShaderImageFootprintFeaturesNV - Structure describing shader image footprint features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shader_image_footprint\ntypedef struct VkPhysicalDeviceShaderImageFootprintFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           imageFootprint;\n} VkPhysicalDeviceShaderImageFootprintFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::image_footprint`] specifies whether the\n  implementation supports the `ImageFootprintNV` SPIR-V capability.\n\nSee [Texel Footprint Evaluation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#textures-footprint) for more information.\n\nIf the [`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderImageFootprintFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderImageFootprintFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a>(PhysicalDeviceShaderImageFootprintFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
        PhysicalDeviceShaderImageFootprintFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image_footprint(mut self, image_footprint: bool) -> Self {
        self.0.image_footprint = image_footprint as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShaderImageFootprintFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceShaderImageFootprintFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShaderImageFootprintFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
