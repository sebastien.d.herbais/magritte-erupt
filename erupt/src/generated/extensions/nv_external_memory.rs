#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_SPEC_VERSION")]
pub const NV_EXTERNAL_MEMORY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_EXTENSION_NAME")]
pub const NV_EXTERNAL_MEMORY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_external_memory");
#[doc = "Provided by [`crate::extensions::nv_external_memory`]"]
impl crate::vk1_0::StructureType {
    pub const EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV: Self = Self(1000056000);
    pub const EXPORT_MEMORY_ALLOCATE_INFO_NV: Self = Self(1000056001);
}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryAllocateInfoNV> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryAllocateInfoNVBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalMemoryImageCreateInfoNV> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalMemoryImageCreateInfoNVBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryImageCreateInfoNV.html)) · Structure <br/> VkExternalMemoryImageCreateInfoNV - Specify that an image may be backed by external memory\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::ExternalMemoryImageCreateInfoNV`]structure, then that structure defines a set of external memory handle types\nthat **may** be used as backing store for the image.\n\nThe [`crate::vk::ExternalMemoryImageCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory\ntypedef struct VkExternalMemoryImageCreateInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleTypes;\n} VkExternalMemoryImageCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_types`] is zero, or a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) specifying one or more\n  external memory handle types.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkExternalMemoryImageCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV`]\n\n* []() VUID-VkExternalMemoryImageCreateInfoNV-handleTypes-parameter  \n  [`Self::handle_types`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExternalMemoryImageCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExternalMemoryImageCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV,
}
impl ExternalMemoryImageCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV;
}
impl Default for ExternalMemoryImageCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_types: Default::default() }
    }
}
impl std::fmt::Debug for ExternalMemoryImageCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExternalMemoryImageCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_types", &self.handle_types).finish()
    }
}
impl ExternalMemoryImageCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ExternalMemoryImageCreateInfoNVBuilder<'a> {
        ExternalMemoryImageCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryImageCreateInfoNV.html)) · Builder of [`ExternalMemoryImageCreateInfoNV`] <br/> VkExternalMemoryImageCreateInfoNV - Specify that an image may be backed by external memory\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a [`crate::vk::ExternalMemoryImageCreateInfoNV`]structure, then that structure defines a set of external memory handle types\nthat **may** be used as backing store for the image.\n\nThe [`crate::vk::ExternalMemoryImageCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory\ntypedef struct VkExternalMemoryImageCreateInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleTypes;\n} VkExternalMemoryImageCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_types`] is zero, or a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) specifying one or more\n  external memory handle types.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkExternalMemoryImageCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV`]\n\n* []() VUID-VkExternalMemoryImageCreateInfoNV-handleTypes-parameter  \n  [`Self::handle_types`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExternalMemoryImageCreateInfoNVBuilder<'a>(ExternalMemoryImageCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> ExternalMemoryImageCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> ExternalMemoryImageCreateInfoNVBuilder<'a> {
        ExternalMemoryImageCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_types(mut self, handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV) -> Self {
        self.0.handle_types = handle_types as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExternalMemoryImageCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for ExternalMemoryImageCreateInfoNVBuilder<'a> {
    fn default() -> ExternalMemoryImageCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExternalMemoryImageCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExternalMemoryImageCreateInfoNVBuilder<'a> {
    type Target = ExternalMemoryImageCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExternalMemoryImageCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryAllocateInfoNV.html)) · Structure <br/> VkExportMemoryAllocateInfoNV - Specify memory handle types that may be exported\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ExportMemoryAllocateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory\ntypedef struct VkExportMemoryAllocateInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleTypes;\n} VkExportMemoryAllocateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) specifying one or more memory\n  handle types that **may** be exported.\n  Multiple handle types **may** be requested for the same allocation as long\n  as they are compatible, as reported by[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryAllocateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_ALLOCATE_INFO_NV`]\n\n* []() VUID-VkExportMemoryAllocateInfoNV-handleTypes-parameter  \n  [`Self::handle_types`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExportMemoryAllocateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExportMemoryAllocateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV,
}
impl ExportMemoryAllocateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXPORT_MEMORY_ALLOCATE_INFO_NV;
}
impl Default for ExportMemoryAllocateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_types: Default::default() }
    }
}
impl std::fmt::Debug for ExportMemoryAllocateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExportMemoryAllocateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_types", &self.handle_types).finish()
    }
}
impl ExportMemoryAllocateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ExportMemoryAllocateInfoNVBuilder<'a> {
        ExportMemoryAllocateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryAllocateInfoNV.html)) · Builder of [`ExportMemoryAllocateInfoNV`] <br/> VkExportMemoryAllocateInfoNV - Specify memory handle types that may be exported\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ExportMemoryAllocateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory\ntypedef struct VkExportMemoryAllocateInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleTypes;\n} VkExportMemoryAllocateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) specifying one or more memory\n  handle types that **may** be exported.\n  Multiple handle types **may** be requested for the same allocation as long\n  as they are compatible, as reported by[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryAllocateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_ALLOCATE_INFO_NV`]\n\n* []() VUID-VkExportMemoryAllocateInfoNV-handleTypes-parameter  \n  [`Self::handle_types`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExportMemoryAllocateInfoNVBuilder<'a>(ExportMemoryAllocateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> ExportMemoryAllocateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> ExportMemoryAllocateInfoNVBuilder<'a> {
        ExportMemoryAllocateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_types(mut self, handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV) -> Self {
        self.0.handle_types = handle_types as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExportMemoryAllocateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for ExportMemoryAllocateInfoNVBuilder<'a> {
    fn default() -> ExportMemoryAllocateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExportMemoryAllocateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExportMemoryAllocateInfoNVBuilder<'a> {
    type Target = ExportMemoryAllocateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExportMemoryAllocateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
