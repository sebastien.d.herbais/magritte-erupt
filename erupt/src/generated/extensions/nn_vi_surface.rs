#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NN_VI_SURFACE_SPEC_VERSION")]
pub const NN_VI_SURFACE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NN_VI_SURFACE_EXTENSION_NAME")]
pub const NN_VI_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NN_vi_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_VI_SURFACE_NN: *const std::os::raw::c_char = crate::cstr!("vkCreateViSurfaceNN");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViSurfaceCreateFlagsNN.html)) · Bitmask of [`ViSurfaceCreateFlagBitsNN`] <br/> VkViSurfaceCreateFlagsNN - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NN_vi_surface\ntypedef VkFlags VkViSurfaceCreateFlagsNN;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ViSurfaceCreateFlagBitsNN`] is a bitmask type for setting a mask, but is\ncurrently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ViSurfaceCreateInfoNN`]\n"] # [doc (alias = "VkViSurfaceCreateFlagsNN")] # [derive (Default)] # [repr (transparent)] pub struct ViSurfaceCreateFlagsNN : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`ViSurfaceCreateFlagsNN`] <br/> "]
#[doc(alias = "VkViSurfaceCreateFlagBitsNN")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ViSurfaceCreateFlagBitsNN(pub u32);
impl ViSurfaceCreateFlagBitsNN {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ViSurfaceCreateFlagsNN {
        ViSurfaceCreateFlagsNN::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ViSurfaceCreateFlagBitsNN {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nn_vi_surface`]"]
impl crate::vk1_0::StructureType {
    pub const VI_SURFACE_CREATE_INFO_NN: Self = Self(1000062000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateViSurfaceNN.html)) · Function <br/> vkCreateViSurfaceNN - Create a slink:VkSurfaceKHR object for a VI layer\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an `nn`::`vi`::`Layer`,\nquery the layer’s native handle using`nn`::`vi`::`GetNativeWindow`, and then call:\n\n```\n// Provided by VK_NN_vi_surface\nVkResult vkCreateViSurfaceNN(\n    VkInstance                                  instance,\n    const VkViSurfaceCreateInfoNN*              pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance with which to associate the surface.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::ViSurfaceCreateInfoNN`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nDuring the lifetime of a surface created using a particular`nn`::`vi`::`NativeWindowHandle`, applications **must** not attempt to\ncreate another surface for the same `nn`::`vi`::`Layer` or attempt\nto connect to the same `nn`::`vi`::`Layer` through other platform\nmechanisms.\n\nIf the native window is created with a specified size, `currentExtent`will reflect that size.\nIn this case, applications should use the same size for the swapchain’s`imageExtent`.\nOtherwise, the `currentExtent` will have the special value(0xFFFFFFFF, 0xFFFFFFFF), indicating that applications are expected to\nchoose an appropriate size for the swapchain’s `imageExtent` (e.g., by\nmatching the result of a call to`nn`::`vi`::`GetDisplayResolution`).\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateViSurfaceNN-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateViSurfaceNN-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::ViSurfaceCreateInfoNN`] structure\n\n* []() VUID-vkCreateViSurfaceNN-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateViSurfaceNN-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::ViSurfaceCreateInfoNN`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateViSurfaceNN = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::nn_vi_surface::ViSurfaceCreateInfoNN, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViSurfaceCreateInfoNN.html)) · Structure <br/> VkViSurfaceCreateInfoNN - Structure specifying parameters of a newly created VI surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ViSurfaceCreateInfoNN`] structure is defined as:\n\n```\n// Provided by VK_NN_vi_surface\ntypedef struct VkViSurfaceCreateInfoNN {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkViSurfaceCreateFlagsNN    flags;\n    void*                       window;\n} VkViSurfaceCreateInfoNN;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::window`] is the `nn`::`vi`::`NativeWindowHandle` for the`nn`::`vi`::`Layer` with which to associate the surface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkViSurfaceCreateInfoNN-window-01318  \n  [`Self::window`] **must** be a valid `nn`::`vi`::`NativeWindowHandle`\n\nValid Usage (Implicit)\n\n* []() VUID-VkViSurfaceCreateInfoNN-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VI_SURFACE_CREATE_INFO_NN`]\n\n* []() VUID-VkViSurfaceCreateInfoNN-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkViSurfaceCreateInfoNN-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ViSurfaceCreateFlagBitsNN`], [`crate::vk::InstanceLoader::create_vi_surface_nn`]\n"]
#[doc(alias = "VkViSurfaceCreateInfoNN")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ViSurfaceCreateInfoNN {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::nn_vi_surface::ViSurfaceCreateFlagsNN,
    pub window: *mut std::ffi::c_void,
}
impl ViSurfaceCreateInfoNN {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VI_SURFACE_CREATE_INFO_NN;
}
impl Default for ViSurfaceCreateInfoNN {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), window: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ViSurfaceCreateInfoNN {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ViSurfaceCreateInfoNN").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("window", &self.window).finish()
    }
}
impl ViSurfaceCreateInfoNN {
    #[inline]
    pub fn into_builder<'a>(self) -> ViSurfaceCreateInfoNNBuilder<'a> {
        ViSurfaceCreateInfoNNBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkViSurfaceCreateInfoNN.html)) · Builder of [`ViSurfaceCreateInfoNN`] <br/> VkViSurfaceCreateInfoNN - Structure specifying parameters of a newly created VI surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ViSurfaceCreateInfoNN`] structure is defined as:\n\n```\n// Provided by VK_NN_vi_surface\ntypedef struct VkViSurfaceCreateInfoNN {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkViSurfaceCreateFlagsNN    flags;\n    void*                       window;\n} VkViSurfaceCreateInfoNN;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::window`] is the `nn`::`vi`::`NativeWindowHandle` for the`nn`::`vi`::`Layer` with which to associate the surface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkViSurfaceCreateInfoNN-window-01318  \n  [`Self::window`] **must** be a valid `nn`::`vi`::`NativeWindowHandle`\n\nValid Usage (Implicit)\n\n* []() VUID-VkViSurfaceCreateInfoNN-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VI_SURFACE_CREATE_INFO_NN`]\n\n* []() VUID-VkViSurfaceCreateInfoNN-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkViSurfaceCreateInfoNN-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ViSurfaceCreateFlagBitsNN`], [`crate::vk::InstanceLoader::create_vi_surface_nn`]\n"]
#[repr(transparent)]
pub struct ViSurfaceCreateInfoNNBuilder<'a>(ViSurfaceCreateInfoNN, std::marker::PhantomData<&'a ()>);
impl<'a> ViSurfaceCreateInfoNNBuilder<'a> {
    #[inline]
    pub fn new() -> ViSurfaceCreateInfoNNBuilder<'a> {
        ViSurfaceCreateInfoNNBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nn_vi_surface::ViSurfaceCreateFlagsNN) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn window(mut self, window: *mut std::ffi::c_void) -> Self {
        self.0.window = window;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ViSurfaceCreateInfoNN {
        self.0
    }
}
impl<'a> std::default::Default for ViSurfaceCreateInfoNNBuilder<'a> {
    fn default() -> ViSurfaceCreateInfoNNBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ViSurfaceCreateInfoNNBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ViSurfaceCreateInfoNNBuilder<'a> {
    type Target = ViSurfaceCreateInfoNN;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ViSurfaceCreateInfoNNBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nn_vi_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateViSurfaceNN.html)) · Function <br/> vkCreateViSurfaceNN - Create a slink:VkSurfaceKHR object for a VI layer\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an `nn`::`vi`::`Layer`,\nquery the layer’s native handle using`nn`::`vi`::`GetNativeWindow`, and then call:\n\n```\n// Provided by VK_NN_vi_surface\nVkResult vkCreateViSurfaceNN(\n    VkInstance                                  instance,\n    const VkViSurfaceCreateInfoNN*              pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance with which to associate the surface.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::ViSurfaceCreateInfoNN`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nDuring the lifetime of a surface created using a particular`nn`::`vi`::`NativeWindowHandle`, applications **must** not attempt to\ncreate another surface for the same `nn`::`vi`::`Layer` or attempt\nto connect to the same `nn`::`vi`::`Layer` through other platform\nmechanisms.\n\nIf the native window is created with a specified size, `currentExtent`will reflect that size.\nIn this case, applications should use the same size for the swapchain’s`imageExtent`.\nOtherwise, the `currentExtent` will have the special value(0xFFFFFFFF, 0xFFFFFFFF), indicating that applications are expected to\nchoose an appropriate size for the swapchain’s `imageExtent` (e.g., by\nmatching the result of a call to`nn`::`vi`::`GetDisplayResolution`).\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateViSurfaceNN-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateViSurfaceNN-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::ViSurfaceCreateInfoNN`] structure\n\n* []() VUID-vkCreateViSurfaceNN-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateViSurfaceNN-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::ViSurfaceCreateInfoNN`]\n"]
    #[doc(alias = "vkCreateViSurfaceNN")]
    pub unsafe fn create_vi_surface_nn(&self, create_info: &crate::extensions::nn_vi_surface::ViSurfaceCreateInfoNN, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_vi_surface_nn.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
