#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_MAX_GLOBAL_PRIORITY_SIZE_EXT")]
pub const MAX_GLOBAL_PRIORITY_SIZE_EXT: u32 = 16;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_GLOBAL_PRIORITY_QUERY_SPEC_VERSION")]
pub const EXT_GLOBAL_PRIORITY_QUERY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_GLOBAL_PRIORITY_QUERY_EXTENSION_NAME")]
pub const EXT_GLOBAL_PRIORITY_QUERY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_global_priority_query");
#[doc = "Provided by [`crate::extensions::ext_global_priority_query`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT: Self = Self(1000388000);
    pub const QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT: Self = Self(1000388001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceGlobalPriorityQueryFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceGlobalPriorityQueryFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, QueueFamilyGlobalPriorityPropertiesEXT> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, QueueFamilyGlobalPriorityPropertiesEXTBuilder<'_>> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT - Structure describing whether global priority query can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_global_priority_query\ntypedef struct VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           globalPriorityQuery;\n} VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThe members of the [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`]structure describe the following features:\n[](#_description)Description\n----------\n\n* []() [`Self::global_priority_query`] indicates\n  whether the implementation supports the ability to query global queue\n  priorities.\n\nIf the [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub global_priority_query: crate::vk1_0::Bool32,
}
impl PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT;
}
impl Default for PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), global_priority_query: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceGlobalPriorityQueryFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("global_priority_query", &(self.global_priority_query != 0)).finish()
    }
}
impl PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
        PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT.html)) · Builder of [`PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] <br/> VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT - Structure describing whether global priority query can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_global_priority_query\ntypedef struct VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           globalPriorityQuery;\n} VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThe members of the [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`]structure describe the following features:\n[](#_description)Description\n----------\n\n* []() [`Self::global_priority_query`] indicates\n  whether the implementation supports the ability to query global queue\n  priorities.\n\nIf the [`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceGlobalPriorityQueryFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a>(PhysicalDeviceGlobalPriorityQueryFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
        PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn global_priority_query(mut self, global_priority_query: bool) -> Self {
        self.0.global_priority_query = global_priority_query as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceGlobalPriorityQueryFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceGlobalPriorityQueryFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyGlobalPriorityPropertiesEXT.html)) · Structure <br/> VkQueueFamilyGlobalPriorityPropertiesEXT - Return structure for queue family global priority information query\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::QueueFamilyGlobalPriorityPropertiesEXT`] is:\n\n```\n// Provided by VK_EXT_global_priority_query\ntypedef struct VkQueueFamilyGlobalPriorityPropertiesEXT {\n    VkStructureType             sType;\n    void*                       pNext;\n    uint32_t                    priorityCount;\n    VkQueueGlobalPriorityEXT    priorities[VK_MAX_GLOBAL_PRIORITY_SIZE_EXT];\n} VkQueueFamilyGlobalPriorityPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::priority_count`] is the number of supported global queue priorities\n  in this queue family, and it **must** be greater than 0.\n\n* [`Self::priorities`] is an array of [`crate::vk::MAX_GLOBAL_PRIORITY_SIZE_EXT`][`crate::vk::QueueGlobalPriorityEXT`] enums representing all supported global\n  queue priorities in this queue family.\n  The first [`Self::priority_count`] elements of the array will be valid.\n[](#_description)Description\n----------\n\nThe valid elements of [`Self::priorities`] **must** not contain any duplicate\nvalues.\n\nThe valid elements of [`Self::priorities`] **must** be a continuous sequence of[`crate::vk::QueueGlobalPriorityEXT`] enums in the ascending order.\n\n|   |Note<br/><br/>For example, returning [`Self::priority_count`] as 3 with supported[`Self::priorities`] as [`crate::vk::QueueGlobalPriorityEXT::LOW_EXT`],[`crate::vk::QueueGlobalPriorityEXT::MEDIUM_EXT`] and[`crate::vk::QueueGlobalPriorityEXT::REALTIME_EXT`] is not allowed.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueueFamilyGlobalPriorityPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT`]\n\n* []() VUID-VkQueueFamilyGlobalPriorityPropertiesEXT-priorities-parameter  \n   Any given element of [`Self::priorities`] **must** be a valid [`crate::vk::QueueGlobalPriorityEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueueGlobalPriorityEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkQueueFamilyGlobalPriorityPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct QueueFamilyGlobalPriorityPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub priority_count: u32,
    pub priorities: [crate::extensions::ext_global_priority::QueueGlobalPriorityEXT; 16],
}
impl QueueFamilyGlobalPriorityPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT;
}
impl Default for QueueFamilyGlobalPriorityPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), priority_count: Default::default(), priorities: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for QueueFamilyGlobalPriorityPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("QueueFamilyGlobalPriorityPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("priority_count", &self.priority_count).field("priorities", &self.priorities).finish()
    }
}
impl QueueFamilyGlobalPriorityPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
        QueueFamilyGlobalPriorityPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyGlobalPriorityPropertiesEXT.html)) · Builder of [`QueueFamilyGlobalPriorityPropertiesEXT`] <br/> VkQueueFamilyGlobalPriorityPropertiesEXT - Return structure for queue family global priority information query\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::QueueFamilyGlobalPriorityPropertiesEXT`] is:\n\n```\n// Provided by VK_EXT_global_priority_query\ntypedef struct VkQueueFamilyGlobalPriorityPropertiesEXT {\n    VkStructureType             sType;\n    void*                       pNext;\n    uint32_t                    priorityCount;\n    VkQueueGlobalPriorityEXT    priorities[VK_MAX_GLOBAL_PRIORITY_SIZE_EXT];\n} VkQueueFamilyGlobalPriorityPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::priority_count`] is the number of supported global queue priorities\n  in this queue family, and it **must** be greater than 0.\n\n* [`Self::priorities`] is an array of [`crate::vk::MAX_GLOBAL_PRIORITY_SIZE_EXT`][`crate::vk::QueueGlobalPriorityEXT`] enums representing all supported global\n  queue priorities in this queue family.\n  The first [`Self::priority_count`] elements of the array will be valid.\n[](#_description)Description\n----------\n\nThe valid elements of [`Self::priorities`] **must** not contain any duplicate\nvalues.\n\nThe valid elements of [`Self::priorities`] **must** be a continuous sequence of[`crate::vk::QueueGlobalPriorityEXT`] enums in the ascending order.\n\n|   |Note<br/><br/>For example, returning [`Self::priority_count`] as 3 with supported[`Self::priorities`] as [`crate::vk::QueueGlobalPriorityEXT::LOW_EXT`],[`crate::vk::QueueGlobalPriorityEXT::MEDIUM_EXT`] and[`crate::vk::QueueGlobalPriorityEXT::REALTIME_EXT`] is not allowed.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueueFamilyGlobalPriorityPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT`]\n\n* []() VUID-VkQueueFamilyGlobalPriorityPropertiesEXT-priorities-parameter  \n   Any given element of [`Self::priorities`] **must** be a valid [`crate::vk::QueueGlobalPriorityEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueueGlobalPriorityEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a>(QueueFamilyGlobalPriorityPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
        QueueFamilyGlobalPriorityPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn priorities(mut self, priorities: &'a [crate::extensions::ext_global_priority::QueueGlobalPriorityEXT]) -> Self {
        let mut priorities_array = [Default::default(); 16];
        let truncated_len = priorities.len().min(priorities_array.len());
        priorities_array[..truncated_len].copy_from_slice(&priorities[..truncated_len]);
        self.0.priority_count = truncated_len as _;
        self.0.priorities = priorities_array;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> QueueFamilyGlobalPriorityPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
    fn default() -> QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
    type Target = QueueFamilyGlobalPriorityPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for QueueFamilyGlobalPriorityPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
