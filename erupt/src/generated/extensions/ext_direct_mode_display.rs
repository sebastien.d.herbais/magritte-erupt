#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DIRECT_MODE_DISPLAY_SPEC_VERSION")]
pub const EXT_DIRECT_MODE_DISPLAY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME")]
pub const EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_direct_mode_display");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_RELEASE_DISPLAY_EXT: *const std::os::raw::c_char = crate::cstr!("vkReleaseDisplayEXT");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseDisplayEXT.html)) · Function <br/> vkReleaseDisplayEXT - Release access to an acquired VkDisplayKHR\n[](#_c_specification)C Specification\n----------\n\nTo release a previously acquired display, call:\n\n```\n// Provided by VK_EXT_direct_mode_display\nVkResult vkReleaseDisplayEXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] The physical device the display is on.\n\n* [`Self::display`] The display to release control of.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleaseDisplayEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkReleaseDisplayEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkReleaseDisplayEXT-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkReleaseDisplayEXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR) -> crate::vk1_0::Result;
#[doc = "Provided by [`crate::extensions::ext_direct_mode_display`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleaseDisplayEXT.html)) · Function <br/> vkReleaseDisplayEXT - Release access to an acquired VkDisplayKHR\n[](#_c_specification)C Specification\n----------\n\nTo release a previously acquired display, call:\n\n```\n// Provided by VK_EXT_direct_mode_display\nVkResult vkReleaseDisplayEXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkDisplayKHR                                display);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] The physical device the display is on.\n\n* [`Self::display`] The display to release control of.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleaseDisplayEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkReleaseDisplayEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkReleaseDisplayEXT-display-parent  \n  [`Self::display`] **must** have been created, allocated, or retrieved from [`Self::physical_device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayKHR`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkReleaseDisplayEXT")]
    pub unsafe fn release_display_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, display: crate::extensions::khr_display::DisplayKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.release_display_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, display as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
