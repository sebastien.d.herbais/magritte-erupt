#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SCISSOR_EXCLUSIVE_SPEC_VERSION")]
pub const NV_SCISSOR_EXCLUSIVE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SCISSOR_EXCLUSIVE_EXTENSION_NAME")]
pub const NV_SCISSOR_EXCLUSIVE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_scissor_exclusive");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_EXCLUSIVE_SCISSOR_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdSetExclusiveScissorNV");
#[doc = "Provided by [`crate::extensions::nv_scissor_exclusive`]"]
impl crate::vk1_0::DynamicState {
    pub const EXCLUSIVE_SCISSOR_NV: Self = Self(1000205001);
}
#[doc = "Provided by [`crate::extensions::nv_scissor_exclusive`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV: Self = Self(1000205000);
    pub const PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV: Self = Self(1000205002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetExclusiveScissorNV.html)) · Function <br/> vkCmdSetExclusiveScissorNV - Set the dynamic exclusive scissor rectangles on a command buffer\n[](#_c_specification)C Specification\n----------\n\nThe exclusive scissor rectangles **can** be set dynamically with the command:\n\n```\n// Provided by VK_NV_scissor_exclusive\nvoid vkCmdSetExclusiveScissorNV(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstExclusiveScissor,\n    uint32_t                                    exclusiveScissorCount,\n    const VkRect2D*                             pExclusiveScissors);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_exclusive_scissor`] is the index of the first exclusive scissor\n  rectangle whose state is updated by the command.\n\n* [`Self::exclusive_scissor_count`] is the number of exclusive scissor\n  rectangles updated by the command.\n\n* [`Self::p_exclusive_scissors`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining exclusive scissor rectangles.\n[](#_description)Description\n----------\n\nThe scissor rectangles taken from element i of[`Self::p_exclusive_scissors`] replace the current state for the scissor index[`Self::first_exclusive_scissor`] + i, for i in [0,[`Self::exclusive_scissor_count`]).\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::EXCLUSIVE_SCISSOR_NV`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetExclusiveScissorNV-None-02031  \n   The [exclusive scissor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-exclusiveScissor) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetExclusiveScissorNV-firstExclusiveScissor-02034  \n   The sum of [`Self::first_exclusive_scissor`] and [`Self::exclusive_scissor_count`]**must** be between `1` and[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`, inclusive\n\n* []() VUID-vkCmdSetExclusiveScissorNV-firstExclusiveScissor-02035  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::first_exclusive_scissor`] **must** be `0`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-exclusiveScissorCount-02036  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::exclusive_scissor_count`] **must** be `1`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-x-02037  \n   The `x` and `y` members of `offset` in each member of[`Self::p_exclusive_scissors`] **must** be greater than or equal to `0`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-offset-02038  \n   Evaluation of (`offset.x` + `extent.width`) for each\n  member of [`Self::p_exclusive_scissors`] **must** not cause a signed integer\n  addition overflow\n\n* []() VUID-vkCmdSetExclusiveScissorNV-offset-02039  \n   Evaluation of (`offset.y` + `extent.height`) for each\n  member of [`Self::p_exclusive_scissors`] **must** not cause a signed integer\n  addition overflow\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetExclusiveScissorNV-pExclusiveScissors-parameter  \n  [`Self::p_exclusive_scissors`] **must** be a valid pointer to an array of [`Self::exclusive_scissor_count`] [`crate::vk::Rect2D`] structures\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetExclusiveScissorNV-exclusiveScissorCount-arraylength  \n  [`Self::exclusive_scissor_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Rect2D`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetExclusiveScissorNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, first_exclusive_scissor: u32, exclusive_scissor_count: u32, p_exclusive_scissors: *const crate::vk1_0::Rect2D) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceExclusiveScissorFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportExclusiveScissorStateCreateInfoNV> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExclusiveScissorFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExclusiveScissorFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceExclusiveScissorFeaturesNV - Structure describing exclusive scissor features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_scissor_exclusive\ntypedef struct VkPhysicalDeviceExclusiveScissorFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           exclusiveScissor;\n} VkPhysicalDeviceExclusiveScissorFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::exclusive_scissor`] indicates that the\n  implementation supports the exclusive scissor test.\n\nSee [Exclusive Scissor Test](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-exclusive-scissor) for more\ninformation.\n\nIf the [`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExclusiveScissorFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceExclusiveScissorFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceExclusiveScissorFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub exclusive_scissor: crate::vk1_0::Bool32,
}
impl PhysicalDeviceExclusiveScissorFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV;
}
impl Default for PhysicalDeviceExclusiveScissorFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), exclusive_scissor: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceExclusiveScissorFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceExclusiveScissorFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("exclusive_scissor", &(self.exclusive_scissor != 0)).finish()
    }
}
impl PhysicalDeviceExclusiveScissorFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
        PhysicalDeviceExclusiveScissorFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExclusiveScissorFeaturesNV.html)) · Builder of [`PhysicalDeviceExclusiveScissorFeaturesNV`] <br/> VkPhysicalDeviceExclusiveScissorFeaturesNV - Structure describing exclusive scissor features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_scissor_exclusive\ntypedef struct VkPhysicalDeviceExclusiveScissorFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           exclusiveScissor;\n} VkPhysicalDeviceExclusiveScissorFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::exclusive_scissor`] indicates that the\n  implementation supports the exclusive scissor test.\n\nSee [Exclusive Scissor Test](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-exclusive-scissor) for more\ninformation.\n\nIf the [`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceExclusiveScissorFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExclusiveScissorFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a>(PhysicalDeviceExclusiveScissorFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
        PhysicalDeviceExclusiveScissorFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn exclusive_scissor(mut self, exclusive_scissor: bool) -> Self {
        self.0.exclusive_scissor = exclusive_scissor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceExclusiveScissorFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceExclusiveScissorFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceExclusiveScissorFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportExclusiveScissorStateCreateInfoNV.html)) · Structure <br/> VkPipelineViewportExclusiveScissorStateCreateInfoNV - Structure specifying parameters controlling exclusive scissor testing\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_scissor_exclusive\ntypedef struct VkPipelineViewportExclusiveScissorStateCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           exclusiveScissorCount;\n    const VkRect2D*    pExclusiveScissors;\n} VkPipelineViewportExclusiveScissorStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::exclusive_scissor_count`] is the number of exclusive scissor\n  rectangles.\n\n* [`Self::p_exclusive_scissors`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining exclusive scissor rectangles.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::DynamicState::EXCLUSIVE_SCISSOR_NV`] dynamic state is enabled\nfor a pipeline, the [`Self::p_exclusive_scissors`] member is ignored.\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::GraphicsPipelineCreateInfo`], it defines parameters of the exclusive\nscissor test.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with a [`Self::exclusive_scissor_count`] of `0`.\n\nValid Usage\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02027  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::exclusive_scissor_count`] **must** be `0` or `1`\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02028  \n  [`Self::exclusive_scissor_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02029  \n  [`Self::exclusive_scissor_count`] **must** be `0` or greater than or equal to the`viewportCount` member of [`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineViewportExclusiveScissorStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineViewportExclusiveScissorStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub exclusive_scissor_count: u32,
    pub p_exclusive_scissors: *const crate::vk1_0::Rect2D,
}
impl PipelineViewportExclusiveScissorStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV;
}
impl Default for PipelineViewportExclusiveScissorStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), exclusive_scissor_count: Default::default(), p_exclusive_scissors: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineViewportExclusiveScissorStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineViewportExclusiveScissorStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("exclusive_scissor_count", &self.exclusive_scissor_count).field("p_exclusive_scissors", &self.p_exclusive_scissors).finish()
    }
}
impl PipelineViewportExclusiveScissorStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
        PipelineViewportExclusiveScissorStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportExclusiveScissorStateCreateInfoNV.html)) · Builder of [`PipelineViewportExclusiveScissorStateCreateInfoNV`] <br/> VkPipelineViewportExclusiveScissorStateCreateInfoNV - Structure specifying parameters controlling exclusive scissor testing\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineViewportExclusiveScissorStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_scissor_exclusive\ntypedef struct VkPipelineViewportExclusiveScissorStateCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           exclusiveScissorCount;\n    const VkRect2D*    pExclusiveScissors;\n} VkPipelineViewportExclusiveScissorStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::exclusive_scissor_count`] is the number of exclusive scissor\n  rectangles.\n\n* [`Self::p_exclusive_scissors`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining exclusive scissor rectangles.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::DynamicState::EXCLUSIVE_SCISSOR_NV`] dynamic state is enabled\nfor a pipeline, the [`Self::p_exclusive_scissors`] member is ignored.\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::GraphicsPipelineCreateInfo`], it defines parameters of the exclusive\nscissor test.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with a [`Self::exclusive_scissor_count`] of `0`.\n\nValid Usage\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02027  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::exclusive_scissor_count`] **must** be `0` or `1`\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02028  \n  [`Self::exclusive_scissor_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-exclusiveScissorCount-02029  \n  [`Self::exclusive_scissor_count`] **must** be `0` or greater than or equal to the`viewportCount` member of [`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportExclusiveScissorStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a>(PipelineViewportExclusiveScissorStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
        PipelineViewportExclusiveScissorStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn exclusive_scissors(mut self, exclusive_scissors: &'a [crate::vk1_0::Rect2DBuilder]) -> Self {
        self.0.p_exclusive_scissors = exclusive_scissors.as_ptr() as _;
        self.0.exclusive_scissor_count = exclusive_scissors.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineViewportExclusiveScissorStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
    type Target = PipelineViewportExclusiveScissorStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineViewportExclusiveScissorStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_scissor_exclusive`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetExclusiveScissorNV.html)) · Function <br/> vkCmdSetExclusiveScissorNV - Set the dynamic exclusive scissor rectangles on a command buffer\n[](#_c_specification)C Specification\n----------\n\nThe exclusive scissor rectangles **can** be set dynamically with the command:\n\n```\n// Provided by VK_NV_scissor_exclusive\nvoid vkCmdSetExclusiveScissorNV(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstExclusiveScissor,\n    uint32_t                                    exclusiveScissorCount,\n    const VkRect2D*                             pExclusiveScissors);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_exclusive_scissor`] is the index of the first exclusive scissor\n  rectangle whose state is updated by the command.\n\n* [`Self::exclusive_scissor_count`] is the number of exclusive scissor\n  rectangles updated by the command.\n\n* [`Self::p_exclusive_scissors`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining exclusive scissor rectangles.\n[](#_description)Description\n----------\n\nThe scissor rectangles taken from element i of[`Self::p_exclusive_scissors`] replace the current state for the scissor index[`Self::first_exclusive_scissor`] + i, for i in [0,[`Self::exclusive_scissor_count`]).\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::EXCLUSIVE_SCISSOR_NV`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetExclusiveScissorNV-None-02031  \n   The [exclusive scissor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-exclusiveScissor) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetExclusiveScissorNV-firstExclusiveScissor-02034  \n   The sum of [`Self::first_exclusive_scissor`] and [`Self::exclusive_scissor_count`]**must** be between `1` and[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`, inclusive\n\n* []() VUID-vkCmdSetExclusiveScissorNV-firstExclusiveScissor-02035  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::first_exclusive_scissor`] **must** be `0`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-exclusiveScissorCount-02036  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::exclusive_scissor_count`] **must** be `1`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-x-02037  \n   The `x` and `y` members of `offset` in each member of[`Self::p_exclusive_scissors`] **must** be greater than or equal to `0`\n\n* []() VUID-vkCmdSetExclusiveScissorNV-offset-02038  \n   Evaluation of (`offset.x` + `extent.width`) for each\n  member of [`Self::p_exclusive_scissors`] **must** not cause a signed integer\n  addition overflow\n\n* []() VUID-vkCmdSetExclusiveScissorNV-offset-02039  \n   Evaluation of (`offset.y` + `extent.height`) for each\n  member of [`Self::p_exclusive_scissors`] **must** not cause a signed integer\n  addition overflow\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetExclusiveScissorNV-pExclusiveScissors-parameter  \n  [`Self::p_exclusive_scissors`] **must** be a valid pointer to an array of [`Self::exclusive_scissor_count`] [`crate::vk::Rect2D`] structures\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetExclusiveScissorNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetExclusiveScissorNV-exclusiveScissorCount-arraylength  \n  [`Self::exclusive_scissor_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Rect2D`]\n"]
    #[doc(alias = "vkCmdSetExclusiveScissorNV")]
    pub unsafe fn cmd_set_exclusive_scissor_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, first_exclusive_scissor: u32, exclusive_scissors: &[crate::vk1_0::Rect2DBuilder]) -> () {
        let _function = self.cmd_set_exclusive_scissor_nv.expect(crate::NOT_LOADED_MESSAGE);
        let exclusive_scissor_count = exclusive_scissors.len();
        let _return = _function(command_buffer as _, first_exclusive_scissor as _, exclusive_scissor_count as _, exclusive_scissors.as_ptr() as _);
        ()
    }
}
