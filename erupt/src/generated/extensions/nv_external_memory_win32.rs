#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_WIN32_SPEC_VERSION")]
pub const NV_EXTERNAL_MEMORY_WIN32_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME")]
pub const NV_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_external_memory_win32");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_WIN32_HANDLE_NV: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryWin32HandleNV");
#[doc = "Provided by [`crate::extensions::nv_external_memory_win32`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_MEMORY_WIN32_HANDLE_INFO_NV: Self = Self(1000057000);
    pub const EXPORT_MEMORY_WIN32_HANDLE_INFO_NV: Self = Self(1000057001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandleNV.html)) · Function <br/> vkGetMemoryWin32HandleNV - retrieve Win32 handle to a device memory object\n[](#_c_specification)C Specification\n----------\n\nTo retrieve the handle corresponding to a device memory object created with[`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] set to include[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_NV`] or[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_KMT_NV`], call:\n\n```\n// Provided by VK_NV_external_memory_win32\nVkResult vkGetMemoryWin32HandleNV(\n    VkDevice                                    device,\n    VkDeviceMemory                              memory,\n    VkExternalMemoryHandleTypeFlagsNV           handleType,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the memory.\n\n* [`Self::memory`] is the [`crate::vk::DeviceMemory`] object.\n\n* [`Self::handle_type`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a single bit\n  specifying the type of handle requested.\n\n* `handle` is a pointer to a Windows `HANDLE` in which the handle\n  is returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-01326  \n  [`Self::handle_type`] **must** be a flag specified in[`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] when allocating[`Self::memory`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandleNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandleNV-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-requiredbitmask  \n  [`Self::handle_type`] **must** not be `0`\n\n* []() VUID-vkGetMemoryWin32HandleNV-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\n* []() VUID-vkGetMemoryWin32HandleNV-memory-parent  \n  [`Self::memory`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DeviceMemory`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryWin32HandleNV = unsafe extern "system" fn(device: crate::vk1_0::Device, memory: crate::vk1_0::DeviceMemory, handle_type: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV, p_handle: *mut *mut std::ffi::c_void) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryWin32HandleInfoNV> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryWin32HandleInfoNVBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryWin32HandleInfoNV> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryWin32HandleInfoNVBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryWin32HandleInfoNV.html)) · Structure <br/> VkImportMemoryWin32HandleInfoNV - import Win32 memory created on the same physical device\n[](#_c_specification)C Specification\n----------\n\nTo import memory created on the same physical device but outside of the\ncurrent Vulkan instance, add a [`crate::vk::ImportMemoryWin32HandleInfoNV`]structure to the [`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`]structure, specifying a handle to and the type of the memory.\n\nThe [`crate::vk::ImportMemoryWin32HandleInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_win32\ntypedef struct VkImportMemoryWin32HandleInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleType;\n    HANDLE                               handle;\n} VkImportMemoryWin32HandleInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is `0` or a [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html)value specifying the type of memory handle in [`Self::handle`].\n\n* [`Self::handle`] is a Windows `HANDLE` referring to the memory.\n[](#_description)Description\n----------\n\nIf [`Self::handle_type`] is `0`, this structure is ignored by consumers of the[`crate::vk::MemoryAllocateInfo`] structure it is chained from.\n\nValid Usage\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handleType-01327  \n  [`Self::handle_type`] **must** not have more than one bit set\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handle-01328  \n  [`Self::handle`] **must** be a valid handle to memory, obtained as specified by[`Self::handle_type`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_NV`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImportMemoryWin32HandleInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportMemoryWin32HandleInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_type: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV,
    pub handle: *mut std::ffi::c_void,
}
impl ImportMemoryWin32HandleInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_NV;
}
impl Default for ImportMemoryWin32HandleInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_type: Default::default(), handle: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ImportMemoryWin32HandleInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportMemoryWin32HandleInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_type", &self.handle_type).field("handle", &self.handle).finish()
    }
}
impl ImportMemoryWin32HandleInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportMemoryWin32HandleInfoNVBuilder<'a> {
        ImportMemoryWin32HandleInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryWin32HandleInfoNV.html)) · Builder of [`ImportMemoryWin32HandleInfoNV`] <br/> VkImportMemoryWin32HandleInfoNV - import Win32 memory created on the same physical device\n[](#_c_specification)C Specification\n----------\n\nTo import memory created on the same physical device but outside of the\ncurrent Vulkan instance, add a [`crate::vk::ImportMemoryWin32HandleInfoNV`]structure to the [`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`]structure, specifying a handle to and the type of the memory.\n\nThe [`crate::vk::ImportMemoryWin32HandleInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_win32\ntypedef struct VkImportMemoryWin32HandleInfoNV {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkExternalMemoryHandleTypeFlagsNV    handleType;\n    HANDLE                               handle;\n} VkImportMemoryWin32HandleInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is `0` or a [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html)value specifying the type of memory handle in [`Self::handle`].\n\n* [`Self::handle`] is a Windows `HANDLE` referring to the memory.\n[](#_description)Description\n----------\n\nIf [`Self::handle_type`] is `0`, this structure is ignored by consumers of the[`crate::vk::MemoryAllocateInfo`] structure it is chained from.\n\nValid Usage\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handleType-01327  \n  [`Self::handle_type`] **must** not have more than one bit set\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handle-01328  \n  [`Self::handle`] **must** be a valid handle to memory, obtained as specified by[`Self::handle_type`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_NV`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImportMemoryWin32HandleInfoNVBuilder<'a>(ImportMemoryWin32HandleInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> ImportMemoryWin32HandleInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> ImportMemoryWin32HandleInfoNVBuilder<'a> {
        ImportMemoryWin32HandleInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn handle(mut self, handle: *mut std::ffi::c_void) -> Self {
        self.0.handle = handle;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportMemoryWin32HandleInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for ImportMemoryWin32HandleInfoNVBuilder<'a> {
    fn default() -> ImportMemoryWin32HandleInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportMemoryWin32HandleInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportMemoryWin32HandleInfoNVBuilder<'a> {
    type Target = ImportMemoryWin32HandleInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportMemoryWin32HandleInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryWin32HandleInfoNV.html)) · Structure <br/> VkExportMemoryWin32HandleInfoNV - specify security attributes and access rights for Win32 memory handles\n[](#_c_specification)C Specification\n----------\n\nWhen [`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] includes[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_NV`], add a[`crate::vk::ExportMemoryWin32HandleInfoNV`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::ExportMemoryAllocateInfoNV`] structure to specify security\nattributes and access rights for the memory object’s external handle.\n\nThe [`crate::vk::ExportMemoryWin32HandleInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_win32\ntypedef struct VkExportMemoryWin32HandleInfoNV {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n} VkExportMemoryWin32HandleInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n[](#_description)Description\n----------\n\nIf this structure is not present, or if [`Self::p_attributes`] is set to `NULL`,\ndefault security descriptor values will be used, and child processes created\nby the application will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights will be\n\n`DXGI_SHARED_RESOURCE_READ` | `DXGI_SHARED_RESOURCE_WRITE`\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryWin32HandleInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_NV`]\n\n* []() VUID-VkExportMemoryWin32HandleInfoNV-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExportMemoryWin32HandleInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExportMemoryWin32HandleInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_attributes: *const std::ffi::c_void,
    pub dw_access: u32,
}
impl ExportMemoryWin32HandleInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_NV;
}
impl Default for ExportMemoryWin32HandleInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_attributes: std::ptr::null(), dw_access: Default::default() }
    }
}
impl std::fmt::Debug for ExportMemoryWin32HandleInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExportMemoryWin32HandleInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_attributes", &self.p_attributes).field("dw_access", &self.dw_access).finish()
    }
}
impl ExportMemoryWin32HandleInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ExportMemoryWin32HandleInfoNVBuilder<'a> {
        ExportMemoryWin32HandleInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryWin32HandleInfoNV.html)) · Builder of [`ExportMemoryWin32HandleInfoNV`] <br/> VkExportMemoryWin32HandleInfoNV - specify security attributes and access rights for Win32 memory handles\n[](#_c_specification)C Specification\n----------\n\nWhen [`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] includes[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_NV`], add a[`crate::vk::ExportMemoryWin32HandleInfoNV`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::ExportMemoryAllocateInfoNV`] structure to specify security\nattributes and access rights for the memory object’s external handle.\n\nThe [`crate::vk::ExportMemoryWin32HandleInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_win32\ntypedef struct VkExportMemoryWin32HandleInfoNV {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n} VkExportMemoryWin32HandleInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n[](#_description)Description\n----------\n\nIf this structure is not present, or if [`Self::p_attributes`] is set to `NULL`,\ndefault security descriptor values will be used, and child processes created\nby the application will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights will be\n\n`DXGI_SHARED_RESOURCE_READ` | `DXGI_SHARED_RESOURCE_WRITE`\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryWin32HandleInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_NV`]\n\n* []() VUID-VkExportMemoryWin32HandleInfoNV-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExportMemoryWin32HandleInfoNVBuilder<'a>(ExportMemoryWin32HandleInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> ExportMemoryWin32HandleInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> ExportMemoryWin32HandleInfoNVBuilder<'a> {
        ExportMemoryWin32HandleInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attributes(mut self, attributes: *const std::ffi::c_void) -> Self {
        self.0.p_attributes = attributes;
        self
    }
    #[inline]
    pub fn dw_access(mut self, dw_access: u32) -> Self {
        self.0.dw_access = dw_access as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExportMemoryWin32HandleInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for ExportMemoryWin32HandleInfoNVBuilder<'a> {
    fn default() -> ExportMemoryWin32HandleInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExportMemoryWin32HandleInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExportMemoryWin32HandleInfoNVBuilder<'a> {
    type Target = ExportMemoryWin32HandleInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExportMemoryWin32HandleInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_win32`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandleNV.html)) · Function <br/> vkGetMemoryWin32HandleNV - retrieve Win32 handle to a device memory object\n[](#_c_specification)C Specification\n----------\n\nTo retrieve the handle corresponding to a device memory object created with[`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] set to include[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_NV`] or[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV::OPAQUE_WIN32_KMT_NV`], call:\n\n```\n// Provided by VK_NV_external_memory_win32\nVkResult vkGetMemoryWin32HandleNV(\n    VkDevice                                    device,\n    VkDeviceMemory                              memory,\n    VkExternalMemoryHandleTypeFlagsNV           handleType,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the memory.\n\n* [`Self::memory`] is the [`crate::vk::DeviceMemory`] object.\n\n* [`Self::handle_type`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a single bit\n  specifying the type of handle requested.\n\n* `handle` is a pointer to a Windows `HANDLE` in which the handle\n  is returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-01326  \n  [`Self::handle_type`] **must** be a flag specified in[`crate::vk::ExportMemoryAllocateInfoNV::handle_types`] when allocating[`Self::memory`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandleNV-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandleNV-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n\n* []() VUID-vkGetMemoryWin32HandleNV-handleType-requiredbitmask  \n  [`Self::handle_type`] **must** not be `0`\n\n* []() VUID-vkGetMemoryWin32HandleNV-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\n* []() VUID-vkGetMemoryWin32HandleNV-memory-parent  \n  [`Self::memory`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DeviceMemory`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`]\n"]
    #[doc(alias = "vkGetMemoryWin32HandleNV")]
    pub unsafe fn get_memory_win32_handle_nv(&self, memory: crate::vk1_0::DeviceMemory, handle_type: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV, handle: *mut *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_memory_win32_handle_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, memory as _, handle_type as _, handle);
        crate::utils::VulkanResult::new(_return, ())
    }
}
