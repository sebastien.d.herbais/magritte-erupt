#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PRESENT_WAIT_SPEC_VERSION")]
pub const KHR_PRESENT_WAIT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PRESENT_WAIT_EXTENSION_NAME")]
pub const KHR_PRESENT_WAIT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_present_wait");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_WAIT_FOR_PRESENT_KHR: *const std::os::raw::c_char = crate::cstr!("vkWaitForPresentKHR");
#[doc = "Provided by [`crate::extensions::khr_present_wait`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR: Self = Self(1000248000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkWaitForPresentKHR.html)) · Function <br/> vkWaitForPresentKHR - Wait for presentation\n[](#_c_specification)C Specification\n----------\n\nWhen the [`presentWait`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-presentWait) feature is enabled, an\napplication **can** wait for an image to be presented to the user by first\nspecifying a presentId for the target presentation by adding a[`crate::vk::PresentIdKHR`] structure to the `pNext` chain of the[`crate::vk::PresentInfoKHR`] structure and then waiting for that presentation to\ncomplete by calling:\n\n```\n// Provided by VK_KHR_present_wait\nVkResult vkWaitForPresentKHR(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    uint64_t                                    presentId,\n    uint64_t                                    timeout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the non-retired swapchain on which an image was\n  queued for presentation.\n\n* [`Self::present_id`] is the presentation presentId to wait for.\n\n* [`Self::timeout`] is the timeout period in units of nanoseconds.[`Self::timeout`] is adjusted to the closest value allowed by the\n  implementation-dependent timeout accuracy, which **may** be substantially\n  longer than one nanosecond, and **may** be longer than the requested\n  period.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::wait_for_present_khr`] waits for the presentId associated with[`Self::swapchain`] to be increased in value so that it is at least equal to[`Self::present_id`].\n\nFor [`crate::vk::PresentModeKHR::MAILBOX_KHR`] (or other present mode where images\nmay be replaced in the presentation queue) any wait of this type associated\nwith such an image **must** be signaled no later than a wait associated with\nthe replacing image would be signaled.\n\nWhen the presentation has completed, the presentId associated with the\nrelated `pSwapChains` entry will be increased in value so that it is at\nleast equal to the value provided in the [`crate::vk::PresentIdKHR`] structure.\n\nThere is no requirement for any precise timing relationship between the\npresentation of the image to the user and the update of the presentId value,\nbut implementations **should** make this as close as possible to the\npresentation of the first pixel in the new image to the user.\n\nThe call to [`crate::vk::DeviceLoader::wait_for_present_khr`] will block until either the presentId\nassociated with [`Self::swapchain`] is greater than or equal to [`Self::present_id`],\nor [`Self::timeout`] nanoseconds passes.\nWhen the swapchain becomes OUT\\_OF\\_DATE, the call will either return[`crate::vk::Result::SUCCESS`] (if the image was delivered to the presentation engine and\nmay have been presented to the user) or will return early with status[`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`] (if the image was not presented to the user).\n\nAs an exception to the normal rules for objects which are externally\nsynchronized, the [`Self::swapchain`] passed to [`crate::vk::DeviceLoader::wait_for_present_khr`] **may**be simultaneously used by other threads in calls to functions other than[`crate::vk::DeviceLoader::destroy_swapchain_khr`].\nAccess to the swapchain data associated with this extension **must** be atomic\nwithin the implementation.\n\nValid Usage\n\n* []() VUID-vkWaitForPresentKHR-swapchain-04997  \n  [`Self::swapchain`] **must** not be in the retired state\n\nValid Usage (Implicit)\n\n* []() VUID-vkWaitForPresentKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkWaitForPresentKHR-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkWaitForPresentKHR-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::TIMEOUT`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkWaitForPresentKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, present_id: u64, timeout: u64) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePresentWaitFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePresentWaitFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePresentWaitFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePresentWaitFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePresentWaitFeaturesKHR.html)) · Structure <br/> VkPhysicalDevicePresentWaitFeaturesKHR - Structure indicating support for present wait\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_wait\ntypedef struct VkPhysicalDevicePresentWaitFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           presentWait;\n} VkPhysicalDevicePresentWaitFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* []() [`Self::present_wait`] indicates that the\n  implementation supports [`crate::vk::DeviceLoader::wait_for_present_khr`].\n\nIf the [`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePresentWaitFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePresentWaitFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePresentWaitFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub present_wait: crate::vk1_0::Bool32,
}
impl PhysicalDevicePresentWaitFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR;
}
impl Default for PhysicalDevicePresentWaitFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), present_wait: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePresentWaitFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePresentWaitFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("present_wait", &(self.present_wait != 0)).finish()
    }
}
impl PhysicalDevicePresentWaitFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
        PhysicalDevicePresentWaitFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePresentWaitFeaturesKHR.html)) · Builder of [`PhysicalDevicePresentWaitFeaturesKHR`] <br/> VkPhysicalDevicePresentWaitFeaturesKHR - Structure indicating support for present wait\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_wait\ntypedef struct VkPhysicalDevicePresentWaitFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           presentWait;\n} VkPhysicalDevicePresentWaitFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* []() [`Self::present_wait`] indicates that the\n  implementation supports [`crate::vk::DeviceLoader::wait_for_present_khr`].\n\nIf the [`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePresentWaitFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePresentWaitFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePresentWaitFeaturesKHRBuilder<'a>(PhysicalDevicePresentWaitFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
        PhysicalDevicePresentWaitFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn present_wait(mut self, present_wait: bool) -> Self {
        self.0.present_wait = present_wait as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePresentWaitFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
    type Target = PhysicalDevicePresentWaitFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePresentWaitFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_present_wait`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkWaitForPresentKHR.html)) · Function <br/> vkWaitForPresentKHR - Wait for presentation\n[](#_c_specification)C Specification\n----------\n\nWhen the [`presentWait`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-presentWait) feature is enabled, an\napplication **can** wait for an image to be presented to the user by first\nspecifying a presentId for the target presentation by adding a[`crate::vk::PresentIdKHR`] structure to the `pNext` chain of the[`crate::vk::PresentInfoKHR`] structure and then waiting for that presentation to\ncomplete by calling:\n\n```\n// Provided by VK_KHR_present_wait\nVkResult vkWaitForPresentKHR(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    uint64_t                                    presentId,\n    uint64_t                                    timeout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the non-retired swapchain on which an image was\n  queued for presentation.\n\n* [`Self::present_id`] is the presentation presentId to wait for.\n\n* [`Self::timeout`] is the timeout period in units of nanoseconds.[`Self::timeout`] is adjusted to the closest value allowed by the\n  implementation-dependent timeout accuracy, which **may** be substantially\n  longer than one nanosecond, and **may** be longer than the requested\n  period.\n[](#_description)Description\n----------\n\n[`crate::vk::DeviceLoader::wait_for_present_khr`] waits for the presentId associated with[`Self::swapchain`] to be increased in value so that it is at least equal to[`Self::present_id`].\n\nFor [`crate::vk::PresentModeKHR::MAILBOX_KHR`] (or other present mode where images\nmay be replaced in the presentation queue) any wait of this type associated\nwith such an image **must** be signaled no later than a wait associated with\nthe replacing image would be signaled.\n\nWhen the presentation has completed, the presentId associated with the\nrelated `pSwapChains` entry will be increased in value so that it is at\nleast equal to the value provided in the [`crate::vk::PresentIdKHR`] structure.\n\nThere is no requirement for any precise timing relationship between the\npresentation of the image to the user and the update of the presentId value,\nbut implementations **should** make this as close as possible to the\npresentation of the first pixel in the new image to the user.\n\nThe call to [`crate::vk::DeviceLoader::wait_for_present_khr`] will block until either the presentId\nassociated with [`Self::swapchain`] is greater than or equal to [`Self::present_id`],\nor [`Self::timeout`] nanoseconds passes.\nWhen the swapchain becomes OUT\\_OF\\_DATE, the call will either return[`crate::vk::Result::SUCCESS`] (if the image was delivered to the presentation engine and\nmay have been presented to the user) or will return early with status[`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`] (if the image was not presented to the user).\n\nAs an exception to the normal rules for objects which are externally\nsynchronized, the [`Self::swapchain`] passed to [`crate::vk::DeviceLoader::wait_for_present_khr`] **may**be simultaneously used by other threads in calls to functions other than[`crate::vk::DeviceLoader::destroy_swapchain_khr`].\nAccess to the swapchain data associated with this extension **must** be atomic\nwithin the implementation.\n\nValid Usage\n\n* []() VUID-vkWaitForPresentKHR-swapchain-04997  \n  [`Self::swapchain`] **must** not be in the retired state\n\nValid Usage (Implicit)\n\n* []() VUID-vkWaitForPresentKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkWaitForPresentKHR-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkWaitForPresentKHR-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::TIMEOUT`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkWaitForPresentKHR")]
    pub unsafe fn wait_for_present_khr(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, present_id: u64, timeout: u64) -> crate::utils::VulkanResult<()> {
        let _function = self.wait_for_present_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, swapchain as _, present_id as _, timeout as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
