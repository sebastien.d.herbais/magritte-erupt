#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_RAY_QUERY_SPEC_VERSION")]
pub const KHR_RAY_QUERY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_RAY_QUERY_EXTENSION_NAME")]
pub const KHR_RAY_QUERY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_ray_query");
#[doc = "Provided by [`crate::extensions::khr_ray_query`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR: Self = Self(1000348013);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRayQueryFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceRayQueryFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRayQueryFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceRayQueryFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayQueryFeaturesKHR.html)) · Structure <br/> VkPhysicalDeviceRayQueryFeaturesKHR - Structure describing the ray query features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_ray_query\ntypedef struct VkPhysicalDeviceRayQueryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rayQuery;\n} VkPhysicalDeviceRayQueryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::ray_query`] indicates whether the\n  implementation supports ray query (`OpRayQueryProceedKHR`)\n  functionality.\n\nIf the [`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRayQueryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceRayQueryFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceRayQueryFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub ray_query: crate::vk1_0::Bool32,
}
impl PhysicalDeviceRayQueryFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
}
impl Default for PhysicalDeviceRayQueryFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), ray_query: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceRayQueryFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceRayQueryFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("ray_query", &(self.ray_query != 0)).finish()
    }
}
impl PhysicalDeviceRayQueryFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
        PhysicalDeviceRayQueryFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceRayQueryFeaturesKHR.html)) · Builder of [`PhysicalDeviceRayQueryFeaturesKHR`] <br/> VkPhysicalDeviceRayQueryFeaturesKHR - Structure describing the ray query features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_ray_query\ntypedef struct VkPhysicalDeviceRayQueryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           rayQuery;\n} VkPhysicalDeviceRayQueryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::ray_query`] indicates whether the\n  implementation supports ray query (`OpRayQueryProceedKHR`)\n  functionality.\n\nIf the [`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceRayQueryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceRayQueryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceRayQueryFeaturesKHRBuilder<'a>(PhysicalDeviceRayQueryFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
        PhysicalDeviceRayQueryFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn ray_query(mut self, ray_query: bool) -> Self {
        self.0.ray_query = ray_query as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceRayQueryFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
    type Target = PhysicalDeviceRayQueryFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceRayQueryFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
