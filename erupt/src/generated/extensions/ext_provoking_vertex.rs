#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PROVOKING_VERTEX_SPEC_VERSION")]
pub const EXT_PROVOKING_VERTEX_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_PROVOKING_VERTEX_EXTENSION_NAME")]
pub const EXT_PROVOKING_VERTEX_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_provoking_vertex");
#[doc = "Provided by [`crate::extensions::ext_provoking_vertex`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT: Self = Self(1000254000);
    pub const PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT: Self = Self(1000254001);
    pub const PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT: Self = Self(1000254002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkProvokingVertexModeEXT.html)) · Enum <br/> VkProvokingVertexModeEXT - Specify which vertex in a primitive is the provoking vertex\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT::provoking_vertex_mode`]are:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef enum VkProvokingVertexModeEXT {\n    VK_PROVOKING_VERTEX_MODE_FIRST_VERTEX_EXT = 0,\n    VK_PROVOKING_VERTEX_MODE_LAST_VERTEX_EXT = 1,\n} VkProvokingVertexModeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::FIRST_VERTEX_EXT`] specifies that the\n  provoking vertex is the first non-adjacency vertex in the list of\n  vertices used by a primitive.\n\n* [`Self::LAST_VERTEX_EXT`] specifies that the\n  provoking vertex is the last non-adjacency vertex in the list of\n  vertices used by a primitive.\n\nThese modes are described more precisely in[Primitive Topologies](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#drawing-primitive-topologies).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT`]\n"]
#[doc(alias = "VkProvokingVertexModeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ProvokingVertexModeEXT(pub i32);
impl std::fmt::Debug for ProvokingVertexModeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::FIRST_VERTEX_EXT => "FIRST_VERTEX_EXT",
            &Self::LAST_VERTEX_EXT => "LAST_VERTEX_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_provoking_vertex`]"]
impl crate::extensions::ext_provoking_vertex::ProvokingVertexModeEXT {
    pub const FIRST_VERTEX_EXT: Self = Self(0);
    pub const LAST_VERTEX_EXT: Self = Self(1);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceProvokingVertexFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationProvokingVertexStateCreateInfoEXT> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceProvokingVertexFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceProvokingVertexPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProvokingVertexFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceProvokingVertexFeaturesEXT - Structure describing the provoking vertex features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPhysicalDeviceProvokingVertexFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           provokingVertexLast;\n    VkBool32           transformFeedbackPreservesProvokingVertex;\n} VkPhysicalDeviceProvokingVertexFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::provoking_vertex_last`] indicates\n  whether the implementation supports the[`crate::vk::ProvokingVertexModeEXT::LAST_VERTEX_EXT`][provoking vertex mode](VkProvokingVertexModeEXT.html) for flat shading.\n\n* []()[`Self::transform_feedback_preserves_provoking_vertex`] indicates that the order\n  of vertices within each primitive written by transform feedback will\n  preserve the provoking vertex.\n  This does not apply to triangle fan primitives when[`transformFeedbackPreservesTriangleFanProvokingVertex`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-transformFeedbackPreservesTriangleFanProvokingVertex)is [`crate::vk::FALSE`].[`Self::transform_feedback_preserves_provoking_vertex`] **must** be [`crate::vk::FALSE`]when the VK\\_EXT\\_transform\\_feedback extension is not supported.\n\nIf the [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nWhen [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] is in the [`Self::p_next`]chain of [`crate::vk::DeviceCreateInfo`] but the[transform feedback feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-transformFeedback) is not enabled,\nthe value of [`Self::transform_feedback_preserves_provoking_vertex`] is ignored.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceProvokingVertexFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceProvokingVertexFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceProvokingVertexFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub provoking_vertex_last: crate::vk1_0::Bool32,
    pub transform_feedback_preserves_provoking_vertex: crate::vk1_0::Bool32,
}
impl PhysicalDeviceProvokingVertexFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT;
}
impl Default for PhysicalDeviceProvokingVertexFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), provoking_vertex_last: Default::default(), transform_feedback_preserves_provoking_vertex: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceProvokingVertexFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceProvokingVertexFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("provoking_vertex_last", &(self.provoking_vertex_last != 0)).field("transform_feedback_preserves_provoking_vertex", &(self.transform_feedback_preserves_provoking_vertex != 0)).finish()
    }
}
impl PhysicalDeviceProvokingVertexFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
        PhysicalDeviceProvokingVertexFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProvokingVertexFeaturesEXT.html)) · Builder of [`PhysicalDeviceProvokingVertexFeaturesEXT`] <br/> VkPhysicalDeviceProvokingVertexFeaturesEXT - Structure describing the provoking vertex features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPhysicalDeviceProvokingVertexFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           provokingVertexLast;\n    VkBool32           transformFeedbackPreservesProvokingVertex;\n} VkPhysicalDeviceProvokingVertexFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::provoking_vertex_last`] indicates\n  whether the implementation supports the[`crate::vk::ProvokingVertexModeEXT::LAST_VERTEX_EXT`][provoking vertex mode](VkProvokingVertexModeEXT.html) for flat shading.\n\n* []()[`Self::transform_feedback_preserves_provoking_vertex`] indicates that the order\n  of vertices within each primitive written by transform feedback will\n  preserve the provoking vertex.\n  This does not apply to triangle fan primitives when[`transformFeedbackPreservesTriangleFanProvokingVertex`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-transformFeedbackPreservesTriangleFanProvokingVertex)is [`crate::vk::FALSE`].[`Self::transform_feedback_preserves_provoking_vertex`] **must** be [`crate::vk::FALSE`]when the VK\\_EXT\\_transform\\_feedback extension is not supported.\n\nIf the [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nWhen [`crate::vk::PhysicalDeviceProvokingVertexFeaturesEXT`] is in the [`Self::p_next`]chain of [`crate::vk::DeviceCreateInfo`] but the[transform feedback feature](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-transformFeedback) is not enabled,\nthe value of [`Self::transform_feedback_preserves_provoking_vertex`] is ignored.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceProvokingVertexFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a>(PhysicalDeviceProvokingVertexFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
        PhysicalDeviceProvokingVertexFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn provoking_vertex_last(mut self, provoking_vertex_last: bool) -> Self {
        self.0.provoking_vertex_last = provoking_vertex_last as _;
        self
    }
    #[inline]
    pub fn transform_feedback_preserves_provoking_vertex(mut self, transform_feedback_preserves_provoking_vertex: bool) -> Self {
        self.0.transform_feedback_preserves_provoking_vertex = transform_feedback_preserves_provoking_vertex as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceProvokingVertexFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceProvokingVertexFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceProvokingVertexFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProvokingVertexPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceProvokingVertexPropertiesEXT - Structure describing provoking vertex properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceProvokingVertexPropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPhysicalDeviceProvokingVertexPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           provokingVertexModePerPipeline;\n    VkBool32           transformFeedbackPreservesTriangleFanProvokingVertex;\n} VkPhysicalDeviceProvokingVertexPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::provoking_vertex_mode_per_pipeline`] indicates whether the\n  implementation supports graphics pipelines with different provoking\n  vertex modes within the same renderpass instance.\n\n* []()[`Self::transform_feedback_preserves_triangle_fan_provoking_vertex`] indicates\n  whether the implementation can preserve the provoking vertex order when\n  writing triangle fan vertices to transform feedback.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceProvokingVertexPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceProvokingVertexPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceProvokingVertexPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceProvokingVertexPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub provoking_vertex_mode_per_pipeline: crate::vk1_0::Bool32,
    pub transform_feedback_preserves_triangle_fan_provoking_vertex: crate::vk1_0::Bool32,
}
impl PhysicalDeviceProvokingVertexPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceProvokingVertexPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), provoking_vertex_mode_per_pipeline: Default::default(), transform_feedback_preserves_triangle_fan_provoking_vertex: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceProvokingVertexPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceProvokingVertexPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("provoking_vertex_mode_per_pipeline", &(self.provoking_vertex_mode_per_pipeline != 0)).field("transform_feedback_preserves_triangle_fan_provoking_vertex", &(self.transform_feedback_preserves_triangle_fan_provoking_vertex != 0)).finish()
    }
}
impl PhysicalDeviceProvokingVertexPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
        PhysicalDeviceProvokingVertexPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceProvokingVertexPropertiesEXT.html)) · Builder of [`PhysicalDeviceProvokingVertexPropertiesEXT`] <br/> VkPhysicalDeviceProvokingVertexPropertiesEXT - Structure describing provoking vertex properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceProvokingVertexPropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPhysicalDeviceProvokingVertexPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           provokingVertexModePerPipeline;\n    VkBool32           transformFeedbackPreservesTriangleFanProvokingVertex;\n} VkPhysicalDeviceProvokingVertexPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::provoking_vertex_mode_per_pipeline`] indicates whether the\n  implementation supports graphics pipelines with different provoking\n  vertex modes within the same renderpass instance.\n\n* []()[`Self::transform_feedback_preserves_triangle_fan_provoking_vertex`] indicates\n  whether the implementation can preserve the provoking vertex order when\n  writing triangle fan vertices to transform feedback.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceProvokingVertexPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceProvokingVertexPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a>(PhysicalDeviceProvokingVertexPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
        PhysicalDeviceProvokingVertexPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn provoking_vertex_mode_per_pipeline(mut self, provoking_vertex_mode_per_pipeline: bool) -> Self {
        self.0.provoking_vertex_mode_per_pipeline = provoking_vertex_mode_per_pipeline as _;
        self
    }
    #[inline]
    pub fn transform_feedback_preserves_triangle_fan_provoking_vertex(mut self, transform_feedback_preserves_triangle_fan_provoking_vertex: bool) -> Self {
        self.0.transform_feedback_preserves_triangle_fan_provoking_vertex = transform_feedback_preserves_triangle_fan_provoking_vertex as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceProvokingVertexPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceProvokingVertexPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceProvokingVertexPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationProvokingVertexStateCreateInfoEXT.html)) · Structure <br/> VkPipelineRasterizationProvokingVertexStateCreateInfoEXT - Structure specifying provoking vertex mode used by a graphics pipeline\n[](#_c_specification)C Specification\n----------\n\nFor a given primitive topology, the pipeline’s provoking vertex mode\ndetermines which vertex is the provoking vertex.\nTo specify the provoking vertex mode, attach an instance of[`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT`] to[`crate::vk::PipelineRasterizationStateCreateInfo::p_next`] when creating the\npipeline.\n\nThe [`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT`] structure\nis defined as:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPipelineRasterizationProvokingVertexStateCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkProvokingVertexModeEXT    provokingVertexMode;\n} VkPipelineRasterizationProvokingVertexStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::provoking_vertex_mode`] is a [`crate::vk::ProvokingVertexModeEXT`] value\n  selecting the provoking vertex mode.\n[](#_description)Description\n----------\n\nIf this struct is not provided when creating the pipeline, the pipeline will\nuse the [`crate::vk::ProvokingVertexModeEXT::FIRST_VERTEX_EXT`] mode.\n\nIf the[provokingVertexModePerPipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-provokingVertexModePerPipeline)limit is [`crate::vk::FALSE`], then the all pipelines bound within a renderpass\ninstance **must** have the same [`Self::provoking_vertex_mode`].\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-provokingVertexMode-04883  \n   If [`Self::provoking_vertex_mode`] is[`crate::vk::ProvokingVertexModeEXT::LAST_VERTEX_EXT`], then the[provokingVertexLast](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-provokingVertexLast) feature **must** be\n  enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-provokingVertexMode-parameter  \n  [`Self::provoking_vertex_mode`] **must** be a valid [`crate::vk::ProvokingVertexModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ProvokingVertexModeEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineRasterizationProvokingVertexStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub provoking_vertex_mode: crate::extensions::ext_provoking_vertex::ProvokingVertexModeEXT,
}
impl PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), provoking_vertex_mode: Default::default() }
    }
}
impl std::fmt::Debug for PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineRasterizationProvokingVertexStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("provoking_vertex_mode", &self.provoking_vertex_mode).finish()
    }
}
impl PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationProvokingVertexStateCreateInfoEXT.html)) · Builder of [`PipelineRasterizationProvokingVertexStateCreateInfoEXT`] <br/> VkPipelineRasterizationProvokingVertexStateCreateInfoEXT - Structure specifying provoking vertex mode used by a graphics pipeline\n[](#_c_specification)C Specification\n----------\n\nFor a given primitive topology, the pipeline’s provoking vertex mode\ndetermines which vertex is the provoking vertex.\nTo specify the provoking vertex mode, attach an instance of[`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT`] to[`crate::vk::PipelineRasterizationStateCreateInfo::p_next`] when creating the\npipeline.\n\nThe [`crate::vk::PipelineRasterizationProvokingVertexStateCreateInfoEXT`] structure\nis defined as:\n\n```\n// Provided by VK_EXT_provoking_vertex\ntypedef struct VkPipelineRasterizationProvokingVertexStateCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkProvokingVertexModeEXT    provokingVertexMode;\n} VkPipelineRasterizationProvokingVertexStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::provoking_vertex_mode`] is a [`crate::vk::ProvokingVertexModeEXT`] value\n  selecting the provoking vertex mode.\n[](#_description)Description\n----------\n\nIf this struct is not provided when creating the pipeline, the pipeline will\nuse the [`crate::vk::ProvokingVertexModeEXT::FIRST_VERTEX_EXT`] mode.\n\nIf the[provokingVertexModePerPipeline](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-provokingVertexModePerPipeline)limit is [`crate::vk::FALSE`], then the all pipelines bound within a renderpass\ninstance **must** have the same [`Self::provoking_vertex_mode`].\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-provokingVertexMode-04883  \n   If [`Self::provoking_vertex_mode`] is[`crate::vk::ProvokingVertexModeEXT::LAST_VERTEX_EXT`], then the[provokingVertexLast](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-provokingVertexLast) feature **must** be\n  enabled\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationProvokingVertexStateCreateInfoEXT-provokingVertexMode-parameter  \n  [`Self::provoking_vertex_mode`] **must** be a valid [`crate::vk::ProvokingVertexModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ProvokingVertexModeEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a>(PipelineRasterizationProvokingVertexStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn provoking_vertex_mode(mut self, provoking_vertex_mode: crate::extensions::ext_provoking_vertex::ProvokingVertexModeEXT) -> Self {
        self.0.provoking_vertex_mode = provoking_vertex_mode as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineRasterizationProvokingVertexStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineRasterizationProvokingVertexStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineRasterizationProvokingVertexStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
