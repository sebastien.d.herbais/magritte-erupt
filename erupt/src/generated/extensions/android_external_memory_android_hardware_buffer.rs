#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_ANDROID_EXTERNAL_MEMORY_ANDROID_HARDWARE_BUFFER_SPEC_VERSION")]
pub const ANDROID_EXTERNAL_MEMORY_ANDROID_HARDWARE_BUFFER_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_ANDROID_EXTERNAL_MEMORY_ANDROID_HARDWARE_BUFFER_EXTENSION_NAME")]
pub const ANDROID_EXTERNAL_MEMORY_ANDROID_HARDWARE_BUFFER_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_ANDROID_external_memory_android_hardware_buffer");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID: *const std::os::raw::c_char = crate::cstr!("vkGetAndroidHardwareBufferPropertiesANDROID");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_ANDROID_HARDWARE_BUFFER_ANDROID: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryAndroidHardwareBufferANDROID");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/AHardwareBuffer.html)) · Basetype <br/> AHardwareBuffer - Android hardware buffer type\n[](#_c_specification)C Specification\n----------\n\nTo remove an unnecessary compile-time dependency, an incomplete type\ndefinition of [`crate::vk::AHardwareBuffer`] is provided in the Vulkan headers:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\nstruct AHardwareBuffer;\n```\n[](#_description)Description\n----------\n\nThe actual [`crate::vk::AHardwareBuffer`] type is defined in Android NDK headers.\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
pub type AHardwareBuffer = std::ffi::c_void;
#[doc = "Provided by [`crate::extensions::android_external_memory_android_hardware_buffer`]"]
impl crate::vk1_0::StructureType {
    pub const ANDROID_HARDWARE_BUFFER_USAGE_ANDROID: Self = Self(1000129000);
    pub const ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID: Self = Self(1000129001);
    pub const ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID: Self = Self(1000129002);
    pub const IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID: Self = Self(1000129003);
    pub const MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID: Self = Self(1000129004);
    pub const EXTERNAL_FORMAT_ANDROID: Self = Self(1000129005);
}
#[doc = "Provided by [`crate::extensions::android_external_memory_android_hardware_buffer`]"]
impl crate::vk1_1::ExternalMemoryHandleTypeFlagBits {
    pub const ANDROID_HARDWARE_BUFFER_ANDROID: Self = Self(1024);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAndroidHardwareBufferPropertiesANDROID.html)) · Function <br/> vkGetAndroidHardwareBufferPropertiesANDROID - Get Properties of External Memory Android Hardware Buffers\n[](#_c_specification)C Specification\n----------\n\nTo determine the memory parameters to use when importing an Android hardware\nbuffer, call:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\nVkResult vkGetAndroidHardwareBufferPropertiesANDROID(\n    VkDevice                                    device,\n    const struct AHardwareBuffer*               buffer,\n    VkAndroidHardwareBufferPropertiesANDROID*   pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::buffer`].\n\n* [`Self::buffer`] is the Android hardware buffer which will be imported.\n\n* [`Self::p_properties`] is a pointer to a[`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure in which the\n  properties of [`Self::buffer`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-buffer-01884  \n  [`Self::buffer`] **must** be a valid Android hardware buffer object with at\n  least one of the `AHARDWAREBUFFER_USAGE_GPU_*` flags in its`AHardwareBuffer_Desc`::`usage`\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-buffer-parameter  \n  [`Self::buffer`] **must** be a valid pointer to a valid [`crate::vk::AHardwareBuffer`] value\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AndroidHardwareBufferPropertiesANDROID`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetAndroidHardwareBufferPropertiesANDROID = unsafe extern "system" fn(device: crate::vk1_0::Device, buffer: *const crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer, p_properties: *mut crate::extensions::android_external_memory_android_hardware_buffer::AndroidHardwareBufferPropertiesANDROID) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryAndroidHardwareBufferANDROID.html)) · Function <br/> vkGetMemoryAndroidHardwareBufferANDROID - Get an Android hardware buffer for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export an Android hardware buffer referencing the payload of a Vulkan\ndevice memory object, call:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\nVkResult vkGetMemoryAndroidHardwareBufferANDROID(\n    VkDevice                                    device,\n    const VkMemoryGetAndroidHardwareBufferInfoANDROID* pInfo,\n    struct AHardwareBuffer**                    pBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_info`] is a pointer to a[`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure containing\n  parameters of the export operation.\n\n* [`Self::p_buffer`] will return an Android hardware buffer referencing the\n  payload of the device memory object.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return an\nAndroid hardware buffer with a new reference acquired in addition to the\nreference held by the [`crate::vk::DeviceMemory`].\nTo avoid leaking resources, the application **must** release the reference by\ncalling `AHardwareBuffer_release` when it is no longer needed.\nWhen called with the same handle in[`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID::memory`],[`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return the same Android\nhardware buffer object.\nIf the device memory was created by importing an Android hardware buffer,[`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return that same Android\nhardware buffer object.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-pBuffer-parameter  \n  [`Self::p_buffer`] **must** be a valid pointer to a valid pointer to an [`crate::vk::AHardwareBuffer`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryAndroidHardwareBufferANDROID = unsafe extern "system" fn(device: crate::vk1_0::Device, p_info: *const crate::extensions::android_external_memory_android_hardware_buffer::MemoryGetAndroidHardwareBufferInfoANDROID, p_buffer: *mut *mut crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImportAndroidHardwareBufferInfoANDROID> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImportAndroidHardwareBufferInfoANDROIDBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalFormatANDROID> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalFormatANDROIDBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, AndroidHardwareBufferUsageANDROID> for crate::vk1_1::ImageFormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, AndroidHardwareBufferUsageANDROIDBuilder<'_>> for crate::vk1_1::ImageFormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalFormatANDROID> for crate::vk1_1::SamplerYcbcrConversionCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExternalFormatANDROIDBuilder<'_>> for crate::vk1_1::SamplerYcbcrConversionCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportAndroidHardwareBufferInfoANDROID.html)) · Structure <br/> VkImportAndroidHardwareBufferInfoANDROID - Import memory from an Android hardware buffer\n[](#_c_specification)C Specification\n----------\n\nTo import memory created outside of the current Vulkan instance from an\nAndroid hardware buffer, add a[`crate::vk::ImportAndroidHardwareBufferInfoANDROID`] structure to the [`Self::p_next`]chain of the [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportAndroidHardwareBufferInfoANDROID`] structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkImportAndroidHardwareBufferInfoANDROID {\n    VkStructureType            sType;\n    const void*                pNext;\n    struct AHardwareBuffer*    buffer;\n} VkImportAndroidHardwareBufferInfoANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::buffer`] is the Android hardware buffer to import.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PFN_vkAllocateMemory`] command succeeds, the implementation **must**acquire a reference to the imported hardware buffer, which it **must** release\nwhen the device memory object is freed.\nIf the command fails, the implementation **must** not retain a reference.\n\nValid Usage\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-01880  \n   If [`Self::buffer`] is not `NULL`, Android hardware buffers **must** be\n  supported for import, as reported by[`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-01881  \n   If [`Self::buffer`] is not `NULL`, it **must** be a valid Android hardware\n  buffer object with `AHardwareBuffer_Desc`::`usage` compatible with\n  Vulkan as described in [Android\n  Hardware Buffers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID`]\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-parameter  \n  [`Self::buffer`] **must** be a valid pointer to an [`crate::vk::AHardwareBuffer`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImportAndroidHardwareBufferInfoANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportAndroidHardwareBufferInfoANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub buffer: *mut crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer,
}
impl ImportAndroidHardwareBufferInfoANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID;
}
impl Default for ImportAndroidHardwareBufferInfoANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), buffer: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ImportAndroidHardwareBufferInfoANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportAndroidHardwareBufferInfoANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("buffer", &self.buffer).finish()
    }
}
impl ImportAndroidHardwareBufferInfoANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        ImportAndroidHardwareBufferInfoANDROIDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportAndroidHardwareBufferInfoANDROID.html)) · Builder of [`ImportAndroidHardwareBufferInfoANDROID`] <br/> VkImportAndroidHardwareBufferInfoANDROID - Import memory from an Android hardware buffer\n[](#_c_specification)C Specification\n----------\n\nTo import memory created outside of the current Vulkan instance from an\nAndroid hardware buffer, add a[`crate::vk::ImportAndroidHardwareBufferInfoANDROID`] structure to the [`Self::p_next`]chain of the [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportAndroidHardwareBufferInfoANDROID`] structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkImportAndroidHardwareBufferInfoANDROID {\n    VkStructureType            sType;\n    const void*                pNext;\n    struct AHardwareBuffer*    buffer;\n} VkImportAndroidHardwareBufferInfoANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::buffer`] is the Android hardware buffer to import.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PFN_vkAllocateMemory`] command succeeds, the implementation **must**acquire a reference to the imported hardware buffer, which it **must** release\nwhen the device memory object is freed.\nIf the command fails, the implementation **must** not retain a reference.\n\nValid Usage\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-01880  \n   If [`Self::buffer`] is not `NULL`, Android hardware buffers **must** be\n  supported for import, as reported by[`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-01881  \n   If [`Self::buffer`] is not `NULL`, it **must** be a valid Android hardware\n  buffer object with `AHardwareBuffer_Desc`::`usage` compatible with\n  Vulkan as described in [Android\n  Hardware Buffers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID`]\n\n* []() VUID-VkImportAndroidHardwareBufferInfoANDROID-buffer-parameter  \n  [`Self::buffer`] **must** be a valid pointer to an [`crate::vk::AHardwareBuffer`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImportAndroidHardwareBufferInfoANDROIDBuilder<'a>(ImportAndroidHardwareBufferInfoANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        ImportAndroidHardwareBufferInfoANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer(mut self, buffer: &'a mut crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer) -> Self {
        self.0.buffer = buffer as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportAndroidHardwareBufferInfoANDROID {
        self.0
    }
}
impl<'a> std::default::Default for ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn default() -> ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    type Target = ImportAndroidHardwareBufferInfoANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferUsageANDROID.html)) · Structure <br/> VkAndroidHardwareBufferUsageANDROID - Struct containing Android hardware buffer usage flags\n[](#_c_specification)C Specification\n----------\n\nTo obtain optimal Android hardware buffer usage flags for specific image\ncreation parameters, add a [`crate::vk::AndroidHardwareBufferUsageANDROID`]structure to the [`Self::p_next`] chain of a [`crate::vk::ImageFormatProperties2`]structure passed to [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`].\nThis structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferUsageANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           androidHardwareBufferUsage;\n} VkAndroidHardwareBufferUsageANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::android_hardware_buffer_usage`] returns the Android hardware buffer\n  usage flags.\n[](#_description)Description\n----------\n\nThe [`Self::android_hardware_buffer_usage`] field **must** include Android hardware\nbuffer usage flags listed in the[AHardwareBuffer Usage\nEquivalence](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-usage) table when the corresponding Vulkan image usage or image\ncreation flags are included in the `usage` or `flags` fields of[`crate::vk::PhysicalDeviceImageFormatInfo2`].\nIt **must** include at least one GPU usage flag\n(`AHARDWAREBUFFER_USAGE_GPU_*`), even if none of the corresponding Vulkan\nusages or flags are requested.\n\n|   |Note<br/><br/>Requiring at least one GPU usage flag ensures that Android hardware buffer<br/>memory will be allocated in a memory pool accessible to the Vulkan<br/>implementation, and that specializing the memory layout based on usage flags<br/>does not prevent it from being compatible with Vulkan.<br/>Implementations **may** avoid unnecessary restrictions caused by this<br/>requirement by using vendor usage flags to indicate that only the Vulkan<br/>uses indicated in [`crate::vk::ImageFormatProperties2`] are required.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferUsageANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_USAGE_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkAndroidHardwareBufferUsageANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AndroidHardwareBufferUsageANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub android_hardware_buffer_usage: u64,
}
impl AndroidHardwareBufferUsageANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ANDROID_HARDWARE_BUFFER_USAGE_ANDROID;
}
impl Default for AndroidHardwareBufferUsageANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), android_hardware_buffer_usage: Default::default() }
    }
}
impl std::fmt::Debug for AndroidHardwareBufferUsageANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AndroidHardwareBufferUsageANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("android_hardware_buffer_usage", &self.android_hardware_buffer_usage).finish()
    }
}
impl AndroidHardwareBufferUsageANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> AndroidHardwareBufferUsageANDROIDBuilder<'a> {
        AndroidHardwareBufferUsageANDROIDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferUsageANDROID.html)) · Builder of [`AndroidHardwareBufferUsageANDROID`] <br/> VkAndroidHardwareBufferUsageANDROID - Struct containing Android hardware buffer usage flags\n[](#_c_specification)C Specification\n----------\n\nTo obtain optimal Android hardware buffer usage flags for specific image\ncreation parameters, add a [`crate::vk::AndroidHardwareBufferUsageANDROID`]structure to the [`Self::p_next`] chain of a [`crate::vk::ImageFormatProperties2`]structure passed to [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`].\nThis structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferUsageANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           androidHardwareBufferUsage;\n} VkAndroidHardwareBufferUsageANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::android_hardware_buffer_usage`] returns the Android hardware buffer\n  usage flags.\n[](#_description)Description\n----------\n\nThe [`Self::android_hardware_buffer_usage`] field **must** include Android hardware\nbuffer usage flags listed in the[AHardwareBuffer Usage\nEquivalence](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-usage) table when the corresponding Vulkan image usage or image\ncreation flags are included in the `usage` or `flags` fields of[`crate::vk::PhysicalDeviceImageFormatInfo2`].\nIt **must** include at least one GPU usage flag\n(`AHARDWAREBUFFER_USAGE_GPU_*`), even if none of the corresponding Vulkan\nusages or flags are requested.\n\n|   |Note<br/><br/>Requiring at least one GPU usage flag ensures that Android hardware buffer<br/>memory will be allocated in a memory pool accessible to the Vulkan<br/>implementation, and that specializing the memory layout based on usage flags<br/>does not prevent it from being compatible with Vulkan.<br/>Implementations **may** avoid unnecessary restrictions caused by this<br/>requirement by using vendor usage flags to indicate that only the Vulkan<br/>uses indicated in [`crate::vk::ImageFormatProperties2`] are required.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferUsageANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_USAGE_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct AndroidHardwareBufferUsageANDROIDBuilder<'a>(AndroidHardwareBufferUsageANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> AndroidHardwareBufferUsageANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> AndroidHardwareBufferUsageANDROIDBuilder<'a> {
        AndroidHardwareBufferUsageANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn android_hardware_buffer_usage(mut self, android_hardware_buffer_usage: u64) -> Self {
        self.0.android_hardware_buffer_usage = android_hardware_buffer_usage as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AndroidHardwareBufferUsageANDROID {
        self.0
    }
}
impl<'a> std::default::Default for AndroidHardwareBufferUsageANDROIDBuilder<'a> {
    fn default() -> AndroidHardwareBufferUsageANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AndroidHardwareBufferUsageANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AndroidHardwareBufferUsageANDROIDBuilder<'a> {
    type Target = AndroidHardwareBufferUsageANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AndroidHardwareBufferUsageANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferPropertiesANDROID.html)) · Structure <br/> VkAndroidHardwareBufferPropertiesANDROID - Properties of External Memory Android Hardware Buffers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure returned is\ndefined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferPropertiesANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       allocationSize;\n    uint32_t           memoryTypeBits;\n} VkAndroidHardwareBufferPropertiesANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::allocation_size`] is the size of the external memory\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified Android hardware buffer **can** be imported\n  as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID`]\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL` or a pointer to a valid instance of [`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`]\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`]\n"]
#[doc(alias = "VkAndroidHardwareBufferPropertiesANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AndroidHardwareBufferPropertiesANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub allocation_size: crate::vk1_0::DeviceSize,
    pub memory_type_bits: u32,
}
impl AndroidHardwareBufferPropertiesANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID;
}
impl Default for AndroidHardwareBufferPropertiesANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), allocation_size: Default::default(), memory_type_bits: Default::default() }
    }
}
impl std::fmt::Debug for AndroidHardwareBufferPropertiesANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AndroidHardwareBufferPropertiesANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("allocation_size", &self.allocation_size).field("memory_type_bits", &self.memory_type_bits).finish()
    }
}
impl AndroidHardwareBufferPropertiesANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
        AndroidHardwareBufferPropertiesANDROIDBuilder(self, std::marker::PhantomData)
    }
}
impl<'a> crate::ExtendableFromMut<'a, AndroidHardwareBufferFormatPropertiesANDROID> for crate::extensions::android_external_memory_android_hardware_buffer::AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'_>> for crate::extensions::android_external_memory_android_hardware_buffer::AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferPropertiesANDROID.html)) · Builder of [`AndroidHardwareBufferPropertiesANDROID`] <br/> VkAndroidHardwareBufferPropertiesANDROID - Properties of External Memory Android Hardware Buffers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure returned is\ndefined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferPropertiesANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       allocationSize;\n    uint32_t           memoryTypeBits;\n} VkAndroidHardwareBufferPropertiesANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::allocation_size`] is the size of the external memory\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified Android hardware buffer **can** be imported\n  as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID`]\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL` or a pointer to a valid instance of [`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`]\n\n* []() VUID-VkAndroidHardwareBufferPropertiesANDROID-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`]\n"]
#[repr(transparent)]
pub struct AndroidHardwareBufferPropertiesANDROIDBuilder<'a>(AndroidHardwareBufferPropertiesANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
        AndroidHardwareBufferPropertiesANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn allocation_size(mut self, allocation_size: crate::vk1_0::DeviceSize) -> Self {
        self.0.allocation_size = allocation_size as _;
        self
    }
    #[inline]
    pub fn memory_type_bits(mut self, memory_type_bits: u32) -> Self {
        self.0.memory_type_bits = memory_type_bits as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AndroidHardwareBufferPropertiesANDROID {
        self.0
    }
}
impl<'a> std::default::Default for AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
    fn default() -> AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
    type Target = AndroidHardwareBufferPropertiesANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AndroidHardwareBufferPropertiesANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetAndroidHardwareBufferInfoANDROID.html)) · Structure <br/> VkMemoryGetAndroidHardwareBufferInfoANDROID - Structure describing an Android hardware buffer memory export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure is defined\nas:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkMemoryGetAndroidHardwareBufferInfoANDROID {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkDeviceMemory     memory;\n} VkMemoryGetAndroidHardwareBufferInfoANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the Android hardware buffer\n  will be exported.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-handleTypes-01882  \n  [`crate::vk::ExternalMemoryHandleTypeFlagBits::ANDROID_HARDWARE_BUFFER_ANDROID`]**must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-pNext-01883  \n   If the [`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`] used to\n  allocate [`Self::memory`] included a [`crate::vk::MemoryDedicatedAllocateInfo`]with non-`NULL` `image` member, then that `image` **must** already\n  be bound to [`Self::memory`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID`]\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`]\n"]
#[doc(alias = "VkMemoryGetAndroidHardwareBufferInfoANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryGetAndroidHardwareBufferInfoANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory: crate::vk1_0::DeviceMemory,
}
impl MemoryGetAndroidHardwareBufferInfoANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID;
}
impl Default for MemoryGetAndroidHardwareBufferInfoANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory: Default::default() }
    }
}
impl std::fmt::Debug for MemoryGetAndroidHardwareBufferInfoANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryGetAndroidHardwareBufferInfoANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory", &self.memory).finish()
    }
}
impl MemoryGetAndroidHardwareBufferInfoANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        MemoryGetAndroidHardwareBufferInfoANDROIDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetAndroidHardwareBufferInfoANDROID.html)) · Builder of [`MemoryGetAndroidHardwareBufferInfoANDROID`] <br/> VkMemoryGetAndroidHardwareBufferInfoANDROID - Structure describing an Android hardware buffer memory export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure is defined\nas:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkMemoryGetAndroidHardwareBufferInfoANDROID {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkDeviceMemory     memory;\n} VkMemoryGetAndroidHardwareBufferInfoANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the Android hardware buffer\n  will be exported.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-handleTypes-01882  \n  [`crate::vk::ExternalMemoryHandleTypeFlagBits::ANDROID_HARDWARE_BUFFER_ANDROID`]**must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-pNext-01883  \n   If the [`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`] used to\n  allocate [`Self::memory`] included a [`crate::vk::MemoryDedicatedAllocateInfo`]with non-`NULL` `image` member, then that `image` **must** already\n  be bound to [`Self::memory`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID`]\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetAndroidHardwareBufferInfoANDROID-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`]\n"]
#[repr(transparent)]
pub struct MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a>(MemoryGetAndroidHardwareBufferInfoANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        MemoryGetAndroidHardwareBufferInfoANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory(mut self, memory: crate::vk1_0::DeviceMemory) -> Self {
        self.0.memory = memory as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryGetAndroidHardwareBufferInfoANDROID {
        self.0
    }
}
impl<'a> std::default::Default for MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn default() -> MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    type Target = MemoryGetAndroidHardwareBufferInfoANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryGetAndroidHardwareBufferInfoANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferFormatPropertiesANDROID.html)) · Structure <br/> VkAndroidHardwareBufferFormatPropertiesANDROID - Structure describing the image format properties of an Android hardware buffer\n[](#_c_specification)C Specification\n----------\n\nTo obtain format properties of an Android hardware buffer, include a[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] structure in the[`Self::p_next`] chain of the [`crate::vk::AndroidHardwareBufferPropertiesANDROID`]structure passed to [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`].\nThis structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferFormatPropertiesANDROID {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkFormat                         format;\n    uint64_t                         externalFormat;\n    VkFormatFeatureFlags             formatFeatures;\n    VkComponentMapping               samplerYcbcrConversionComponents;\n    VkSamplerYcbcrModelConversion    suggestedYcbcrModel;\n    VkSamplerYcbcrRange              suggestedYcbcrRange;\n    VkChromaLocation                 suggestedXChromaOffset;\n    VkChromaLocation                 suggestedYChromaOffset;\n} VkAndroidHardwareBufferFormatPropertiesANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::format`] is the Vulkan format corresponding to the Android hardware\n  buffer’s format, or [`crate::vk::Format::UNDEFINED`] if there is not an\n  equivalent Vulkan format.\n\n* [`Self::external_format`] is an implementation-defined external format\n  identifier for use with [`crate::vk::ExternalFormatANDROID`].\n  It **must** not be zero.\n\n* [`Self::format_features`] describes the capabilities of this external format\n  when used with an image bound to memory imported from `buffer`.\n\n* [`Self::sampler_ycbcr_conversion_components`] is the component swizzle that**should** be used in [`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_ycbcr_model`] is a suggested color model to use in the[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_ycbcr_range`] is a suggested numerical value range to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_x_chroma_offset`] is a suggested X chroma offset to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_y_chroma_offset`] is a suggested Y chroma offset to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n[](#_description)Description\n----------\n\nIf the Android hardware buffer has one of the formats listed in the[Format Equivalence\ntable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-formats), then [`Self::format`] **must** have the equivalent Vulkan format listed in\nthe table.\nOtherwise, [`Self::format`] **may** be [`crate::vk::Format::UNDEFINED`], indicating the\nAndroid hardware buffer **can** only be used with an external format.\n\nThe [`Self::format_features`] member **must** include[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`] and at least one of[`crate::vk::FormatFeatureFlagBits::MIDPOINT_CHROMA_SAMPLES`] or[`crate::vk::FormatFeatureFlagBits::COSITED_CHROMA_SAMPLES`], and **should** include[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`] and[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER`].\n\n|   |Note<br/><br/>The [`Self::format_features`] member only indicates the features available when<br/>using an[external-format<br/>image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-external-formats) created from the Android hardware buffer.<br/>Images from Android hardware buffers with a format other than[`crate::vk::Format::UNDEFINED`] are subject to the format capabilities obtained<br/>from [`crate::vk::PFN_vkGetPhysicalDeviceFormatProperties2`], and[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`] with appropriate parameters.<br/>These sets of features are independent of each other, e.g. the external<br/>format will support sampler Y′C<sub>B</sub>C<sub>R</sub> conversion even if the non-external<br/>format does not, and writing to non-external format images is possible but<br/>writing to external format images is not.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nAndroid hardware buffers with the same external format **must** have the same\nsupport for [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`],[`crate::vk::FormatFeatureFlagBits::MIDPOINT_CHROMA_SAMPLES`],[`crate::vk::FormatFeatureFlagBits::COSITED_CHROMA_SAMPLES`],[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER`],[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER`],\nand[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE`].\nin [`Self::format_features`].\nOther format features **may** differ between Android hardware buffers that have\nthe same external format.\nThis allows applications to use the same [`crate::vk::SamplerYcbcrConversion`]object (and samplers and pipelines created from them) for any Android\nhardware buffers that have the same external format.\n\nIf [`Self::format`] is not [`crate::vk::Format::UNDEFINED`], then the value of[`Self::sampler_ycbcr_conversion_components`] **must** be valid when used as the`components` member of [`crate::vk::SamplerYcbcrConversionCreateInfo`] with\nthat format.\nIf [`Self::format`] is [`crate::vk::Format::UNDEFINED`], all members of[`Self::sampler_ycbcr_conversion_components`] **must** be the[identity swizzle](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-image-views-identity-mappings).\n\nImplementations **may** not always be able to determine the color model,\nnumerical range, or chroma offsets of the image contents, so the values in[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] are only suggestions.\nApplications **should** treat these values as sensible defaults to use in the\nabsence of more reliable information obtained through some other means.\nIf the underlying physical device is also usable via OpenGL ES with the[`GL_OES_EGL_image_external`](https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt)extension, the implementation **should** suggest values that will produce\nsimilar sampled values as would be obtained by sampling the same external\nimage via `samplerExternalOES` in OpenGL ES using equivalent sampler\nparameters.\n\n|   |Note<br/><br/>Since[`GL_OES_EGL_image_external`](https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt)does not require the same sampling and conversion calculations as Vulkan<br/>does, achieving identical results between APIs **may** not be possible on some<br/>implementations.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferFormatPropertiesANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ChromaLocation`], [`crate::vk::ComponentMapping`], [`crate::vk::Format`], [`crate::vk::FormatFeatureFlagBits`], [`crate::vk::SamplerYcbcrModelConversion`], [`crate::vk::SamplerYcbcrRange`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkAndroidHardwareBufferFormatPropertiesANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AndroidHardwareBufferFormatPropertiesANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub format: crate::vk1_0::Format,
    pub external_format: u64,
    pub format_features: crate::vk1_0::FormatFeatureFlags,
    pub sampler_ycbcr_conversion_components: crate::vk1_0::ComponentMapping,
    pub suggested_ycbcr_model: crate::vk1_1::SamplerYcbcrModelConversion,
    pub suggested_ycbcr_range: crate::vk1_1::SamplerYcbcrRange,
    pub suggested_x_chroma_offset: crate::vk1_1::ChromaLocation,
    pub suggested_y_chroma_offset: crate::vk1_1::ChromaLocation,
}
impl AndroidHardwareBufferFormatPropertiesANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID;
}
impl Default for AndroidHardwareBufferFormatPropertiesANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), format: Default::default(), external_format: Default::default(), format_features: Default::default(), sampler_ycbcr_conversion_components: Default::default(), suggested_ycbcr_model: Default::default(), suggested_ycbcr_range: Default::default(), suggested_x_chroma_offset: Default::default(), suggested_y_chroma_offset: Default::default() }
    }
}
impl std::fmt::Debug for AndroidHardwareBufferFormatPropertiesANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AndroidHardwareBufferFormatPropertiesANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("format", &self.format).field("external_format", &self.external_format).field("format_features", &self.format_features).field("sampler_ycbcr_conversion_components", &self.sampler_ycbcr_conversion_components).field("suggested_ycbcr_model", &self.suggested_ycbcr_model).field("suggested_ycbcr_range", &self.suggested_ycbcr_range).field("suggested_x_chroma_offset", &self.suggested_x_chroma_offset).field("suggested_y_chroma_offset", &self.suggested_y_chroma_offset).finish()
    }
}
impl AndroidHardwareBufferFormatPropertiesANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
        AndroidHardwareBufferFormatPropertiesANDROIDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidHardwareBufferFormatPropertiesANDROID.html)) · Builder of [`AndroidHardwareBufferFormatPropertiesANDROID`] <br/> VkAndroidHardwareBufferFormatPropertiesANDROID - Structure describing the image format properties of an Android hardware buffer\n[](#_c_specification)C Specification\n----------\n\nTo obtain format properties of an Android hardware buffer, include a[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] structure in the[`Self::p_next`] chain of the [`crate::vk::AndroidHardwareBufferPropertiesANDROID`]structure passed to [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`].\nThis structure is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkAndroidHardwareBufferFormatPropertiesANDROID {\n    VkStructureType                  sType;\n    void*                            pNext;\n    VkFormat                         format;\n    uint64_t                         externalFormat;\n    VkFormatFeatureFlags             formatFeatures;\n    VkComponentMapping               samplerYcbcrConversionComponents;\n    VkSamplerYcbcrModelConversion    suggestedYcbcrModel;\n    VkSamplerYcbcrRange              suggestedYcbcrRange;\n    VkChromaLocation                 suggestedXChromaOffset;\n    VkChromaLocation                 suggestedYChromaOffset;\n} VkAndroidHardwareBufferFormatPropertiesANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::format`] is the Vulkan format corresponding to the Android hardware\n  buffer’s format, or [`crate::vk::Format::UNDEFINED`] if there is not an\n  equivalent Vulkan format.\n\n* [`Self::external_format`] is an implementation-defined external format\n  identifier for use with [`crate::vk::ExternalFormatANDROID`].\n  It **must** not be zero.\n\n* [`Self::format_features`] describes the capabilities of this external format\n  when used with an image bound to memory imported from `buffer`.\n\n* [`Self::sampler_ycbcr_conversion_components`] is the component swizzle that**should** be used in [`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_ycbcr_model`] is a suggested color model to use in the[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_ycbcr_range`] is a suggested numerical value range to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_x_chroma_offset`] is a suggested X chroma offset to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n\n* [`Self::suggested_y_chroma_offset`] is a suggested Y chroma offset to use in[`crate::vk::SamplerYcbcrConversionCreateInfo`].\n[](#_description)Description\n----------\n\nIf the Android hardware buffer has one of the formats listed in the[Format Equivalence\ntable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-formats), then [`Self::format`] **must** have the equivalent Vulkan format listed in\nthe table.\nOtherwise, [`Self::format`] **may** be [`crate::vk::Format::UNDEFINED`], indicating the\nAndroid hardware buffer **can** only be used with an external format.\n\nThe [`Self::format_features`] member **must** include[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE`] and at least one of[`crate::vk::FormatFeatureFlagBits::MIDPOINT_CHROMA_SAMPLES`] or[`crate::vk::FormatFeatureFlagBits::COSITED_CHROMA_SAMPLES`], and **should** include[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`] and[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER`].\n\n|   |Note<br/><br/>The [`Self::format_features`] member only indicates the features available when<br/>using an[external-format<br/>image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-external-formats) created from the Android hardware buffer.<br/>Images from Android hardware buffers with a format other than[`crate::vk::Format::UNDEFINED`] are subject to the format capabilities obtained<br/>from [`crate::vk::PFN_vkGetPhysicalDeviceFormatProperties2`], and[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`] with appropriate parameters.<br/>These sets of features are independent of each other, e.g. the external<br/>format will support sampler Y′C<sub>B</sub>C<sub>R</sub> conversion even if the non-external<br/>format does not, and writing to non-external format images is possible but<br/>writing to external format images is not.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nAndroid hardware buffers with the same external format **must** have the same\nsupport for [`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_FILTER_LINEAR`],[`crate::vk::FormatFeatureFlagBits::MIDPOINT_CHROMA_SAMPLES`],[`crate::vk::FormatFeatureFlagBits::COSITED_CHROMA_SAMPLES`],[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER`],[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER`],\nand[`crate::vk::FormatFeatureFlagBits::SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE`].\nin [`Self::format_features`].\nOther format features **may** differ between Android hardware buffers that have\nthe same external format.\nThis allows applications to use the same [`crate::vk::SamplerYcbcrConversion`]object (and samplers and pipelines created from them) for any Android\nhardware buffers that have the same external format.\n\nIf [`Self::format`] is not [`crate::vk::Format::UNDEFINED`], then the value of[`Self::sampler_ycbcr_conversion_components`] **must** be valid when used as the`components` member of [`crate::vk::SamplerYcbcrConversionCreateInfo`] with\nthat format.\nIf [`Self::format`] is [`crate::vk::Format::UNDEFINED`], all members of[`Self::sampler_ycbcr_conversion_components`] **must** be the[identity swizzle](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-image-views-identity-mappings).\n\nImplementations **may** not always be able to determine the color model,\nnumerical range, or chroma offsets of the image contents, so the values in[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] are only suggestions.\nApplications **should** treat these values as sensible defaults to use in the\nabsence of more reliable information obtained through some other means.\nIf the underlying physical device is also usable via OpenGL ES with the[`GL_OES_EGL_image_external`](https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt)extension, the implementation **should** suggest values that will produce\nsimilar sampled values as would be obtained by sampling the same external\nimage via `samplerExternalOES` in OpenGL ES using equivalent sampler\nparameters.\n\n|   |Note<br/><br/>Since[`GL_OES_EGL_image_external`](https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt)does not require the same sampling and conversion calculations as Vulkan<br/>does, achieving identical results between APIs **may** not be possible on some<br/>implementations.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidHardwareBufferFormatPropertiesANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ChromaLocation`], [`crate::vk::ComponentMapping`], [`crate::vk::Format`], [`crate::vk::FormatFeatureFlagBits`], [`crate::vk::SamplerYcbcrModelConversion`], [`crate::vk::SamplerYcbcrRange`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a>(AndroidHardwareBufferFormatPropertiesANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
        AndroidHardwareBufferFormatPropertiesANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn format(mut self, format: crate::vk1_0::Format) -> Self {
        self.0.format = format as _;
        self
    }
    #[inline]
    pub fn external_format(mut self, external_format: u64) -> Self {
        self.0.external_format = external_format as _;
        self
    }
    #[inline]
    pub fn format_features(mut self, format_features: crate::vk1_0::FormatFeatureFlags) -> Self {
        self.0.format_features = format_features as _;
        self
    }
    #[inline]
    pub fn sampler_ycbcr_conversion_components(mut self, sampler_ycbcr_conversion_components: crate::vk1_0::ComponentMapping) -> Self {
        self.0.sampler_ycbcr_conversion_components = sampler_ycbcr_conversion_components as _;
        self
    }
    #[inline]
    pub fn suggested_ycbcr_model(mut self, suggested_ycbcr_model: crate::vk1_1::SamplerYcbcrModelConversion) -> Self {
        self.0.suggested_ycbcr_model = suggested_ycbcr_model as _;
        self
    }
    #[inline]
    pub fn suggested_ycbcr_range(mut self, suggested_ycbcr_range: crate::vk1_1::SamplerYcbcrRange) -> Self {
        self.0.suggested_ycbcr_range = suggested_ycbcr_range as _;
        self
    }
    #[inline]
    pub fn suggested_x_chroma_offset(mut self, suggested_x_chroma_offset: crate::vk1_1::ChromaLocation) -> Self {
        self.0.suggested_x_chroma_offset = suggested_x_chroma_offset as _;
        self
    }
    #[inline]
    pub fn suggested_y_chroma_offset(mut self, suggested_y_chroma_offset: crate::vk1_1::ChromaLocation) -> Self {
        self.0.suggested_y_chroma_offset = suggested_y_chroma_offset as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AndroidHardwareBufferFormatPropertiesANDROID {
        self.0
    }
}
impl<'a> std::default::Default for AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
    fn default() -> AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
    type Target = AndroidHardwareBufferFormatPropertiesANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AndroidHardwareBufferFormatPropertiesANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFormatANDROID.html)) · Structure <br/> VkExternalFormatANDROID - Structure containing an Android hardware buffer external format\n[](#_c_specification)C Specification\n----------\n\nTo create an image with an[external\nformat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-external-formats), add a [`crate::vk::ExternalFormatANDROID`] structure in the [`Self::p_next`]chain of [`crate::vk::ImageCreateInfo`].[`crate::vk::ExternalFormatANDROID`] is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkExternalFormatANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           externalFormat;\n} VkExternalFormatANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::external_format`] is an implementation-defined identifier for the\n  external format\n[](#_description)Description\n----------\n\nIf [`Self::external_format`] is zero, the effect is as if the[`crate::vk::ExternalFormatANDROID`] structure was not present.\nOtherwise, the `image` will have the specified external format.\n\nValid Usage\n\n* []() VUID-VkExternalFormatANDROID-externalFormat-01894  \n  [`Self::external_format`] **must** be `0` or a value returned in the[`Self::external_format`] member of[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] by an earlier call\n  to [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExternalFormatANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXTERNAL_FORMAT_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExternalFormatANDROID")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExternalFormatANDROID {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub external_format: u64,
}
impl ExternalFormatANDROID {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXTERNAL_FORMAT_ANDROID;
}
impl Default for ExternalFormatANDROID {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), external_format: Default::default() }
    }
}
impl std::fmt::Debug for ExternalFormatANDROID {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExternalFormatANDROID").field("s_type", &self.s_type).field("p_next", &self.p_next).field("external_format", &self.external_format).finish()
    }
}
impl ExternalFormatANDROID {
    #[inline]
    pub fn into_builder<'a>(self) -> ExternalFormatANDROIDBuilder<'a> {
        ExternalFormatANDROIDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFormatANDROID.html)) · Builder of [`ExternalFormatANDROID`] <br/> VkExternalFormatANDROID - Structure containing an Android hardware buffer external format\n[](#_c_specification)C Specification\n----------\n\nTo create an image with an[external\nformat](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-external-android-hardware-buffer-external-formats), add a [`crate::vk::ExternalFormatANDROID`] structure in the [`Self::p_next`]chain of [`crate::vk::ImageCreateInfo`].[`crate::vk::ExternalFormatANDROID`] is defined as:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\ntypedef struct VkExternalFormatANDROID {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           externalFormat;\n} VkExternalFormatANDROID;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::external_format`] is an implementation-defined identifier for the\n  external format\n[](#_description)Description\n----------\n\nIf [`Self::external_format`] is zero, the effect is as if the[`crate::vk::ExternalFormatANDROID`] structure was not present.\nOtherwise, the `image` will have the specified external format.\n\nValid Usage\n\n* []() VUID-VkExternalFormatANDROID-externalFormat-01894  \n  [`Self::external_format`] **must** be `0` or a value returned in the[`Self::external_format`] member of[`crate::vk::AndroidHardwareBufferFormatPropertiesANDROID`] by an earlier call\n  to [`crate::vk::DeviceLoader::get_android_hardware_buffer_properties_android`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExternalFormatANDROID-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXTERNAL_FORMAT_ANDROID`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExternalFormatANDROIDBuilder<'a>(ExternalFormatANDROID, std::marker::PhantomData<&'a ()>);
impl<'a> ExternalFormatANDROIDBuilder<'a> {
    #[inline]
    pub fn new() -> ExternalFormatANDROIDBuilder<'a> {
        ExternalFormatANDROIDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn external_format(mut self, external_format: u64) -> Self {
        self.0.external_format = external_format as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExternalFormatANDROID {
        self.0
    }
}
impl<'a> std::default::Default for ExternalFormatANDROIDBuilder<'a> {
    fn default() -> ExternalFormatANDROIDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExternalFormatANDROIDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExternalFormatANDROIDBuilder<'a> {
    type Target = ExternalFormatANDROID;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExternalFormatANDROIDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::android_external_memory_android_hardware_buffer`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetAndroidHardwareBufferPropertiesANDROID.html)) · Function <br/> vkGetAndroidHardwareBufferPropertiesANDROID - Get Properties of External Memory Android Hardware Buffers\n[](#_c_specification)C Specification\n----------\n\nTo determine the memory parameters to use when importing an Android hardware\nbuffer, call:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\nVkResult vkGetAndroidHardwareBufferPropertiesANDROID(\n    VkDevice                                    device,\n    const struct AHardwareBuffer*               buffer,\n    VkAndroidHardwareBufferPropertiesANDROID*   pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::buffer`].\n\n* [`Self::buffer`] is the Android hardware buffer which will be imported.\n\n* [`Self::p_properties`] is a pointer to a[`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure in which the\n  properties of [`Self::buffer`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-buffer-01884  \n  [`Self::buffer`] **must** be a valid Android hardware buffer object with at\n  least one of the `AHARDWAREBUFFER_USAGE_GPU_*` flags in its`AHardwareBuffer_Desc`::`usage`\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-buffer-parameter  \n  [`Self::buffer`] **must** be a valid pointer to a valid [`crate::vk::AHardwareBuffer`] value\n\n* []() VUID-vkGetAndroidHardwareBufferPropertiesANDROID-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::AndroidHardwareBufferPropertiesANDROID`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AndroidHardwareBufferPropertiesANDROID`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkGetAndroidHardwareBufferPropertiesANDROID")]
    pub unsafe fn get_android_hardware_buffer_properties_android(&self, buffer: &crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer, properties: Option<crate::extensions::android_external_memory_android_hardware_buffer::AndroidHardwareBufferPropertiesANDROID>) -> crate::utils::VulkanResult<crate::extensions::android_external_memory_android_hardware_buffer::AndroidHardwareBufferPropertiesANDROID> {
        let _function = self.get_android_hardware_buffer_properties_android.expect(crate::NOT_LOADED_MESSAGE);
        let mut properties = match properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, buffer as _, &mut properties);
        crate::utils::VulkanResult::new(_return, {
            properties.p_next = std::ptr::null_mut() as _;
            properties
        })
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryAndroidHardwareBufferANDROID.html)) · Function <br/> vkGetMemoryAndroidHardwareBufferANDROID - Get an Android hardware buffer for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export an Android hardware buffer referencing the payload of a Vulkan\ndevice memory object, call:\n\n```\n// Provided by VK_ANDROID_external_memory_android_hardware_buffer\nVkResult vkGetMemoryAndroidHardwareBufferANDROID(\n    VkDevice                                    device,\n    const VkMemoryGetAndroidHardwareBufferInfoANDROID* pInfo,\n    struct AHardwareBuffer**                    pBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_info`] is a pointer to a[`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure containing\n  parameters of the export operation.\n\n* [`Self::p_buffer`] will return an Android hardware buffer referencing the\n  payload of the device memory object.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return an\nAndroid hardware buffer with a new reference acquired in addition to the\nreference held by the [`crate::vk::DeviceMemory`].\nTo avoid leaking resources, the application **must** release the reference by\ncalling `AHardwareBuffer_release` when it is no longer needed.\nWhen called with the same handle in[`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID::memory`],[`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return the same Android\nhardware buffer object.\nIf the device memory was created by importing an Android hardware buffer,[`crate::vk::DeviceLoader::get_memory_android_hardware_buffer_android`] **must** return that same Android\nhardware buffer object.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`] structure\n\n* []() VUID-vkGetMemoryAndroidHardwareBufferANDROID-pBuffer-parameter  \n  [`Self::p_buffer`] **must** be a valid pointer to a valid pointer to an [`crate::vk::AHardwareBuffer`] value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetAndroidHardwareBufferInfoANDROID`]\n"]
    #[doc(alias = "vkGetMemoryAndroidHardwareBufferANDROID")]
    pub unsafe fn get_memory_android_hardware_buffer_android(&self, info: &crate::extensions::android_external_memory_android_hardware_buffer::MemoryGetAndroidHardwareBufferInfoANDROID) -> crate::utils::VulkanResult<*mut crate::extensions::android_external_memory_android_hardware_buffer::AHardwareBuffer> {
        let _function = self.get_memory_android_hardware_buffer_android.expect(crate::NOT_LOADED_MESSAGE);
        let mut buffer = std::ptr::null_mut();
        let _return = _function(self.handle, info as _, &mut buffer);
        crate::utils::VulkanResult::new(_return, buffer)
    }
}
