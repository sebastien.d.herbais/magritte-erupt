#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_BLEND_OPERATION_ADVANCED_SPEC_VERSION")]
pub const EXT_BLEND_OPERATION_ADVANCED_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_BLEND_OPERATION_ADVANCED_EXTENSION_NAME")]
pub const EXT_BLEND_OPERATION_ADVANCED_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_blend_operation_advanced");
#[doc = "Provided by [`crate::extensions::ext_blend_operation_advanced`]"]
impl crate::vk1_0::BlendOp {
    pub const ZERO_EXT: Self = Self(1000148000);
    pub const SRC_EXT: Self = Self(1000148001);
    pub const DST_EXT: Self = Self(1000148002);
    pub const SRC_OVER_EXT: Self = Self(1000148003);
    pub const DST_OVER_EXT: Self = Self(1000148004);
    pub const SRC_IN_EXT: Self = Self(1000148005);
    pub const DST_IN_EXT: Self = Self(1000148006);
    pub const SRC_OUT_EXT: Self = Self(1000148007);
    pub const DST_OUT_EXT: Self = Self(1000148008);
    pub const SRC_ATOP_EXT: Self = Self(1000148009);
    pub const DST_ATOP_EXT: Self = Self(1000148010);
    pub const XOR_EXT: Self = Self(1000148011);
    pub const MULTIPLY_EXT: Self = Self(1000148012);
    pub const SCREEN_EXT: Self = Self(1000148013);
    pub const OVERLAY_EXT: Self = Self(1000148014);
    pub const DARKEN_EXT: Self = Self(1000148015);
    pub const LIGHTEN_EXT: Self = Self(1000148016);
    pub const COLORDODGE_EXT: Self = Self(1000148017);
    pub const COLORBURN_EXT: Self = Self(1000148018);
    pub const HARDLIGHT_EXT: Self = Self(1000148019);
    pub const SOFTLIGHT_EXT: Self = Self(1000148020);
    pub const DIFFERENCE_EXT: Self = Self(1000148021);
    pub const EXCLUSION_EXT: Self = Self(1000148022);
    pub const INVERT_EXT: Self = Self(1000148023);
    pub const INVERT_RGB_EXT: Self = Self(1000148024);
    pub const LINEARDODGE_EXT: Self = Self(1000148025);
    pub const LINEARBURN_EXT: Self = Self(1000148026);
    pub const VIVIDLIGHT_EXT: Self = Self(1000148027);
    pub const LINEARLIGHT_EXT: Self = Self(1000148028);
    pub const PINLIGHT_EXT: Self = Self(1000148029);
    pub const HARDMIX_EXT: Self = Self(1000148030);
    pub const HSL_HUE_EXT: Self = Self(1000148031);
    pub const HSL_SATURATION_EXT: Self = Self(1000148032);
    pub const HSL_COLOR_EXT: Self = Self(1000148033);
    pub const HSL_LUMINOSITY_EXT: Self = Self(1000148034);
    pub const PLUS_EXT: Self = Self(1000148035);
    pub const PLUS_CLAMPED_EXT: Self = Self(1000148036);
    pub const PLUS_CLAMPED_ALPHA_EXT: Self = Self(1000148037);
    pub const PLUS_DARKER_EXT: Self = Self(1000148038);
    pub const MINUS_EXT: Self = Self(1000148039);
    pub const MINUS_CLAMPED_EXT: Self = Self(1000148040);
    pub const CONTRAST_EXT: Self = Self(1000148041);
    pub const INVERT_OVG_EXT: Self = Self(1000148042);
    pub const RED_EXT: Self = Self(1000148043);
    pub const GREEN_EXT: Self = Self(1000148044);
    pub const BLUE_EXT: Self = Self(1000148045);
}
#[doc = "Provided by [`crate::extensions::ext_blend_operation_advanced`]"]
impl crate::vk1_0::AccessFlagBits {
    pub const COLOR_ATTACHMENT_READ_NONCOHERENT_EXT: Self = Self(524288);
}
#[doc = "Provided by [`crate::extensions::ext_blend_operation_advanced`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT: Self = Self(1000148000);
    pub const PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT: Self = Self(1000148001);
    pub const PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT: Self = Self(1000148002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkBlendOverlapEXT.html)) · Enum <br/> VkBlendOverlapEXT - Enumerant specifying the blend overlap parameter\n[](#_c_specification)C Specification\n----------\n\nThe weighting functions p<sub>0</sub>, p<sub>1</sub>, and p<sub>2</sub> are defined\nin table [Advanced Blend Overlap\nModes](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-blend-advanced-overlap-modes).\nIn these functions, the A components of the source and destination colors\nare taken to indicate the portion of the pixel covered by the fragment\n(source) and the fragments previously accumulated in the pixel\n(destination).\nThe functions p<sub>0</sub>, p<sub>1</sub>, and p<sub>2</sub> approximate the\nrelative portion of the pixel covered by the intersection of the source and\ndestination, covered only by the source, and covered only by the\ndestination, respectively.\n\nPossible values of[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::blend_overlap`],\nspecifying the blend overlap functions, are:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef enum VkBlendOverlapEXT {\n    VK_BLEND_OVERLAP_UNCORRELATED_EXT = 0,\n    VK_BLEND_OVERLAP_DISJOINT_EXT = 1,\n    VK_BLEND_OVERLAP_CONJOINT_EXT = 2,\n} VkBlendOverlapEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::UNCORRELATED_EXT`] specifies that there is no\n  correlation between the source and destination coverage.\n\n* [`Self::CONJOINT_EXT`] specifies that the source and\n  destination coverage are considered to have maximal overlap.\n\n* [`Self::DISJOINT_EXT`] specifies that the source and\n  destination coverage are considered to have minimal overlap.\n\n|           Overlap Mode            |                                 Weighting Equations                                 |\n|-----------------------------------|-------------------------------------------------------------------------------------|\n|[`Self::UNCORRELATED_EXT`]|         p0\u{200b}(As\u{200b},Ad\u{200b})p1\u{200b}(As\u{200b},Ad\u{200b})p2\u{200b}(As\u{200b},Ad\u{200b})\u{200b}=As\u{200b}Ad\u{200b}=As\u{200b}(1−Ad\u{200b})=Ad\u{200b}(1−As\u{200b})\u{200b}         |\n|  [`Self::CONJOINT_EXT`]  |  p0\u{200b}(As\u{200b},Ad\u{200b})p1\u{200b}(As\u{200b},Ad\u{200b})p2\u{200b}(As\u{200b},Ad\u{200b})\u{200b}=min(As\u{200b},Ad\u{200b})=max(As\u{200b}−Ad\u{200b},0)=max(Ad\u{200b}−As\u{200b},0)\u{200b}  |\n|  [`Self::DISJOINT_EXT`]  |p0\u{200b}(As\u{200b},Ad\u{200b})p1\u{200b}(As\u{200b},Ad\u{200b})p2\u{200b}(As\u{200b},Ad\u{200b})\u{200b}=max(As\u{200b}+Ad\u{200b}−1,0)=min(As\u{200b},1−Ad\u{200b})=min(Ad\u{200b},1−As\u{200b})\u{200b}|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT`]\n"]
#[doc(alias = "VkBlendOverlapEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct BlendOverlapEXT(pub i32);
impl std::fmt::Debug for BlendOverlapEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::UNCORRELATED_EXT => "UNCORRELATED_EXT",
            &Self::DISJOINT_EXT => "DISJOINT_EXT",
            &Self::CONJOINT_EXT => "CONJOINT_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_blend_operation_advanced`]"]
impl crate::extensions::ext_blend_operation_advanced::BlendOverlapEXT {
    pub const UNCORRELATED_EXT: Self = Self(0);
    pub const DISJOINT_EXT: Self = Self(1);
    pub const CONJOINT_EXT: Self = Self(2);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceBlendOperationAdvancedFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineColorBlendAdvancedStateCreateInfoEXT> for crate::vk1_0::PipelineColorBlendStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineColorBlendStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBlendOperationAdvancedFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBlendOperationAdvancedPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT - Structure describing advanced blending features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           advancedBlendCoherentOperations;\n} VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::advanced_blend_coherent_operations`] specifies whether blending using[advanced blend operations](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-blend-advanced) is guaranteed\n  to execute atomically and in [primitive\n  order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#drawing-primitive-order).\n  If this is [`crate::vk::TRUE`],[`crate::vk::AccessFlagBits::COLOR_ATTACHMENT_READ_NONCOHERENT_EXT`] is treated the\n  same as [`crate::vk::AccessFlagBits::COLOR_ATTACHMENT_READ`], and advanced blending\n  needs no additional synchronization over basic blending.\n  If this is [`crate::vk::FALSE`], then memory dependencies are required to\n  guarantee order between two advanced blending operations that occur on\n  the same sample.\n\nIf the [`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub advanced_blend_coherent_operations: crate::vk1_0::Bool32,
}
impl PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT;
}
impl Default for PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), advanced_blend_coherent_operations: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceBlendOperationAdvancedFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("advanced_blend_coherent_operations", &(self.advanced_blend_coherent_operations != 0)).finish()
    }
}
impl PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
        PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT.html)) · Builder of [`PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] <br/> VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT - Structure describing advanced blending features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           advancedBlendCoherentOperations;\n} VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::advanced_blend_coherent_operations`] specifies whether blending using[advanced blend operations](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-blend-advanced) is guaranteed\n  to execute atomically and in [primitive\n  order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#drawing-primitive-order).\n  If this is [`crate::vk::TRUE`],[`crate::vk::AccessFlagBits::COLOR_ATTACHMENT_READ_NONCOHERENT_EXT`] is treated the\n  same as [`crate::vk::AccessFlagBits::COLOR_ATTACHMENT_READ`], and advanced blending\n  needs no additional synchronization over basic blending.\n  If this is [`crate::vk::FALSE`], then memory dependencies are required to\n  guarantee order between two advanced blending operations that occur on\n  the same sample.\n\nIf the [`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceBlendOperationAdvancedFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a>(PhysicalDeviceBlendOperationAdvancedFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
        PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn advanced_blend_coherent_operations(mut self, advanced_blend_coherent_operations: bool) -> Self {
        self.0.advanced_blend_coherent_operations = advanced_blend_coherent_operations as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceBlendOperationAdvancedFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceBlendOperationAdvancedFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT - Structure describing advanced blending limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBlendOperationAdvancedPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           advancedBlendMaxColorAttachments;\n    VkBool32           advancedBlendIndependentBlend;\n    VkBool32           advancedBlendNonPremultipliedSrcColor;\n    VkBool32           advancedBlendNonPremultipliedDstColor;\n    VkBool32           advancedBlendCorrelatedOverlap;\n    VkBool32           advancedBlendAllOperations;\n} VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::advanced_blend_max_color_attachments`] is one greater than the highest\n  color attachment index that **can** be used in a subpass, for a pipeline\n  that uses an [advanced blend operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-blend-advanced).\n\n* []()[`Self::advanced_blend_independent_blend`] specifies whether advanced blend\n  operations **can** vary per-attachment.\n\n* []()[`Self::advanced_blend_non_premultiplied_src_color`] specifies whether the source\n  color **can** be treated as non-premultiplied.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::src_premultiplied`]**must** be [`crate::vk::TRUE`].\n\n* []()[`Self::advanced_blend_non_premultiplied_dst_color`] specifies whether the\n  destination color **can** be treated as non-premultiplied.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::dst_premultiplied`]**must** be [`crate::vk::TRUE`].\n\n* []()[`Self::advanced_blend_correlated_overlap`] specifies whether the overlap mode**can** be treated as correlated.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::blend_overlap`]**must** be [`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`].\n\n* []() [`Self::advanced_blend_all_operations`]specifies whether all advanced blend operation enums are supported.\n  See the valid usage of [`crate::vk::PipelineColorBlendAttachmentState`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceBlendOperationAdvancedPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub advanced_blend_max_color_attachments: u32,
    pub advanced_blend_independent_blend: crate::vk1_0::Bool32,
    pub advanced_blend_non_premultiplied_src_color: crate::vk1_0::Bool32,
    pub advanced_blend_non_premultiplied_dst_color: crate::vk1_0::Bool32,
    pub advanced_blend_correlated_overlap: crate::vk1_0::Bool32,
    pub advanced_blend_all_operations: crate::vk1_0::Bool32,
}
impl PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), advanced_blend_max_color_attachments: Default::default(), advanced_blend_independent_blend: Default::default(), advanced_blend_non_premultiplied_src_color: Default::default(), advanced_blend_non_premultiplied_dst_color: Default::default(), advanced_blend_correlated_overlap: Default::default(), advanced_blend_all_operations: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceBlendOperationAdvancedPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("advanced_blend_max_color_attachments", &self.advanced_blend_max_color_attachments).field("advanced_blend_independent_blend", &(self.advanced_blend_independent_blend != 0)).field("advanced_blend_non_premultiplied_src_color", &(self.advanced_blend_non_premultiplied_src_color != 0)).field("advanced_blend_non_premultiplied_dst_color", &(self.advanced_blend_non_premultiplied_dst_color != 0)).field("advanced_blend_correlated_overlap", &(self.advanced_blend_correlated_overlap != 0)).field("advanced_blend_all_operations", &(self.advanced_blend_all_operations != 0)).finish()
    }
}
impl PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
        PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT.html)) · Builder of [`PhysicalDeviceBlendOperationAdvancedPropertiesEXT`] <br/> VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT - Structure describing advanced blending limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceBlendOperationAdvancedPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           advancedBlendMaxColorAttachments;\n    VkBool32           advancedBlendIndependentBlend;\n    VkBool32           advancedBlendNonPremultipliedSrcColor;\n    VkBool32           advancedBlendNonPremultipliedDstColor;\n    VkBool32           advancedBlendCorrelatedOverlap;\n    VkBool32           advancedBlendAllOperations;\n} VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::advanced_blend_max_color_attachments`] is one greater than the highest\n  color attachment index that **can** be used in a subpass, for a pipeline\n  that uses an [advanced blend operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-blend-advanced).\n\n* []()[`Self::advanced_blend_independent_blend`] specifies whether advanced blend\n  operations **can** vary per-attachment.\n\n* []()[`Self::advanced_blend_non_premultiplied_src_color`] specifies whether the source\n  color **can** be treated as non-premultiplied.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::src_premultiplied`]**must** be [`crate::vk::TRUE`].\n\n* []()[`Self::advanced_blend_non_premultiplied_dst_color`] specifies whether the\n  destination color **can** be treated as non-premultiplied.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::dst_premultiplied`]**must** be [`crate::vk::TRUE`].\n\n* []()[`Self::advanced_blend_correlated_overlap`] specifies whether the overlap mode**can** be treated as correlated.\n  If this is [`crate::vk::FALSE`], then[`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT::blend_overlap`]**must** be [`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`].\n\n* []() [`Self::advanced_blend_all_operations`]specifies whether all advanced blend operation enums are supported.\n  See the valid usage of [`crate::vk::PipelineColorBlendAttachmentState`].\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceBlendOperationAdvancedPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a>(PhysicalDeviceBlendOperationAdvancedPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
        PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn advanced_blend_max_color_attachments(mut self, advanced_blend_max_color_attachments: u32) -> Self {
        self.0.advanced_blend_max_color_attachments = advanced_blend_max_color_attachments as _;
        self
    }
    #[inline]
    pub fn advanced_blend_independent_blend(mut self, advanced_blend_independent_blend: bool) -> Self {
        self.0.advanced_blend_independent_blend = advanced_blend_independent_blend as _;
        self
    }
    #[inline]
    pub fn advanced_blend_non_premultiplied_src_color(mut self, advanced_blend_non_premultiplied_src_color: bool) -> Self {
        self.0.advanced_blend_non_premultiplied_src_color = advanced_blend_non_premultiplied_src_color as _;
        self
    }
    #[inline]
    pub fn advanced_blend_non_premultiplied_dst_color(mut self, advanced_blend_non_premultiplied_dst_color: bool) -> Self {
        self.0.advanced_blend_non_premultiplied_dst_color = advanced_blend_non_premultiplied_dst_color as _;
        self
    }
    #[inline]
    pub fn advanced_blend_correlated_overlap(mut self, advanced_blend_correlated_overlap: bool) -> Self {
        self.0.advanced_blend_correlated_overlap = advanced_blend_correlated_overlap as _;
        self
    }
    #[inline]
    pub fn advanced_blend_all_operations(mut self, advanced_blend_all_operations: bool) -> Self {
        self.0.advanced_blend_all_operations = advanced_blend_all_operations as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceBlendOperationAdvancedPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceBlendOperationAdvancedPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineColorBlendAdvancedStateCreateInfoEXT.html)) · Structure <br/> VkPipelineColorBlendAdvancedStateCreateInfoEXT - Structure specifying parameters that affect advanced blend operations\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineColorBlendStateCreateInfo`]includes a [`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT`] structure,\nthen that structure includes parameters that affect advanced blend\noperations.\n\nThe [`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPipelineColorBlendAdvancedStateCreateInfoEXT {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkBool32             srcPremultiplied;\n    VkBool32             dstPremultiplied;\n    VkBlendOverlapEXT    blendOverlap;\n} VkPipelineColorBlendAdvancedStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::src_premultiplied`] specifies whether the source color of the blend\n  operation is treated as premultiplied.\n\n* [`Self::dst_premultiplied`] specifies whether the destination color of the\n  blend operation is treated as premultiplied.\n\n* [`Self::blend_overlap`] is a [`crate::vk::BlendOverlapEXT`] value specifying how the\n  source and destination sample’s coverage is correlated.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::src_premultiplied`] and[`Self::dst_premultiplied`] are both considered to be [`crate::vk::TRUE`], and[`Self::blend_overlap`] is considered to be[`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`].\n\nValid Usage\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-srcPremultiplied-01424  \n   If the [non-premultiplied\n  source color](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendNonPremultipliedSrcColor) property is not supported, [`Self::src_premultiplied`] **must**be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-dstPremultiplied-01425  \n   If the [non-premultiplied\n  destination color](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendNonPremultipliedDstColor) property is not supported, [`Self::dst_premultiplied`]**must** be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-blendOverlap-01426  \n   If the [correlated overlap](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendCorrelatedOverlap)property is not supported, [`Self::blend_overlap`] **must** be[`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-blendOverlap-parameter  \n  [`Self::blend_overlap`] **must** be a valid [`crate::vk::BlendOverlapEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::BlendOverlapEXT`], [`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineColorBlendAdvancedStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineColorBlendAdvancedStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub src_premultiplied: crate::vk1_0::Bool32,
    pub dst_premultiplied: crate::vk1_0::Bool32,
    pub blend_overlap: crate::extensions::ext_blend_operation_advanced::BlendOverlapEXT,
}
impl PipelineColorBlendAdvancedStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineColorBlendAdvancedStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), src_premultiplied: Default::default(), dst_premultiplied: Default::default(), blend_overlap: Default::default() }
    }
}
impl std::fmt::Debug for PipelineColorBlendAdvancedStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineColorBlendAdvancedStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("src_premultiplied", &(self.src_premultiplied != 0)).field("dst_premultiplied", &(self.dst_premultiplied != 0)).field("blend_overlap", &self.blend_overlap).finish()
    }
}
impl PipelineColorBlendAdvancedStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
        PipelineColorBlendAdvancedStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineColorBlendAdvancedStateCreateInfoEXT.html)) · Builder of [`PipelineColorBlendAdvancedStateCreateInfoEXT`] <br/> VkPipelineColorBlendAdvancedStateCreateInfoEXT - Structure specifying parameters that affect advanced blend operations\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineColorBlendStateCreateInfo`]includes a [`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT`] structure,\nthen that structure includes parameters that affect advanced blend\noperations.\n\nThe [`crate::vk::PipelineColorBlendAdvancedStateCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_blend_operation_advanced\ntypedef struct VkPipelineColorBlendAdvancedStateCreateInfoEXT {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkBool32             srcPremultiplied;\n    VkBool32             dstPremultiplied;\n    VkBlendOverlapEXT    blendOverlap;\n} VkPipelineColorBlendAdvancedStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::src_premultiplied`] specifies whether the source color of the blend\n  operation is treated as premultiplied.\n\n* [`Self::dst_premultiplied`] specifies whether the destination color of the\n  blend operation is treated as premultiplied.\n\n* [`Self::blend_overlap`] is a [`crate::vk::BlendOverlapEXT`] value specifying how the\n  source and destination sample’s coverage is correlated.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::src_premultiplied`] and[`Self::dst_premultiplied`] are both considered to be [`crate::vk::TRUE`], and[`Self::blend_overlap`] is considered to be[`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`].\n\nValid Usage\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-srcPremultiplied-01424  \n   If the [non-premultiplied\n  source color](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendNonPremultipliedSrcColor) property is not supported, [`Self::src_premultiplied`] **must**be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-dstPremultiplied-01425  \n   If the [non-premultiplied\n  destination color](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendNonPremultipliedDstColor) property is not supported, [`Self::dst_premultiplied`]**must** be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-blendOverlap-01426  \n   If the [correlated overlap](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-advancedBlendCorrelatedOverlap)property is not supported, [`Self::blend_overlap`] **must** be[`crate::vk::BlendOverlapEXT::UNCORRELATED_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineColorBlendAdvancedStateCreateInfoEXT-blendOverlap-parameter  \n  [`Self::blend_overlap`] **must** be a valid [`crate::vk::BlendOverlapEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::BlendOverlapEXT`], [`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a>(PipelineColorBlendAdvancedStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
        PipelineColorBlendAdvancedStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn src_premultiplied(mut self, src_premultiplied: bool) -> Self {
        self.0.src_premultiplied = src_premultiplied as _;
        self
    }
    #[inline]
    pub fn dst_premultiplied(mut self, dst_premultiplied: bool) -> Self {
        self.0.dst_premultiplied = dst_premultiplied as _;
        self
    }
    #[inline]
    pub fn blend_overlap(mut self, blend_overlap: crate::extensions::ext_blend_operation_advanced::BlendOverlapEXT) -> Self {
        self.0.blend_overlap = blend_overlap as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineColorBlendAdvancedStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineColorBlendAdvancedStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineColorBlendAdvancedStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
