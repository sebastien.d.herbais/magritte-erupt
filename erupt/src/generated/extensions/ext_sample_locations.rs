#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_SAMPLE_LOCATIONS_SPEC_VERSION")]
pub const EXT_SAMPLE_LOCATIONS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_SAMPLE_LOCATIONS_EXTENSION_NAME")]
pub const EXT_SAMPLE_LOCATIONS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_sample_locations");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_SAMPLE_LOCATIONS_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdSetSampleLocationsEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_MULTISAMPLE_PROPERTIES_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceMultisamplePropertiesEXT");
#[doc = "Provided by [`crate::extensions::ext_sample_locations`]"]
impl crate::vk1_0::DynamicState {
    pub const SAMPLE_LOCATIONS_EXT: Self = Self(1000143000);
}
#[doc = "Provided by [`crate::extensions::ext_sample_locations`]"]
impl crate::vk1_0::ImageCreateFlagBits {
    pub const SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT: Self = Self(4096);
}
#[doc = "Provided by [`crate::extensions::ext_sample_locations`]"]
impl crate::vk1_0::StructureType {
    pub const SAMPLE_LOCATIONS_INFO_EXT: Self = Self(1000143000);
    pub const RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT: Self = Self(1000143001);
    pub const PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT: Self = Self(1000143002);
    pub const PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT: Self = Self(1000143003);
    pub const MULTISAMPLE_PROPERTIES_EXT: Self = Self(1000143004);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetSampleLocationsEXT.html)) · Function <br/> vkCmdSetSampleLocationsEXT - Set the dynamic sample locations state\n[](#_c_specification)C Specification\n----------\n\nThe custom sample locations used for rasterization when[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`]::`sampleLocationsEnable`is [`crate::vk::TRUE`] are specified by the[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`]::`sampleLocationsInfo`property of the bound graphics pipeline, if the pipeline was not created\nwith [`crate::vk::DynamicState::SAMPLE_LOCATIONS_EXT`] enabled.\n\nOtherwise, the sample locations used for rasterization are set by calling[`crate::vk::DeviceLoader::cmd_set_sample_locations_ext`]:\n\n```\n// Provided by VK_EXT_sample_locations\nvoid vkCmdSetSampleLocationsEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkSampleLocationsInfoEXT*             pSampleLocationsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::p_sample_locations_info`] is the sample locations state to set.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetSampleLocationsEXT-sampleLocationsPerPixel-01529  \n   The `sampleLocationsPerPixel` member of [`Self::p_sample_locations_info`]**must** equal the `rasterizationSamples` member of the[`crate::vk::PipelineMultisampleStateCreateInfo`] structure the bound graphics\n  pipeline has been created with\n\n* []() VUID-vkCmdSetSampleLocationsEXT-variableSampleLocations-01530  \n   If[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::FALSE`] then the current render pass **must** have been begun by\n  specifying a [`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure\n  whose `pPostSubpassSampleLocations` member contains an element with\n  a `subpassIndex` matching the current subpass index and the`sampleLocationsInfo` member of that element **must** match the sample\n  locations state pointed to by [`Self::p_sample_locations_info`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetSampleLocationsEXT-pSampleLocationsInfo-parameter  \n  [`Self::p_sample_locations_info`] **must** be a valid pointer to a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetSampleLocationsEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_sample_locations_info: *const crate::extensions::ext_sample_locations::SampleLocationsInfoEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceMultisamplePropertiesEXT.html)) · Function <br/> vkGetPhysicalDeviceMultisamplePropertiesEXT - Report sample count specific multisampling capabilities of a physical device\n[](#_c_specification)C Specification\n----------\n\nTo query additional multisampling capabilities which **may** be supported for a\nspecific sample count, beyond the minimum capabilities described for[Limits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits) above, call:\n\n```\n// Provided by VK_EXT_sample_locations\nvoid vkGetPhysicalDeviceMultisamplePropertiesEXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkSampleCountFlagBits                       samples,\n    VkMultisamplePropertiesEXT*                 pMultisampleProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the\n  additional multisampling capabilities.\n\n* [`Self::samples`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value specifying the\n  sample count to query capabilities for.\n\n* [`Self::p_multisample_properties`] is a pointer to a[`crate::vk::MultisamplePropertiesEXT`] structure in which information about\n  additional multisampling capabilities specific to the sample count is\n  returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-samples-parameter  \n  [`Self::samples`] **must** be a valid [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-pMultisampleProperties-parameter  \n  [`Self::p_multisample_properties`] **must** be a valid pointer to a [`crate::vk::MultisamplePropertiesEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MultisamplePropertiesEXT`], [`crate::vk::PhysicalDevice`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html)\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceMultisamplePropertiesEXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, samples: crate::vk1_0::SampleCountFlagBits, p_multisample_properties: *mut crate::extensions::ext_sample_locations::MultisamplePropertiesEXT) -> ();
impl<'a> crate::ExtendableFromConst<'a, SampleLocationsInfoEXT> for crate::vk1_0::ImageMemoryBarrierBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SampleLocationsInfoEXTBuilder<'_>> for crate::vk1_0::ImageMemoryBarrierBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineSampleLocationsStateCreateInfoEXT> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineSampleLocationsStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, RenderPassSampleLocationsBeginInfoEXT> for crate::vk1_0::RenderPassBeginInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, RenderPassSampleLocationsBeginInfoEXTBuilder<'_>> for crate::vk1_0::RenderPassBeginInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceSampleLocationsPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleLocationEXT.html)) · Structure <br/> VkSampleLocationEXT - Structure specifying the coordinates of a sample location\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SampleLocationEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSampleLocationEXT {\n    float    x;\n    float    y;\n} VkSampleLocationEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::x`] is the horizontal coordinate of the sample’s location.\n\n* [`Self::y`] is the vertical coordinate of the sample’s location.\n[](#_description)Description\n----------\n\nThe domain space of the sample location coordinates has an upper-left origin\nwithin the pixel in framebuffer space.\n\nThe values specified in a [`crate::vk::SampleLocationEXT`] structure are always\nclamped to the implementation-dependent sample location coordinate range[`sampleLocationCoordinateRange`[0],`sampleLocationCoordinateRange`[1]]that **can** be queried using[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SampleLocationsInfoEXT`]\n"]
#[doc(alias = "VkSampleLocationEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SampleLocationEXT {
    pub x: std::os::raw::c_float,
    pub y: std::os::raw::c_float,
}
impl Default for SampleLocationEXT {
    fn default() -> Self {
        Self { x: Default::default(), y: Default::default() }
    }
}
impl std::fmt::Debug for SampleLocationEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SampleLocationEXT").field("x", &self.x).field("y", &self.y).finish()
    }
}
impl SampleLocationEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SampleLocationEXTBuilder<'a> {
        SampleLocationEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleLocationEXT.html)) · Builder of [`SampleLocationEXT`] <br/> VkSampleLocationEXT - Structure specifying the coordinates of a sample location\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SampleLocationEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSampleLocationEXT {\n    float    x;\n    float    y;\n} VkSampleLocationEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::x`] is the horizontal coordinate of the sample’s location.\n\n* [`Self::y`] is the vertical coordinate of the sample’s location.\n[](#_description)Description\n----------\n\nThe domain space of the sample location coordinates has an upper-left origin\nwithin the pixel in framebuffer space.\n\nThe values specified in a [`crate::vk::SampleLocationEXT`] structure are always\nclamped to the implementation-dependent sample location coordinate range[`sampleLocationCoordinateRange`[0],`sampleLocationCoordinateRange`[1]]that **can** be queried using[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SampleLocationsInfoEXT`]\n"]
#[repr(transparent)]
pub struct SampleLocationEXTBuilder<'a>(SampleLocationEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SampleLocationEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SampleLocationEXTBuilder<'a> {
        SampleLocationEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn x(mut self, x: std::os::raw::c_float) -> Self {
        self.0.x = x as _;
        self
    }
    #[inline]
    pub fn y(mut self, y: std::os::raw::c_float) -> Self {
        self.0.y = y as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SampleLocationEXT {
        self.0
    }
}
impl<'a> std::default::Default for SampleLocationEXTBuilder<'a> {
    fn default() -> SampleLocationEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SampleLocationEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SampleLocationEXTBuilder<'a> {
    type Target = SampleLocationEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SampleLocationEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleLocationsInfoEXT.html)) · Structure <br/> VkSampleLocationsInfoEXT - Structure specifying a set of sample locations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SampleLocationsInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSampleLocationsInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkSampleCountFlagBits         sampleLocationsPerPixel;\n    VkExtent2D                    sampleLocationGridSize;\n    uint32_t                      sampleLocationsCount;\n    const VkSampleLocationEXT*    pSampleLocations;\n} VkSampleLocationsInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_locations_per_pixel`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n  specifying the number of sample locations per pixel.\n\n* [`Self::sample_location_grid_size`] is the size of the sample location grid to\n  select custom sample locations for.\n\n* [`Self::sample_locations_count`] is the number of sample locations in[`Self::p_sample_locations`].\n\n* [`Self::p_sample_locations`] is a pointer to an array of[`Self::sample_locations_count`] [`crate::vk::SampleLocationEXT`] structures.\n[](#_description)Description\n----------\n\nThis structure **can** be used either to specify the sample locations to be\nused for rendering or to specify the set of sample locations an image\nsubresource has been last rendered with for the purposes of layout\ntransitions of depth/stencil images created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`].\n\nThe sample locations in [`Self::p_sample_locations`] specify[`Self::sample_locations_per_pixel`] number of sample locations for each pixel in\nthe grid of the size specified in [`Self::sample_location_grid_size`].\nThe sample location for sample i at the pixel grid location(x,y) is taken from [`Self::p_sample_locations`][(x + y ×`sampleLocationGridSize.width`) × [`Self::sample_locations_per_pixel`]+ i].\n\nIf the render pass has a fragment density map, the implementation will\nchoose the sample locations for the fragment and the contents of[`Self::p_sample_locations`] **may** be ignored.\n\nValid Usage\n\n* []() VUID-VkSampleLocationsInfoEXT-sampleLocationsPerPixel-01526  \n  [`Self::sample_locations_per_pixel`] **must** be a bit value that is set in[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::sample_location_sample_counts`]\n\n* []() VUID-VkSampleLocationsInfoEXT-sampleLocationsCount-01527  \n  [`Self::sample_locations_count`] **must** equal[`Self::sample_locations_per_pixel`] ×`sampleLocationGridSize.width` ×`sampleLocationGridSize.height`\n\nValid Usage (Implicit)\n\n* []() VUID-VkSampleLocationsInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SAMPLE_LOCATIONS_INFO_EXT`]\n\n* []() VUID-VkSampleLocationsInfoEXT-pSampleLocations-parameter  \n   If [`Self::sample_locations_count`] is not `0`, [`Self::p_sample_locations`] **must** be a valid pointer to an array of [`Self::sample_locations_count`] [`crate::vk::SampleLocationEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AttachmentSampleLocationsEXT`], [`crate::vk::Extent2D`], [`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::SampleLocationEXT`], [`crate::vk::StructureType`], [`crate::vk::SubpassSampleLocationsEXT`], [`crate::vk::DeviceLoader::cmd_set_sample_locations_ext`]\n"]
#[doc(alias = "VkSampleLocationsInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SampleLocationsInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub sample_locations_per_pixel: crate::vk1_0::SampleCountFlagBits,
    pub sample_location_grid_size: crate::vk1_0::Extent2D,
    pub sample_locations_count: u32,
    pub p_sample_locations: *const crate::extensions::ext_sample_locations::SampleLocationEXT,
}
impl SampleLocationsInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SAMPLE_LOCATIONS_INFO_EXT;
}
impl Default for SampleLocationsInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), sample_locations_per_pixel: Default::default(), sample_location_grid_size: Default::default(), sample_locations_count: Default::default(), p_sample_locations: std::ptr::null() }
    }
}
impl std::fmt::Debug for SampleLocationsInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SampleLocationsInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("sample_locations_per_pixel", &self.sample_locations_per_pixel).field("sample_location_grid_size", &self.sample_location_grid_size).field("sample_locations_count", &self.sample_locations_count).field("p_sample_locations", &self.p_sample_locations).finish()
    }
}
impl SampleLocationsInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SampleLocationsInfoEXTBuilder<'a> {
        SampleLocationsInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleLocationsInfoEXT.html)) · Builder of [`SampleLocationsInfoEXT`] <br/> VkSampleLocationsInfoEXT - Structure specifying a set of sample locations\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SampleLocationsInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSampleLocationsInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkSampleCountFlagBits         sampleLocationsPerPixel;\n    VkExtent2D                    sampleLocationGridSize;\n    uint32_t                      sampleLocationsCount;\n    const VkSampleLocationEXT*    pSampleLocations;\n} VkSampleLocationsInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_locations_per_pixel`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n  specifying the number of sample locations per pixel.\n\n* [`Self::sample_location_grid_size`] is the size of the sample location grid to\n  select custom sample locations for.\n\n* [`Self::sample_locations_count`] is the number of sample locations in[`Self::p_sample_locations`].\n\n* [`Self::p_sample_locations`] is a pointer to an array of[`Self::sample_locations_count`] [`crate::vk::SampleLocationEXT`] structures.\n[](#_description)Description\n----------\n\nThis structure **can** be used either to specify the sample locations to be\nused for rendering or to specify the set of sample locations an image\nsubresource has been last rendered with for the purposes of layout\ntransitions of depth/stencil images created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`].\n\nThe sample locations in [`Self::p_sample_locations`] specify[`Self::sample_locations_per_pixel`] number of sample locations for each pixel in\nthe grid of the size specified in [`Self::sample_location_grid_size`].\nThe sample location for sample i at the pixel grid location(x,y) is taken from [`Self::p_sample_locations`][(x + y ×`sampleLocationGridSize.width`) × [`Self::sample_locations_per_pixel`]+ i].\n\nIf the render pass has a fragment density map, the implementation will\nchoose the sample locations for the fragment and the contents of[`Self::p_sample_locations`] **may** be ignored.\n\nValid Usage\n\n* []() VUID-VkSampleLocationsInfoEXT-sampleLocationsPerPixel-01526  \n  [`Self::sample_locations_per_pixel`] **must** be a bit value that is set in[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::sample_location_sample_counts`]\n\n* []() VUID-VkSampleLocationsInfoEXT-sampleLocationsCount-01527  \n  [`Self::sample_locations_count`] **must** equal[`Self::sample_locations_per_pixel`] ×`sampleLocationGridSize.width` ×`sampleLocationGridSize.height`\n\nValid Usage (Implicit)\n\n* []() VUID-VkSampleLocationsInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SAMPLE_LOCATIONS_INFO_EXT`]\n\n* []() VUID-VkSampleLocationsInfoEXT-pSampleLocations-parameter  \n   If [`Self::sample_locations_count`] is not `0`, [`Self::p_sample_locations`] **must** be a valid pointer to an array of [`Self::sample_locations_count`] [`crate::vk::SampleLocationEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AttachmentSampleLocationsEXT`], [`crate::vk::Extent2D`], [`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::SampleLocationEXT`], [`crate::vk::StructureType`], [`crate::vk::SubpassSampleLocationsEXT`], [`crate::vk::DeviceLoader::cmd_set_sample_locations_ext`]\n"]
#[repr(transparent)]
pub struct SampleLocationsInfoEXTBuilder<'a>(SampleLocationsInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SampleLocationsInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SampleLocationsInfoEXTBuilder<'a> {
        SampleLocationsInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sample_locations_per_pixel(mut self, sample_locations_per_pixel: crate::vk1_0::SampleCountFlagBits) -> Self {
        self.0.sample_locations_per_pixel = sample_locations_per_pixel as _;
        self
    }
    #[inline]
    pub fn sample_location_grid_size(mut self, sample_location_grid_size: crate::vk1_0::Extent2D) -> Self {
        self.0.sample_location_grid_size = sample_location_grid_size as _;
        self
    }
    #[inline]
    pub fn sample_locations(mut self, sample_locations: &'a [crate::extensions::ext_sample_locations::SampleLocationEXTBuilder]) -> Self {
        self.0.p_sample_locations = sample_locations.as_ptr() as _;
        self.0.sample_locations_count = sample_locations.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SampleLocationsInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for SampleLocationsInfoEXTBuilder<'a> {
    fn default() -> SampleLocationsInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SampleLocationsInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SampleLocationsInfoEXTBuilder<'a> {
    type Target = SampleLocationsInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SampleLocationsInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentSampleLocationsEXT.html)) · Structure <br/> VkAttachmentSampleLocationsEXT - Structure specifying the sample locations state to use in the initial layout transition of attachments\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AttachmentSampleLocationsEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkAttachmentSampleLocationsEXT {\n    uint32_t                    attachmentIndex;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkAttachmentSampleLocationsEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::attachment_index`] is the index of the attachment for which the\n  sample locations state is provided.\n\n* [`Self::sample_locations_info`] is the sample locations state to use for the\n  layout transition of the given attachment from the initial layout of the\n  attachment to the image layout specified for the attachment in the first\n  subpass using it.\n[](#_description)Description\n----------\n\nIf the image referenced by the framebuffer attachment at index[`Self::attachment_index`] was not created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] then the\nvalues specified in [`Self::sample_locations_info`] are ignored.\n\nValid Usage\n\n* []() VUID-VkAttachmentSampleLocationsEXT-attachmentIndex-01531  \n  [`Self::attachment_index`] **must** be less than the `attachmentCount`specified in [`crate::vk::RenderPassCreateInfo`] the render pass specified by[`crate::vk::RenderPassBeginInfo::render_pass`] was created with\n\nValid Usage (Implicit)\n\n* []() VUID-VkAttachmentSampleLocationsEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
#[doc(alias = "VkAttachmentSampleLocationsEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AttachmentSampleLocationsEXT {
    pub attachment_index: u32,
    pub sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT,
}
impl Default for AttachmentSampleLocationsEXT {
    fn default() -> Self {
        Self { attachment_index: Default::default(), sample_locations_info: Default::default() }
    }
}
impl std::fmt::Debug for AttachmentSampleLocationsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AttachmentSampleLocationsEXT").field("attachment_index", &self.attachment_index).field("sample_locations_info", &self.sample_locations_info).finish()
    }
}
impl AttachmentSampleLocationsEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> AttachmentSampleLocationsEXTBuilder<'a> {
        AttachmentSampleLocationsEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAttachmentSampleLocationsEXT.html)) · Builder of [`AttachmentSampleLocationsEXT`] <br/> VkAttachmentSampleLocationsEXT - Structure specifying the sample locations state to use in the initial layout transition of attachments\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AttachmentSampleLocationsEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkAttachmentSampleLocationsEXT {\n    uint32_t                    attachmentIndex;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkAttachmentSampleLocationsEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::attachment_index`] is the index of the attachment for which the\n  sample locations state is provided.\n\n* [`Self::sample_locations_info`] is the sample locations state to use for the\n  layout transition of the given attachment from the initial layout of the\n  attachment to the image layout specified for the attachment in the first\n  subpass using it.\n[](#_description)Description\n----------\n\nIf the image referenced by the framebuffer attachment at index[`Self::attachment_index`] was not created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] then the\nvalues specified in [`Self::sample_locations_info`] are ignored.\n\nValid Usage\n\n* []() VUID-VkAttachmentSampleLocationsEXT-attachmentIndex-01531  \n  [`Self::attachment_index`] **must** be less than the `attachmentCount`specified in [`crate::vk::RenderPassCreateInfo`] the render pass specified by[`crate::vk::RenderPassBeginInfo::render_pass`] was created with\n\nValid Usage (Implicit)\n\n* []() VUID-VkAttachmentSampleLocationsEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
#[repr(transparent)]
pub struct AttachmentSampleLocationsEXTBuilder<'a>(AttachmentSampleLocationsEXT, std::marker::PhantomData<&'a ()>);
impl<'a> AttachmentSampleLocationsEXTBuilder<'a> {
    #[inline]
    pub fn new() -> AttachmentSampleLocationsEXTBuilder<'a> {
        AttachmentSampleLocationsEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attachment_index(mut self, attachment_index: u32) -> Self {
        self.0.attachment_index = attachment_index as _;
        self
    }
    #[inline]
    pub fn sample_locations_info(mut self, sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT) -> Self {
        self.0.sample_locations_info = sample_locations_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AttachmentSampleLocationsEXT {
        self.0
    }
}
impl<'a> std::default::Default for AttachmentSampleLocationsEXTBuilder<'a> {
    fn default() -> AttachmentSampleLocationsEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AttachmentSampleLocationsEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AttachmentSampleLocationsEXTBuilder<'a> {
    type Target = AttachmentSampleLocationsEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AttachmentSampleLocationsEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassSampleLocationsEXT.html)) · Structure <br/> VkSubpassSampleLocationsEXT - Structure specifying the sample locations state to use for layout transitions of attachments performed after a given subpass\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SubpassSampleLocationsEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSubpassSampleLocationsEXT {\n    uint32_t                    subpassIndex;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkSubpassSampleLocationsEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::subpass_index`] is the index of the subpass for which the sample\n  locations state is provided.\n\n* [`Self::sample_locations_info`] is the sample locations state to use for the\n  layout transition of the depth/stencil attachment away from the image\n  layout the attachment is used with in the subpass specified in[`Self::subpass_index`].\n[](#_description)Description\n----------\n\nIf the image referenced by the depth/stencil attachment used in the subpass\nidentified by [`Self::subpass_index`] was not created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] or if the\nsubpass does not use a depth/stencil attachment, and[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::TRUE`] then the values specified in [`Self::sample_locations_info`] are\nignored.\n\nValid Usage\n\n* []() VUID-VkSubpassSampleLocationsEXT-subpassIndex-01532  \n  [`Self::subpass_index`] **must** be less than the `subpassCount` specified\n  in [`crate::vk::RenderPassCreateInfo`] the render pass specified by[`crate::vk::RenderPassBeginInfo::render_pass`] was created with\n\nValid Usage (Implicit)\n\n* []() VUID-VkSubpassSampleLocationsEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
#[doc(alias = "VkSubpassSampleLocationsEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SubpassSampleLocationsEXT {
    pub subpass_index: u32,
    pub sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT,
}
impl Default for SubpassSampleLocationsEXT {
    fn default() -> Self {
        Self { subpass_index: Default::default(), sample_locations_info: Default::default() }
    }
}
impl std::fmt::Debug for SubpassSampleLocationsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SubpassSampleLocationsEXT").field("subpass_index", &self.subpass_index).field("sample_locations_info", &self.sample_locations_info).finish()
    }
}
impl SubpassSampleLocationsEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SubpassSampleLocationsEXTBuilder<'a> {
        SubpassSampleLocationsEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassSampleLocationsEXT.html)) · Builder of [`SubpassSampleLocationsEXT`] <br/> VkSubpassSampleLocationsEXT - Structure specifying the sample locations state to use for layout transitions of attachments performed after a given subpass\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SubpassSampleLocationsEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkSubpassSampleLocationsEXT {\n    uint32_t                    subpassIndex;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkSubpassSampleLocationsEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::subpass_index`] is the index of the subpass for which the sample\n  locations state is provided.\n\n* [`Self::sample_locations_info`] is the sample locations state to use for the\n  layout transition of the depth/stencil attachment away from the image\n  layout the attachment is used with in the subpass specified in[`Self::subpass_index`].\n[](#_description)Description\n----------\n\nIf the image referenced by the depth/stencil attachment used in the subpass\nidentified by [`Self::subpass_index`] was not created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] or if the\nsubpass does not use a depth/stencil attachment, and[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::TRUE`] then the values specified in [`Self::sample_locations_info`] are\nignored.\n\nValid Usage\n\n* []() VUID-VkSubpassSampleLocationsEXT-subpassIndex-01532  \n  [`Self::subpass_index`] **must** be less than the `subpassCount` specified\n  in [`crate::vk::RenderPassCreateInfo`] the render pass specified by[`crate::vk::RenderPassBeginInfo::render_pass`] was created with\n\nValid Usage (Implicit)\n\n* []() VUID-VkSubpassSampleLocationsEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
#[repr(transparent)]
pub struct SubpassSampleLocationsEXTBuilder<'a>(SubpassSampleLocationsEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SubpassSampleLocationsEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SubpassSampleLocationsEXTBuilder<'a> {
        SubpassSampleLocationsEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn subpass_index(mut self, subpass_index: u32) -> Self {
        self.0.subpass_index = subpass_index as _;
        self
    }
    #[inline]
    pub fn sample_locations_info(mut self, sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT) -> Self {
        self.0.sample_locations_info = sample_locations_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SubpassSampleLocationsEXT {
        self.0
    }
}
impl<'a> std::default::Default for SubpassSampleLocationsEXTBuilder<'a> {
    fn default() -> SubpassSampleLocationsEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SubpassSampleLocationsEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SubpassSampleLocationsEXTBuilder<'a> {
    type Target = SubpassSampleLocationsEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SubpassSampleLocationsEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassSampleLocationsBeginInfoEXT.html)) · Structure <br/> VkRenderPassSampleLocationsBeginInfoEXT - Structure specifying sample locations to use for the layout transition of custom sample locations compatible depth/stencil attachments\n[](#_c_specification)C Specification\n----------\n\nThe image layout of the depth aspect of a depth/stencil attachment referring\nto an image created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] is dependent\non the last sample locations used to render to the image subresource, thus\npreserving the contents of such depth/stencil attachments across subpass\nboundaries requires the application to specify these sample locations\nwhenever a layout transition of the attachment **may** occur.\nThis information **can** be provided by adding a[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::RenderPassBeginInfo`].\n\nThe [`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkRenderPassSampleLocationsBeginInfoEXT {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    uint32_t                                 attachmentInitialSampleLocationsCount;\n    const VkAttachmentSampleLocationsEXT*    pAttachmentInitialSampleLocations;\n    uint32_t                                 postSubpassSampleLocationsCount;\n    const VkSubpassSampleLocationsEXT*       pPostSubpassSampleLocations;\n} VkRenderPassSampleLocationsBeginInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::attachment_initial_sample_locations_count`] is the number of elements in\n  the [`Self::p_attachment_initial_sample_locations`] array.\n\n* [`Self::p_attachment_initial_sample_locations`] is a pointer to an array of[`Self::attachment_initial_sample_locations_count`][`crate::vk::AttachmentSampleLocationsEXT`] structures specifying the\n  attachment indices and their corresponding sample location state.\n  Each element of [`Self::p_attachment_initial_sample_locations`] **can** specify the\n  sample location state to use in the automatic layout transition\n  performed to transition a depth/stencil attachment from the initial\n  layout of the attachment to the image layout specified for the\n  attachment in the first subpass using it.\n\n* [`Self::post_subpass_sample_locations_count`] is the number of elements in the[`Self::p_post_subpass_sample_locations`] array.\n\n* [`Self::p_post_subpass_sample_locations`] is a pointer to an array of[`Self::post_subpass_sample_locations_count`] [`crate::vk::SubpassSampleLocationsEXT`]structures specifying the subpass indices and their corresponding sample\n  location state.\n  Each element of [`Self::p_post_subpass_sample_locations`] **can** specify the\n  sample location state to use in the automatic layout transition\n  performed to transition the depth/stencil attachment used by the\n  specified subpass to the image layout specified in a dependent subpass\n  or to the final layout of the attachment in case the specified subpass\n  is the last subpass using that attachment.\n  In addition, if[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::FALSE`], each element of [`Self::p_post_subpass_sample_locations`]**must** specify the sample location state that matches the sample\n  locations used by all pipelines that will be bound to a command buffer\n  during the specified subpass.\n  If `variableSampleLocations` is [`crate::vk::TRUE`], the sample locations\n  used for rasterization do not depend on[`Self::p_post_subpass_sample_locations`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT`]\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-pAttachmentInitialSampleLocations-parameter  \n   If [`Self::attachment_initial_sample_locations_count`] is not `0`, [`Self::p_attachment_initial_sample_locations`] **must** be a valid pointer to an array of [`Self::attachment_initial_sample_locations_count`] valid [`crate::vk::AttachmentSampleLocationsEXT`] structures\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-pPostSubpassSampleLocations-parameter  \n   If [`Self::post_subpass_sample_locations_count`] is not `0`, [`Self::p_post_subpass_sample_locations`] **must** be a valid pointer to an array of [`Self::post_subpass_sample_locations_count`] valid [`crate::vk::SubpassSampleLocationsEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AttachmentSampleLocationsEXT`], [`crate::vk::StructureType`], [`crate::vk::SubpassSampleLocationsEXT`]\n"]
#[doc(alias = "VkRenderPassSampleLocationsBeginInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RenderPassSampleLocationsBeginInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub attachment_initial_sample_locations_count: u32,
    pub p_attachment_initial_sample_locations: *const crate::extensions::ext_sample_locations::AttachmentSampleLocationsEXT,
    pub post_subpass_sample_locations_count: u32,
    pub p_post_subpass_sample_locations: *const crate::extensions::ext_sample_locations::SubpassSampleLocationsEXT,
}
impl RenderPassSampleLocationsBeginInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT;
}
impl Default for RenderPassSampleLocationsBeginInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), attachment_initial_sample_locations_count: Default::default(), p_attachment_initial_sample_locations: std::ptr::null(), post_subpass_sample_locations_count: Default::default(), p_post_subpass_sample_locations: std::ptr::null() }
    }
}
impl std::fmt::Debug for RenderPassSampleLocationsBeginInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RenderPassSampleLocationsBeginInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("attachment_initial_sample_locations_count", &self.attachment_initial_sample_locations_count).field("p_attachment_initial_sample_locations", &self.p_attachment_initial_sample_locations).field("post_subpass_sample_locations_count", &self.post_subpass_sample_locations_count).field("p_post_subpass_sample_locations", &self.p_post_subpass_sample_locations).finish()
    }
}
impl RenderPassSampleLocationsBeginInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
        RenderPassSampleLocationsBeginInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassSampleLocationsBeginInfoEXT.html)) · Builder of [`RenderPassSampleLocationsBeginInfoEXT`] <br/> VkRenderPassSampleLocationsBeginInfoEXT - Structure specifying sample locations to use for the layout transition of custom sample locations compatible depth/stencil attachments\n[](#_c_specification)C Specification\n----------\n\nThe image layout of the depth aspect of a depth/stencil attachment referring\nto an image created with[`crate::vk::ImageCreateFlagBits::SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_EXT`] is dependent\non the last sample locations used to render to the image subresource, thus\npreserving the contents of such depth/stencil attachments across subpass\nboundaries requires the application to specify these sample locations\nwhenever a layout transition of the attachment **may** occur.\nThis information **can** be provided by adding a[`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::RenderPassBeginInfo`].\n\nThe [`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkRenderPassSampleLocationsBeginInfoEXT {\n    VkStructureType                          sType;\n    const void*                              pNext;\n    uint32_t                                 attachmentInitialSampleLocationsCount;\n    const VkAttachmentSampleLocationsEXT*    pAttachmentInitialSampleLocations;\n    uint32_t                                 postSubpassSampleLocationsCount;\n    const VkSubpassSampleLocationsEXT*       pPostSubpassSampleLocations;\n} VkRenderPassSampleLocationsBeginInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::attachment_initial_sample_locations_count`] is the number of elements in\n  the [`Self::p_attachment_initial_sample_locations`] array.\n\n* [`Self::p_attachment_initial_sample_locations`] is a pointer to an array of[`Self::attachment_initial_sample_locations_count`][`crate::vk::AttachmentSampleLocationsEXT`] structures specifying the\n  attachment indices and their corresponding sample location state.\n  Each element of [`Self::p_attachment_initial_sample_locations`] **can** specify the\n  sample location state to use in the automatic layout transition\n  performed to transition a depth/stencil attachment from the initial\n  layout of the attachment to the image layout specified for the\n  attachment in the first subpass using it.\n\n* [`Self::post_subpass_sample_locations_count`] is the number of elements in the[`Self::p_post_subpass_sample_locations`] array.\n\n* [`Self::p_post_subpass_sample_locations`] is a pointer to an array of[`Self::post_subpass_sample_locations_count`] [`crate::vk::SubpassSampleLocationsEXT`]structures specifying the subpass indices and their corresponding sample\n  location state.\n  Each element of [`Self::p_post_subpass_sample_locations`] **can** specify the\n  sample location state to use in the automatic layout transition\n  performed to transition the depth/stencil attachment used by the\n  specified subpass to the image layout specified in a dependent subpass\n  or to the final layout of the attachment in case the specified subpass\n  is the last subpass using that attachment.\n  In addition, if[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::FALSE`], each element of [`Self::p_post_subpass_sample_locations`]**must** specify the sample location state that matches the sample\n  locations used by all pipelines that will be bound to a command buffer\n  during the specified subpass.\n  If `variableSampleLocations` is [`crate::vk::TRUE`], the sample locations\n  used for rasterization do not depend on[`Self::p_post_subpass_sample_locations`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT`]\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-pAttachmentInitialSampleLocations-parameter  \n   If [`Self::attachment_initial_sample_locations_count`] is not `0`, [`Self::p_attachment_initial_sample_locations`] **must** be a valid pointer to an array of [`Self::attachment_initial_sample_locations_count`] valid [`crate::vk::AttachmentSampleLocationsEXT`] structures\n\n* []() VUID-VkRenderPassSampleLocationsBeginInfoEXT-pPostSubpassSampleLocations-parameter  \n   If [`Self::post_subpass_sample_locations_count`] is not `0`, [`Self::p_post_subpass_sample_locations`] **must** be a valid pointer to an array of [`Self::post_subpass_sample_locations_count`] valid [`crate::vk::SubpassSampleLocationsEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AttachmentSampleLocationsEXT`], [`crate::vk::StructureType`], [`crate::vk::SubpassSampleLocationsEXT`]\n"]
#[repr(transparent)]
pub struct RenderPassSampleLocationsBeginInfoEXTBuilder<'a>(RenderPassSampleLocationsBeginInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
        RenderPassSampleLocationsBeginInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attachment_initial_sample_locations(mut self, attachment_initial_sample_locations: &'a [crate::extensions::ext_sample_locations::AttachmentSampleLocationsEXTBuilder]) -> Self {
        self.0.p_attachment_initial_sample_locations = attachment_initial_sample_locations.as_ptr() as _;
        self.0.attachment_initial_sample_locations_count = attachment_initial_sample_locations.len() as _;
        self
    }
    #[inline]
    pub fn post_subpass_sample_locations(mut self, post_subpass_sample_locations: &'a [crate::extensions::ext_sample_locations::SubpassSampleLocationsEXTBuilder]) -> Self {
        self.0.p_post_subpass_sample_locations = post_subpass_sample_locations.as_ptr() as _;
        self.0.post_subpass_sample_locations_count = post_subpass_sample_locations.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RenderPassSampleLocationsBeginInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
    fn default() -> RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
    type Target = RenderPassSampleLocationsBeginInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RenderPassSampleLocationsBeginInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineSampleLocationsStateCreateInfoEXT.html)) · Structure <br/> VkPipelineSampleLocationsStateCreateInfoEXT - Structure specifying sample locations for a pipeline\n[](#_c_specification)C Specification\n----------\n\nApplications **can** also control the sample locations used for rasterization.\n\nIf the [`Self::p_next`] chain of the [`crate::vk::PipelineMultisampleStateCreateInfo`]structure specified at pipeline creation time includes a[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`] structure, then that\nstructure controls the sample locations used when rasterizing primitives\nwith the pipeline.\n\nThe [`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkPipelineSampleLocationsStateCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkBool32                    sampleLocationsEnable;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkPipelineSampleLocationsStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_locations_enable`] controls whether custom sample locations are\n  used.\n  If [`Self::sample_locations_enable`] is [`crate::vk::FALSE`], the default sample\n  locations are used and the values specified in [`Self::sample_locations_info`]are ignored.\n\n* [`Self::sample_locations_info`] is the sample locations to use during\n  rasterization if [`Self::sample_locations_enable`] is [`crate::vk::TRUE`] and the\n  graphics pipeline is not created with[`crate::vk::DynamicState::SAMPLE_LOCATIONS_EXT`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineSampleLocationsStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineSampleLocationsStateCreateInfoEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::SampleLocationsInfoEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineSampleLocationsStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineSampleLocationsStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub sample_locations_enable: crate::vk1_0::Bool32,
    pub sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT,
}
impl PipelineSampleLocationsStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineSampleLocationsStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), sample_locations_enable: Default::default(), sample_locations_info: Default::default() }
    }
}
impl std::fmt::Debug for PipelineSampleLocationsStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineSampleLocationsStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("sample_locations_enable", &(self.sample_locations_enable != 0)).field("sample_locations_info", &self.sample_locations_info).finish()
    }
}
impl PipelineSampleLocationsStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
        PipelineSampleLocationsStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineSampleLocationsStateCreateInfoEXT.html)) · Builder of [`PipelineSampleLocationsStateCreateInfoEXT`] <br/> VkPipelineSampleLocationsStateCreateInfoEXT - Structure specifying sample locations for a pipeline\n[](#_c_specification)C Specification\n----------\n\nApplications **can** also control the sample locations used for rasterization.\n\nIf the [`Self::p_next`] chain of the [`crate::vk::PipelineMultisampleStateCreateInfo`]structure specified at pipeline creation time includes a[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`] structure, then that\nstructure controls the sample locations used when rasterizing primitives\nwith the pipeline.\n\nThe [`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkPipelineSampleLocationsStateCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkBool32                    sampleLocationsEnable;\n    VkSampleLocationsInfoEXT    sampleLocationsInfo;\n} VkPipelineSampleLocationsStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_locations_enable`] controls whether custom sample locations are\n  used.\n  If [`Self::sample_locations_enable`] is [`crate::vk::FALSE`], the default sample\n  locations are used and the values specified in [`Self::sample_locations_info`]are ignored.\n\n* [`Self::sample_locations_info`] is the sample locations to use during\n  rasterization if [`Self::sample_locations_enable`] is [`crate::vk::TRUE`] and the\n  graphics pipeline is not created with[`crate::vk::DynamicState::SAMPLE_LOCATIONS_EXT`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineSampleLocationsStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineSampleLocationsStateCreateInfoEXT-sampleLocationsInfo-parameter  \n  [`Self::sample_locations_info`] **must** be a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::SampleLocationsInfoEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineSampleLocationsStateCreateInfoEXTBuilder<'a>(PipelineSampleLocationsStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
        PipelineSampleLocationsStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sample_locations_enable(mut self, sample_locations_enable: bool) -> Self {
        self.0.sample_locations_enable = sample_locations_enable as _;
        self
    }
    #[inline]
    pub fn sample_locations_info(mut self, sample_locations_info: crate::extensions::ext_sample_locations::SampleLocationsInfoEXT) -> Self {
        self.0.sample_locations_info = sample_locations_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineSampleLocationsStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineSampleLocationsStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineSampleLocationsStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSampleLocationsPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceSampleLocationsPropertiesEXT - Structure describing sample location limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkPhysicalDeviceSampleLocationsPropertiesEXT {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkSampleCountFlags    sampleLocationSampleCounts;\n    VkExtent2D            maxSampleLocationGridSize;\n    float                 sampleLocationCoordinateRange[2];\n    uint32_t              sampleLocationSubPixelBits;\n    VkBool32              variableSampleLocations;\n} VkPhysicalDeviceSampleLocationsPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::sample_location_sample_counts`]is a bitmask of [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) indicating the sample counts\n  supporting custom sample locations.\n\n* []() [`Self::max_sample_location_grid_size`] is\n  the maximum size of the pixel grid in which sample locations **can** vary\n  that is supported for all sample counts in[`Self::sample_location_sample_counts`].\n\n* []()[`Self::sample_location_coordinate_range`][2] is the range of supported sample\n  location coordinates.\n\n* []() [`Self::sample_location_sub_pixel_bits`]is the number of bits of subpixel precision for sample locations.\n\n* []() [`Self::variable_sample_locations`]specifies whether the sample locations used by all pipelines that will\n  be bound to a command buffer during a subpass **must** match.\n  If set to [`crate::vk::TRUE`], the implementation supports variable sample\n  locations in a subpass.\n  If set to [`crate::vk::FALSE`], then the sample locations **must** stay constant\n  in each subpass.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceSampleLocationsPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::Extent2D`], [`crate::vk::SampleCountFlagBits`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceSampleLocationsPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceSampleLocationsPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub sample_location_sample_counts: crate::vk1_0::SampleCountFlags,
    pub max_sample_location_grid_size: crate::vk1_0::Extent2D,
    pub sample_location_coordinate_range: [std::os::raw::c_float; 2],
    pub sample_location_sub_pixel_bits: u32,
    pub variable_sample_locations: crate::vk1_0::Bool32,
}
impl PhysicalDeviceSampleLocationsPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceSampleLocationsPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), sample_location_sample_counts: Default::default(), max_sample_location_grid_size: Default::default(), sample_location_coordinate_range: unsafe { std::mem::zeroed() }, sample_location_sub_pixel_bits: Default::default(), variable_sample_locations: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceSampleLocationsPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceSampleLocationsPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("sample_location_sample_counts", &self.sample_location_sample_counts).field("max_sample_location_grid_size", &self.max_sample_location_grid_size).field("sample_location_coordinate_range", &self.sample_location_coordinate_range).field("sample_location_sub_pixel_bits", &self.sample_location_sub_pixel_bits).field("variable_sample_locations", &(self.variable_sample_locations != 0)).finish()
    }
}
impl PhysicalDeviceSampleLocationsPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
        PhysicalDeviceSampleLocationsPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSampleLocationsPropertiesEXT.html)) · Builder of [`PhysicalDeviceSampleLocationsPropertiesEXT`] <br/> VkPhysicalDeviceSampleLocationsPropertiesEXT - Structure describing sample location limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkPhysicalDeviceSampleLocationsPropertiesEXT {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkSampleCountFlags    sampleLocationSampleCounts;\n    VkExtent2D            maxSampleLocationGridSize;\n    float                 sampleLocationCoordinateRange[2];\n    uint32_t              sampleLocationSubPixelBits;\n    VkBool32              variableSampleLocations;\n} VkPhysicalDeviceSampleLocationsPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::sample_location_sample_counts`]is a bitmask of [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) indicating the sample counts\n  supporting custom sample locations.\n\n* []() [`Self::max_sample_location_grid_size`] is\n  the maximum size of the pixel grid in which sample locations **can** vary\n  that is supported for all sample counts in[`Self::sample_location_sample_counts`].\n\n* []()[`Self::sample_location_coordinate_range`][2] is the range of supported sample\n  location coordinates.\n\n* []() [`Self::sample_location_sub_pixel_bits`]is the number of bits of subpixel precision for sample locations.\n\n* []() [`Self::variable_sample_locations`]specifies whether the sample locations used by all pipelines that will\n  be bound to a command buffer during a subpass **must** match.\n  If set to [`crate::vk::TRUE`], the implementation supports variable sample\n  locations in a subpass.\n  If set to [`crate::vk::FALSE`], then the sample locations **must** stay constant\n  in each subpass.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceSampleLocationsPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::Extent2D`], [`crate::vk::SampleCountFlagBits`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a>(PhysicalDeviceSampleLocationsPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
        PhysicalDeviceSampleLocationsPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sample_location_sample_counts(mut self, sample_location_sample_counts: crate::vk1_0::SampleCountFlags) -> Self {
        self.0.sample_location_sample_counts = sample_location_sample_counts as _;
        self
    }
    #[inline]
    pub fn max_sample_location_grid_size(mut self, max_sample_location_grid_size: crate::vk1_0::Extent2D) -> Self {
        self.0.max_sample_location_grid_size = max_sample_location_grid_size as _;
        self
    }
    #[inline]
    pub fn sample_location_coordinate_range(mut self, sample_location_coordinate_range: [std::os::raw::c_float; 2]) -> Self {
        self.0.sample_location_coordinate_range = sample_location_coordinate_range as _;
        self
    }
    #[inline]
    pub fn sample_location_sub_pixel_bits(mut self, sample_location_sub_pixel_bits: u32) -> Self {
        self.0.sample_location_sub_pixel_bits = sample_location_sub_pixel_bits as _;
        self
    }
    #[inline]
    pub fn variable_sample_locations(mut self, variable_sample_locations: bool) -> Self {
        self.0.variable_sample_locations = variable_sample_locations as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceSampleLocationsPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceSampleLocationsPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceSampleLocationsPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMultisamplePropertiesEXT.html)) · Structure <br/> VkMultisamplePropertiesEXT - Structure returning information about sample count specific additional multisampling capabilities\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MultisamplePropertiesEXT`] structure is defined as\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkMultisamplePropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkExtent2D         maxSampleLocationGridSize;\n} VkMultisamplePropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_sample_location_grid_size`] is the maximum size of the pixel grid in\n  which sample locations **can** vary.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMultisamplePropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MULTISAMPLE_PROPERTIES_EXT`]\n\n* []() VUID-VkMultisamplePropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_multisample_properties_ext`]\n"]
#[doc(alias = "VkMultisamplePropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MultisamplePropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_sample_location_grid_size: crate::vk1_0::Extent2D,
}
impl MultisamplePropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MULTISAMPLE_PROPERTIES_EXT;
}
impl Default for MultisamplePropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_sample_location_grid_size: Default::default() }
    }
}
impl std::fmt::Debug for MultisamplePropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MultisamplePropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_sample_location_grid_size", &self.max_sample_location_grid_size).finish()
    }
}
impl MultisamplePropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> MultisamplePropertiesEXTBuilder<'a> {
        MultisamplePropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMultisamplePropertiesEXT.html)) · Builder of [`MultisamplePropertiesEXT`] <br/> VkMultisamplePropertiesEXT - Structure returning information about sample count specific additional multisampling capabilities\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MultisamplePropertiesEXT`] structure is defined as\n\n```\n// Provided by VK_EXT_sample_locations\ntypedef struct VkMultisamplePropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkExtent2D         maxSampleLocationGridSize;\n} VkMultisamplePropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_sample_location_grid_size`] is the maximum size of the pixel grid in\n  which sample locations **can** vary.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMultisamplePropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MULTISAMPLE_PROPERTIES_EXT`]\n\n* []() VUID-VkMultisamplePropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_multisample_properties_ext`]\n"]
#[repr(transparent)]
pub struct MultisamplePropertiesEXTBuilder<'a>(MultisamplePropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> MultisamplePropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> MultisamplePropertiesEXTBuilder<'a> {
        MultisamplePropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_sample_location_grid_size(mut self, max_sample_location_grid_size: crate::vk1_0::Extent2D) -> Self {
        self.0.max_sample_location_grid_size = max_sample_location_grid_size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MultisamplePropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for MultisamplePropertiesEXTBuilder<'a> {
    fn default() -> MultisamplePropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MultisamplePropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MultisamplePropertiesEXTBuilder<'a> {
    type Target = MultisamplePropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MultisamplePropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromConst<'a, SampleLocationsInfoEXT> for crate::extensions::khr_synchronization2::ImageMemoryBarrier2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SampleLocationsInfoEXTBuilder<'_>> for crate::extensions::khr_synchronization2::ImageMemoryBarrier2KHRBuilder<'a> {}
#[doc = "Provided by [`crate::extensions::ext_sample_locations`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetSampleLocationsEXT.html)) · Function <br/> vkCmdSetSampleLocationsEXT - Set the dynamic sample locations state\n[](#_c_specification)C Specification\n----------\n\nThe custom sample locations used for rasterization when[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`]::`sampleLocationsEnable`is [`crate::vk::TRUE`] are specified by the[`crate::vk::PipelineSampleLocationsStateCreateInfoEXT`]::`sampleLocationsInfo`property of the bound graphics pipeline, if the pipeline was not created\nwith [`crate::vk::DynamicState::SAMPLE_LOCATIONS_EXT`] enabled.\n\nOtherwise, the sample locations used for rasterization are set by calling[`crate::vk::DeviceLoader::cmd_set_sample_locations_ext`]:\n\n```\n// Provided by VK_EXT_sample_locations\nvoid vkCmdSetSampleLocationsEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkSampleLocationsInfoEXT*             pSampleLocationsInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::p_sample_locations_info`] is the sample locations state to set.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetSampleLocationsEXT-sampleLocationsPerPixel-01529  \n   The `sampleLocationsPerPixel` member of [`Self::p_sample_locations_info`]**must** equal the `rasterizationSamples` member of the[`crate::vk::PipelineMultisampleStateCreateInfo`] structure the bound graphics\n  pipeline has been created with\n\n* []() VUID-vkCmdSetSampleLocationsEXT-variableSampleLocations-01530  \n   If[`crate::vk::PhysicalDeviceSampleLocationsPropertiesEXT::variable_sample_locations`]is [`crate::vk::FALSE`] then the current render pass **must** have been begun by\n  specifying a [`crate::vk::RenderPassSampleLocationsBeginInfoEXT`] structure\n  whose `pPostSubpassSampleLocations` member contains an element with\n  a `subpassIndex` matching the current subpass index and the`sampleLocationsInfo` member of that element **must** match the sample\n  locations state pointed to by [`Self::p_sample_locations_info`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetSampleLocationsEXT-pSampleLocationsInfo-parameter  \n  [`Self::p_sample_locations_info`] **must** be a valid pointer to a valid [`crate::vk::SampleLocationsInfoEXT`] structure\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetSampleLocationsEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::SampleLocationsInfoEXT`]\n"]
    #[doc(alias = "vkCmdSetSampleLocationsEXT")]
    pub unsafe fn cmd_set_sample_locations_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, sample_locations_info: &crate::extensions::ext_sample_locations::SampleLocationsInfoEXT) -> () {
        let _function = self.cmd_set_sample_locations_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, sample_locations_info as _);
        ()
    }
}
#[doc = "Provided by [`crate::extensions::ext_sample_locations`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceMultisamplePropertiesEXT.html)) · Function <br/> vkGetPhysicalDeviceMultisamplePropertiesEXT - Report sample count specific multisampling capabilities of a physical device\n[](#_c_specification)C Specification\n----------\n\nTo query additional multisampling capabilities which **may** be supported for a\nspecific sample count, beyond the minimum capabilities described for[Limits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits) above, call:\n\n```\n// Provided by VK_EXT_sample_locations\nvoid vkGetPhysicalDeviceMultisamplePropertiesEXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkSampleCountFlagBits                       samples,\n    VkMultisamplePropertiesEXT*                 pMultisampleProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the\n  additional multisampling capabilities.\n\n* [`Self::samples`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value specifying the\n  sample count to query capabilities for.\n\n* [`Self::p_multisample_properties`] is a pointer to a[`crate::vk::MultisamplePropertiesEXT`] structure in which information about\n  additional multisampling capabilities specific to the sample count is\n  returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-samples-parameter  \n  [`Self::samples`] **must** be a valid [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) value\n\n* []() VUID-vkGetPhysicalDeviceMultisamplePropertiesEXT-pMultisampleProperties-parameter  \n  [`Self::p_multisample_properties`] **must** be a valid pointer to a [`crate::vk::MultisamplePropertiesEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MultisamplePropertiesEXT`], [`crate::vk::PhysicalDevice`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html)\n"]
    #[doc(alias = "vkGetPhysicalDeviceMultisamplePropertiesEXT")]
    pub unsafe fn get_physical_device_multisample_properties_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, samples: crate::vk1_0::SampleCountFlagBits, multisample_properties: Option<crate::extensions::ext_sample_locations::MultisamplePropertiesEXT>) -> crate::extensions::ext_sample_locations::MultisamplePropertiesEXT {
        let _function = self.get_physical_device_multisample_properties_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut multisample_properties = match multisample_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, samples as _, &mut multisample_properties);
        {
            multisample_properties.p_next = std::ptr::null_mut() as _;
            multisample_properties
        }
    }
}
