#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_SHADER_DEMOTE_TO_HELPER_INVOCATION_SPEC_VERSION")]
pub const EXT_SHADER_DEMOTE_TO_HELPER_INVOCATION_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_SHADER_DEMOTE_TO_HELPER_INVOCATION_EXTENSION_NAME")]
pub const EXT_SHADER_DEMOTE_TO_HELPER_INVOCATION_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_shader_demote_to_helper_invocation");
#[doc = "Provided by [`crate::extensions::ext_shader_demote_to_helper_invocation`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT: Self = Self(1000276000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT - Structure describing the shader demote to helper invocations features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`]structure is defined as:\n\n```\n// Provided by VK_EXT_shader_demote_to_helper_invocation\ntypedef struct VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderDemoteToHelperInvocation;\n} VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::shader_demote_to_helper_invocation`] indicates whether the\n  implementation supports the SPIR-V `DemoteToHelperInvocationEXT`capability.\n\nIf the [`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_demote_to_helper_invocation: crate::vk1_0::Bool32,
}
impl PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT;
}
impl Default for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shader_demote_to_helper_invocation: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shader_demote_to_helper_invocation", &(self.shader_demote_to_helper_invocation != 0)).finish()
    }
}
impl PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
        PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT.html)) · Builder of [`PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`] <br/> VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT - Structure describing the shader demote to helper invocations features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`]structure is defined as:\n\n```\n// Provided by VK_EXT_shader_demote_to_helper_invocation\ntypedef struct VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderDemoteToHelperInvocation;\n} VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::shader_demote_to_helper_invocation`] indicates whether the\n  implementation supports the SPIR-V `DemoteToHelperInvocationEXT`capability.\n\nIf the [`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a>(PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
        PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_demote_to_helper_invocation(mut self, shader_demote_to_helper_invocation: bool) -> Self {
        self.0.shader_demote_to_helper_invocation = shader_demote_to_helper_invocation as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
