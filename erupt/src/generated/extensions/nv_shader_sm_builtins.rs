#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADER_SM_BUILTINS_SPEC_VERSION")]
pub const NV_SHADER_SM_BUILTINS_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADER_SM_BUILTINS_EXTENSION_NAME")]
pub const NV_SHADER_SM_BUILTINS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_shader_sm_builtins");
#[doc = "Provided by [`crate::extensions::nv_shader_sm_builtins`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV: Self = Self(1000154000);
    pub const PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV: Self = Self(1000154001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderSMBuiltinsFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderSMBuiltinsFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderSMBuiltinsPropertiesNV> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSMBuiltinsPropertiesNV.html)) · Structure <br/> VkPhysicalDeviceShaderSMBuiltinsPropertiesNV - Structure describing shader SM Builtins properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderSMBuiltinsPropertiesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shader_sm_builtins\ntypedef struct VkPhysicalDeviceShaderSMBuiltinsPropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           shaderSMCount;\n    uint32_t           shaderWarpsPerSM;\n} VkPhysicalDeviceShaderSMBuiltinsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_sm_count`] is the number of SMs on the\n  device.\n\n* []() [`Self::shader_warps_per_sm`] is the maximum number\n  of simultaneously executing warps on an SM.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceShaderSMBuiltinsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderSMBuiltinsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShaderSMBuiltinsPropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_sm_count: u32,
    pub shader_warps_per_sm: u32,
}
impl PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV;
}
impl Default for PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shader_sm_count: Default::default(), shader_warps_per_sm: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShaderSMBuiltinsPropertiesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shader_sm_count", &self.shader_sm_count).field("shader_warps_per_sm", &self.shader_warps_per_sm).finish()
    }
}
impl PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
        PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSMBuiltinsPropertiesNV.html)) · Builder of [`PhysicalDeviceShaderSMBuiltinsPropertiesNV`] <br/> VkPhysicalDeviceShaderSMBuiltinsPropertiesNV - Structure describing shader SM Builtins properties supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderSMBuiltinsPropertiesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shader_sm_builtins\ntypedef struct VkPhysicalDeviceShaderSMBuiltinsPropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           shaderSMCount;\n    uint32_t           shaderWarpsPerSM;\n} VkPhysicalDeviceShaderSMBuiltinsPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_sm_count`] is the number of SMs on the\n  device.\n\n* []() [`Self::shader_warps_per_sm`] is the maximum number\n  of simultaneously executing warps on an SM.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceShaderSMBuiltinsPropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderSMBuiltinsPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a>(PhysicalDeviceShaderSMBuiltinsPropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
        PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_sm_count(mut self, shader_sm_count: u32) -> Self {
        self.0.shader_sm_count = shader_sm_count as _;
        self
    }
    #[inline]
    pub fn shader_warps_per_sm(mut self, shader_warps_per_sm: u32) -> Self {
        self.0.shader_warps_per_sm = shader_warps_per_sm as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShaderSMBuiltinsPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
    fn default() -> PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
    type Target = PhysicalDeviceShaderSMBuiltinsPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShaderSMBuiltinsPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSMBuiltinsFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceShaderSMBuiltinsFeaturesNV - Structure describing the shader SM Builtins features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shader_sm_builtins\ntypedef struct VkPhysicalDeviceShaderSMBuiltinsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderSMBuiltins;\n} VkPhysicalDeviceShaderSMBuiltinsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_sm_builtins`] indicates whether\n  the implementation supports the SPIR-V `ShaderSMBuiltinsNV`capability.\n\nIf the [`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderSMBuiltinsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShaderSMBuiltinsFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_sm_builtins: crate::vk1_0::Bool32,
}
impl PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV;
}
impl Default for PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shader_sm_builtins: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShaderSMBuiltinsFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shader_sm_builtins", &(self.shader_sm_builtins != 0)).finish()
    }
}
impl PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
        PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderSMBuiltinsFeaturesNV.html)) · Builder of [`PhysicalDeviceShaderSMBuiltinsFeaturesNV`] <br/> VkPhysicalDeviceShaderSMBuiltinsFeaturesNV - Structure describing the shader SM Builtins features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shader_sm_builtins\ntypedef struct VkPhysicalDeviceShaderSMBuiltinsFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderSMBuiltins;\n} VkPhysicalDeviceShaderSMBuiltinsFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_sm_builtins`] indicates whether\n  the implementation supports the SPIR-V `ShaderSMBuiltinsNV`capability.\n\nIf the [`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderSMBuiltinsFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderSMBuiltinsFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a>(PhysicalDeviceShaderSMBuiltinsFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
        PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_sm_builtins(mut self, shader_sm_builtins: bool) -> Self {
        self.0.shader_sm_builtins = shader_sm_builtins as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShaderSMBuiltinsFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceShaderSMBuiltinsFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShaderSMBuiltinsFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
