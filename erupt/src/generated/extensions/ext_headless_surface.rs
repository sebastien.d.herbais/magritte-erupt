#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_HEADLESS_SURFACE_SPEC_VERSION")]
pub const EXT_HEADLESS_SURFACE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_HEADLESS_SURFACE_EXTENSION_NAME")]
pub const EXT_HEADLESS_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_headless_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_HEADLESS_SURFACE_EXT: *const std::os::raw::c_char = crate::cstr!("vkCreateHeadlessSurfaceEXT");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkHeadlessSurfaceCreateFlagsEXT.html)) · Bitmask of [`HeadlessSurfaceCreateFlagBitsEXT`] <br/> VkHeadlessSurfaceCreateFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_headless_surface\ntypedef VkFlags VkHeadlessSurfaceCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::HeadlessSurfaceCreateFlagBitsEXT`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::HeadlessSurfaceCreateInfoEXT`]\n"] # [doc (alias = "VkHeadlessSurfaceCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct HeadlessSurfaceCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`HeadlessSurfaceCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkHeadlessSurfaceCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct HeadlessSurfaceCreateFlagBitsEXT(pub u32);
impl HeadlessSurfaceCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> HeadlessSurfaceCreateFlagsEXT {
        HeadlessSurfaceCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for HeadlessSurfaceCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_headless_surface`]"]
impl crate::vk1_0::StructureType {
    pub const HEADLESS_SURFACE_CREATE_INFO_EXT: Self = Self(1000256000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateHeadlessSurfaceEXT.html)) · Function <br/> vkCreateHeadlessSurfaceEXT - Create a headless slink:VkSurfaceKHR object\n[](#_c_specification)C Specification\n----------\n\nTo create a headless [`crate::vk::SurfaceKHR`] object, call:\n\n```\n// Provided by VK_EXT_headless_surface\nVkResult vkCreateHeadlessSurfaceEXT(\n    VkInstance                                  instance,\n    const VkHeadlessSurfaceCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::HeadlessSurfaceCreateInfoEXT`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::HeadlessSurfaceCreateInfoEXT`] structure\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::HeadlessSurfaceCreateInfoEXT`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateHeadlessSurfaceEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::ext_headless_surface::HeadlessSurfaceCreateInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkHeadlessSurfaceCreateInfoEXT.html)) · Structure <br/> VkHeadlessSurfaceCreateInfoEXT - Structure specifying parameters of a newly created headless surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::HeadlessSurfaceCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_headless_surface\ntypedef struct VkHeadlessSurfaceCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkHeadlessSurfaceCreateFlagsEXT    flags;\n} VkHeadlessSurfaceCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::HEADLESS_SURFACE_CREATE_INFO_EXT`]\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::HeadlessSurfaceCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_headless_surface_ext`]\n"]
#[doc(alias = "VkHeadlessSurfaceCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct HeadlessSurfaceCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_headless_surface::HeadlessSurfaceCreateFlagsEXT,
}
impl HeadlessSurfaceCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::HEADLESS_SURFACE_CREATE_INFO_EXT;
}
impl Default for HeadlessSurfaceCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default() }
    }
}
impl std::fmt::Debug for HeadlessSurfaceCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("HeadlessSurfaceCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).finish()
    }
}
impl HeadlessSurfaceCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> HeadlessSurfaceCreateInfoEXTBuilder<'a> {
        HeadlessSurfaceCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkHeadlessSurfaceCreateInfoEXT.html)) · Builder of [`HeadlessSurfaceCreateInfoEXT`] <br/> VkHeadlessSurfaceCreateInfoEXT - Structure specifying parameters of a newly created headless surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::HeadlessSurfaceCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_headless_surface\ntypedef struct VkHeadlessSurfaceCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkHeadlessSurfaceCreateFlagsEXT    flags;\n} VkHeadlessSurfaceCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::HEADLESS_SURFACE_CREATE_INFO_EXT`]\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkHeadlessSurfaceCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::HeadlessSurfaceCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_headless_surface_ext`]\n"]
#[repr(transparent)]
pub struct HeadlessSurfaceCreateInfoEXTBuilder<'a>(HeadlessSurfaceCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> HeadlessSurfaceCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> HeadlessSurfaceCreateInfoEXTBuilder<'a> {
        HeadlessSurfaceCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_headless_surface::HeadlessSurfaceCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> HeadlessSurfaceCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for HeadlessSurfaceCreateInfoEXTBuilder<'a> {
    fn default() -> HeadlessSurfaceCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for HeadlessSurfaceCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for HeadlessSurfaceCreateInfoEXTBuilder<'a> {
    type Target = HeadlessSurfaceCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for HeadlessSurfaceCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_headless_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateHeadlessSurfaceEXT.html)) · Function <br/> vkCreateHeadlessSurfaceEXT - Create a headless slink:VkSurfaceKHR object\n[](#_c_specification)C Specification\n----------\n\nTo create a headless [`crate::vk::SurfaceKHR`] object, call:\n\n```\n// Provided by VK_EXT_headless_surface\nVkResult vkCreateHeadlessSurfaceEXT(\n    VkInstance                                  instance,\n    const VkHeadlessSurfaceCreateInfoEXT*       pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::HeadlessSurfaceCreateInfoEXT`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::HeadlessSurfaceCreateInfoEXT`] structure\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateHeadlessSurfaceEXT-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::HeadlessSurfaceCreateInfoEXT`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateHeadlessSurfaceEXT")]
    pub unsafe fn create_headless_surface_ext(&self, create_info: &crate::extensions::ext_headless_surface::HeadlessSurfaceCreateInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_headless_surface_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
