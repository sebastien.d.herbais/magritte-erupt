#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_SHADER_INFO_SPEC_VERSION")]
pub const AMD_SHADER_INFO_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_SHADER_INFO_EXTENSION_NAME")]
pub const AMD_SHADER_INFO_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_AMD_shader_info");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_SHADER_INFO_AMD: *const std::os::raw::c_char = crate::cstr!("vkGetShaderInfoAMD");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderInfoTypeAMD.html)) · Enum <br/> VkShaderInfoTypeAMD - Enum specifying which type of shader information to query\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DeviceLoader::get_shader_info_amd::info_type`], specifying the\ninformation being queried from a shader, are:\n\n```\n// Provided by VK_AMD_shader_info\ntypedef enum VkShaderInfoTypeAMD {\n    VK_SHADER_INFO_TYPE_STATISTICS_AMD = 0,\n    VK_SHADER_INFO_TYPE_BINARY_AMD = 1,\n    VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD = 2,\n} VkShaderInfoTypeAMD;\n```\n[](#_description)Description\n----------\n\n* [`Self::STATISTICS_AMD`] specifies that device resources\n  used by a shader will be queried.\n\n* [`Self::BINARY_AMD`] specifies that\n  implementation-specific information will be queried.\n\n* [`Self::DISASSEMBLY_AMD`] specifies that human-readable\n  dissassembly of a shader.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_shader_info_amd`]\n"]
#[doc(alias = "VkShaderInfoTypeAMD")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ShaderInfoTypeAMD(pub i32);
impl std::fmt::Debug for ShaderInfoTypeAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::STATISTICS_AMD => "STATISTICS_AMD",
            &Self::BINARY_AMD => "BINARY_AMD",
            &Self::DISASSEMBLY_AMD => "DISASSEMBLY_AMD",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::amd_shader_info`]"]
impl crate::extensions::amd_shader_info::ShaderInfoTypeAMD {
    pub const STATISTICS_AMD: Self = Self(0);
    pub const BINARY_AMD: Self = Self(1);
    pub const DISASSEMBLY_AMD: Self = Self(2);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetShaderInfoAMD.html)) · Function <br/> vkGetShaderInfoAMD - Get information about a shader in a pipeline\n[](#_c_specification)C Specification\n----------\n\nInformation about a particular shader that has been compiled as part of a\npipeline object can be extracted by calling:\n\n```\n// Provided by VK_AMD_shader_info\nVkResult vkGetShaderInfoAMD(\n    VkDevice                                    device,\n    VkPipeline                                  pipeline,\n    VkShaderStageFlagBits                       shaderStage,\n    VkShaderInfoTypeAMD                         infoType,\n    size_t*                                     pInfoSize,\n    void*                                       pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created [`Self::pipeline`].\n\n* [`Self::pipeline`] is the target of the query.\n\n* [`Self::shader_stage`] is a [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) specifying the\n  particular shader within the pipeline about which information is being\n  queried.\n\n* [`Self::info_type`] describes what kind of information is being queried.\n\n* [`Self::p_info_size`] is a pointer to a value related to the amount of data\n  the query returns, as described below.\n\n* [`Self::p_info`] is either `NULL` or a pointer to a buffer.\n[](#_description)Description\n----------\n\nIf [`Self::p_info`] is `NULL`, then the maximum size of the information that **can**be retrieved about the shader, in bytes, is returned in [`Self::p_info_size`].\nOtherwise, [`Self::p_info_size`] **must** point to a variable set by the user to the\nsize of the buffer, in bytes, pointed to by [`Self::p_info`], and on return the\nvariable is overwritten with the amount of data actually written to[`Self::p_info`].\nIf [`Self::p_info_size`] is less than the maximum size that **can** be retrieved by\nthe pipeline cache, then at most [`Self::p_info_size`] bytes will be written to[`Self::p_info`], and [`crate::vk::Result::INCOMPLETE`] will be returned, instead of[`crate::vk::Result::SUCCESS`], to indicate that not all required of the pipeline cache\nwas returned.\n\nNot all information is available for every shader and implementations may\nnot support all kinds of information for any shader.\nWhen a certain type of information is unavailable, the function returns[`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`].\n\nIf information is successfully and fully queried, the function will return[`crate::vk::Result::SUCCESS`].\n\nFor [`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::STATISTICS_AMD`], a[`crate::vk::ShaderStatisticsInfoAMD`] structure will be written to the buffer\npointed to by [`Self::p_info`].\nThis structure will be populated with statistics regarding the physical\ndevice resources used by that shader along with other miscellaneous\ninformation and is described in further detail below.\n\nFor [`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::DISASSEMBLY_AMD`], [`Self::p_info`] is\na pointer to a UTF-8 null-terminated string containing human-readable\ndisassembly.\nThe exact formatting and contents of the disassembly string are\nvendor-specific.\n\nThe formatting and contents of all other types of information, including[`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::BINARY_AMD`], are left to the vendor\nand are not further specified by this extension.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetShaderInfoAMD-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetShaderInfoAMD-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-vkGetShaderInfoAMD-shaderStage-parameter  \n  [`Self::shader_stage`] **must** be a valid [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) value\n\n* []() VUID-vkGetShaderInfoAMD-infoType-parameter  \n  [`Self::info_type`] **must** be a valid [`crate::vk::ShaderInfoTypeAMD`] value\n\n* []() VUID-vkGetShaderInfoAMD-pInfoSize-parameter  \n  [`Self::p_info_size`] **must** be a valid pointer to a `size_t` value\n\n* []() VUID-vkGetShaderInfoAMD-pInfo-parameter  \n   If the value referenced by [`Self::p_info_size`] is not `0`, and [`Self::p_info`] is not `NULL`, [`Self::p_info`] **must** be a valid pointer to an array of [`Self::p_info_size`] bytes\n\n* []() VUID-vkGetShaderInfoAMD-pipeline-parent  \n  [`Self::pipeline`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::Pipeline`], [`crate::vk::ShaderInfoTypeAMD`], [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html)\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetShaderInfoAMD = unsafe extern "system" fn(device: crate::vk1_0::Device, pipeline: crate::vk1_0::Pipeline, shader_stage: crate::vk1_0::ShaderStageFlagBits, info_type: crate::extensions::amd_shader_info::ShaderInfoTypeAMD, p_info_size: *mut usize, p_info: *mut std::ffi::c_void) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderResourceUsageAMD.html)) · Structure <br/> VkShaderResourceUsageAMD - Resource usage information about a particular shader within a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShaderResourceUsageAMD`] structure is defined as:\n\n```\n// Provided by VK_AMD_shader_info\ntypedef struct VkShaderResourceUsageAMD {\n    uint32_t    numUsedVgprs;\n    uint32_t    numUsedSgprs;\n    uint32_t    ldsSizePerLocalWorkGroup;\n    size_t      ldsUsageSizeInBytes;\n    size_t      scratchMemUsageInBytes;\n} VkShaderResourceUsageAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::num_used_vgprs`] is the number of vector instruction general-purpose\n  registers used by this shader.\n\n* [`Self::num_used_sgprs`] is the number of scalar instruction general-purpose\n  registers used by this shader.\n\n* [`Self::lds_size_per_local_work_group`] is the maximum local data store size per\n  work group in bytes.\n\n* [`Self::lds_usage_size_in_bytes`] is the LDS usage size in bytes per work group\n  by this shader.\n\n* [`Self::scratch_mem_usage_in_bytes`] is the scratch memory usage in bytes by\n  this shader.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderStatisticsInfoAMD`]\n"]
#[doc(alias = "VkShaderResourceUsageAMD")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ShaderResourceUsageAMD {
    pub num_used_vgprs: u32,
    pub num_used_sgprs: u32,
    pub lds_size_per_local_work_group: u32,
    pub lds_usage_size_in_bytes: usize,
    pub scratch_mem_usage_in_bytes: usize,
}
impl Default for ShaderResourceUsageAMD {
    fn default() -> Self {
        Self { num_used_vgprs: Default::default(), num_used_sgprs: Default::default(), lds_size_per_local_work_group: Default::default(), lds_usage_size_in_bytes: Default::default(), scratch_mem_usage_in_bytes: Default::default() }
    }
}
impl std::fmt::Debug for ShaderResourceUsageAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ShaderResourceUsageAMD").field("num_used_vgprs", &self.num_used_vgprs).field("num_used_sgprs", &self.num_used_sgprs).field("lds_size_per_local_work_group", &self.lds_size_per_local_work_group).field("lds_usage_size_in_bytes", &self.lds_usage_size_in_bytes).field("scratch_mem_usage_in_bytes", &self.scratch_mem_usage_in_bytes).finish()
    }
}
impl ShaderResourceUsageAMD {
    #[inline]
    pub fn into_builder<'a>(self) -> ShaderResourceUsageAMDBuilder<'a> {
        ShaderResourceUsageAMDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderResourceUsageAMD.html)) · Builder of [`ShaderResourceUsageAMD`] <br/> VkShaderResourceUsageAMD - Resource usage information about a particular shader within a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShaderResourceUsageAMD`] structure is defined as:\n\n```\n// Provided by VK_AMD_shader_info\ntypedef struct VkShaderResourceUsageAMD {\n    uint32_t    numUsedVgprs;\n    uint32_t    numUsedSgprs;\n    uint32_t    ldsSizePerLocalWorkGroup;\n    size_t      ldsUsageSizeInBytes;\n    size_t      scratchMemUsageInBytes;\n} VkShaderResourceUsageAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::num_used_vgprs`] is the number of vector instruction general-purpose\n  registers used by this shader.\n\n* [`Self::num_used_sgprs`] is the number of scalar instruction general-purpose\n  registers used by this shader.\n\n* [`Self::lds_size_per_local_work_group`] is the maximum local data store size per\n  work group in bytes.\n\n* [`Self::lds_usage_size_in_bytes`] is the LDS usage size in bytes per work group\n  by this shader.\n\n* [`Self::scratch_mem_usage_in_bytes`] is the scratch memory usage in bytes by\n  this shader.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderStatisticsInfoAMD`]\n"]
#[repr(transparent)]
pub struct ShaderResourceUsageAMDBuilder<'a>(ShaderResourceUsageAMD, std::marker::PhantomData<&'a ()>);
impl<'a> ShaderResourceUsageAMDBuilder<'a> {
    #[inline]
    pub fn new() -> ShaderResourceUsageAMDBuilder<'a> {
        ShaderResourceUsageAMDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn num_used_vgprs(mut self, num_used_vgprs: u32) -> Self {
        self.0.num_used_vgprs = num_used_vgprs as _;
        self
    }
    #[inline]
    pub fn num_used_sgprs(mut self, num_used_sgprs: u32) -> Self {
        self.0.num_used_sgprs = num_used_sgprs as _;
        self
    }
    #[inline]
    pub fn lds_size_per_local_work_group(mut self, lds_size_per_local_work_group: u32) -> Self {
        self.0.lds_size_per_local_work_group = lds_size_per_local_work_group as _;
        self
    }
    #[inline]
    pub fn lds_usage_size_in_bytes(mut self, lds_usage_size_in_bytes: usize) -> Self {
        self.0.lds_usage_size_in_bytes = lds_usage_size_in_bytes as _;
        self
    }
    #[inline]
    pub fn scratch_mem_usage_in_bytes(mut self, scratch_mem_usage_in_bytes: usize) -> Self {
        self.0.scratch_mem_usage_in_bytes = scratch_mem_usage_in_bytes as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ShaderResourceUsageAMD {
        self.0
    }
}
impl<'a> std::default::Default for ShaderResourceUsageAMDBuilder<'a> {
    fn default() -> ShaderResourceUsageAMDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ShaderResourceUsageAMDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ShaderResourceUsageAMDBuilder<'a> {
    type Target = ShaderResourceUsageAMD;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ShaderResourceUsageAMDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStatisticsInfoAMD.html)) · Structure <br/> VkShaderStatisticsInfoAMD - Statistical information about a particular shader within a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShaderStatisticsInfoAMD`] structure is defined as:\n\n```\n// Provided by VK_AMD_shader_info\ntypedef struct VkShaderStatisticsInfoAMD {\n    VkShaderStageFlags          shaderStageMask;\n    VkShaderResourceUsageAMD    resourceUsage;\n    uint32_t                    numPhysicalVgprs;\n    uint32_t                    numPhysicalSgprs;\n    uint32_t                    numAvailableVgprs;\n    uint32_t                    numAvailableSgprs;\n    uint32_t                    computeWorkGroupSize[3];\n} VkShaderStatisticsInfoAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::shader_stage_mask`] are the combination of logical shader stages\n  contained within this shader.\n\n* [`Self::resource_usage`] is a [`crate::vk::ShaderResourceUsageAMD`] structure\n  describing internal physical device resources used by this shader.\n\n* [`Self::num_physical_vgprs`] is the maximum number of vector instruction\n  general-purpose registers (VGPRs) available to the physical device.\n\n* [`Self::num_physical_sgprs`] is the maximum number of scalar instruction\n  general-purpose registers (SGPRs) available to the physical device.\n\n* [`Self::num_available_vgprs`] is the maximum limit of VGPRs made available to\n  the shader compiler.\n\n* [`Self::num_available_sgprs`] is the maximum limit of SGPRs made available to\n  the shader compiler.\n\n* [`Self::compute_work_group_size`] is the local workgroup size of this shader in\n  { X, Y, Z } dimensions.\n[](#_description)Description\n----------\n\nSome implementations may merge multiple logical shader stages together in a\nsingle shader.\nIn such cases, [`Self::shader_stage_mask`] will contain a bitmask of all of the\nstages that are active within that shader.\nConsequently, if specifying those stages as input to[`crate::vk::DeviceLoader::get_shader_info_amd`], the same output information **may** be returned for\nall such shader stage queries.\n\nThe number of available VGPRs and SGPRs ([`Self::num_available_vgprs`] and[`Self::num_available_sgprs`] respectively) are the shader-addressable subset of\nphysical registers that is given as a limit to the compiler for register\nassignment.\nThese values **may** further be limited by implementations due to performance\noptimizations where register pressure is a bottleneck.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderResourceUsageAMD`], [`crate::vk::ShaderStageFlagBits`]\n"]
#[doc(alias = "VkShaderStatisticsInfoAMD")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ShaderStatisticsInfoAMD {
    pub shader_stage_mask: crate::vk1_0::ShaderStageFlags,
    pub resource_usage: crate::extensions::amd_shader_info::ShaderResourceUsageAMD,
    pub num_physical_vgprs: u32,
    pub num_physical_sgprs: u32,
    pub num_available_vgprs: u32,
    pub num_available_sgprs: u32,
    pub compute_work_group_size: [u32; 3],
}
impl Default for ShaderStatisticsInfoAMD {
    fn default() -> Self {
        Self { shader_stage_mask: Default::default(), resource_usage: Default::default(), num_physical_vgprs: Default::default(), num_physical_sgprs: Default::default(), num_available_vgprs: Default::default(), num_available_sgprs: Default::default(), compute_work_group_size: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for ShaderStatisticsInfoAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ShaderStatisticsInfoAMD").field("shader_stage_mask", &self.shader_stage_mask).field("resource_usage", &self.resource_usage).field("num_physical_vgprs", &self.num_physical_vgprs).field("num_physical_sgprs", &self.num_physical_sgprs).field("num_available_vgprs", &self.num_available_vgprs).field("num_available_sgprs", &self.num_available_sgprs).field("compute_work_group_size", &self.compute_work_group_size).finish()
    }
}
impl ShaderStatisticsInfoAMD {
    #[inline]
    pub fn into_builder<'a>(self) -> ShaderStatisticsInfoAMDBuilder<'a> {
        ShaderStatisticsInfoAMDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStatisticsInfoAMD.html)) · Builder of [`ShaderStatisticsInfoAMD`] <br/> VkShaderStatisticsInfoAMD - Statistical information about a particular shader within a pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShaderStatisticsInfoAMD`] structure is defined as:\n\n```\n// Provided by VK_AMD_shader_info\ntypedef struct VkShaderStatisticsInfoAMD {\n    VkShaderStageFlags          shaderStageMask;\n    VkShaderResourceUsageAMD    resourceUsage;\n    uint32_t                    numPhysicalVgprs;\n    uint32_t                    numPhysicalSgprs;\n    uint32_t                    numAvailableVgprs;\n    uint32_t                    numAvailableSgprs;\n    uint32_t                    computeWorkGroupSize[3];\n} VkShaderStatisticsInfoAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::shader_stage_mask`] are the combination of logical shader stages\n  contained within this shader.\n\n* [`Self::resource_usage`] is a [`crate::vk::ShaderResourceUsageAMD`] structure\n  describing internal physical device resources used by this shader.\n\n* [`Self::num_physical_vgprs`] is the maximum number of vector instruction\n  general-purpose registers (VGPRs) available to the physical device.\n\n* [`Self::num_physical_sgprs`] is the maximum number of scalar instruction\n  general-purpose registers (SGPRs) available to the physical device.\n\n* [`Self::num_available_vgprs`] is the maximum limit of VGPRs made available to\n  the shader compiler.\n\n* [`Self::num_available_sgprs`] is the maximum limit of SGPRs made available to\n  the shader compiler.\n\n* [`Self::compute_work_group_size`] is the local workgroup size of this shader in\n  { X, Y, Z } dimensions.\n[](#_description)Description\n----------\n\nSome implementations may merge multiple logical shader stages together in a\nsingle shader.\nIn such cases, [`Self::shader_stage_mask`] will contain a bitmask of all of the\nstages that are active within that shader.\nConsequently, if specifying those stages as input to[`crate::vk::DeviceLoader::get_shader_info_amd`], the same output information **may** be returned for\nall such shader stage queries.\n\nThe number of available VGPRs and SGPRs ([`Self::num_available_vgprs`] and[`Self::num_available_sgprs`] respectively) are the shader-addressable subset of\nphysical registers that is given as a limit to the compiler for register\nassignment.\nThese values **may** further be limited by implementations due to performance\noptimizations where register pressure is a bottleneck.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ShaderResourceUsageAMD`], [`crate::vk::ShaderStageFlagBits`]\n"]
#[repr(transparent)]
pub struct ShaderStatisticsInfoAMDBuilder<'a>(ShaderStatisticsInfoAMD, std::marker::PhantomData<&'a ()>);
impl<'a> ShaderStatisticsInfoAMDBuilder<'a> {
    #[inline]
    pub fn new() -> ShaderStatisticsInfoAMDBuilder<'a> {
        ShaderStatisticsInfoAMDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_stage_mask(mut self, shader_stage_mask: crate::vk1_0::ShaderStageFlags) -> Self {
        self.0.shader_stage_mask = shader_stage_mask as _;
        self
    }
    #[inline]
    pub fn resource_usage(mut self, resource_usage: crate::extensions::amd_shader_info::ShaderResourceUsageAMD) -> Self {
        self.0.resource_usage = resource_usage as _;
        self
    }
    #[inline]
    pub fn num_physical_vgprs(mut self, num_physical_vgprs: u32) -> Self {
        self.0.num_physical_vgprs = num_physical_vgprs as _;
        self
    }
    #[inline]
    pub fn num_physical_sgprs(mut self, num_physical_sgprs: u32) -> Self {
        self.0.num_physical_sgprs = num_physical_sgprs as _;
        self
    }
    #[inline]
    pub fn num_available_vgprs(mut self, num_available_vgprs: u32) -> Self {
        self.0.num_available_vgprs = num_available_vgprs as _;
        self
    }
    #[inline]
    pub fn num_available_sgprs(mut self, num_available_sgprs: u32) -> Self {
        self.0.num_available_sgprs = num_available_sgprs as _;
        self
    }
    #[inline]
    pub fn compute_work_group_size(mut self, compute_work_group_size: [u32; 3]) -> Self {
        self.0.compute_work_group_size = compute_work_group_size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ShaderStatisticsInfoAMD {
        self.0
    }
}
impl<'a> std::default::Default for ShaderStatisticsInfoAMDBuilder<'a> {
    fn default() -> ShaderStatisticsInfoAMDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ShaderStatisticsInfoAMDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ShaderStatisticsInfoAMDBuilder<'a> {
    type Target = ShaderStatisticsInfoAMD;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ShaderStatisticsInfoAMDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::amd_shader_info`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetShaderInfoAMD.html)) · Function <br/> vkGetShaderInfoAMD - Get information about a shader in a pipeline\n[](#_c_specification)C Specification\n----------\n\nInformation about a particular shader that has been compiled as part of a\npipeline object can be extracted by calling:\n\n```\n// Provided by VK_AMD_shader_info\nVkResult vkGetShaderInfoAMD(\n    VkDevice                                    device,\n    VkPipeline                                  pipeline,\n    VkShaderStageFlagBits                       shaderStage,\n    VkShaderInfoTypeAMD                         infoType,\n    size_t*                                     pInfoSize,\n    void*                                       pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created [`Self::pipeline`].\n\n* [`Self::pipeline`] is the target of the query.\n\n* [`Self::shader_stage`] is a [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) specifying the\n  particular shader within the pipeline about which information is being\n  queried.\n\n* [`Self::info_type`] describes what kind of information is being queried.\n\n* [`Self::p_info_size`] is a pointer to a value related to the amount of data\n  the query returns, as described below.\n\n* [`Self::p_info`] is either `NULL` or a pointer to a buffer.\n[](#_description)Description\n----------\n\nIf [`Self::p_info`] is `NULL`, then the maximum size of the information that **can**be retrieved about the shader, in bytes, is returned in [`Self::p_info_size`].\nOtherwise, [`Self::p_info_size`] **must** point to a variable set by the user to the\nsize of the buffer, in bytes, pointed to by [`Self::p_info`], and on return the\nvariable is overwritten with the amount of data actually written to[`Self::p_info`].\nIf [`Self::p_info_size`] is less than the maximum size that **can** be retrieved by\nthe pipeline cache, then at most [`Self::p_info_size`] bytes will be written to[`Self::p_info`], and [`crate::vk::Result::INCOMPLETE`] will be returned, instead of[`crate::vk::Result::SUCCESS`], to indicate that not all required of the pipeline cache\nwas returned.\n\nNot all information is available for every shader and implementations may\nnot support all kinds of information for any shader.\nWhen a certain type of information is unavailable, the function returns[`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`].\n\nIf information is successfully and fully queried, the function will return[`crate::vk::Result::SUCCESS`].\n\nFor [`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::STATISTICS_AMD`], a[`crate::vk::ShaderStatisticsInfoAMD`] structure will be written to the buffer\npointed to by [`Self::p_info`].\nThis structure will be populated with statistics regarding the physical\ndevice resources used by that shader along with other miscellaneous\ninformation and is described in further detail below.\n\nFor [`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::DISASSEMBLY_AMD`], [`Self::p_info`] is\na pointer to a UTF-8 null-terminated string containing human-readable\ndisassembly.\nThe exact formatting and contents of the disassembly string are\nvendor-specific.\n\nThe formatting and contents of all other types of information, including[`Self::info_type`] [`crate::vk::ShaderInfoTypeAMD::BINARY_AMD`], are left to the vendor\nand are not further specified by this extension.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetShaderInfoAMD-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetShaderInfoAMD-pipeline-parameter  \n  [`Self::pipeline`] **must** be a valid [`crate::vk::Pipeline`] handle\n\n* []() VUID-vkGetShaderInfoAMD-shaderStage-parameter  \n  [`Self::shader_stage`] **must** be a valid [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html) value\n\n* []() VUID-vkGetShaderInfoAMD-infoType-parameter  \n  [`Self::info_type`] **must** be a valid [`crate::vk::ShaderInfoTypeAMD`] value\n\n* []() VUID-vkGetShaderInfoAMD-pInfoSize-parameter  \n  [`Self::p_info_size`] **must** be a valid pointer to a `size_t` value\n\n* []() VUID-vkGetShaderInfoAMD-pInfo-parameter  \n   If the value referenced by [`Self::p_info_size`] is not `0`, and [`Self::p_info`] is not `NULL`, [`Self::p_info`] **must** be a valid pointer to an array of [`Self::p_info_size`] bytes\n\n* []() VUID-vkGetShaderInfoAMD-pipeline-parent  \n  [`Self::pipeline`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_FEATURE_NOT_PRESENT`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::Pipeline`], [`crate::vk::ShaderInfoTypeAMD`], [VkShaderStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html)\n"]
    #[doc(alias = "vkGetShaderInfoAMD")]
    pub unsafe fn get_shader_info_amd(&self, pipeline: crate::vk1_0::Pipeline, shader_stage: crate::vk1_0::ShaderStageFlagBits, info_type: crate::extensions::amd_shader_info::ShaderInfoTypeAMD, info_size: *mut usize, info: *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_shader_info_amd.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, pipeline as _, shader_stage as _, info_type as _, info_size, info);
        crate::utils::VulkanResult::new(_return, ())
    }
}
