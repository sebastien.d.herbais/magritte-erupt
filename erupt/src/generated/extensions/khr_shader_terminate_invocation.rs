#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_SHADER_TERMINATE_INVOCATION_SPEC_VERSION")]
pub const KHR_SHADER_TERMINATE_INVOCATION_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_SHADER_TERMINATE_INVOCATION_EXTENSION_NAME")]
pub const KHR_SHADER_TERMINATE_INVOCATION_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_shader_terminate_invocation");
#[doc = "Provided by [`crate::extensions::khr_shader_terminate_invocation`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR: Self = Self(1000215000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderTerminateInvocationFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderTerminateInvocationFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR.html)) · Structure <br/> VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR - Structure describing support for the SPIR-V code:SPV\\_KHR\\_terminate\\_invocation extension\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] structure is\ndefined as:\n\n```\n// Provided by VK_KHR_shader_terminate_invocation\ntypedef struct VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderTerminateInvocation;\n} VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_terminate_invocation`]specifies whether the implementation supports SPIR-V modules that use\n  the `SPV_KHR_terminate_invocation` extension.\n\nIf the [`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_terminate_invocation: crate::vk1_0::Bool32,
}
impl PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR;
}
impl Default for PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shader_terminate_invocation: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShaderTerminateInvocationFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shader_terminate_invocation", &(self.shader_terminate_invocation != 0)).finish()
    }
}
impl PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
        PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR.html)) · Builder of [`PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] <br/> VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR - Structure describing support for the SPIR-V code:SPV\\_KHR\\_terminate\\_invocation extension\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] structure is\ndefined as:\n\n```\n// Provided by VK_KHR_shader_terminate_invocation\ntypedef struct VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderTerminateInvocation;\n} VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shader_terminate_invocation`]specifies whether the implementation supports SPIR-V modules that use\n  the `SPV_KHR_terminate_invocation` extension.\n\nIf the [`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShaderTerminateInvocationFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a>(PhysicalDeviceShaderTerminateInvocationFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
        PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_terminate_invocation(mut self, shader_terminate_invocation: bool) -> Self {
        self.0.shader_terminate_invocation = shader_terminate_invocation as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
    type Target = PhysicalDeviceShaderTerminateInvocationFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShaderTerminateInvocationFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
