#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_SPEC_VERSION")]
pub const EXT_IMAGE_DRM_FORMAT_MODIFIER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME")]
pub const EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_image_drm_format_modifier");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetImageDrmFormatModifierPropertiesEXT");
#[doc = "Provided by [`crate::extensions::ext_image_drm_format_modifier`]"]
impl crate::vk1_0::ImageAspectFlagBits {
    pub const MEMORY_PLANE_0_EXT: Self = Self(128);
    pub const MEMORY_PLANE_1_EXT: Self = Self(256);
    pub const MEMORY_PLANE_2_EXT: Self = Self(512);
    pub const MEMORY_PLANE_3_EXT: Self = Self(1024);
}
#[doc = "Provided by [`crate::extensions::ext_image_drm_format_modifier`]"]
impl crate::vk1_0::ImageTiling {
    pub const DRM_FORMAT_MODIFIER_EXT: Self = Self(1000158000);
}
#[doc = "Provided by [`crate::extensions::ext_image_drm_format_modifier`]"]
impl crate::vk1_0::Result {
    pub const ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: Self = Self(-1000158000);
}
#[doc = "Provided by [`crate::extensions::ext_image_drm_format_modifier`]"]
impl crate::vk1_0::StructureType {
    pub const DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT: Self = Self(1000158000);
    pub const PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT: Self = Self(1000158002);
    pub const IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT: Self = Self(1000158003);
    pub const IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT: Self = Self(1000158004);
    pub const IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT: Self = Self(1000158005);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageDrmFormatModifierPropertiesEXT.html)) · Function <br/> vkGetImageDrmFormatModifierPropertiesEXT - Returns an image's DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nIf an image was created with [`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`],\nthen the image has a [Linux DRM format\nmodifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\nTo query the *modifier*, call:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\nVkResult vkGetImageDrmFormatModifierPropertiesEXT(\n    VkDevice                                    device,\n    VkImage                                     image,\n    VkImageDrmFormatModifierPropertiesEXT*      pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image.\n\n* [`Self::image`] is the queried image.\n\n* [`Self::p_properties`] is a pointer to a[`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure in which\n  properties of the image’s *DRM format modifier* are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-02272  \n  [`Self::image`] **must** have been created with[`tiling`](VkImageCreateInfo.html) equal to[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-parameter  \n  [`Self::image`] **must** be a valid [`crate::vk::Image`] handle\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-parent  \n  [`Self::image`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::Image`], [`crate::vk::ImageDrmFormatModifierPropertiesEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetImageDrmFormatModifierPropertiesEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, image: crate::vk1_0::Image, p_properties: *mut crate::extensions::ext_image_drm_format_modifier::ImageDrmFormatModifierPropertiesEXT) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImageDrmFormatModifierListCreateInfoEXT> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImageDrmFormatModifierListCreateInfoEXTBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImageDrmFormatModifierExplicitCreateInfoEXT> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, DrmFormatModifierPropertiesListEXT> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, DrmFormatModifierPropertiesListEXTBuilder<'_>> for crate::vk1_1::FormatProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceImageDrmFormatModifierInfoEXT> for crate::vk1_1::PhysicalDeviceImageFormatInfo2Builder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceImageFormatInfo2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDrmFormatModifierPropertiesListEXT.html)) · Structure <br/> VkDrmFormatModifierPropertiesListEXT - Structure specifying the list of DRM format modifiers supported for a format\n[](#_c_specification)C Specification\n----------\n\nTo obtain the list of [Linux DRM format\nmodifiers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) compatible with a [`crate::vk::Format`], add a[`crate::vk::DrmFormatModifierPropertiesListEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::FormatProperties2`].\n\nThe [`crate::vk::DrmFormatModifierPropertiesListEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkDrmFormatModifierPropertiesListEXT {\n    VkStructureType                      sType;\n    void*                                pNext;\n    uint32_t                             drmFormatModifierCount;\n    VkDrmFormatModifierPropertiesEXT*    pDrmFormatModifierProperties;\n} VkDrmFormatModifierPropertiesListEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier_count`] is an inout parameter related to the number\n  of modifiers compatible with the `format`, as described below.\n\n* [`Self::p_drm_format_modifier_properties`] is either `NULL` or a pointer to an\n  array of [`crate::vk::DrmFormatModifierPropertiesEXT`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_drm_format_modifier_properties`] is `NULL`, then the function returns\nin [`Self::drm_format_modifier_count`] the number of modifiers compatible with the\nqueried `format`.\nOtherwise, the application **must** set [`Self::drm_format_modifier_count`] to the\nlength of the array [`Self::p_drm_format_modifier_properties`]; the function will\nwrite at most [`Self::drm_format_modifier_count`] elements to the array, and will\nreturn in [`Self::drm_format_modifier_count`] the number of elements written.\n\nAmong the elements in array [`Self::p_drm_format_modifier_properties`], each\nreturned `drmFormatModifier` **must** be unique.\n\nValid Usage (Implicit)\n\n* []() VUID-VkDrmFormatModifierPropertiesListEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DrmFormatModifierPropertiesEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDrmFormatModifierPropertiesListEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DrmFormatModifierPropertiesListEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub drm_format_modifier_count: u32,
    pub p_drm_format_modifier_properties: *mut crate::extensions::ext_image_drm_format_modifier::DrmFormatModifierPropertiesEXT,
}
impl DrmFormatModifierPropertiesListEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT;
}
impl Default for DrmFormatModifierPropertiesListEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), drm_format_modifier_count: Default::default(), p_drm_format_modifier_properties: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for DrmFormatModifierPropertiesListEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DrmFormatModifierPropertiesListEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("drm_format_modifier_count", &self.drm_format_modifier_count).field("p_drm_format_modifier_properties", &self.p_drm_format_modifier_properties).finish()
    }
}
impl DrmFormatModifierPropertiesListEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DrmFormatModifierPropertiesListEXTBuilder<'a> {
        DrmFormatModifierPropertiesListEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDrmFormatModifierPropertiesListEXT.html)) · Builder of [`DrmFormatModifierPropertiesListEXT`] <br/> VkDrmFormatModifierPropertiesListEXT - Structure specifying the list of DRM format modifiers supported for a format\n[](#_c_specification)C Specification\n----------\n\nTo obtain the list of [Linux DRM format\nmodifiers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) compatible with a [`crate::vk::Format`], add a[`crate::vk::DrmFormatModifierPropertiesListEXT`] structure to the [`Self::p_next`]chain of [`crate::vk::FormatProperties2`].\n\nThe [`crate::vk::DrmFormatModifierPropertiesListEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkDrmFormatModifierPropertiesListEXT {\n    VkStructureType                      sType;\n    void*                                pNext;\n    uint32_t                             drmFormatModifierCount;\n    VkDrmFormatModifierPropertiesEXT*    pDrmFormatModifierProperties;\n} VkDrmFormatModifierPropertiesListEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier_count`] is an inout parameter related to the number\n  of modifiers compatible with the `format`, as described below.\n\n* [`Self::p_drm_format_modifier_properties`] is either `NULL` or a pointer to an\n  array of [`crate::vk::DrmFormatModifierPropertiesEXT`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_drm_format_modifier_properties`] is `NULL`, then the function returns\nin [`Self::drm_format_modifier_count`] the number of modifiers compatible with the\nqueried `format`.\nOtherwise, the application **must** set [`Self::drm_format_modifier_count`] to the\nlength of the array [`Self::p_drm_format_modifier_properties`]; the function will\nwrite at most [`Self::drm_format_modifier_count`] elements to the array, and will\nreturn in [`Self::drm_format_modifier_count`] the number of elements written.\n\nAmong the elements in array [`Self::p_drm_format_modifier_properties`], each\nreturned `drmFormatModifier` **must** be unique.\n\nValid Usage (Implicit)\n\n* []() VUID-VkDrmFormatModifierPropertiesListEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DrmFormatModifierPropertiesEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DrmFormatModifierPropertiesListEXTBuilder<'a>(DrmFormatModifierPropertiesListEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DrmFormatModifierPropertiesListEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DrmFormatModifierPropertiesListEXTBuilder<'a> {
        DrmFormatModifierPropertiesListEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifier_properties(mut self, drm_format_modifier_properties: &'a mut [crate::extensions::ext_image_drm_format_modifier::DrmFormatModifierPropertiesEXTBuilder]) -> Self {
        self.0.p_drm_format_modifier_properties = drm_format_modifier_properties.as_ptr() as _;
        self.0.drm_format_modifier_count = drm_format_modifier_properties.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DrmFormatModifierPropertiesListEXT {
        self.0
    }
}
impl<'a> std::default::Default for DrmFormatModifierPropertiesListEXTBuilder<'a> {
    fn default() -> DrmFormatModifierPropertiesListEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DrmFormatModifierPropertiesListEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DrmFormatModifierPropertiesListEXTBuilder<'a> {
    type Target = DrmFormatModifierPropertiesListEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DrmFormatModifierPropertiesListEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDrmFormatModifierPropertiesEXT.html)) · Structure <br/> VkDrmFormatModifierPropertiesEXT - Structure specifying properties of a format when combined with a DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DrmFormatModifierPropertiesEXT`] structure describes properties of\na [`crate::vk::Format`] when that format is combined with a[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\nThese properties, like those of [`crate::vk::FormatProperties2`], are independent\nof any particular image.\n\nThe [`crate::vk::DrmFormatModifierPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkDrmFormatModifierPropertiesEXT {\n    uint64_t                drmFormatModifier;\n    uint32_t                drmFormatModifierPlaneCount;\n    VkFormatFeatureFlags    drmFormatModifierTilingFeatures;\n} VkDrmFormatModifierPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::drm_format_modifier`] is a *Linux DRM format modifier*.\n\n* [`Self::drm_format_modifier_plane_count`] is the number of *memory planes* in\n  any image created with `format` and [`Self::drm_format_modifier`].\n  An image’s *memory planecount* is distinct from its *format planecount*,\n  as explained below.\n\n* [`Self::drm_format_modifier_tiling_features`] is a bitmask of[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html) that are supported by any image created\n  with `format` and [`Self::drm_format_modifier`].\n[](#_description)Description\n----------\n\nThe returned [`Self::drm_format_modifier_tiling_features`] **must** contain at least\none bit.\n\nThe implementation **must** not return `DRM_FORMAT_MOD_INVALID` in[`Self::drm_format_modifier`].\n\nAn image’s *memory planecount* (as returned by[`Self::drm_format_modifier_plane_count`]) is distinct from its *format planecount*(in the sense of [multi-planar](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-requiring-sampler-ycbcr-conversion)Y′C<sub>B</sub>C<sub>R</sub> formats).\nIn [`crate::vk::ImageAspectFlagBits`], each`VK_IMAGE_ASPECT_MEMORY_PLANE`**i*\\_BIT\\_EXT represents a \\_memory plane*and each `VK_IMAGE_ASPECT_PLANE`**i*\\_BIT a \\_format plane*.\n\nAn image’s set of *format planes* is an ordered partition of the image’s**content** into separable groups of format channels.\nThe ordered partition is encoded in the name of each [`crate::vk::Format`].\nFor example, [`crate::vk::Format::G8_B8R8_2PLANE_420_UNORM`] contains two *format\nplanes*; the first plane contains the green channel and the second plane\ncontains the blue channel and red channel.\nIf the format name does not contain `PLANE`, then the format contains a\nsingle plane; for example, [`crate::vk::Format::R8G8B8A8_UNORM`].\nSome commands, such as [`crate::vk::PFN_vkCmdCopyBufferToImage`], do not operate on all\nformat channels in the image, but instead operate only on the *format\nplanes* explicitly chosen by the application and operate on each *format\nplane* independently.\n\nAn image’s set of *memory planes* is an ordered partition of the image’s**memory** rather than the image’s **content**.\nEach *memory plane* is a contiguous range of memory.\nThe union of an image’s *memory planes* is not necessarily contiguous.\n\nIf an image is [linear](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-linear-resource), then the partition is\nthe same for *memory planes* and for *format planes*.\nTherefore, if the returned [`Self::drm_format_modifier`] is`DRM_FORMAT_MOD_LINEAR`, then [`Self::drm_format_modifier_plane_count`] **must**equal the *format planecount*, and [`Self::drm_format_modifier_tiling_features`]**must** be identical to the[`crate::vk::FormatProperties2::linear_tiling_features`] returned in the same`pNext` chain.\n\nIf an image is [non-linear](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-linear-resource), then the partition\nof the image’s **memory** into *memory planes* is implementation-specific and**may** be unrelated to the partition of the image’s **content** into *format\nplanes*.\nFor example, consider an image whose `format` is[`crate::vk::Format::G8_B8_R8_3PLANE_420_UNORM`], `tiling` is[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`], whose [`Self::drm_format_modifier`]is not `DRM_FORMAT_MOD_LINEAR`, and `flags` lacks[`crate::vk::ImageCreateFlagBits::DISJOINT`].\nThe image has 3 *format planes*, and commands such[`crate::vk::PFN_vkCmdCopyBufferToImage`] act on each *format plane* independently as if\nthe data of each *format plane* were separable from the data of the other\nplanes.\nIn a straightforward implementation, the implementation **may** store the\nimage’s content in 3 adjacent *memory planes* where each *memory plane*corresponds exactly to a *format plane*.\nHowever, the implementation **may** also store the image’s content in a single*memory plane* where all format channels are combined using an\nimplementation-private block-compressed format; or the implementation **may**store the image’s content in a collection of 7 adjacent *memory planes*using an implementation-private sharding technique.\nBecause the image is non-linear and non-disjoint, the implementation has\nmuch freedom when choosing the image’s placement in memory.\n\nThe *memory planecount* applies to function parameters and structures only\nwhen the API specifies an explicit requirement on[`Self::drm_format_modifier_plane_count`].\nIn all other cases, the *memory planecount* is ignored.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DrmFormatModifierPropertiesListEXT`], [`crate::vk::FormatFeatureFlagBits`]\n"]
#[doc(alias = "VkDrmFormatModifierPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DrmFormatModifierPropertiesEXT {
    pub drm_format_modifier: u64,
    pub drm_format_modifier_plane_count: u32,
    pub drm_format_modifier_tiling_features: crate::vk1_0::FormatFeatureFlags,
}
impl Default for DrmFormatModifierPropertiesEXT {
    fn default() -> Self {
        Self { drm_format_modifier: Default::default(), drm_format_modifier_plane_count: Default::default(), drm_format_modifier_tiling_features: Default::default() }
    }
}
impl std::fmt::Debug for DrmFormatModifierPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DrmFormatModifierPropertiesEXT").field("drm_format_modifier", &self.drm_format_modifier).field("drm_format_modifier_plane_count", &self.drm_format_modifier_plane_count).field("drm_format_modifier_tiling_features", &self.drm_format_modifier_tiling_features).finish()
    }
}
impl DrmFormatModifierPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DrmFormatModifierPropertiesEXTBuilder<'a> {
        DrmFormatModifierPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDrmFormatModifierPropertiesEXT.html)) · Builder of [`DrmFormatModifierPropertiesEXT`] <br/> VkDrmFormatModifierPropertiesEXT - Structure specifying properties of a format when combined with a DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DrmFormatModifierPropertiesEXT`] structure describes properties of\na [`crate::vk::Format`] when that format is combined with a[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\nThese properties, like those of [`crate::vk::FormatProperties2`], are independent\nof any particular image.\n\nThe [`crate::vk::DrmFormatModifierPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkDrmFormatModifierPropertiesEXT {\n    uint64_t                drmFormatModifier;\n    uint32_t                drmFormatModifierPlaneCount;\n    VkFormatFeatureFlags    drmFormatModifierTilingFeatures;\n} VkDrmFormatModifierPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::drm_format_modifier`] is a *Linux DRM format modifier*.\n\n* [`Self::drm_format_modifier_plane_count`] is the number of *memory planes* in\n  any image created with `format` and [`Self::drm_format_modifier`].\n  An image’s *memory planecount* is distinct from its *format planecount*,\n  as explained below.\n\n* [`Self::drm_format_modifier_tiling_features`] is a bitmask of[VkFormatFeatureFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFormatFeatureFlagBits.html) that are supported by any image created\n  with `format` and [`Self::drm_format_modifier`].\n[](#_description)Description\n----------\n\nThe returned [`Self::drm_format_modifier_tiling_features`] **must** contain at least\none bit.\n\nThe implementation **must** not return `DRM_FORMAT_MOD_INVALID` in[`Self::drm_format_modifier`].\n\nAn image’s *memory planecount* (as returned by[`Self::drm_format_modifier_plane_count`]) is distinct from its *format planecount*(in the sense of [multi-planar](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#formats-requiring-sampler-ycbcr-conversion)Y′C<sub>B</sub>C<sub>R</sub> formats).\nIn [`crate::vk::ImageAspectFlagBits`], each`VK_IMAGE_ASPECT_MEMORY_PLANE`**i*\\_BIT\\_EXT represents a \\_memory plane*and each `VK_IMAGE_ASPECT_PLANE`**i*\\_BIT a \\_format plane*.\n\nAn image’s set of *format planes* is an ordered partition of the image’s**content** into separable groups of format channels.\nThe ordered partition is encoded in the name of each [`crate::vk::Format`].\nFor example, [`crate::vk::Format::G8_B8R8_2PLANE_420_UNORM`] contains two *format\nplanes*; the first plane contains the green channel and the second plane\ncontains the blue channel and red channel.\nIf the format name does not contain `PLANE`, then the format contains a\nsingle plane; for example, [`crate::vk::Format::R8G8B8A8_UNORM`].\nSome commands, such as [`crate::vk::PFN_vkCmdCopyBufferToImage`], do not operate on all\nformat channels in the image, but instead operate only on the *format\nplanes* explicitly chosen by the application and operate on each *format\nplane* independently.\n\nAn image’s set of *memory planes* is an ordered partition of the image’s**memory** rather than the image’s **content**.\nEach *memory plane* is a contiguous range of memory.\nThe union of an image’s *memory planes* is not necessarily contiguous.\n\nIf an image is [linear](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-linear-resource), then the partition is\nthe same for *memory planes* and for *format planes*.\nTherefore, if the returned [`Self::drm_format_modifier`] is`DRM_FORMAT_MOD_LINEAR`, then [`Self::drm_format_modifier_plane_count`] **must**equal the *format planecount*, and [`Self::drm_format_modifier_tiling_features`]**must** be identical to the[`crate::vk::FormatProperties2::linear_tiling_features`] returned in the same`pNext` chain.\n\nIf an image is [non-linear](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-linear-resource), then the partition\nof the image’s **memory** into *memory planes* is implementation-specific and**may** be unrelated to the partition of the image’s **content** into *format\nplanes*.\nFor example, consider an image whose `format` is[`crate::vk::Format::G8_B8_R8_3PLANE_420_UNORM`], `tiling` is[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`], whose [`Self::drm_format_modifier`]is not `DRM_FORMAT_MOD_LINEAR`, and `flags` lacks[`crate::vk::ImageCreateFlagBits::DISJOINT`].\nThe image has 3 *format planes*, and commands such[`crate::vk::PFN_vkCmdCopyBufferToImage`] act on each *format plane* independently as if\nthe data of each *format plane* were separable from the data of the other\nplanes.\nIn a straightforward implementation, the implementation **may** store the\nimage’s content in 3 adjacent *memory planes* where each *memory plane*corresponds exactly to a *format plane*.\nHowever, the implementation **may** also store the image’s content in a single*memory plane* where all format channels are combined using an\nimplementation-private block-compressed format; or the implementation **may**store the image’s content in a collection of 7 adjacent *memory planes*using an implementation-private sharding technique.\nBecause the image is non-linear and non-disjoint, the implementation has\nmuch freedom when choosing the image’s placement in memory.\n\nThe *memory planecount* applies to function parameters and structures only\nwhen the API specifies an explicit requirement on[`Self::drm_format_modifier_plane_count`].\nIn all other cases, the *memory planecount* is ignored.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DrmFormatModifierPropertiesListEXT`], [`crate::vk::FormatFeatureFlagBits`]\n"]
#[repr(transparent)]
pub struct DrmFormatModifierPropertiesEXTBuilder<'a>(DrmFormatModifierPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DrmFormatModifierPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DrmFormatModifierPropertiesEXTBuilder<'a> {
        DrmFormatModifierPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifier(mut self, drm_format_modifier: u64) -> Self {
        self.0.drm_format_modifier = drm_format_modifier as _;
        self
    }
    #[inline]
    pub fn drm_format_modifier_plane_count(mut self, drm_format_modifier_plane_count: u32) -> Self {
        self.0.drm_format_modifier_plane_count = drm_format_modifier_plane_count as _;
        self
    }
    #[inline]
    pub fn drm_format_modifier_tiling_features(mut self, drm_format_modifier_tiling_features: crate::vk1_0::FormatFeatureFlags) -> Self {
        self.0.drm_format_modifier_tiling_features = drm_format_modifier_tiling_features as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DrmFormatModifierPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for DrmFormatModifierPropertiesEXTBuilder<'a> {
    fn default() -> DrmFormatModifierPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DrmFormatModifierPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DrmFormatModifierPropertiesEXTBuilder<'a> {
    type Target = DrmFormatModifierPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DrmFormatModifierPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageDrmFormatModifierInfoEXT.html)) · Structure <br/> VkPhysicalDeviceImageDrmFormatModifierInfoEXT - Structure specifying a DRM format modifier as image creation parameter\n[](#_c_specification)C Specification\n----------\n\nTo query the image capabilities that are compatible with a[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier), set[`crate::vk::PhysicalDeviceImageFormatInfo2::tiling`] to[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`] and add a[`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`] structure to the[`Self::p_next`] chain of [`crate::vk::PhysicalDeviceImageFormatInfo2`].\n\nThe [`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkPhysicalDeviceImageDrmFormatModifierInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint64_t           drmFormatModifier;\n    VkSharingMode      sharingMode;\n    uint32_t           queueFamilyIndexCount;\n    const uint32_t*    pQueueFamilyIndices;\n} VkPhysicalDeviceImageDrmFormatModifierInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] is the image’s *Linux DRM format modifier*,\n  corresponding to[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT::modifier`] or\n  to [`crate::vk::ImageDrmFormatModifierListCreateInfoEXT::p_modifiers`].\n\n* [`Self::sharing_mode`] specifies how the image will be accessed by multiple\n  queue families.\n\n* [`Self::queue_family_index_count`] is the number of entries in the[`Self::p_queue_family_indices`] array.\n\n* [`Self::p_queue_family_indices`] is a pointer to an array of queue families\n  that will access the image.\n  It is ignored if [`Self::sharing_mode`] is not[`crate::vk::SharingMode::CONCURRENT`].\n[](#_description)Description\n----------\n\nIf the [`Self::drm_format_modifier`] is incompatible with the parameters specified\nin [`crate::vk::PhysicalDeviceImageFormatInfo2`] and its [`Self::p_next`] chain, then[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`] returns[`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`].\nThe implementation **must** support the query of any [`Self::drm_format_modifier`],\nincluding unknown and invalid modifier values.\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02314  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], then[`Self::p_queue_family_indices`] **must** be a valid pointer to an array of[`Self::queue_family_index_count`] `uint32_t` values\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02315  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], then[`Self::queue_family_index_count`] **must** be greater than `1`\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02316  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], each element\n  of [`Self::p_queue_family_indices`] **must** be unique and **must** be less than the`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties2`] for the`physicalDevice` that was used to create `device`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT`]\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-parameter  \n  [`Self::sharing_mode`] **must** be a valid [`crate::vk::SharingMode`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SharingMode`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceImageDrmFormatModifierInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceImageDrmFormatModifierInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub drm_format_modifier: u64,
    pub sharing_mode: crate::vk1_0::SharingMode,
    pub queue_family_index_count: u32,
    pub p_queue_family_indices: *const u32,
}
impl PhysicalDeviceImageDrmFormatModifierInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT;
}
impl Default for PhysicalDeviceImageDrmFormatModifierInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), drm_format_modifier: Default::default(), sharing_mode: Default::default(), queue_family_index_count: Default::default(), p_queue_family_indices: std::ptr::null() }
    }
}
impl std::fmt::Debug for PhysicalDeviceImageDrmFormatModifierInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceImageDrmFormatModifierInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("drm_format_modifier", &self.drm_format_modifier).field("sharing_mode", &self.sharing_mode).field("queue_family_index_count", &self.queue_family_index_count).field("p_queue_family_indices", &self.p_queue_family_indices).finish()
    }
}
impl PhysicalDeviceImageDrmFormatModifierInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
        PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceImageDrmFormatModifierInfoEXT.html)) · Builder of [`PhysicalDeviceImageDrmFormatModifierInfoEXT`] <br/> VkPhysicalDeviceImageDrmFormatModifierInfoEXT - Structure specifying a DRM format modifier as image creation parameter\n[](#_c_specification)C Specification\n----------\n\nTo query the image capabilities that are compatible with a[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier), set[`crate::vk::PhysicalDeviceImageFormatInfo2::tiling`] to[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`] and add a[`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`] structure to the[`Self::p_next`] chain of [`crate::vk::PhysicalDeviceImageFormatInfo2`].\n\nThe [`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkPhysicalDeviceImageDrmFormatModifierInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint64_t           drmFormatModifier;\n    VkSharingMode      sharingMode;\n    uint32_t           queueFamilyIndexCount;\n    const uint32_t*    pQueueFamilyIndices;\n} VkPhysicalDeviceImageDrmFormatModifierInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] is the image’s *Linux DRM format modifier*,\n  corresponding to[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT::modifier`] or\n  to [`crate::vk::ImageDrmFormatModifierListCreateInfoEXT::p_modifiers`].\n\n* [`Self::sharing_mode`] specifies how the image will be accessed by multiple\n  queue families.\n\n* [`Self::queue_family_index_count`] is the number of entries in the[`Self::p_queue_family_indices`] array.\n\n* [`Self::p_queue_family_indices`] is a pointer to an array of queue families\n  that will access the image.\n  It is ignored if [`Self::sharing_mode`] is not[`crate::vk::SharingMode::CONCURRENT`].\n[](#_description)Description\n----------\n\nIf the [`Self::drm_format_modifier`] is incompatible with the parameters specified\nin [`crate::vk::PhysicalDeviceImageFormatInfo2`] and its [`Self::p_next`] chain, then[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties2`] returns[`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`].\nThe implementation **must** support the query of any [`Self::drm_format_modifier`],\nincluding unknown and invalid modifier values.\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02314  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], then[`Self::p_queue_family_indices`] **must** be a valid pointer to an array of[`Self::queue_family_index_count`] `uint32_t` values\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02315  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], then[`Self::queue_family_index_count`] **must** be greater than `1`\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-02316  \n   If [`Self::sharing_mode`] is [`crate::vk::SharingMode::CONCURRENT`], each element\n  of [`Self::p_queue_family_indices`] **must** be unique and **must** be less than the`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties2`] for the`physicalDevice` that was used to create `device`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT`]\n\n* []() VUID-VkPhysicalDeviceImageDrmFormatModifierInfoEXT-sharingMode-parameter  \n  [`Self::sharing_mode`] **must** be a valid [`crate::vk::SharingMode`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SharingMode`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a>(PhysicalDeviceImageDrmFormatModifierInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
        PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifier(mut self, drm_format_modifier: u64) -> Self {
        self.0.drm_format_modifier = drm_format_modifier as _;
        self
    }
    #[inline]
    pub fn sharing_mode(mut self, sharing_mode: crate::vk1_0::SharingMode) -> Self {
        self.0.sharing_mode = sharing_mode as _;
        self
    }
    #[inline]
    pub fn queue_family_indices(mut self, queue_family_indices: &'a [u32]) -> Self {
        self.0.p_queue_family_indices = queue_family_indices.as_ptr() as _;
        self.0.queue_family_index_count = queue_family_indices.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceImageDrmFormatModifierInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
    fn default() -> PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
    type Target = PhysicalDeviceImageDrmFormatModifierInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceImageDrmFormatModifierInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierListCreateInfoEXT.html)) · Structure <br/> VkImageDrmFormatModifierListCreateInfoEXT - Specify that an image must be created with a DRM format modifier from the provided list\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::ImageCreateInfo`] includes a[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`] structure, then the image\nwill be created with one of the [Linux DRM\nformat modifiers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) listed in the structure.\nThe choice of modifier is implementation-dependent.\n\nThe [`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierListCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           drmFormatModifierCount;\n    const uint64_t*    pDrmFormatModifiers;\n} VkImageDrmFormatModifierListCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier_count`] is the length of the[`Self::p_drm_format_modifiers`] array.\n\n* [`Self::p_drm_format_modifiers`] is a pointer to an array of *Linux DRM format\n  modifiers*.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-pDrmFormatModifiers-02263  \n   Each *modifier* in [`Self::p_drm_format_modifiers`] **must** be compatible with\n  the parameters in [`crate::vk::ImageCreateInfo`] and its [`Self::p_next`] chain, as\n  determined by querying [`crate::vk::PhysicalDeviceImageFormatInfo2`] extended\n  with [`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-pDrmFormatModifiers-parameter  \n  [`Self::p_drm_format_modifiers`] **must** be a valid pointer to an array of [`Self::drm_format_modifier_count`] `uint64_t` values\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-drmFormatModifierCount-arraylength  \n  [`Self::drm_format_modifier_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImageDrmFormatModifierListCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageDrmFormatModifierListCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub drm_format_modifier_count: u32,
    pub p_drm_format_modifiers: *const u64,
}
impl ImageDrmFormatModifierListCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT;
}
impl Default for ImageDrmFormatModifierListCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), drm_format_modifier_count: Default::default(), p_drm_format_modifiers: std::ptr::null() }
    }
}
impl std::fmt::Debug for ImageDrmFormatModifierListCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageDrmFormatModifierListCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("drm_format_modifier_count", &self.drm_format_modifier_count).field("p_drm_format_modifiers", &self.p_drm_format_modifiers).finish()
    }
}
impl ImageDrmFormatModifierListCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
        ImageDrmFormatModifierListCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierListCreateInfoEXT.html)) · Builder of [`ImageDrmFormatModifierListCreateInfoEXT`] <br/> VkImageDrmFormatModifierListCreateInfoEXT - Specify that an image must be created with a DRM format modifier from the provided list\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::ImageCreateInfo`] includes a[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`] structure, then the image\nwill be created with one of the [Linux DRM\nformat modifiers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) listed in the structure.\nThe choice of modifier is implementation-dependent.\n\nThe [`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierListCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           drmFormatModifierCount;\n    const uint64_t*    pDrmFormatModifiers;\n} VkImageDrmFormatModifierListCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier_count`] is the length of the[`Self::p_drm_format_modifiers`] array.\n\n* [`Self::p_drm_format_modifiers`] is a pointer to an array of *Linux DRM format\n  modifiers*.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-pDrmFormatModifiers-02263  \n   Each *modifier* in [`Self::p_drm_format_modifiers`] **must** be compatible with\n  the parameters in [`crate::vk::ImageCreateInfo`] and its [`Self::p_next`] chain, as\n  determined by querying [`crate::vk::PhysicalDeviceImageFormatInfo2`] extended\n  with [`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-pDrmFormatModifiers-parameter  \n  [`Self::p_drm_format_modifiers`] **must** be a valid pointer to an array of [`Self::drm_format_modifier_count`] `uint64_t` values\n\n* []() VUID-VkImageDrmFormatModifierListCreateInfoEXT-drmFormatModifierCount-arraylength  \n  [`Self::drm_format_modifier_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImageDrmFormatModifierListCreateInfoEXTBuilder<'a>(ImageDrmFormatModifierListCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
        ImageDrmFormatModifierListCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifiers(mut self, drm_format_modifiers: &'a [u64]) -> Self {
        self.0.p_drm_format_modifiers = drm_format_modifiers.as_ptr() as _;
        self.0.drm_format_modifier_count = drm_format_modifiers.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageDrmFormatModifierListCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
    fn default() -> ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
    type Target = ImageDrmFormatModifierListCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageDrmFormatModifierListCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierExplicitCreateInfoEXT.html)) · Structure <br/> VkImageDrmFormatModifierExplicitCreateInfoEXT - Specify that an image be created with the provided DRM format modifier and explicit memory layout\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::ImageCreateInfo`] includes a[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`] structure, then the\nimage will be created with the [Linux DRM\nformat modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) and memory layout defined by the structure.\n\nThe [`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierExplicitCreateInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    uint64_t                      drmFormatModifier;\n    uint32_t                      drmFormatModifierPlaneCount;\n    const VkSubresourceLayout*    pPlaneLayouts;\n} VkImageDrmFormatModifierExplicitCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] is the *Linux DRM format modifier* with which\n  the image will be created.\n\n* [`Self::drm_format_modifier_plane_count`] is the number of *memory planes* in\n  the image (as reported by [`crate::vk::DrmFormatModifierPropertiesEXT`]) as\n  well as the length of the [`Self::p_plane_layouts`] array.\n\n* [`Self::p_plane_layouts`] is a pointer to an array of[`crate::vk::SubresourceLayout`] structures describing the image’s *memory\n  planes*.\n[](#_description)Description\n----------\n\nThe `i`<sup>th</sup> member of [`Self::p_plane_layouts`] describes the layout of the\nimage’s `i`<sup>th</sup> *memory plane* (that is,`VK_IMAGE_ASPECT_MEMORY_PLANE_i_BIT_EXT`).\nIn each element of [`Self::p_plane_layouts`], the implementation **must** ignore`size`.\nThe implementation calculates the size of each plane, which the application**can** query with [`crate::vk::PFN_vkGetImageSubresourceLayout`].\n\nWhen creating an image with[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`], it is the application’s\nresponsibility to satisfy all valid usage requirements.\nHowever, the implementation **must** validate that the provided[`Self::p_plane_layouts`], when combined with the provided [`Self::drm_format_modifier`]and other creation parameters in [`crate::vk::ImageCreateInfo`] and its [`Self::p_next`]chain, produce a valid image.\n(This validation is necessarily implementation-dependent and outside the\nscope of Vulkan, and therefore not described by valid usage requirements).\nIf this validation fails, then [`crate::vk::PFN_vkCreateImage`] returns[`crate::vk::Result::ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT`].\n\nValid Usage\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-drmFormatModifier-02264  \n  [`Self::drm_format_modifier`] **must** be compatible with the parameters in[`crate::vk::ImageCreateInfo`] and its [`Self::p_next`] chain, as determined by\n  querying [`crate::vk::PhysicalDeviceImageFormatInfo2`] extended with[`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-drmFormatModifierPlaneCount-02265  \n  [`Self::drm_format_modifier_plane_count`] **must** be equal to the[`crate::vk::DrmFormatModifierPropertiesEXT::drm_format_modifier_plane_count`]associated with [`crate::vk::ImageCreateInfo::format`] and[`Self::drm_format_modifier`], as found by querying[`crate::vk::DrmFormatModifierPropertiesListEXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-size-02267  \n   For each element of [`Self::p_plane_layouts`], `size` **must** be 0\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-arrayPitch-02268  \n   For each element of [`Self::p_plane_layouts`], `arrayPitch` **must** be 0 if[`crate::vk::ImageCreateInfo::array_layers`] is 1\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-depthPitch-02269  \n   For each element of [`Self::p_plane_layouts`], `depthPitch` **must** be 0 if[`crate::vk::ImageCreateInfo`]::`extent.depth` is 1\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-pPlaneLayouts-parameter  \n   If [`Self::drm_format_modifier_plane_count`] is not `0`, [`Self::p_plane_layouts`] **must** be a valid pointer to an array of [`Self::drm_format_modifier_plane_count`] [`crate::vk::SubresourceLayout`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SubresourceLayout`]\n"]
#[doc(alias = "VkImageDrmFormatModifierExplicitCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageDrmFormatModifierExplicitCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub drm_format_modifier: u64,
    pub drm_format_modifier_plane_count: u32,
    pub p_plane_layouts: *const crate::vk1_0::SubresourceLayout,
}
impl ImageDrmFormatModifierExplicitCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT;
}
impl Default for ImageDrmFormatModifierExplicitCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), drm_format_modifier: Default::default(), drm_format_modifier_plane_count: Default::default(), p_plane_layouts: std::ptr::null() }
    }
}
impl std::fmt::Debug for ImageDrmFormatModifierExplicitCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageDrmFormatModifierExplicitCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("drm_format_modifier", &self.drm_format_modifier).field("drm_format_modifier_plane_count", &self.drm_format_modifier_plane_count).field("p_plane_layouts", &self.p_plane_layouts).finish()
    }
}
impl ImageDrmFormatModifierExplicitCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
        ImageDrmFormatModifierExplicitCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierExplicitCreateInfoEXT.html)) · Builder of [`ImageDrmFormatModifierExplicitCreateInfoEXT`] <br/> VkImageDrmFormatModifierExplicitCreateInfoEXT - Specify that an image be created with the provided DRM format modifier and explicit memory layout\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::ImageCreateInfo`] includes a[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`] structure, then the\nimage will be created with the [Linux DRM\nformat modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier) and memory layout defined by the structure.\n\nThe [`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierExplicitCreateInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    uint64_t                      drmFormatModifier;\n    uint32_t                      drmFormatModifierPlaneCount;\n    const VkSubresourceLayout*    pPlaneLayouts;\n} VkImageDrmFormatModifierExplicitCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] is the *Linux DRM format modifier* with which\n  the image will be created.\n\n* [`Self::drm_format_modifier_plane_count`] is the number of *memory planes* in\n  the image (as reported by [`crate::vk::DrmFormatModifierPropertiesEXT`]) as\n  well as the length of the [`Self::p_plane_layouts`] array.\n\n* [`Self::p_plane_layouts`] is a pointer to an array of[`crate::vk::SubresourceLayout`] structures describing the image’s *memory\n  planes*.\n[](#_description)Description\n----------\n\nThe `i`<sup>th</sup> member of [`Self::p_plane_layouts`] describes the layout of the\nimage’s `i`<sup>th</sup> *memory plane* (that is,`VK_IMAGE_ASPECT_MEMORY_PLANE_i_BIT_EXT`).\nIn each element of [`Self::p_plane_layouts`], the implementation **must** ignore`size`.\nThe implementation calculates the size of each plane, which the application**can** query with [`crate::vk::PFN_vkGetImageSubresourceLayout`].\n\nWhen creating an image with[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`], it is the application’s\nresponsibility to satisfy all valid usage requirements.\nHowever, the implementation **must** validate that the provided[`Self::p_plane_layouts`], when combined with the provided [`Self::drm_format_modifier`]and other creation parameters in [`crate::vk::ImageCreateInfo`] and its [`Self::p_next`]chain, produce a valid image.\n(This validation is necessarily implementation-dependent and outside the\nscope of Vulkan, and therefore not described by valid usage requirements).\nIf this validation fails, then [`crate::vk::PFN_vkCreateImage`] returns[`crate::vk::Result::ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT`].\n\nValid Usage\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-drmFormatModifier-02264  \n  [`Self::drm_format_modifier`] **must** be compatible with the parameters in[`crate::vk::ImageCreateInfo`] and its [`Self::p_next`] chain, as determined by\n  querying [`crate::vk::PhysicalDeviceImageFormatInfo2`] extended with[`crate::vk::PhysicalDeviceImageDrmFormatModifierInfoEXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-drmFormatModifierPlaneCount-02265  \n  [`Self::drm_format_modifier_plane_count`] **must** be equal to the[`crate::vk::DrmFormatModifierPropertiesEXT::drm_format_modifier_plane_count`]associated with [`crate::vk::ImageCreateInfo::format`] and[`Self::drm_format_modifier`], as found by querying[`crate::vk::DrmFormatModifierPropertiesListEXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-size-02267  \n   For each element of [`Self::p_plane_layouts`], `size` **must** be 0\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-arrayPitch-02268  \n   For each element of [`Self::p_plane_layouts`], `arrayPitch` **must** be 0 if[`crate::vk::ImageCreateInfo::array_layers`] is 1\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-depthPitch-02269  \n   For each element of [`Self::p_plane_layouts`], `depthPitch` **must** be 0 if[`crate::vk::ImageCreateInfo`]::`extent.depth` is 1\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierExplicitCreateInfoEXT-pPlaneLayouts-parameter  \n   If [`Self::drm_format_modifier_plane_count`] is not `0`, [`Self::p_plane_layouts`] **must** be a valid pointer to an array of [`Self::drm_format_modifier_plane_count`] [`crate::vk::SubresourceLayout`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SubresourceLayout`]\n"]
#[repr(transparent)]
pub struct ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a>(ImageDrmFormatModifierExplicitCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
        ImageDrmFormatModifierExplicitCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifier(mut self, drm_format_modifier: u64) -> Self {
        self.0.drm_format_modifier = drm_format_modifier as _;
        self
    }
    #[inline]
    pub fn plane_layouts(mut self, plane_layouts: &'a [crate::vk1_0::SubresourceLayoutBuilder]) -> Self {
        self.0.p_plane_layouts = plane_layouts.as_ptr() as _;
        self.0.drm_format_modifier_plane_count = plane_layouts.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageDrmFormatModifierExplicitCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
    fn default() -> ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
    type Target = ImageDrmFormatModifierExplicitCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageDrmFormatModifierExplicitCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierPropertiesEXT.html)) · Structure <br/> VkImageDrmFormatModifierPropertiesEXT - Properties of an image's Linux DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           drmFormatModifier;\n} VkImageDrmFormatModifierPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] returns the image’s[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\n[](#_description)Description\n----------\n\nIf the `image` was created with[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`], then the returned[`Self::drm_format_modifier`] **must** belong to the list of modifiers provided at\ntime of image creation in[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT::p_drm_format_modifiers`].\nIf the `image` was created with[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`], then the returned[`Self::drm_format_modifier`] **must** be the modifier provided at time of image\ncreation in[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT::drm_format_modifier`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_drm_format_modifier_properties_ext`]\n"]
#[doc(alias = "VkImageDrmFormatModifierPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageDrmFormatModifierPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub drm_format_modifier: u64,
}
impl ImageDrmFormatModifierPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT;
}
impl Default for ImageDrmFormatModifierPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), drm_format_modifier: Default::default() }
    }
}
impl std::fmt::Debug for ImageDrmFormatModifierPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageDrmFormatModifierPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("drm_format_modifier", &self.drm_format_modifier).finish()
    }
}
impl ImageDrmFormatModifierPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
        ImageDrmFormatModifierPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierPropertiesEXT.html)) · Builder of [`ImageDrmFormatModifierPropertiesEXT`] <br/> VkImageDrmFormatModifierPropertiesEXT - Properties of an image's Linux DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\ntypedef struct VkImageDrmFormatModifierPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint64_t           drmFormatModifier;\n} VkImageDrmFormatModifierPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::drm_format_modifier`] returns the image’s[Linux DRM format modifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\n[](#_description)Description\n----------\n\nIf the `image` was created with[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT`], then the returned[`Self::drm_format_modifier`] **must** belong to the list of modifiers provided at\ntime of image creation in[`crate::vk::ImageDrmFormatModifierListCreateInfoEXT::p_drm_format_modifiers`].\nIf the `image` was created with[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT`], then the returned[`Self::drm_format_modifier`] **must** be the modifier provided at time of image\ncreation in[`crate::vk::ImageDrmFormatModifierExplicitCreateInfoEXT::drm_format_modifier`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageDrmFormatModifierPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT`]\n\n* []() VUID-VkImageDrmFormatModifierPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_drm_format_modifier_properties_ext`]\n"]
#[repr(transparent)]
pub struct ImageDrmFormatModifierPropertiesEXTBuilder<'a>(ImageDrmFormatModifierPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
        ImageDrmFormatModifierPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn drm_format_modifier(mut self, drm_format_modifier: u64) -> Self {
        self.0.drm_format_modifier = drm_format_modifier as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageDrmFormatModifierPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
    fn default() -> ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
    type Target = ImageDrmFormatModifierPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageDrmFormatModifierPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_image_drm_format_modifier`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageDrmFormatModifierPropertiesEXT.html)) · Function <br/> vkGetImageDrmFormatModifierPropertiesEXT - Returns an image's DRM format modifier\n[](#_c_specification)C Specification\n----------\n\nIf an image was created with [`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`],\nthen the image has a [Linux DRM format\nmodifier](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#glossary-drm-format-modifier).\nTo query the *modifier*, call:\n\n```\n// Provided by VK_EXT_image_drm_format_modifier\nVkResult vkGetImageDrmFormatModifierPropertiesEXT(\n    VkDevice                                    device,\n    VkImage                                     image,\n    VkImageDrmFormatModifierPropertiesEXT*      pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image.\n\n* [`Self::image`] is the queried image.\n\n* [`Self::p_properties`] is a pointer to a[`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure in which\n  properties of the image’s *DRM format modifier* are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-02272  \n  [`Self::image`] **must** have been created with[`tiling`](VkImageCreateInfo.html) equal to[`crate::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-parameter  \n  [`Self::image`] **must** be a valid [`crate::vk::Image`] handle\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::ImageDrmFormatModifierPropertiesEXT`] structure\n\n* []() VUID-vkGetImageDrmFormatModifierPropertiesEXT-image-parent  \n  [`Self::image`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::Image`], [`crate::vk::ImageDrmFormatModifierPropertiesEXT`]\n"]
    #[doc(alias = "vkGetImageDrmFormatModifierPropertiesEXT")]
    pub unsafe fn get_image_drm_format_modifier_properties_ext(&self, image: crate::vk1_0::Image, properties: Option<crate::extensions::ext_image_drm_format_modifier::ImageDrmFormatModifierPropertiesEXT>) -> crate::utils::VulkanResult<crate::extensions::ext_image_drm_format_modifier::ImageDrmFormatModifierPropertiesEXT> {
        let _function = self.get_image_drm_format_modifier_properties_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut properties = match properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, image as _, &mut properties);
        crate::utils::VulkanResult::new(_return, {
            properties.p_next = std::ptr::null_mut() as _;
            properties
        })
    }
}
