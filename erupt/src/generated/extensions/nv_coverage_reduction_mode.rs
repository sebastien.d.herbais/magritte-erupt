#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_COVERAGE_REDUCTION_MODE_SPEC_VERSION")]
pub const NV_COVERAGE_REDUCTION_MODE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_COVERAGE_REDUCTION_MODE_EXTENSION_NAME")]
pub const NV_COVERAGE_REDUCTION_MODE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_coverage_reduction_mode");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SUPPORTED_FRAMEBUFFER_MIXED_SAMPLES_COMBINATIONS_NV: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageReductionStateCreateFlagsNV.html)) · Bitmask of [`PipelineCoverageReductionStateCreateFlagBitsNV`] <br/> VkPipelineCoverageReductionStateCreateFlagsNV - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef VkFlags VkPipelineCoverageReductionStateCreateFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineCoverageReductionStateCreateFlagBitsNV`] is a bitmask type for\nsetting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineCoverageReductionStateCreateInfoNV`]\n"] # [doc (alias = "VkPipelineCoverageReductionStateCreateFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct PipelineCoverageReductionStateCreateFlagsNV : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineCoverageReductionStateCreateFlagsNV`] <br/> "]
#[doc(alias = "VkPipelineCoverageReductionStateCreateFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineCoverageReductionStateCreateFlagBitsNV(pub u32);
impl PipelineCoverageReductionStateCreateFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineCoverageReductionStateCreateFlagsNV {
        PipelineCoverageReductionStateCreateFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineCoverageReductionStateCreateFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_coverage_reduction_mode`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV: Self = Self(1000250000);
    pub const PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV: Self = Self(1000250001);
    pub const FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV: Self = Self(1000250002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoverageReductionModeNV.html)) · Enum <br/> VkCoverageReductionModeNV - Specify the coverage reduction mode\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineCoverageReductionStateCreateInfoNV::coverage_reduction_mode`],\nspecifying how color sample coverage is generated from pixel coverage, are:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef enum VkCoverageReductionModeNV {\n    VK_COVERAGE_REDUCTION_MODE_MERGE_NV = 0,\n    VK_COVERAGE_REDUCTION_MODE_TRUNCATE_NV = 1,\n} VkCoverageReductionModeNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::MERGE_NV`] specifies that each color\n  sample will be associated with an implementation-dependent subset of\n  samples in the pixel coverage.\n  If any of those associated samples are covered, the color sample is\n  covered.\n\n* [`Self::TRUNCATE_NV`] specifies that for color\n  samples present in the color attachments, a color sample is covered if\n  the pixel coverage sample with the same[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) i is\n  covered; other pixel coverage samples are discarded.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FramebufferMixedSamplesCombinationNV`], [`crate::vk::PipelineCoverageReductionStateCreateInfoNV`]\n"]
#[doc(alias = "VkCoverageReductionModeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct CoverageReductionModeNV(pub i32);
impl std::fmt::Debug for CoverageReductionModeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::MERGE_NV => "MERGE_NV",
            &Self::TRUNCATE_NV => "TRUNCATE_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_coverage_reduction_mode`]"]
impl crate::extensions::nv_coverage_reduction_mode::CoverageReductionModeNV {
    pub const MERGE_NV: Self = Self(0);
    pub const TRUNCATE_NV: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV.html)) · Function <br/> vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV - Query supported sample count combinations\n[](#_c_specification)C Specification\n----------\n\nTo query the set of mixed sample combinations of coverage reduction mode,\nrasterization samples and color, depth, stencil attachment sample counts\nthat are supported by a physical device, call:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\nVkResult vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pCombinationCount,\n    VkFramebufferMixedSamplesCombinationNV*     pCombinations);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the set\n  of combinations.\n\n* [`Self::p_combination_count`] is a pointer to an integer related to the number\n  of combinations available or queried, as described below.\n\n* [`Self::p_combinations`] is either `NULL` or a pointer to an array of[`crate::vk::FramebufferMixedSamplesCombinationNV`] values, indicating the\n  supported combinations of coverage reduction mode, rasterization\n  samples, and color, depth, stencil attachment sample counts.\n[](#_description)Description\n----------\n\nIf [`Self::p_combinations`] is `NULL`, then the number of supported combinations\nfor the given [`Self::physical_device`] is returned in [`Self::p_combination_count`].\nOtherwise, [`Self::p_combination_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_combinations`] array, and on return\nthe variable is overwritten with the number of values actually written to[`Self::p_combinations`].\nIf the value of [`Self::p_combination_count`] is less than the number of\ncombinations supported for the given [`Self::physical_device`], at most[`Self::p_combination_count`] values will be written to [`Self::p_combinations`], and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the supported values were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-pCombinationCount-parameter  \n  [`Self::p_combination_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-pCombinations-parameter  \n   If the value referenced by [`Self::p_combination_count`] is not `0`, and [`Self::p_combinations`] is not `NULL`, [`Self::p_combinations`] **must** be a valid pointer to an array of [`Self::p_combination_count`] [`crate::vk::FramebufferMixedSamplesCombinationNV`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FramebufferMixedSamplesCombinationNV`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_combination_count: *mut u32, p_combinations: *mut crate::extensions::nv_coverage_reduction_mode::FramebufferMixedSamplesCombinationNV) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCoverageReductionModeFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCoverageReductionStateCreateInfoNV> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineCoverageReductionStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineMultisampleStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCoverageReductionModeFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCoverageReductionModeFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceCoverageReductionModeFeaturesNV - Structure describing the coverage reduction mode features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkPhysicalDeviceCoverageReductionModeFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           coverageReductionMode;\n} VkPhysicalDeviceCoverageReductionModeFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::coverage_reduction_mode`] indicates\n  whether the implementation supports coverage reduction modes.\n  See [Coverage Reduction](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-coverage-reduction).\n\nIf the [`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCoverageReductionModeFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceCoverageReductionModeFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceCoverageReductionModeFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub coverage_reduction_mode: crate::vk1_0::Bool32,
}
impl PhysicalDeviceCoverageReductionModeFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV;
}
impl Default for PhysicalDeviceCoverageReductionModeFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), coverage_reduction_mode: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceCoverageReductionModeFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceCoverageReductionModeFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("coverage_reduction_mode", &(self.coverage_reduction_mode != 0)).finish()
    }
}
impl PhysicalDeviceCoverageReductionModeFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
        PhysicalDeviceCoverageReductionModeFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCoverageReductionModeFeaturesNV.html)) · Builder of [`PhysicalDeviceCoverageReductionModeFeaturesNV`] <br/> VkPhysicalDeviceCoverageReductionModeFeaturesNV - Structure describing the coverage reduction mode features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkPhysicalDeviceCoverageReductionModeFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           coverageReductionMode;\n} VkPhysicalDeviceCoverageReductionModeFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::coverage_reduction_mode`] indicates\n  whether the implementation supports coverage reduction modes.\n  See [Coverage Reduction](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#fragops-coverage-reduction).\n\nIf the [`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCoverageReductionModeFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCoverageReductionModeFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a>(PhysicalDeviceCoverageReductionModeFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
        PhysicalDeviceCoverageReductionModeFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn coverage_reduction_mode(mut self, coverage_reduction_mode: bool) -> Self {
        self.0.coverage_reduction_mode = coverage_reduction_mode as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceCoverageReductionModeFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceCoverageReductionModeFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceCoverageReductionModeFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageReductionStateCreateInfoNV.html)) · Structure <br/> VkPipelineCoverageReductionStateCreateInfoNV - Structure specifying parameters controlling coverage reduction\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineCoverageReductionStateCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkPipelineCoverageReductionStateCreateInfoNV {\n    VkStructureType                                  sType;\n    const void*                                      pNext;\n    VkPipelineCoverageReductionStateCreateFlagsNV    flags;\n    VkCoverageReductionModeNV                        coverageReductionMode;\n} VkPipelineCoverageReductionStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::coverage_reduction_mode`] is a [`crate::vk::CoverageReductionModeNV`] value\n  controlling how color sample coverage is generated from pixel coverage.\n[](#_description)Description\n----------\n\nIf this structure is not included in the [`Self::p_next`] chain, or if the\nextension is not enabled, the default coverage reduction mode is inferred as\nfollows:\n\n* If the [VK_NV_framebuffer_mixed_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_NV_framebuffer_mixed_samples.html) extension is enabled, then\n  it is as if the [`Self::coverage_reduction_mode`] is[`crate::vk::CoverageReductionModeNV::MERGE_NV`].\n\n* If the [VK_AMD_mixed_attachment_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_mixed_attachment_samples.html) extension is enabled, then\n  it is as if the [`Self::coverage_reduction_mode`] is[`crate::vk::CoverageReductionModeNV::TRUNCATE_NV`].\n\n* If both [VK_NV_framebuffer_mixed_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_NV_framebuffer_mixed_samples.html) and[VK_AMD_mixed_attachment_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_mixed_attachment_samples.html) are enabled, then the default\n  coverage reduction mode is implementation-dependent.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-coverageReductionMode-parameter  \n  [`Self::coverage_reduction_mode`] **must** be a valid [`crate::vk::CoverageReductionModeNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoverageReductionModeNV`], [`crate::vk::PipelineCoverageReductionStateCreateFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineCoverageReductionStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineCoverageReductionStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::nv_coverage_reduction_mode::PipelineCoverageReductionStateCreateFlagsNV,
    pub coverage_reduction_mode: crate::extensions::nv_coverage_reduction_mode::CoverageReductionModeNV,
}
impl PipelineCoverageReductionStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV;
}
impl Default for PipelineCoverageReductionStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), coverage_reduction_mode: Default::default() }
    }
}
impl std::fmt::Debug for PipelineCoverageReductionStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineCoverageReductionStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("coverage_reduction_mode", &self.coverage_reduction_mode).finish()
    }
}
impl PipelineCoverageReductionStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
        PipelineCoverageReductionStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineCoverageReductionStateCreateInfoNV.html)) · Builder of [`PipelineCoverageReductionStateCreateInfoNV`] <br/> VkPipelineCoverageReductionStateCreateInfoNV - Structure specifying parameters controlling coverage reduction\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineCoverageReductionStateCreateInfoNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkPipelineCoverageReductionStateCreateInfoNV {\n    VkStructureType                                  sType;\n    const void*                                      pNext;\n    VkPipelineCoverageReductionStateCreateFlagsNV    flags;\n    VkCoverageReductionModeNV                        coverageReductionMode;\n} VkPipelineCoverageReductionStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::coverage_reduction_mode`] is a [`crate::vk::CoverageReductionModeNV`] value\n  controlling how color sample coverage is generated from pixel coverage.\n[](#_description)Description\n----------\n\nIf this structure is not included in the [`Self::p_next`] chain, or if the\nextension is not enabled, the default coverage reduction mode is inferred as\nfollows:\n\n* If the [VK_NV_framebuffer_mixed_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_NV_framebuffer_mixed_samples.html) extension is enabled, then\n  it is as if the [`Self::coverage_reduction_mode`] is[`crate::vk::CoverageReductionModeNV::MERGE_NV`].\n\n* If the [VK_AMD_mixed_attachment_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_mixed_attachment_samples.html) extension is enabled, then\n  it is as if the [`Self::coverage_reduction_mode`] is[`crate::vk::CoverageReductionModeNV::TRUNCATE_NV`].\n\n* If both [VK_NV_framebuffer_mixed_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_NV_framebuffer_mixed_samples.html) and[VK_AMD_mixed_attachment_samples](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_mixed_attachment_samples.html) are enabled, then the default\n  coverage reduction mode is implementation-dependent.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineCoverageReductionStateCreateInfoNV-coverageReductionMode-parameter  \n  [`Self::coverage_reduction_mode`] **must** be a valid [`crate::vk::CoverageReductionModeNV`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoverageReductionModeNV`], [`crate::vk::PipelineCoverageReductionStateCreateFlagBitsNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineCoverageReductionStateCreateInfoNVBuilder<'a>(PipelineCoverageReductionStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
        PipelineCoverageReductionStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::nv_coverage_reduction_mode::PipelineCoverageReductionStateCreateFlagsNV) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn coverage_reduction_mode(mut self, coverage_reduction_mode: crate::extensions::nv_coverage_reduction_mode::CoverageReductionModeNV) -> Self {
        self.0.coverage_reduction_mode = coverage_reduction_mode as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineCoverageReductionStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
    type Target = PipelineCoverageReductionStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineCoverageReductionStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferMixedSamplesCombinationNV.html)) · Structure <br/> VkFramebufferMixedSamplesCombinationNV - Structure specifying a supported sample count combination\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FramebufferMixedSamplesCombinationNV`] structure is defined as:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkFramebufferMixedSamplesCombinationNV {\n    VkStructureType              sType;\n    void*                        pNext;\n    VkCoverageReductionModeNV    coverageReductionMode;\n    VkSampleCountFlagBits        rasterizationSamples;\n    VkSampleCountFlags           depthStencilSamples;\n    VkSampleCountFlags           colorSamples;\n} VkFramebufferMixedSamplesCombinationNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::coverage_reduction_mode`] is a [`crate::vk::CoverageReductionModeNV`] value\n  specifying the coverage reduction mode.\n\n* [`Self::rasterization_samples`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) specifying\n  the number of rasterization samples in the supported combination.\n\n* [`Self::depth_stencil_samples`] specifies the number of samples in the depth\n  stencil attachment in the supported combination.\n  A value of 0 indicates the combination does not have a depth stencil\n  attachment.\n\n* [`Self::color_samples`] specifies the number of color samples in a color\n  attachment in the supported combination.\n  A value of 0 indicates the combination does not have a color attachment.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkFramebufferMixedSamplesCombinationNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV`]\n\n* []() VUID-VkFramebufferMixedSamplesCombinationNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoverageReductionModeNV`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::SampleCountFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_supported_framebuffer_mixed_samples_combinations_nv`]\n"]
#[doc(alias = "VkFramebufferMixedSamplesCombinationNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct FramebufferMixedSamplesCombinationNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub coverage_reduction_mode: crate::extensions::nv_coverage_reduction_mode::CoverageReductionModeNV,
    pub rasterization_samples: crate::vk1_0::SampleCountFlagBits,
    pub depth_stencil_samples: crate::vk1_0::SampleCountFlags,
    pub color_samples: crate::vk1_0::SampleCountFlags,
}
impl FramebufferMixedSamplesCombinationNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV;
}
impl Default for FramebufferMixedSamplesCombinationNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), coverage_reduction_mode: Default::default(), rasterization_samples: Default::default(), depth_stencil_samples: Default::default(), color_samples: Default::default() }
    }
}
impl std::fmt::Debug for FramebufferMixedSamplesCombinationNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("FramebufferMixedSamplesCombinationNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("coverage_reduction_mode", &self.coverage_reduction_mode).field("rasterization_samples", &self.rasterization_samples).field("depth_stencil_samples", &self.depth_stencil_samples).field("color_samples", &self.color_samples).finish()
    }
}
impl FramebufferMixedSamplesCombinationNV {
    #[inline]
    pub fn into_builder<'a>(self) -> FramebufferMixedSamplesCombinationNVBuilder<'a> {
        FramebufferMixedSamplesCombinationNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFramebufferMixedSamplesCombinationNV.html)) · Builder of [`FramebufferMixedSamplesCombinationNV`] <br/> VkFramebufferMixedSamplesCombinationNV - Structure specifying a supported sample count combination\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FramebufferMixedSamplesCombinationNV`] structure is defined as:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\ntypedef struct VkFramebufferMixedSamplesCombinationNV {\n    VkStructureType              sType;\n    void*                        pNext;\n    VkCoverageReductionModeNV    coverageReductionMode;\n    VkSampleCountFlagBits        rasterizationSamples;\n    VkSampleCountFlags           depthStencilSamples;\n    VkSampleCountFlags           colorSamples;\n} VkFramebufferMixedSamplesCombinationNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::coverage_reduction_mode`] is a [`crate::vk::CoverageReductionModeNV`] value\n  specifying the coverage reduction mode.\n\n* [`Self::rasterization_samples`] is a [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html) specifying\n  the number of rasterization samples in the supported combination.\n\n* [`Self::depth_stencil_samples`] specifies the number of samples in the depth\n  stencil attachment in the supported combination.\n  A value of 0 indicates the combination does not have a depth stencil\n  attachment.\n\n* [`Self::color_samples`] specifies the number of color samples in a color\n  attachment in the supported combination.\n  A value of 0 indicates the combination does not have a color attachment.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkFramebufferMixedSamplesCombinationNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV`]\n\n* []() VUID-VkFramebufferMixedSamplesCombinationNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoverageReductionModeNV`], [VkSampleCountFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSampleCountFlagBits.html), [`crate::vk::SampleCountFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_physical_device_supported_framebuffer_mixed_samples_combinations_nv`]\n"]
#[repr(transparent)]
pub struct FramebufferMixedSamplesCombinationNVBuilder<'a>(FramebufferMixedSamplesCombinationNV, std::marker::PhantomData<&'a ()>);
impl<'a> FramebufferMixedSamplesCombinationNVBuilder<'a> {
    #[inline]
    pub fn new() -> FramebufferMixedSamplesCombinationNVBuilder<'a> {
        FramebufferMixedSamplesCombinationNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn coverage_reduction_mode(mut self, coverage_reduction_mode: crate::extensions::nv_coverage_reduction_mode::CoverageReductionModeNV) -> Self {
        self.0.coverage_reduction_mode = coverage_reduction_mode as _;
        self
    }
    #[inline]
    pub fn rasterization_samples(mut self, rasterization_samples: crate::vk1_0::SampleCountFlagBits) -> Self {
        self.0.rasterization_samples = rasterization_samples as _;
        self
    }
    #[inline]
    pub fn depth_stencil_samples(mut self, depth_stencil_samples: crate::vk1_0::SampleCountFlags) -> Self {
        self.0.depth_stencil_samples = depth_stencil_samples as _;
        self
    }
    #[inline]
    pub fn color_samples(mut self, color_samples: crate::vk1_0::SampleCountFlags) -> Self {
        self.0.color_samples = color_samples as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> FramebufferMixedSamplesCombinationNV {
        self.0
    }
}
impl<'a> std::default::Default for FramebufferMixedSamplesCombinationNVBuilder<'a> {
    fn default() -> FramebufferMixedSamplesCombinationNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for FramebufferMixedSamplesCombinationNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for FramebufferMixedSamplesCombinationNVBuilder<'a> {
    type Target = FramebufferMixedSamplesCombinationNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for FramebufferMixedSamplesCombinationNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_coverage_reduction_mode`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV.html)) · Function <br/> vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV - Query supported sample count combinations\n[](#_c_specification)C Specification\n----------\n\nTo query the set of mixed sample combinations of coverage reduction mode,\nrasterization samples and color, depth, stencil attachment sample counts\nthat are supported by a physical device, call:\n\n```\n// Provided by VK_NV_coverage_reduction_mode\nVkResult vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pCombinationCount,\n    VkFramebufferMixedSamplesCombinationNV*     pCombinations);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the set\n  of combinations.\n\n* [`Self::p_combination_count`] is a pointer to an integer related to the number\n  of combinations available or queried, as described below.\n\n* [`Self::p_combinations`] is either `NULL` or a pointer to an array of[`crate::vk::FramebufferMixedSamplesCombinationNV`] values, indicating the\n  supported combinations of coverage reduction mode, rasterization\n  samples, and color, depth, stencil attachment sample counts.\n[](#_description)Description\n----------\n\nIf [`Self::p_combinations`] is `NULL`, then the number of supported combinations\nfor the given [`Self::physical_device`] is returned in [`Self::p_combination_count`].\nOtherwise, [`Self::p_combination_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_combinations`] array, and on return\nthe variable is overwritten with the number of values actually written to[`Self::p_combinations`].\nIf the value of [`Self::p_combination_count`] is less than the number of\ncombinations supported for the given [`Self::physical_device`], at most[`Self::p_combination_count`] values will be written to [`Self::p_combinations`], and[`crate::vk::Result::INCOMPLETE`] will be returned instead of [`crate::vk::Result::SUCCESS`], to\nindicate that not all the supported values were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-pCombinationCount-parameter  \n  [`Self::p_combination_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV-pCombinations-parameter  \n   If the value referenced by [`Self::p_combination_count`] is not `0`, and [`Self::p_combinations`] is not `NULL`, [`Self::p_combinations`] **must** be a valid pointer to an array of [`Self::p_combination_count`] [`crate::vk::FramebufferMixedSamplesCombinationNV`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::FramebufferMixedSamplesCombinationNV`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV")]
    pub unsafe fn get_physical_device_supported_framebuffer_mixed_samples_combinations_nv(&self, physical_device: crate::vk1_0::PhysicalDevice, combination_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::nv_coverage_reduction_mode::FramebufferMixedSamplesCombinationNV>> {
        let _function = self.get_physical_device_supported_framebuffer_mixed_samples_combinations_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut combination_count = match combination_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut combinations = crate::SmallVec::from_elem(Default::default(), combination_count as _);
        let _return = _function(physical_device as _, &mut combination_count, combinations.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, combinations)
    }
}
