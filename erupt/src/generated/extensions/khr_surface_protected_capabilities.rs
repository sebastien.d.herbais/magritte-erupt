#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_SURFACE_PROTECTED_CAPABILITIES_SPEC_VERSION")]
pub const KHR_SURFACE_PROTECTED_CAPABILITIES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_SURFACE_PROTECTED_CAPABILITIES_EXTENSION_NAME")]
pub const KHR_SURFACE_PROTECTED_CAPABILITIES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_surface_protected_capabilities");
#[doc = "Provided by [`crate::extensions::khr_surface_protected_capabilities`]"]
impl crate::vk1_0::StructureType {
    pub const SURFACE_PROTECTED_CAPABILITIES_KHR: Self = Self(1000239000);
}
impl<'a> crate::ExtendableFromMut<'a, SurfaceProtectedCapabilitiesKHR> for crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, SurfaceProtectedCapabilitiesKHRBuilder<'_>> for crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceProtectedCapabilitiesKHR.html)) · Structure <br/> VkSurfaceProtectedCapabilitiesKHR - Structure describing capability of a surface to be protected\n[](#_c_specification)C Specification\n----------\n\nAn application queries if a protected [`crate::vk::SurfaceKHR`] is displayable on a\nspecific windowing system using [`crate::vk::SurfaceProtectedCapabilitiesKHR`],\nwhich **can** be passed in [`Self::p_next`] parameter of[`crate::vk::SurfaceCapabilities2KHR`].\n\nThe [`crate::vk::SurfaceProtectedCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_surface_protected_capabilities\ntypedef struct VkSurfaceProtectedCapabilitiesKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           supportsProtected;\n} VkSurfaceProtectedCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::supports_protected`] specifies whether a protected swapchain created\n  from [`crate::vk::PhysicalDeviceSurfaceInfo2KHR::surface`] for a\n  particular windowing system **can** be displayed on screen or not.\n  If [`Self::supports_protected`] is [`crate::vk::TRUE`], then creation of swapchains\n  with the [`crate::vk::SwapchainCreateFlagBitsKHR::PROTECTED_KHR`] flag set **must** be\n  supported for `surface`.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceProtectedCapabilitiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_PROTECTED_CAPABILITIES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkSurfaceProtectedCapabilitiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceProtectedCapabilitiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub supports_protected: crate::vk1_0::Bool32,
}
impl SurfaceProtectedCapabilitiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_PROTECTED_CAPABILITIES_KHR;
}
impl Default for SurfaceProtectedCapabilitiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), supports_protected: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceProtectedCapabilitiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceProtectedCapabilitiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("supports_protected", &(self.supports_protected != 0)).finish()
    }
}
impl SurfaceProtectedCapabilitiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceProtectedCapabilitiesKHRBuilder<'a> {
        SurfaceProtectedCapabilitiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceProtectedCapabilitiesKHR.html)) · Builder of [`SurfaceProtectedCapabilitiesKHR`] <br/> VkSurfaceProtectedCapabilitiesKHR - Structure describing capability of a surface to be protected\n[](#_c_specification)C Specification\n----------\n\nAn application queries if a protected [`crate::vk::SurfaceKHR`] is displayable on a\nspecific windowing system using [`crate::vk::SurfaceProtectedCapabilitiesKHR`],\nwhich **can** be passed in [`Self::p_next`] parameter of[`crate::vk::SurfaceCapabilities2KHR`].\n\nThe [`crate::vk::SurfaceProtectedCapabilitiesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_surface_protected_capabilities\ntypedef struct VkSurfaceProtectedCapabilitiesKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           supportsProtected;\n} VkSurfaceProtectedCapabilitiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::supports_protected`] specifies whether a protected swapchain created\n  from [`crate::vk::PhysicalDeviceSurfaceInfo2KHR::surface`] for a\n  particular windowing system **can** be displayed on screen or not.\n  If [`Self::supports_protected`] is [`crate::vk::TRUE`], then creation of swapchains\n  with the [`crate::vk::SwapchainCreateFlagBitsKHR::PROTECTED_KHR`] flag set **must** be\n  supported for `surface`.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceProtectedCapabilitiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_PROTECTED_CAPABILITIES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct SurfaceProtectedCapabilitiesKHRBuilder<'a>(SurfaceProtectedCapabilitiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceProtectedCapabilitiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceProtectedCapabilitiesKHRBuilder<'a> {
        SurfaceProtectedCapabilitiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn supports_protected(mut self, supports_protected: bool) -> Self {
        self.0.supports_protected = supports_protected as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceProtectedCapabilitiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceProtectedCapabilitiesKHRBuilder<'a> {
    fn default() -> SurfaceProtectedCapabilitiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceProtectedCapabilitiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceProtectedCapabilitiesKHRBuilder<'a> {
    type Target = SurfaceProtectedCapabilitiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceProtectedCapabilitiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
