#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_COLOR_WRITE_ENABLE_SPEC_VERSION")]
pub const EXT_COLOR_WRITE_ENABLE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_COLOR_WRITE_ENABLE_EXTENSION_NAME")]
pub const EXT_COLOR_WRITE_ENABLE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_color_write_enable");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_COLOR_WRITE_ENABLE_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdSetColorWriteEnableEXT");
#[doc = "Provided by [`crate::extensions::ext_color_write_enable`]"]
impl crate::vk1_0::DynamicState {
    pub const COLOR_WRITE_ENABLE_EXT: Self = Self(1000381000);
}
#[doc = "Provided by [`crate::extensions::ext_color_write_enable`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT: Self = Self(1000381000);
    pub const PIPELINE_COLOR_WRITE_CREATE_INFO_EXT: Self = Self(1000381001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetColorWriteEnableEXT.html)) · Function <br/> vkCmdSetColorWriteEnableEXT - Enable or disable writes to a color attachment\n[](#_c_specification)C Specification\n----------\n\nTo dynamically enable or disable color writes, call:\n\n```\n// Provided by VK_EXT_color_write_enable\nvoid                                    vkCmdSetColorWriteEnableEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    attachmentCount,\n    const VkBool32*                             pColorWriteEnables);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::attachment_count`] is the number of [`crate::vk::Bool32`] elements in[`Self::p_color_write_enables`].\n\n* [`Self::p_color_write_enables`] is a pointer to an array of per target\n  attachment boolean values specifying whether color writes are enabled\n  for the given attachment.\n[](#_description)Description\n----------\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::COLOR_WRITE_ENABLE_EXT`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-None-04803  \n   The [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-attachmentCount-04804  \n  [`Self::attachment_count`] **must** be equal to the [`Self::attachment_count`] member\n  of the [`crate::vk::PipelineColorBlendStateCreateInfo`] structure specified\n  during pipeline creation\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-pColorWriteEnables-parameter  \n  [`Self::p_color_write_enables`] **must** be a valid pointer to an array of [`Self::attachment_count`] [`crate::vk::Bool32`] values\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-attachmentCount-arraylength  \n  [`Self::attachment_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetColorWriteEnableEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, attachment_count: u32, p_color_write_enables: *const crate::vk1_0::Bool32) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceColorWriteEnableFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineColorWriteCreateInfoEXT> for crate::vk1_0::PipelineColorBlendStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineColorWriteCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineColorBlendStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceColorWriteEnableFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceColorWriteEnableFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceColorWriteEnableFeaturesEXT - Structure describing whether writes to color attachments can be enabled and disabled dynamically\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_color_write_enable\ntypedef struct VkPhysicalDeviceColorWriteEnableFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           colorWriteEnable;\n} VkPhysicalDeviceColorWriteEnableFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::color_write_enable`] indicates that the\n  implementation supports the dynamic state[`crate::vk::DynamicState::COLOR_WRITE_ENABLE_EXT`].\n\nIf the [`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceColorWriteEnableFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceColorWriteEnableFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceColorWriteEnableFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub color_write_enable: crate::vk1_0::Bool32,
}
impl PhysicalDeviceColorWriteEnableFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT;
}
impl Default for PhysicalDeviceColorWriteEnableFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), color_write_enable: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceColorWriteEnableFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceColorWriteEnableFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("color_write_enable", &(self.color_write_enable != 0)).finish()
    }
}
impl PhysicalDeviceColorWriteEnableFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
        PhysicalDeviceColorWriteEnableFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceColorWriteEnableFeaturesEXT.html)) · Builder of [`PhysicalDeviceColorWriteEnableFeaturesEXT`] <br/> VkPhysicalDeviceColorWriteEnableFeaturesEXT - Structure describing whether writes to color attachments can be enabled and disabled dynamically\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_color_write_enable\ntypedef struct VkPhysicalDeviceColorWriteEnableFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           colorWriteEnable;\n} VkPhysicalDeviceColorWriteEnableFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::color_write_enable`] indicates that the\n  implementation supports the dynamic state[`crate::vk::DynamicState::COLOR_WRITE_ENABLE_EXT`].\n\nIf the [`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceColorWriteEnableFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceColorWriteEnableFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a>(PhysicalDeviceColorWriteEnableFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
        PhysicalDeviceColorWriteEnableFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn color_write_enable(mut self, color_write_enable: bool) -> Self {
        self.0.color_write_enable = color_write_enable as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceColorWriteEnableFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceColorWriteEnableFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceColorWriteEnableFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineColorWriteCreateInfoEXT.html)) · Structure <br/> VkPipelineColorWriteCreateInfoEXT - Structure specifying color write state of a newly created pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineColorWriteCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_color_write_enable\ntypedef struct VkPipelineColorWriteCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           attachmentCount;\n    const VkBool32*    pColorWriteEnables;\n} VkPipelineColorWriteCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::attachment_count`] is the number of [`crate::vk::Bool32`] elements in[`Self::p_color_write_enables`].\n\n* [`Self::p_color_write_enables`] is a pointer to an array of per target\n  attachment boolean values specifying whether color writes are enabled\n  for the given attachment.\n[](#_description)Description\n----------\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::PipelineColorBlendStateCreateInfo`], it defines per-attachment color\nwrite state.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with [`Self::attachment_count`] equal to the[`Self::attachment_count`] member of [`crate::vk::PipelineColorBlendStateCreateInfo`],\nand [`Self::p_color_write_enables`] pointing to an array of as many [`crate::vk::TRUE`]values.\n\nIf the [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature is not enabled\non the device, all [`crate::vk::Bool32`] elements in the[`Self::p_color_write_enables`] array **must** be [`crate::vk::TRUE`].\n\nColor Write Enable interacts with the [Color\nWrite Mask](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-color-write-mask) as follows:\n\n* If `colorWriteEnable` is [`crate::vk::TRUE`], writes to the attachment are\n  determined by the `colorWriteMask`.\n\n* If `colorWriteEnable` is [`crate::vk::FALSE`], the `colorWriteMask` is\n  ignored and writes to all components of the attachment are disabled.\n  This is equivalent to specifying a `colorWriteMask` of 0.\n\nValid Usage\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-pAttachments-04801  \n   If the [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature is not\n  enabled, all elements of [`Self::p_color_write_enables`] **must** be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-attachmentCount-04802  \n  [`Self::attachment_count`] **must** be equal to the [`Self::attachment_count`] member\n  of the [`crate::vk::PipelineColorBlendStateCreateInfo`] structure specified\n  during pipeline creation\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COLOR_WRITE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-pColorWriteEnables-parameter  \n   If [`Self::attachment_count`] is not `0`, [`Self::p_color_write_enables`] **must** be a valid pointer to an array of [`Self::attachment_count`] [`crate::vk::Bool32`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineColorWriteCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineColorWriteCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub attachment_count: u32,
    pub p_color_write_enables: *const crate::vk1_0::Bool32,
}
impl PipelineColorWriteCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_COLOR_WRITE_CREATE_INFO_EXT;
}
impl Default for PipelineColorWriteCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), attachment_count: Default::default(), p_color_write_enables: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineColorWriteCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineColorWriteCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("attachment_count", &self.attachment_count).field("p_color_write_enables", &self.p_color_write_enables).finish()
    }
}
impl PipelineColorWriteCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineColorWriteCreateInfoEXTBuilder<'a> {
        PipelineColorWriteCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineColorWriteCreateInfoEXT.html)) · Builder of [`PipelineColorWriteCreateInfoEXT`] <br/> VkPipelineColorWriteCreateInfoEXT - Structure specifying color write state of a newly created pipeline\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineColorWriteCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_color_write_enable\ntypedef struct VkPipelineColorWriteCreateInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           attachmentCount;\n    const VkBool32*    pColorWriteEnables;\n} VkPipelineColorWriteCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::attachment_count`] is the number of [`crate::vk::Bool32`] elements in[`Self::p_color_write_enables`].\n\n* [`Self::p_color_write_enables`] is a pointer to an array of per target\n  attachment boolean values specifying whether color writes are enabled\n  for the given attachment.\n[](#_description)Description\n----------\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::PipelineColorBlendStateCreateInfo`], it defines per-attachment color\nwrite state.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with [`Self::attachment_count`] equal to the[`Self::attachment_count`] member of [`crate::vk::PipelineColorBlendStateCreateInfo`],\nand [`Self::p_color_write_enables`] pointing to an array of as many [`crate::vk::TRUE`]values.\n\nIf the [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature is not enabled\non the device, all [`crate::vk::Bool32`] elements in the[`Self::p_color_write_enables`] array **must** be [`crate::vk::TRUE`].\n\nColor Write Enable interacts with the [Color\nWrite Mask](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#framebuffer-color-write-mask) as follows:\n\n* If `colorWriteEnable` is [`crate::vk::TRUE`], writes to the attachment are\n  determined by the `colorWriteMask`.\n\n* If `colorWriteEnable` is [`crate::vk::FALSE`], the `colorWriteMask` is\n  ignored and writes to all components of the attachment are disabled.\n  This is equivalent to specifying a `colorWriteMask` of 0.\n\nValid Usage\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-pAttachments-04801  \n   If the [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature is not\n  enabled, all elements of [`Self::p_color_write_enables`] **must** be [`crate::vk::TRUE`]\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-attachmentCount-04802  \n  [`Self::attachment_count`] **must** be equal to the [`Self::attachment_count`] member\n  of the [`crate::vk::PipelineColorBlendStateCreateInfo`] structure specified\n  during pipeline creation\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_COLOR_WRITE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineColorWriteCreateInfoEXT-pColorWriteEnables-parameter  \n   If [`Self::attachment_count`] is not `0`, [`Self::p_color_write_enables`] **must** be a valid pointer to an array of [`Self::attachment_count`] [`crate::vk::Bool32`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineColorWriteCreateInfoEXTBuilder<'a>(PipelineColorWriteCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineColorWriteCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineColorWriteCreateInfoEXTBuilder<'a> {
        PipelineColorWriteCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn color_write_enables(mut self, color_write_enables: &'a [crate::vk1_0::Bool32]) -> Self {
        self.0.p_color_write_enables = color_write_enables.as_ptr() as _;
        self.0.attachment_count = color_write_enables.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineColorWriteCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineColorWriteCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineColorWriteCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineColorWriteCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineColorWriteCreateInfoEXTBuilder<'a> {
    type Target = PipelineColorWriteCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineColorWriteCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_color_write_enable`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetColorWriteEnableEXT.html)) · Function <br/> vkCmdSetColorWriteEnableEXT - Enable or disable writes to a color attachment\n[](#_c_specification)C Specification\n----------\n\nTo dynamically enable or disable color writes, call:\n\n```\n// Provided by VK_EXT_color_write_enable\nvoid                                    vkCmdSetColorWriteEnableEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    attachmentCount,\n    const VkBool32*                             pColorWriteEnables);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::attachment_count`] is the number of [`crate::vk::Bool32`] elements in[`Self::p_color_write_enables`].\n\n* [`Self::p_color_write_enables`] is a pointer to an array of per target\n  attachment boolean values specifying whether color writes are enabled\n  for the given attachment.\n[](#_description)Description\n----------\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::COLOR_WRITE_ENABLE_EXT`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-None-04803  \n   The [colorWriteEnable](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-colorWriteEnable) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-attachmentCount-04804  \n  [`Self::attachment_count`] **must** be equal to the [`Self::attachment_count`] member\n  of the [`crate::vk::PipelineColorBlendStateCreateInfo`] structure specified\n  during pipeline creation\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-pColorWriteEnables-parameter  \n  [`Self::p_color_write_enables`] **must** be a valid pointer to an array of [`Self::attachment_count`] [`crate::vk::Bool32`] values\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetColorWriteEnableEXT-attachmentCount-arraylength  \n  [`Self::attachment_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdSetColorWriteEnableEXT")]
    pub unsafe fn cmd_set_color_write_enable_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, color_write_enables: &[crate::vk1_0::Bool32]) -> () {
        let _function = self.cmd_set_color_write_enable_ext.expect(crate::NOT_LOADED_MESSAGE);
        let attachment_count = color_write_enables.len();
        let _return = _function(command_buffer as _, attachment_count as _, color_write_enables.as_ptr() as _);
        ()
    }
}
