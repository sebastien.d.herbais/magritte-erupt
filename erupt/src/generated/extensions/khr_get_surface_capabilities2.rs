#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_GET_SURFACE_CAPABILITIES_2_SPEC_VERSION")]
pub const KHR_GET_SURFACE_CAPABILITIES_2_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_GET_SURFACE_CAPABILITIES_2_EXTENSION_NAME")]
pub const KHR_GET_SURFACE_CAPABILITIES_2_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_get_surface_capabilities2");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SURFACE_CAPABILITIES2_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceSurfaceCapabilities2KHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SURFACE_FORMATS2_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceSurfaceFormats2KHR");
#[doc = "Provided by [`crate::extensions::khr_get_surface_capabilities2`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_SURFACE_INFO_2_KHR: Self = Self(1000119000);
    pub const SURFACE_CAPABILITIES_2_KHR: Self = Self(1000119001);
    pub const SURFACE_FORMAT_2_KHR: Self = Self(1000119002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceCapabilities2KHR.html)) · Function <br/> vkGetPhysicalDeviceSurfaceCapabilities2KHR - Reports capabilities of a surface on a physical device\n[](#_c_specification)C Specification\n----------\n\nTo query the basic capabilities of a surface defined by the core or\nextensions, call:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\nVkResult vkGetPhysicalDeviceSurfaceCapabilities2KHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    VkSurfaceCapabilities2KHR*                  pSurfaceCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_capabilities`] is a pointer to a[`crate::vk::SurfaceCapabilities2KHR`] structure in which the capabilities are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with the ability to specify\nextended inputs via chained input structures, and to return extended\ninformation via chained output structures.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pNext-02671  \n   If a [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`] structure is\n  included in the `pNext` chain of [`Self::p_surface_capabilities`], a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **must** be\n  included in the `pNext` chain of [`Self::p_surface_info`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pSurfaceCapabilities-parameter  \n  [`Self::p_surface_capabilities`] **must** be a valid pointer to a [`crate::vk::SurfaceCapabilities2KHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::SurfaceCapabilities2KHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSurfaceCapabilities2KHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_surface_info: *const crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, p_surface_capabilities: *mut crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceFormats2KHR.html)) · Function <br/> vkGetPhysicalDeviceSurfaceFormats2KHR - Query color formats supported by surface\n[](#_c_specification)C Specification\n----------\n\nTo query the supported swapchain format tuples for a surface, call:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\nVkResult vkGetPhysicalDeviceSurfaceFormats2KHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    uint32_t*                                   pSurfaceFormatCount,\n    VkSurfaceFormat2KHR*                        pSurfaceFormats);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_format_count`] is a pointer to an integer related to the\n  number of format tuples available or queried, as described below.\n\n* [`Self::p_surface_formats`] is either `NULL` or a pointer to an array of[`crate::vk::SurfaceFormat2KHR`] structures.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_formats_khr`], with the ability to be extended\nvia `pNext` chains.\n\nIf [`Self::p_surface_formats`] is `NULL`, then the number of format tuples\nsupported for the given `surface` is returned in[`Self::p_surface_format_count`].\nOtherwise, [`Self::p_surface_format_count`] **must** point to a variable set by the\nuser to the number of elements in the [`Self::p_surface_formats`] array, and on\nreturn the variable is overwritten with the number of structures actually\nwritten to [`Self::p_surface_formats`].\nIf the value of [`Self::p_surface_format_count`] is less than the number of format\ntuples supported, at most [`Self::p_surface_format_count`] structures will be\nwritten, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available values were\nreturned.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceFormatCount-parameter  \n  [`Self::p_surface_format_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceFormats-parameter  \n   If the value referenced by [`Self::p_surface_format_count`] is not `0`, and [`Self::p_surface_formats`] is not `NULL`, [`Self::p_surface_formats`] **must** be a valid pointer to an array of [`Self::p_surface_format_count`] [`crate::vk::SurfaceFormat2KHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::SurfaceFormat2KHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSurfaceFormats2KHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_surface_info: *const crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, p_surface_format_count: *mut u32, p_surface_formats: *mut crate::extensions::khr_get_surface_capabilities2::SurfaceFormat2KHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSurfaceInfo2KHR.html)) · Structure <br/> VkPhysicalDeviceSurfaceInfo2KHR - Structure specifying a surface and related swapchain creation parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkPhysicalDeviceSurfaceInfo2KHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkSurfaceKHR       surface;\n} VkPhysicalDeviceSurfaceInfo2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface`] is the surface that will be associated with the swapchain.\n[](#_description)Description\n----------\n\nThe members of [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] correspond to the\narguments to [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with[`Self::s_type`] and [`Self::p_next`] added for extensibility.\n\nAdditional capabilities of a surface **may** be available to swapchains created\nwith different full-screen exclusive settings - particularly if exclusive\nfull-screen access is application controlled.\nThese additional capabilities **can** be queried by adding a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure to the [`Self::p_next`] chain\nof this structure when used to query surface properties.\nAdditionally, for Win32 surfaces with application controlled exclusive\nfull-screen access, chaining a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **may** also report\nadditional surface capabilities.\nThese additional capabilities only apply to swapchains created with the same\nparameters included in the [`Self::p_next`] chain of[`crate::vk::SwapchainCreateInfoKHR`].\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-pNext-02672  \n   If the [`Self::p_next`] chain includes a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure with its`fullScreenExclusive` member set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`], and[`Self::surface`] was created using [`crate::vk::InstanceLoader::create_win32_surface_khr`], a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **must** be\n  included in the [`Self::p_next`] chain\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SURFACE_INFO_2_KHR`]\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] or [`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`]\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-surface-parameter  \n  [`Self::surface`] **must** be a valid [`crate::vk::SurfaceKHR`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceKHR`], [`crate::vk::DeviceLoader::get_device_group_surface_present_modes2_ext`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`], [`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`], [`crate::vk::DeviceLoader::get_physical_device_surface_present_modes2_ext`]\n"]
#[doc(alias = "VkPhysicalDeviceSurfaceInfo2KHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceSurfaceInfo2KHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub surface: crate::extensions::khr_surface::SurfaceKHR,
}
impl PhysicalDeviceSurfaceInfo2KHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SURFACE_INFO_2_KHR;
}
impl Default for PhysicalDeviceSurfaceInfo2KHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), surface: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceSurfaceInfo2KHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceSurfaceInfo2KHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("surface", &self.surface).finish()
    }
}
impl PhysicalDeviceSurfaceInfo2KHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
        PhysicalDeviceSurfaceInfo2KHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceSurfaceInfo2KHR.html)) · Builder of [`PhysicalDeviceSurfaceInfo2KHR`] <br/> VkPhysicalDeviceSurfaceInfo2KHR - Structure specifying a surface and related swapchain creation parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkPhysicalDeviceSurfaceInfo2KHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkSurfaceKHR       surface;\n} VkPhysicalDeviceSurfaceInfo2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface`] is the surface that will be associated with the swapchain.\n[](#_description)Description\n----------\n\nThe members of [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] correspond to the\narguments to [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with[`Self::s_type`] and [`Self::p_next`] added for extensibility.\n\nAdditional capabilities of a surface **may** be available to swapchains created\nwith different full-screen exclusive settings - particularly if exclusive\nfull-screen access is application controlled.\nThese additional capabilities **can** be queried by adding a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure to the [`Self::p_next`] chain\nof this structure when used to query surface properties.\nAdditionally, for Win32 surfaces with application controlled exclusive\nfull-screen access, chaining a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **may** also report\nadditional surface capabilities.\nThese additional capabilities only apply to swapchains created with the same\nparameters included in the [`Self::p_next`] chain of[`crate::vk::SwapchainCreateInfoKHR`].\n\nValid Usage\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-pNext-02672  \n   If the [`Self::p_next`] chain includes a[`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] structure with its`fullScreenExclusive` member set to[`crate::vk::FullScreenExclusiveEXT::APPLICATION_CONTROLLED_EXT`], and[`Self::surface`] was created using [`crate::vk::InstanceLoader::create_win32_surface_khr`], a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **must** be\n  included in the [`Self::p_next`] chain\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SURFACE_INFO_2_KHR`]\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::SurfaceFullScreenExclusiveInfoEXT`] or [`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`]\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n\n* []() VUID-VkPhysicalDeviceSurfaceInfo2KHR-surface-parameter  \n  [`Self::surface`] **must** be a valid [`crate::vk::SurfaceKHR`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceKHR`], [`crate::vk::DeviceLoader::get_device_group_surface_present_modes2_ext`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`], [`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`], [`crate::vk::DeviceLoader::get_physical_device_surface_present_modes2_ext`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceSurfaceInfo2KHRBuilder<'a>(PhysicalDeviceSurfaceInfo2KHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
        PhysicalDeviceSurfaceInfo2KHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn surface(mut self, surface: crate::extensions::khr_surface::SurfaceKHR) -> Self {
        self.0.surface = surface as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceSurfaceInfo2KHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
    fn default() -> PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
    type Target = PhysicalDeviceSurfaceInfo2KHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceSurfaceInfo2KHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilities2KHR.html)) · Structure <br/> VkSurfaceCapabilities2KHR - Structure describing capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilities2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkSurfaceCapabilities2KHR {\n    VkStructureType             sType;\n    void*                       pNext;\n    VkSurfaceCapabilitiesKHR    surfaceCapabilities;\n} VkSurfaceCapabilities2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_capabilities`] is a [`crate::vk::SurfaceCapabilitiesKHR`] structure\n  describing the capabilities of the specified surface.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilities2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_2_KHR`]\n\n* []() VUID-VkSurfaceCapabilities2KHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::DisplayNativeHdrSurfaceCapabilitiesAMD`], [`crate::vk::SharedPresentSurfaceCapabilitiesKHR`], [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`], or [`crate::vk::SurfaceProtectedCapabilitiesKHR`]\n\n* []() VUID-VkSurfaceCapabilities2KHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceCapabilitiesKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`]\n"]
#[doc(alias = "VkSurfaceCapabilities2KHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceCapabilities2KHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub surface_capabilities: crate::extensions::khr_surface::SurfaceCapabilitiesKHR,
}
impl SurfaceCapabilities2KHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_CAPABILITIES_2_KHR;
}
impl Default for SurfaceCapabilities2KHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), surface_capabilities: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceCapabilities2KHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceCapabilities2KHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("surface_capabilities", &self.surface_capabilities).finish()
    }
}
impl SurfaceCapabilities2KHR {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceCapabilities2KHRBuilder<'a> {
        SurfaceCapabilities2KHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilities2KHR.html)) · Builder of [`SurfaceCapabilities2KHR`] <br/> VkSurfaceCapabilities2KHR - Structure describing capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilities2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkSurfaceCapabilities2KHR {\n    VkStructureType             sType;\n    void*                       pNext;\n    VkSurfaceCapabilitiesKHR    surfaceCapabilities;\n} VkSurfaceCapabilities2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_capabilities`] is a [`crate::vk::SurfaceCapabilitiesKHR`] structure\n  describing the capabilities of the specified surface.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilities2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_2_KHR`]\n\n* []() VUID-VkSurfaceCapabilities2KHR-pNext-pNext  \n   Each [`Self::p_next`] member of any structure (including this one) in the [`Self::p_next`] chain **must** be either `NULL` or a pointer to a valid instance of [`crate::vk::DisplayNativeHdrSurfaceCapabilitiesAMD`], [`crate::vk::SharedPresentSurfaceCapabilitiesKHR`], [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`], or [`crate::vk::SurfaceProtectedCapabilitiesKHR`]\n\n* []() VUID-VkSurfaceCapabilities2KHR-sType-unique  \n   The [`Self::s_type`] value of each struct in the [`Self::p_next`] chain **must** be unique\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceCapabilitiesKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`]\n"]
#[repr(transparent)]
pub struct SurfaceCapabilities2KHRBuilder<'a>(SurfaceCapabilities2KHR, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceCapabilities2KHRBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceCapabilities2KHRBuilder<'a> {
        SurfaceCapabilities2KHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn surface_capabilities(mut self, surface_capabilities: crate::extensions::khr_surface::SurfaceCapabilitiesKHR) -> Self {
        self.0.surface_capabilities = surface_capabilities as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceCapabilities2KHR {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceCapabilities2KHRBuilder<'a> {
    fn default() -> SurfaceCapabilities2KHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceCapabilities2KHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceCapabilities2KHRBuilder<'a> {
    type Target = SurfaceCapabilities2KHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceCapabilities2KHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFormat2KHR.html)) · Structure <br/> VkSurfaceFormat2KHR - Structure describing a supported swapchain format tuple\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceFormat2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkSurfaceFormat2KHR {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkSurfaceFormatKHR    surfaceFormat;\n} VkSurfaceFormat2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_format`] is a [`crate::vk::SurfaceFormatKHR`] structure describing a\n  format-color space pair that is compatible with the specified surface.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFormat2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FORMAT_2_KHR`]\n\n* []() VUID-VkSurfaceFormat2KHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceFormatKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`]\n"]
#[doc(alias = "VkSurfaceFormat2KHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceFormat2KHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub surface_format: crate::extensions::khr_surface::SurfaceFormatKHR,
}
impl SurfaceFormat2KHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_FORMAT_2_KHR;
}
impl Default for SurfaceFormat2KHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), surface_format: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceFormat2KHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceFormat2KHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("surface_format", &self.surface_format).finish()
    }
}
impl SurfaceFormat2KHR {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceFormat2KHRBuilder<'a> {
        SurfaceFormat2KHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceFormat2KHR.html)) · Builder of [`SurfaceFormat2KHR`] <br/> VkSurfaceFormat2KHR - Structure describing a supported swapchain format tuple\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceFormat2KHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\ntypedef struct VkSurfaceFormat2KHR {\n    VkStructureType       sType;\n    void*                 pNext;\n    VkSurfaceFormatKHR    surfaceFormat;\n} VkSurfaceFormat2KHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_format`] is a [`crate::vk::SurfaceFormatKHR`] structure describing a\n  format-color space pair that is compatible with the specified surface.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceFormat2KHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_FORMAT_2_KHR`]\n\n* []() VUID-VkSurfaceFormat2KHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceFormatKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`]\n"]
#[repr(transparent)]
pub struct SurfaceFormat2KHRBuilder<'a>(SurfaceFormat2KHR, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceFormat2KHRBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceFormat2KHRBuilder<'a> {
        SurfaceFormat2KHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn surface_format(mut self, surface_format: crate::extensions::khr_surface::SurfaceFormatKHR) -> Self {
        self.0.surface_format = surface_format as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceFormat2KHR {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceFormat2KHRBuilder<'a> {
    fn default() -> SurfaceFormat2KHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceFormat2KHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceFormat2KHRBuilder<'a> {
    type Target = SurfaceFormat2KHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceFormat2KHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_get_surface_capabilities2`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceCapabilities2KHR.html)) · Function <br/> vkGetPhysicalDeviceSurfaceCapabilities2KHR - Reports capabilities of a surface on a physical device\n[](#_c_specification)C Specification\n----------\n\nTo query the basic capabilities of a surface defined by the core or\nextensions, call:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\nVkResult vkGetPhysicalDeviceSurfaceCapabilities2KHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    VkSurfaceCapabilities2KHR*                  pSurfaceCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_capabilities`] is a pointer to a[`crate::vk::SurfaceCapabilities2KHR`] structure in which the capabilities are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_khr`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with the ability to specify\nextended inputs via chained input structures, and to return extended\ninformation via chained output structures.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pNext-02671  \n   If a [`crate::vk::SurfaceCapabilitiesFullScreenExclusiveEXT`] structure is\n  included in the `pNext` chain of [`Self::p_surface_capabilities`], a[`crate::vk::SurfaceFullScreenExclusiveWin32InfoEXT`] structure **must** be\n  included in the `pNext` chain of [`Self::p_surface_info`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2KHR-pSurfaceCapabilities-parameter  \n  [`Self::p_surface_capabilities`] **must** be a valid pointer to a [`crate::vk::SurfaceCapabilities2KHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::SurfaceCapabilities2KHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceSurfaceCapabilities2KHR")]
    pub unsafe fn get_physical_device_surface_capabilities2_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, surface_info: &crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, surface_capabilities: Option<crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHR>) -> crate::utils::VulkanResult<crate::extensions::khr_get_surface_capabilities2::SurfaceCapabilities2KHR> {
        let _function = self.get_physical_device_surface_capabilities2_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface_capabilities = match surface_capabilities {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, surface_info as _, &mut surface_capabilities);
        crate::utils::VulkanResult::new(_return, {
            surface_capabilities.p_next = std::ptr::null_mut() as _;
            surface_capabilities
        })
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceFormats2KHR.html)) · Function <br/> vkGetPhysicalDeviceSurfaceFormats2KHR - Query color formats supported by surface\n[](#_c_specification)C Specification\n----------\n\nTo query the supported swapchain format tuples for a surface, call:\n\n```\n// Provided by VK_KHR_get_surface_capabilities2\nVkResult vkGetPhysicalDeviceSurfaceFormats2KHR(\n    VkPhysicalDevice                            physicalDevice,\n    const VkPhysicalDeviceSurfaceInfo2KHR*      pSurfaceInfo,\n    uint32_t*                                   pSurfaceFormatCount,\n    VkSurfaceFormat2KHR*                        pSurfaceFormats);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_info`] is a pointer to a[`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure describing the surface\n  and other fixed parameters that would be consumed by[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::p_surface_format_count`] is a pointer to an integer related to the\n  number of format tuples available or queried, as described below.\n\n* [`Self::p_surface_formats`] is either `NULL` or a pointer to an array of[`crate::vk::SurfaceFormat2KHR`] structures.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_formats2_khr`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_formats_khr`], with the ability to be extended\nvia `pNext` chains.\n\nIf [`Self::p_surface_formats`] is `NULL`, then the number of format tuples\nsupported for the given `surface` is returned in[`Self::p_surface_format_count`].\nOtherwise, [`Self::p_surface_format_count`] **must** point to a variable set by the\nuser to the number of elements in the [`Self::p_surface_formats`] array, and on\nreturn the variable is overwritten with the number of structures actually\nwritten to [`Self::p_surface_formats`].\nIf the value of [`Self::p_surface_format_count`] is less than the number of format\ntuples supported, at most [`Self::p_surface_format_count`] structures will be\nwritten, and [`crate::vk::Result::INCOMPLETE`] will be returned instead of[`crate::vk::Result::SUCCESS`], to indicate that not all the available values were\nreturned.\n\nValid Usage\n\n* [[VUID-{refpage}-pSurfaceInfo-06210]] VUID-{refpage}-pSurfaceInfo-06210  \n  `pSurfaceInfo->surface` **must** be supported by [`Self::physical_device`],\n  as reported by [`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an\n  equivalent platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceInfo-parameter  \n  [`Self::p_surface_info`] **must** be a valid pointer to a valid [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceFormatCount-parameter  \n  [`Self::p_surface_format_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceSurfaceFormats2KHR-pSurfaceFormats-parameter  \n   If the value referenced by [`Self::p_surface_format_count`] is not `0`, and [`Self::p_surface_formats`] is not `NULL`, [`Self::p_surface_formats`] **must** be a valid pointer to an array of [`Self::p_surface_format_count`] [`crate::vk::SurfaceFormat2KHR`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceSurfaceInfo2KHR`], [`crate::vk::SurfaceFormat2KHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceSurfaceFormats2KHR")]
    pub unsafe fn get_physical_device_surface_formats2_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, surface_info: &crate::extensions::khr_get_surface_capabilities2::PhysicalDeviceSurfaceInfo2KHR, surface_format_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::khr_get_surface_capabilities2::SurfaceFormat2KHR>> {
        let _function = self.get_physical_device_surface_formats2_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface_format_count = match surface_format_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, surface_info as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut surface_formats = crate::SmallVec::from_elem(Default::default(), surface_format_count as _);
        let _return = _function(physical_device as _, surface_info as _, &mut surface_format_count, surface_formats.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, surface_formats)
    }
}
