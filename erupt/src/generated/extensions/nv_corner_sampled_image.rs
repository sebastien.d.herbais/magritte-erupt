#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_CORNER_SAMPLED_IMAGE_SPEC_VERSION")]
pub const NV_CORNER_SAMPLED_IMAGE_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_CORNER_SAMPLED_IMAGE_EXTENSION_NAME")]
pub const NV_CORNER_SAMPLED_IMAGE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_corner_sampled_image");
#[doc = "Provided by [`crate::extensions::nv_corner_sampled_image`]"]
impl crate::vk1_0::ImageCreateFlagBits {
    pub const CORNER_SAMPLED_NV: Self = Self(8192);
}
#[doc = "Provided by [`crate::extensions::nv_corner_sampled_image`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV: Self = Self(1000050000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCornerSampledImageFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCornerSampledImageFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCornerSampledImageFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceCornerSampledImageFeaturesNV - Structure describing corner sampled image features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_corner_sampled_image\ntypedef struct VkPhysicalDeviceCornerSampledImageFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           cornerSampledImage;\n} VkPhysicalDeviceCornerSampledImageFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::corner_sampled_image`] specifies\n  whether images can be created with a[`crate::vk::ImageCreateInfo::flags`] containing[`crate::vk::ImageCreateFlagBits::CORNER_SAMPLED_NV`].\n  See [Corner-Sampled Images](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-images-corner-sampled).\n\nIf the [`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCornerSampledImageFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceCornerSampledImageFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceCornerSampledImageFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub corner_sampled_image: crate::vk1_0::Bool32,
}
impl PhysicalDeviceCornerSampledImageFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV;
}
impl Default for PhysicalDeviceCornerSampledImageFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), corner_sampled_image: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceCornerSampledImageFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceCornerSampledImageFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("corner_sampled_image", &(self.corner_sampled_image != 0)).finish()
    }
}
impl PhysicalDeviceCornerSampledImageFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
        PhysicalDeviceCornerSampledImageFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceCornerSampledImageFeaturesNV.html)) · Builder of [`PhysicalDeviceCornerSampledImageFeaturesNV`] <br/> VkPhysicalDeviceCornerSampledImageFeaturesNV - Structure describing corner sampled image features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_corner_sampled_image\ntypedef struct VkPhysicalDeviceCornerSampledImageFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           cornerSampledImage;\n} VkPhysicalDeviceCornerSampledImageFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::corner_sampled_image`] specifies\n  whether images can be created with a[`crate::vk::ImageCreateInfo::flags`] containing[`crate::vk::ImageCreateFlagBits::CORNER_SAMPLED_NV`].\n  See [Corner-Sampled Images](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#resources-images-corner-sampled).\n\nIf the [`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceCornerSampledImageFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceCornerSampledImageFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a>(PhysicalDeviceCornerSampledImageFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
        PhysicalDeviceCornerSampledImageFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn corner_sampled_image(mut self, corner_sampled_image: bool) -> Self {
        self.0.corner_sampled_image = corner_sampled_image as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceCornerSampledImageFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceCornerSampledImageFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceCornerSampledImageFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
