#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WAYLAND_SURFACE_SPEC_VERSION")]
pub const KHR_WAYLAND_SURFACE_SPEC_VERSION: u32 = 6;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME")]
pub const KHR_WAYLAND_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_wayland_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_WAYLAND_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateWaylandSurfaceKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_WAYLAND_PRESENTATION_SUPPORT_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceWaylandPresentationSupportKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWaylandSurfaceCreateFlagsKHR.html)) · Bitmask of [`WaylandSurfaceCreateFlagBitsKHR`] <br/> VkWaylandSurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_wayland_surface\ntypedef VkFlags VkWaylandSurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::WaylandSurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::WaylandSurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkWaylandSurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct WaylandSurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`WaylandSurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkWaylandSurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct WaylandSurfaceCreateFlagBitsKHR(pub u32);
impl WaylandSurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> WaylandSurfaceCreateFlagsKHR {
        WaylandSurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for WaylandSurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_wayland_surface`]"]
impl crate::vk1_0::StructureType {
    pub const WAYLAND_SURFACE_CREATE_INFO_KHR: Self = Self(1000006000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateWaylandSurfaceKHR.html)) · Function <br/> vkCreateWaylandSurfaceKHR - Create a slink:VkSurfaceKHR object for a Wayland window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Wayland surface, call:\n\n```\n// Provided by VK_KHR_wayland_surface\nVkResult vkCreateWaylandSurfaceKHR(\n    VkInstance                                  instance,\n    const VkWaylandSurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::WaylandSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateWaylandSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::WaylandSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::WaylandSurfaceCreateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateWaylandSurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_wayland_surface::WaylandSurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceWaylandPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceWaylandPresentationSupportKHR - Query physical device for presentation to Wayland\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to a Wayland compositor, call:\n\n```\n// Provided by VK_KHR_wayland_surface\nVkBool32 vkGetPhysicalDeviceWaylandPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    struct wl_display*                          display);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::display`] is a pointer to the `wl_display` associated with a\n  Wayland compositor.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-queueFamilyIndex-01306  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-display-parameter  \n  [`Self::display`] **must** be a valid pointer to a `wl_display` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceWaylandPresentationSupportKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, display: *mut std::ffi::c_void) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWaylandSurfaceCreateInfoKHR.html)) · Structure <br/> VkWaylandSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Wayland surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::WaylandSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_wayland_surface\ntypedef struct VkWaylandSurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkWaylandSurfaceCreateFlagsKHR    flags;\n    struct wl_display*                display;\n    struct wl_surface*                surface;\n} VkWaylandSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::display`] and [`Self::surface`] are pointers to the Wayland`wl_display` and `wl_surface` to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-display-01304  \n  [`Self::display`] **must** point to a valid Wayland `wl_display`\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-surface-01305  \n  [`Self::surface`] **must** point to a valid Wayland `wl_surface`\n\nValid Usage (Implicit)\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WAYLAND_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::WaylandSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_wayland_surface_khr`]\n"]
#[doc(alias = "VkWaylandSurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct WaylandSurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_wayland_surface::WaylandSurfaceCreateFlagsKHR,
    pub display: *mut std::ffi::c_void,
    pub surface: *mut std::ffi::c_void,
}
impl WaylandSurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::WAYLAND_SURFACE_CREATE_INFO_KHR;
}
impl Default for WaylandSurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), display: std::ptr::null_mut(), surface: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for WaylandSurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("WaylandSurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("display", &self.display).field("surface", &self.surface).finish()
    }
}
impl WaylandSurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> WaylandSurfaceCreateInfoKHRBuilder<'a> {
        WaylandSurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWaylandSurfaceCreateInfoKHR.html)) · Builder of [`WaylandSurfaceCreateInfoKHR`] <br/> VkWaylandSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Wayland surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::WaylandSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_wayland_surface\ntypedef struct VkWaylandSurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkWaylandSurfaceCreateFlagsKHR    flags;\n    struct wl_display*                display;\n    struct wl_surface*                surface;\n} VkWaylandSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::display`] and [`Self::surface`] are pointers to the Wayland`wl_display` and `wl_surface` to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-display-01304  \n  [`Self::display`] **must** point to a valid Wayland `wl_display`\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-surface-01305  \n  [`Self::surface`] **must** point to a valid Wayland `wl_surface`\n\nValid Usage (Implicit)\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WAYLAND_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkWaylandSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::WaylandSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_wayland_surface_khr`]\n"]
#[repr(transparent)]
pub struct WaylandSurfaceCreateInfoKHRBuilder<'a>(WaylandSurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> WaylandSurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> WaylandSurfaceCreateInfoKHRBuilder<'a> {
        WaylandSurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_wayland_surface::WaylandSurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn display(mut self, display: *mut std::ffi::c_void) -> Self {
        self.0.display = display;
        self
    }
    #[inline]
    pub fn surface(mut self, surface: *mut std::ffi::c_void) -> Self {
        self.0.surface = surface;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> WaylandSurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for WaylandSurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> WaylandSurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for WaylandSurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for WaylandSurfaceCreateInfoKHRBuilder<'a> {
    type Target = WaylandSurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for WaylandSurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_wayland_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateWaylandSurfaceKHR.html)) · Function <br/> vkCreateWaylandSurfaceKHR - Create a slink:VkSurfaceKHR object for a Wayland window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Wayland surface, call:\n\n```\n// Provided by VK_KHR_wayland_surface\nVkResult vkCreateWaylandSurfaceKHR(\n    VkInstance                                  instance,\n    const VkWaylandSurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::WaylandSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateWaylandSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::WaylandSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateWaylandSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::WaylandSurfaceCreateInfoKHR`]\n"]
    #[doc(alias = "vkCreateWaylandSurfaceKHR")]
    pub unsafe fn create_wayland_surface_khr(&self, create_info: &crate::extensions::khr_wayland_surface::WaylandSurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_wayland_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceWaylandPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceWaylandPresentationSupportKHR - Query physical device for presentation to Wayland\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to a Wayland compositor, call:\n\n```\n// Provided by VK_KHR_wayland_surface\nVkBool32 vkGetPhysicalDeviceWaylandPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    struct wl_display*                          display);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::display`] is a pointer to the `wl_display` associated with a\n  Wayland compositor.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-queueFamilyIndex-01306  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceWaylandPresentationSupportKHR-display-parameter  \n  [`Self::display`] **must** be a valid pointer to a `wl_display` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceWaylandPresentationSupportKHR")]
    pub unsafe fn get_physical_device_wayland_presentation_support_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, display: *mut std::ffi::c_void) -> bool {
        let _function = self.get_physical_device_wayland_presentation_support_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _, display);
        _return != 0
    }
}
