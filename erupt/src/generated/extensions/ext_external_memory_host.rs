#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_EXTERNAL_MEMORY_HOST_SPEC_VERSION")]
pub const EXT_EXTERNAL_MEMORY_HOST_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_EXTERNAL_MEMORY_HOST_EXTENSION_NAME")]
pub const EXT_EXTERNAL_MEMORY_HOST_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_external_memory_host");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_HOST_POINTER_PROPERTIES_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryHostPointerPropertiesEXT");
#[doc = "Provided by [`crate::extensions::ext_external_memory_host`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_MEMORY_HOST_POINTER_INFO_EXT: Self = Self(1000178000);
    pub const MEMORY_HOST_POINTER_PROPERTIES_EXT: Self = Self(1000178001);
    pub const PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT: Self = Self(1000178002);
}
#[doc = "Provided by [`crate::extensions::ext_external_memory_host`]"]
impl crate::vk1_1::ExternalMemoryHandleTypeFlagBits {
    pub const HOST_ALLOCATION_EXT: Self = Self(128);
    pub const HOST_MAPPED_FOREIGN_MEMORY_EXT: Self = Self(256);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryHostPointerPropertiesEXT.html)) · Function <br/> vkGetMemoryHostPointerPropertiesEXT - Get properties of external memory host pointer\n[](#_c_specification)C Specification\n----------\n\nTo determine the correct parameters to use when importing host pointers,\ncall:\n\n```\n// Provided by VK_EXT_external_memory_host\nVkResult vkGetMemoryHostPointerPropertiesEXT(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    const void*                                 pHostPointer,\n    VkMemoryHostPointerPropertiesEXT*           pMemoryHostPointerProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing[`Self::p_host_pointer`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::p_host_pointer`].\n\n* [`Self::p_host_pointer`] is the host pointer to import from.\n\n* [`Self::p_memory_host_pointer_properties`] is a pointer to a[`crate::vk::MemoryHostPointerPropertiesEXT`] structure in which the host\n  pointer properties are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01752  \n  [`Self::handle_type`] **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`]\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-pHostPointer-01753  \n  [`Self::p_host_pointer`] **must** be a pointer aligned to an integer multiple of[`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`]::`minImportedHostPointerAlignment`\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01754  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`],[`Self::p_host_pointer`] **must** be a pointer to host memory\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01755  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`],[`Self::p_host_pointer`] **must** be a pointer to host mapped foreign memory\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-pMemoryHostPointerProperties-parameter  \n  [`Self::p_memory_host_pointer_properties`] **must** be a valid pointer to a [`crate::vk::MemoryHostPointerPropertiesEXT`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryHostPointerPropertiesEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryHostPointerPropertiesEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, p_host_pointer: *const std::ffi::c_void, p_memory_host_pointer_properties: *mut crate::extensions::ext_external_memory_host::MemoryHostPointerPropertiesEXT) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryHostPointerInfoEXT> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryHostPointerInfoEXTBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExternalMemoryHostPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryHostPointerInfoEXT.html)) · Structure <br/> VkImportMemoryHostPointerInfoEXT - Import memory from a host pointer\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a host pointer, add a[`crate::vk::ImportMemoryHostPointerInfoEXT`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportMemoryHostPointerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkImportMemoryHostPointerInfoEXT {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    void*                                 pHostPointer;\n} VkImportMemoryHostPointerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the handle type.\n\n* [`Self::p_host_pointer`] is the host pointer to import from.\n[](#_description)Description\n----------\n\nImporting memory from a host pointer shares ownership of the memory between\nthe host and the Vulkan implementation.\nThe application **can** continue to access the memory through the host pointer\nbut it is the application’s responsibility to synchronize device and\nnon-device access to the payload as defined in[Host Access to Device Memory Objects](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-device-hostaccess).\n\nApplications **can** import the same payload into multiple instances of Vulkan\nand multiple times into a given Vulkan instance.\nHowever, implementations **may** fail to import the same payload multiple times\ninto a given physical device due to platform constraints.\n\nImporting memory from a particular host pointer **may** not be possible due to\nadditional platform-specific restrictions beyond the scope of this\nspecification in which case the implementation **must** fail the memory import\noperation with the error code [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE_KHR`].\n\nWhether device memory objects imported from a host pointer hold a reference\nto their payload is undefined.\nAs such, the application **must** ensure that the imported memory range remains\nvalid and accessible for the lifetime of the imported memory object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01747  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported in [`crate::vk::ExternalMemoryProperties`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01748  \n   If [`Self::handle_type`] is not `0`, it **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-pHostPointer-01749  \n  [`Self::p_host_pointer`] **must** be a pointer aligned to an integer multiple of[`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`]::`minImportedHostPointerAlignment`\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01750  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`],[`Self::p_host_pointer`] **must** be a pointer to `allocationSize` number of\n  bytes of host memory, where `allocationSize` is the member of the[`crate::vk::MemoryAllocateInfo`] structure this structure is chained to\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01751  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`],[`Self::p_host_pointer`] **must** be a pointer to `allocationSize` number of\n  bytes of host mapped foreign memory, where `allocationSize` is the\n  member of the [`crate::vk::MemoryAllocateInfo`] structure this structure is\n  chained to\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_HOST_POINTER_INFO_EXT`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImportMemoryHostPointerInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportMemoryHostPointerInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
    pub p_host_pointer: *mut std::ffi::c_void,
}
impl ImportMemoryHostPointerInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_MEMORY_HOST_POINTER_INFO_EXT;
}
impl Default for ImportMemoryHostPointerInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_type: Default::default(), p_host_pointer: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for ImportMemoryHostPointerInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportMemoryHostPointerInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_type", &self.handle_type).field("p_host_pointer", &self.p_host_pointer).finish()
    }
}
impl ImportMemoryHostPointerInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportMemoryHostPointerInfoEXTBuilder<'a> {
        ImportMemoryHostPointerInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryHostPointerInfoEXT.html)) · Builder of [`ImportMemoryHostPointerInfoEXT`] <br/> VkImportMemoryHostPointerInfoEXT - Import memory from a host pointer\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a host pointer, add a[`crate::vk::ImportMemoryHostPointerInfoEXT`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportMemoryHostPointerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkImportMemoryHostPointerInfoEXT {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    void*                                 pHostPointer;\n} VkImportMemoryHostPointerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the handle type.\n\n* [`Self::p_host_pointer`] is the host pointer to import from.\n[](#_description)Description\n----------\n\nImporting memory from a host pointer shares ownership of the memory between\nthe host and the Vulkan implementation.\nThe application **can** continue to access the memory through the host pointer\nbut it is the application’s responsibility to synchronize device and\nnon-device access to the payload as defined in[Host Access to Device Memory Objects](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-device-hostaccess).\n\nApplications **can** import the same payload into multiple instances of Vulkan\nand multiple times into a given Vulkan instance.\nHowever, implementations **may** fail to import the same payload multiple times\ninto a given physical device due to platform constraints.\n\nImporting memory from a particular host pointer **may** not be possible due to\nadditional platform-specific restrictions beyond the scope of this\nspecification in which case the implementation **must** fail the memory import\noperation with the error code [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE_KHR`].\n\nWhether device memory objects imported from a host pointer hold a reference\nto their payload is undefined.\nAs such, the application **must** ensure that the imported memory range remains\nvalid and accessible for the lifetime of the imported memory object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01747  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported in [`crate::vk::ExternalMemoryProperties`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01748  \n   If [`Self::handle_type`] is not `0`, it **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-pHostPointer-01749  \n  [`Self::p_host_pointer`] **must** be a pointer aligned to an integer multiple of[`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`]::`minImportedHostPointerAlignment`\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01750  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`],[`Self::p_host_pointer`] **must** be a pointer to `allocationSize` number of\n  bytes of host memory, where `allocationSize` is the member of the[`crate::vk::MemoryAllocateInfo`] structure this structure is chained to\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-01751  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`],[`Self::p_host_pointer`] **must** be a pointer to `allocationSize` number of\n  bytes of host mapped foreign memory, where `allocationSize` is the\n  member of the [`crate::vk::MemoryAllocateInfo`] structure this structure is\n  chained to\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_HOST_POINTER_INFO_EXT`]\n\n* []() VUID-VkImportMemoryHostPointerInfoEXT-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImportMemoryHostPointerInfoEXTBuilder<'a>(ImportMemoryHostPointerInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ImportMemoryHostPointerInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ImportMemoryHostPointerInfoEXTBuilder<'a> {
        ImportMemoryHostPointerInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn host_pointer(mut self, host_pointer: *mut std::ffi::c_void) -> Self {
        self.0.p_host_pointer = host_pointer;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportMemoryHostPointerInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for ImportMemoryHostPointerInfoEXTBuilder<'a> {
    fn default() -> ImportMemoryHostPointerInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportMemoryHostPointerInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportMemoryHostPointerInfoEXTBuilder<'a> {
    type Target = ImportMemoryHostPointerInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportMemoryHostPointerInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryHostPointerPropertiesEXT.html)) · Structure <br/> VkMemoryHostPointerPropertiesEXT - Properties of external memory host pointer\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryHostPointerPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkMemoryHostPointerPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryHostPointerPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified host pointer **can** be imported as.\n[](#_description)Description\n----------\n\nThe value returned by [`Self::memory_type_bits`] **must** only include bits that\nidentify memory types which are host visible.\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryHostPointerPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_HOST_POINTER_PROPERTIES_EXT`]\n\n* []() VUID-VkMemoryHostPointerPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_host_pointer_properties_ext`]\n"]
#[doc(alias = "VkMemoryHostPointerPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryHostPointerPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub memory_type_bits: u32,
}
impl MemoryHostPointerPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_HOST_POINTER_PROPERTIES_EXT;
}
impl Default for MemoryHostPointerPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), memory_type_bits: Default::default() }
    }
}
impl std::fmt::Debug for MemoryHostPointerPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryHostPointerPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_type_bits", &self.memory_type_bits).finish()
    }
}
impl MemoryHostPointerPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryHostPointerPropertiesEXTBuilder<'a> {
        MemoryHostPointerPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryHostPointerPropertiesEXT.html)) · Builder of [`MemoryHostPointerPropertiesEXT`] <br/> VkMemoryHostPointerPropertiesEXT - Properties of external memory host pointer\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryHostPointerPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkMemoryHostPointerPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryHostPointerPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified host pointer **can** be imported as.\n[](#_description)Description\n----------\n\nThe value returned by [`Self::memory_type_bits`] **must** only include bits that\nidentify memory types which are host visible.\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryHostPointerPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_HOST_POINTER_PROPERTIES_EXT`]\n\n* []() VUID-VkMemoryHostPointerPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_host_pointer_properties_ext`]\n"]
#[repr(transparent)]
pub struct MemoryHostPointerPropertiesEXTBuilder<'a>(MemoryHostPointerPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryHostPointerPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryHostPointerPropertiesEXTBuilder<'a> {
        MemoryHostPointerPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_type_bits(mut self, memory_type_bits: u32) -> Self {
        self.0.memory_type_bits = memory_type_bits as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryHostPointerPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for MemoryHostPointerPropertiesEXTBuilder<'a> {
    fn default() -> MemoryHostPointerPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryHostPointerPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryHostPointerPropertiesEXTBuilder<'a> {
    type Target = MemoryHostPointerPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryHostPointerPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExternalMemoryHostPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceExternalMemoryHostPropertiesEXT - Structure describing external memory host pointer limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkPhysicalDeviceExternalMemoryHostPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       minImportedHostPointerAlignment;\n} VkPhysicalDeviceExternalMemoryHostPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::min_imported_host_pointer_alignment`] is the minimum **required**alignment, in bytes, for the base address and size of host pointers that**can** be imported to a Vulkan memory object.\n  The value **must** be a power of two.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExternalMemoryHostPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceExternalMemoryHostPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceExternalMemoryHostPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub min_imported_host_pointer_alignment: crate::vk1_0::DeviceSize,
}
impl PhysicalDeviceExternalMemoryHostPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceExternalMemoryHostPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), min_imported_host_pointer_alignment: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceExternalMemoryHostPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceExternalMemoryHostPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("min_imported_host_pointer_alignment", &self.min_imported_host_pointer_alignment).finish()
    }
}
impl PhysicalDeviceExternalMemoryHostPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
        PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceExternalMemoryHostPropertiesEXT.html)) · Builder of [`PhysicalDeviceExternalMemoryHostPropertiesEXT`] <br/> VkPhysicalDeviceExternalMemoryHostPropertiesEXT - Structure describing external memory host pointer limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_external_memory_host\ntypedef struct VkPhysicalDeviceExternalMemoryHostPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceSize       minImportedHostPointerAlignment;\n} VkPhysicalDeviceExternalMemoryHostPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::min_imported_host_pointer_alignment`] is the minimum **required**alignment, in bytes, for the base address and size of host pointers that**can** be imported to a Vulkan memory object.\n  The value **must** be a power of two.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceExternalMemoryHostPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceSize`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a>(PhysicalDeviceExternalMemoryHostPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
        PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn min_imported_host_pointer_alignment(mut self, min_imported_host_pointer_alignment: crate::vk1_0::DeviceSize) -> Self {
        self.0.min_imported_host_pointer_alignment = min_imported_host_pointer_alignment as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceExternalMemoryHostPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceExternalMemoryHostPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceExternalMemoryHostPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_external_memory_host`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryHostPointerPropertiesEXT.html)) · Function <br/> vkGetMemoryHostPointerPropertiesEXT - Get properties of external memory host pointer\n[](#_c_specification)C Specification\n----------\n\nTo determine the correct parameters to use when importing host pointers,\ncall:\n\n```\n// Provided by VK_EXT_external_memory_host\nVkResult vkGetMemoryHostPointerPropertiesEXT(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    const void*                                 pHostPointer,\n    VkMemoryHostPointerPropertiesEXT*           pMemoryHostPointerProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing[`Self::p_host_pointer`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::p_host_pointer`].\n\n* [`Self::p_host_pointer`] is the host pointer to import from.\n\n* [`Self::p_memory_host_pointer_properties`] is a pointer to a[`crate::vk::MemoryHostPointerPropertiesEXT`] structure in which the host\n  pointer properties are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01752  \n  [`Self::handle_type`] **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`]\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-pHostPointer-01753  \n  [`Self::p_host_pointer`] **must** be a pointer aligned to an integer multiple of[`crate::vk::PhysicalDeviceExternalMemoryHostPropertiesEXT`]::`minImportedHostPointerAlignment`\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01754  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_ALLOCATION_EXT`],[`Self::p_host_pointer`] **must** be a pointer to host memory\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-01755  \n   If [`Self::handle_type`] is[`crate::vk::ExternalMemoryHandleTypeFlagBits::HOST_MAPPED_FOREIGN_MEMORY_EXT`],[`Self::p_host_pointer`] **must** be a pointer to host mapped foreign memory\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryHostPointerPropertiesEXT-pMemoryHostPointerProperties-parameter  \n  [`Self::p_memory_host_pointer_properties`] **must** be a valid pointer to a [`crate::vk::MemoryHostPointerPropertiesEXT`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryHostPointerPropertiesEXT`]\n"]
    #[doc(alias = "vkGetMemoryHostPointerPropertiesEXT")]
    pub unsafe fn get_memory_host_pointer_properties_ext(&self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, host_pointer: *const std::ffi::c_void, memory_host_pointer_properties: Option<crate::extensions::ext_external_memory_host::MemoryHostPointerPropertiesEXT>) -> crate::utils::VulkanResult<crate::extensions::ext_external_memory_host::MemoryHostPointerPropertiesEXT> {
        let _function = self.get_memory_host_pointer_properties_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut memory_host_pointer_properties = match memory_host_pointer_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, handle_type as _, host_pointer, &mut memory_host_pointer_properties);
        crate::utils::VulkanResult::new(_return, {
            memory_host_pointer_properties.p_next = std::ptr::null_mut() as _;
            memory_host_pointer_properties
        })
    }
}
