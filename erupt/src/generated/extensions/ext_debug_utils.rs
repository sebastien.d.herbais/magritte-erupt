#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_UTILS_SPEC_VERSION")]
pub const EXT_DEBUG_UTILS_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_UTILS_EXTENSION_NAME")]
pub const EXT_DEBUG_UTILS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_debug_utils");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_SET_DEBUG_UTILS_OBJECT_NAME_EXT: *const std::os::raw::c_char = crate::cstr!("vkSetDebugUtilsObjectNameEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_SET_DEBUG_UTILS_OBJECT_TAG_EXT: *const std::os::raw::c_char = crate::cstr!("vkSetDebugUtilsObjectTagEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_QUEUE_BEGIN_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkQueueBeginDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_QUEUE_END_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkQueueEndDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_QUEUE_INSERT_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkQueueInsertDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BEGIN_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdBeginDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_END_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdEndDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_INSERT_DEBUG_UTILS_LABEL_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdInsertDebugUtilsLabelEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_DEBUG_UTILS_MESSENGER_EXT: *const std::os::raw::c_char = crate::cstr!("vkCreateDebugUtilsMessengerEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_DEBUG_UTILS_MESSENGER_EXT: *const std::os::raw::c_char = crate::cstr!("vkDestroyDebugUtilsMessengerEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_SUBMIT_DEBUG_UTILS_MESSAGE_EXT: *const std::os::raw::c_char = crate::cstr!("vkSubmitDebugUtilsMessageEXT");
crate::non_dispatchable_handle!(DebugUtilsMessengerEXT, DEBUG_UTILS_MESSENGER_EXT, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerEXT.html)) · Non-dispatchable Handle <br/> VkDebugUtilsMessengerEXT - Opaque handle to a debug messenger object\n[](#_c_specification)C Specification\n----------\n\nA [`crate::vk::DebugUtilsMessengerEXT`] is a messenger object which handles passing\nalong debug messages to a provided debug callback.\n\n```\n// Provided by VK_EXT_debug_utils\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkDebugUtilsMessengerEXT)\n```\n[](#_description)Description\n----------\n\nThe debug messenger will provide detailed feedback on the application’s use\nof Vulkan when events of interest occur.\nWhen an event of interest does occur, the debug messenger will submit a\ndebug message to the debug callback that was provided during its creation.\nAdditionally, the debug messenger is responsible with filtering out debug\nmessages that the callback is not interested in and will only provide\ndesired debug messages.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`], [`crate::vk::InstanceLoader::destroy_debug_utils_messenger_ext`]\n", "VkDebugUtilsMessengerEXT");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCreateFlagsEXT.html)) · Bitmask of [`DebugUtilsMessengerCreateFlagBitsEXT`] <br/> VkDebugUtilsMessengerCreateFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef VkFlags VkDebugUtilsMessengerCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DebugUtilsMessengerCreateFlagBitsEXT`] is a bitmask type for setting a\nmask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]\n"] # [doc (alias = "VkDebugUtilsMessengerCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DebugUtilsMessengerCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DebugUtilsMessengerCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkDebugUtilsMessengerCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugUtilsMessengerCreateFlagBitsEXT(pub u32);
impl DebugUtilsMessengerCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DebugUtilsMessengerCreateFlagsEXT {
        DebugUtilsMessengerCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DebugUtilsMessengerCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCallbackDataFlagsEXT.html)) · Bitmask of [`DebugUtilsMessengerCallbackDataFlagBitsEXT`] <br/> VkDebugUtilsMessengerCallbackDataFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef VkFlags VkDebugUtilsMessengerCallbackDataFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataFlagBitsEXT`] is a bitmask type for\nsetting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataEXT`]\n"] # [doc (alias = "VkDebugUtilsMessengerCallbackDataFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DebugUtilsMessengerCallbackDataFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`DebugUtilsMessengerCallbackDataFlagsEXT`] <br/> "]
#[doc(alias = "VkDebugUtilsMessengerCallbackDataFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugUtilsMessengerCallbackDataFlagBitsEXT(pub u32);
impl DebugUtilsMessengerCallbackDataFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DebugUtilsMessengerCallbackDataFlagsEXT {
        DebugUtilsMessengerCallbackDataFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DebugUtilsMessengerCallbackDataFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::vk1_0::StructureType {
    pub const DEBUG_UTILS_OBJECT_NAME_INFO_EXT: Self = Self(1000128000);
    pub const DEBUG_UTILS_OBJECT_TAG_INFO_EXT: Self = Self(1000128001);
    pub const DEBUG_UTILS_LABEL_EXT: Self = Self(1000128002);
    pub const DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT: Self = Self(1000128003);
    pub const DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT: Self = Self(1000128004);
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::vk1_0::ObjectType {
    pub const DEBUG_UTILS_MESSENGER_EXT: Self = Self(1000128000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagsEXT.html)) · Bitmask of [`DebugUtilsMessageSeverityFlagBitsEXT`] <br/> VkDebugUtilsMessageSeverityFlagsEXT - Bitmask of VkDebugUtilsMessageSeverityFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef VkFlags VkDebugUtilsMessageSeverityFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DebugUtilsMessageSeverityFlagBitsEXT`] is a bitmask type for setting a\nmask of zero or more [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html), [`crate::vk::DebugUtilsMessengerCreateInfoEXT`]\n"] # [doc (alias = "VkDebugUtilsMessageSeverityFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DebugUtilsMessageSeverityFlagsEXT : u32 { const VERBOSE_EXT = DebugUtilsMessageSeverityFlagBitsEXT :: VERBOSE_EXT . 0 ; const INFO_EXT = DebugUtilsMessageSeverityFlagBitsEXT :: INFO_EXT . 0 ; const WARNING_EXT = DebugUtilsMessageSeverityFlagBitsEXT :: WARNING_EXT . 0 ; const ERROR_EXT = DebugUtilsMessageSeverityFlagBitsEXT :: ERROR_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html)) · Bits enum of [`DebugUtilsMessageSeverityFlagsEXT`] <br/> VkDebugUtilsMessageSeverityFlagBitsEXT - Bitmask specifying which severities of events cause a debug messenger callback\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::DebugUtilsMessengerCreateInfoEXT::message_severity`], specifying\nevent severities which cause a debug messenger to call the callback, are:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef enum VkDebugUtilsMessageSeverityFlagBitsEXT {\n    VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT = 0x00000001,\n    VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT = 0x00000010,\n    VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT = 0x00000100,\n    VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT = 0x00001000,\n} VkDebugUtilsMessageSeverityFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::VERBOSE_EXT`] specifies the most\n  verbose output indicating all diagnostic messages from the Vulkan\n  loader, layers, and drivers should be captured.\n\n* [`Self::INFO_EXT`] specifies an\n  informational message such as resource details that may be handy when\n  debugging an application.\n\n* [`Self::WARNING_EXT`] specifies use of\n  Vulkan that **may** expose an app bug.\n  Such cases may not be immediately harmful, such as a fragment shader\n  outputting to a location with no attachment.\n  Other cases **may** point to behavior that is almost certainly bad when\n  unintended such as using an image whose memory has not been filled.\n  In general if you see a warning but you know that the behavior is\n  intended/desired, then simply ignore the warning.\n\n* [`Self::ERROR_EXT`] specifies that the\n  application has violated a valid usage condition of the specification.\n\n|   |Note<br/><br/>The values of [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) are sorted based<br/>on severity.<br/>The higher the flag value, the more severe the message.<br/>This allows for simple boolean operation comparisons when looking at[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) values.<br/><br/>For example:<br/><br/>```<br/>    if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {<br/>        // Do something for warnings and errors<br/>    }<br/>```<br/><br/>In addition, space has been left between the enums to allow for later<br/>addition of new severities in between the existing values.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessageSeverityFlagBitsEXT`], [`crate::vk::InstanceLoader::submit_debug_utils_message_ext`]\n"]
#[doc(alias = "VkDebugUtilsMessageSeverityFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugUtilsMessageSeverityFlagBitsEXT(pub u32);
impl DebugUtilsMessageSeverityFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DebugUtilsMessageSeverityFlagsEXT {
        DebugUtilsMessageSeverityFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DebugUtilsMessageSeverityFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VERBOSE_EXT => "VERBOSE_EXT",
            &Self::INFO_EXT => "INFO_EXT",
            &Self::WARNING_EXT => "WARNING_EXT",
            &Self::ERROR_EXT => "ERROR_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagBitsEXT {
    pub const VERBOSE_EXT: Self = Self(1);
    pub const INFO_EXT: Self = Self(16);
    pub const WARNING_EXT: Self = Self(256);
    pub const ERROR_EXT: Self = Self(4096);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagsEXT.html)) · Bitmask of [`DebugUtilsMessageTypeFlagBitsEXT`] <br/> VkDebugUtilsMessageTypeFlagsEXT - Bitmask of VkDebugUtilsMessageTypeFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef VkFlags VkDebugUtilsMessageTypeFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`] is a bitmask type for setting a mask\nof zero or more [VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html), [`crate::vk::DebugUtilsMessengerCreateInfoEXT`], [`crate::vk::InstanceLoader::submit_debug_utils_message_ext`]\n"] # [doc (alias = "VkDebugUtilsMessageTypeFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DebugUtilsMessageTypeFlagsEXT : u32 { const GENERAL_EXT = DebugUtilsMessageTypeFlagBitsEXT :: GENERAL_EXT . 0 ; const VALIDATION_EXT = DebugUtilsMessageTypeFlagBitsEXT :: VALIDATION_EXT . 0 ; const PERFORMANCE_EXT = DebugUtilsMessageTypeFlagBitsEXT :: PERFORMANCE_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html)) · Bits enum of [`DebugUtilsMessageTypeFlagsEXT`] <br/> VkDebugUtilsMessageTypeFlagBitsEXT - Bitmask specifying which types of events cause a debug messenger callback\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::DebugUtilsMessengerCreateInfoEXT::message_type`], specifying\nevent types which cause a debug messenger to call the callback, are:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef enum VkDebugUtilsMessageTypeFlagBitsEXT {\n    VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT = 0x00000001,\n    VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT = 0x00000002,\n    VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT = 0x00000004,\n} VkDebugUtilsMessageTypeFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::GENERAL_EXT`] specifies that some\n  general event has occurred.\n  This is typically a non-specification, non-performance event.\n\n* [`Self::VALIDATION_EXT`] specifies that\n  something has occurred during validation against the Vulkan\n  specification that may indicate invalid behavior.\n\n* [`Self::PERFORMANCE_EXT`] specifies a\n  potentially non-optimal use of Vulkan, e.g. using[`crate::vk::PFN_vkCmdClearColorImage`] when setting[`crate::vk::AttachmentDescription::load_op`] to[`crate::vk::AttachmentLoadOp::CLEAR`] would have worked.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`]\n"]
#[doc(alias = "VkDebugUtilsMessageTypeFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugUtilsMessageTypeFlagBitsEXT(pub u32);
impl DebugUtilsMessageTypeFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DebugUtilsMessageTypeFlagsEXT {
        DebugUtilsMessageTypeFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DebugUtilsMessageTypeFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::GENERAL_EXT => "GENERAL_EXT",
            &Self::VALIDATION_EXT => "VALIDATION_EXT",
            &Self::PERFORMANCE_EXT => "PERFORMANCE_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagBitsEXT {
    pub const GENERAL_EXT: Self = Self(1);
    pub const VALIDATION_EXT: Self = Self(2);
    pub const PERFORMANCE_EXT: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetDebugUtilsObjectNameEXT.html)) · Function <br/> vkSetDebugUtilsObjectNameEXT - Give a user-friendly name to an object\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkSetDebugUtilsObjectNameEXT(\n    VkDevice                                    device,\n    const VkDebugUtilsObjectNameInfoEXT*        pNameInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_name_info`] is a pointer to a [`crate::vk::DebugUtilsObjectNameInfoEXT`]structure specifying parameters of the name to set on the object.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-02587  \n  `pNameInfo->objectType` **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-02588  \n  `pNameInfo->objectHandle` **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-parameter  \n  [`Self::p_name_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsObjectNameInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pNameInfo->objectHandle` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsObjectNameInfoEXT`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkSetDebugUtilsObjectNameEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_name_info: *const crate::extensions::ext_debug_utils::DebugUtilsObjectNameInfoEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetDebugUtilsObjectTagEXT.html)) · Function <br/> vkSetDebugUtilsObjectTagEXT - Attach arbitrary data to an object\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkSetDebugUtilsObjectTagEXT(\n    VkDevice                                    device,\n    const VkDebugUtilsObjectTagInfoEXT*         pTagInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_tag_info`] is a pointer to a [`crate::vk::DebugUtilsObjectTagInfoEXT`]structure specifying parameters of the tag to attach to the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetDebugUtilsObjectTagEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetDebugUtilsObjectTagEXT-pTagInfo-parameter  \n  [`Self::p_tag_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsObjectTagInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pTagInfo->objectHandle` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsObjectTagInfoEXT`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkSetDebugUtilsObjectTagEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_tag_info: *const crate::extensions::ext_debug_utils::DebugUtilsObjectTagInfoEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueBeginDebugUtilsLabelEXT.html)) · Function <br/> vkQueueBeginDebugUtilsLabelEXT - Open a queue debug label region\n[](#_c_specification)C Specification\n----------\n\nA queue debug label region is opened by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueBeginDebugUtilsLabelEXT(\n    VkQueue                                     queue,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue in which to start a debug label region.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueBeginDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueBeginDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::Queue`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkQueueBeginDebugUtilsLabelEXT = unsafe extern "system" fn(queue: crate::vk1_0::Queue, p_label_info: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueEndDebugUtilsLabelEXT.html)) · Function <br/> vkQueueEndDebugUtilsLabelEXT - Close a queue debug label region\n[](#_c_specification)C Specification\n----------\n\nA queue debug label region is closed by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueEndDebugUtilsLabelEXT(\n    VkQueue                                     queue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue in which a debug label region should be closed.\n[](#_description)Description\n----------\n\nThe calls to [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`] and[`crate::vk::InstanceLoader::queue_end_debug_utils_label_ext`] **must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkQueueEndDebugUtilsLabelEXT-None-01911  \n   There **must** be an outstanding [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`]command prior to the [`crate::vk::InstanceLoader::queue_end_debug_utils_label_ext`] on the queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueEndDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Queue`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkQueueEndDebugUtilsLabelEXT = unsafe extern "system" fn(queue: crate::vk1_0::Queue) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueInsertDebugUtilsLabelEXT.html)) · Function <br/> vkQueueInsertDebugUtilsLabelEXT - Insert a label into a queue\n[](#_c_specification)C Specification\n----------\n\nA single label can be inserted into a queue by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueInsertDebugUtilsLabelEXT(\n    VkQueue                                     queue,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue into which a debug label will be inserted.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueInsertDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueInsertDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::Queue`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkQueueInsertDebugUtilsLabelEXT = unsafe extern "system" fn(queue: crate::vk1_0::Queue, p_label_info: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginDebugUtilsLabelEXT.html)) · Function <br/> vkCmdBeginDebugUtilsLabelEXT - Open a command buffer debug label region\n[](#_c_specification)C Specification\n----------\n\nA command buffer debug label region can be opened by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdBeginDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugUtilsLabelEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBeginDebugUtilsLabelEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_label_info: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndDebugUtilsLabelEXT.html)) · Function <br/> vkCmdEndDebugUtilsLabelEXT - Close a command buffer label region\n[](#_c_specification)C Specification\n----------\n\nA command buffer label region can be closed by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdEndDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n[](#_description)Description\n----------\n\nAn application **may** open a debug label region in one command buffer and\nclose it in another, or otherwise split debug label regions across multiple\ncommand buffers or multiple queue submissions.\nWhen viewed from the linear series of submissions to a single queue, the\ncalls to [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] and[`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`] **must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-01912  \n   There **must** be an outstanding [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] command\n  prior to the [`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`] on the queue that[`Self::command_buffer`] is submitted to\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-01913  \n   If [`Self::command_buffer`] is a secondary command buffer, there **must** be an\n  outstanding [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] command recorded to[`Self::command_buffer`] that has not previously been ended by a call to[`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdEndDebugUtilsLabelEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdInsertDebugUtilsLabelEXT.html)) · Function <br/> vkCmdInsertDebugUtilsLabelEXT - Insert a label into a command buffer\n[](#_c_specification)C Specification\n----------\n\nA single debug label can be inserted into a command buffer by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdInsertDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* `pInfo` is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugUtilsLabelEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdInsertDebugUtilsLabelEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_label_info: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDebugUtilsMessengerEXT.html)) · Function <br/> vkCreateDebugUtilsMessengerEXT - Create a debug messenger object\n[](#_c_specification)C Specification\n----------\n\nA debug messenger triggers a debug callback with a debug message when an\nevent of interest occurs.\nTo create a debug messenger which will trigger a debug callback, call:\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkCreateDebugUtilsMessengerEXT(\n    VkInstance                                  instance,\n    const VkDebugUtilsMessengerCreateInfoEXT*   pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDebugUtilsMessengerEXT*                   pMessenger);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance the messenger will be used with.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::DebugUtilsMessengerCreateInfoEXT`] structure containing the\n  callback pointer, as well as defining conditions under which this\n  messenger will trigger the callback.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_messenger`] is a pointer to a [`crate::vk::DebugUtilsMessengerEXT`] handle\n  in which the created object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsMessengerCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pMessenger-parameter  \n  [`Self::p_messenger`] **must** be a valid pointer to a [`crate::vk::DebugUtilsMessengerEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\nThe application **must** ensure that [`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`] is\nnot executed in parallel with any Vulkan command that is also called with[`Self::instance`] or child of [`Self::instance`] as the dispatchable argument.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugUtilsMessengerCreateInfoEXT`], [`crate::vk::DebugUtilsMessengerEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateDebugUtilsMessengerEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::ext_debug_utils::DebugUtilsMessengerCreateInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_messenger: *mut crate::extensions::ext_debug_utils::DebugUtilsMessengerEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyDebugUtilsMessengerEXT.html)) · Function <br/> vkDestroyDebugUtilsMessengerEXT - Destroy a debug messenger object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a [`crate::vk::DebugUtilsMessengerEXT`] object, call:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkDestroyDebugUtilsMessengerEXT(\n    VkInstance                                  instance,\n    VkDebugUtilsMessengerEXT                    messenger,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance where the callback was created.\n\n* [`Self::messenger`] is the [`crate::vk::DebugUtilsMessengerEXT`] object to destroy.[`Self::messenger`] is an externally synchronized object and **must** not be\n  used on more than one thread at a time.\n  This means that [`crate::vk::InstanceLoader::destroy_debug_utils_messenger_ext`] **must** not be\n  called when a callback is active.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-01915  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::messenger`] was\n  created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-01916  \n   If no [`crate::vk::AllocationCallbacks`] were provided when [`Self::messenger`] was\n  created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-parameter  \n   If [`Self::messenger`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::messenger`] **must** be a valid [`crate::vk::DebugUtilsMessengerEXT`] handle\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-parent  \n   If [`Self::messenger`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::instance`]\n\nHost Synchronization\n\n* Host access to [`Self::messenger`] **must** be externally synchronized\n\nThe application **must** ensure that [`crate::vk::InstanceLoader::destroy_debug_utils_messenger_ext`] is\nnot executed in parallel with any Vulkan command that is also called with[`Self::instance`] or child of [`Self::instance`] as the dispatchable argument.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugUtilsMessengerEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyDebugUtilsMessengerEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, messenger: crate::extensions::ext_debug_utils::DebugUtilsMessengerEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSubmitDebugUtilsMessageEXT.html)) · Function <br/> vkSubmitDebugUtilsMessageEXT - Inject a message into a debug stream\n[](#_c_specification)C Specification\n----------\n\nThere may be times that a user wishes to intentionally submit a debug\nmessage.\nTo do this, call:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkSubmitDebugUtilsMessageEXT(\n    VkInstance                                  instance,\n    VkDebugUtilsMessageSeverityFlagBitsEXT      messageSeverity,\n    VkDebugUtilsMessageTypeFlagsEXT             messageTypes,\n    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the debug stream’s [`crate::vk::Instance`].\n\n* [`Self::message_severity`] is a [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html)value specifying the severity of this event/message.\n\n* [`Self::message_types`] is a bitmask of[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) specifying which type of\n  event(s) to identify with this message.\n\n* [`Self::p_callback_data`] contains all the callback related data in the[`crate::vk::DebugUtilsMessengerCallbackDataEXT`] structure.\n[](#_description)Description\n----------\n\nThe call will propagate through the layers and generate callback(s) as\nindicated by the message’s flags.\nThe parameters are passed on to the callback in addition to the`pUserData` value that was defined at the time the messenger was\nregistered.\n\nValid Usage\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-objectType-02591  \n   The `objectType` member of each element of`pCallbackData->pObjects` **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageSeverity-parameter  \n  [`Self::message_severity`] **must** be a valid [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) value\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageTypes-parameter  \n  [`Self::message_types`] **must** be a valid combination of [VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) values\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageTypes-requiredbitmask  \n  [`Self::message_types`] **must** not be `0`\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-pCallbackData-parameter  \n  [`Self::p_callback_data`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsMessengerCallbackDataEXT`] structure\n[](#_see_also)See Also\n----------\n\n[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html), [`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`], [`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkSubmitDebugUtilsMessageEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, message_severity: crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagBitsEXT, message_types: crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagsEXT, p_callback_data: *const crate::extensions::ext_debug_utils::DebugUtilsMessengerCallbackDataEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/PFN_vkDebugUtilsMessengerCallbackEXT.html)) · Function <br/> PFN\\_vkDebugUtilsMessengerCallbackEXT - Application-defined debug messenger callback function\n[](#_c_specification)C Specification\n----------\n\nThe prototype for the[`crate::vk::DebugUtilsMessengerCreateInfoEXT::pfn_user_callback`] function\nimplemented by the application is:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef VkBool32 (VKAPI_PTR *PFN_vkDebugUtilsMessengerCallbackEXT)(\n    VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity,\n    VkDebugUtilsMessageTypeFlagsEXT                  messageTypes,\n    const VkDebugUtilsMessengerCallbackDataEXT*      pCallbackData,\n    void*                                            pUserData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::message_severity`] specifies the[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) that triggered this\n  callback.\n\n* [`Self::message_types`] is a bitmask of[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) specifying which type of\n  event(s) triggered this callback.\n\n* [`Self::p_callback_data`] contains all the callback related data in the[`crate::vk::DebugUtilsMessengerCallbackDataEXT`] structure.\n\n* [`Self::p_user_data`] is the user data provided when the[`crate::vk::DebugUtilsMessengerEXT`] was created.\n[](#_description)Description\n----------\n\nThe callback returns a [`crate::vk::Bool32`], which is interpreted in a\nlayer-specified manner.\nThe application **should** always return [`crate::vk::FALSE`].\nThe [`crate::vk::TRUE`] value is reserved for use in layer development.\n\nValid Usage\n\n* []() VUID-PFN\\_vkDebugUtilsMessengerCallbackEXT-None-04769  \n   The callback **must** not make calls to any Vulkan commands.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDebugUtilsMessengerCallbackEXT = unsafe extern "system" fn(message_severity: crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagBitsEXT, message_types: crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagsEXT, p_callback_data: *const crate::extensions::ext_debug_utils::DebugUtilsMessengerCallbackDataEXT, p_user_data: *mut std::ffi::c_void) -> crate::vk1_0::Bool32;
impl<'a> crate::ExtendableFromConst<'a, DebugUtilsMessengerCreateInfoEXT> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DebugUtilsMessengerCreateInfoEXTBuilder<'_>> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsObjectNameInfoEXT.html)) · Structure <br/> VkDebugUtilsObjectNameInfoEXT - Specify parameters of a name to give to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsObjectNameInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsObjectNameInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkObjectType       objectType;\n    uint64_t           objectHandle;\n    const char*        pObjectName;\n} VkDebugUtilsObjectNameInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of the\n  object to be named.\n\n* [`Self::object_handle`] is the object to be named.\n\n* [`Self::p_object_name`] is either `NULL` or a null-terminated UTF-8 string\n  specifying the name to apply to [`Self::object_handle`].\n[](#_description)Description\n----------\n\nApplications **may** change the name associated with an object simply by\ncalling [`crate::vk::InstanceLoader::set_debug_utils_object_name_ext`] again with a new string.\nIf [`Self::p_object_name`] is either `NULL` or an empty string, then any\npreviously set name is removed.\n\nValid Usage\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-02589  \n   If [`Self::object_type`] is [`crate::vk::ObjectType::UNKNOWN`], [`Self::object_handle`]**must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-02590  \n   If [`Self::object_type`] is not [`crate::vk::ObjectType::UNKNOWN`],[`Self::object_handle`] **must** be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a valid Vulkan\n  handle of the type associated with [`Self::object_type`] as defined in the[[`crate::vk::ObjectType`] and Vulkan Handle\n  Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_OBJECT_NAME_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-pObjectName-parameter  \n   If [`Self::p_object_name`] is not `NULL`, [`Self::p_object_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::ObjectType`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::set_debug_utils_object_name_ext`]\n"]
#[doc(alias = "VkDebugUtilsObjectNameInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugUtilsObjectNameInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub object_type: crate::vk1_0::ObjectType,
    pub object_handle: u64,
    pub p_object_name: *const std::os::raw::c_char,
}
impl DebugUtilsObjectNameInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
}
impl Default for DebugUtilsObjectNameInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), object_type: Default::default(), object_handle: Default::default(), p_object_name: std::ptr::null() }
    }
}
impl std::fmt::Debug for DebugUtilsObjectNameInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugUtilsObjectNameInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("object_type", &self.object_type).field("object_handle", &self.object_handle).field("p_object_name", &self.p_object_name).finish()
    }
}
impl DebugUtilsObjectNameInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugUtilsObjectNameInfoEXTBuilder<'a> {
        DebugUtilsObjectNameInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsObjectNameInfoEXT.html)) · Builder of [`DebugUtilsObjectNameInfoEXT`] <br/> VkDebugUtilsObjectNameInfoEXT - Specify parameters of a name to give to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsObjectNameInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsObjectNameInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkObjectType       objectType;\n    uint64_t           objectHandle;\n    const char*        pObjectName;\n} VkDebugUtilsObjectNameInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of the\n  object to be named.\n\n* [`Self::object_handle`] is the object to be named.\n\n* [`Self::p_object_name`] is either `NULL` or a null-terminated UTF-8 string\n  specifying the name to apply to [`Self::object_handle`].\n[](#_description)Description\n----------\n\nApplications **may** change the name associated with an object simply by\ncalling [`crate::vk::InstanceLoader::set_debug_utils_object_name_ext`] again with a new string.\nIf [`Self::p_object_name`] is either `NULL` or an empty string, then any\npreviously set name is removed.\n\nValid Usage\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-02589  \n   If [`Self::object_type`] is [`crate::vk::ObjectType::UNKNOWN`], [`Self::object_handle`]**must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-02590  \n   If [`Self::object_type`] is not [`crate::vk::ObjectType::UNKNOWN`],[`Self::object_handle`] **must** be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a valid Vulkan\n  handle of the type associated with [`Self::object_type`] as defined in the[[`crate::vk::ObjectType`] and Vulkan Handle\n  Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_OBJECT_NAME_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-VkDebugUtilsObjectNameInfoEXT-pObjectName-parameter  \n   If [`Self::p_object_name`] is not `NULL`, [`Self::p_object_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::ObjectType`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::set_debug_utils_object_name_ext`]\n"]
#[repr(transparent)]
pub struct DebugUtilsObjectNameInfoEXTBuilder<'a>(DebugUtilsObjectNameInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugUtilsObjectNameInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugUtilsObjectNameInfoEXTBuilder<'a> {
        DebugUtilsObjectNameInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn object_type(mut self, object_type: crate::vk1_0::ObjectType) -> Self {
        self.0.object_type = object_type as _;
        self
    }
    #[inline]
    pub fn object_handle(mut self, object_handle: u64) -> Self {
        self.0.object_handle = object_handle as _;
        self
    }
    #[inline]
    pub fn object_name(mut self, object_name: &'a std::ffi::CStr) -> Self {
        self.0.p_object_name = object_name.as_ptr();
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugUtilsObjectNameInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugUtilsObjectNameInfoEXTBuilder<'a> {
    fn default() -> DebugUtilsObjectNameInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugUtilsObjectNameInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugUtilsObjectNameInfoEXTBuilder<'a> {
    type Target = DebugUtilsObjectNameInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugUtilsObjectNameInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsObjectTagInfoEXT.html)) · Structure <br/> VkDebugUtilsObjectTagInfoEXT - Specify parameters of a tag to attach to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsObjectTagInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsObjectTagInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkObjectType       objectType;\n    uint64_t           objectHandle;\n    uint64_t           tagName;\n    size_t             tagSize;\n    const void*        pTag;\n} VkDebugUtilsObjectTagInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of the\n  object to be named.\n\n* [`Self::object_handle`] is the object to be tagged.\n\n* [`Self::tag_name`] is a numerical identifier of the tag.\n\n* [`Self::tag_size`] is the number of bytes of data to attach to the object.\n\n* [`Self::p_tag`] is a pointer to an array of [`Self::tag_size`] bytes containing\n  the data to be associated with the object.\n[](#_description)Description\n----------\n\nThe [`Self::tag_name`] parameter gives a name or identifier to the type of data\nbeing tagged.\nThis can be used by debugging layers to easily filter for only data that can\nbe used by that implementation.\n\nValid Usage\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectType-01908  \n  [`Self::object_type`] **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectHandle-01910  \n  [`Self::object_handle`] **must** be a valid Vulkan handle of the type associated\n  with [`Self::object_type`] as defined in the [[`crate::vk::ObjectType`] and Vulkan Handle Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_OBJECT_TAG_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-pTag-parameter  \n  [`Self::p_tag`] **must** be a valid pointer to an array of [`Self::tag_size`] bytes\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-tagSize-arraylength  \n  [`Self::tag_size`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ObjectType`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::set_debug_utils_object_tag_ext`]\n"]
#[doc(alias = "VkDebugUtilsObjectTagInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugUtilsObjectTagInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub object_type: crate::vk1_0::ObjectType,
    pub object_handle: u64,
    pub tag_name: u64,
    pub tag_size: usize,
    pub p_tag: *const std::ffi::c_void,
}
impl DebugUtilsObjectTagInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_UTILS_OBJECT_TAG_INFO_EXT;
}
impl Default for DebugUtilsObjectTagInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), object_type: Default::default(), object_handle: Default::default(), tag_name: Default::default(), tag_size: Default::default(), p_tag: std::ptr::null() }
    }
}
impl std::fmt::Debug for DebugUtilsObjectTagInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugUtilsObjectTagInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("object_type", &self.object_type).field("object_handle", &self.object_handle).field("tag_name", &self.tag_name).field("tag_size", &self.tag_size).field("p_tag", &self.p_tag).finish()
    }
}
impl DebugUtilsObjectTagInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugUtilsObjectTagInfoEXTBuilder<'a> {
        DebugUtilsObjectTagInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsObjectTagInfoEXT.html)) · Builder of [`DebugUtilsObjectTagInfoEXT`] <br/> VkDebugUtilsObjectTagInfoEXT - Specify parameters of a tag to attach to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsObjectTagInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsObjectTagInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkObjectType       objectType;\n    uint64_t           objectHandle;\n    uint64_t           tagName;\n    size_t             tagSize;\n    const void*        pTag;\n} VkDebugUtilsObjectTagInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::ObjectType`] specifying the type of the\n  object to be named.\n\n* [`Self::object_handle`] is the object to be tagged.\n\n* [`Self::tag_name`] is a numerical identifier of the tag.\n\n* [`Self::tag_size`] is the number of bytes of data to attach to the object.\n\n* [`Self::p_tag`] is a pointer to an array of [`Self::tag_size`] bytes containing\n  the data to be associated with the object.\n[](#_description)Description\n----------\n\nThe [`Self::tag_name`] parameter gives a name or identifier to the type of data\nbeing tagged.\nThis can be used by debugging layers to easily filter for only data that can\nbe used by that implementation.\n\nValid Usage\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectType-01908  \n  [`Self::object_type`] **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectHandle-01910  \n  [`Self::object_handle`] **must** be a valid Vulkan handle of the type associated\n  with [`Self::object_type`] as defined in the [[`crate::vk::ObjectType`] and Vulkan Handle Relationship](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-types) table\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_OBJECT_TAG_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::ObjectType`] value\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-pTag-parameter  \n  [`Self::p_tag`] **must** be a valid pointer to an array of [`Self::tag_size`] bytes\n\n* []() VUID-VkDebugUtilsObjectTagInfoEXT-tagSize-arraylength  \n  [`Self::tag_size`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ObjectType`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::set_debug_utils_object_tag_ext`]\n"]
#[repr(transparent)]
pub struct DebugUtilsObjectTagInfoEXTBuilder<'a>(DebugUtilsObjectTagInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugUtilsObjectTagInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugUtilsObjectTagInfoEXTBuilder<'a> {
        DebugUtilsObjectTagInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn object_type(mut self, object_type: crate::vk1_0::ObjectType) -> Self {
        self.0.object_type = object_type as _;
        self
    }
    #[inline]
    pub fn object_handle(mut self, object_handle: u64) -> Self {
        self.0.object_handle = object_handle as _;
        self
    }
    #[inline]
    pub fn tag_name(mut self, tag_name: u64) -> Self {
        self.0.tag_name = tag_name as _;
        self
    }
    #[inline]
    pub fn tag_size(mut self, tag_size: usize) -> Self {
        self.0.tag_size = tag_size;
        self
    }
    #[inline]
    pub fn tag(mut self, tag: *const std::ffi::c_void) -> Self {
        self.0.p_tag = tag;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugUtilsObjectTagInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugUtilsObjectTagInfoEXTBuilder<'a> {
    fn default() -> DebugUtilsObjectTagInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugUtilsObjectTagInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugUtilsObjectTagInfoEXTBuilder<'a> {
    type Target = DebugUtilsObjectTagInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugUtilsObjectTagInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsLabelEXT.html)) · Structure <br/> VkDebugUtilsLabelEXT - Specify parameters of a label region\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsLabelEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsLabelEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    const char*        pLabelName;\n    float              color[4];\n} VkDebugUtilsLabelEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_label_name`] is a pointer to a null-terminated UTF-8 string\n  containing the name of the label.\n\n* [`Self::color`] is an optional RGBA color value that can be associated with\n  the label.\n  A particular implementation **may** choose to ignore this color value.\n  The values contain RGBA values in order, in the range 0.0 to 1.0.\n  If all elements in [`Self::color`] are set to 0.0 then it is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsLabelEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_LABEL_EXT`]\n\n* []() VUID-VkDebugUtilsLabelEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsLabelEXT-pLabelName-parameter  \n  [`Self::p_label_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`], [`crate::vk::InstanceLoader::cmd_insert_debug_utils_label_ext`], [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`], [`crate::vk::InstanceLoader::queue_insert_debug_utils_label_ext`]\n"]
#[doc(alias = "VkDebugUtilsLabelEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugUtilsLabelEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_label_name: *const std::os::raw::c_char,
    pub color: [std::os::raw::c_float; 4],
}
impl DebugUtilsLabelEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_UTILS_LABEL_EXT;
}
impl Default for DebugUtilsLabelEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_label_name: std::ptr::null(), color: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for DebugUtilsLabelEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugUtilsLabelEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_label_name", &self.p_label_name).field("color", &self.color).finish()
    }
}
impl DebugUtilsLabelEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugUtilsLabelEXTBuilder<'a> {
        DebugUtilsLabelEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsLabelEXT.html)) · Builder of [`DebugUtilsLabelEXT`] <br/> VkDebugUtilsLabelEXT - Specify parameters of a label region\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugUtilsLabelEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsLabelEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    const char*        pLabelName;\n    float              color[4];\n} VkDebugUtilsLabelEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_label_name`] is a pointer to a null-terminated UTF-8 string\n  containing the name of the label.\n\n* [`Self::color`] is an optional RGBA color value that can be associated with\n  the label.\n  A particular implementation **may** choose to ignore this color value.\n  The values contain RGBA values in order, in the range 0.0 to 1.0.\n  If all elements in [`Self::color`] are set to 0.0 then it is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsLabelEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_LABEL_EXT`]\n\n* []() VUID-VkDebugUtilsLabelEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsLabelEXT-pLabelName-parameter  \n  [`Self::p_label_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`], [`crate::vk::InstanceLoader::cmd_insert_debug_utils_label_ext`], [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`], [`crate::vk::InstanceLoader::queue_insert_debug_utils_label_ext`]\n"]
#[repr(transparent)]
pub struct DebugUtilsLabelEXTBuilder<'a>(DebugUtilsLabelEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugUtilsLabelEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugUtilsLabelEXTBuilder<'a> {
        DebugUtilsLabelEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn label_name(mut self, label_name: &'a std::ffi::CStr) -> Self {
        self.0.p_label_name = label_name.as_ptr();
        self
    }
    #[inline]
    pub fn color(mut self, color: [std::os::raw::c_float; 4]) -> Self {
        self.0.color = color as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugUtilsLabelEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugUtilsLabelEXTBuilder<'a> {
    fn default() -> DebugUtilsLabelEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugUtilsLabelEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugUtilsLabelEXTBuilder<'a> {
    type Target = DebugUtilsLabelEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugUtilsLabelEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCreateInfoEXT.html)) · Structure <br/> VkDebugUtilsMessengerCreateInfoEXT - Structure specifying parameters of a newly created debug messenger\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugUtilsMessengerCreateInfoEXT`] is:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsMessengerCreateInfoEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    VkDebugUtilsMessengerCreateFlagsEXT     flags;\n    VkDebugUtilsMessageSeverityFlagsEXT     messageSeverity;\n    VkDebugUtilsMessageTypeFlagsEXT         messageType;\n    PFN_vkDebugUtilsMessengerCallbackEXT    pfnUserCallback;\n    void*                                   pUserData;\n} VkDebugUtilsMessengerCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is `0` and is reserved for future use.\n\n* [`Self::message_severity`] is a bitmask of[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) specifying which severity\n  of event(s) will cause this callback to be called.\n\n* [`Self::message_type`] is a bitmask of[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) specifying which type of\n  event(s) will cause this callback to be called.\n\n* [`Self::pfn_user_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nFor each [`crate::vk::DebugUtilsMessengerEXT`] that is created the[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::message_severity`] and[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::message_type`] determine when\nthat [`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::pfn_user_callback`] is\ncalled.\nThe process to determine if the user’s [`Self::pfn_user_callback`] is triggered\nwhen an event occurs is as follows:\n\n1. The implementation will perform a bitwise AND of the event’s[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) with the[`Self::message_severity`] provided during creation of the[`crate::vk::DebugUtilsMessengerEXT`] object.\n\n   1. If the value is 0, the message is skipped.\n\n2. The implementation will perform bitwise AND of the event’s[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) with the [`Self::message_type`]provided during the creation of the [`crate::vk::DebugUtilsMessengerEXT`]object.\n\n   1. If the value is 0, the message is skipped.\n\n3. The callback will trigger a debug message for the current event\n\nThe callback will come directly from the component that detected the event,\nunless some other layer intercepts the calls for its own purposes (filter\nthem in a different way, log to a system error log, etc.).\n\nAn application **can** receive multiple callbacks if multiple[`crate::vk::DebugUtilsMessengerEXT`] objects are created.\nA callback will always be executed in the same thread as the originating\nVulkan call.\n\nA callback **can** be called from multiple threads simultaneously (if the\napplication is making Vulkan calls from multiple threads).\n\nValid Usage\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-pfnUserCallback-01914  \n  [`Self::pfn_user_callback`] **must** be a valid[PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageSeverity-parameter  \n  [`Self::message_severity`] **must** be a valid combination of [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) values\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageSeverity-requiredbitmask  \n  [`Self::message_severity`] **must** not be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageType-parameter  \n  [`Self::message_type`] **must** be a valid combination of [VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) values\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageType-requiredbitmask  \n  [`Self::message_type`] **must** not be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-pfnUserCallback-parameter  \n  [`Self::pfn_user_callback`] **must** be a valid [PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html) value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html), [`crate::vk::DebugUtilsMessageSeverityFlagBitsEXT`], [`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`], [`crate::vk::DebugUtilsMessengerCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`]\n"]
#[doc(alias = "VkDebugUtilsMessengerCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugUtilsMessengerCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_debug_utils::DebugUtilsMessengerCreateFlagsEXT,
    pub message_severity: crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagsEXT,
    pub message_type: crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagsEXT,
    pub pfn_user_callback: Option<crate::extensions::ext_debug_utils::PFN_vkDebugUtilsMessengerCallbackEXT>,
    pub p_user_data: *mut std::ffi::c_void,
}
impl DebugUtilsMessengerCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
}
impl Default for DebugUtilsMessengerCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), message_severity: Default::default(), message_type: Default::default(), pfn_user_callback: Default::default(), p_user_data: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for DebugUtilsMessengerCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugUtilsMessengerCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("message_severity", &self.message_severity).field("message_type", &self.message_type).field("pfn_user_callback", unsafe { &std::mem::transmute::<_, *const ()>(self.pfn_user_callback) }).field("p_user_data", &self.p_user_data).finish()
    }
}
impl DebugUtilsMessengerCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
        DebugUtilsMessengerCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCreateInfoEXT.html)) · Builder of [`DebugUtilsMessengerCreateInfoEXT`] <br/> VkDebugUtilsMessengerCreateInfoEXT - Structure specifying parameters of a newly created debug messenger\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugUtilsMessengerCreateInfoEXT`] is:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsMessengerCreateInfoEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    VkDebugUtilsMessengerCreateFlagsEXT     flags;\n    VkDebugUtilsMessageSeverityFlagsEXT     messageSeverity;\n    VkDebugUtilsMessageTypeFlagsEXT         messageType;\n    PFN_vkDebugUtilsMessengerCallbackEXT    pfnUserCallback;\n    void*                                   pUserData;\n} VkDebugUtilsMessengerCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is `0` and is reserved for future use.\n\n* [`Self::message_severity`] is a bitmask of[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) specifying which severity\n  of event(s) will cause this callback to be called.\n\n* [`Self::message_type`] is a bitmask of[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) specifying which type of\n  event(s) will cause this callback to be called.\n\n* [`Self::pfn_user_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nFor each [`crate::vk::DebugUtilsMessengerEXT`] that is created the[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::message_severity`] and[`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::message_type`] determine when\nthat [`crate::vk::DebugUtilsMessengerCreateInfoEXT`]::[`Self::pfn_user_callback`] is\ncalled.\nThe process to determine if the user’s [`Self::pfn_user_callback`] is triggered\nwhen an event occurs is as follows:\n\n1. The implementation will perform a bitwise AND of the event’s[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) with the[`Self::message_severity`] provided during creation of the[`crate::vk::DebugUtilsMessengerEXT`] object.\n\n   1. If the value is 0, the message is skipped.\n\n2. The implementation will perform bitwise AND of the event’s[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) with the [`Self::message_type`]provided during the creation of the [`crate::vk::DebugUtilsMessengerEXT`]object.\n\n   1. If the value is 0, the message is skipped.\n\n3. The callback will trigger a debug message for the current event\n\nThe callback will come directly from the component that detected the event,\nunless some other layer intercepts the calls for its own purposes (filter\nthem in a different way, log to a system error log, etc.).\n\nAn application **can** receive multiple callbacks if multiple[`crate::vk::DebugUtilsMessengerEXT`] objects are created.\nA callback will always be executed in the same thread as the originating\nVulkan call.\n\nA callback **can** be called from multiple threads simultaneously (if the\napplication is making Vulkan calls from multiple threads).\n\nValid Usage\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-pfnUserCallback-01914  \n  [`Self::pfn_user_callback`] **must** be a valid[PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT`]\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageSeverity-parameter  \n  [`Self::message_severity`] **must** be a valid combination of [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) values\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageSeverity-requiredbitmask  \n  [`Self::message_severity`] **must** not be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageType-parameter  \n  [`Self::message_type`] **must** be a valid combination of [VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) values\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-messageType-requiredbitmask  \n  [`Self::message_type`] **must** not be `0`\n\n* []() VUID-VkDebugUtilsMessengerCreateInfoEXT-pfnUserCallback-parameter  \n  [`Self::pfn_user_callback`] **must** be a valid [PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html) value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDebugUtilsMessengerCallbackEXT](PFN_vkDebugUtilsMessengerCallbackEXT.html), [`crate::vk::DebugUtilsMessageSeverityFlagBitsEXT`], [`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`], [`crate::vk::DebugUtilsMessengerCreateFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`]\n"]
#[repr(transparent)]
pub struct DebugUtilsMessengerCreateInfoEXTBuilder<'a>(DebugUtilsMessengerCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
        DebugUtilsMessengerCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_debug_utils::DebugUtilsMessengerCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn message_severity(mut self, message_severity: crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagsEXT) -> Self {
        self.0.message_severity = message_severity as _;
        self
    }
    #[inline]
    pub fn message_type(mut self, message_type: crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagsEXT) -> Self {
        self.0.message_type = message_type as _;
        self
    }
    #[inline]
    pub fn pfn_user_callback(mut self, pfn_user_callback: Option<crate::extensions::ext_debug_utils::PFN_vkDebugUtilsMessengerCallbackEXT>) -> Self {
        self.0.pfn_user_callback = pfn_user_callback as _;
        self
    }
    #[inline]
    pub fn user_data(mut self, user_data: *mut std::ffi::c_void) -> Self {
        self.0.p_user_data = user_data;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugUtilsMessengerCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
    fn default() -> DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
    type Target = DebugUtilsMessengerCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugUtilsMessengerCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCallbackDataEXT.html)) · Structure <br/> VkDebugUtilsMessengerCallbackDataEXT - Structure specifying parameters returned to the callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugUtilsMessengerCallbackDataEXT`] is:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsMessengerCallbackDataEXT {\n    VkStructureType                              sType;\n    const void*                                  pNext;\n    VkDebugUtilsMessengerCallbackDataFlagsEXT    flags;\n    const char*                                  pMessageIdName;\n    int32_t                                      messageIdNumber;\n    const char*                                  pMessage;\n    uint32_t                                     queueLabelCount;\n    const VkDebugUtilsLabelEXT*                  pQueueLabels;\n    uint32_t                                     cmdBufLabelCount;\n    const VkDebugUtilsLabelEXT*                  pCmdBufLabels;\n    uint32_t                                     objectCount;\n    const VkDebugUtilsObjectNameInfoEXT*         pObjects;\n} VkDebugUtilsMessengerCallbackDataEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is `0` and is reserved for future use.\n\n* [`Self::p_message_id_name`] is a null-terminated string that identifies the\n  particular message ID that is associated with the provided message.\n  If the message corresponds to a validation layer message, then this\n  string may contain the portion of the Vulkan specification that is\n  believed to have been violated.\n\n* [`Self::message_id_number`] is the ID number of the triggering message.\n  If the message corresponds to a validation layer message, then this\n  number is related to the internal number associated with the message\n  being triggered.\n\n* [`Self::p_message`] is a null-terminated string detailing the trigger\n  conditions.\n\n* [`Self::queue_label_count`] is a count of items contained in the[`Self::p_queue_labels`] array.\n\n* [`Self::p_queue_labels`] is `NULL` or a pointer to an array of[`crate::vk::DebugUtilsLabelEXT`] active in the current [`crate::vk::Queue`] at the\n  time the callback was triggered.\n  Refer to [Queue Labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-queue-labels) for more information.\n\n* [`Self::cmd_buf_label_count`] is a count of items contained in the[`Self::p_cmd_buf_labels`] array.\n\n* [`Self::p_cmd_buf_labels`] is `NULL` or a pointer to an array of[`crate::vk::DebugUtilsLabelEXT`] active in the current [`crate::vk::CommandBuffer`]at the time the callback was triggered.\n  Refer to [Command Buffer Labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-command-buffer-labels) for\n  more information.\n\n* [`Self::object_count`] is a count of items contained in the [`Self::p_objects`]array.\n\n* [`Self::p_objects`] is a pointer to an array of[`crate::vk::DebugUtilsObjectNameInfoEXT`] objects related to the detected\n  issue.\n  The array is roughly in order or importance, but the 0th element is\n  always guaranteed to be the most important object for this message.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>This structure should only be considered valid during the lifetime of the<br/>triggered callback.|\n|---|---------------------------------------------------------------------------------------------------------------|\n\nSince adding queue and command buffer labels behaves like pushing and\npopping onto a stack, the order of both [`Self::p_queue_labels`] and[`Self::p_cmd_buf_labels`] is based on the order the labels were defined.\nThe result is that the first label in either [`Self::p_queue_labels`] or[`Self::p_cmd_buf_labels`] will be the first defined (and therefore the oldest)\nwhile the last label in each list will be the most recent.\n\n|   |Note<br/><br/>[`Self::p_queue_labels`] will only be non-`NULL` if one of the objects in[`Self::p_objects`] can be related directly to a defined [`crate::vk::Queue`] which has<br/>had one or more labels associated with it.<br/><br/>Likewise, [`Self::p_cmd_buf_labels`] will only be non-`NULL` if one of the objects<br/>in [`Self::p_objects`] can be related directly to a defined [`crate::vk::CommandBuffer`]which has had one or more labels associated with it.<br/>Additionally, while command buffer labels allow for beginning and ending<br/>across different command buffers, the debug messaging framework **cannot**guarantee that labels in `pCmdBufLables` will contain those defined<br/>outside of the associated command buffer.<br/>This is partially due to the fact that the association of one command buffer<br/>with another may not have been defined at the time the debug message is<br/>triggered.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT`]\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pMessageIdName-parameter  \n   If [`Self::p_message_id_name`] is not `NULL`, [`Self::p_message_id_name`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pMessage-parameter  \n  [`Self::p_message`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pQueueLabels-parameter  \n   If [`Self::queue_label_count`] is not `0`, [`Self::p_queue_labels`] **must** be a valid pointer to an array of [`Self::queue_label_count`] valid [`crate::vk::DebugUtilsLabelEXT`] structures\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pCmdBufLabels-parameter  \n   If [`Self::cmd_buf_label_count`] is not `0`, [`Self::p_cmd_buf_labels`] **must** be a valid pointer to an array of [`Self::cmd_buf_label_count`] valid [`crate::vk::DebugUtilsLabelEXT`] structures\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pObjects-parameter  \n   If [`Self::object_count`] is not `0`, [`Self::p_objects`] **must** be a valid pointer to an array of [`Self::object_count`] valid [`crate::vk::DebugUtilsObjectNameInfoEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::DebugUtilsMessengerCallbackDataFlagBitsEXT`], [`crate::vk::DebugUtilsObjectNameInfoEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::submit_debug_utils_message_ext`]\n"]
#[doc(alias = "VkDebugUtilsMessengerCallbackDataEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugUtilsMessengerCallbackDataEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_debug_utils::DebugUtilsMessengerCallbackDataFlagsEXT,
    pub p_message_id_name: *const std::os::raw::c_char,
    pub message_id_number: i32,
    pub p_message: *const std::os::raw::c_char,
    pub queue_label_count: u32,
    pub p_queue_labels: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT,
    pub cmd_buf_label_count: u32,
    pub p_cmd_buf_labels: *const crate::extensions::ext_debug_utils::DebugUtilsLabelEXT,
    pub object_count: u32,
    pub p_objects: *const crate::extensions::ext_debug_utils::DebugUtilsObjectNameInfoEXT,
}
impl DebugUtilsMessengerCallbackDataEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT;
}
impl Default for DebugUtilsMessengerCallbackDataEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), p_message_id_name: std::ptr::null(), message_id_number: Default::default(), p_message: std::ptr::null(), queue_label_count: Default::default(), p_queue_labels: std::ptr::null(), cmd_buf_label_count: Default::default(), p_cmd_buf_labels: std::ptr::null(), object_count: Default::default(), p_objects: std::ptr::null() }
    }
}
impl std::fmt::Debug for DebugUtilsMessengerCallbackDataEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugUtilsMessengerCallbackDataEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("p_message_id_name", &self.p_message_id_name).field("message_id_number", &self.message_id_number).field("p_message", &self.p_message).field("queue_label_count", &self.queue_label_count).field("p_queue_labels", &self.p_queue_labels).field("cmd_buf_label_count", &self.cmd_buf_label_count).field("p_cmd_buf_labels", &self.p_cmd_buf_labels).field("object_count", &self.object_count).field("p_objects", &self.p_objects).finish()
    }
}
impl DebugUtilsMessengerCallbackDataEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
        DebugUtilsMessengerCallbackDataEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessengerCallbackDataEXT.html)) · Builder of [`DebugUtilsMessengerCallbackDataEXT`] <br/> VkDebugUtilsMessengerCallbackDataEXT - Structure specifying parameters returned to the callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugUtilsMessengerCallbackDataEXT`] is:\n\n```\n// Provided by VK_EXT_debug_utils\ntypedef struct VkDebugUtilsMessengerCallbackDataEXT {\n    VkStructureType                              sType;\n    const void*                                  pNext;\n    VkDebugUtilsMessengerCallbackDataFlagsEXT    flags;\n    const char*                                  pMessageIdName;\n    int32_t                                      messageIdNumber;\n    const char*                                  pMessage;\n    uint32_t                                     queueLabelCount;\n    const VkDebugUtilsLabelEXT*                  pQueueLabels;\n    uint32_t                                     cmdBufLabelCount;\n    const VkDebugUtilsLabelEXT*                  pCmdBufLabels;\n    uint32_t                                     objectCount;\n    const VkDebugUtilsObjectNameInfoEXT*         pObjects;\n} VkDebugUtilsMessengerCallbackDataEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is `0` and is reserved for future use.\n\n* [`Self::p_message_id_name`] is a null-terminated string that identifies the\n  particular message ID that is associated with the provided message.\n  If the message corresponds to a validation layer message, then this\n  string may contain the portion of the Vulkan specification that is\n  believed to have been violated.\n\n* [`Self::message_id_number`] is the ID number of the triggering message.\n  If the message corresponds to a validation layer message, then this\n  number is related to the internal number associated with the message\n  being triggered.\n\n* [`Self::p_message`] is a null-terminated string detailing the trigger\n  conditions.\n\n* [`Self::queue_label_count`] is a count of items contained in the[`Self::p_queue_labels`] array.\n\n* [`Self::p_queue_labels`] is `NULL` or a pointer to an array of[`crate::vk::DebugUtilsLabelEXT`] active in the current [`crate::vk::Queue`] at the\n  time the callback was triggered.\n  Refer to [Queue Labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-queue-labels) for more information.\n\n* [`Self::cmd_buf_label_count`] is a count of items contained in the[`Self::p_cmd_buf_labels`] array.\n\n* [`Self::p_cmd_buf_labels`] is `NULL` or a pointer to an array of[`crate::vk::DebugUtilsLabelEXT`] active in the current [`crate::vk::CommandBuffer`]at the time the callback was triggered.\n  Refer to [Command Buffer Labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-command-buffer-labels) for\n  more information.\n\n* [`Self::object_count`] is a count of items contained in the [`Self::p_objects`]array.\n\n* [`Self::p_objects`] is a pointer to an array of[`crate::vk::DebugUtilsObjectNameInfoEXT`] objects related to the detected\n  issue.\n  The array is roughly in order or importance, but the 0th element is\n  always guaranteed to be the most important object for this message.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>This structure should only be considered valid during the lifetime of the<br/>triggered callback.|\n|---|---------------------------------------------------------------------------------------------------------------|\n\nSince adding queue and command buffer labels behaves like pushing and\npopping onto a stack, the order of both [`Self::p_queue_labels`] and[`Self::p_cmd_buf_labels`] is based on the order the labels were defined.\nThe result is that the first label in either [`Self::p_queue_labels`] or[`Self::p_cmd_buf_labels`] will be the first defined (and therefore the oldest)\nwhile the last label in each list will be the most recent.\n\n|   |Note<br/><br/>[`Self::p_queue_labels`] will only be non-`NULL` if one of the objects in[`Self::p_objects`] can be related directly to a defined [`crate::vk::Queue`] which has<br/>had one or more labels associated with it.<br/><br/>Likewise, [`Self::p_cmd_buf_labels`] will only be non-`NULL` if one of the objects<br/>in [`Self::p_objects`] can be related directly to a defined [`crate::vk::CommandBuffer`]which has had one or more labels associated with it.<br/>Additionally, while command buffer labels allow for beginning and ending<br/>across different command buffers, the debug messaging framework **cannot**guarantee that labels in `pCmdBufLables` will contain those defined<br/>outside of the associated command buffer.<br/>This is partially due to the fact that the association of one command buffer<br/>with another may not have been defined at the time the debug message is<br/>triggered.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT`]\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pMessageIdName-parameter  \n   If [`Self::p_message_id_name`] is not `NULL`, [`Self::p_message_id_name`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pMessage-parameter  \n  [`Self::p_message`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pQueueLabels-parameter  \n   If [`Self::queue_label_count`] is not `0`, [`Self::p_queue_labels`] **must** be a valid pointer to an array of [`Self::queue_label_count`] valid [`crate::vk::DebugUtilsLabelEXT`] structures\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pCmdBufLabels-parameter  \n   If [`Self::cmd_buf_label_count`] is not `0`, [`Self::p_cmd_buf_labels`] **must** be a valid pointer to an array of [`Self::cmd_buf_label_count`] valid [`crate::vk::DebugUtilsLabelEXT`] structures\n\n* []() VUID-VkDebugUtilsMessengerCallbackDataEXT-pObjects-parameter  \n   If [`Self::object_count`] is not `0`, [`Self::p_objects`] **must** be a valid pointer to an array of [`Self::object_count`] valid [`crate::vk::DebugUtilsObjectNameInfoEXT`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::DebugUtilsMessengerCallbackDataFlagBitsEXT`], [`crate::vk::DebugUtilsObjectNameInfoEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::submit_debug_utils_message_ext`]\n"]
#[repr(transparent)]
pub struct DebugUtilsMessengerCallbackDataEXTBuilder<'a>(DebugUtilsMessengerCallbackDataEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
        DebugUtilsMessengerCallbackDataEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_debug_utils::DebugUtilsMessengerCallbackDataFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn message_id_name(mut self, message_id_name: &'a std::ffi::CStr) -> Self {
        self.0.p_message_id_name = message_id_name.as_ptr();
        self
    }
    #[inline]
    pub fn message_id_number(mut self, message_id_number: i32) -> Self {
        self.0.message_id_number = message_id_number as _;
        self
    }
    #[inline]
    pub fn message(mut self, message: &'a std::ffi::CStr) -> Self {
        self.0.p_message = message.as_ptr();
        self
    }
    #[inline]
    pub fn queue_labels(mut self, queue_labels: &'a [crate::extensions::ext_debug_utils::DebugUtilsLabelEXTBuilder]) -> Self {
        self.0.p_queue_labels = queue_labels.as_ptr() as _;
        self.0.queue_label_count = queue_labels.len() as _;
        self
    }
    #[inline]
    pub fn cmd_buf_labels(mut self, cmd_buf_labels: &'a [crate::extensions::ext_debug_utils::DebugUtilsLabelEXTBuilder]) -> Self {
        self.0.p_cmd_buf_labels = cmd_buf_labels.as_ptr() as _;
        self.0.cmd_buf_label_count = cmd_buf_labels.len() as _;
        self
    }
    #[inline]
    pub fn objects(mut self, objects: &'a [crate::extensions::ext_debug_utils::DebugUtilsObjectNameInfoEXTBuilder]) -> Self {
        self.0.p_objects = objects.as_ptr() as _;
        self.0.object_count = objects.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugUtilsMessengerCallbackDataEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
    fn default() -> DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
    type Target = DebugUtilsMessengerCallbackDataEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugUtilsMessengerCallbackDataEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetDebugUtilsObjectNameEXT.html)) · Function <br/> vkSetDebugUtilsObjectNameEXT - Give a user-friendly name to an object\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkSetDebugUtilsObjectNameEXT(\n    VkDevice                                    device,\n    const VkDebugUtilsObjectNameInfoEXT*        pNameInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_name_info`] is a pointer to a [`crate::vk::DebugUtilsObjectNameInfoEXT`]structure specifying parameters of the name to set on the object.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-02587  \n  `pNameInfo->objectType` **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-02588  \n  `pNameInfo->objectHandle` **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetDebugUtilsObjectNameEXT-pNameInfo-parameter  \n  [`Self::p_name_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsObjectNameInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pNameInfo->objectHandle` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsObjectNameInfoEXT`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkSetDebugUtilsObjectNameEXT")]
    pub unsafe fn set_debug_utils_object_name_ext(&self, name_info: &crate::extensions::ext_debug_utils::DebugUtilsObjectNameInfoEXT) -> crate::utils::VulkanResult<()> {
        let _function = self.set_debug_utils_object_name_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, name_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSetDebugUtilsObjectTagEXT.html)) · Function <br/> vkSetDebugUtilsObjectTagEXT - Attach arbitrary data to an object\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkSetDebugUtilsObjectTagEXT(\n    VkDevice                                    device,\n    const VkDebugUtilsObjectTagInfoEXT*         pTagInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_tag_info`] is a pointer to a [`crate::vk::DebugUtilsObjectTagInfoEXT`]structure specifying parameters of the tag to attach to the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkSetDebugUtilsObjectTagEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkSetDebugUtilsObjectTagEXT-pTagInfo-parameter  \n  [`Self::p_tag_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsObjectTagInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pTagInfo->objectHandle` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsObjectTagInfoEXT`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkSetDebugUtilsObjectTagEXT")]
    pub unsafe fn set_debug_utils_object_tag_ext(&self, tag_info: &crate::extensions::ext_debug_utils::DebugUtilsObjectTagInfoEXT) -> crate::utils::VulkanResult<()> {
        let _function = self.set_debug_utils_object_tag_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, tag_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueBeginDebugUtilsLabelEXT.html)) · Function <br/> vkQueueBeginDebugUtilsLabelEXT - Open a queue debug label region\n[](#_c_specification)C Specification\n----------\n\nA queue debug label region is opened by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueBeginDebugUtilsLabelEXT(\n    VkQueue                                     queue,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue in which to start a debug label region.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueBeginDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueBeginDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::Queue`]\n"]
    #[doc(alias = "vkQueueBeginDebugUtilsLabelEXT")]
    pub unsafe fn queue_begin_debug_utils_label_ext(&self, queue: crate::vk1_0::Queue, label_info: &crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> () {
        let _function = self.queue_begin_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(queue as _, label_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueEndDebugUtilsLabelEXT.html)) · Function <br/> vkQueueEndDebugUtilsLabelEXT - Close a queue debug label region\n[](#_c_specification)C Specification\n----------\n\nA queue debug label region is closed by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueEndDebugUtilsLabelEXT(\n    VkQueue                                     queue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue in which a debug label region should be closed.\n[](#_description)Description\n----------\n\nThe calls to [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`] and[`crate::vk::InstanceLoader::queue_end_debug_utils_label_ext`] **must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkQueueEndDebugUtilsLabelEXT-None-01911  \n   There **must** be an outstanding [`crate::vk::InstanceLoader::queue_begin_debug_utils_label_ext`]command prior to the [`crate::vk::InstanceLoader::queue_end_debug_utils_label_ext`] on the queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueEndDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Queue`]\n"]
    #[doc(alias = "vkQueueEndDebugUtilsLabelEXT")]
    pub unsafe fn queue_end_debug_utils_label_ext(&self, queue: crate::vk1_0::Queue) -> () {
        let _function = self.queue_end_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(queue as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueInsertDebugUtilsLabelEXT.html)) · Function <br/> vkQueueInsertDebugUtilsLabelEXT - Insert a label into a queue\n[](#_c_specification)C Specification\n----------\n\nA single label can be inserted into a queue by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkQueueInsertDebugUtilsLabelEXT(\n    VkQueue                                     queue,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue into which a debug label will be inserted.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueInsertDebugUtilsLabelEXT-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueInsertDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugUtilsLabelEXT`], [`crate::vk::Queue`]\n"]
    #[doc(alias = "vkQueueInsertDebugUtilsLabelEXT")]
    pub unsafe fn queue_insert_debug_utils_label_ext(&self, queue: crate::vk1_0::Queue, label_info: &crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> () {
        let _function = self.queue_insert_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(queue as _, label_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginDebugUtilsLabelEXT.html)) · Function <br/> vkCmdBeginDebugUtilsLabelEXT - Open a command buffer debug label region\n[](#_c_specification)C Specification\n----------\n\nA command buffer debug label region can be opened by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdBeginDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_label_info`] is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugUtilsLabelEXT`]\n"]
    #[doc(alias = "vkCmdBeginDebugUtilsLabelEXT")]
    pub unsafe fn cmd_begin_debug_utils_label_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, label_info: &crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> () {
        let _function = self.cmd_begin_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, label_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndDebugUtilsLabelEXT.html)) · Function <br/> vkCmdEndDebugUtilsLabelEXT - Close a command buffer label region\n[](#_c_specification)C Specification\n----------\n\nA command buffer label region can be closed by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdEndDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n[](#_description)Description\n----------\n\nAn application **may** open a debug label region in one command buffer and\nclose it in another, or otherwise split debug label regions across multiple\ncommand buffers or multiple queue submissions.\nWhen viewed from the linear series of submissions to a single queue, the\ncalls to [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] and[`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`] **must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-01912  \n   There **must** be an outstanding [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] command\n  prior to the [`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`] on the queue that[`Self::command_buffer`] is submitted to\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-01913  \n   If [`Self::command_buffer`] is a secondary command buffer, there **must** be an\n  outstanding [`crate::vk::InstanceLoader::cmd_begin_debug_utils_label_ext`] command recorded to[`Self::command_buffer`] that has not previously been ended by a call to[`crate::vk::InstanceLoader::cmd_end_debug_utils_label_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdEndDebugUtilsLabelEXT")]
    pub unsafe fn cmd_end_debug_utils_label_ext(&self, command_buffer: crate::vk1_0::CommandBuffer) -> () {
        let _function = self.cmd_end_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdInsertDebugUtilsLabelEXT.html)) · Function <br/> vkCmdInsertDebugUtilsLabelEXT - Insert a label into a command buffer\n[](#_c_specification)C Specification\n----------\n\nA single debug label can be inserted into a command buffer by calling:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkCmdInsertDebugUtilsLabelEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugUtilsLabelEXT*                 pLabelInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* `pInfo` is a pointer to a [`crate::vk::DebugUtilsLabelEXT`] structure\n  specifying parameters of the label to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-pLabelInfo-parameter  \n  [`Self::p_label_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsLabelEXT`] structure\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdInsertDebugUtilsLabelEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugUtilsLabelEXT`]\n"]
    #[doc(alias = "vkCmdInsertDebugUtilsLabelEXT")]
    pub unsafe fn cmd_insert_debug_utils_label_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, label_info: &crate::extensions::ext_debug_utils::DebugUtilsLabelEXT) -> () {
        let _function = self.cmd_insert_debug_utils_label_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, label_info as _);
        ()
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_utils`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDebugUtilsMessengerEXT.html)) · Function <br/> vkCreateDebugUtilsMessengerEXT - Create a debug messenger object\n[](#_c_specification)C Specification\n----------\n\nA debug messenger triggers a debug callback with a debug message when an\nevent of interest occurs.\nTo create a debug messenger which will trigger a debug callback, call:\n\n```\n// Provided by VK_EXT_debug_utils\nVkResult vkCreateDebugUtilsMessengerEXT(\n    VkInstance                                  instance,\n    const VkDebugUtilsMessengerCreateInfoEXT*   pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDebugUtilsMessengerEXT*                   pMessenger);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance the messenger will be used with.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::DebugUtilsMessengerCreateInfoEXT`] structure containing the\n  callback pointer, as well as defining conditions under which this\n  messenger will trigger the callback.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_messenger`] is a pointer to a [`crate::vk::DebugUtilsMessengerEXT`] handle\n  in which the created object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsMessengerCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDebugUtilsMessengerEXT-pMessenger-parameter  \n  [`Self::p_messenger`] **must** be a valid pointer to a [`crate::vk::DebugUtilsMessengerEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\nThe application **must** ensure that [`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`] is\nnot executed in parallel with any Vulkan command that is also called with[`Self::instance`] or child of [`Self::instance`] as the dispatchable argument.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugUtilsMessengerCreateInfoEXT`], [`crate::vk::DebugUtilsMessengerEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkCreateDebugUtilsMessengerEXT")]
    pub unsafe fn create_debug_utils_messenger_ext(&self, create_info: &crate::extensions::ext_debug_utils::DebugUtilsMessengerCreateInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::ext_debug_utils::DebugUtilsMessengerEXT> {
        let _function = self.create_debug_utils_messenger_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut messenger = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut messenger,
        );
        crate::utils::VulkanResult::new(_return, messenger)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyDebugUtilsMessengerEXT.html)) · Function <br/> vkDestroyDebugUtilsMessengerEXT - Destroy a debug messenger object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a [`crate::vk::DebugUtilsMessengerEXT`] object, call:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkDestroyDebugUtilsMessengerEXT(\n    VkInstance                                  instance,\n    VkDebugUtilsMessengerEXT                    messenger,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance where the callback was created.\n\n* [`Self::messenger`] is the [`crate::vk::DebugUtilsMessengerEXT`] object to destroy.[`Self::messenger`] is an externally synchronized object and **must** not be\n  used on more than one thread at a time.\n  This means that [`crate::vk::InstanceLoader::destroy_debug_utils_messenger_ext`] **must** not be\n  called when a callback is active.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-01915  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::messenger`] was\n  created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-01916  \n   If no [`crate::vk::AllocationCallbacks`] were provided when [`Self::messenger`] was\n  created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-parameter  \n   If [`Self::messenger`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::messenger`] **must** be a valid [`crate::vk::DebugUtilsMessengerEXT`] handle\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyDebugUtilsMessengerEXT-messenger-parent  \n   If [`Self::messenger`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::instance`]\n\nHost Synchronization\n\n* Host access to [`Self::messenger`] **must** be externally synchronized\n\nThe application **must** ensure that [`crate::vk::InstanceLoader::destroy_debug_utils_messenger_ext`] is\nnot executed in parallel with any Vulkan command that is also called with[`Self::instance`] or child of [`Self::instance`] as the dispatchable argument.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugUtilsMessengerEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkDestroyDebugUtilsMessengerEXT")]
    pub unsafe fn destroy_debug_utils_messenger_ext(&self, messenger: Option<crate::extensions::ext_debug_utils::DebugUtilsMessengerEXT>, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_debug_utils_messenger_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            match messenger {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkSubmitDebugUtilsMessageEXT.html)) · Function <br/> vkSubmitDebugUtilsMessageEXT - Inject a message into a debug stream\n[](#_c_specification)C Specification\n----------\n\nThere may be times that a user wishes to intentionally submit a debug\nmessage.\nTo do this, call:\n\n```\n// Provided by VK_EXT_debug_utils\nvoid vkSubmitDebugUtilsMessageEXT(\n    VkInstance                                  instance,\n    VkDebugUtilsMessageSeverityFlagBitsEXT      messageSeverity,\n    VkDebugUtilsMessageTypeFlagsEXT             messageTypes,\n    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the debug stream’s [`crate::vk::Instance`].\n\n* [`Self::message_severity`] is a [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html)value specifying the severity of this event/message.\n\n* [`Self::message_types`] is a bitmask of[VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) specifying which type of\n  event(s) to identify with this message.\n\n* [`Self::p_callback_data`] contains all the callback related data in the[`crate::vk::DebugUtilsMessengerCallbackDataEXT`] structure.\n[](#_description)Description\n----------\n\nThe call will propagate through the layers and generate callback(s) as\nindicated by the message’s flags.\nThe parameters are passed on to the callback in addition to the`pUserData` value that was defined at the time the messenger was\nregistered.\n\nValid Usage\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-objectType-02591  \n   The `objectType` member of each element of`pCallbackData->pObjects` **must** not be [`crate::vk::ObjectType::UNKNOWN`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageSeverity-parameter  \n  [`Self::message_severity`] **must** be a valid [VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html) value\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageTypes-parameter  \n  [`Self::message_types`] **must** be a valid combination of [VkDebugUtilsMessageTypeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html) values\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-messageTypes-requiredbitmask  \n  [`Self::message_types`] **must** not be `0`\n\n* []() VUID-vkSubmitDebugUtilsMessageEXT-pCallbackData-parameter  \n  [`Self::p_callback_data`] **must** be a valid pointer to a valid [`crate::vk::DebugUtilsMessengerCallbackDataEXT`] structure\n[](#_see_also)See Also\n----------\n\n[VkDebugUtilsMessageSeverityFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html), [`crate::vk::DebugUtilsMessageTypeFlagBitsEXT`], [`crate::vk::DebugUtilsMessengerCallbackDataEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkSubmitDebugUtilsMessageEXT")]
    pub unsafe fn submit_debug_utils_message_ext(&self, message_severity: crate::extensions::ext_debug_utils::DebugUtilsMessageSeverityFlagBitsEXT, message_types: crate::extensions::ext_debug_utils::DebugUtilsMessageTypeFlagsEXT, callback_data: &crate::extensions::ext_debug_utils::DebugUtilsMessengerCallbackDataEXT) -> () {
        let _function = self.submit_debug_utils_message_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, message_severity as _, message_types as _, callback_data as _);
        ()
    }
}
