#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_FENCE_FD_SPEC_VERSION")]
pub const KHR_EXTERNAL_FENCE_FD_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_FENCE_FD_EXTENSION_NAME")]
pub const KHR_EXTERNAL_FENCE_FD_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_external_fence_fd");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_FENCE_FD_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetFenceFdKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_IMPORT_FENCE_FD_KHR: *const std::os::raw::c_char = crate::cstr!("vkImportFenceFdKHR");
#[doc = "Provided by [`crate::extensions::khr_external_fence_fd`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_FENCE_FD_INFO_KHR: Self = Self(1000115000);
    pub const FENCE_GET_FD_INFO_KHR: Self = Self(1000115001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetFenceFdKHR.html)) · Function <br/> vkGetFenceFdKHR - Get a POSIX file descriptor handle for a fence\n[](#_c_specification)C Specification\n----------\n\nTo export a POSIX file descriptor representing the payload of a fence, call:\n\n```\n// Provided by VK_KHR_external_fence_fd\nVkResult vkGetFenceFdKHR(\n    VkDevice                                    device,\n    const VkFenceGetFdInfoKHR*                  pGetFdInfo,\n    int*                                        pFd);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence being\n  exported.\n\n* [`Self::p_get_fd_info`] is a pointer to a [`crate::vk::FenceGetFdInfoKHR`] structure\n  containing parameters of the export operation.\n\n* [`Self::p_fd`] will return the file descriptor representing the fence\n  payload.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_fence_fd_khr`] **must** create a new file descriptor and\ntransfer ownership of it to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nfile descriptor when it is no longer needed.\n\n|   |Note<br/><br/>Ownership can be released in many ways.<br/>For example, the application can call `close`() on the file descriptor,<br/>or transfer ownership back to Vulkan by using the file descriptor to import<br/>a fence payload.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nIf `pGetFdInfo->handleType` is[`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`] and the fence is signaled at\nthe time [`crate::vk::DeviceLoader::get_fence_fd_khr`] is called, [`Self::p_fd`] **may** return the value`-1` instead of a valid file descriptor.\n\nWhere supported by the operating system, the implementation **must** set the\nfile descriptor to be closed automatically when an `execve` system call\nis made.\n\nExporting a file descriptor from a fence **may** have side effects depending on\nthe transference of the specified handle type, as described in[Importing Fence State](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetFenceFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetFenceFdKHR-pGetFdInfo-parameter  \n  [`Self::p_get_fd_info`] **must** be a valid pointer to a valid [`crate::vk::FenceGetFdInfoKHR`] structure\n\n* []() VUID-vkGetFenceFdKHR-pFd-parameter  \n  [`Self::p_fd`] **must** be a valid pointer to an `int` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::FenceGetFdInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetFenceFdKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_fd_info: *const crate::extensions::khr_external_fence_fd::FenceGetFdInfoKHR, p_fd: *mut std::os::raw::c_int) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportFenceFdKHR.html)) · Function <br/> vkImportFenceFdKHR - Import a fence from a POSIX file descriptor\n[](#_c_specification)C Specification\n----------\n\nTo import a fence payload from a POSIX file descriptor, call:\n\n```\n// Provided by VK_KHR_external_fence_fd\nVkResult vkImportFenceFdKHR(\n    VkDevice                                    device,\n    const VkImportFenceFdInfoKHR*               pImportFenceFdInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence.\n\n* [`Self::p_import_fence_fd_info`] is a pointer to a [`crate::vk::ImportFenceFdInfoKHR`]structure specifying the fence and import parameters.\n[](#_description)Description\n----------\n\nImporting a fence payload from a file descriptor transfers ownership of the\nfile descriptor from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the file descriptor\nafter a successful import.\n\nApplications **can** import the same fence payload into multiple instances of\nVulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportFenceFdKHR-fence-01463  \n  `fence` **must** not be associated with any queue command that has not\n  yet completed execution on that queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportFenceFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportFenceFdKHR-pImportFenceFdInfo-parameter  \n  [`Self::p_import_fence_fd_info`] **must** be a valid pointer to a valid [`crate::vk::ImportFenceFdInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportFenceFdInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkImportFenceFdKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_import_fence_fd_info: *const crate::extensions::khr_external_fence_fd::ImportFenceFdInfoKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportFenceFdInfoKHR.html)) · Structure <br/> VkImportFenceFdInfoKHR - (None)\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportFenceFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_fd\ntypedef struct VkImportFenceFdInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkFenceImportFlags                   flags;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n    int                                  fd;\n} VkImportFenceFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence into which the payload will be imported.\n\n* [`Self::flags`] is a bitmask of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) specifying\n  additional parameters for the fence payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of [`Self::fd`].\n\n* [`Self::fd`] is the external handle to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                 Handle Type                 |Transference|Permanence Supported|\n|---------------------------------------------|------------|--------------------|\n|[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_FD`]| Reference  |Temporary,Permanent |\n| [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`] |    Copy    |     Temporary      |\n\nValid Usage\n\n* []() VUID-VkImportFenceFdInfoKHR-handleType-01464  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportFenceFdInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fence-handletypes-fd) table\n\n* []() VUID-VkImportFenceFdInfoKHR-fd-01541  \n  [`Self::fd`] **must** obey any requirements listed for [`Self::handle_type`] in[external fence handle types\n  compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\nIf [`Self::handle_type`] is [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`], the\nspecial value `-1` for [`Self::fd`] is treated like a valid sync file descriptor\nreferring to an object that has already signaled.\nThe import operation will succeed and the [`crate::vk::Fence`] will have a\ntemporarily imported payload as if a valid file descriptor had been\nprovided.\n\n|   |Note<br/><br/>This special behavior for importing an invalid sync file descriptor allows<br/>easier interoperability with other system APIs which use the convention that<br/>an invalid sync file descriptor represents work that has already completed<br/>and does not need to be waited for.<br/>It is consistent with the option for implementations to return a `-1` file<br/>descriptor when exporting a [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`]from a [`crate::vk::Fence`] which is signaled.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportFenceFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_FENCE_FD_INFO_KHR`]\n\n* []() VUID-VkImportFenceFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportFenceFdInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkImportFenceFdInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) values\n\n* []() VUID-VkImportFenceFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n\nHost Synchronization\n\n* Host access to [`Self::fence`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::FenceImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_fence_fd_khr`]\n"]
#[doc(alias = "VkImportFenceFdInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportFenceFdInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub fence: crate::vk1_0::Fence,
    pub flags: crate::vk1_1::FenceImportFlags,
    pub handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits,
    pub fd: std::os::raw::c_int,
}
impl ImportFenceFdInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_FENCE_FD_INFO_KHR;
}
impl Default for ImportFenceFdInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), fence: Default::default(), flags: Default::default(), handle_type: Default::default(), fd: Default::default() }
    }
}
impl std::fmt::Debug for ImportFenceFdInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportFenceFdInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fence", &self.fence).field("flags", &self.flags).field("handle_type", &self.handle_type).field("fd", &self.fd).finish()
    }
}
impl ImportFenceFdInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportFenceFdInfoKHRBuilder<'a> {
        ImportFenceFdInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportFenceFdInfoKHR.html)) · Builder of [`ImportFenceFdInfoKHR`] <br/> VkImportFenceFdInfoKHR - (None)\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportFenceFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_fd\ntypedef struct VkImportFenceFdInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkFenceImportFlags                   flags;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n    int                                  fd;\n} VkImportFenceFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence into which the payload will be imported.\n\n* [`Self::flags`] is a bitmask of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) specifying\n  additional parameters for the fence payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of [`Self::fd`].\n\n* [`Self::fd`] is the external handle to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                 Handle Type                 |Transference|Permanence Supported|\n|---------------------------------------------|------------|--------------------|\n|[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_FD`]| Reference  |Temporary,Permanent |\n| [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`] |    Copy    |     Temporary      |\n\nValid Usage\n\n* []() VUID-VkImportFenceFdInfoKHR-handleType-01464  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportFenceFdInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fence-handletypes-fd) table\n\n* []() VUID-VkImportFenceFdInfoKHR-fd-01541  \n  [`Self::fd`] **must** obey any requirements listed for [`Self::handle_type`] in[external fence handle types\n  compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\nIf [`Self::handle_type`] is [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`], the\nspecial value `-1` for [`Self::fd`] is treated like a valid sync file descriptor\nreferring to an object that has already signaled.\nThe import operation will succeed and the [`crate::vk::Fence`] will have a\ntemporarily imported payload as if a valid file descriptor had been\nprovided.\n\n|   |Note<br/><br/>This special behavior for importing an invalid sync file descriptor allows<br/>easier interoperability with other system APIs which use the convention that<br/>an invalid sync file descriptor represents work that has already completed<br/>and does not need to be waited for.<br/>It is consistent with the option for implementations to return a `-1` file<br/>descriptor when exporting a [`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`]from a [`crate::vk::Fence`] which is signaled.|\n|---|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportFenceFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_FENCE_FD_INFO_KHR`]\n\n* []() VUID-VkImportFenceFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportFenceFdInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkImportFenceFdInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) values\n\n* []() VUID-VkImportFenceFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n\nHost Synchronization\n\n* Host access to [`Self::fence`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::FenceImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_fence_fd_khr`]\n"]
#[repr(transparent)]
pub struct ImportFenceFdInfoKHRBuilder<'a>(ImportFenceFdInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ImportFenceFdInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ImportFenceFdInfoKHRBuilder<'a> {
        ImportFenceFdInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fence(mut self, fence: crate::vk1_0::Fence) -> Self {
        self.0.fence = fence as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::vk1_1::FenceImportFlags) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn fd(mut self, fd: std::os::raw::c_int) -> Self {
        self.0.fd = fd as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportFenceFdInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ImportFenceFdInfoKHRBuilder<'a> {
    fn default() -> ImportFenceFdInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportFenceFdInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportFenceFdInfoKHRBuilder<'a> {
    type Target = ImportFenceFdInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportFenceFdInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceGetFdInfoKHR.html)) · Structure <br/> VkFenceGetFdInfoKHR - Structure describing a POSIX FD fence export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FenceGetFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_fd\ntypedef struct VkFenceGetFdInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n} VkFenceGetFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the file descriptor returned depend on the value of[`Self::handle_type`].\nSee [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) for a description of the\nproperties of the defined external fence handle types.\n\nValid Usage\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01453  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportFenceCreateInfo::handle_types`] when [`Self::fence`]’s\n  current payload was created\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01454  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::fence`] **must** be signaled, or have an\n  associated [fence signal operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-signaling)pending execution\n\n* []() VUID-VkFenceGetFdInfoKHR-fence-01455  \n  [`Self::fence`] **must** not currently have its payload replaced by an imported\n  payload as described below in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing) unless\n  that imported payload’s handle type was included in[`crate::vk::ExternalFenceProperties::export_from_imported_handle_types`] for[`Self::handle_type`]\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01456  \n  [`Self::handle_type`] **must** be defined as a POSIX file descriptor handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkFenceGetFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FENCE_GET_FD_INFO_KHR`]\n\n* []() VUID-VkFenceGetFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkFenceGetFdInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_fence_fd_khr`]\n"]
#[doc(alias = "VkFenceGetFdInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct FenceGetFdInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub fence: crate::vk1_0::Fence,
    pub handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits,
}
impl FenceGetFdInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::FENCE_GET_FD_INFO_KHR;
}
impl Default for FenceGetFdInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), fence: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for FenceGetFdInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("FenceGetFdInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fence", &self.fence).field("handle_type", &self.handle_type).finish()
    }
}
impl FenceGetFdInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> FenceGetFdInfoKHRBuilder<'a> {
        FenceGetFdInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceGetFdInfoKHR.html)) · Builder of [`FenceGetFdInfoKHR`] <br/> VkFenceGetFdInfoKHR - Structure describing a POSIX FD fence export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FenceGetFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_fd\ntypedef struct VkFenceGetFdInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n} VkFenceGetFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the file descriptor returned depend on the value of[`Self::handle_type`].\nSee [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) for a description of the\nproperties of the defined external fence handle types.\n\nValid Usage\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01453  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportFenceCreateInfo::handle_types`] when [`Self::fence`]’s\n  current payload was created\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01454  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::fence`] **must** be signaled, or have an\n  associated [fence signal operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-signaling)pending execution\n\n* []() VUID-VkFenceGetFdInfoKHR-fence-01455  \n  [`Self::fence`] **must** not currently have its payload replaced by an imported\n  payload as described below in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing) unless\n  that imported payload’s handle type was included in[`crate::vk::ExternalFenceProperties::export_from_imported_handle_types`] for[`Self::handle_type`]\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-01456  \n  [`Self::handle_type`] **must** be defined as a POSIX file descriptor handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkFenceGetFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FENCE_GET_FD_INFO_KHR`]\n\n* []() VUID-VkFenceGetFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkFenceGetFdInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkFenceGetFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_fence_fd_khr`]\n"]
#[repr(transparent)]
pub struct FenceGetFdInfoKHRBuilder<'a>(FenceGetFdInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> FenceGetFdInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> FenceGetFdInfoKHRBuilder<'a> {
        FenceGetFdInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fence(mut self, fence: crate::vk1_0::Fence) -> Self {
        self.0.fence = fence as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> FenceGetFdInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for FenceGetFdInfoKHRBuilder<'a> {
    fn default() -> FenceGetFdInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for FenceGetFdInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for FenceGetFdInfoKHRBuilder<'a> {
    type Target = FenceGetFdInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for FenceGetFdInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_external_fence_fd`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetFenceFdKHR.html)) · Function <br/> vkGetFenceFdKHR - Get a POSIX file descriptor handle for a fence\n[](#_c_specification)C Specification\n----------\n\nTo export a POSIX file descriptor representing the payload of a fence, call:\n\n```\n// Provided by VK_KHR_external_fence_fd\nVkResult vkGetFenceFdKHR(\n    VkDevice                                    device,\n    const VkFenceGetFdInfoKHR*                  pGetFdInfo,\n    int*                                        pFd);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence being\n  exported.\n\n* [`Self::p_get_fd_info`] is a pointer to a [`crate::vk::FenceGetFdInfoKHR`] structure\n  containing parameters of the export operation.\n\n* [`Self::p_fd`] will return the file descriptor representing the fence\n  payload.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_fence_fd_khr`] **must** create a new file descriptor and\ntransfer ownership of it to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nfile descriptor when it is no longer needed.\n\n|   |Note<br/><br/>Ownership can be released in many ways.<br/>For example, the application can call `close`() on the file descriptor,<br/>or transfer ownership back to Vulkan by using the file descriptor to import<br/>a fence payload.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nIf `pGetFdInfo->handleType` is[`crate::vk::ExternalFenceHandleTypeFlagBits::SYNC_FD`] and the fence is signaled at\nthe time [`crate::vk::DeviceLoader::get_fence_fd_khr`] is called, [`Self::p_fd`] **may** return the value`-1` instead of a valid file descriptor.\n\nWhere supported by the operating system, the implementation **must** set the\nfile descriptor to be closed automatically when an `execve` system call\nis made.\n\nExporting a file descriptor from a fence **may** have side effects depending on\nthe transference of the specified handle type, as described in[Importing Fence State](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetFenceFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetFenceFdKHR-pGetFdInfo-parameter  \n  [`Self::p_get_fd_info`] **must** be a valid pointer to a valid [`crate::vk::FenceGetFdInfoKHR`] structure\n\n* []() VUID-vkGetFenceFdKHR-pFd-parameter  \n  [`Self::p_fd`] **must** be a valid pointer to an `int` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::FenceGetFdInfoKHR`]\n"]
    #[doc(alias = "vkGetFenceFdKHR")]
    pub unsafe fn get_fence_fd_khr(&self, get_fd_info: &crate::extensions::khr_external_fence_fd::FenceGetFdInfoKHR) -> crate::utils::VulkanResult<std::os::raw::c_int> {
        let _function = self.get_fence_fd_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut fd = Default::default();
        let _return = _function(self.handle, get_fd_info as _, &mut fd);
        crate::utils::VulkanResult::new(_return, fd)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportFenceFdKHR.html)) · Function <br/> vkImportFenceFdKHR - Import a fence from a POSIX file descriptor\n[](#_c_specification)C Specification\n----------\n\nTo import a fence payload from a POSIX file descriptor, call:\n\n```\n// Provided by VK_KHR_external_fence_fd\nVkResult vkImportFenceFdKHR(\n    VkDevice                                    device,\n    const VkImportFenceFdInfoKHR*               pImportFenceFdInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence.\n\n* [`Self::p_import_fence_fd_info`] is a pointer to a [`crate::vk::ImportFenceFdInfoKHR`]structure specifying the fence and import parameters.\n[](#_description)Description\n----------\n\nImporting a fence payload from a file descriptor transfers ownership of the\nfile descriptor from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the file descriptor\nafter a successful import.\n\nApplications **can** import the same fence payload into multiple instances of\nVulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportFenceFdKHR-fence-01463  \n  `fence` **must** not be associated with any queue command that has not\n  yet completed execution on that queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportFenceFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportFenceFdKHR-pImportFenceFdInfo-parameter  \n  [`Self::p_import_fence_fd_info`] **must** be a valid pointer to a valid [`crate::vk::ImportFenceFdInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportFenceFdInfoKHR`]\n"]
    #[doc(alias = "vkImportFenceFdKHR")]
    pub unsafe fn import_fence_fd_khr(&self, import_fence_fd_info: &crate::extensions::khr_external_fence_fd::ImportFenceFdInfoKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.import_fence_fd_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, import_fence_fd_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
