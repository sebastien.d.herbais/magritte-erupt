#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_VALVE_MUTABLE_DESCRIPTOR_TYPE_SPEC_VERSION")]
pub const VALVE_MUTABLE_DESCRIPTOR_TYPE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_VALVE_MUTABLE_DESCRIPTOR_TYPE_EXTENSION_NAME")]
pub const VALVE_MUTABLE_DESCRIPTOR_TYPE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_VALVE_mutable_descriptor_type");
#[doc = "Provided by [`crate::extensions::valve_mutable_descriptor_type`]"]
impl crate::vk1_0::DescriptorSetLayoutCreateFlagBits {
    pub const HOST_ONLY_POOL_VALVE: Self = Self(4);
}
#[doc = "Provided by [`crate::extensions::valve_mutable_descriptor_type`]"]
impl crate::vk1_0::DescriptorType {
    pub const MUTABLE_VALVE: Self = Self(1000351000);
}
#[doc = "Provided by [`crate::extensions::valve_mutable_descriptor_type`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE: Self = Self(1000351000);
    pub const MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE: Self = Self(1000351002);
}
#[doc = "Provided by [`crate::extensions::valve_mutable_descriptor_type`]"]
impl crate::vk1_0::DescriptorPoolCreateFlagBits {
    pub const HOST_ONLY_VALVE: Self = Self(4);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceMutableDescriptorTypeFeaturesVALVE> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MutableDescriptorTypeCreateInfoVALVE> for crate::vk1_0::DescriptorSetLayoutCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MutableDescriptorTypeCreateInfoVALVEBuilder<'_>> for crate::vk1_0::DescriptorSetLayoutCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MutableDescriptorTypeCreateInfoVALVE> for crate::vk1_0::DescriptorPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, MutableDescriptorTypeCreateInfoVALVEBuilder<'_>> for crate::vk1_0::DescriptorPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMutableDescriptorTypeFeaturesVALVE> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE.html)) · Structure <br/> VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE - Structure describing whether the mutable descriptor type is supported\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] structure is\ndefined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           mutableDescriptorType;\n} VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::mutable_descriptor_type`] indicates\n  that the implementation **must** support using the [`crate::vk::DescriptorType`]of [`crate::vk::DescriptorType::MUTABLE_VALVE`] with at least the following\n  descriptor types, where any combination of the types **must** be supported:\n\n  * [`crate::vk::DescriptorType::SAMPLED_IMAGE`]\n\n  * [`crate::vk::DescriptorType::STORAGE_IMAGE`]\n\n  * [`crate::vk::DescriptorType::UNIFORM_TEXEL_BUFFER`]\n\n  * [`crate::vk::DescriptorType::STORAGE_TEXEL_BUFFER`]\n\n  * [`crate::vk::DescriptorType::UNIFORM_BUFFER`]\n\n  * [`crate::vk::DescriptorType::STORAGE_BUFFER`]\n\n* Additionally, [`Self::mutable_descriptor_type`] indicates that:\n\n  * Non-uniform descriptor indexing **must** be supported if all descriptor\n    types in a [`crate::vk::MutableDescriptorTypeListVALVE`] for[`crate::vk::DescriptorType::MUTABLE_VALVE`] have the corresponding\n    non-uniform indexing features enabled in[`crate::vk::PhysicalDeviceDescriptorIndexingFeatures`].\n\n  * [`crate::vk::DescriptorBindingFlagBits::UPDATE_AFTER_BIND`] with`descriptorType` of [`crate::vk::DescriptorType::MUTABLE_VALVE`] relaxes\n    the list of required descriptor types to the descriptor types which\n    have the corresponding update-after-bind feature enabled in[`crate::vk::PhysicalDeviceDescriptorIndexingFeatures`].\n\n  * Dynamically uniform descriptor indexing **must** be supported if all\n    descriptor types in a [`crate::vk::MutableDescriptorTypeListVALVE`] for[`crate::vk::DescriptorType::MUTABLE_VALVE`] have the corresponding dynamic\n    indexing features enabled.\n\n  * [`crate::vk::DescriptorSetLayoutCreateFlagBits::HOST_ONLY_POOL_VALVE`] **must** be\n    supported.\n\n  * [`crate::vk::DescriptorPoolCreateFlagBits::HOST_ONLY_VALVE`] **must** be supported.\n\nIf the [`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub mutable_descriptor_type: crate::vk1_0::Bool32,
}
impl PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE;
}
impl Default for PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), mutable_descriptor_type: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceMutableDescriptorTypeFeaturesVALVE").field("s_type", &self.s_type).field("p_next", &self.p_next).field("mutable_descriptor_type", &(self.mutable_descriptor_type != 0)).finish()
    }
}
impl PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
        PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE.html)) · Builder of [`PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] <br/> VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE - Structure describing whether the mutable descriptor type is supported\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] structure is\ndefined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           mutableDescriptorType;\n} VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::mutable_descriptor_type`] indicates\n  that the implementation **must** support using the [`crate::vk::DescriptorType`]of [`crate::vk::DescriptorType::MUTABLE_VALVE`] with at least the following\n  descriptor types, where any combination of the types **must** be supported:\n\n  * [`crate::vk::DescriptorType::SAMPLED_IMAGE`]\n\n  * [`crate::vk::DescriptorType::STORAGE_IMAGE`]\n\n  * [`crate::vk::DescriptorType::UNIFORM_TEXEL_BUFFER`]\n\n  * [`crate::vk::DescriptorType::STORAGE_TEXEL_BUFFER`]\n\n  * [`crate::vk::DescriptorType::UNIFORM_BUFFER`]\n\n  * [`crate::vk::DescriptorType::STORAGE_BUFFER`]\n\n* Additionally, [`Self::mutable_descriptor_type`] indicates that:\n\n  * Non-uniform descriptor indexing **must** be supported if all descriptor\n    types in a [`crate::vk::MutableDescriptorTypeListVALVE`] for[`crate::vk::DescriptorType::MUTABLE_VALVE`] have the corresponding\n    non-uniform indexing features enabled in[`crate::vk::PhysicalDeviceDescriptorIndexingFeatures`].\n\n  * [`crate::vk::DescriptorBindingFlagBits::UPDATE_AFTER_BIND`] with`descriptorType` of [`crate::vk::DescriptorType::MUTABLE_VALVE`] relaxes\n    the list of required descriptor types to the descriptor types which\n    have the corresponding update-after-bind feature enabled in[`crate::vk::PhysicalDeviceDescriptorIndexingFeatures`].\n\n  * Dynamically uniform descriptor indexing **must** be supported if all\n    descriptor types in a [`crate::vk::MutableDescriptorTypeListVALVE`] for[`crate::vk::DescriptorType::MUTABLE_VALVE`] have the corresponding dynamic\n    indexing features enabled.\n\n  * [`crate::vk::DescriptorSetLayoutCreateFlagBits::HOST_ONLY_POOL_VALVE`] **must** be\n    supported.\n\n  * [`crate::vk::DescriptorPoolCreateFlagBits::HOST_ONLY_VALVE`] **must** be supported.\n\nIf the [`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceMutableDescriptorTypeFeaturesVALVE`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a>(PhysicalDeviceMutableDescriptorTypeFeaturesVALVE, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
        PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn mutable_descriptor_type(mut self, mutable_descriptor_type: bool) -> Self {
        self.0.mutable_descriptor_type = mutable_descriptor_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
    fn default() -> PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
    type Target = PhysicalDeviceMutableDescriptorTypeFeaturesVALVE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceMutableDescriptorTypeFeaturesVALVEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMutableDescriptorTypeListVALVE.html)) · Structure <br/> VkMutableDescriptorTypeListVALVE - Structure describing descriptor types that a given descriptor may mutate to\n[](#_c_specification)C Specification\n----------\n\nThe list of potential descriptor types a given mutable descriptor **can**mutate to is passed in a [`crate::vk::MutableDescriptorTypeListVALVE`] structure.\n\nThe [`crate::vk::MutableDescriptorTypeListVALVE`] structure is defined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkMutableDescriptorTypeListVALVE {\n    uint32_t                   descriptorTypeCount;\n    const VkDescriptorType*    pDescriptorTypes;\n} VkMutableDescriptorTypeListVALVE;\n```\n[](#_members)Members\n----------\n\n* [`Self::descriptor_type_count`] is the number of elements in[`Self::p_descriptor_types`].\n\n* [`Self::p_descriptor_types`] is `NULL` or a pointer to an array of[`Self::descriptor_type_count`] [`crate::vk::DescriptorType`] values which define\n  which descriptor types a given binding may mutate to.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-descriptorTypeCount-04597  \n  [`Self::descriptor_type_count`] **must** not be `0` if the corresponding binding\n  is of [`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04598  \n  [`Self::p_descriptor_types`] **must** be a valid pointer to an array of[`Self::descriptor_type_count`] valid, unique [`crate::vk::DescriptorType`] values if\n  the given binding is of [`crate::vk::DescriptorType::MUTABLE_VALVE`] type\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-descriptorTypeCount-04599  \n  [`Self::descriptor_type_count`] **must** be `0` if the corresponding binding is\n  not of [`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04600  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04601  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04602  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04603  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::INLINE_UNIFORM_BLOCK_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-parameter  \n   If [`Self::descriptor_type_count`] is not `0`, [`Self::p_descriptor_types`] **must** be a valid pointer to an array of [`Self::descriptor_type_count`] valid [`crate::vk::DescriptorType`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DescriptorType`], [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`]\n"]
#[doc(alias = "VkMutableDescriptorTypeListVALVE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MutableDescriptorTypeListVALVE {
    pub descriptor_type_count: u32,
    pub p_descriptor_types: *const crate::vk1_0::DescriptorType,
}
impl Default for MutableDescriptorTypeListVALVE {
    fn default() -> Self {
        Self { descriptor_type_count: Default::default(), p_descriptor_types: std::ptr::null() }
    }
}
impl std::fmt::Debug for MutableDescriptorTypeListVALVE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MutableDescriptorTypeListVALVE").field("descriptor_type_count", &self.descriptor_type_count).field("p_descriptor_types", &self.p_descriptor_types).finish()
    }
}
impl MutableDescriptorTypeListVALVE {
    #[inline]
    pub fn into_builder<'a>(self) -> MutableDescriptorTypeListVALVEBuilder<'a> {
        MutableDescriptorTypeListVALVEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMutableDescriptorTypeListVALVE.html)) · Builder of [`MutableDescriptorTypeListVALVE`] <br/> VkMutableDescriptorTypeListVALVE - Structure describing descriptor types that a given descriptor may mutate to\n[](#_c_specification)C Specification\n----------\n\nThe list of potential descriptor types a given mutable descriptor **can**mutate to is passed in a [`crate::vk::MutableDescriptorTypeListVALVE`] structure.\n\nThe [`crate::vk::MutableDescriptorTypeListVALVE`] structure is defined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkMutableDescriptorTypeListVALVE {\n    uint32_t                   descriptorTypeCount;\n    const VkDescriptorType*    pDescriptorTypes;\n} VkMutableDescriptorTypeListVALVE;\n```\n[](#_members)Members\n----------\n\n* [`Self::descriptor_type_count`] is the number of elements in[`Self::p_descriptor_types`].\n\n* [`Self::p_descriptor_types`] is `NULL` or a pointer to an array of[`Self::descriptor_type_count`] [`crate::vk::DescriptorType`] values which define\n  which descriptor types a given binding may mutate to.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-descriptorTypeCount-04597  \n  [`Self::descriptor_type_count`] **must** not be `0` if the corresponding binding\n  is of [`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04598  \n  [`Self::p_descriptor_types`] **must** be a valid pointer to an array of[`Self::descriptor_type_count`] valid, unique [`crate::vk::DescriptorType`] values if\n  the given binding is of [`crate::vk::DescriptorType::MUTABLE_VALVE`] type\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-descriptorTypeCount-04599  \n  [`Self::descriptor_type_count`] **must** be `0` if the corresponding binding is\n  not of [`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04600  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::MUTABLE_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04601  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04602  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC`]\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-04603  \n  [`Self::p_descriptor_types`] **must** not contain[`crate::vk::DescriptorType::INLINE_UNIFORM_BLOCK_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMutableDescriptorTypeListVALVE-pDescriptorTypes-parameter  \n   If [`Self::descriptor_type_count`] is not `0`, [`Self::p_descriptor_types`] **must** be a valid pointer to an array of [`Self::descriptor_type_count`] valid [`crate::vk::DescriptorType`] values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DescriptorType`], [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`]\n"]
#[repr(transparent)]
pub struct MutableDescriptorTypeListVALVEBuilder<'a>(MutableDescriptorTypeListVALVE, std::marker::PhantomData<&'a ()>);
impl<'a> MutableDescriptorTypeListVALVEBuilder<'a> {
    #[inline]
    pub fn new() -> MutableDescriptorTypeListVALVEBuilder<'a> {
        MutableDescriptorTypeListVALVEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn descriptor_types(mut self, descriptor_types: &'a [crate::vk1_0::DescriptorType]) -> Self {
        self.0.p_descriptor_types = descriptor_types.as_ptr() as _;
        self.0.descriptor_type_count = descriptor_types.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MutableDescriptorTypeListVALVE {
        self.0
    }
}
impl<'a> std::default::Default for MutableDescriptorTypeListVALVEBuilder<'a> {
    fn default() -> MutableDescriptorTypeListVALVEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MutableDescriptorTypeListVALVEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MutableDescriptorTypeListVALVEBuilder<'a> {
    type Target = MutableDescriptorTypeListVALVE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MutableDescriptorTypeListVALVEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMutableDescriptorTypeCreateInfoVALVE.html)) · Structure <br/> VkMutableDescriptorTypeCreateInfoVALVE - Structure describing the list of possible active descriptor types for mutable type descriptors\n[](#_c_specification)C Specification\n----------\n\nInformation about the possible descriptor types for mutable descriptor types\nis passed in a [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`] structure as a[`Self::p_next`] to a [`crate::vk::DescriptorSetLayoutCreateInfo`] structure or a[`crate::vk::DescriptorPoolCreateInfo`] structure.\n\nThe [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`] structure is defined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkMutableDescriptorTypeCreateInfoVALVE {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    uint32_t                                   mutableDescriptorTypeListCount;\n    const VkMutableDescriptorTypeListVALVE*    pMutableDescriptorTypeLists;\n} VkMutableDescriptorTypeCreateInfoVALVE;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::mutable_descriptor_type_list_count`] is the number of elements in[`Self::p_mutable_descriptor_type_lists`].\n\n* [`Self::p_mutable_descriptor_type_lists`] is a pointer to an array of[`crate::vk::MutableDescriptorTypeListVALVE`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::mutable_descriptor_type_list_count`] is zero or if this structure is not\nincluded in the [`Self::p_next`] chain, the[`crate::vk::MutableDescriptorTypeListVALVE`] for each element is considered to be\nzero or `NULL` for each member.\nOtherwise, the descriptor set layout binding at[`crate::vk::DescriptorSetLayoutCreateInfo::p_bindings`][i] uses the\ndescriptor type lists in[`crate::vk::MutableDescriptorTypeCreateInfoVALVE::p_mutable_descriptor_type_lists`][i].\n\nValid Usage (Implicit)\n\n* []() VUID-VkMutableDescriptorTypeCreateInfoVALVE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeCreateInfoVALVE-pMutableDescriptorTypeLists-parameter  \n   If [`Self::mutable_descriptor_type_list_count`] is not `0`, [`Self::p_mutable_descriptor_type_lists`] **must** be a valid pointer to an array of [`Self::mutable_descriptor_type_list_count`] valid [`crate::vk::MutableDescriptorTypeListVALVE`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MutableDescriptorTypeListVALVE`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkMutableDescriptorTypeCreateInfoVALVE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MutableDescriptorTypeCreateInfoVALVE {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub mutable_descriptor_type_list_count: u32,
    pub p_mutable_descriptor_type_lists: *const crate::extensions::valve_mutable_descriptor_type::MutableDescriptorTypeListVALVE,
}
impl MutableDescriptorTypeCreateInfoVALVE {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE;
}
impl Default for MutableDescriptorTypeCreateInfoVALVE {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), mutable_descriptor_type_list_count: Default::default(), p_mutable_descriptor_type_lists: std::ptr::null() }
    }
}
impl std::fmt::Debug for MutableDescriptorTypeCreateInfoVALVE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MutableDescriptorTypeCreateInfoVALVE").field("s_type", &self.s_type).field("p_next", &self.p_next).field("mutable_descriptor_type_list_count", &self.mutable_descriptor_type_list_count).field("p_mutable_descriptor_type_lists", &self.p_mutable_descriptor_type_lists).finish()
    }
}
impl MutableDescriptorTypeCreateInfoVALVE {
    #[inline]
    pub fn into_builder<'a>(self) -> MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
        MutableDescriptorTypeCreateInfoVALVEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMutableDescriptorTypeCreateInfoVALVE.html)) · Builder of [`MutableDescriptorTypeCreateInfoVALVE`] <br/> VkMutableDescriptorTypeCreateInfoVALVE - Structure describing the list of possible active descriptor types for mutable type descriptors\n[](#_c_specification)C Specification\n----------\n\nInformation about the possible descriptor types for mutable descriptor types\nis passed in a [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`] structure as a[`Self::p_next`] to a [`crate::vk::DescriptorSetLayoutCreateInfo`] structure or a[`crate::vk::DescriptorPoolCreateInfo`] structure.\n\nThe [`crate::vk::MutableDescriptorTypeCreateInfoVALVE`] structure is defined as:\n\n```\n// Provided by VK_VALVE_mutable_descriptor_type\ntypedef struct VkMutableDescriptorTypeCreateInfoVALVE {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    uint32_t                                   mutableDescriptorTypeListCount;\n    const VkMutableDescriptorTypeListVALVE*    pMutableDescriptorTypeLists;\n} VkMutableDescriptorTypeCreateInfoVALVE;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::mutable_descriptor_type_list_count`] is the number of elements in[`Self::p_mutable_descriptor_type_lists`].\n\n* [`Self::p_mutable_descriptor_type_lists`] is a pointer to an array of[`crate::vk::MutableDescriptorTypeListVALVE`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::mutable_descriptor_type_list_count`] is zero or if this structure is not\nincluded in the [`Self::p_next`] chain, the[`crate::vk::MutableDescriptorTypeListVALVE`] for each element is considered to be\nzero or `NULL` for each member.\nOtherwise, the descriptor set layout binding at[`crate::vk::DescriptorSetLayoutCreateInfo::p_bindings`][i] uses the\ndescriptor type lists in[`crate::vk::MutableDescriptorTypeCreateInfoVALVE::p_mutable_descriptor_type_lists`][i].\n\nValid Usage (Implicit)\n\n* []() VUID-VkMutableDescriptorTypeCreateInfoVALVE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE`]\n\n* []() VUID-VkMutableDescriptorTypeCreateInfoVALVE-pMutableDescriptorTypeLists-parameter  \n   If [`Self::mutable_descriptor_type_list_count`] is not `0`, [`Self::p_mutable_descriptor_type_lists`] **must** be a valid pointer to an array of [`Self::mutable_descriptor_type_list_count`] valid [`crate::vk::MutableDescriptorTypeListVALVE`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::MutableDescriptorTypeListVALVE`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct MutableDescriptorTypeCreateInfoVALVEBuilder<'a>(MutableDescriptorTypeCreateInfoVALVE, std::marker::PhantomData<&'a ()>);
impl<'a> MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
    #[inline]
    pub fn new() -> MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
        MutableDescriptorTypeCreateInfoVALVEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn mutable_descriptor_type_lists(mut self, mutable_descriptor_type_lists: &'a [crate::extensions::valve_mutable_descriptor_type::MutableDescriptorTypeListVALVEBuilder]) -> Self {
        self.0.p_mutable_descriptor_type_lists = mutable_descriptor_type_lists.as_ptr() as _;
        self.0.mutable_descriptor_type_list_count = mutable_descriptor_type_lists.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MutableDescriptorTypeCreateInfoVALVE {
        self.0
    }
}
impl<'a> std::default::Default for MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
    fn default() -> MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
    type Target = MutableDescriptorTypeCreateInfoVALVE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MutableDescriptorTypeCreateInfoVALVEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
