#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_XLIB_SURFACE_SPEC_VERSION")]
pub const KHR_XLIB_SURFACE_SPEC_VERSION: u32 = 6;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_XLIB_SURFACE_EXTENSION_NAME")]
pub const KHR_XLIB_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_xlib_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_XLIB_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateXlibSurfaceKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_XLIB_PRESENTATION_SUPPORT_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceXlibPresentationSupportKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXlibSurfaceCreateFlagsKHR.html)) · Bitmask of [`XlibSurfaceCreateFlagBitsKHR`] <br/> VkXlibSurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_xlib_surface\ntypedef VkFlags VkXlibSurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::XlibSurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask, but\nis currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::XlibSurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkXlibSurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct XlibSurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`XlibSurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkXlibSurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct XlibSurfaceCreateFlagBitsKHR(pub u32);
impl XlibSurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> XlibSurfaceCreateFlagsKHR {
        XlibSurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for XlibSurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_xlib_surface`]"]
impl crate::vk1_0::StructureType {
    pub const XLIB_SURFACE_CREATE_INFO_KHR: Self = Self(1000004000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateXlibSurfaceKHR.html)) · Function <br/> vkCreateXlibSurfaceKHR - Create a slink:VkSurfaceKHR object for an X11 window, using the Xlib client-side library\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an X11 window, using the Xlib\nclient-side library, call:\n\n```\n// Provided by VK_KHR_xlib_surface\nVkResult vkCreateXlibSurfaceKHR(\n    VkInstance                                  instance,\n    const VkXlibSurfaceCreateInfoKHR*           pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::XlibSurfaceCreateInfoKHR`]structure containing the parameters affecting the creation of the\n  surface object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateXlibSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateXlibSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::XlibSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateXlibSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateXlibSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::XlibSurfaceCreateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateXlibSurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_xlib_surface::XlibSurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceXlibPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceXlibPresentationSupportKHR - Query physical device for presentation to X11 server using Xlib\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to an X11 server, using the Xlib client-side library, call:\n\n```\n// Provided by VK_KHR_xlib_surface\nVkBool32 vkGetPhysicalDeviceXlibPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    Display*                                    dpy,\n    VisualID                                    visualID);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::dpy`] is a pointer to an Xlib `Display` connection to the server.\n\n* `visualId` is an X11 visual (`VisualID`).\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-queueFamilyIndex-01315  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-dpy-parameter  \n  [`Self::dpy`] **must** be a valid pointer to a `Display` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceXlibPresentationSupportKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, dpy: *mut std::ffi::c_void, visual_id: u64) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXlibSurfaceCreateInfoKHR.html)) · Structure <br/> VkXlibSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Xlib surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::XlibSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_xlib_surface\ntypedef struct VkXlibSurfaceCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkXlibSurfaceCreateFlagsKHR    flags;\n    Display*                       dpy;\n    Window                         window;\n} VkXlibSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::dpy`] is a pointer to an Xlib `Display` connection to the X\n  server.\n\n* [`Self::window`] is an Xlib `Window` to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-dpy-01313  \n  [`Self::dpy`] **must** point to a valid Xlib `Display`\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-window-01314  \n  [`Self::window`] **must** be a valid Xlib `Window`\n\nValid Usage (Implicit)\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::XLIB_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::XlibSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_xlib_surface_khr`]\n"]
#[doc(alias = "VkXlibSurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct XlibSurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_xlib_surface::XlibSurfaceCreateFlagsKHR,
    pub dpy: *mut std::ffi::c_void,
    pub window: u64,
}
impl XlibSurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::XLIB_SURFACE_CREATE_INFO_KHR;
}
impl Default for XlibSurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), dpy: std::ptr::null_mut(), window: Default::default() }
    }
}
impl std::fmt::Debug for XlibSurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("XlibSurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("dpy", &self.dpy).field("window", &self.window).finish()
    }
}
impl XlibSurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> XlibSurfaceCreateInfoKHRBuilder<'a> {
        XlibSurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkXlibSurfaceCreateInfoKHR.html)) · Builder of [`XlibSurfaceCreateInfoKHR`] <br/> VkXlibSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Xlib surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::XlibSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_xlib_surface\ntypedef struct VkXlibSurfaceCreateInfoKHR {\n    VkStructureType                sType;\n    const void*                    pNext;\n    VkXlibSurfaceCreateFlagsKHR    flags;\n    Display*                       dpy;\n    Window                         window;\n} VkXlibSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::dpy`] is a pointer to an Xlib `Display` connection to the X\n  server.\n\n* [`Self::window`] is an Xlib `Window` to associate the surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-dpy-01313  \n  [`Self::dpy`] **must** point to a valid Xlib `Display`\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-window-01314  \n  [`Self::window`] **must** be a valid Xlib `Window`\n\nValid Usage (Implicit)\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::XLIB_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkXlibSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::XlibSurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_xlib_surface_khr`]\n"]
#[repr(transparent)]
pub struct XlibSurfaceCreateInfoKHRBuilder<'a>(XlibSurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> XlibSurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> XlibSurfaceCreateInfoKHRBuilder<'a> {
        XlibSurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_xlib_surface::XlibSurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn dpy(mut self, dpy: *mut std::ffi::c_void) -> Self {
        self.0.dpy = dpy;
        self
    }
    #[inline]
    pub fn window(mut self, window: u64) -> Self {
        self.0.window = window as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> XlibSurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for XlibSurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> XlibSurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for XlibSurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for XlibSurfaceCreateInfoKHRBuilder<'a> {
    type Target = XlibSurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for XlibSurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_xlib_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateXlibSurfaceKHR.html)) · Function <br/> vkCreateXlibSurfaceKHR - Create a slink:VkSurfaceKHR object for an X11 window, using the Xlib client-side library\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an X11 window, using the Xlib\nclient-side library, call:\n\n```\n// Provided by VK_KHR_xlib_surface\nVkResult vkCreateXlibSurfaceKHR(\n    VkInstance                                  instance,\n    const VkXlibSurfaceCreateInfoKHR*           pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::XlibSurfaceCreateInfoKHR`]structure containing the parameters affecting the creation of the\n  surface object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateXlibSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateXlibSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::XlibSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateXlibSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateXlibSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::XlibSurfaceCreateInfoKHR`]\n"]
    #[doc(alias = "vkCreateXlibSurfaceKHR")]
    pub unsafe fn create_xlib_surface_khr(&self, create_info: &crate::extensions::khr_xlib_surface::XlibSurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_xlib_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceXlibPresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceXlibPresentationSupportKHR - Query physical device for presentation to X11 server using Xlib\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to an X11 server, using the Xlib client-side library, call:\n\n```\n// Provided by VK_KHR_xlib_surface\nVkBool32 vkGetPhysicalDeviceXlibPresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex,\n    Display*                                    dpy,\n    VisualID                                    visualID);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n\n* [`Self::dpy`] is a pointer to an Xlib `Display` connection to the server.\n\n* `visualId` is an X11 visual (`VisualID`).\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-queueFamilyIndex-01315  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceXlibPresentationSupportKHR-dpy-parameter  \n  [`Self::dpy`] **must** be a valid pointer to a `Display` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceXlibPresentationSupportKHR")]
    pub unsafe fn get_physical_device_xlib_presentation_support_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32, dpy: *mut std::ffi::c_void, visual_id: u64) -> bool {
        let _function = self.get_physical_device_xlib_presentation_support_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _, dpy, visual_id as _);
        _return != 0
    }
}
