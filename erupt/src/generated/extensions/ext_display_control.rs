#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISPLAY_CONTROL_SPEC_VERSION")]
pub const EXT_DISPLAY_CONTROL_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISPLAY_CONTROL_EXTENSION_NAME")]
pub const EXT_DISPLAY_CONTROL_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_display_control");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DISPLAY_POWER_CONTROL_EXT: *const std::os::raw::c_char = crate::cstr!("vkDisplayPowerControlEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_REGISTER_DEVICE_EVENT_EXT: *const std::os::raw::c_char = crate::cstr!("vkRegisterDeviceEventEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_REGISTER_DISPLAY_EVENT_EXT: *const std::os::raw::c_char = crate::cstr!("vkRegisterDisplayEventEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_SWAPCHAIN_COUNTER_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetSwapchainCounterEXT");
#[doc = "Provided by [`crate::extensions::ext_display_control`]"]
impl crate::vk1_0::StructureType {
    pub const DISPLAY_POWER_INFO_EXT: Self = Self(1000091000);
    pub const DEVICE_EVENT_INFO_EXT: Self = Self(1000091001);
    pub const DISPLAY_EVENT_INFO_EXT: Self = Self(1000091002);
    pub const SWAPCHAIN_COUNTER_CREATE_INFO_EXT: Self = Self(1000091003);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPowerStateEXT.html)) · Enum <br/> VkDisplayPowerStateEXT - Possible power states for a display\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DisplayPowerInfoEXT::power_state`], specifying\nthe new power state of a display, are:\n\n```\n// Provided by VK_EXT_display_control\ntypedef enum VkDisplayPowerStateEXT {\n    VK_DISPLAY_POWER_STATE_OFF_EXT = 0,\n    VK_DISPLAY_POWER_STATE_SUSPEND_EXT = 1,\n    VK_DISPLAY_POWER_STATE_ON_EXT = 2,\n} VkDisplayPowerStateEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::OFF_EXT`] specifies that the display is\n  powered down.\n\n* [`Self::SUSPEND_EXT`] specifies that the display is\n  put into a low power mode, from which it **may** be able to transition back\n  to [`Self::ON_EXT`] more quickly than if it were in[`Self::OFF_EXT`].\n  This state **may** be the same as [`Self::OFF_EXT`].\n\n* [`Self::ON_EXT`] specifies that the display is\n  powered on.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPowerInfoEXT`]\n"]
#[doc(alias = "VkDisplayPowerStateEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DisplayPowerStateEXT(pub i32);
impl std::fmt::Debug for DisplayPowerStateEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::OFF_EXT => "OFF_EXT",
            &Self::SUSPEND_EXT => "SUSPEND_EXT",
            &Self::ON_EXT => "ON_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_control`]"]
impl crate::extensions::ext_display_control::DisplayPowerStateEXT {
    pub const OFF_EXT: Self = Self(0);
    pub const SUSPEND_EXT: Self = Self(1);
    pub const ON_EXT: Self = Self(2);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceEventTypeEXT.html)) · Enum <br/> VkDeviceEventTypeEXT - Events that can occur on a device object\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DeviceEventInfoEXT::device`], specifying when\na fence will be signaled, are:\n\n```\n// Provided by VK_EXT_display_control\ntypedef enum VkDeviceEventTypeEXT {\n    VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT = 0,\n} VkDeviceEventTypeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::DISPLAY_HOTPLUG_EXT`] specifies that the fence\n  is signaled when a display is plugged into or unplugged from the\n  specified device.\n  Applications **can** use this notification to determine when they need to\n  re-enumerate the available displays on a device.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceEventInfoEXT`]\n"]
#[doc(alias = "VkDeviceEventTypeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DeviceEventTypeEXT(pub i32);
impl std::fmt::Debug for DeviceEventTypeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DISPLAY_HOTPLUG_EXT => "DISPLAY_HOTPLUG_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_control`]"]
impl crate::extensions::ext_display_control::DeviceEventTypeEXT {
    pub const DISPLAY_HOTPLUG_EXT: Self = Self(0);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayEventTypeEXT.html)) · Enum <br/> VkDisplayEventTypeEXT - Events that can occur on a display object\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DisplayEventInfoEXT::display_event`],\nspecifying when a fence will be signaled, are:\n\n```\n// Provided by VK_EXT_display_control\ntypedef enum VkDisplayEventTypeEXT {\n    VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT = 0,\n} VkDisplayEventTypeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::FIRST_PIXEL_OUT_EXT`] specifies that the fence\n  is signaled when the first pixel of the next display refresh cycle\n  leaves the display engine for the display.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayEventInfoEXT`]\n"]
#[doc(alias = "VkDisplayEventTypeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DisplayEventTypeEXT(pub i32);
impl std::fmt::Debug for DisplayEventTypeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::FIRST_PIXEL_OUT_EXT => "FIRST_PIXEL_OUT_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_control`]"]
impl crate::extensions::ext_display_control::DisplayEventTypeEXT {
    pub const FIRST_PIXEL_OUT_EXT: Self = Self(0);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDisplayPowerControlEXT.html)) · Function <br/> vkDisplayPowerControlEXT - Set the power state of a display\n[](#_c_specification)C Specification\n----------\n\nTo set the power state of a display, call:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkDisplayPowerControlEXT(\n    VkDevice                                    device,\n    VkDisplayKHR                                display,\n    const VkDisplayPowerInfoEXT*                pDisplayPowerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device associated with [`Self::display`].\n\n* [`Self::display`] is the display whose power state is modified.\n\n* [`Self::p_display_power_info`] is a pointer to a [`crate::vk::DisplayPowerInfoEXT`]structure specifying the new power state of [`Self::display`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDisplayPowerControlEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDisplayPowerControlEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkDisplayPowerControlEXT-pDisplayPowerInfo-parameter  \n  [`Self::p_display_power_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayPowerInfoEXT`] structure\n\n* []() VUID-vkDisplayPowerControlEXT-commonparent  \n   Both of [`Self::device`], and [`Self::display`] **must** have been created, allocated, or retrieved from the same [`crate::vk::PhysicalDevice`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayPowerInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDisplayPowerControlEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, display: crate::extensions::khr_display::DisplayKHR, p_display_power_info: *const crate::extensions::ext_display_control::DisplayPowerInfoEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkRegisterDeviceEventEXT.html)) · Function <br/> vkRegisterDeviceEventEXT - Signal a fence when a device event occurs\n[](#_c_specification)C Specification\n----------\n\nTo create a fence that will be signaled when an event occurs on a device,\ncall:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkRegisterDeviceEventEXT(\n    VkDevice                                    device,\n    const VkDeviceEventInfoEXT*                 pDeviceEventInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkFence*                                    pFence);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device on which the event **may** occur.\n\n* [`Self::p_device_event_info`] is a pointer to a [`crate::vk::DeviceEventInfoEXT`]structure describing the event of interest to the application.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_fence`] is a pointer to a handle in which the resulting fence\n  object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkRegisterDeviceEventEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkRegisterDeviceEventEXT-pDeviceEventInfo-parameter  \n  [`Self::p_device_event_info`] **must** be a valid pointer to a valid [`crate::vk::DeviceEventInfoEXT`] structure\n\n* []() VUID-vkRegisterDeviceEventEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkRegisterDeviceEventEXT-pFence-parameter  \n  [`Self::p_fence`] **must** be a valid pointer to a [`crate::vk::Fence`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::DeviceEventInfoEXT`], [`crate::vk::Fence`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkRegisterDeviceEventEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_device_event_info: *const crate::extensions::ext_display_control::DeviceEventInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_fence: *mut crate::vk1_0::Fence) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkRegisterDisplayEventEXT.html)) · Function <br/> vkRegisterDisplayEventEXT - Signal a fence when a display event occurs\n[](#_c_specification)C Specification\n----------\n\nTo create a fence that will be signaled when an event occurs on a[`crate::vk::DisplayKHR`] object, call:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkRegisterDisplayEventEXT(\n    VkDevice                                    device,\n    VkDisplayKHR                                display,\n    const VkDisplayEventInfoEXT*                pDisplayEventInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkFence*                                    pFence);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device associated with [`Self::display`]\n\n* [`Self::display`] is the display on which the event **may** occur.\n\n* [`Self::p_display_event_info`] is a pointer to a [`crate::vk::DisplayEventInfoEXT`]structure describing the event of interest to the application.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_fence`] is a pointer to a handle in which the resulting fence\n  object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkRegisterDisplayEventEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-pDisplayEventInfo-parameter  \n  [`Self::p_display_event_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayEventInfoEXT`] structure\n\n* []() VUID-vkRegisterDisplayEventEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkRegisterDisplayEventEXT-pFence-parameter  \n  [`Self::p_fence`] **must** be a valid pointer to a [`crate::vk::Fence`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-commonparent  \n   Both of [`Self::device`], and [`Self::display`] **must** have been created, allocated, or retrieved from the same [`crate::vk::PhysicalDevice`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::DisplayEventInfoEXT`], [`crate::vk::DisplayKHR`], [`crate::vk::Fence`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkRegisterDisplayEventEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, display: crate::extensions::khr_display::DisplayKHR, p_display_event_info: *const crate::extensions::ext_display_control::DisplayEventInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_fence: *mut crate::vk1_0::Fence) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSwapchainCounterEXT.html)) · Function <br/> vkGetSwapchainCounterEXT - Query the current value of a surface counter\n[](#_c_specification)C Specification\n----------\n\nThe requested counters become active when the first presentation command for\nthe associated swapchain is processed by the presentation engine.\nTo query the value of an active counter, use:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkGetSwapchainCounterEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    VkSurfaceCounterFlagBitsEXT                 counter,\n    uint64_t*                                   pCounterValue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the [`crate::vk::Device`] associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain from which to query the counter value.\n\n* [`Self::counter`] is a [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) value specifying\n  the counter to query.\n\n* [`Self::p_counter_value`] will return the current value of the counter.\n[](#_description)Description\n----------\n\nIf a counter is not available because the swapchain is out of date, the\nimplementation **may** return [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`].\n\nValid Usage\n\n* []() VUID-vkGetSwapchainCounterEXT-swapchain-01245  \n   One or more present commands on [`Self::swapchain`] **must** have been\n  processed by the presentation engine\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSwapchainCounterEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSwapchainCounterEXT-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetSwapchainCounterEXT-counter-parameter  \n  [`Self::counter`] **must** be a valid [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) value\n\n* []() VUID-vkGetSwapchainCounterEXT-pCounterValue-parameter  \n  [`Self::p_counter_value`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetSwapchainCounterEXT-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html), [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetSwapchainCounterEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, counter: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagBitsEXT, p_counter_value: *mut u64) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, SwapchainCounterCreateInfoEXT> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, SwapchainCounterCreateInfoEXTBuilder<'_>> for crate::extensions::khr_swapchain::SwapchainCreateInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPowerInfoEXT.html)) · Structure <br/> VkDisplayPowerInfoEXT - Describe the power state of a display\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPowerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDisplayPowerInfoEXT {\n    VkStructureType           sType;\n    const void*               pNext;\n    VkDisplayPowerStateEXT    powerState;\n} VkDisplayPowerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::power_state`] is a [`crate::vk::DisplayPowerStateEXT`] value specifying the\n  new power state of the display.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayPowerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_POWER_INFO_EXT`]\n\n* []() VUID-VkDisplayPowerInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayPowerInfoEXT-powerState-parameter  \n  [`Self::power_state`] **must** be a valid [`crate::vk::DisplayPowerStateEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPowerStateEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::display_power_control_ext`]\n"]
#[doc(alias = "VkDisplayPowerInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayPowerInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub power_state: crate::extensions::ext_display_control::DisplayPowerStateEXT,
}
impl DisplayPowerInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DISPLAY_POWER_INFO_EXT;
}
impl Default for DisplayPowerInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), power_state: Default::default() }
    }
}
impl std::fmt::Debug for DisplayPowerInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayPowerInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("power_state", &self.power_state).finish()
    }
}
impl DisplayPowerInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayPowerInfoEXTBuilder<'a> {
        DisplayPowerInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayPowerInfoEXT.html)) · Builder of [`DisplayPowerInfoEXT`] <br/> VkDisplayPowerInfoEXT - Describe the power state of a display\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayPowerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDisplayPowerInfoEXT {\n    VkStructureType           sType;\n    const void*               pNext;\n    VkDisplayPowerStateEXT    powerState;\n} VkDisplayPowerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::power_state`] is a [`crate::vk::DisplayPowerStateEXT`] value specifying the\n  new power state of the display.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayPowerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_POWER_INFO_EXT`]\n\n* []() VUID-VkDisplayPowerInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayPowerInfoEXT-powerState-parameter  \n  [`Self::power_state`] **must** be a valid [`crate::vk::DisplayPowerStateEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayPowerStateEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::display_power_control_ext`]\n"]
#[repr(transparent)]
pub struct DisplayPowerInfoEXTBuilder<'a>(DisplayPowerInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayPowerInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayPowerInfoEXTBuilder<'a> {
        DisplayPowerInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn power_state(mut self, power_state: crate::extensions::ext_display_control::DisplayPowerStateEXT) -> Self {
        self.0.power_state = power_state as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayPowerInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DisplayPowerInfoEXTBuilder<'a> {
    fn default() -> DisplayPowerInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayPowerInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayPowerInfoEXTBuilder<'a> {
    type Target = DisplayPowerInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayPowerInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceEventInfoEXT.html)) · Structure <br/> VkDeviceEventInfoEXT - Describe a device event to create\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DeviceEventInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDeviceEventInfoEXT {\n    VkStructureType         sType;\n    const void*             pNext;\n    VkDeviceEventTypeEXT    deviceEvent;\n} VkDeviceEventInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `device` is a [`crate::vk::DeviceEventTypeEXT`] value specifying when the\n  fence will be signaled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceEventInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_EVENT_INFO_EXT`]\n\n* []() VUID-VkDeviceEventInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDeviceEventInfoEXT-deviceEvent-parameter  \n  [`Self::device_event`] **must** be a valid [`crate::vk::DeviceEventTypeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceEventTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::register_device_event_ext`]\n"]
#[doc(alias = "VkDeviceEventInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DeviceEventInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub device_event: crate::extensions::ext_display_control::DeviceEventTypeEXT,
}
impl DeviceEventInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEVICE_EVENT_INFO_EXT;
}
impl Default for DeviceEventInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), device_event: Default::default() }
    }
}
impl std::fmt::Debug for DeviceEventInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DeviceEventInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("device_event", &self.device_event).finish()
    }
}
impl DeviceEventInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DeviceEventInfoEXTBuilder<'a> {
        DeviceEventInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDeviceEventInfoEXT.html)) · Builder of [`DeviceEventInfoEXT`] <br/> VkDeviceEventInfoEXT - Describe a device event to create\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DeviceEventInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDeviceEventInfoEXT {\n    VkStructureType         sType;\n    const void*             pNext;\n    VkDeviceEventTypeEXT    deviceEvent;\n} VkDeviceEventInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `device` is a [`crate::vk::DeviceEventTypeEXT`] value specifying when the\n  fence will be signaled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDeviceEventInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEVICE_EVENT_INFO_EXT`]\n\n* []() VUID-VkDeviceEventInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDeviceEventInfoEXT-deviceEvent-parameter  \n  [`Self::device_event`] **must** be a valid [`crate::vk::DeviceEventTypeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceEventTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::register_device_event_ext`]\n"]
#[repr(transparent)]
pub struct DeviceEventInfoEXTBuilder<'a>(DeviceEventInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DeviceEventInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DeviceEventInfoEXTBuilder<'a> {
        DeviceEventInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn device_event(mut self, device_event: crate::extensions::ext_display_control::DeviceEventTypeEXT) -> Self {
        self.0.device_event = device_event as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DeviceEventInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DeviceEventInfoEXTBuilder<'a> {
    fn default() -> DeviceEventInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DeviceEventInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DeviceEventInfoEXTBuilder<'a> {
    type Target = DeviceEventInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DeviceEventInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayEventInfoEXT.html)) · Structure <br/> VkDisplayEventInfoEXT - Describe a display event to create\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayEventInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDisplayEventInfoEXT {\n    VkStructureType          sType;\n    const void*              pNext;\n    VkDisplayEventTypeEXT    displayEvent;\n} VkDisplayEventInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::display_event`] is a [`crate::vk::DisplayEventTypeEXT`] specifying when the\n  fence will be signaled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayEventInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_EVENT_INFO_EXT`]\n\n* []() VUID-VkDisplayEventInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayEventInfoEXT-displayEvent-parameter  \n  [`Self::display_event`] **must** be a valid [`crate::vk::DisplayEventTypeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayEventTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::register_display_event_ext`]\n"]
#[doc(alias = "VkDisplayEventInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DisplayEventInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub display_event: crate::extensions::ext_display_control::DisplayEventTypeEXT,
}
impl DisplayEventInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DISPLAY_EVENT_INFO_EXT;
}
impl Default for DisplayEventInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), display_event: Default::default() }
    }
}
impl std::fmt::Debug for DisplayEventInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DisplayEventInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("display_event", &self.display_event).finish()
    }
}
impl DisplayEventInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DisplayEventInfoEXTBuilder<'a> {
        DisplayEventInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDisplayEventInfoEXT.html)) · Builder of [`DisplayEventInfoEXT`] <br/> VkDisplayEventInfoEXT - Describe a display event to create\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DisplayEventInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkDisplayEventInfoEXT {\n    VkStructureType          sType;\n    const void*              pNext;\n    VkDisplayEventTypeEXT    displayEvent;\n} VkDisplayEventInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::display_event`] is a [`crate::vk::DisplayEventTypeEXT`] specifying when the\n  fence will be signaled.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDisplayEventInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DISPLAY_EVENT_INFO_EXT`]\n\n* []() VUID-VkDisplayEventInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDisplayEventInfoEXT-displayEvent-parameter  \n  [`Self::display_event`] **must** be a valid [`crate::vk::DisplayEventTypeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DisplayEventTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::register_display_event_ext`]\n"]
#[repr(transparent)]
pub struct DisplayEventInfoEXTBuilder<'a>(DisplayEventInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DisplayEventInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DisplayEventInfoEXTBuilder<'a> {
        DisplayEventInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn display_event(mut self, display_event: crate::extensions::ext_display_control::DisplayEventTypeEXT) -> Self {
        self.0.display_event = display_event as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DisplayEventInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DisplayEventInfoEXTBuilder<'a> {
    fn default() -> DisplayEventInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DisplayEventInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DisplayEventInfoEXTBuilder<'a> {
    type Target = DisplayEventInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DisplayEventInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSwapchainCounterCreateInfoEXT.html)) · Structure <br/> VkSwapchainCounterCreateInfoEXT - Specify the surface counters desired\n[](#_c_specification)C Specification\n----------\n\nTo enable surface counters when creating a swapchain, add a[`crate::vk::SwapchainCounterCreateInfoEXT`] structure to the [`Self::p_next`] chain of[`crate::vk::SwapchainCreateInfoKHR`].[`crate::vk::SwapchainCounterCreateInfoEXT`] is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkSwapchainCounterCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkSurfaceCounterFlagsEXT    surfaceCounters;\n} VkSwapchainCounterCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_counters`] is a bitmask of [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html)specifying surface counters to enable for the swapchain.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-surfaceCounters-01244  \n   The bits in [`Self::surface_counters`] **must** be supported by[`crate::vk::SwapchainCreateInfoKHR::surface`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SWAPCHAIN_COUNTER_CREATE_INFO_EXT`]\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-surfaceCounters-parameter  \n  [`Self::surface_counters`] **must** be a valid combination of [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceCounterFlagBitsEXT`]\n"]
#[doc(alias = "VkSwapchainCounterCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SwapchainCounterCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub surface_counters: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagsEXT,
}
impl SwapchainCounterCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SWAPCHAIN_COUNTER_CREATE_INFO_EXT;
}
impl Default for SwapchainCounterCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), surface_counters: Default::default() }
    }
}
impl std::fmt::Debug for SwapchainCounterCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SwapchainCounterCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("surface_counters", &self.surface_counters).finish()
    }
}
impl SwapchainCounterCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SwapchainCounterCreateInfoEXTBuilder<'a> {
        SwapchainCounterCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSwapchainCounterCreateInfoEXT.html)) · Builder of [`SwapchainCounterCreateInfoEXT`] <br/> VkSwapchainCounterCreateInfoEXT - Specify the surface counters desired\n[](#_c_specification)C Specification\n----------\n\nTo enable surface counters when creating a swapchain, add a[`crate::vk::SwapchainCounterCreateInfoEXT`] structure to the [`Self::p_next`] chain of[`crate::vk::SwapchainCreateInfoKHR`].[`crate::vk::SwapchainCounterCreateInfoEXT`] is defined as:\n\n```\n// Provided by VK_EXT_display_control\ntypedef struct VkSwapchainCounterCreateInfoEXT {\n    VkStructureType             sType;\n    const void*                 pNext;\n    VkSurfaceCounterFlagsEXT    surfaceCounters;\n} VkSwapchainCounterCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::surface_counters`] is a bitmask of [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html)specifying surface counters to enable for the swapchain.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-surfaceCounters-01244  \n   The bits in [`Self::surface_counters`] **must** be supported by[`crate::vk::SwapchainCreateInfoKHR::surface`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SWAPCHAIN_COUNTER_CREATE_INFO_EXT`]\n\n* []() VUID-VkSwapchainCounterCreateInfoEXT-surfaceCounters-parameter  \n  [`Self::surface_counters`] **must** be a valid combination of [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::SurfaceCounterFlagBitsEXT`]\n"]
#[repr(transparent)]
pub struct SwapchainCounterCreateInfoEXTBuilder<'a>(SwapchainCounterCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> SwapchainCounterCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> SwapchainCounterCreateInfoEXTBuilder<'a> {
        SwapchainCounterCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn surface_counters(mut self, surface_counters: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagsEXT) -> Self {
        self.0.surface_counters = surface_counters as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SwapchainCounterCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for SwapchainCounterCreateInfoEXTBuilder<'a> {
    fn default() -> SwapchainCounterCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SwapchainCounterCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SwapchainCounterCreateInfoEXTBuilder<'a> {
    type Target = SwapchainCounterCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SwapchainCounterCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_control`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDisplayPowerControlEXT.html)) · Function <br/> vkDisplayPowerControlEXT - Set the power state of a display\n[](#_c_specification)C Specification\n----------\n\nTo set the power state of a display, call:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkDisplayPowerControlEXT(\n    VkDevice                                    device,\n    VkDisplayKHR                                display,\n    const VkDisplayPowerInfoEXT*                pDisplayPowerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device associated with [`Self::display`].\n\n* [`Self::display`] is the display whose power state is modified.\n\n* [`Self::p_display_power_info`] is a pointer to a [`crate::vk::DisplayPowerInfoEXT`]structure specifying the new power state of [`Self::display`].\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDisplayPowerControlEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDisplayPowerControlEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkDisplayPowerControlEXT-pDisplayPowerInfo-parameter  \n  [`Self::p_display_power_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayPowerInfoEXT`] structure\n\n* []() VUID-vkDisplayPowerControlEXT-commonparent  \n   Both of [`Self::device`], and [`Self::display`] **must** have been created, allocated, or retrieved from the same [`crate::vk::PhysicalDevice`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::DisplayKHR`], [`crate::vk::DisplayPowerInfoEXT`]\n"]
    #[doc(alias = "vkDisplayPowerControlEXT")]
    pub unsafe fn display_power_control_ext(&self, display: crate::extensions::khr_display::DisplayKHR, display_power_info: &crate::extensions::ext_display_control::DisplayPowerInfoEXT) -> crate::utils::VulkanResult<()> {
        let _function = self.display_power_control_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, display as _, display_power_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkRegisterDeviceEventEXT.html)) · Function <br/> vkRegisterDeviceEventEXT - Signal a fence when a device event occurs\n[](#_c_specification)C Specification\n----------\n\nTo create a fence that will be signaled when an event occurs on a device,\ncall:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkRegisterDeviceEventEXT(\n    VkDevice                                    device,\n    const VkDeviceEventInfoEXT*                 pDeviceEventInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkFence*                                    pFence);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device on which the event **may** occur.\n\n* [`Self::p_device_event_info`] is a pointer to a [`crate::vk::DeviceEventInfoEXT`]structure describing the event of interest to the application.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_fence`] is a pointer to a handle in which the resulting fence\n  object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkRegisterDeviceEventEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkRegisterDeviceEventEXT-pDeviceEventInfo-parameter  \n  [`Self::p_device_event_info`] **must** be a valid pointer to a valid [`crate::vk::DeviceEventInfoEXT`] structure\n\n* []() VUID-vkRegisterDeviceEventEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkRegisterDeviceEventEXT-pFence-parameter  \n  [`Self::p_fence`] **must** be a valid pointer to a [`crate::vk::Fence`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::DeviceEventInfoEXT`], [`crate::vk::Fence`]\n"]
    #[doc(alias = "vkRegisterDeviceEventEXT")]
    pub unsafe fn register_device_event_ext(&self, device_event_info: &crate::extensions::ext_display_control::DeviceEventInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::vk1_0::Fence> {
        let _function = self.register_device_event_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut fence = Default::default();
        let _return = _function(
            self.handle,
            device_event_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut fence,
        );
        crate::utils::VulkanResult::new(_return, fence)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkRegisterDisplayEventEXT.html)) · Function <br/> vkRegisterDisplayEventEXT - Signal a fence when a display event occurs\n[](#_c_specification)C Specification\n----------\n\nTo create a fence that will be signaled when an event occurs on a[`crate::vk::DisplayKHR`] object, call:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkRegisterDisplayEventEXT(\n    VkDevice                                    device,\n    VkDisplayKHR                                display,\n    const VkDisplayEventInfoEXT*                pDisplayEventInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkFence*                                    pFence);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is a logical device associated with [`Self::display`]\n\n* [`Self::display`] is the display on which the event **may** occur.\n\n* [`Self::p_display_event_info`] is a pointer to a [`crate::vk::DisplayEventInfoEXT`]structure describing the event of interest to the application.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_fence`] is a pointer to a handle in which the resulting fence\n  object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkRegisterDisplayEventEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-display-parameter  \n  [`Self::display`] **must** be a valid [`crate::vk::DisplayKHR`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-pDisplayEventInfo-parameter  \n  [`Self::p_display_event_info`] **must** be a valid pointer to a valid [`crate::vk::DisplayEventInfoEXT`] structure\n\n* []() VUID-vkRegisterDisplayEventEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkRegisterDisplayEventEXT-pFence-parameter  \n  [`Self::p_fence`] **must** be a valid pointer to a [`crate::vk::Fence`] handle\n\n* []() VUID-vkRegisterDisplayEventEXT-commonparent  \n   Both of [`Self::device`], and [`Self::display`] **must** have been created, allocated, or retrieved from the same [`crate::vk::PhysicalDevice`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Device`], [`crate::vk::DisplayEventInfoEXT`], [`crate::vk::DisplayKHR`], [`crate::vk::Fence`]\n"]
    #[doc(alias = "vkRegisterDisplayEventEXT")]
    pub unsafe fn register_display_event_ext(&self, display: crate::extensions::khr_display::DisplayKHR, display_event_info: &crate::extensions::ext_display_control::DisplayEventInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::vk1_0::Fence> {
        let _function = self.register_display_event_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut fence = Default::default();
        let _return = _function(
            self.handle,
            display as _,
            display_event_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut fence,
        );
        crate::utils::VulkanResult::new(_return, fence)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetSwapchainCounterEXT.html)) · Function <br/> vkGetSwapchainCounterEXT - Query the current value of a surface counter\n[](#_c_specification)C Specification\n----------\n\nThe requested counters become active when the first presentation command for\nthe associated swapchain is processed by the presentation engine.\nTo query the value of an active counter, use:\n\n```\n// Provided by VK_EXT_display_control\nVkResult vkGetSwapchainCounterEXT(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    VkSurfaceCounterFlagBitsEXT                 counter,\n    uint64_t*                                   pCounterValue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the [`crate::vk::Device`] associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain from which to query the counter value.\n\n* [`Self::counter`] is a [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) value specifying\n  the counter to query.\n\n* [`Self::p_counter_value`] will return the current value of the counter.\n[](#_description)Description\n----------\n\nIf a counter is not available because the swapchain is out of date, the\nimplementation **may** return [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`].\n\nValid Usage\n\n* []() VUID-vkGetSwapchainCounterEXT-swapchain-01245  \n   One or more present commands on [`Self::swapchain`] **must** have been\n  processed by the presentation engine\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetSwapchainCounterEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetSwapchainCounterEXT-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetSwapchainCounterEXT-counter-parameter  \n  [`Self::counter`] **must** be a valid [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) value\n\n* []() VUID-vkGetSwapchainCounterEXT-pCounterValue-parameter  \n  [`Self::p_counter_value`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetSwapchainCounterEXT-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html), [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkGetSwapchainCounterEXT")]
    pub unsafe fn get_swapchain_counter_ext(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, counter: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagBitsEXT) -> crate::utils::VulkanResult<u64> {
        let _function = self.get_swapchain_counter_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut counter_value = Default::default();
        let _return = _function(self.handle, swapchain as _, counter as _, &mut counter_value);
        crate::utils::VulkanResult::new(_return, counter_value)
    }
}
