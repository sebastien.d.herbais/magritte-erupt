#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_SPEC_VERSION")]
pub const NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_EXTENSION_NAME")]
pub const NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_device_diagnostic_checkpoints");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_CHECKPOINT_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdSetCheckpointNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_QUEUE_CHECKPOINT_DATA_NV: *const std::os::raw::c_char = crate::cstr!("vkGetQueueCheckpointDataNV");
#[doc = "Provided by [`crate::extensions::nv_device_diagnostic_checkpoints`]"]
impl crate::vk1_0::StructureType {
    pub const CHECKPOINT_DATA_NV: Self = Self(1000206000);
    pub const QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV: Self = Self(1000206001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetCheckpointNV.html)) · Function <br/> vkCmdSetCheckpointNV - insert diagnostic checkpoint in command stream\n[](#_c_specification)C Specification\n----------\n\nDevice diagnostic checkpoints are inserted into the command stream by\ncalling [`crate::vk::DeviceLoader::cmd_set_checkpoint_nv`].\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\nvoid vkCmdSetCheckpointNV(\n    VkCommandBuffer                             commandBuffer,\n    const void*                                 pCheckpointMarker);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer that will receive the marker\n\n* [`Self::p_checkpoint_marker`] is an opaque application-provided value that\n  will be associated with the checkpoint.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetCheckpointNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_checkpoint_marker: *const std::ffi::c_void) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetQueueCheckpointDataNV.html)) · Function <br/> vkGetQueueCheckpointDataNV - retrieve diagnostic checkpoint data\n[](#_c_specification)C Specification\n----------\n\nIf the device encounters an error during execution, the implementation will\nreturn a [`crate::vk::Result::ERROR_DEVICE_LOST`] error to the application at a certain\npoint during host execution.\nWhen this happens, the application **can** call[`crate::vk::DeviceLoader::get_queue_checkpoint_data_nv`] to retrieve information on the most recent\ndiagnostic checkpoints that were executed by the device.\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\nvoid vkGetQueueCheckpointDataNV(\n    VkQueue                                     queue,\n    uint32_t*                                   pCheckpointDataCount,\n    VkCheckpointDataNV*                         pCheckpointData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the [`crate::vk::Queue`] object the caller would like to\n  retrieve checkpoint data for\n\n* [`Self::p_checkpoint_data_count`] is a pointer to an integer related to the\n  number of checkpoint markers available or queried, as described below.\n\n* [`Self::p_checkpoint_data`] is either `NULL` or a pointer to an array of[`crate::vk::CheckpointDataNV`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_checkpoint_data`] is `NULL`, then the number of checkpoint markers\navailable is returned in [`Self::p_checkpoint_data_count`].\n\nOtherwise, [`Self::p_checkpoint_data_count`] **must** point to a variable set by the\nuser to the number of elements in the [`Self::p_checkpoint_data`] array, and on\nreturn the variable is overwritten with the number of structures actually\nwritten to [`Self::p_checkpoint_data`].\n\nIf [`Self::p_checkpoint_data_count`] is less than the number of checkpoint markers\navailable, at most [`Self::p_checkpoint_data_count`] structures will be written.\n\nValid Usage\n\n* []() VUID-vkGetQueueCheckpointDataNV-queue-02025  \n   The device that [`Self::queue`] belongs to **must** be in the lost state\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetQueueCheckpointDataNV-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkGetQueueCheckpointDataNV-pCheckpointDataCount-parameter  \n  [`Self::p_checkpoint_data_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetQueueCheckpointDataNV-pCheckpointData-parameter  \n   If the value referenced by [`Self::p_checkpoint_data_count`] is not `0`, and [`Self::p_checkpoint_data`] is not `NULL`, [`Self::p_checkpoint_data`] **must** be a valid pointer to an array of [`Self::p_checkpoint_data_count`] [`crate::vk::CheckpointDataNV`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CheckpointDataNV`], [`crate::vk::Queue`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetQueueCheckpointDataNV = unsafe extern "system" fn(queue: crate::vk1_0::Queue, p_checkpoint_data_count: *mut u32, p_checkpoint_data: *mut crate::extensions::nv_device_diagnostic_checkpoints::CheckpointDataNV) -> ();
impl<'a> crate::ExtendableFromMut<'a, QueueFamilyCheckpointPropertiesNV> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, QueueFamilyCheckpointPropertiesNVBuilder<'_>> for crate::vk1_1::QueueFamilyProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyCheckpointPropertiesNV.html)) · Structure <br/> VkQueueFamilyCheckpointPropertiesNV - return structure for queue family checkpoint information query\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueueFamilyCheckpointPropertiesNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\ntypedef struct VkQueueFamilyCheckpointPropertiesNV {\n    VkStructureType         sType;\n    void*                   pNext;\n    VkPipelineStageFlags    checkpointExecutionStageMask;\n} VkQueueFamilyCheckpointPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::checkpoint_execution_stage_mask`] is a mask indicating which pipeline\n  stages the implementation can execute checkpoint markers in.\n[](#_description)Description\n----------\n\nAdditional queue family information can be queried by setting[`crate::vk::QueueFamilyProperties2::p_next`] to point to a[`crate::vk::QueueFamilyCheckpointPropertiesNV`] structure.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueueFamilyCheckpointPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineStageFlagBits`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkQueueFamilyCheckpointPropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct QueueFamilyCheckpointPropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub checkpoint_execution_stage_mask: crate::vk1_0::PipelineStageFlags,
}
impl QueueFamilyCheckpointPropertiesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV;
}
impl Default for QueueFamilyCheckpointPropertiesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), checkpoint_execution_stage_mask: Default::default() }
    }
}
impl std::fmt::Debug for QueueFamilyCheckpointPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("QueueFamilyCheckpointPropertiesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("checkpoint_execution_stage_mask", &self.checkpoint_execution_stage_mask).finish()
    }
}
impl QueueFamilyCheckpointPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> QueueFamilyCheckpointPropertiesNVBuilder<'a> {
        QueueFamilyCheckpointPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueueFamilyCheckpointPropertiesNV.html)) · Builder of [`QueueFamilyCheckpointPropertiesNV`] <br/> VkQueueFamilyCheckpointPropertiesNV - return structure for queue family checkpoint information query\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueueFamilyCheckpointPropertiesNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\ntypedef struct VkQueueFamilyCheckpointPropertiesNV {\n    VkStructureType         sType;\n    void*                   pNext;\n    VkPipelineStageFlags    checkpointExecutionStageMask;\n} VkQueueFamilyCheckpointPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::checkpoint_execution_stage_mask`] is a mask indicating which pipeline\n  stages the implementation can execute checkpoint markers in.\n[](#_description)Description\n----------\n\nAdditional queue family information can be queried by setting[`crate::vk::QueueFamilyProperties2::p_next`] to point to a[`crate::vk::QueueFamilyCheckpointPropertiesNV`] structure.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueueFamilyCheckpointPropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineStageFlagBits`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct QueueFamilyCheckpointPropertiesNVBuilder<'a>(QueueFamilyCheckpointPropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> QueueFamilyCheckpointPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> QueueFamilyCheckpointPropertiesNVBuilder<'a> {
        QueueFamilyCheckpointPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn checkpoint_execution_stage_mask(mut self, checkpoint_execution_stage_mask: crate::vk1_0::PipelineStageFlags) -> Self {
        self.0.checkpoint_execution_stage_mask = checkpoint_execution_stage_mask as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> QueueFamilyCheckpointPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for QueueFamilyCheckpointPropertiesNVBuilder<'a> {
    fn default() -> QueueFamilyCheckpointPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for QueueFamilyCheckpointPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for QueueFamilyCheckpointPropertiesNVBuilder<'a> {
    type Target = QueueFamilyCheckpointPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for QueueFamilyCheckpointPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCheckpointDataNV.html)) · Structure <br/> VkCheckpointDataNV - return structure for command buffer checkpoint data\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CheckpointDataNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\ntypedef struct VkCheckpointDataNV {\n    VkStructureType            sType;\n    void*                      pNext;\n    VkPipelineStageFlagBits    stage;\n    void*                      pCheckpointMarker;\n} VkCheckpointDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stage`] is a `VkPipelineStageFlagBits` value specifying which\n  pipeline stage the checkpoint marker data refers to.\n\n* [`Self::p_checkpoint_marker`] contains the value of the last checkpoint marker\n  executed in the stage that [`Self::stage`] refers to.\n[](#_description)Description\n----------\n\nThe stages at which a checkpoint marker **can** be executed are\nimplementation-defined and **can** be queried by calling[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties2`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkCheckpointDataNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CHECKPOINT_DATA_NV`]\n\n* []() VUID-VkCheckpointDataNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[VkPipelineStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineStageFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_queue_checkpoint_data_nv`]\n"]
#[doc(alias = "VkCheckpointDataNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CheckpointDataNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub stage: crate::vk1_0::PipelineStageFlagBits,
    pub p_checkpoint_marker: *mut std::ffi::c_void,
}
impl CheckpointDataNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::CHECKPOINT_DATA_NV;
}
impl Default for CheckpointDataNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), stage: Default::default(), p_checkpoint_marker: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for CheckpointDataNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CheckpointDataNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("stage", &self.stage).field("p_checkpoint_marker", &self.p_checkpoint_marker).finish()
    }
}
impl CheckpointDataNV {
    #[inline]
    pub fn into_builder<'a>(self) -> CheckpointDataNVBuilder<'a> {
        CheckpointDataNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCheckpointDataNV.html)) · Builder of [`CheckpointDataNV`] <br/> VkCheckpointDataNV - return structure for command buffer checkpoint data\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CheckpointDataNV`] structure is defined as:\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\ntypedef struct VkCheckpointDataNV {\n    VkStructureType            sType;\n    void*                      pNext;\n    VkPipelineStageFlagBits    stage;\n    void*                      pCheckpointMarker;\n} VkCheckpointDataNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::stage`] is a `VkPipelineStageFlagBits` value specifying which\n  pipeline stage the checkpoint marker data refers to.\n\n* [`Self::p_checkpoint_marker`] contains the value of the last checkpoint marker\n  executed in the stage that [`Self::stage`] refers to.\n[](#_description)Description\n----------\n\nThe stages at which a checkpoint marker **can** be executed are\nimplementation-defined and **can** be queried by calling[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties2`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkCheckpointDataNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CHECKPOINT_DATA_NV`]\n\n* []() VUID-VkCheckpointDataNV-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[VkPipelineStageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineStageFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_queue_checkpoint_data_nv`]\n"]
#[repr(transparent)]
pub struct CheckpointDataNVBuilder<'a>(CheckpointDataNV, std::marker::PhantomData<&'a ()>);
impl<'a> CheckpointDataNVBuilder<'a> {
    #[inline]
    pub fn new() -> CheckpointDataNVBuilder<'a> {
        CheckpointDataNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn stage(mut self, stage: crate::vk1_0::PipelineStageFlagBits) -> Self {
        self.0.stage = stage as _;
        self
    }
    #[inline]
    pub fn checkpoint_marker(mut self, checkpoint_marker: *mut std::ffi::c_void) -> Self {
        self.0.p_checkpoint_marker = checkpoint_marker;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CheckpointDataNV {
        self.0
    }
}
impl<'a> std::default::Default for CheckpointDataNVBuilder<'a> {
    fn default() -> CheckpointDataNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CheckpointDataNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CheckpointDataNVBuilder<'a> {
    type Target = CheckpointDataNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CheckpointDataNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_device_diagnostic_checkpoints`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetCheckpointNV.html)) · Function <br/> vkCmdSetCheckpointNV - insert diagnostic checkpoint in command stream\n[](#_c_specification)C Specification\n----------\n\nDevice diagnostic checkpoints are inserted into the command stream by\ncalling [`crate::vk::DeviceLoader::cmd_set_checkpoint_nv`].\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\nvoid vkCmdSetCheckpointNV(\n    VkCommandBuffer                             commandBuffer,\n    const void*                                 pCheckpointMarker);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer that will receive the marker\n\n* [`Self::p_checkpoint_marker`] is an opaque application-provided value that\n  will be associated with the checkpoint.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetCheckpointNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdSetCheckpointNV")]
    pub unsafe fn cmd_set_checkpoint_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, checkpoint_marker: *const std::ffi::c_void) -> () {
        let _function = self.cmd_set_checkpoint_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, checkpoint_marker);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetQueueCheckpointDataNV.html)) · Function <br/> vkGetQueueCheckpointDataNV - retrieve diagnostic checkpoint data\n[](#_c_specification)C Specification\n----------\n\nIf the device encounters an error during execution, the implementation will\nreturn a [`crate::vk::Result::ERROR_DEVICE_LOST`] error to the application at a certain\npoint during host execution.\nWhen this happens, the application **can** call[`crate::vk::DeviceLoader::get_queue_checkpoint_data_nv`] to retrieve information on the most recent\ndiagnostic checkpoints that were executed by the device.\n\n```\n// Provided by VK_NV_device_diagnostic_checkpoints\nvoid vkGetQueueCheckpointDataNV(\n    VkQueue                                     queue,\n    uint32_t*                                   pCheckpointDataCount,\n    VkCheckpointDataNV*                         pCheckpointData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the [`crate::vk::Queue`] object the caller would like to\n  retrieve checkpoint data for\n\n* [`Self::p_checkpoint_data_count`] is a pointer to an integer related to the\n  number of checkpoint markers available or queried, as described below.\n\n* [`Self::p_checkpoint_data`] is either `NULL` or a pointer to an array of[`crate::vk::CheckpointDataNV`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_checkpoint_data`] is `NULL`, then the number of checkpoint markers\navailable is returned in [`Self::p_checkpoint_data_count`].\n\nOtherwise, [`Self::p_checkpoint_data_count`] **must** point to a variable set by the\nuser to the number of elements in the [`Self::p_checkpoint_data`] array, and on\nreturn the variable is overwritten with the number of structures actually\nwritten to [`Self::p_checkpoint_data`].\n\nIf [`Self::p_checkpoint_data_count`] is less than the number of checkpoint markers\navailable, at most [`Self::p_checkpoint_data_count`] structures will be written.\n\nValid Usage\n\n* []() VUID-vkGetQueueCheckpointDataNV-queue-02025  \n   The device that [`Self::queue`] belongs to **must** be in the lost state\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetQueueCheckpointDataNV-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkGetQueueCheckpointDataNV-pCheckpointDataCount-parameter  \n  [`Self::p_checkpoint_data_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetQueueCheckpointDataNV-pCheckpointData-parameter  \n   If the value referenced by [`Self::p_checkpoint_data_count`] is not `0`, and [`Self::p_checkpoint_data`] is not `NULL`, [`Self::p_checkpoint_data`] **must** be a valid pointer to an array of [`Self::p_checkpoint_data_count`] [`crate::vk::CheckpointDataNV`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CheckpointDataNV`], [`crate::vk::Queue`]\n"]
    #[doc(alias = "vkGetQueueCheckpointDataNV")]
    pub unsafe fn get_queue_checkpoint_data_nv(&self, queue: crate::vk1_0::Queue, checkpoint_data_count: Option<u32>) -> crate::SmallVec<crate::extensions::nv_device_diagnostic_checkpoints::CheckpointDataNV> {
        let _function = self.get_queue_checkpoint_data_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut checkpoint_data_count = match checkpoint_data_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(queue as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut checkpoint_data = crate::SmallVec::from_elem(Default::default(), checkpoint_data_count as _);
        let _return = _function(queue as _, &mut checkpoint_data_count, checkpoint_data.as_mut_ptr());
        checkpoint_data
    }
}
