#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISPLAY_SURFACE_COUNTER_SPEC_VERSION")]
pub const EXT_DISPLAY_SURFACE_COUNTER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISPLAY_SURFACE_COUNTER_EXTENSION_NAME")]
pub const EXT_DISPLAY_SURFACE_COUNTER_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_display_surface_counter");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_SURFACE_CAPABILITIES2_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceSurfaceCapabilities2EXT");
#[doc = "Provided by [`crate::extensions::ext_display_surface_counter`]"]
impl crate::vk1_0::StructureType {
    pub const SURFACE_CAPABILITIES_2_EXT: Self = Self(1000090000);
    pub const SURFACE_CAPABILITIES2_EXT: Self = Self::SURFACE_CAPABILITIES_2_EXT;
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagsEXT.html)) · Bitmask of [`SurfaceCounterFlagBitsEXT`] <br/> VkSurfaceCounterFlagsEXT - Bitmask of VkSurfaceCounterFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_display_surface_counter\ntypedef VkFlags VkSurfaceCounterFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::SurfaceCounterFlagBitsEXT`] is a bitmask type for setting a mask of zero\nor more [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SurfaceCapabilities2EXT`], [VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html), [`crate::vk::SwapchainCounterCreateInfoEXT`]\n"] # [doc (alias = "VkSurfaceCounterFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct SurfaceCounterFlagsEXT : u32 { const VBLANK_EXT = SurfaceCounterFlagBitsEXT :: VBLANK_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html)) · Bits enum of [`SurfaceCounterFlagsEXT`] <br/> VkSurfaceCounterFlagBitsEXT - Surface-relative counter types\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::SurfaceCapabilities2EXT::supported_surface_counters`], indicating\nsupported surface counter types, are:\n\n```\n// Provided by VK_EXT_display_surface_counter\ntypedef enum VkSurfaceCounterFlagBitsEXT {\n    VK_SURFACE_COUNTER_VBLANK_BIT_EXT = 0x00000001,\n    VK_SURFACE_COUNTER_VBLANK_EXT = VK_SURFACE_COUNTER_VBLANK_BIT_EXT,\n} VkSurfaceCounterFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::VBLANK_EXT`] specifies a counter incrementing\n  once every time a vertical blanking period occurs on the display\n  associated with the surface.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::SurfaceCounterFlagBitsEXT`], [`crate::vk::DeviceLoader::get_swapchain_counter_ext`]\n"]
#[doc(alias = "VkSurfaceCounterFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct SurfaceCounterFlagBitsEXT(pub u32);
impl SurfaceCounterFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> SurfaceCounterFlagsEXT {
        SurfaceCounterFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for SurfaceCounterFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VBLANK_EXT => "VBLANK_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_surface_counter`]"]
impl crate::extensions::ext_display_surface_counter::SurfaceCounterFlagBitsEXT {
    pub const VBLANK_EXT: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceCapabilities2EXT.html)) · Function <br/> vkGetPhysicalDeviceSurfaceCapabilities2EXT - Query surface capabilities\n[](#_c_specification)C Specification\n----------\n\nTo query the basic capabilities of a surface, needed in order to create a\nswapchain, call:\n\n```\n// Provided by VK_EXT_display_surface_counter\nVkResult vkGetPhysicalDeviceSurfaceCapabilities2EXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkSurfaceKHR                                surface,\n    VkSurfaceCapabilities2EXT*                  pSurfaceCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::surface`] is the surface that will be associated with the swapchain.\n\n* [`Self::p_surface_capabilities`] is a pointer to a[`crate::vk::SurfaceCapabilities2EXT`] structure in which the capabilities are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with the ability to return\nextended information by adding extending structures to the `pNext` chain\nof its [`Self::p_surface_capabilities`] parameter.\n\nValid Usage\n\n* [[VUID-{refpage}-surface-06211]] VUID-{refpage}-surface-06211  \n  [`Self::surface`] **must** be supported by [`Self::physical_device`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an equivalent\n  platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-surface-parameter  \n  [`Self::surface`] **must** be a valid [`crate::vk::SurfaceKHR`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-pSurfaceCapabilities-parameter  \n  [`Self::p_surface_capabilities`] **must** be a valid pointer to a [`crate::vk::SurfaceCapabilities2EXT`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-commonparent  \n   Both of [`Self::physical_device`], and [`Self::surface`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::SurfaceCapabilities2EXT`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceSurfaceCapabilities2EXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, surface: crate::extensions::khr_surface::SurfaceKHR, p_surface_capabilities: *mut crate::extensions::ext_display_surface_counter::SurfaceCapabilities2EXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilities2EXT.html)) · Structure <br/> VkSurfaceCapabilities2EXT - Structure describing capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilities2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_surface_counter\ntypedef struct VkSurfaceCapabilities2EXT {\n    VkStructureType                  sType;\n    void*                            pNext;\n    uint32_t                         minImageCount;\n    uint32_t                         maxImageCount;\n    VkExtent2D                       currentExtent;\n    VkExtent2D                       minImageExtent;\n    VkExtent2D                       maxImageExtent;\n    uint32_t                         maxImageArrayLayers;\n    VkSurfaceTransformFlagsKHR       supportedTransforms;\n    VkSurfaceTransformFlagBitsKHR    currentTransform;\n    VkCompositeAlphaFlagsKHR         supportedCompositeAlpha;\n    VkImageUsageFlags                supportedUsageFlags;\n    VkSurfaceCounterFlagsEXT         supportedSurfaceCounters;\n} VkSurfaceCapabilities2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::min_image_count`] is the minimum number of images the specified device\n  supports for a swapchain created for the surface, and will be at least\n  one.\n\n* [`Self::max_image_count`] is the maximum number of images the specified device\n  supports for a swapchain created for the surface, and will be either 0,\n  or greater than or equal to [`Self::min_image_count`].\n  A value of 0 means that there is no limit on the number of images,\n  though there **may** be limits related to the total amount of memory used\n  by presentable images.\n\n* [`Self::current_extent`] is the current width and height of the surface, or\n  the special value (0xFFFFFFFF, 0xFFFFFFFF) indicating that the\n  surface size will be determined by the extent of a swapchain targeting\n  the surface.\n\n* [`Self::min_image_extent`] contains the smallest valid swapchain extent for\n  the surface on the specified device.\n  The `width` and `height` of the extent will each be less than or\n  equal to the corresponding `width` and `height` of[`Self::current_extent`], unless [`Self::current_extent`] has the special value\n  described above.\n\n* [`Self::max_image_extent`] contains the largest valid swapchain extent for the\n  surface on the specified device.\n  The `width` and `height` of the extent will each be greater than\n  or equal to the corresponding `width` and `height` of[`Self::min_image_extent`].\n  The `width` and `height` of the extent will each be greater than\n  or equal to the corresponding `width` and `height` of[`Self::current_extent`], unless [`Self::current_extent`] has the special value\n  described above.\n\n* [`Self::max_image_array_layers`] is the maximum number of layers presentable\n  images **can** have for a swapchain created for this device and surface,\n  and will be at least one.\n\n* [`Self::supported_transforms`] is a bitmask of[VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) indicating the presentation\n  transforms supported for the surface on the specified device.\n  At least one bit will be set.\n\n* [`Self::current_transform`] is [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  indicating the surface’s current transform relative to the presentation\n  engine’s natural orientation.\n\n* [`Self::supported_composite_alpha`] is a bitmask of[VkCompositeAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCompositeAlphaFlagBitsKHR.html), representing the alpha compositing\n  modes supported by the presentation engine for the surface on the\n  specified device, and at least one bit will be set.\n  Opaque composition **can** be achieved in any alpha compositing mode by\n  either using an image format that has no alpha component, or by ensuring\n  that all pixels in the presentable images have an alpha value of 1.0.\n\n* [`Self::supported_usage_flags`] is a bitmask of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html)representing the ways the application **can** use the presentable images of\n  a swapchain created\n  with [`crate::vk::PresentModeKHR`] set to [`crate::vk::PresentModeKHR::IMMEDIATE_KHR`],[`crate::vk::PresentModeKHR::MAILBOX_KHR`], [`crate::vk::PresentModeKHR::FIFO_KHR`] or[`crate::vk::PresentModeKHR::FIFO_RELAXED_KHR`]for the surface on the specified device.[`crate::vk::ImageUsageFlagBits::COLOR_ATTACHMENT`] **must** be included in the set.\n  Implementations **may** support additional usages.\n\n* [`Self::supported_surface_counters`] is a bitmask of[VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) indicating the supported surface\n  counter types.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSurfaceCapabilities2EXT-supportedSurfaceCounters-01246  \n  [`Self::supported_surface_counters`] **must** not include[`crate::vk::SurfaceCounterFlagBitsEXT::VBLANK_EXT`] unless the surface queried is a[display surface](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#wsi-display-surfaces)\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilities2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_2_EXT`]\n\n* []() VUID-VkSurfaceCapabilities2EXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CompositeAlphaFlagBitsKHR`], [`crate::vk::Extent2D`], [`crate::vk::ImageUsageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::SurfaceCounterFlagBitsEXT`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html), [`crate::vk::SurfaceTransformFlagBitsKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`]\n"]
#[doc(alias = "VkSurfaceCapabilities2EXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SurfaceCapabilities2EXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub min_image_count: u32,
    pub max_image_count: u32,
    pub current_extent: crate::vk1_0::Extent2D,
    pub min_image_extent: crate::vk1_0::Extent2D,
    pub max_image_extent: crate::vk1_0::Extent2D,
    pub max_image_array_layers: u32,
    pub supported_transforms: crate::extensions::khr_surface::SurfaceTransformFlagsKHR,
    pub current_transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR,
    pub supported_composite_alpha: crate::extensions::khr_surface::CompositeAlphaFlagsKHR,
    pub supported_usage_flags: crate::vk1_0::ImageUsageFlags,
    pub supported_surface_counters: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagsEXT,
}
impl SurfaceCapabilities2EXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::SURFACE_CAPABILITIES_2_EXT;
}
impl Default for SurfaceCapabilities2EXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), min_image_count: Default::default(), max_image_count: Default::default(), current_extent: Default::default(), min_image_extent: Default::default(), max_image_extent: Default::default(), max_image_array_layers: Default::default(), supported_transforms: Default::default(), current_transform: Default::default(), supported_composite_alpha: Default::default(), supported_usage_flags: Default::default(), supported_surface_counters: Default::default() }
    }
}
impl std::fmt::Debug for SurfaceCapabilities2EXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SurfaceCapabilities2EXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("min_image_count", &self.min_image_count).field("max_image_count", &self.max_image_count).field("current_extent", &self.current_extent).field("min_image_extent", &self.min_image_extent).field("max_image_extent", &self.max_image_extent).field("max_image_array_layers", &self.max_image_array_layers).field("supported_transforms", &self.supported_transforms).field("current_transform", &self.current_transform).field("supported_composite_alpha", &self.supported_composite_alpha).field("supported_usage_flags", &self.supported_usage_flags).field("supported_surface_counters", &self.supported_surface_counters).finish()
    }
}
impl SurfaceCapabilities2EXT {
    #[inline]
    pub fn into_builder<'a>(self) -> SurfaceCapabilities2EXTBuilder<'a> {
        SurfaceCapabilities2EXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCapabilities2EXT.html)) · Builder of [`SurfaceCapabilities2EXT`] <br/> VkSurfaceCapabilities2EXT - Structure describing capabilities of a surface\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::SurfaceCapabilities2EXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_display_surface_counter\ntypedef struct VkSurfaceCapabilities2EXT {\n    VkStructureType                  sType;\n    void*                            pNext;\n    uint32_t                         minImageCount;\n    uint32_t                         maxImageCount;\n    VkExtent2D                       currentExtent;\n    VkExtent2D                       minImageExtent;\n    VkExtent2D                       maxImageExtent;\n    uint32_t                         maxImageArrayLayers;\n    VkSurfaceTransformFlagsKHR       supportedTransforms;\n    VkSurfaceTransformFlagBitsKHR    currentTransform;\n    VkCompositeAlphaFlagsKHR         supportedCompositeAlpha;\n    VkImageUsageFlags                supportedUsageFlags;\n    VkSurfaceCounterFlagsEXT         supportedSurfaceCounters;\n} VkSurfaceCapabilities2EXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::min_image_count`] is the minimum number of images the specified device\n  supports for a swapchain created for the surface, and will be at least\n  one.\n\n* [`Self::max_image_count`] is the maximum number of images the specified device\n  supports for a swapchain created for the surface, and will be either 0,\n  or greater than or equal to [`Self::min_image_count`].\n  A value of 0 means that there is no limit on the number of images,\n  though there **may** be limits related to the total amount of memory used\n  by presentable images.\n\n* [`Self::current_extent`] is the current width and height of the surface, or\n  the special value (0xFFFFFFFF, 0xFFFFFFFF) indicating that the\n  surface size will be determined by the extent of a swapchain targeting\n  the surface.\n\n* [`Self::min_image_extent`] contains the smallest valid swapchain extent for\n  the surface on the specified device.\n  The `width` and `height` of the extent will each be less than or\n  equal to the corresponding `width` and `height` of[`Self::current_extent`], unless [`Self::current_extent`] has the special value\n  described above.\n\n* [`Self::max_image_extent`] contains the largest valid swapchain extent for the\n  surface on the specified device.\n  The `width` and `height` of the extent will each be greater than\n  or equal to the corresponding `width` and `height` of[`Self::min_image_extent`].\n  The `width` and `height` of the extent will each be greater than\n  or equal to the corresponding `width` and `height` of[`Self::current_extent`], unless [`Self::current_extent`] has the special value\n  described above.\n\n* [`Self::max_image_array_layers`] is the maximum number of layers presentable\n  images **can** have for a swapchain created for this device and surface,\n  and will be at least one.\n\n* [`Self::supported_transforms`] is a bitmask of[VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) indicating the presentation\n  transforms supported for the surface on the specified device.\n  At least one bit will be set.\n\n* [`Self::current_transform`] is [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html) value\n  indicating the surface’s current transform relative to the presentation\n  engine’s natural orientation.\n\n* [`Self::supported_composite_alpha`] is a bitmask of[VkCompositeAlphaFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCompositeAlphaFlagBitsKHR.html), representing the alpha compositing\n  modes supported by the presentation engine for the surface on the\n  specified device, and at least one bit will be set.\n  Opaque composition **can** be achieved in any alpha compositing mode by\n  either using an image format that has no alpha component, or by ensuring\n  that all pixels in the presentable images have an alpha value of 1.0.\n\n* [`Self::supported_usage_flags`] is a bitmask of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html)representing the ways the application **can** use the presentable images of\n  a swapchain created\n  with [`crate::vk::PresentModeKHR`] set to [`crate::vk::PresentModeKHR::IMMEDIATE_KHR`],[`crate::vk::PresentModeKHR::MAILBOX_KHR`], [`crate::vk::PresentModeKHR::FIFO_KHR`] or[`crate::vk::PresentModeKHR::FIFO_RELAXED_KHR`]for the surface on the specified device.[`crate::vk::ImageUsageFlagBits::COLOR_ATTACHMENT`] **must** be included in the set.\n  Implementations **may** support additional usages.\n\n* [`Self::supported_surface_counters`] is a bitmask of[VkSurfaceCounterFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceCounterFlagBitsEXT.html) indicating the supported surface\n  counter types.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkSurfaceCapabilities2EXT-supportedSurfaceCounters-01246  \n  [`Self::supported_surface_counters`] **must** not include[`crate::vk::SurfaceCounterFlagBitsEXT::VBLANK_EXT`] unless the surface queried is a[display surface](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#wsi-display-surfaces)\n\nValid Usage (Implicit)\n\n* []() VUID-VkSurfaceCapabilities2EXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::SURFACE_CAPABILITIES_2_EXT`]\n\n* []() VUID-VkSurfaceCapabilities2EXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CompositeAlphaFlagBitsKHR`], [`crate::vk::Extent2D`], [`crate::vk::ImageUsageFlagBits`], [`crate::vk::StructureType`], [`crate::vk::SurfaceCounterFlagBitsEXT`], [VkSurfaceTransformFlagBitsKHR](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSurfaceTransformFlagBitsKHR.html), [`crate::vk::SurfaceTransformFlagBitsKHR`], [`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`]\n"]
#[repr(transparent)]
pub struct SurfaceCapabilities2EXTBuilder<'a>(SurfaceCapabilities2EXT, std::marker::PhantomData<&'a ()>);
impl<'a> SurfaceCapabilities2EXTBuilder<'a> {
    #[inline]
    pub fn new() -> SurfaceCapabilities2EXTBuilder<'a> {
        SurfaceCapabilities2EXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn min_image_count(mut self, min_image_count: u32) -> Self {
        self.0.min_image_count = min_image_count as _;
        self
    }
    #[inline]
    pub fn max_image_count(mut self, max_image_count: u32) -> Self {
        self.0.max_image_count = max_image_count as _;
        self
    }
    #[inline]
    pub fn current_extent(mut self, current_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.current_extent = current_extent as _;
        self
    }
    #[inline]
    pub fn min_image_extent(mut self, min_image_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.min_image_extent = min_image_extent as _;
        self
    }
    #[inline]
    pub fn max_image_extent(mut self, max_image_extent: crate::vk1_0::Extent2D) -> Self {
        self.0.max_image_extent = max_image_extent as _;
        self
    }
    #[inline]
    pub fn max_image_array_layers(mut self, max_image_array_layers: u32) -> Self {
        self.0.max_image_array_layers = max_image_array_layers as _;
        self
    }
    #[inline]
    pub fn supported_transforms(mut self, supported_transforms: crate::extensions::khr_surface::SurfaceTransformFlagsKHR) -> Self {
        self.0.supported_transforms = supported_transforms as _;
        self
    }
    #[inline]
    pub fn current_transform(mut self, current_transform: crate::extensions::khr_surface::SurfaceTransformFlagBitsKHR) -> Self {
        self.0.current_transform = current_transform as _;
        self
    }
    #[inline]
    pub fn supported_composite_alpha(mut self, supported_composite_alpha: crate::extensions::khr_surface::CompositeAlphaFlagsKHR) -> Self {
        self.0.supported_composite_alpha = supported_composite_alpha as _;
        self
    }
    #[inline]
    pub fn supported_usage_flags(mut self, supported_usage_flags: crate::vk1_0::ImageUsageFlags) -> Self {
        self.0.supported_usage_flags = supported_usage_flags as _;
        self
    }
    #[inline]
    pub fn supported_surface_counters(mut self, supported_surface_counters: crate::extensions::ext_display_surface_counter::SurfaceCounterFlagsEXT) -> Self {
        self.0.supported_surface_counters = supported_surface_counters as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> SurfaceCapabilities2EXT {
        self.0
    }
}
impl<'a> std::default::Default for SurfaceCapabilities2EXTBuilder<'a> {
    fn default() -> SurfaceCapabilities2EXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for SurfaceCapabilities2EXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for SurfaceCapabilities2EXTBuilder<'a> {
    type Target = SurfaceCapabilities2EXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for SurfaceCapabilities2EXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_display_surface_counter`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceSurfaceCapabilities2EXT.html)) · Function <br/> vkGetPhysicalDeviceSurfaceCapabilities2EXT - Query surface capabilities\n[](#_c_specification)C Specification\n----------\n\nTo query the basic capabilities of a surface, needed in order to create a\nswapchain, call:\n\n```\n// Provided by VK_EXT_display_surface_counter\nVkResult vkGetPhysicalDeviceSurfaceCapabilities2EXT(\n    VkPhysicalDevice                            physicalDevice,\n    VkSurfaceKHR                                surface,\n    VkSurfaceCapabilities2EXT*                  pSurfaceCapabilities);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device that will be associated with\n  the swapchain to be created, as described for[`crate::vk::DeviceLoader::create_swapchain_khr`].\n\n* [`Self::surface`] is the surface that will be associated with the swapchain.\n\n* [`Self::p_surface_capabilities`] is a pointer to a[`crate::vk::SurfaceCapabilities2EXT`] structure in which the capabilities are\n  returned.\n[](#_description)Description\n----------\n\n[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities2_ext`] behaves similarly to[`crate::vk::InstanceLoader::get_physical_device_surface_capabilities_khr`], with the ability to return\nextended information by adding extending structures to the `pNext` chain\nof its [`Self::p_surface_capabilities`] parameter.\n\nValid Usage\n\n* [[VUID-{refpage}-surface-06211]] VUID-{refpage}-surface-06211  \n  [`Self::surface`] **must** be supported by [`Self::physical_device`], as reported by[`crate::vk::InstanceLoader::get_physical_device_surface_support_khr`] or an equivalent\n  platform-specific mechanism\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-surface-parameter  \n  [`Self::surface`] **must** be a valid [`crate::vk::SurfaceKHR`] handle\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-pSurfaceCapabilities-parameter  \n  [`Self::p_surface_capabilities`] **must** be a valid pointer to a [`crate::vk::SurfaceCapabilities2EXT`] structure\n\n* []() VUID-vkGetPhysicalDeviceSurfaceCapabilities2EXT-commonparent  \n   Both of [`Self::physical_device`], and [`Self::surface`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::SurfaceCapabilities2EXT`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceSurfaceCapabilities2EXT")]
    pub unsafe fn get_physical_device_surface_capabilities2_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, surface: crate::extensions::khr_surface::SurfaceKHR, surface_capabilities: Option<crate::extensions::ext_display_surface_counter::SurfaceCapabilities2EXT>) -> crate::utils::VulkanResult<crate::extensions::ext_display_surface_counter::SurfaceCapabilities2EXT> {
        let _function = self.get_physical_device_surface_capabilities2_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface_capabilities = match surface_capabilities {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(physical_device as _, surface as _, &mut surface_capabilities);
        crate::utils::VulkanResult::new(_return, {
            surface_capabilities.p_next = std::ptr::null_mut() as _;
            surface_capabilities
        })
    }
}
