#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PRESENT_ID_SPEC_VERSION")]
pub const KHR_PRESENT_ID_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_PRESENT_ID_EXTENSION_NAME")]
pub const KHR_PRESENT_ID_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_present_id");
#[doc = "Provided by [`crate::extensions::khr_present_id`]"]
impl crate::vk1_0::StructureType {
    pub const PRESENT_ID_KHR: Self = Self(1000294000);
    pub const PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR: Self = Self(1000294001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePresentIdFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDevicePresentIdFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PresentIdKHR> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PresentIdKHRBuilder<'_>> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePresentIdFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDevicePresentIdFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePresentIdFeaturesKHR.html)) · Structure <br/> VkPhysicalDevicePresentIdFeaturesKHR - Structure indicating support for present id\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_id\ntypedef struct VkPhysicalDevicePresentIdFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           presentId;\n} VkPhysicalDevicePresentIdFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* []() [`Self::present_id`] indicates that the implementation\n  supports specifying present ID values in the [`crate::vk::PresentIdKHR`]extension to the [`crate::vk::PresentInfoKHR`] struct.\n\nIf the [`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePresentIdFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDevicePresentIdFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDevicePresentIdFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub present_id: crate::vk1_0::Bool32,
}
impl PhysicalDevicePresentIdFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR;
}
impl Default for PhysicalDevicePresentIdFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), present_id: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDevicePresentIdFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDevicePresentIdFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("present_id", &(self.present_id != 0)).finish()
    }
}
impl PhysicalDevicePresentIdFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
        PhysicalDevicePresentIdFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDevicePresentIdFeaturesKHR.html)) · Builder of [`PhysicalDevicePresentIdFeaturesKHR`] <br/> VkPhysicalDevicePresentIdFeaturesKHR - Structure indicating support for present id\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_id\ntypedef struct VkPhysicalDevicePresentIdFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           presentId;\n} VkPhysicalDevicePresentIdFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* []() [`Self::present_id`] indicates that the implementation\n  supports specifying present ID values in the [`crate::vk::PresentIdKHR`]extension to the [`crate::vk::PresentInfoKHR`] struct.\n\nIf the [`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDevicePresentIdFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDevicePresentIdFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDevicePresentIdFeaturesKHRBuilder<'a>(PhysicalDevicePresentIdFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
        PhysicalDevicePresentIdFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn present_id(mut self, present_id: bool) -> Self {
        self.0.present_id = present_id as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDevicePresentIdFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
    type Target = PhysicalDevicePresentIdFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDevicePresentIdFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentIdKHR.html)) · Structure <br/> VkPresentIdKHR - The list of presentation identifiers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PresentIdKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_id\ntypedef struct VkPresentIdKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           swapchainCount;\n    const uint64_t*    pPresentIds;\n} VkPresentIdKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to the[`crate::vk::DeviceLoader::queue_present_khr`] command.\n\n* [`Self::p_present_ids`] is `NULL` or a pointer to an array of uint64\\_t with[`Self::swapchain_count`] entries.\n  If not `NULL`, each non-zero value in [`Self::p_present_ids`] specifies the\n  present id to be associated with the presentation of the swapchain with\n  the same index in the [`crate::vk::DeviceLoader::queue_present_khr`] call.\n[](#_description)Description\n----------\n\nFor applications to be able to reference specific presentation events queued\nby a call to [`crate::vk::DeviceLoader::queue_present_khr`], an identifier needs to be associated\nwith them.\nWhen the [`presentId`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-presentId) feature is enabled,\napplications **can** include the [`crate::vk::PresentIdKHR`] structure in the[`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure to supply\nidentifiers.\n\nEach [`crate::vk::SwapchainKHR`] has a presentId associated with it.\nThis value is initially set to zero when the [`crate::vk::SwapchainKHR`] is\ncreated.\n\nWhen a [`crate::vk::PresentIdKHR`] structure with a non-NULL [`Self::p_present_ids`] is\nincluded in the [`Self::p_next`] chain of a [`crate::vk::PresentInfoKHR`] structure,\neach `pSwapchains` entry has a presentId associated in the[`Self::p_present_ids`] array at the same index as the swapchain in the`pSwapchains` array.\nIf this presentId is non-zero, then the application **can** later use this\nvalue to refer to that image presentation.\nA value of zero indicates that this presentation has no associated\npresentId.\nA non-zero presentId **must** be greater than any non-zero presentId passed\npreviously by the application for the same swapchain.\n\nThere is no requirement for any precise timing relationship between the\npresentation of the image to the user and the update of the presentId value,\nbut implementations **should** make this as close as possible to the\npresentation of the first pixel in the new image to the user.\n\nValid Usage\n\n* []() VUID-VkPresentIdKHR-swapchainCount-04998  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where this[`crate::vk::PresentIdKHR`] is in the pNext-chain of the [`crate::vk::PresentInfoKHR`]structure\n\n* []() VUID-VkPresentIdKHR-presentIds-04999  \n   Each `presentIds` entry **must** be greater than any previous`presentIds` entry passed for the associated `pSwapchains` entry\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentIdKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_ID_KHR`]\n\n* []() VUID-VkPresentIdKHR-pPresentIds-parameter  \n   If [`Self::p_present_ids`] is not `NULL`, [`Self::p_present_ids`] **must** be a valid pointer to an array of [`Self::swapchain_count`] `uint64_t` values\n\n* []() VUID-VkPresentIdKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPresentIdKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentIdKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub swapchain_count: u32,
    pub p_present_ids: *const u64,
}
impl PresentIdKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PRESENT_ID_KHR;
}
impl Default for PresentIdKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), swapchain_count: Default::default(), p_present_ids: std::ptr::null() }
    }
}
impl std::fmt::Debug for PresentIdKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentIdKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("swapchain_count", &self.swapchain_count).field("p_present_ids", &self.p_present_ids).finish()
    }
}
impl PresentIdKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentIdKHRBuilder<'a> {
        PresentIdKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentIdKHR.html)) · Builder of [`PresentIdKHR`] <br/> VkPresentIdKHR - The list of presentation identifiers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PresentIdKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_present_id\ntypedef struct VkPresentIdKHR {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           swapchainCount;\n    const uint64_t*    pPresentIds;\n} VkPresentIdKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to the[`crate::vk::DeviceLoader::queue_present_khr`] command.\n\n* [`Self::p_present_ids`] is `NULL` or a pointer to an array of uint64\\_t with[`Self::swapchain_count`] entries.\n  If not `NULL`, each non-zero value in [`Self::p_present_ids`] specifies the\n  present id to be associated with the presentation of the swapchain with\n  the same index in the [`crate::vk::DeviceLoader::queue_present_khr`] call.\n[](#_description)Description\n----------\n\nFor applications to be able to reference specific presentation events queued\nby a call to [`crate::vk::DeviceLoader::queue_present_khr`], an identifier needs to be associated\nwith them.\nWhen the [`presentId`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-presentId) feature is enabled,\napplications **can** include the [`crate::vk::PresentIdKHR`] structure in the[`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure to supply\nidentifiers.\n\nEach [`crate::vk::SwapchainKHR`] has a presentId associated with it.\nThis value is initially set to zero when the [`crate::vk::SwapchainKHR`] is\ncreated.\n\nWhen a [`crate::vk::PresentIdKHR`] structure with a non-NULL [`Self::p_present_ids`] is\nincluded in the [`Self::p_next`] chain of a [`crate::vk::PresentInfoKHR`] structure,\neach `pSwapchains` entry has a presentId associated in the[`Self::p_present_ids`] array at the same index as the swapchain in the`pSwapchains` array.\nIf this presentId is non-zero, then the application **can** later use this\nvalue to refer to that image presentation.\nA value of zero indicates that this presentation has no associated\npresentId.\nA non-zero presentId **must** be greater than any non-zero presentId passed\npreviously by the application for the same swapchain.\n\nThere is no requirement for any precise timing relationship between the\npresentation of the image to the user and the update of the presentId value,\nbut implementations **should** make this as close as possible to the\npresentation of the first pixel in the new image to the user.\n\nValid Usage\n\n* []() VUID-VkPresentIdKHR-swapchainCount-04998  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where this[`crate::vk::PresentIdKHR`] is in the pNext-chain of the [`crate::vk::PresentInfoKHR`]structure\n\n* []() VUID-VkPresentIdKHR-presentIds-04999  \n   Each `presentIds` entry **must** be greater than any previous`presentIds` entry passed for the associated `pSwapchains` entry\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentIdKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_ID_KHR`]\n\n* []() VUID-VkPresentIdKHR-pPresentIds-parameter  \n   If [`Self::p_present_ids`] is not `NULL`, [`Self::p_present_ids`] **must** be a valid pointer to an array of [`Self::swapchain_count`] `uint64_t` values\n\n* []() VUID-VkPresentIdKHR-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PresentIdKHRBuilder<'a>(PresentIdKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PresentIdKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PresentIdKHRBuilder<'a> {
        PresentIdKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn present_ids(mut self, present_ids: &'a [u64]) -> Self {
        self.0.p_present_ids = present_ids.as_ptr() as _;
        self.0.swapchain_count = present_ids.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentIdKHR {
        self.0
    }
}
impl<'a> std::default::Default for PresentIdKHRBuilder<'a> {
    fn default() -> PresentIdKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentIdKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentIdKHRBuilder<'a> {
    type Target = PresentIdKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentIdKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
