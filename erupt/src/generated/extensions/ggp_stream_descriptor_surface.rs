#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GGP_STREAM_DESCRIPTOR_SURFACE_SPEC_VERSION")]
pub const GGP_STREAM_DESCRIPTOR_SURFACE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GGP_STREAM_DESCRIPTOR_SURFACE_EXTENSION_NAME")]
pub const GGP_STREAM_DESCRIPTOR_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_GGP_stream_descriptor_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_STREAM_DESCRIPTOR_SURFACE_GGP: *const std::os::raw::c_char = crate::cstr!("vkCreateStreamDescriptorSurfaceGGP");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkStreamDescriptorSurfaceCreateFlagsGGP.html)) · Bitmask of [`StreamDescriptorSurfaceCreateFlagBitsGGP`] <br/> VkStreamDescriptorSurfaceCreateFlagsGGP - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_GGP_stream_descriptor_surface\ntypedef VkFlags VkStreamDescriptorSurfaceCreateFlagsGGP;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::StreamDescriptorSurfaceCreateFlagBitsGGP`] is a bitmask type for setting\na mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`]\n"] # [doc (alias = "VkStreamDescriptorSurfaceCreateFlagsGGP")] # [derive (Default)] # [repr (transparent)] pub struct StreamDescriptorSurfaceCreateFlagsGGP : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`StreamDescriptorSurfaceCreateFlagsGGP`] <br/> "]
#[doc(alias = "VkStreamDescriptorSurfaceCreateFlagBitsGGP")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct StreamDescriptorSurfaceCreateFlagBitsGGP(pub u32);
impl StreamDescriptorSurfaceCreateFlagBitsGGP {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> StreamDescriptorSurfaceCreateFlagsGGP {
        StreamDescriptorSurfaceCreateFlagsGGP::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for StreamDescriptorSurfaceCreateFlagBitsGGP {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ggp_stream_descriptor_surface`]"]
impl crate::vk1_0::StructureType {
    pub const STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP: Self = Self(1000049000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateStreamDescriptorSurfaceGGP.html)) · Function <br/> vkCreateStreamDescriptorSurfaceGGP - Create a slink:VkSurfaceKHR object for a Google Games Platform stream\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Google Games Platform stream\ndescriptor, call:\n\n```\n// Provided by VK_GGP_stream_descriptor_surface\nVkResult vkCreateStreamDescriptorSurfaceGGP(\n    VkInstance                                  instance,\n    const VkStreamDescriptorSurfaceCreateInfoGGP* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate with the surface.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure containing\n  parameters that affect the creation of the surface object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateStreamDescriptorSurfaceGGP = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::ggp_stream_descriptor_surface::StreamDescriptorSurfaceCreateInfoGGP, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkStreamDescriptorSurfaceCreateInfoGGP.html)) · Structure <br/> VkStreamDescriptorSurfaceCreateInfoGGP - Structure specifying parameters of a newly created Google Games Platform stream surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure is defined as:\n\n```\n// Provided by VK_GGP_stream_descriptor_surface\ntypedef struct VkStreamDescriptorSurfaceCreateInfoGGP {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    VkStreamDescriptorSurfaceCreateFlagsGGP    flags;\n    GgpStreamDescriptor                        streamDescriptor;\n} VkStreamDescriptorSurfaceCreateInfoGGP;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::stream_descriptor`] is a `GgpStreamDescriptor` referring to the\n  GGP stream descriptor to associate with the surface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-streamDescriptor-02681  \n  [`Self::stream_descriptor`] **must** be a valid `GgpStreamDescriptor`\n\nValid Usage (Implicit)\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP`]\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StreamDescriptorSurfaceCreateFlagBitsGGP`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_stream_descriptor_surface_ggp`]\n"]
#[doc(alias = "VkStreamDescriptorSurfaceCreateInfoGGP")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct StreamDescriptorSurfaceCreateInfoGGP {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ggp_stream_descriptor_surface::StreamDescriptorSurfaceCreateFlagsGGP,
    pub stream_descriptor: u32,
}
impl StreamDescriptorSurfaceCreateInfoGGP {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP;
}
impl Default for StreamDescriptorSurfaceCreateInfoGGP {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), stream_descriptor: Default::default() }
    }
}
impl std::fmt::Debug for StreamDescriptorSurfaceCreateInfoGGP {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("StreamDescriptorSurfaceCreateInfoGGP").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("stream_descriptor", &self.stream_descriptor).finish()
    }
}
impl StreamDescriptorSurfaceCreateInfoGGP {
    #[inline]
    pub fn into_builder<'a>(self) -> StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
        StreamDescriptorSurfaceCreateInfoGGPBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkStreamDescriptorSurfaceCreateInfoGGP.html)) · Builder of [`StreamDescriptorSurfaceCreateInfoGGP`] <br/> VkStreamDescriptorSurfaceCreateInfoGGP - Structure specifying parameters of a newly created Google Games Platform stream surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure is defined as:\n\n```\n// Provided by VK_GGP_stream_descriptor_surface\ntypedef struct VkStreamDescriptorSurfaceCreateInfoGGP {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    VkStreamDescriptorSurfaceCreateFlagsGGP    flags;\n    GgpStreamDescriptor                        streamDescriptor;\n} VkStreamDescriptorSurfaceCreateInfoGGP;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::stream_descriptor`] is a `GgpStreamDescriptor` referring to the\n  GGP stream descriptor to associate with the surface.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-streamDescriptor-02681  \n  [`Self::stream_descriptor`] **must** be a valid `GgpStreamDescriptor`\n\nValid Usage (Implicit)\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP`]\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkStreamDescriptorSurfaceCreateInfoGGP-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StreamDescriptorSurfaceCreateFlagBitsGGP`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_stream_descriptor_surface_ggp`]\n"]
#[repr(transparent)]
pub struct StreamDescriptorSurfaceCreateInfoGGPBuilder<'a>(StreamDescriptorSurfaceCreateInfoGGP, std::marker::PhantomData<&'a ()>);
impl<'a> StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
    #[inline]
    pub fn new() -> StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
        StreamDescriptorSurfaceCreateInfoGGPBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ggp_stream_descriptor_surface::StreamDescriptorSurfaceCreateFlagsGGP) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn stream_descriptor(mut self, stream_descriptor: u32) -> Self {
        self.0.stream_descriptor = stream_descriptor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> StreamDescriptorSurfaceCreateInfoGGP {
        self.0
    }
}
impl<'a> std::default::Default for StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
    fn default() -> StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
    type Target = StreamDescriptorSurfaceCreateInfoGGP;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for StreamDescriptorSurfaceCreateInfoGGPBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ggp_stream_descriptor_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateStreamDescriptorSurfaceGGP.html)) · Function <br/> vkCreateStreamDescriptorSurfaceGGP - Create a slink:VkSurfaceKHR object for a Google Games Platform stream\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Google Games Platform stream\ndescriptor, call:\n\n```\n// Provided by VK_GGP_stream_descriptor_surface\nVkResult vkCreateStreamDescriptorSurfaceGGP(\n    VkInstance                                  instance,\n    const VkStreamDescriptorSurfaceCreateInfoGGP* pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate with the surface.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure containing\n  parameters that affect the creation of the surface object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`] structure\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateStreamDescriptorSurfaceGGP-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::StreamDescriptorSurfaceCreateInfoGGP`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateStreamDescriptorSurfaceGGP")]
    pub unsafe fn create_stream_descriptor_surface_ggp(&self, create_info: &crate::extensions::ggp_stream_descriptor_surface::StreamDescriptorSurfaceCreateInfoGGP, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_stream_descriptor_surface_ggp.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
