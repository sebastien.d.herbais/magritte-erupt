#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_REPORT_SPEC_VERSION")]
pub const EXT_DEBUG_REPORT_SPEC_VERSION: u32 = 10;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_REPORT_EXTENSION_NAME")]
pub const EXT_DEBUG_REPORT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_debug_report");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_DEBUG_REPORT_CALLBACK_EXT: *const std::os::raw::c_char = crate::cstr!("vkCreateDebugReportCallbackEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DESTROY_DEBUG_REPORT_CALLBACK_EXT: *const std::os::raw::c_char = crate::cstr!("vkDestroyDebugReportCallbackEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DEBUG_REPORT_MESSAGE_EXT: *const std::os::raw::c_char = crate::cstr!("vkDebugReportMessageEXT");
crate::non_dispatchable_handle!(DebugReportCallbackEXT, DEBUG_REPORT_CALLBACK_EXT, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportCallbackEXT.html)) · Non-dispatchable Handle <br/> VkDebugReportCallbackEXT - Opaque handle to a debug report callback object\n[](#_c_specification)C Specification\n----------\n\nDebug report callbacks are represented by [`crate::vk::DebugReportCallbackEXT`]handles:\n\n```\n// Provided by VK_EXT_debug_report\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkDebugReportCallbackEXT)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::InstanceLoader::create_debug_report_callback_ext`], [`crate::vk::InstanceLoader::destroy_debug_report_callback_ext`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkDebugReportCallbackEXT)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkDebugReportCallbackEXT");
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::vk1_0::Result {
    pub const ERROR_VALIDATION_FAILED_EXT: Self = Self(-1000011001);
}
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::vk1_0::StructureType {
    pub const DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT: Self = Self(1000011000);
    pub const DEBUG_REPORT_CREATE_INFO_EXT: Self = Self::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
}
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::vk1_0::ObjectType {
    pub const DEBUG_REPORT_CALLBACK_EXT: Self = Self(1000011000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagsEXT.html)) · Bitmask of [`DebugReportFlagBitsEXT`] <br/> VkDebugReportFlagsEXT - Bitmask of VkDebugReportFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_debug_report\ntypedef VkFlags VkDebugReportFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::DebugReportFlagBitsEXT`] is a bitmask type for setting a mask of zero or\nmore [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportCallbackCreateInfoEXT`], [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html), [`crate::vk::InstanceLoader::debug_report_message_ext`]\n"] # [doc (alias = "VkDebugReportFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct DebugReportFlagsEXT : u32 { const INFORMATION_EXT = DebugReportFlagBitsEXT :: INFORMATION_EXT . 0 ; const WARNING_EXT = DebugReportFlagBitsEXT :: WARNING_EXT . 0 ; const PERFORMANCE_WARNING_EXT = DebugReportFlagBitsEXT :: PERFORMANCE_WARNING_EXT . 0 ; const ERROR_EXT = DebugReportFlagBitsEXT :: ERROR_EXT . 0 ; const DEBUG_EXT = DebugReportFlagBitsEXT :: DEBUG_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html)) · Bits enum of [`DebugReportFlagsEXT`] <br/> VkDebugReportFlagBitsEXT - Bitmask specifying events which cause a debug report callback\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::DebugReportCallbackCreateInfoEXT::flags`], specifying events\nwhich cause a debug report, are:\n\n```\n// Provided by VK_EXT_debug_report\ntypedef enum VkDebugReportFlagBitsEXT {\n    VK_DEBUG_REPORT_INFORMATION_BIT_EXT = 0x00000001,\n    VK_DEBUG_REPORT_WARNING_BIT_EXT = 0x00000002,\n    VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT = 0x00000004,\n    VK_DEBUG_REPORT_ERROR_BIT_EXT = 0x00000008,\n    VK_DEBUG_REPORT_DEBUG_BIT_EXT = 0x00000010,\n} VkDebugReportFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::ERROR_EXT`] specifies that the application has\n  violated a valid usage condition of the specification.\n\n* [`Self::WARNING_EXT`] specifies use of Vulkan that **may**expose an app bug.\n  Such cases may not be immediately harmful, such as a fragment shader\n  outputting to a location with no attachment.\n  Other cases **may** point to behavior that is almost certainly bad when\n  unintended such as using an image whose memory has not been filled.\n  In general if you see a warning but you know that the behavior is\n  intended/desired, then simply ignore the warning.\n\n* [`Self::PERFORMANCE_WARNING_EXT`] specifies a\n  potentially non-optimal use of Vulkan, e.g. using[`crate::vk::PFN_vkCmdClearColorImage`] when setting[`crate::vk::AttachmentDescription::load_op`] to[`crate::vk::AttachmentLoadOp::CLEAR`] would have worked.\n\n* [`Self::INFORMATION_EXT`] specifies an informational\n  message such as resource details that may be handy when debugging an\n  application.\n\n* [`Self::DEBUG_EXT`] specifies diagnostic information\n  from the implementation and layers.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportFlagBitsEXT`]\n"]
#[doc(alias = "VkDebugReportFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugReportFlagBitsEXT(pub u32);
impl DebugReportFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> DebugReportFlagsEXT {
        DebugReportFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for DebugReportFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INFORMATION_EXT => "INFORMATION_EXT",
            &Self::WARNING_EXT => "WARNING_EXT",
            &Self::PERFORMANCE_WARNING_EXT => "PERFORMANCE_WARNING_EXT",
            &Self::ERROR_EXT => "ERROR_EXT",
            &Self::DEBUG_EXT => "DEBUG_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::extensions::ext_debug_report::DebugReportFlagBitsEXT {
    pub const INFORMATION_EXT: Self = Self(1);
    pub const WARNING_EXT: Self = Self(2);
    pub const PERFORMANCE_WARNING_EXT: Self = Self(4);
    pub const ERROR_EXT: Self = Self(8);
    pub const DEBUG_EXT: Self = Self(16);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportObjectTypeEXT.html)) · Enum <br/> VkDebugReportObjectTypeEXT - Specify the type of an object handle\n[](#_c_specification)C Specification\n----------\n\nPossible values passed to the `objectType` parameter of the callback\nfunction specified by[`crate::vk::DebugReportCallbackCreateInfoEXT::pfn_callback`], specifying the\ntype of object handle being reported, are:\n\n```\n// Provided by VK_EXT_debug_report, VK_EXT_debug_marker\ntypedef enum VkDebugReportObjectTypeEXT {\n    VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT = 0,\n    VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT = 1,\n    VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT = 2,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT = 3,\n    VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT = 4,\n    VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT = 5,\n    VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT = 6,\n    VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT = 7,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT = 8,\n    VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT = 9,\n    VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT = 10,\n    VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT = 11,\n    VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT = 12,\n    VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT = 13,\n    VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT = 14,\n    VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT = 15,\n    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT = 16,\n    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT = 17,\n    VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT = 18,\n    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT = 19,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT = 20,\n    VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT = 21,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT = 22,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT = 23,\n    VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT = 24,\n    VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT = 25,\n    VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT = 26,\n    VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT = 27,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT = 28,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT = 29,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT = 30,\n    VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT = 33,\n  // Provided by VK_KHR_sampler_ycbcr_conversion with VK_EXT_debug_report, VK_EXT_debug_report with VK_VERSION_1_1\n    VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT = 1000156000,\n  // Provided by VK_EXT_debug_report with VK_VERSION_1_1\n    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT = 1000085000,\n  // Provided by VK_NVX_binary_import\n    VK_DEBUG_REPORT_OBJECT_TYPE_CU_MODULE_NVX_EXT = 1000029000,\n  // Provided by VK_NVX_binary_import\n    VK_DEBUG_REPORT_OBJECT_TYPE_CU_FUNCTION_NVX_EXT = 1000029001,\n  // Provided by VK_KHR_acceleration_structure\n    VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT = 1000150000,\n  // Provided by VK_NV_ray_tracing\n    VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT = 1000165000,\n    VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT = VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT,\n    VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT = VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT,\n  // Provided by VK_KHR_descriptor_update_template with VK_EXT_debug_report\n    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR_EXT = VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT,\n  // Provided by VK_KHR_sampler_ycbcr_conversion\n    VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR_EXT = VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT,\n} VkDebugReportObjectTypeEXT;\n```\n[](#_description)Description\n----------\n\n|[`crate::vk::DebugReportObjectTypeEXT`]|                     Vulkan Handle Type                      |\n|-------------------------------------------------------------|-------------------------------------------------------------|\n|          [`Self::UNKNOWN_EXT`]          |                  Unknown/Undefined Handle                   |\n|         [`Self::INSTANCE_EXT`]          |                [`crate::vk::Instance`]                |\n|      [`Self::PHYSICAL_DEVICE_EXT`]      |          [`crate::vk::PhysicalDevice`]          |\n|          [`Self::DEVICE_EXT`]           |                  [`crate::vk::Device`]                  |\n|           [`Self::QUEUE_EXT`]           |                   [`crate::vk::Queue`]                   |\n|         [`Self::SEMAPHORE_EXT`]         |               [`crate::vk::Semaphore`]               |\n|      [`Self::COMMAND_BUFFER_EXT`]       |           [`crate::vk::CommandBuffer`]           |\n|           [`Self::FENCE_EXT`]           |                   [`crate::vk::Fence`]                   |\n|       [`Self::DEVICE_MEMORY_EXT`]       |            [`crate::vk::DeviceMemory`]            |\n|          [`Self::BUFFER_EXT`]           |                  [`crate::vk::Buffer`]                  |\n|           [`Self::IMAGE_EXT`]           |                   [`crate::vk::Image`]                   |\n|           [`Self::EVENT_EXT`]           |                   [`crate::vk::Event`]                   |\n|        [`Self::QUERY_POOL_EXT`]         |               [`crate::vk::QueryPool`]               |\n|        [`Self::BUFFER_VIEW_EXT`]        |              [`crate::vk::BufferView`]              |\n|        [`Self::IMAGE_VIEW_EXT`]         |               [`crate::vk::ImageView`]               |\n|       [`Self::SHADER_MODULE_EXT`]       |            [`crate::vk::ShaderModule`]            |\n|      [`Self::PIPELINE_CACHE_EXT`]       |           [`crate::vk::PipelineCache`]           |\n|      [`Self::PIPELINE_LAYOUT_EXT`]      |          [`crate::vk::PipelineLayout`]          |\n|        [`Self::RENDER_PASS_EXT`]        |              [`crate::vk::RenderPass`]              |\n|         [`Self::PIPELINE_EXT`]          |                [`crate::vk::Pipeline`]                |\n|   [`Self::DESCRIPTOR_SET_LAYOUT_EXT`]   |     [`crate::vk::DescriptorSetLayout`]     |\n|          [`Self::SAMPLER_EXT`]          |                 [`crate::vk::Sampler`]                 |\n|      [`Self::DESCRIPTOR_POOL_EXT`]      |          [`crate::vk::DescriptorPool`]          |\n|      [`Self::DESCRIPTOR_SET_EXT`]       |           [`crate::vk::DescriptorSet`]           |\n|        [`Self::FRAMEBUFFER_EXT`]        |             [`crate::vk::Framebuffer`]             |\n|       [`Self::COMMAND_POOL_EXT`]        |             [`crate::vk::CommandPool`]             |\n|        [`Self::SURFACE_KHR_EXT`]        |              [`crate::vk::SurfaceKHR`]              |\n|       [`Self::SWAPCHAIN_KHR_EXT`]       |            [`crate::vk::SwapchainKHR`]            |\n| [`Self::DEBUG_REPORT_CALLBACK_EXT_EXT`] |  [`crate::vk::DebugReportCallbackEXT`]  |\n|        [`Self::DISPLAY_KHR_EXT`]        |              [`crate::vk::DisplayKHR`]              |\n|     [`Self::DISPLAY_MODE_KHR_EXT`]      |          [`crate::vk::DisplayModeKHR`]          |\n|[`Self::DESCRIPTOR_UPDATE_TEMPLATE_EXT`] |[`crate::vk::DescriptorUpdateTemplate`]|\n\n|   |Note<br/><br/>The primary expected use of [`crate::vk::Result::ERROR_VALIDATION_FAILED_EXT`] is for<br/>validation layer testing.<br/>It is not expected that an application would see this error code during<br/>normal use of the validation layers.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugMarkerObjectNameInfoEXT`], [`crate::vk::DebugMarkerObjectTagInfoEXT`], [`crate::vk::InstanceLoader::debug_report_message_ext`]\n"]
#[doc(alias = "VkDebugReportObjectTypeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DebugReportObjectTypeEXT(pub i32);
impl std::fmt::Debug for DebugReportObjectTypeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::UNKNOWN_EXT => "UNKNOWN_EXT",
            &Self::INSTANCE_EXT => "INSTANCE_EXT",
            &Self::PHYSICAL_DEVICE_EXT => "PHYSICAL_DEVICE_EXT",
            &Self::DEVICE_EXT => "DEVICE_EXT",
            &Self::QUEUE_EXT => "QUEUE_EXT",
            &Self::SEMAPHORE_EXT => "SEMAPHORE_EXT",
            &Self::COMMAND_BUFFER_EXT => "COMMAND_BUFFER_EXT",
            &Self::FENCE_EXT => "FENCE_EXT",
            &Self::DEVICE_MEMORY_EXT => "DEVICE_MEMORY_EXT",
            &Self::BUFFER_EXT => "BUFFER_EXT",
            &Self::IMAGE_EXT => "IMAGE_EXT",
            &Self::EVENT_EXT => "EVENT_EXT",
            &Self::QUERY_POOL_EXT => "QUERY_POOL_EXT",
            &Self::BUFFER_VIEW_EXT => "BUFFER_VIEW_EXT",
            &Self::IMAGE_VIEW_EXT => "IMAGE_VIEW_EXT",
            &Self::SHADER_MODULE_EXT => "SHADER_MODULE_EXT",
            &Self::PIPELINE_CACHE_EXT => "PIPELINE_CACHE_EXT",
            &Self::PIPELINE_LAYOUT_EXT => "PIPELINE_LAYOUT_EXT",
            &Self::RENDER_PASS_EXT => "RENDER_PASS_EXT",
            &Self::PIPELINE_EXT => "PIPELINE_EXT",
            &Self::DESCRIPTOR_SET_LAYOUT_EXT => "DESCRIPTOR_SET_LAYOUT_EXT",
            &Self::SAMPLER_EXT => "SAMPLER_EXT",
            &Self::DESCRIPTOR_POOL_EXT => "DESCRIPTOR_POOL_EXT",
            &Self::DESCRIPTOR_SET_EXT => "DESCRIPTOR_SET_EXT",
            &Self::FRAMEBUFFER_EXT => "FRAMEBUFFER_EXT",
            &Self::COMMAND_POOL_EXT => "COMMAND_POOL_EXT",
            &Self::SURFACE_KHR_EXT => "SURFACE_KHR_EXT",
            &Self::SWAPCHAIN_KHR_EXT => "SWAPCHAIN_KHR_EXT",
            &Self::DEBUG_REPORT_CALLBACK_EXT_EXT => "DEBUG_REPORT_CALLBACK_EXT_EXT",
            &Self::DISPLAY_KHR_EXT => "DISPLAY_KHR_EXT",
            &Self::DISPLAY_MODE_KHR_EXT => "DISPLAY_MODE_KHR_EXT",
            &Self::VALIDATION_CACHE_EXT_EXT => "VALIDATION_CACHE_EXT_EXT",
            &Self::SAMPLER_YCBCR_CONVERSION_EXT => "SAMPLER_YCBCR_CONVERSION_EXT",
            &Self::DESCRIPTOR_UPDATE_TEMPLATE_EXT => "DESCRIPTOR_UPDATE_TEMPLATE_EXT",
            &Self::CU_MODULE_NVX_EXT => "CU_MODULE_NVX_EXT",
            &Self::CU_FUNCTION_NVX_EXT => "CU_FUNCTION_NVX_EXT",
            &Self::ACCELERATION_STRUCTURE_KHR_EXT => "ACCELERATION_STRUCTURE_KHR_EXT",
            &Self::ACCELERATION_STRUCTURE_NV_EXT => "ACCELERATION_STRUCTURE_NV_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::extensions::ext_debug_report::DebugReportObjectTypeEXT {
    pub const UNKNOWN_EXT: Self = Self(0);
    pub const INSTANCE_EXT: Self = Self(1);
    pub const PHYSICAL_DEVICE_EXT: Self = Self(2);
    pub const DEVICE_EXT: Self = Self(3);
    pub const QUEUE_EXT: Self = Self(4);
    pub const SEMAPHORE_EXT: Self = Self(5);
    pub const COMMAND_BUFFER_EXT: Self = Self(6);
    pub const FENCE_EXT: Self = Self(7);
    pub const DEVICE_MEMORY_EXT: Self = Self(8);
    pub const BUFFER_EXT: Self = Self(9);
    pub const IMAGE_EXT: Self = Self(10);
    pub const EVENT_EXT: Self = Self(11);
    pub const QUERY_POOL_EXT: Self = Self(12);
    pub const BUFFER_VIEW_EXT: Self = Self(13);
    pub const IMAGE_VIEW_EXT: Self = Self(14);
    pub const SHADER_MODULE_EXT: Self = Self(15);
    pub const PIPELINE_CACHE_EXT: Self = Self(16);
    pub const PIPELINE_LAYOUT_EXT: Self = Self(17);
    pub const RENDER_PASS_EXT: Self = Self(18);
    pub const PIPELINE_EXT: Self = Self(19);
    pub const DESCRIPTOR_SET_LAYOUT_EXT: Self = Self(20);
    pub const SAMPLER_EXT: Self = Self(21);
    pub const DESCRIPTOR_POOL_EXT: Self = Self(22);
    pub const DESCRIPTOR_SET_EXT: Self = Self(23);
    pub const FRAMEBUFFER_EXT: Self = Self(24);
    pub const COMMAND_POOL_EXT: Self = Self(25);
    pub const SURFACE_KHR_EXT: Self = Self(26);
    pub const SWAPCHAIN_KHR_EXT: Self = Self(27);
    pub const DEBUG_REPORT_CALLBACK_EXT_EXT: Self = Self(28);
    pub const DISPLAY_KHR_EXT: Self = Self(29);
    pub const DISPLAY_MODE_KHR_EXT: Self = Self(30);
    pub const VALIDATION_CACHE_EXT_EXT: Self = Self(33);
    pub const SAMPLER_YCBCR_CONVERSION_EXT: Self = Self(1000156000);
    pub const DESCRIPTOR_UPDATE_TEMPLATE_EXT: Self = Self(1000085000);
    pub const DEBUG_REPORT_EXT: Self = Self::DEBUG_REPORT_CALLBACK_EXT_EXT;
    pub const VALIDATION_CACHE_EXT: Self = Self::VALIDATION_CACHE_EXT_EXT;
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDebugReportCallbackEXT.html)) · Function <br/> vkCreateDebugReportCallbackEXT - Create a debug report callback object\n[](#_c_specification)C Specification\n----------\n\nDebug report callbacks give more detailed feedback on the application’s use\nof Vulkan when events of interest occur.\n\nTo register a debug report callback, an application uses[`crate::vk::InstanceLoader::create_debug_report_callback_ext`].\n\n```\n// Provided by VK_EXT_debug_report\nVkResult vkCreateDebugReportCallbackEXT(\n    VkInstance                                  instance,\n    const VkDebugReportCallbackCreateInfoEXT*   pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDebugReportCallbackEXT*                   pCallback);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance the callback will be logged on.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::DebugReportCallbackCreateInfoEXT`] structure defining the\n  conditions under which this callback will be called.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_callback`] is a pointer to a [`crate::vk::DebugReportCallbackEXT`] handle\n  in which the created object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDebugReportCallbackEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DebugReportCallbackCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pCallback-parameter  \n  [`Self::p_callback`] **must** be a valid pointer to a [`crate::vk::DebugReportCallbackEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugReportCallbackCreateInfoEXT`], [`crate::vk::DebugReportCallbackEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateDebugReportCallbackEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::ext_debug_report::DebugReportCallbackCreateInfoEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_callback: *mut crate::extensions::ext_debug_report::DebugReportCallbackEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyDebugReportCallbackEXT.html)) · Function <br/> vkDestroyDebugReportCallbackEXT - Destroy a debug report callback object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a [`crate::vk::DebugReportCallbackEXT`] object, call:\n\n```\n// Provided by VK_EXT_debug_report\nvoid vkDestroyDebugReportCallbackEXT(\n    VkInstance                                  instance,\n    VkDebugReportCallbackEXT                    callback,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance where the callback was created.\n\n* [`Self::callback`] is the [`crate::vk::DebugReportCallbackEXT`] object to destroy.[`Self::callback`] is an externally synchronized object and **must** not be\n  used on more than one thread at a time.\n  This means that [`crate::vk::InstanceLoader::destroy_debug_report_callback_ext`] **must** not be\n  called when a callback is active.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-01242  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::callback`] was\n  created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-01243  \n   If no [`crate::vk::AllocationCallbacks`] were provided when [`Self::callback`] was\n  created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-callback-parameter  \n   If [`Self::callback`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::callback`] **must** be a valid [`crate::vk::DebugReportCallbackEXT`] handle\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-callback-parent  \n   If [`Self::callback`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::instance`]\n\nHost Synchronization\n\n* Host access to [`Self::callback`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugReportCallbackEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDestroyDebugReportCallbackEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, callback: crate::extensions::ext_debug_report::DebugReportCallbackEXT, p_allocator: *const crate::vk1_0::AllocationCallbacks) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugReportMessageEXT.html)) · Function <br/> vkDebugReportMessageEXT - Inject a message into a debug stream\n[](#_c_specification)C Specification\n----------\n\nTo inject its own messages into the debug stream, call:\n\n```\n// Provided by VK_EXT_debug_report\nvoid vkDebugReportMessageEXT(\n    VkInstance                                  instance,\n    VkDebugReportFlagsEXT                       flags,\n    VkDebugReportObjectTypeEXT                  objectType,\n    uint64_t                                    object,\n    size_t                                      location,\n    int32_t                                     messageCode,\n    const char*                                 pLayerPrefix,\n    const char*                                 pMessage);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the debug stream’s [`crate::vk::Instance`].\n\n* [`Self::flags`] specifies the [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) classification\n  of this event/message.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of object being used or created at the time the event was\n  triggered.\n\n* [`Self::object`] is the object where the issue was detected.[`Self::object`] **can** be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) if there is no object\n  associated with the event.\n\n* [`Self::location`] is an application defined value.\n\n* [`Self::message_code`] is an application defined value.\n\n* [`Self::p_layer_prefix`] is the abbreviation of the component making this\n  event/message.\n\n* [`Self::p_message`] is a null-terminated string detailing the trigger\n  conditions.\n[](#_description)Description\n----------\n\nThe call will propagate through the layers and generate callback(s) as\nindicated by the message’s flags.\nThe parameters are passed on to the callback in addition to the`pUserData` value that was defined at the time the callback was\nregistered.\n\nValid Usage\n\n* []() VUID-vkDebugReportMessageEXT-object-01241  \n  [`Self::object`] **must** be a Vulkan object or [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-vkDebugReportMessageEXT-objectType-01498  \n   If [`Self::object_type`] is not [`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]and [`Self::object`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::object`] **must** be a\n  Vulkan object of the corresponding type associated with [`Self::object_type`]as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugReportMessageEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDebugReportMessageEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) values\n\n* []() VUID-vkDebugReportMessageEXT-flags-requiredbitmask  \n  [`Self::flags`] **must** not be `0`\n\n* []() VUID-vkDebugReportMessageEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-vkDebugReportMessageEXT-pLayerPrefix-parameter  \n  [`Self::p_layer_prefix`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-vkDebugReportMessageEXT-pMessage-parameter  \n  [`Self::p_message`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportFlagBitsEXT`], [`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::Instance`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDebugReportMessageEXT = unsafe extern "system" fn(instance: crate::vk1_0::Instance, flags: crate::extensions::ext_debug_report::DebugReportFlagsEXT, object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, p_layer_prefix: *const std::os::raw::c_char, p_message: *const std::os::raw::c_char) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/PFN_vkDebugReportCallbackEXT.html)) · Function <br/> PFN\\_vkDebugReportCallbackEXT - Application-defined debug report callback function\n[](#_c_specification)C Specification\n----------\n\nThe prototype for the[`crate::vk::DebugReportCallbackCreateInfoEXT::pfn_callback`] function\nimplemented by the application is:\n\n```\n// Provided by VK_EXT_debug_report\ntypedef VkBool32 (VKAPI_PTR *PFN_vkDebugReportCallbackEXT)(\n    VkDebugReportFlagsEXT                       flags,\n    VkDebugReportObjectTypeEXT                  objectType,\n    uint64_t                                    object,\n    size_t                                      location,\n    int32_t                                     messageCode,\n    const char*                                 pLayerPrefix,\n    const char*                                 pMessage,\n    void*                                       pUserData);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::flags`] specifies the [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) that triggered\n  this callback.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] value specifying\n  the type of object being used or created at the time the event was\n  triggered.\n\n* [`Self::object`] is the object where the issue was detected.\n  If [`Self::object_type`] is [`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`],[`Self::object`] is undefined.\n\n* [`Self::location`] is a component (layer, driver, loader) defined value\n  specifying the *location* of the trigger.\n  This is an **optional** value.\n\n* [`Self::message_code`] is a layer-defined value indicating what test\n  triggered this callback.\n\n* [`Self::p_layer_prefix`] is a null-terminated string that is an abbreviation\n  of the name of the component making the callback.[`Self::p_layer_prefix`] is only valid for the duration of the callback.\n\n* [`Self::p_message`] is a null-terminated string detailing the trigger\n  conditions.[`Self::p_message`] is only valid for the duration of the callback.\n\n* [`Self::p_user_data`] is the user data given when the[`crate::vk::DebugReportCallbackEXT`] was created.\n[](#_description)Description\n----------\n\nThe callback **must** not call [`crate::vk::InstanceLoader::destroy_debug_report_callback_ext`].\n\nThe callback returns a [`crate::vk::Bool32`], which is interpreted in a\nlayer-specified manner.\nThe application **should** always return [`crate::vk::FALSE`].\nThe [`crate::vk::TRUE`] value is reserved for use in layer development.\n\n[`Self::object`] **must** be a Vulkan object or [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html).\nIf [`Self::object_type`] is not [`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`] and[`Self::object`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::object`] **must** be a Vulkan\nobject of the corresponding type associated with [`Self::object_type`] as defined\nin [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportCallbackCreateInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDebugReportCallbackEXT = unsafe extern "system" fn(flags: crate::extensions::ext_debug_report::DebugReportFlagsEXT, object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, p_layer_prefix: *const std::os::raw::c_char, p_message: *const std::os::raw::c_char, p_user_data: *mut std::ffi::c_void) -> crate::vk1_0::Bool32;
impl<'a> crate::ExtendableFromConst<'a, DebugReportCallbackCreateInfoEXT> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DebugReportCallbackCreateInfoEXTBuilder<'_>> for crate::vk1_0::InstanceCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportCallbackCreateInfoEXT.html)) · Structure <br/> VkDebugReportCallbackCreateInfoEXT - Structure specifying parameters of a newly created debug report callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugReportCallbackCreateInfoEXT`] is:\n\n```\n// Provided by VK_EXT_debug_report\ntypedef struct VkDebugReportCallbackCreateInfoEXT {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkDebugReportFlagsEXT           flags;\n    PFN_vkDebugReportCallbackEXT    pfnCallback;\n    void*                           pUserData;\n} VkDebugReportCallbackCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) specifying\n  which event(s) will cause this callback to be called.\n\n* [`Self::pfn_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nFor each [`crate::vk::DebugReportCallbackEXT`] that is created the[`crate::vk::DebugReportCallbackCreateInfoEXT`]::[`Self::flags`] determine when that[`crate::vk::DebugReportCallbackCreateInfoEXT`]::[`Self::pfn_callback`] is called.\nWhen an event happens, the implementation will do a bitwise AND of the\nevent’s [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) flags to each[`crate::vk::DebugReportCallbackEXT`] object’s flags.\nFor each non-zero result the corresponding callback will be called.\nThe callback will come directly from the component that detected the event,\nunless some other layer intercepts the calls for its own purposes (filter\nthem in a different way, log to a system error log, etc.).\n\nAn application **may** receive multiple callbacks if multiple[`crate::vk::DebugReportCallbackEXT`] objects were created.\nA callback will always be executed in the same thread as the originating\nVulkan call.\n\nA callback may be called from multiple threads simultaneously (if the\napplication is making Vulkan calls from multiple threads).\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT`]\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) values\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-pfnCallback-parameter  \n  [`Self::pfn_callback`] **must** be a valid [PFN\\_vkDebugReportCallbackEXT](PFN_vkDebugReportCallbackEXT.html) value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDebugReportCallbackEXT](PFN_vkDebugReportCallbackEXT.html), [`crate::vk::DebugReportFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_debug_report_callback_ext`]\n"]
#[doc(alias = "VkDebugReportCallbackCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugReportCallbackCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_debug_report::DebugReportFlagsEXT,
    pub pfn_callback: Option<crate::extensions::ext_debug_report::PFN_vkDebugReportCallbackEXT>,
    pub p_user_data: *mut std::ffi::c_void,
}
impl DebugReportCallbackCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
}
impl Default for DebugReportCallbackCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), pfn_callback: Default::default(), p_user_data: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for DebugReportCallbackCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugReportCallbackCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("pfn_callback", unsafe { &std::mem::transmute::<_, *const ()>(self.pfn_callback) }).field("p_user_data", &self.p_user_data).finish()
    }
}
impl DebugReportCallbackCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugReportCallbackCreateInfoEXTBuilder<'a> {
        DebugReportCallbackCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportCallbackCreateInfoEXT.html)) · Builder of [`DebugReportCallbackCreateInfoEXT`] <br/> VkDebugReportCallbackCreateInfoEXT - Structure specifying parameters of a newly created debug report callback\n[](#_c_specification)C Specification\n----------\n\nThe definition of [`crate::vk::DebugReportCallbackCreateInfoEXT`] is:\n\n```\n// Provided by VK_EXT_debug_report\ntypedef struct VkDebugReportCallbackCreateInfoEXT {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkDebugReportFlagsEXT           flags;\n    PFN_vkDebugReportCallbackEXT    pfnCallback;\n    void*                           pUserData;\n} VkDebugReportCallbackCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is a bitmask of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) specifying\n  which event(s) will cause this callback to be called.\n\n* [`Self::pfn_callback`] is the application callback function to call.\n\n* [`Self::p_user_data`] is user data to be passed to the callback.\n[](#_description)Description\n----------\n\nFor each [`crate::vk::DebugReportCallbackEXT`] that is created the[`crate::vk::DebugReportCallbackCreateInfoEXT`]::[`Self::flags`] determine when that[`crate::vk::DebugReportCallbackCreateInfoEXT`]::[`Self::pfn_callback`] is called.\nWhen an event happens, the implementation will do a bitwise AND of the\nevent’s [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) flags to each[`crate::vk::DebugReportCallbackEXT`] object’s flags.\nFor each non-zero result the corresponding callback will be called.\nThe callback will come directly from the component that detected the event,\nunless some other layer intercepts the calls for its own purposes (filter\nthem in a different way, log to a system error log, etc.).\n\nAn application **may** receive multiple callbacks if multiple[`crate::vk::DebugReportCallbackEXT`] objects were created.\nA callback will always be executed in the same thread as the originating\nVulkan call.\n\nA callback may be called from multiple threads simultaneously (if the\napplication is making Vulkan calls from multiple threads).\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT`]\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) values\n\n* []() VUID-VkDebugReportCallbackCreateInfoEXT-pfnCallback-parameter  \n  [`Self::pfn_callback`] **must** be a valid [PFN\\_vkDebugReportCallbackEXT](PFN_vkDebugReportCallbackEXT.html) value\n[](#_see_also)See Also\n----------\n\n[PFN\\_vkDebugReportCallbackEXT](PFN_vkDebugReportCallbackEXT.html), [`crate::vk::DebugReportFlagBitsEXT`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_debug_report_callback_ext`]\n"]
#[repr(transparent)]
pub struct DebugReportCallbackCreateInfoEXTBuilder<'a>(DebugReportCallbackCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugReportCallbackCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugReportCallbackCreateInfoEXTBuilder<'a> {
        DebugReportCallbackCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_debug_report::DebugReportFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn pfn_callback(mut self, pfn_callback: Option<crate::extensions::ext_debug_report::PFN_vkDebugReportCallbackEXT>) -> Self {
        self.0.pfn_callback = pfn_callback as _;
        self
    }
    #[inline]
    pub fn user_data(mut self, user_data: *mut std::ffi::c_void) -> Self {
        self.0.p_user_data = user_data;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugReportCallbackCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugReportCallbackCreateInfoEXTBuilder<'a> {
    fn default() -> DebugReportCallbackCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugReportCallbackCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugReportCallbackCreateInfoEXTBuilder<'a> {
    type Target = DebugReportCallbackCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugReportCallbackCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_report`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateDebugReportCallbackEXT.html)) · Function <br/> vkCreateDebugReportCallbackEXT - Create a debug report callback object\n[](#_c_specification)C Specification\n----------\n\nDebug report callbacks give more detailed feedback on the application’s use\nof Vulkan when events of interest occur.\n\nTo register a debug report callback, an application uses[`crate::vk::InstanceLoader::create_debug_report_callback_ext`].\n\n```\n// Provided by VK_EXT_debug_report\nVkResult vkCreateDebugReportCallbackEXT(\n    VkInstance                                  instance,\n    const VkDebugReportCallbackCreateInfoEXT*   pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkDebugReportCallbackEXT*                   pCallback);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance the callback will be logged on.\n\n* [`Self::p_create_info`] is a pointer to a[`crate::vk::DebugReportCallbackCreateInfoEXT`] structure defining the\n  conditions under which this callback will be called.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n\n* [`Self::p_callback`] is a pointer to a [`crate::vk::DebugReportCallbackEXT`] handle\n  in which the created object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateDebugReportCallbackEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::DebugReportCallbackCreateInfoEXT`] structure\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateDebugReportCallbackEXT-pCallback-parameter  \n  [`Self::p_callback`] **must** be a valid pointer to a [`crate::vk::DebugReportCallbackEXT`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugReportCallbackCreateInfoEXT`], [`crate::vk::DebugReportCallbackEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkCreateDebugReportCallbackEXT")]
    pub unsafe fn create_debug_report_callback_ext(&self, create_info: &crate::extensions::ext_debug_report::DebugReportCallbackCreateInfoEXT, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::ext_debug_report::DebugReportCallbackEXT> {
        let _function = self.create_debug_report_callback_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut callback = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut callback,
        );
        crate::utils::VulkanResult::new(_return, callback)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDestroyDebugReportCallbackEXT.html)) · Function <br/> vkDestroyDebugReportCallbackEXT - Destroy a debug report callback object\n[](#_c_specification)C Specification\n----------\n\nTo destroy a [`crate::vk::DebugReportCallbackEXT`] object, call:\n\n```\n// Provided by VK_EXT_debug_report\nvoid vkDestroyDebugReportCallbackEXT(\n    VkInstance                                  instance,\n    VkDebugReportCallbackEXT                    callback,\n    const VkAllocationCallbacks*                pAllocator);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance where the callback was created.\n\n* [`Self::callback`] is the [`crate::vk::DebugReportCallbackEXT`] object to destroy.[`Self::callback`] is an externally synchronized object and **must** not be\n  used on more than one thread at a time.\n  This means that [`crate::vk::InstanceLoader::destroy_debug_report_callback_ext`] **must** not be\n  called when a callback is active.\n\n* [`Self::p_allocator`] controls host memory allocation as described in the[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation) chapter.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-01242  \n   If [`crate::vk::AllocationCallbacks`] were provided when [`Self::callback`] was\n  created, a compatible set of callbacks **must** be provided here\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-01243  \n   If no [`crate::vk::AllocationCallbacks`] were provided when [`Self::callback`] was\n  created, [`Self::p_allocator`] **must** be `NULL`\n\nValid Usage (Implicit)\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-callback-parameter  \n   If [`Self::callback`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::callback`] **must** be a valid [`crate::vk::DebugReportCallbackEXT`] handle\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkDestroyDebugReportCallbackEXT-callback-parent  \n   If [`Self::callback`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::instance`]\n\nHost Synchronization\n\n* Host access to [`Self::callback`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::DebugReportCallbackEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkDestroyDebugReportCallbackEXT")]
    pub unsafe fn destroy_debug_report_callback_ext(&self, callback: Option<crate::extensions::ext_debug_report::DebugReportCallbackEXT>, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> () {
        let _function = self.destroy_debug_report_callback_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            match callback {
                Some(v) => v,
                None => Default::default(),
            },
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugReportMessageEXT.html)) · Function <br/> vkDebugReportMessageEXT - Inject a message into a debug stream\n[](#_c_specification)C Specification\n----------\n\nTo inject its own messages into the debug stream, call:\n\n```\n// Provided by VK_EXT_debug_report\nvoid vkDebugReportMessageEXT(\n    VkInstance                                  instance,\n    VkDebugReportFlagsEXT                       flags,\n    VkDebugReportObjectTypeEXT                  objectType,\n    uint64_t                                    object,\n    size_t                                      location,\n    int32_t                                     messageCode,\n    const char*                                 pLayerPrefix,\n    const char*                                 pMessage);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the debug stream’s [`crate::vk::Instance`].\n\n* [`Self::flags`] specifies the [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) classification\n  of this event/message.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of object being used or created at the time the event was\n  triggered.\n\n* [`Self::object`] is the object where the issue was detected.[`Self::object`] **can** be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) if there is no object\n  associated with the event.\n\n* [`Self::location`] is an application defined value.\n\n* [`Self::message_code`] is an application defined value.\n\n* [`Self::p_layer_prefix`] is the abbreviation of the component making this\n  event/message.\n\n* [`Self::p_message`] is a null-terminated string detailing the trigger\n  conditions.\n[](#_description)Description\n----------\n\nThe call will propagate through the layers and generate callback(s) as\nindicated by the message’s flags.\nThe parameters are passed on to the callback in addition to the`pUserData` value that was defined at the time the callback was\nregistered.\n\nValid Usage\n\n* []() VUID-vkDebugReportMessageEXT-object-01241  \n  [`Self::object`] **must** be a Vulkan object or [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-vkDebugReportMessageEXT-objectType-01498  \n   If [`Self::object_type`] is not [`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]and [`Self::object`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::object`] **must** be a\n  Vulkan object of the corresponding type associated with [`Self::object_type`]as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugReportMessageEXT-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkDebugReportMessageEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkDebugReportFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugReportFlagBitsEXT.html) values\n\n* []() VUID-vkDebugReportMessageEXT-flags-requiredbitmask  \n  [`Self::flags`] **must** not be `0`\n\n* []() VUID-vkDebugReportMessageEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-vkDebugReportMessageEXT-pLayerPrefix-parameter  \n  [`Self::p_layer_prefix`] **must** be a null-terminated UTF-8 string\n\n* []() VUID-vkDebugReportMessageEXT-pMessage-parameter  \n  [`Self::p_message`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportFlagBitsEXT`], [`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::Instance`]\n"]
    #[doc(alias = "vkDebugReportMessageEXT")]
    pub unsafe fn debug_report_message_ext(&self, flags: crate::extensions::ext_debug_report::DebugReportFlagsEXT, object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, layer_prefix: Option<&std::ffi::CStr>, message: Option<&std::ffi::CStr>) -> () {
        let _function = self.debug_report_message_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            flags as _,
            object_type as _,
            object as _,
            location as _,
            message_code as _,
            match layer_prefix {
                Some(v) => v.as_ptr(),
                None => std::ptr::null(),
            },
            match message {
                Some(v) => v.as_ptr(),
                None => std::ptr::null(),
            },
        );
        ()
    }
}
