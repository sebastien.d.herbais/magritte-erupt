#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CALIBRATED_TIMESTAMPS_SPEC_VERSION")]
pub const EXT_CALIBRATED_TIMESTAMPS_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CALIBRATED_TIMESTAMPS_EXTENSION_NAME")]
pub const EXT_CALIBRATED_TIMESTAMPS_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_calibrated_timestamps");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_CALIBRATEABLE_TIME_DOMAINS_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceCalibrateableTimeDomainsEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_CALIBRATED_TIMESTAMPS_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetCalibratedTimestampsEXT");
#[doc = "Provided by [`crate::extensions::ext_calibrated_timestamps`]"]
impl crate::vk1_0::StructureType {
    pub const CALIBRATED_TIMESTAMP_INFO_EXT: Self = Self(1000184000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkTimeDomainEXT.html)) · Enum <br/> VkTimeDomainEXT - Supported time domains\n[](#_c_specification)C Specification\n----------\n\nThe set of supported time domains consists of:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\ntypedef enum VkTimeDomainEXT {\n    VK_TIME_DOMAIN_DEVICE_EXT = 0,\n    VK_TIME_DOMAIN_CLOCK_MONOTONIC_EXT = 1,\n    VK_TIME_DOMAIN_CLOCK_MONOTONIC_RAW_EXT = 2,\n    VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT = 3,\n} VkTimeDomainEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::DEVICE_EXT`] specifies the device time domain.\n  Timestamp values in this time domain use the same units and are\n  comparable with device timestamp values captured using[`crate::vk::PFN_vkCmdWriteTimestamp`]or [`crate::vk::DeviceLoader::cmd_write_timestamp2_khr`]and are defined to be incrementing according to the[timestampPeriod](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#limits-timestampPeriod) of the device.\n\n* [`Self::CLOCK_MONOTONIC_EXT`] specifies the CLOCK\\_MONOTONIC\n  time domain available on POSIX platforms.\n  Timestamp values in this time domain are in units of nanoseconds and are\n  comparable with platform timestamp values captured using the POSIX\n  clock\\_gettime API as computed by this example:\n\n|   |Note<br/><br/>An implementation supporting [VK_EXT_calibrated_timestamps](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_calibrated_timestamps.html) will use the<br/>same time domain for all its [`crate::vk::Queue`] so that timestamp values reported<br/>for [`Self::DEVICE_EXT`] can be matched to any timestamp captured<br/>through [`crate::vk::PFN_vkCmdWriteTimestamp`]or [`crate::vk::DeviceLoader::cmd_write_timestamp2_khr`].|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\n```\nstruct timespec tv;\nclock_gettime(CLOCK_MONOTONIC, &tv);\nreturn tv.tv_nsec + tv.tv_sec*1000000000ull;\n```\n\n* [`Self::CLOCK_MONOTONIC_RAW_EXT`] specifies the\n  CLOCK\\_MONOTONIC\\_RAW time domain available on POSIX platforms.\n  Timestamp values in this time domain are in units of nanoseconds and are\n  comparable with platform timestamp values captured using the POSIX\n  clock\\_gettime API as computed by this example:\n\n```\nstruct timespec tv;\nclock_gettime(CLOCK_MONOTONIC_RAW, &tv);\nreturn tv.tv_nsec + tv.tv_sec*1000000000ull;\n```\n\n* [`Self::QUERY_PERFORMANCE_COUNTER_EXT`] specifies the\n  performance counter (QPC) time domain available on Windows.\n  Timestamp values in this time domain are in the same units as those\n  provided by the Windows QueryPerformanceCounter API and are comparable\n  with platform timestamp values captured using that API as computed by\n  this example:\n\n```\nLARGE_INTEGER counter;\nQueryPerformanceCounter(&counter);\nreturn counter.QuadPart;\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CalibratedTimestampInfoEXT`], [`crate::vk::DeviceLoader::get_physical_device_calibrateable_time_domains_ext`]\n"]
#[doc(alias = "VkTimeDomainEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct TimeDomainEXT(pub i32);
impl std::fmt::Debug for TimeDomainEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEVICE_EXT => "DEVICE_EXT",
            &Self::CLOCK_MONOTONIC_EXT => "CLOCK_MONOTONIC_EXT",
            &Self::CLOCK_MONOTONIC_RAW_EXT => "CLOCK_MONOTONIC_RAW_EXT",
            &Self::QUERY_PERFORMANCE_COUNTER_EXT => "QUERY_PERFORMANCE_COUNTER_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_calibrated_timestamps`]"]
impl crate::extensions::ext_calibrated_timestamps::TimeDomainEXT {
    pub const DEVICE_EXT: Self = Self(0);
    pub const CLOCK_MONOTONIC_EXT: Self = Self(1);
    pub const CLOCK_MONOTONIC_RAW_EXT: Self = Self(2);
    pub const QUERY_PERFORMANCE_COUNTER_EXT: Self = Self(3);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceCalibrateableTimeDomainsEXT.html)) · Function <br/> vkGetPhysicalDeviceCalibrateableTimeDomainsEXT - Query calibrateable time domains\n[](#_c_specification)C Specification\n----------\n\nTo query the set of time domains for which a physical device supports\ntimestamp calibration, call:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\nVkResult vkGetPhysicalDeviceCalibrateableTimeDomainsEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pTimeDomainCount,\n    VkTimeDomainEXT*                            pTimeDomains);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the set\n  of calibrateable time domains.\n\n* [`Self::p_time_domain_count`] is a pointer to an integer related to the number\n  of calibrateable time domains available or queried, as described below.\n\n* [`Self::p_time_domains`] is either `NULL` or a pointer to an array of[`crate::vk::TimeDomainEXT`] values, indicating the supported calibrateable\n  time domains.\n[](#_description)Description\n----------\n\nIf [`Self::p_time_domains`] is `NULL`, then the number of calibrateable time\ndomains supported for the given [`Self::physical_device`] is returned in[`Self::p_time_domain_count`].\nOtherwise, [`Self::p_time_domain_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_time_domains`] array, and on return the\nvariable is overwritten with the number of values actually written to[`Self::p_time_domains`].\nIf the value of [`Self::p_time_domain_count`] is less than the number of\ncalibrateable time domains supported, at most [`Self::p_time_domain_count`] values\nwill be written to [`Self::p_time_domains`], and [`crate::vk::Result::INCOMPLETE`] will be\nreturned instead of [`crate::vk::Result::SUCCESS`], to indicate that not all the available\ntime domains were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-pTimeDomainCount-parameter  \n  [`Self::p_time_domain_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-pTimeDomains-parameter  \n   If the value referenced by [`Self::p_time_domain_count`] is not `0`, and [`Self::p_time_domains`] is not `NULL`, [`Self::p_time_domains`] **must** be a valid pointer to an array of [`Self::p_time_domain_count`] [`crate::vk::TimeDomainEXT`] values\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::TimeDomainEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceCalibrateableTimeDomainsEXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_time_domain_count: *mut u32, p_time_domains: *mut crate::extensions::ext_calibrated_timestamps::TimeDomainEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetCalibratedTimestampsEXT.html)) · Function <br/> vkGetCalibratedTimestampsEXT - Query calibrated timestamps\n[](#_c_specification)C Specification\n----------\n\nIn order to be able to correlate the time a particular operation took place\nat on timelines of different time domains (e.g. a device operation vs a host\noperation), Vulkan allows querying calibrated timestamps from multiple time\ndomains.\n\nTo query calibrated timestamps from a set of time domains, call:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\nVkResult vkGetCalibratedTimestampsEXT(\n    VkDevice                                    device,\n    uint32_t                                    timestampCount,\n    const VkCalibratedTimestampInfoEXT*         pTimestampInfos,\n    uint64_t*                                   pTimestamps,\n    uint64_t*                                   pMaxDeviation);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used to perform the query.\n\n* [`Self::timestamp_count`] is the number of timestamps to query.\n\n* [`Self::p_timestamp_infos`] is a pointer to an array of [`Self::timestamp_count`][`crate::vk::CalibratedTimestampInfoEXT`] structures, describing the time\n  domains the calibrated timestamps should be captured from.\n\n* [`Self::p_timestamps`] is a pointer to an array of [`Self::timestamp_count`]64-bit unsigned integer values in which the requested calibrated\n  timestamp values are returned.\n\n* [`Self::p_max_deviation`] is a pointer to a 64-bit unsigned integer value in\n  which the strictly positive maximum deviation, in nanoseconds, of the\n  calibrated timestamp values is returned.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>The maximum deviation **may** vary between calls to[`crate::vk::DeviceLoader::get_calibrated_timestamps_ext`] even for the same set of time domains due<br/>to implementation and platform specific reasons.<br/>It is the application’s responsibility to assess whether the returned<br/>maximum deviation makes the timestamp values suitable for any particular<br/>purpose and **can** choose to re-issue the timestamp calibration call pursuing<br/>a lower devation value.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nCalibrated timestamp values **can** be extrapolated to estimate future\ncoinciding timestamp values, however, depending on the nature of the time\ndomains and other properties of the platform extrapolating values over a\nsufficiently long period of time **may** no longer be accurate enough to fit\nany particular purpose, so applications are expected to re-calibrate the\ntimestamps on a regular basis.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetCalibratedTimestampsEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pTimestampInfos-parameter  \n  [`Self::p_timestamp_infos`] **must** be a valid pointer to an array of [`Self::timestamp_count`] valid [`crate::vk::CalibratedTimestampInfoEXT`] structures\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pTimestamps-parameter  \n  [`Self::p_timestamps`] **must** be a valid pointer to an array of [`Self::timestamp_count`] `uint64_t` values\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pMaxDeviation-parameter  \n  [`Self::p_max_deviation`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetCalibratedTimestampsEXT-timestampCount-arraylength  \n  [`Self::timestamp_count`] **must** be greater than `0`\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CalibratedTimestampInfoEXT`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetCalibratedTimestampsEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, timestamp_count: u32, p_timestamp_infos: *const crate::extensions::ext_calibrated_timestamps::CalibratedTimestampInfoEXT, p_timestamps: *mut u64, p_max_deviation: *mut u64) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCalibratedTimestampInfoEXT.html)) · Structure <br/> VkCalibratedTimestampInfoEXT - Structure specifying the input parameters of a calibrated timestamp query\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CalibratedTimestampInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\ntypedef struct VkCalibratedTimestampInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkTimeDomainEXT    timeDomain;\n} VkCalibratedTimestampInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::time_domain`] is a [`crate::vk::TimeDomainEXT`] value specifying the time\n  domain from which the calibrated timestamp value should be returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkCalibratedTimestampInfoEXT-timeDomain-02354  \n  [`Self::time_domain`] **must** be one of the [`crate::vk::TimeDomainEXT`] values\n  returned by [`crate::vk::DeviceLoader::get_physical_device_calibrateable_time_domains_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCalibratedTimestampInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CALIBRATED_TIMESTAMP_INFO_EXT`]\n\n* []() VUID-VkCalibratedTimestampInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkCalibratedTimestampInfoEXT-timeDomain-parameter  \n  [`Self::time_domain`] **must** be a valid [`crate::vk::TimeDomainEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::TimeDomainEXT`], [`crate::vk::DeviceLoader::get_calibrated_timestamps_ext`]\n"]
#[doc(alias = "VkCalibratedTimestampInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CalibratedTimestampInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub time_domain: crate::extensions::ext_calibrated_timestamps::TimeDomainEXT,
}
impl CalibratedTimestampInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::CALIBRATED_TIMESTAMP_INFO_EXT;
}
impl Default for CalibratedTimestampInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), time_domain: Default::default() }
    }
}
impl std::fmt::Debug for CalibratedTimestampInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CalibratedTimestampInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("time_domain", &self.time_domain).finish()
    }
}
impl CalibratedTimestampInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> CalibratedTimestampInfoEXTBuilder<'a> {
        CalibratedTimestampInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCalibratedTimestampInfoEXT.html)) · Builder of [`CalibratedTimestampInfoEXT`] <br/> VkCalibratedTimestampInfoEXT - Structure specifying the input parameters of a calibrated timestamp query\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CalibratedTimestampInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\ntypedef struct VkCalibratedTimestampInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkTimeDomainEXT    timeDomain;\n} VkCalibratedTimestampInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::time_domain`] is a [`crate::vk::TimeDomainEXT`] value specifying the time\n  domain from which the calibrated timestamp value should be returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkCalibratedTimestampInfoEXT-timeDomain-02354  \n  [`Self::time_domain`] **must** be one of the [`crate::vk::TimeDomainEXT`] values\n  returned by [`crate::vk::DeviceLoader::get_physical_device_calibrateable_time_domains_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCalibratedTimestampInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CALIBRATED_TIMESTAMP_INFO_EXT`]\n\n* []() VUID-VkCalibratedTimestampInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkCalibratedTimestampInfoEXT-timeDomain-parameter  \n  [`Self::time_domain`] **must** be a valid [`crate::vk::TimeDomainEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::TimeDomainEXT`], [`crate::vk::DeviceLoader::get_calibrated_timestamps_ext`]\n"]
#[repr(transparent)]
pub struct CalibratedTimestampInfoEXTBuilder<'a>(CalibratedTimestampInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> CalibratedTimestampInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> CalibratedTimestampInfoEXTBuilder<'a> {
        CalibratedTimestampInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn time_domain(mut self, time_domain: crate::extensions::ext_calibrated_timestamps::TimeDomainEXT) -> Self {
        self.0.time_domain = time_domain as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CalibratedTimestampInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for CalibratedTimestampInfoEXTBuilder<'a> {
    fn default() -> CalibratedTimestampInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CalibratedTimestampInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CalibratedTimestampInfoEXTBuilder<'a> {
    type Target = CalibratedTimestampInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CalibratedTimestampInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_calibrated_timestamps`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceCalibrateableTimeDomainsEXT.html)) · Function <br/> vkGetPhysicalDeviceCalibrateableTimeDomainsEXT - Query calibrateable time domains\n[](#_c_specification)C Specification\n----------\n\nTo query the set of time domains for which a physical device supports\ntimestamp calibration, call:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\nVkResult vkGetPhysicalDeviceCalibrateableTimeDomainsEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pTimeDomainCount,\n    VkTimeDomainEXT*                            pTimeDomains);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the set\n  of calibrateable time domains.\n\n* [`Self::p_time_domain_count`] is a pointer to an integer related to the number\n  of calibrateable time domains available or queried, as described below.\n\n* [`Self::p_time_domains`] is either `NULL` or a pointer to an array of[`crate::vk::TimeDomainEXT`] values, indicating the supported calibrateable\n  time domains.\n[](#_description)Description\n----------\n\nIf [`Self::p_time_domains`] is `NULL`, then the number of calibrateable time\ndomains supported for the given [`Self::physical_device`] is returned in[`Self::p_time_domain_count`].\nOtherwise, [`Self::p_time_domain_count`] **must** point to a variable set by the user\nto the number of elements in the [`Self::p_time_domains`] array, and on return the\nvariable is overwritten with the number of values actually written to[`Self::p_time_domains`].\nIf the value of [`Self::p_time_domain_count`] is less than the number of\ncalibrateable time domains supported, at most [`Self::p_time_domain_count`] values\nwill be written to [`Self::p_time_domains`], and [`crate::vk::Result::INCOMPLETE`] will be\nreturned instead of [`crate::vk::Result::SUCCESS`], to indicate that not all the available\ntime domains were returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-pTimeDomainCount-parameter  \n  [`Self::p_time_domain_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceCalibrateableTimeDomainsEXT-pTimeDomains-parameter  \n   If the value referenced by [`Self::p_time_domain_count`] is not `0`, and [`Self::p_time_domains`] is not `NULL`, [`Self::p_time_domains`] **must** be a valid pointer to an array of [`Self::p_time_domain_count`] [`crate::vk::TimeDomainEXT`] values\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::TimeDomainEXT`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceCalibrateableTimeDomainsEXT")]
    pub unsafe fn get_physical_device_calibrateable_time_domains_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, time_domain_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::ext_calibrated_timestamps::TimeDomainEXT>> {
        let _function = self.get_physical_device_calibrateable_time_domains_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut time_domain_count = match time_domain_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut time_domains = crate::SmallVec::from_elem(Default::default(), time_domain_count as _);
        let _return = _function(physical_device as _, &mut time_domain_count, time_domains.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, time_domains)
    }
}
#[doc = "Provided by [`crate::extensions::ext_calibrated_timestamps`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetCalibratedTimestampsEXT.html)) · Function <br/> vkGetCalibratedTimestampsEXT - Query calibrated timestamps\n[](#_c_specification)C Specification\n----------\n\nIn order to be able to correlate the time a particular operation took place\nat on timelines of different time domains (e.g. a device operation vs a host\noperation), Vulkan allows querying calibrated timestamps from multiple time\ndomains.\n\nTo query calibrated timestamps from a set of time domains, call:\n\n```\n// Provided by VK_EXT_calibrated_timestamps\nVkResult vkGetCalibratedTimestampsEXT(\n    VkDevice                                    device,\n    uint32_t                                    timestampCount,\n    const VkCalibratedTimestampInfoEXT*         pTimestampInfos,\n    uint64_t*                                   pTimestamps,\n    uint64_t*                                   pMaxDeviation);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used to perform the query.\n\n* [`Self::timestamp_count`] is the number of timestamps to query.\n\n* [`Self::p_timestamp_infos`] is a pointer to an array of [`Self::timestamp_count`][`crate::vk::CalibratedTimestampInfoEXT`] structures, describing the time\n  domains the calibrated timestamps should be captured from.\n\n* [`Self::p_timestamps`] is a pointer to an array of [`Self::timestamp_count`]64-bit unsigned integer values in which the requested calibrated\n  timestamp values are returned.\n\n* [`Self::p_max_deviation`] is a pointer to a 64-bit unsigned integer value in\n  which the strictly positive maximum deviation, in nanoseconds, of the\n  calibrated timestamp values is returned.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>The maximum deviation **may** vary between calls to[`crate::vk::DeviceLoader::get_calibrated_timestamps_ext`] even for the same set of time domains due<br/>to implementation and platform specific reasons.<br/>It is the application’s responsibility to assess whether the returned<br/>maximum deviation makes the timestamp values suitable for any particular<br/>purpose and **can** choose to re-issue the timestamp calibration call pursuing<br/>a lower devation value.|\n|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nCalibrated timestamp values **can** be extrapolated to estimate future\ncoinciding timestamp values, however, depending on the nature of the time\ndomains and other properties of the platform extrapolating values over a\nsufficiently long period of time **may** no longer be accurate enough to fit\nany particular purpose, so applications are expected to re-calibrate the\ntimestamps on a regular basis.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetCalibratedTimestampsEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pTimestampInfos-parameter  \n  [`Self::p_timestamp_infos`] **must** be a valid pointer to an array of [`Self::timestamp_count`] valid [`crate::vk::CalibratedTimestampInfoEXT`] structures\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pTimestamps-parameter  \n  [`Self::p_timestamps`] **must** be a valid pointer to an array of [`Self::timestamp_count`] `uint64_t` values\n\n* []() VUID-vkGetCalibratedTimestampsEXT-pMaxDeviation-parameter  \n  [`Self::p_max_deviation`] **must** be a valid pointer to a `uint64_t` value\n\n* []() VUID-vkGetCalibratedTimestampsEXT-timestampCount-arraylength  \n  [`Self::timestamp_count`] **must** be greater than `0`\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CalibratedTimestampInfoEXT`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkGetCalibratedTimestampsEXT")]
    pub unsafe fn get_calibrated_timestamps_ext(&self, timestamp_infos: &[crate::extensions::ext_calibrated_timestamps::CalibratedTimestampInfoEXTBuilder]) -> crate::utils::VulkanResult<(crate::SmallVec<u64>, u64)> {
        let _function = self.get_calibrated_timestamps_ext.expect(crate::NOT_LOADED_MESSAGE);
        let timestamp_count = timestamp_infos.len();
        let mut timestamps = crate::SmallVec::from_elem(Default::default(), timestamp_count as _);
        let mut max_deviation = Default::default();
        let _return = _function(self.handle, timestamp_count as _, timestamp_infos.as_ptr() as _, timestamps.as_mut_ptr(), &mut max_deviation);
        crate::utils::VulkanResult::new(_return, (timestamps, max_deviation))
    }
}
