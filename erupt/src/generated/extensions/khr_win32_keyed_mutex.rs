#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WIN32_KEYED_MUTEX_SPEC_VERSION")]
pub const KHR_WIN32_KEYED_MUTEX_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WIN32_KEYED_MUTEX_EXTENSION_NAME")]
pub const KHR_WIN32_KEYED_MUTEX_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_win32_keyed_mutex");
#[doc = "Provided by [`crate::extensions::khr_win32_keyed_mutex`]"]
impl crate::vk1_0::StructureType {
    pub const WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR: Self = Self(1000075000);
}
impl<'a> crate::ExtendableFromConst<'a, Win32KeyedMutexAcquireReleaseInfoKHR> for crate::vk1_0::SubmitInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'_>> for crate::vk1_0::SubmitInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWin32KeyedMutexAcquireReleaseInfoKHR.html)) · Structure <br/> VkWin32KeyedMutexAcquireReleaseInfoKHR - Use the Windows keyed mutex mechanism to synchronize work\n[](#_c_specification)C Specification\n----------\n\nWhen submitting work that operates on memory imported from a Direct3D 11\nresource to a queue, the keyed mutex mechanism **may** be used in addition to\nVulkan semaphores to synchronize the work.\nKeyed mutexes are a property of a properly created shareable Direct3D 11\nresource.\nThey **can** only be used if the imported resource was created with the`D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX` flag.\n\nTo acquire keyed mutexes before submitted work and/or release them after,\nadd a [`crate::vk::Win32KeyedMutexAcquireReleaseInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::SubmitInfo`] structure.\n\nThe [`crate::vk::Win32KeyedMutexAcquireReleaseInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_win32_keyed_mutex\ntypedef struct VkWin32KeyedMutexAcquireReleaseInfoKHR {\n    VkStructureType          sType;\n    const void*              pNext;\n    uint32_t                 acquireCount;\n    const VkDeviceMemory*    pAcquireSyncs;\n    const uint64_t*          pAcquireKeys;\n    const uint32_t*          pAcquireTimeouts;\n    uint32_t                 releaseCount;\n    const VkDeviceMemory*    pReleaseSyncs;\n    const uint64_t*          pReleaseKeys;\n} VkWin32KeyedMutexAcquireReleaseInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::acquire_count`] is the number of entries in the [`Self::p_acquire_syncs`],[`Self::p_acquire_keys`], and [`Self::p_acquire_timeouts`] arrays.\n\n* [`Self::p_acquire_syncs`] is a pointer to an array of [`crate::vk::DeviceMemory`]objects which were imported from Direct3D 11 resources.\n\n* [`Self::p_acquire_keys`] is a pointer to an array of mutex key values to wait\n  for prior to beginning the submitted work.\n  Entries refer to the keyed mutex associated with the corresponding\n  entries in [`Self::p_acquire_syncs`].\n\n* [`Self::p_acquire_timeouts`] is a pointer to an array of timeout values, in\n  millisecond units, for each acquire specified in [`Self::p_acquire_keys`].\n\n* [`Self::release_count`] is the number of entries in the [`Self::p_release_syncs`]and [`Self::p_release_keys`] arrays.\n\n* [`Self::p_release_syncs`] is a pointer to an array of [`crate::vk::DeviceMemory`]objects which were imported from Direct3D 11 resources.\n\n* [`Self::p_release_keys`] is a pointer to an array of mutex key values to set\n  when the submitted work has completed.\n  Entries refer to the keyed mutex associated with the corresponding\n  entries in [`Self::p_release_syncs`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireSyncs-00081  \n   Each member of [`Self::p_acquire_syncs`] and [`Self::p_release_syncs`] **must** be a\n  device memory object imported by setting[`crate::vk::ImportMemoryWin32HandleInfoKHR::handle_type`] to[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE_KMT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR`]\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireSyncs-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_syncs`] **must** be a valid pointer to an array of [`Self::acquire_count`] valid [`crate::vk::DeviceMemory`] handles\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireKeys-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_keys`] **must** be a valid pointer to an array of [`Self::acquire_count`] `uint64_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireTimeouts-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_timeouts`] **must** be a valid pointer to an array of [`Self::acquire_count`] `uint32_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pReleaseSyncs-parameter  \n   If [`Self::release_count`] is not `0`, [`Self::p_release_syncs`] **must** be a valid pointer to an array of [`Self::release_count`] valid [`crate::vk::DeviceMemory`] handles\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pReleaseKeys-parameter  \n   If [`Self::release_count`] is not `0`, [`Self::p_release_keys`] **must** be a valid pointer to an array of [`Self::release_count`] `uint64_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-commonparent  \n   Both of the elements of [`Self::p_acquire_syncs`], and the elements of [`Self::p_release_syncs`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkWin32KeyedMutexAcquireReleaseInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Win32KeyedMutexAcquireReleaseInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub acquire_count: u32,
    pub p_acquire_syncs: *const crate::vk1_0::DeviceMemory,
    pub p_acquire_keys: *const u64,
    pub p_acquire_timeouts: *const u32,
    pub release_count: u32,
    pub p_release_syncs: *const crate::vk1_0::DeviceMemory,
    pub p_release_keys: *const u64,
}
impl Win32KeyedMutexAcquireReleaseInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR;
}
impl Default for Win32KeyedMutexAcquireReleaseInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), acquire_count: Default::default(), p_acquire_syncs: std::ptr::null(), p_acquire_keys: std::ptr::null(), p_acquire_timeouts: std::ptr::null(), release_count: Default::default(), p_release_syncs: std::ptr::null(), p_release_keys: std::ptr::null() }
    }
}
impl std::fmt::Debug for Win32KeyedMutexAcquireReleaseInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Win32KeyedMutexAcquireReleaseInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("acquire_count", &self.acquire_count).field("p_acquire_syncs", &self.p_acquire_syncs).field("p_acquire_keys", &self.p_acquire_keys).field("p_acquire_timeouts", &self.p_acquire_timeouts).field("release_count", &self.release_count).field("p_release_syncs", &self.p_release_syncs).field("p_release_keys", &self.p_release_keys).finish()
    }
}
impl Win32KeyedMutexAcquireReleaseInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
        Win32KeyedMutexAcquireReleaseInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWin32KeyedMutexAcquireReleaseInfoKHR.html)) · Builder of [`Win32KeyedMutexAcquireReleaseInfoKHR`] <br/> VkWin32KeyedMutexAcquireReleaseInfoKHR - Use the Windows keyed mutex mechanism to synchronize work\n[](#_c_specification)C Specification\n----------\n\nWhen submitting work that operates on memory imported from a Direct3D 11\nresource to a queue, the keyed mutex mechanism **may** be used in addition to\nVulkan semaphores to synchronize the work.\nKeyed mutexes are a property of a properly created shareable Direct3D 11\nresource.\nThey **can** only be used if the imported resource was created with the`D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX` flag.\n\nTo acquire keyed mutexes before submitted work and/or release them after,\nadd a [`crate::vk::Win32KeyedMutexAcquireReleaseInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::SubmitInfo`] structure.\n\nThe [`crate::vk::Win32KeyedMutexAcquireReleaseInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_win32_keyed_mutex\ntypedef struct VkWin32KeyedMutexAcquireReleaseInfoKHR {\n    VkStructureType          sType;\n    const void*              pNext;\n    uint32_t                 acquireCount;\n    const VkDeviceMemory*    pAcquireSyncs;\n    const uint64_t*          pAcquireKeys;\n    const uint32_t*          pAcquireTimeouts;\n    uint32_t                 releaseCount;\n    const VkDeviceMemory*    pReleaseSyncs;\n    const uint64_t*          pReleaseKeys;\n} VkWin32KeyedMutexAcquireReleaseInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::acquire_count`] is the number of entries in the [`Self::p_acquire_syncs`],[`Self::p_acquire_keys`], and [`Self::p_acquire_timeouts`] arrays.\n\n* [`Self::p_acquire_syncs`] is a pointer to an array of [`crate::vk::DeviceMemory`]objects which were imported from Direct3D 11 resources.\n\n* [`Self::p_acquire_keys`] is a pointer to an array of mutex key values to wait\n  for prior to beginning the submitted work.\n  Entries refer to the keyed mutex associated with the corresponding\n  entries in [`Self::p_acquire_syncs`].\n\n* [`Self::p_acquire_timeouts`] is a pointer to an array of timeout values, in\n  millisecond units, for each acquire specified in [`Self::p_acquire_keys`].\n\n* [`Self::release_count`] is the number of entries in the [`Self::p_release_syncs`]and [`Self::p_release_keys`] arrays.\n\n* [`Self::p_release_syncs`] is a pointer to an array of [`crate::vk::DeviceMemory`]objects which were imported from Direct3D 11 resources.\n\n* [`Self::p_release_keys`] is a pointer to an array of mutex key values to set\n  when the submitted work has completed.\n  Entries refer to the keyed mutex associated with the corresponding\n  entries in [`Self::p_release_syncs`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireSyncs-00081  \n   Each member of [`Self::p_acquire_syncs`] and [`Self::p_release_syncs`] **must** be a\n  device memory object imported by setting[`crate::vk::ImportMemoryWin32HandleInfoKHR::handle_type`] to[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE_KMT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR`]\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireSyncs-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_syncs`] **must** be a valid pointer to an array of [`Self::acquire_count`] valid [`crate::vk::DeviceMemory`] handles\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireKeys-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_keys`] **must** be a valid pointer to an array of [`Self::acquire_count`] `uint64_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pAcquireTimeouts-parameter  \n   If [`Self::acquire_count`] is not `0`, [`Self::p_acquire_timeouts`] **must** be a valid pointer to an array of [`Self::acquire_count`] `uint32_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pReleaseSyncs-parameter  \n   If [`Self::release_count`] is not `0`, [`Self::p_release_syncs`] **must** be a valid pointer to an array of [`Self::release_count`] valid [`crate::vk::DeviceMemory`] handles\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-pReleaseKeys-parameter  \n   If [`Self::release_count`] is not `0`, [`Self::p_release_keys`] **must** be a valid pointer to an array of [`Self::release_count`] `uint64_t` values\n\n* []() VUID-VkWin32KeyedMutexAcquireReleaseInfoKHR-commonparent  \n   Both of the elements of [`Self::p_acquire_syncs`], and the elements of [`Self::p_release_syncs`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a>(Win32KeyedMutexAcquireReleaseInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
        Win32KeyedMutexAcquireReleaseInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn acquire_syncs(mut self, acquire_syncs: &'a [crate::vk1_0::DeviceMemory]) -> Self {
        self.0.p_acquire_syncs = acquire_syncs.as_ptr() as _;
        self.0.acquire_count = acquire_syncs.len() as _;
        self
    }
    #[inline]
    pub fn acquire_keys(mut self, acquire_keys: &'a [u64]) -> Self {
        self.0.p_acquire_keys = acquire_keys.as_ptr() as _;
        self.0.acquire_count = acquire_keys.len() as _;
        self
    }
    #[inline]
    pub fn acquire_timeouts(mut self, acquire_timeouts: &'a [u32]) -> Self {
        self.0.p_acquire_timeouts = acquire_timeouts.as_ptr() as _;
        self.0.acquire_count = acquire_timeouts.len() as _;
        self
    }
    #[inline]
    pub fn release_syncs(mut self, release_syncs: &'a [crate::vk1_0::DeviceMemory]) -> Self {
        self.0.p_release_syncs = release_syncs.as_ptr() as _;
        self.0.release_count = release_syncs.len() as _;
        self
    }
    #[inline]
    pub fn release_keys(mut self, release_keys: &'a [u64]) -> Self {
        self.0.p_release_keys = release_keys.as_ptr() as _;
        self.0.release_count = release_keys.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> Win32KeyedMutexAcquireReleaseInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
    fn default() -> Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
    type Target = Win32KeyedMutexAcquireReleaseInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromConst<'a, Win32KeyedMutexAcquireReleaseInfoKHR> for crate::extensions::khr_synchronization2::SubmitInfo2KHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, Win32KeyedMutexAcquireReleaseInfoKHRBuilder<'_>> for crate::extensions::khr_synchronization2::SubmitInfo2KHRBuilder<'a> {}
