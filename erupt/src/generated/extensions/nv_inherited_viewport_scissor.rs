#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_INHERITED_VIEWPORT_SCISSOR_SPEC_VERSION")]
pub const NV_INHERITED_VIEWPORT_SCISSOR_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_INHERITED_VIEWPORT_SCISSOR_EXTENSION_NAME")]
pub const NV_INHERITED_VIEWPORT_SCISSOR_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_inherited_viewport_scissor");
#[doc = "Provided by [`crate::extensions::nv_inherited_viewport_scissor`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV: Self = Self(1000278000);
    pub const COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV: Self = Self(1000278001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceInheritedViewportScissorFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceViewportScissorInfoNV> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceViewportScissorInfoNVBuilder<'_>> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceInheritedViewportScissorFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceInheritedViewportScissorFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceInheritedViewportScissorFeaturesNV - Structure describing the viewport scissor inheritance behavior for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_inherited_viewport_scissor\ntypedef struct VkPhysicalDeviceInheritedViewportScissorFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           inheritedViewportScissor2D;\n} VkPhysicalDeviceInheritedViewportScissorFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::inherited_viewport_scissor2_d`] indicates whether secondary command\n  buffers can inherit most of the dynamic state affected by[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`],[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`],[`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`],[`crate::vk::DynamicState::VIEWPORT`] or [`crate::vk::DynamicState::SCISSOR`],\n  from a primary command buffer.\n\nIf the [`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceInheritedViewportScissorFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceInheritedViewportScissorFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceInheritedViewportScissorFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub inherited_viewport_scissor2_d: crate::vk1_0::Bool32,
}
impl PhysicalDeviceInheritedViewportScissorFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV;
}
impl Default for PhysicalDeviceInheritedViewportScissorFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), inherited_viewport_scissor2_d: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceInheritedViewportScissorFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceInheritedViewportScissorFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("inherited_viewport_scissor2_d", &(self.inherited_viewport_scissor2_d != 0)).finish()
    }
}
impl PhysicalDeviceInheritedViewportScissorFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
        PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceInheritedViewportScissorFeaturesNV.html)) · Builder of [`PhysicalDeviceInheritedViewportScissorFeaturesNV`] <br/> VkPhysicalDeviceInheritedViewportScissorFeaturesNV - Structure describing the viewport scissor inheritance behavior for an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_inherited_viewport_scissor\ntypedef struct VkPhysicalDeviceInheritedViewportScissorFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           inheritedViewportScissor2D;\n} VkPhysicalDeviceInheritedViewportScissorFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::inherited_viewport_scissor2_d`] indicates whether secondary command\n  buffers can inherit most of the dynamic state affected by[`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`],[`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`],[`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`],[`crate::vk::DynamicState::VIEWPORT`] or [`crate::vk::DynamicState::SCISSOR`],\n  from a primary command buffer.\n\nIf the [`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceInheritedViewportScissorFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceInheritedViewportScissorFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a>(PhysicalDeviceInheritedViewportScissorFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
        PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn inherited_viewport_scissor2_d(mut self, inherited_viewport_scissor2_d: bool) -> Self {
        self.0.inherited_viewport_scissor2_d = inherited_viewport_scissor2_d as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceInheritedViewportScissorFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceInheritedViewportScissorFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceInheritedViewportScissorFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceViewportScissorInfoNV.html)) · Structure <br/> VkCommandBufferInheritanceViewportScissorInfoNV - Structure specifying command buffer inheritance information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CommandBufferInheritanceViewportScissorInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_inherited_viewport_scissor\ntypedef struct VkCommandBufferInheritanceViewportScissorInfoNV {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkBool32             viewportScissor2D;\n    uint32_t             viewportDepthCount;\n    const VkViewport*    pViewportDepths;\n} VkCommandBufferInheritanceViewportScissorInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::viewport_scissor2_d`] specifies whether the listed dynamic state is\n  inherited.\n\n* [`Self::viewport_depth_count`] specifies the maximum number of viewports to\n  inherit.\n  When [`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`], the behavior is as if\n  this value is zero.\n\n* [`Self::p_viewport_depths`] is a pointer to a [`crate::vk::Viewport`] structure\n  specifying the expected depth range for each inherited viewport.\n[](#_description)Description\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] includes a[`crate::vk::CommandBufferInheritanceViewportScissorInfoNV`] structure, then that\nstructure controls whether a command buffer **can** inherit the following state\nfrom other command buffers:\n\n* [`crate::vk::DynamicState::SCISSOR`]\n\n* [`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`]\n\n* [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`]\n\nas well as the following state, with restrictions on inherited depth values\nand viewport count:\n\n* [`crate::vk::DynamicState::VIEWPORT`]\n\n* [`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`]\n\nIf [`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`], then the command buffer does\nnot inherit the listed dynamic state, and **should** set this state itself.\nIf this structure is not present, the behavior is as if[`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`].\n\nIf [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then the listed dynamic state\nis inherited, and the command buffer **must** not set this\nstate, except that the viewport and scissor count **may** be set by binding a\ngraphics pipeline that does not specify this state as dynamic.\n\n|   |Note<br/><br/>Due to this restriction, applications **should** ensure either all or none of<br/>the graphics pipelines bound in this secondary command buffer use dynamic<br/>viewport/scissor counts.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nWhen the command buffer is executed as part of a the execution of a[`crate::vk::PFN_vkCmdExecuteCommands`] command, the inherited state (if enabled) is\ndetermined by the following procedure, performed separately for each dynamic\nstate, and separately for each value for dynamic state that consists of\nmultiple values (e.g. multiple viewports).\n\n* With i being the index of the executed command buffer in the`pCommandBuffers` array of [`crate::vk::PFN_vkCmdExecuteCommands`], ifi\\>0 and any secondary command buffer from index0 to i−1 modifies the state, the inherited state\n  is provisionally set to the final value set by the last such secondary\n  command buffer.\n  Binding a graphics pipeline that defines the state statically is\n  equivalent to setting the state to an undefined value.\n\n* Otherwise, the tentatative inherited state is that of the primary\n  command buffer at the point the [`crate::vk::PFN_vkCmdExecuteCommands`] command was\n  recorded; if the state is undefined, then so is the provisional\n  inherited state.\n\n* If the provisional inherited state is an undefined value, then the\n  state is not inherited.\n\n* If the provisional inherited state is a viewport, with nbeing its viewport index, then if n≥[`Self::viewport_depth_count`], or if either [`crate::vk::Viewport::min_depth`]or [`crate::vk::Viewport::max_depth`] are not equal to the respective\n  values of the nth element of [`Self::p_viewport_depths`], then\n  the state is not inherited.\n\n* If the provisional inherited state passes both checks, then it becomes\n  the actual inherited state.\n\n|   |Note<br/><br/>There is no support for inheriting dynamic state from a secondary command<br/>buffer executed as part of a different [`crate::vk::PFN_vkCmdExecuteCommands`] command.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04782  \n   If the [inherited viewport\n  scissor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-inheritedViewportScissor2D) feature is not enabled, [`Self::viewport_scissor2_d`] **must** be[`crate::vk::FALSE`]\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04783  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled and [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then[`Self::viewport_depth_count`] **must** be `1`\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04784  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then[`Self::viewport_depth_count`] **must** be greater than `0`\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04785  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then [`Self::p_viewport_depths`]**must** be a valid pointer to an array of [`Self::viewport_depth_count`] valid[`crate::vk::Viewport`] structures, except any requirements on `x`, `y`,`width`, and `height` do not apply.\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04786  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then the command buffer**must** be recorded with the[`crate::vk::CommandBufferUsageFlagBits::RENDER_PASS_CONTINUE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`], [`crate::vk::Viewport`]\n"]
#[doc(alias = "VkCommandBufferInheritanceViewportScissorInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CommandBufferInheritanceViewportScissorInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub viewport_scissor2_d: crate::vk1_0::Bool32,
    pub viewport_depth_count: u32,
    pub p_viewport_depths: *const crate::vk1_0::Viewport,
}
impl CommandBufferInheritanceViewportScissorInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV;
}
impl Default for CommandBufferInheritanceViewportScissorInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), viewport_scissor2_d: Default::default(), viewport_depth_count: Default::default(), p_viewport_depths: std::ptr::null() }
    }
}
impl std::fmt::Debug for CommandBufferInheritanceViewportScissorInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CommandBufferInheritanceViewportScissorInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("viewport_scissor2_d", &(self.viewport_scissor2_d != 0)).field("viewport_depth_count", &self.viewport_depth_count).field("p_viewport_depths", &self.p_viewport_depths).finish()
    }
}
impl CommandBufferInheritanceViewportScissorInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
        CommandBufferInheritanceViewportScissorInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceViewportScissorInfoNV.html)) · Builder of [`CommandBufferInheritanceViewportScissorInfoNV`] <br/> VkCommandBufferInheritanceViewportScissorInfoNV - Structure specifying command buffer inheritance information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CommandBufferInheritanceViewportScissorInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_inherited_viewport_scissor\ntypedef struct VkCommandBufferInheritanceViewportScissorInfoNV {\n    VkStructureType      sType;\n    const void*          pNext;\n    VkBool32             viewportScissor2D;\n    uint32_t             viewportDepthCount;\n    const VkViewport*    pViewportDepths;\n} VkCommandBufferInheritanceViewportScissorInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::viewport_scissor2_d`] specifies whether the listed dynamic state is\n  inherited.\n\n* [`Self::viewport_depth_count`] specifies the maximum number of viewports to\n  inherit.\n  When [`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`], the behavior is as if\n  this value is zero.\n\n* [`Self::p_viewport_depths`] is a pointer to a [`crate::vk::Viewport`] structure\n  specifying the expected depth range for each inherited viewport.\n[](#_description)Description\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] includes a[`crate::vk::CommandBufferInheritanceViewportScissorInfoNV`] structure, then that\nstructure controls whether a command buffer **can** inherit the following state\nfrom other command buffers:\n\n* [`crate::vk::DynamicState::SCISSOR`]\n\n* [`crate::vk::DynamicState::SCISSOR_WITH_COUNT_EXT`]\n\n* [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`]\n\nas well as the following state, with restrictions on inherited depth values\nand viewport count:\n\n* [`crate::vk::DynamicState::VIEWPORT`]\n\n* [`crate::vk::DynamicState::VIEWPORT_WITH_COUNT_EXT`]\n\nIf [`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`], then the command buffer does\nnot inherit the listed dynamic state, and **should** set this state itself.\nIf this structure is not present, the behavior is as if[`Self::viewport_scissor2_d`] is [`crate::vk::FALSE`].\n\nIf [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then the listed dynamic state\nis inherited, and the command buffer **must** not set this\nstate, except that the viewport and scissor count **may** be set by binding a\ngraphics pipeline that does not specify this state as dynamic.\n\n|   |Note<br/><br/>Due to this restriction, applications **should** ensure either all or none of<br/>the graphics pipelines bound in this secondary command buffer use dynamic<br/>viewport/scissor counts.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nWhen the command buffer is executed as part of a the execution of a[`crate::vk::PFN_vkCmdExecuteCommands`] command, the inherited state (if enabled) is\ndetermined by the following procedure, performed separately for each dynamic\nstate, and separately for each value for dynamic state that consists of\nmultiple values (e.g. multiple viewports).\n\n* With i being the index of the executed command buffer in the`pCommandBuffers` array of [`crate::vk::PFN_vkCmdExecuteCommands`], ifi\\>0 and any secondary command buffer from index0 to i−1 modifies the state, the inherited state\n  is provisionally set to the final value set by the last such secondary\n  command buffer.\n  Binding a graphics pipeline that defines the state statically is\n  equivalent to setting the state to an undefined value.\n\n* Otherwise, the tentatative inherited state is that of the primary\n  command buffer at the point the [`crate::vk::PFN_vkCmdExecuteCommands`] command was\n  recorded; if the state is undefined, then so is the provisional\n  inherited state.\n\n* If the provisional inherited state is an undefined value, then the\n  state is not inherited.\n\n* If the provisional inherited state is a viewport, with nbeing its viewport index, then if n≥[`Self::viewport_depth_count`], or if either [`crate::vk::Viewport::min_depth`]or [`crate::vk::Viewport::max_depth`] are not equal to the respective\n  values of the nth element of [`Self::p_viewport_depths`], then\n  the state is not inherited.\n\n* If the provisional inherited state passes both checks, then it becomes\n  the actual inherited state.\n\n|   |Note<br/><br/>There is no support for inheriting dynamic state from a secondary command<br/>buffer executed as part of a different [`crate::vk::PFN_vkCmdExecuteCommands`] command.|\n|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04782  \n   If the [inherited viewport\n  scissor](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-inheritedViewportScissor2D) feature is not enabled, [`Self::viewport_scissor2_d`] **must** be[`crate::vk::FALSE`]\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04783  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled and [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then[`Self::viewport_depth_count`] **must** be `1`\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04784  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then[`Self::viewport_depth_count`] **must** be greater than `0`\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04785  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then [`Self::p_viewport_depths`]**must** be a valid pointer to an array of [`Self::viewport_depth_count`] valid[`crate::vk::Viewport`] structures, except any requirements on `x`, `y`,`width`, and `height` do not apply.\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-viewportScissor2D-04786  \n   If [`Self::viewport_scissor2_d`] is [`crate::vk::TRUE`], then the command buffer**must** be recorded with the[`crate::vk::CommandBufferUsageFlagBits::RENDER_PASS_CONTINUE`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceViewportScissorInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`], [`crate::vk::Viewport`]\n"]
#[repr(transparent)]
pub struct CommandBufferInheritanceViewportScissorInfoNVBuilder<'a>(CommandBufferInheritanceViewportScissorInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
        CommandBufferInheritanceViewportScissorInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn viewport_scissor2_d(mut self, viewport_scissor2_d: bool) -> Self {
        self.0.viewport_scissor2_d = viewport_scissor2_d as _;
        self
    }
    #[inline]
    pub fn viewport_depth_count(mut self, viewport_depth_count: u32) -> Self {
        self.0.viewport_depth_count = viewport_depth_count as _;
        self
    }
    #[inline]
    pub fn viewport_depths(mut self, viewport_depths: &'a crate::vk1_0::Viewport) -> Self {
        self.0.p_viewport_depths = viewport_depths as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CommandBufferInheritanceViewportScissorInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
    fn default() -> CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
    type Target = CommandBufferInheritanceViewportScissorInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CommandBufferInheritanceViewportScissorInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
