#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CONSERVATIVE_RASTERIZATION_SPEC_VERSION")]
pub const EXT_CONSERVATIVE_RASTERIZATION_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CONSERVATIVE_RASTERIZATION_EXTENSION_NAME")]
pub const EXT_CONSERVATIVE_RASTERIZATION_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_conservative_rasterization");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationConservativeStateCreateFlagsEXT.html)) · Bitmask of [`PipelineRasterizationConservativeStateCreateFlagBitsEXT`] <br/> VkPipelineRasterizationConservativeStateCreateFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef VkFlags VkPipelineRasterizationConservativeStateCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineRasterizationConservativeStateCreateFlagBitsEXT`] is a bitmask\ntype for setting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`]\n"] # [doc (alias = "VkPipelineRasterizationConservativeStateCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct PipelineRasterizationConservativeStateCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineRasterizationConservativeStateCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkPipelineRasterizationConservativeStateCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineRasterizationConservativeStateCreateFlagBitsEXT(pub u32);
impl PipelineRasterizationConservativeStateCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineRasterizationConservativeStateCreateFlagsEXT {
        PipelineRasterizationConservativeStateCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineRasterizationConservativeStateCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_conservative_rasterization`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT: Self = Self(1000101000);
    pub const PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT: Self = Self(1000101001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConservativeRasterizationModeEXT.html)) · Enum <br/> VkConservativeRasterizationModeEXT - Specify the conservative rasterization mode\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT::conservative_rasterization_mode`],\nspecifying the conservative rasterization mode are:\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef enum VkConservativeRasterizationModeEXT {\n    VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT = 0,\n    VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT = 1,\n    VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT = 2,\n} VkConservativeRasterizationModeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::DISABLED_EXT`] specifies that\n  conservative rasterization is disabled and rasterization proceeds as\n  normal.\n\n* [`Self::OVERESTIMATE_EXT`] specifies that\n  conservative rasterization is enabled in overestimation mode.\n\n* [`Self::UNDERESTIMATE_EXT`] specifies\n  that conservative rasterization is enabled in underestimation mode.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`]\n"]
#[doc(alias = "VkConservativeRasterizationModeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ConservativeRasterizationModeEXT(pub i32);
impl std::fmt::Debug for ConservativeRasterizationModeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DISABLED_EXT => "DISABLED_EXT",
            &Self::OVERESTIMATE_EXT => "OVERESTIMATE_EXT",
            &Self::UNDERESTIMATE_EXT => "UNDERESTIMATE_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_conservative_rasterization`]"]
impl crate::extensions::ext_conservative_rasterization::ConservativeRasterizationModeEXT {
    pub const DISABLED_EXT: Self = Self(0);
    pub const OVERESTIMATE_EXT: Self = Self(1);
    pub const UNDERESTIMATE_EXT: Self = Self(2);
}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationConservativeStateCreateInfoEXT> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceConservativeRasterizationPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceConservativeRasterizationPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceConservativeRasterizationPropertiesEXT - Structure describing conservative raster properties that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`] structure\nis defined as:\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef struct VkPhysicalDeviceConservativeRasterizationPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    float              primitiveOverestimationSize;\n    float              maxExtraPrimitiveOverestimationSize;\n    float              extraPrimitiveOverestimationSizeGranularity;\n    VkBool32           primitiveUnderestimation;\n    VkBool32           conservativePointAndLineRasterization;\n    VkBool32           degenerateTrianglesRasterized;\n    VkBool32           degenerateLinesRasterized;\n    VkBool32           fullyCoveredFragmentShaderInputVariable;\n    VkBool32           conservativeRasterizationPostDepthCoverage;\n} VkPhysicalDeviceConservativeRasterizationPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::primitive_overestimation_size`]is the size in pixels the generating primitive is increased at each of\n  its edges during conservative rasterization overestimation mode.\n  Even with a size of 0.0, conservative rasterization overestimation rules\n  still apply and if any part of the pixel rectangle is covered by the\n  generating primitive, fragments are generated for the entire pixel.\n  However implementations **may** make the pixel coverage area even more\n  conservative by increasing the size of the generating primitive.\n\n* []()[`Self::max_extra_primitive_overestimation_size`] is the maximum size in pixels\n  of extra overestimation the implementation supports in the pipeline\n  state.\n  A value of 0.0 means the implementation does not support any additional\n  overestimation of the generating primitive during conservative\n  rasterization.\n  A value above 0.0 allows the application to further increase the size of\n  the generating primitive during conservative rasterization\n  overestimation.\n\n* []()[`Self::extra_primitive_overestimation_size_granularity`] is the granularity of\n  extra overestimation that can be specified in the pipeline state between\n  0.0 and [`Self::max_extra_primitive_overestimation_size`] inclusive.\n  A value of 0.0 means the implementation can use the smallest\n  representable non-zero value in the screen space pixel fixed-point grid.\n\n* []() [`Self::primitive_underestimation`] is[`crate::vk::TRUE`] if the implementation supports the[`crate::vk::ConservativeRasterizationModeEXT::UNDERESTIMATE_EXT`] conservative\n  rasterization mode in addition to[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`].\n  Otherwise the implementation only supports[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`].\n\n* []()[`Self::conservative_point_and_line_rasterization`] is [`crate::vk::TRUE`] if the\n  implementation supports conservative rasterization of point and line\n  primitives as well as triangle primitives.\n  Otherwise the implementation only supports triangle primitives.\n\n* []()[`Self::degenerate_triangles_rasterized`] is [`crate::vk::FALSE`] if the\n  implementation culls primitives generated from triangles that become\n  zero area after they are quantized to the fixed-point rasterization\n  pixel grid.[`Self::degenerate_triangles_rasterized`] is [`crate::vk::TRUE`] if these primitives\n  are not culled and the provoking vertex attributes and depth value are\n  used for the fragments.\n  The primitive area calculation is done on the primitive generated from\n  the clipped triangle if applicable.\n  Zero area primitives are backfacing and the application **can** enable\n  backface culling if desired.\n\n* []() [`Self::degenerate_lines_rasterized`] is[`crate::vk::FALSE`] if the implementation culls lines that become zero length\n  after they are quantized to the fixed-point rasterization pixel grid.[`Self::degenerate_lines_rasterized`] is [`crate::vk::TRUE`] if zero length lines\n  are not culled and the provoking vertex attributes and depth value are\n  used for the fragments.\n\n* []()[`Self::fully_covered_fragment_shader_input_variable`] is [`crate::vk::TRUE`] if the\n  implementation supports the SPIR-V builtin fragment shader input\n  variable `FullyCoveredEXT` which specifies that conservative\n  rasterization is enabled and the fragment area is fully covered by the\n  generating primitive.\n\n* []()[`Self::conservative_rasterization_post_depth_coverage`] is [`crate::vk::TRUE`] if the\n  implementation supports conservative rasterization with the[`PostDepthCoverage`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#shaders-fragment-earlytest-postdepthcoverage)execution mode enabled.\n  When supported the `SampleMask` built-in input variable will reflect\n  the coverage after the early per-fragment depth and stencil tests are\n  applied even when conservative rasterization is enabled.\n  Otherwise[`PostDepthCoverage`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#shaders-fragment-earlytest-postdepthcoverage)execution mode **must** not be used when conservative rasterization is\n  enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceConservativeRasterizationPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceConservativeRasterizationPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceConservativeRasterizationPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub primitive_overestimation_size: std::os::raw::c_float,
    pub max_extra_primitive_overestimation_size: std::os::raw::c_float,
    pub extra_primitive_overestimation_size_granularity: std::os::raw::c_float,
    pub primitive_underestimation: crate::vk1_0::Bool32,
    pub conservative_point_and_line_rasterization: crate::vk1_0::Bool32,
    pub degenerate_triangles_rasterized: crate::vk1_0::Bool32,
    pub degenerate_lines_rasterized: crate::vk1_0::Bool32,
    pub fully_covered_fragment_shader_input_variable: crate::vk1_0::Bool32,
    pub conservative_rasterization_post_depth_coverage: crate::vk1_0::Bool32,
}
impl PhysicalDeviceConservativeRasterizationPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceConservativeRasterizationPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), primitive_overestimation_size: Default::default(), max_extra_primitive_overestimation_size: Default::default(), extra_primitive_overestimation_size_granularity: Default::default(), primitive_underestimation: Default::default(), conservative_point_and_line_rasterization: Default::default(), degenerate_triangles_rasterized: Default::default(), degenerate_lines_rasterized: Default::default(), fully_covered_fragment_shader_input_variable: Default::default(), conservative_rasterization_post_depth_coverage: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceConservativeRasterizationPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceConservativeRasterizationPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("primitive_overestimation_size", &self.primitive_overestimation_size).field("max_extra_primitive_overestimation_size", &self.max_extra_primitive_overestimation_size).field("extra_primitive_overestimation_size_granularity", &self.extra_primitive_overestimation_size_granularity).field("primitive_underestimation", &(self.primitive_underestimation != 0)).field("conservative_point_and_line_rasterization", &(self.conservative_point_and_line_rasterization != 0)).field("degenerate_triangles_rasterized", &(self.degenerate_triangles_rasterized != 0)).field("degenerate_lines_rasterized", &(self.degenerate_lines_rasterized != 0)).field("fully_covered_fragment_shader_input_variable", &(self.fully_covered_fragment_shader_input_variable != 0)).field("conservative_rasterization_post_depth_coverage", &(self.conservative_rasterization_post_depth_coverage != 0)).finish()
    }
}
impl PhysicalDeviceConservativeRasterizationPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
        PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceConservativeRasterizationPropertiesEXT.html)) · Builder of [`PhysicalDeviceConservativeRasterizationPropertiesEXT`] <br/> VkPhysicalDeviceConservativeRasterizationPropertiesEXT - Structure describing conservative raster properties that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`] structure\nis defined as:\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef struct VkPhysicalDeviceConservativeRasterizationPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    float              primitiveOverestimationSize;\n    float              maxExtraPrimitiveOverestimationSize;\n    float              extraPrimitiveOverestimationSizeGranularity;\n    VkBool32           primitiveUnderestimation;\n    VkBool32           conservativePointAndLineRasterization;\n    VkBool32           degenerateTrianglesRasterized;\n    VkBool32           degenerateLinesRasterized;\n    VkBool32           fullyCoveredFragmentShaderInputVariable;\n    VkBool32           conservativeRasterizationPostDepthCoverage;\n} VkPhysicalDeviceConservativeRasterizationPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::primitive_overestimation_size`]is the size in pixels the generating primitive is increased at each of\n  its edges during conservative rasterization overestimation mode.\n  Even with a size of 0.0, conservative rasterization overestimation rules\n  still apply and if any part of the pixel rectangle is covered by the\n  generating primitive, fragments are generated for the entire pixel.\n  However implementations **may** make the pixel coverage area even more\n  conservative by increasing the size of the generating primitive.\n\n* []()[`Self::max_extra_primitive_overestimation_size`] is the maximum size in pixels\n  of extra overestimation the implementation supports in the pipeline\n  state.\n  A value of 0.0 means the implementation does not support any additional\n  overestimation of the generating primitive during conservative\n  rasterization.\n  A value above 0.0 allows the application to further increase the size of\n  the generating primitive during conservative rasterization\n  overestimation.\n\n* []()[`Self::extra_primitive_overestimation_size_granularity`] is the granularity of\n  extra overestimation that can be specified in the pipeline state between\n  0.0 and [`Self::max_extra_primitive_overestimation_size`] inclusive.\n  A value of 0.0 means the implementation can use the smallest\n  representable non-zero value in the screen space pixel fixed-point grid.\n\n* []() [`Self::primitive_underestimation`] is[`crate::vk::TRUE`] if the implementation supports the[`crate::vk::ConservativeRasterizationModeEXT::UNDERESTIMATE_EXT`] conservative\n  rasterization mode in addition to[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`].\n  Otherwise the implementation only supports[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`].\n\n* []()[`Self::conservative_point_and_line_rasterization`] is [`crate::vk::TRUE`] if the\n  implementation supports conservative rasterization of point and line\n  primitives as well as triangle primitives.\n  Otherwise the implementation only supports triangle primitives.\n\n* []()[`Self::degenerate_triangles_rasterized`] is [`crate::vk::FALSE`] if the\n  implementation culls primitives generated from triangles that become\n  zero area after they are quantized to the fixed-point rasterization\n  pixel grid.[`Self::degenerate_triangles_rasterized`] is [`crate::vk::TRUE`] if these primitives\n  are not culled and the provoking vertex attributes and depth value are\n  used for the fragments.\n  The primitive area calculation is done on the primitive generated from\n  the clipped triangle if applicable.\n  Zero area primitives are backfacing and the application **can** enable\n  backface culling if desired.\n\n* []() [`Self::degenerate_lines_rasterized`] is[`crate::vk::FALSE`] if the implementation culls lines that become zero length\n  after they are quantized to the fixed-point rasterization pixel grid.[`Self::degenerate_lines_rasterized`] is [`crate::vk::TRUE`] if zero length lines\n  are not culled and the provoking vertex attributes and depth value are\n  used for the fragments.\n\n* []()[`Self::fully_covered_fragment_shader_input_variable`] is [`crate::vk::TRUE`] if the\n  implementation supports the SPIR-V builtin fragment shader input\n  variable `FullyCoveredEXT` which specifies that conservative\n  rasterization is enabled and the fragment area is fully covered by the\n  generating primitive.\n\n* []()[`Self::conservative_rasterization_post_depth_coverage`] is [`crate::vk::TRUE`] if the\n  implementation supports conservative rasterization with the[`PostDepthCoverage`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#shaders-fragment-earlytest-postdepthcoverage)execution mode enabled.\n  When supported the `SampleMask` built-in input variable will reflect\n  the coverage after the early per-fragment depth and stencil tests are\n  applied even when conservative rasterization is enabled.\n  Otherwise[`PostDepthCoverage`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#shaders-fragment-earlytest-postdepthcoverage)execution mode **must** not be used when conservative rasterization is\n  enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceConservativeRasterizationPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a>(PhysicalDeviceConservativeRasterizationPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
        PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn primitive_overestimation_size(mut self, primitive_overestimation_size: std::os::raw::c_float) -> Self {
        self.0.primitive_overestimation_size = primitive_overestimation_size as _;
        self
    }
    #[inline]
    pub fn max_extra_primitive_overestimation_size(mut self, max_extra_primitive_overestimation_size: std::os::raw::c_float) -> Self {
        self.0.max_extra_primitive_overestimation_size = max_extra_primitive_overestimation_size as _;
        self
    }
    #[inline]
    pub fn extra_primitive_overestimation_size_granularity(mut self, extra_primitive_overestimation_size_granularity: std::os::raw::c_float) -> Self {
        self.0.extra_primitive_overestimation_size_granularity = extra_primitive_overestimation_size_granularity as _;
        self
    }
    #[inline]
    pub fn primitive_underestimation(mut self, primitive_underestimation: bool) -> Self {
        self.0.primitive_underestimation = primitive_underestimation as _;
        self
    }
    #[inline]
    pub fn conservative_point_and_line_rasterization(mut self, conservative_point_and_line_rasterization: bool) -> Self {
        self.0.conservative_point_and_line_rasterization = conservative_point_and_line_rasterization as _;
        self
    }
    #[inline]
    pub fn degenerate_triangles_rasterized(mut self, degenerate_triangles_rasterized: bool) -> Self {
        self.0.degenerate_triangles_rasterized = degenerate_triangles_rasterized as _;
        self
    }
    #[inline]
    pub fn degenerate_lines_rasterized(mut self, degenerate_lines_rasterized: bool) -> Self {
        self.0.degenerate_lines_rasterized = degenerate_lines_rasterized as _;
        self
    }
    #[inline]
    pub fn fully_covered_fragment_shader_input_variable(mut self, fully_covered_fragment_shader_input_variable: bool) -> Self {
        self.0.fully_covered_fragment_shader_input_variable = fully_covered_fragment_shader_input_variable as _;
        self
    }
    #[inline]
    pub fn conservative_rasterization_post_depth_coverage(mut self, conservative_rasterization_post_depth_coverage: bool) -> Self {
        self.0.conservative_rasterization_post_depth_coverage = conservative_rasterization_post_depth_coverage as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceConservativeRasterizationPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceConservativeRasterizationPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceConservativeRasterizationPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationConservativeStateCreateInfoEXT.html)) · Structure <br/> VkPipelineRasterizationConservativeStateCreateInfoEXT - Structure specifying conservative raster state\n[](#_c_specification)C Specification\n----------\n\nPolygon rasterization **can** be made conservative by setting[`Self::conservative_rasterization_mode`] to[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`] or[`crate::vk::ConservativeRasterizationModeEXT::UNDERESTIMATE_EXT`] in[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`].\nThe [`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`] state is set\nby adding this structure to the [`Self::p_next`] chain of a[`crate::vk::PipelineRasterizationStateCreateInfo`] structure when creating the\ngraphics pipeline.\nEnabling these modes also affects line and point rasterization if the\nimplementation sets[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`conservativePointAndLineRasterization`to [`crate::vk::TRUE`].\n\n[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`] is defined as:\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef struct VkPipelineRasterizationConservativeStateCreateInfoEXT {\n    VkStructureType                                           sType;\n    const void*                                               pNext;\n    VkPipelineRasterizationConservativeStateCreateFlagsEXT    flags;\n    VkConservativeRasterizationModeEXT                        conservativeRasterizationMode;\n    float                                                     extraPrimitiveOverestimationSize;\n} VkPipelineRasterizationConservativeStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::conservative_rasterization_mode`] is the conservative rasterization\n  mode to use.\n\n* [`Self::extra_primitive_overestimation_size`] is the extra size in pixels to\n  increase the generating primitive during conservative rasterization at\n  each of its edges in `X` and `Y` equally in screen space beyond the base\n  overestimation specified in[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`primitiveOverestimationSize`.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-extraPrimitiveOverestimationSize-01769  \n  [`Self::extra_primitive_overestimation_size`] **must** be in the range of `0.0` to[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`maxExtraPrimitiveOverestimationSize`inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-conservativeRasterizationMode-parameter  \n  [`Self::conservative_rasterization_mode`] **must** be a valid [`crate::vk::ConservativeRasterizationModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ConservativeRasterizationModeEXT`], [`crate::vk::PipelineRasterizationConservativeStateCreateFlagBitsEXT`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineRasterizationConservativeStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineRasterizationConservativeStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_conservative_rasterization::PipelineRasterizationConservativeStateCreateFlagsEXT,
    pub conservative_rasterization_mode: crate::extensions::ext_conservative_rasterization::ConservativeRasterizationModeEXT,
    pub extra_primitive_overestimation_size: std::os::raw::c_float,
}
impl PipelineRasterizationConservativeStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineRasterizationConservativeStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), conservative_rasterization_mode: Default::default(), extra_primitive_overestimation_size: Default::default() }
    }
}
impl std::fmt::Debug for PipelineRasterizationConservativeStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineRasterizationConservativeStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("conservative_rasterization_mode", &self.conservative_rasterization_mode).field("extra_primitive_overestimation_size", &self.extra_primitive_overestimation_size).finish()
    }
}
impl PipelineRasterizationConservativeStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationConservativeStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationConservativeStateCreateInfoEXT.html)) · Builder of [`PipelineRasterizationConservativeStateCreateInfoEXT`] <br/> VkPipelineRasterizationConservativeStateCreateInfoEXT - Structure specifying conservative raster state\n[](#_c_specification)C Specification\n----------\n\nPolygon rasterization **can** be made conservative by setting[`Self::conservative_rasterization_mode`] to[`crate::vk::ConservativeRasterizationModeEXT::OVERESTIMATE_EXT`] or[`crate::vk::ConservativeRasterizationModeEXT::UNDERESTIMATE_EXT`] in[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`].\nThe [`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`] state is set\nby adding this structure to the [`Self::p_next`] chain of a[`crate::vk::PipelineRasterizationStateCreateInfo`] structure when creating the\ngraphics pipeline.\nEnabling these modes also affects line and point rasterization if the\nimplementation sets[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`conservativePointAndLineRasterization`to [`crate::vk::TRUE`].\n\n[`crate::vk::PipelineRasterizationConservativeStateCreateInfoEXT`] is defined as:\n\n```\n// Provided by VK_EXT_conservative_rasterization\ntypedef struct VkPipelineRasterizationConservativeStateCreateInfoEXT {\n    VkStructureType                                           sType;\n    const void*                                               pNext;\n    VkPipelineRasterizationConservativeStateCreateFlagsEXT    flags;\n    VkConservativeRasterizationModeEXT                        conservativeRasterizationMode;\n    float                                                     extraPrimitiveOverestimationSize;\n} VkPipelineRasterizationConservativeStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::conservative_rasterization_mode`] is the conservative rasterization\n  mode to use.\n\n* [`Self::extra_primitive_overestimation_size`] is the extra size in pixels to\n  increase the generating primitive during conservative rasterization at\n  each of its edges in `X` and `Y` equally in screen space beyond the base\n  overestimation specified in[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`primitiveOverestimationSize`.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-extraPrimitiveOverestimationSize-01769  \n  [`Self::extra_primitive_overestimation_size`] **must** be in the range of `0.0` to[`crate::vk::PhysicalDeviceConservativeRasterizationPropertiesEXT`]::`maxExtraPrimitiveOverestimationSize`inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineRasterizationConservativeStateCreateInfoEXT-conservativeRasterizationMode-parameter  \n  [`Self::conservative_rasterization_mode`] **must** be a valid [`crate::vk::ConservativeRasterizationModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ConservativeRasterizationModeEXT`], [`crate::vk::PipelineRasterizationConservativeStateCreateFlagBitsEXT`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a>(PipelineRasterizationConservativeStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
        PipelineRasterizationConservativeStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_conservative_rasterization::PipelineRasterizationConservativeStateCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn conservative_rasterization_mode(mut self, conservative_rasterization_mode: crate::extensions::ext_conservative_rasterization::ConservativeRasterizationModeEXT) -> Self {
        self.0.conservative_rasterization_mode = conservative_rasterization_mode as _;
        self
    }
    #[inline]
    pub fn extra_primitive_overestimation_size(mut self, extra_primitive_overestimation_size: std::os::raw::c_float) -> Self {
        self.0.extra_primitive_overestimation_size = extra_primitive_overestimation_size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineRasterizationConservativeStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineRasterizationConservativeStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineRasterizationConservativeStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
