#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WIN32_SURFACE_SPEC_VERSION")]
pub const KHR_WIN32_SURFACE_SPEC_VERSION: u32 = 6;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WIN32_SURFACE_EXTENSION_NAME")]
pub const KHR_WIN32_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_win32_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_WIN32_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateWin32SurfaceKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_WIN32_PRESENTATION_SUPPORT_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceWin32PresentationSupportKHR");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWin32SurfaceCreateFlagsKHR.html)) · Bitmask of [`Win32SurfaceCreateFlagBitsKHR`] <br/> VkWin32SurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_win32_surface\ntypedef VkFlags VkWin32SurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::Win32SurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask, but\nis currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Win32SurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkWin32SurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct Win32SurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`Win32SurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkWin32SurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct Win32SurfaceCreateFlagBitsKHR(pub u32);
impl Win32SurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> Win32SurfaceCreateFlagsKHR {
        Win32SurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for Win32SurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_win32_surface`]"]
impl crate::vk1_0::StructureType {
    pub const WIN32_SURFACE_CREATE_INFO_KHR: Self = Self(1000009000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateWin32SurfaceKHR.html)) · Function <br/> vkCreateWin32SurfaceKHR - Create a slink:VkSurfaceKHR object for an Win32 native window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Win32 window, call:\n\n```\n// Provided by VK_KHR_win32_surface\nVkResult vkCreateWin32SurfaceKHR(\n    VkInstance                                  instance,\n    const VkWin32SurfaceCreateInfoKHR*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::Win32SurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateWin32SurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateWin32SurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::Win32SurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateWin32SurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateWin32SurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::Win32SurfaceCreateInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateWin32SurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_win32_surface::Win32SurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceWin32PresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceWin32PresentationSupportKHR - query queue family support for presentation on a Win32 display\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to the Microsoft Windows desktop, call:\n\n```\n// Provided by VK_KHR_win32_surface\nVkBool32 vkGetPhysicalDeviceWin32PresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceWin32PresentationSupportKHR-queueFamilyIndex-01309  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceWin32PresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceWin32PresentationSupportKHR = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32) -> crate::vk1_0::Bool32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWin32SurfaceCreateInfoKHR.html)) · Structure <br/> VkWin32SurfaceCreateInfoKHR - Structure specifying parameters of a newly created Win32 surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::Win32SurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_win32_surface\ntypedef struct VkWin32SurfaceCreateInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkWin32SurfaceCreateFlagsKHR    flags;\n    HINSTANCE                       hinstance;\n    HWND                            hwnd;\n} VkWin32SurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::hinstance`] is the Win32 `HINSTANCE` for the window to associate\n  the surface with.\n\n* [`Self::hwnd`] is the Win32 `HWND` for the window to associate the\n  surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-hinstance-01307  \n  [`Self::hinstance`] **must** be a valid Win32 `HINSTANCE`\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-hwnd-01308  \n  [`Self::hwnd`] **must** be a valid Win32 `HWND`\n\nValid Usage (Implicit)\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WIN32_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::Win32SurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_win32_surface_khr`]\n"]
#[doc(alias = "VkWin32SurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Win32SurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_win32_surface::Win32SurfaceCreateFlagsKHR,
    pub hinstance: *mut std::ffi::c_void,
    pub hwnd: *mut std::ffi::c_void,
}
impl Win32SurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::WIN32_SURFACE_CREATE_INFO_KHR;
}
impl Default for Win32SurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), hinstance: std::ptr::null_mut(), hwnd: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for Win32SurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Win32SurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("hinstance", &self.hinstance).field("hwnd", &self.hwnd).finish()
    }
}
impl Win32SurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> Win32SurfaceCreateInfoKHRBuilder<'a> {
        Win32SurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkWin32SurfaceCreateInfoKHR.html)) · Builder of [`Win32SurfaceCreateInfoKHR`] <br/> VkWin32SurfaceCreateInfoKHR - Structure specifying parameters of a newly created Win32 surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::Win32SurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_win32_surface\ntypedef struct VkWin32SurfaceCreateInfoKHR {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkWin32SurfaceCreateFlagsKHR    flags;\n    HINSTANCE                       hinstance;\n    HWND                            hwnd;\n} VkWin32SurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::hinstance`] is the Win32 `HINSTANCE` for the window to associate\n  the surface with.\n\n* [`Self::hwnd`] is the Win32 `HWND` for the window to associate the\n  surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-hinstance-01307  \n  [`Self::hinstance`] **must** be a valid Win32 `HINSTANCE`\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-hwnd-01308  \n  [`Self::hwnd`] **must** be a valid Win32 `HWND`\n\nValid Usage (Implicit)\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::WIN32_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkWin32SurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::Win32SurfaceCreateFlagBitsKHR`], [`crate::vk::InstanceLoader::create_win32_surface_khr`]\n"]
#[repr(transparent)]
pub struct Win32SurfaceCreateInfoKHRBuilder<'a>(Win32SurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> Win32SurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> Win32SurfaceCreateInfoKHRBuilder<'a> {
        Win32SurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_win32_surface::Win32SurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn hinstance(mut self, hinstance: *mut std::ffi::c_void) -> Self {
        self.0.hinstance = hinstance;
        self
    }
    #[inline]
    pub fn hwnd(mut self, hwnd: *mut std::ffi::c_void) -> Self {
        self.0.hwnd = hwnd;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> Win32SurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for Win32SurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> Win32SurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for Win32SurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for Win32SurfaceCreateInfoKHRBuilder<'a> {
    type Target = Win32SurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for Win32SurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_win32_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateWin32SurfaceKHR.html)) · Function <br/> vkCreateWin32SurfaceKHR - Create a slink:VkSurfaceKHR object for an Win32 native window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for a Win32 window, call:\n\n```\n// Provided by VK_KHR_win32_surface\nVkResult vkCreateWin32SurfaceKHR(\n    VkInstance                                  instance,\n    const VkWin32SurfaceCreateInfoKHR*          pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::Win32SurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateWin32SurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateWin32SurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::Win32SurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateWin32SurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateWin32SurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`], [`crate::vk::Win32SurfaceCreateInfoKHR`]\n"]
    #[doc(alias = "vkCreateWin32SurfaceKHR")]
    pub unsafe fn create_win32_surface_khr(&self, create_info: &crate::extensions::khr_win32_surface::Win32SurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_win32_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceWin32PresentationSupportKHR.html)) · Function <br/> vkGetPhysicalDeviceWin32PresentationSupportKHR - query queue family support for presentation on a Win32 display\n[](#_c_specification)C Specification\n----------\n\nTo determine whether a queue family of a physical device supports\npresentation to the Microsoft Windows desktop, call:\n\n```\n// Provided by VK_KHR_win32_surface\nVkBool32 vkGetPhysicalDeviceWin32PresentationSupportKHR(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t                                    queueFamilyIndex);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device.\n\n* [`Self::queue_family_index`] is the queue family index.\n[](#_description)Description\n----------\n\nThis platform-specific function **can** be called prior to creating a surface.\n\nValid Usage\n\n* []() VUID-vkGetPhysicalDeviceWin32PresentationSupportKHR-queueFamilyIndex-01309  \n  [`Self::queue_family_index`] **must** be less than`pQueueFamilyPropertyCount` returned by[`crate::vk::PFN_vkGetPhysicalDeviceQueueFamilyProperties`] for the given[`Self::physical_device`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceWin32PresentationSupportKHR-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceWin32PresentationSupportKHR")]
    pub unsafe fn get_physical_device_win32_presentation_support_khr(&self, physical_device: crate::vk1_0::PhysicalDevice, queue_family_index: u32) -> bool {
        let _function = self.get_physical_device_win32_presentation_support_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(physical_device as _, queue_family_index as _);
        _return != 0
    }
}
