#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_MEMORY_WIN32_SPEC_VERSION")]
pub const KHR_EXTERNAL_MEMORY_WIN32_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME")]
pub const KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_external_memory_win32");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_WIN32_HANDLE_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryWin32HandleKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_WIN32_HANDLE_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryWin32HandlePropertiesKHR");
#[doc = "Provided by [`crate::extensions::khr_external_memory_win32`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR: Self = Self(1000073000);
    pub const EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR: Self = Self(1000073001);
    pub const MEMORY_WIN32_HANDLE_PROPERTIES_KHR: Self = Self(1000073002);
    pub const MEMORY_GET_WIN32_HANDLE_INFO_KHR: Self = Self(1000073003);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandleKHR.html)) · Function <br/> vkGetMemoryWin32HandleKHR - Get a Windows HANDLE for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the payload of a Vulkan device\nmemory object, call:\n\n```\n// Provided by VK_KHR_external_memory_win32\nVkResult vkGetMemoryWin32HandleKHR(\n    VkDevice                                    device,\n    const VkMemoryGetWin32HandleInfoKHR*        pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::MemoryGetWin32HandleInfoKHR`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the payload of\n  the device memory object.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_memory_win32_handle_khr`] are owned by the application and hold a\nreference to their payload.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\n|   |Note<br/><br/>Non-NT handle types do not add a reference to their associated payload.<br/>If the original object owning the payload is destroyed, all resources and<br/>handles sharing that payload will become invalid.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetMemoryWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetWin32HandleInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryWin32HandleKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_win32_handle_info: *const crate::extensions::khr_external_memory_win32::MemoryGetWin32HandleInfoKHR, p_handle: *mut *mut std::ffi::c_void) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandlePropertiesKHR.html)) · Function <br/> vkGetMemoryWin32HandlePropertiesKHR - Get Properties of External Memory Win32 Handles\n[](#_c_specification)C Specification\n----------\n\nWindows memory handles compatible with Vulkan **may** also be created by\nnon-Vulkan APIs using methods beyond the scope of this specification.\nTo determine the correct parameters to use when importing such handles,\ncall:\n\n```\n// Provided by VK_KHR_external_memory_win32\nVkResult vkGetMemoryWin32HandlePropertiesKHR(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    HANDLE                                      handle,\n    VkMemoryWin32HandlePropertiesKHR*           pMemoryWin32HandleProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::handle`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::handle`].\n\n* [`Self::handle`] is the handle which will be imported.\n\n* [`Self::p_memory_win32_handle_properties`] is a pointer to a[`crate::vk::MemoryWin32HandlePropertiesKHR`] structure in which properties of[`Self::handle`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handle-00665  \n  [`Self::handle`] **must** be an external memory handle created outside of the\n  Vulkan API\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handleType-00666  \n  [`Self::handle_type`] **must** not be one of the handle types defined as opaque\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-pMemoryWin32HandleProperties-parameter  \n  [`Self::p_memory_win32_handle_properties`] **must** be a valid pointer to a [`crate::vk::MemoryWin32HandlePropertiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryWin32HandlePropertiesKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryWin32HandlePropertiesKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, handle: *mut std::ffi::c_void, p_memory_win32_handle_properties: *mut crate::extensions::khr_external_memory_win32::MemoryWin32HandlePropertiesKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryWin32HandleInfoKHR> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryWin32HandleInfoKHRBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryWin32HandleInfoKHR> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportMemoryWin32HandleInfoKHRBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryWin32HandleInfoKHR.html)) · Structure <br/> VkImportMemoryWin32HandleInfoKHR - Import Win32 memory created on the same physical device\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a Windows handle, add a[`crate::vk::ImportMemoryWin32HandleInfoKHR`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::MemoryAllocateInfo`] structure.\n\nThe [`crate::vk::ImportMemoryWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkImportMemoryWin32HandleInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    HANDLE                                handle;\n    LPCWSTR                               name;\n} VkImportMemoryWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`] or [`Self::name`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  payload to import.\n[](#_description)Description\n----------\n\nImporting memory object payloads from Windows handles does not transfer\nownership of the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release handle\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\nFor handle types defined as NT handles, the imported memory object holds a\nreference to its payload.\n\n|   |Note<br/><br/>Non-NT handle import operations do not add a reference to their associated<br/>payload.<br/>If the original object owning the payload is destroyed, all resources and<br/>handles sharing that payload will become invalid.|\n|---|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nApplications **can** import the same payload into multiple instances of Vulkan,\ninto the same instance from which it was exported, and multiple times into a\ngiven Vulkan instance.\nIn all cases, each import operation **must** create a distinct[`crate::vk::DeviceMemory`] object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00658  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported by [`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-00659  \n   The memory from which [`Self::handle`] was exported, or the memory named by[`Self::name`] **must** have been created on the same underlying physical\n  device as `device`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00660  \n   If [`Self::handle_type`] is not `0`, it **must** be defined as an NT handle or a\n  global share handle\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-01439  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`], or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`], [`Self::name`]**must** be `NULL`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-01440  \n   If [`Self::handle_type`] is not `0` and [`Self::handle`] is `NULL`, [`Self::name`]**must** name a valid memory resource of the type specified by[`Self::handle_type`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00661  \n   If [`Self::handle_type`] is not `0` and [`Self::name`] is `NULL`, [`Self::handle`]**must** be a valid handle of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-01441  \n   if [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-01518  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-name-01519  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-parameter  \n   If [`Self::handle_type`] is not `0`, [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImportMemoryWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportMemoryWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
    pub handle: *mut std::ffi::c_void,
    pub name: *const u16,
}
impl ImportMemoryWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR;
}
impl Default for ImportMemoryWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_type: Default::default(), handle: std::ptr::null_mut(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ImportMemoryWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportMemoryWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_type", &self.handle_type).field("handle", &self.handle).field("name", &self.name).finish()
    }
}
impl ImportMemoryWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportMemoryWin32HandleInfoKHRBuilder<'a> {
        ImportMemoryWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryWin32HandleInfoKHR.html)) · Builder of [`ImportMemoryWin32HandleInfoKHR`] <br/> VkImportMemoryWin32HandleInfoKHR - Import Win32 memory created on the same physical device\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a Windows handle, add a[`crate::vk::ImportMemoryWin32HandleInfoKHR`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::MemoryAllocateInfo`] structure.\n\nThe [`crate::vk::ImportMemoryWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkImportMemoryWin32HandleInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    HANDLE                                handle;\n    LPCWSTR                               name;\n} VkImportMemoryWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`] or [`Self::name`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  payload to import.\n[](#_description)Description\n----------\n\nImporting memory object payloads from Windows handles does not transfer\nownership of the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release handle\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\nFor handle types defined as NT handles, the imported memory object holds a\nreference to its payload.\n\n|   |Note<br/><br/>Non-NT handle import operations do not add a reference to their associated<br/>payload.<br/>If the original object owning the payload is destroyed, all resources and<br/>handles sharing that payload will become invalid.|\n|---|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nApplications **can** import the same payload into multiple instances of Vulkan,\ninto the same instance from which it was exported, and multiple times into a\ngiven Vulkan instance.\nIn all cases, each import operation **must** create a distinct[`crate::vk::DeviceMemory`] object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00658  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported by [`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-00659  \n   The memory from which [`Self::handle`] was exported, or the memory named by[`Self::name`] **must** have been created on the same underlying physical\n  device as `device`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00660  \n   If [`Self::handle_type`] is not `0`, it **must** be defined as an NT handle or a\n  global share handle\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-01439  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`], or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`], [`Self::name`]**must** be `NULL`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-01440  \n   If [`Self::handle_type`] is not `0` and [`Self::handle`] is `NULL`, [`Self::name`]**must** name a valid memory resource of the type specified by[`Self::handle_type`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-00661  \n   If [`Self::handle_type`] is not `0` and [`Self::name`] is `NULL`, [`Self::handle`]**must** be a valid handle of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-01441  \n   if [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handle-01518  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-name-01519  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportMemoryWin32HandleInfoKHR-handleType-parameter  \n   If [`Self::handle_type`] is not `0`, [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImportMemoryWin32HandleInfoKHRBuilder<'a>(ImportMemoryWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ImportMemoryWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ImportMemoryWin32HandleInfoKHRBuilder<'a> {
        ImportMemoryWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn handle(mut self, handle: *mut std::ffi::c_void) -> Self {
        self.0.handle = handle;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportMemoryWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ImportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ImportMemoryWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportMemoryWin32HandleInfoKHRBuilder<'a> {
    type Target = ImportMemoryWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryWin32HandleInfoKHR.html)) · Structure <br/> VkExportMemoryWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a memory\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a memory\nobject, add a [`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkExportMemoryWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportMemoryWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  payload referenced by NT handles exported from the created memory.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportMemoryAllocateInfo`] is not included in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportMemoryAllocateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::MemoryAllocateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportMemoryWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights used depend on\nthe handle type.\n\nFor handles of the following types:\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`]\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`]\n\nThe implementation **must** ensure the access rights allow read and write\naccess to the memory.\n\nFor handles of the following types:\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`]\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`]\n\nThe access rights **must** be:\n\n* `GENERIC_ALL`\n\n  1\n\n  [https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-handleTypes-00657  \n   If [`crate::vk::ExportMemoryAllocateInfo::handle_types`] does not include[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`], or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`], a[`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure **must** not be included\n  in the [`Self::p_next`] chain of [`crate::vk::MemoryAllocateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExportMemoryWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExportMemoryWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_attributes: *const std::ffi::c_void,
    pub dw_access: u32,
    pub name: *const u16,
}
impl ExportMemoryWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR;
}
impl Default for ExportMemoryWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_attributes: std::ptr::null(), dw_access: Default::default(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ExportMemoryWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExportMemoryWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_attributes", &self.p_attributes).field("dw_access", &self.dw_access).field("name", &self.name).finish()
    }
}
impl ExportMemoryWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ExportMemoryWin32HandleInfoKHRBuilder<'a> {
        ExportMemoryWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportMemoryWin32HandleInfoKHR.html)) · Builder of [`ExportMemoryWin32HandleInfoKHR`] <br/> VkExportMemoryWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a memory\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a memory\nobject, add a [`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure to the[`Self::p_next`] chain of the [`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkExportMemoryWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportMemoryWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  payload referenced by NT handles exported from the created memory.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportMemoryAllocateInfo`] is not included in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportMemoryAllocateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::MemoryAllocateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportMemoryWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights used depend on\nthe handle type.\n\nFor handles of the following types:\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`]\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`]\n\nThe implementation **must** ensure the access rights allow read and write\naccess to the memory.\n\nFor handles of the following types:\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`]\n\n* [`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`]\n\nThe access rights **must** be:\n\n* `GENERIC_ALL`\n\n  1\n\n  [https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-handleTypes-00657  \n   If [`crate::vk::ExportMemoryAllocateInfo::handle_types`] does not include[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_WIN32`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D11_TEXTURE`],[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_HEAP`], or[`crate::vk::ExternalMemoryHandleTypeFlagBits::D3D12_RESOURCE`], a[`crate::vk::ExportMemoryWin32HandleInfoKHR`] structure **must** not be included\n  in the [`Self::p_next`] chain of [`crate::vk::MemoryAllocateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportMemoryWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExportMemoryWin32HandleInfoKHRBuilder<'a>(ExportMemoryWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ExportMemoryWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ExportMemoryWin32HandleInfoKHRBuilder<'a> {
        ExportMemoryWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attributes(mut self, attributes: *const std::ffi::c_void) -> Self {
        self.0.p_attributes = attributes;
        self
    }
    #[inline]
    pub fn dw_access(mut self, dw_access: u32) -> Self {
        self.0.dw_access = dw_access as _;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExportMemoryWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ExportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ExportMemoryWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExportMemoryWin32HandleInfoKHRBuilder<'a> {
    type Target = ExportMemoryWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExportMemoryWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryWin32HandlePropertiesKHR.html)) · Structure <br/> VkMemoryWin32HandlePropertiesKHR - Properties of External Memory Windows Handles\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryWin32HandlePropertiesKHR`] structure returned is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkMemoryWin32HandlePropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryWin32HandlePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified windows handle **can** be imported as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryWin32HandlePropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_WIN32_HANDLE_PROPERTIES_KHR`]\n\n* []() VUID-VkMemoryWin32HandlePropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_win32_handle_properties_khr`]\n"]
#[doc(alias = "VkMemoryWin32HandlePropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryWin32HandlePropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub memory_type_bits: u32,
}
impl MemoryWin32HandlePropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_WIN32_HANDLE_PROPERTIES_KHR;
}
impl Default for MemoryWin32HandlePropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), memory_type_bits: Default::default() }
    }
}
impl std::fmt::Debug for MemoryWin32HandlePropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryWin32HandlePropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_type_bits", &self.memory_type_bits).finish()
    }
}
impl MemoryWin32HandlePropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryWin32HandlePropertiesKHRBuilder<'a> {
        MemoryWin32HandlePropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryWin32HandlePropertiesKHR.html)) · Builder of [`MemoryWin32HandlePropertiesKHR`] <br/> VkMemoryWin32HandlePropertiesKHR - Properties of External Memory Windows Handles\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryWin32HandlePropertiesKHR`] structure returned is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkMemoryWin32HandlePropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryWin32HandlePropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified windows handle **can** be imported as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryWin32HandlePropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_WIN32_HANDLE_PROPERTIES_KHR`]\n\n* []() VUID-VkMemoryWin32HandlePropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_win32_handle_properties_khr`]\n"]
#[repr(transparent)]
pub struct MemoryWin32HandlePropertiesKHRBuilder<'a>(MemoryWin32HandlePropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryWin32HandlePropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryWin32HandlePropertiesKHRBuilder<'a> {
        MemoryWin32HandlePropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_type_bits(mut self, memory_type_bits: u32) -> Self {
        self.0.memory_type_bits = memory_type_bits as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryWin32HandlePropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for MemoryWin32HandlePropertiesKHRBuilder<'a> {
    fn default() -> MemoryWin32HandlePropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryWin32HandlePropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryWin32HandlePropertiesKHRBuilder<'a> {
    type Target = MemoryWin32HandlePropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryWin32HandlePropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetWin32HandleInfoKHR.html)) · Structure <br/> VkMemoryGetWin32HandleInfoKHR - Structure describing a Win32 handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkMemoryGetWin32HandleInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the handle will be\n  exported.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) for a description of the\nproperties of the defined external memory handle types.\n\nValid Usage\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00662  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00663  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_memory_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::memory`] and [`Self::handle_type`]\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00664  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_win32_handle_khr`]\n"]
#[doc(alias = "VkMemoryGetWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryGetWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory: crate::vk1_0::DeviceMemory,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
}
impl MemoryGetWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_GET_WIN32_HANDLE_INFO_KHR;
}
impl Default for MemoryGetWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for MemoryGetWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryGetWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory", &self.memory).field("handle_type", &self.handle_type).finish()
    }
}
impl MemoryGetWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryGetWin32HandleInfoKHRBuilder<'a> {
        MemoryGetWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetWin32HandleInfoKHR.html)) · Builder of [`MemoryGetWin32HandleInfoKHR`] <br/> VkMemoryGetWin32HandleInfoKHR - Structure describing a Win32 handle semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_win32\ntypedef struct VkMemoryGetWin32HandleInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the handle will be\n  exported.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) for a description of the\nproperties of the defined external memory handle types.\n\nValid Usage\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00662  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00663  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_memory_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::memory`] and [`Self::handle_type`]\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-00664  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_win32_handle_khr`]\n"]
#[repr(transparent)]
pub struct MemoryGetWin32HandleInfoKHRBuilder<'a>(MemoryGetWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryGetWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryGetWin32HandleInfoKHRBuilder<'a> {
        MemoryGetWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory(mut self, memory: crate::vk1_0::DeviceMemory) -> Self {
        self.0.memory = memory as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryGetWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for MemoryGetWin32HandleInfoKHRBuilder<'a> {
    fn default() -> MemoryGetWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryGetWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryGetWin32HandleInfoKHRBuilder<'a> {
    type Target = MemoryGetWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryGetWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_external_memory_win32`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandleKHR.html)) · Function <br/> vkGetMemoryWin32HandleKHR - Get a Windows HANDLE for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the payload of a Vulkan device\nmemory object, call:\n\n```\n// Provided by VK_KHR_external_memory_win32\nVkResult vkGetMemoryWin32HandleKHR(\n    VkDevice                                    device,\n    const VkMemoryGetWin32HandleInfoKHR*        pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::MemoryGetWin32HandleInfoKHR`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the payload of\n  the device memory object.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_memory_win32_handle_khr`] are owned by the application and hold a\nreference to their payload.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\n|   |Note<br/><br/>Non-NT handle types do not add a reference to their associated payload.<br/>If the original object owning the payload is destroyed, all resources and<br/>handles sharing that payload will become invalid.|\n|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetMemoryWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetWin32HandleInfoKHR`]\n"]
    #[doc(alias = "vkGetMemoryWin32HandleKHR")]
    pub unsafe fn get_memory_win32_handle_khr(&self, get_win32_handle_info: &crate::extensions::khr_external_memory_win32::MemoryGetWin32HandleInfoKHR, handle: *mut *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_memory_win32_handle_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, get_win32_handle_info as _, handle);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryWin32HandlePropertiesKHR.html)) · Function <br/> vkGetMemoryWin32HandlePropertiesKHR - Get Properties of External Memory Win32 Handles\n[](#_c_specification)C Specification\n----------\n\nWindows memory handles compatible with Vulkan **may** also be created by\nnon-Vulkan APIs using methods beyond the scope of this specification.\nTo determine the correct parameters to use when importing such handles,\ncall:\n\n```\n// Provided by VK_KHR_external_memory_win32\nVkResult vkGetMemoryWin32HandlePropertiesKHR(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    HANDLE                                      handle,\n    VkMemoryWin32HandlePropertiesKHR*           pMemoryWin32HandleProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::handle`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::handle`].\n\n* [`Self::handle`] is the handle which will be imported.\n\n* [`Self::p_memory_win32_handle_properties`] is a pointer to a[`crate::vk::MemoryWin32HandlePropertiesKHR`] structure in which properties of[`Self::handle`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handle-00665  \n  [`Self::handle`] **must** be an external memory handle created outside of the\n  Vulkan API\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handleType-00666  \n  [`Self::handle_type`] **must** not be one of the handle types defined as opaque\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryWin32HandlePropertiesKHR-pMemoryWin32HandleProperties-parameter  \n  [`Self::p_memory_win32_handle_properties`] **must** be a valid pointer to a [`crate::vk::MemoryWin32HandlePropertiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryWin32HandlePropertiesKHR`]\n"]
    #[doc(alias = "vkGetMemoryWin32HandlePropertiesKHR")]
    pub unsafe fn get_memory_win32_handle_properties_khr(&self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, handle: *mut std::ffi::c_void, memory_win32_handle_properties: Option<crate::extensions::khr_external_memory_win32::MemoryWin32HandlePropertiesKHR>) -> crate::utils::VulkanResult<crate::extensions::khr_external_memory_win32::MemoryWin32HandlePropertiesKHR> {
        let _function = self.get_memory_win32_handle_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut memory_win32_handle_properties = match memory_win32_handle_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, handle_type as _, handle, &mut memory_win32_handle_properties);
        crate::utils::VulkanResult::new(_return, {
            memory_win32_handle_properties.p_next = std::ptr::null_mut() as _;
            memory_win32_handle_properties
        })
    }
}
