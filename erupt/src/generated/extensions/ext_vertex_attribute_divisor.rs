#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VERTEX_ATTRIBUTE_DIVISOR_SPEC_VERSION")]
pub const EXT_VERTEX_ATTRIBUTE_DIVISOR_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VERTEX_ATTRIBUTE_DIVISOR_EXTENSION_NAME")]
pub const EXT_VERTEX_ATTRIBUTE_DIVISOR_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_vertex_attribute_divisor");
#[doc = "Provided by [`crate::extensions::ext_vertex_attribute_divisor`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT: Self = Self(1000190000);
    pub const PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT: Self = Self(1000190001);
    pub const PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT: Self = Self(1000190002);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceVertexAttributeDivisorFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineVertexInputDivisorStateCreateInfoEXT> for crate::vk1_0::PipelineVertexInputStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::PipelineVertexInputStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexAttributeDivisorFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexAttributeDivisorPropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputBindingDivisorDescriptionEXT.html)) · Structure <br/> VkVertexInputBindingDivisorDescriptionEXT - Structure specifying a divisor used in instanced rendering\n[](#_c_specification)C Specification\n----------\n\nThe individual divisor values per binding are specified using the[`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structure which is defined\nas:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkVertexInputBindingDivisorDescriptionEXT {\n    uint32_t    binding;\n    uint32_t    divisor;\n} VkVertexInputBindingDivisorDescriptionEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::binding`] is the binding number for which the divisor is specified.\n\n* [`Self::divisor`] is the number of successive instances that will use the\n  same value of the vertex attribute when instanced rendering is enabled.\n  For example, if the divisor is N, the same vertex attribute will be\n  applied to N successive instances before moving on to the next vertex\n  attribute.\n  The maximum value of [`Self::divisor`] is implementation-dependent and can\n  be queried using[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`.\n  A value of `0` **can** be used for the divisor if the[`vertexAttributeInstanceRateZeroDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor)feature is enabled.\n  In this case, the same vertex attribute will be applied to all\n  instances.\n[](#_description)Description\n----------\n\nIf this structure is not used to define a divisor value for an attribute,\nthen the divisor has a logical default value of 1.\n\nValid Usage\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-binding-01869  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-vertexAttributeInstanceRateZeroDivisor-02228  \n   If the `vertexAttributeInstanceRateZeroDivisor` feature is not\n  enabled, [`Self::divisor`] **must** not be `0`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-vertexAttributeInstanceRateDivisor-02229  \n   If the `vertexAttributeInstanceRateDivisor` feature is not enabled,[`Self::divisor`] **must** be `1`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-divisor-01870  \n  [`Self::divisor`] **must** be a value between `0` and[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`,\n  inclusive\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-inputRate-01871  \n  [`crate::vk::VertexInputBindingDescription::input_rate`] **must** be of type[`crate::vk::VertexInputRate::INSTANCE`] for this [`Self::binding`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`]\n"]
#[doc(alias = "VkVertexInputBindingDivisorDescriptionEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VertexInputBindingDivisorDescriptionEXT {
    pub binding: u32,
    pub divisor: u32,
}
impl Default for VertexInputBindingDivisorDescriptionEXT {
    fn default() -> Self {
        Self { binding: Default::default(), divisor: Default::default() }
    }
}
impl std::fmt::Debug for VertexInputBindingDivisorDescriptionEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VertexInputBindingDivisorDescriptionEXT").field("binding", &self.binding).field("divisor", &self.divisor).finish()
    }
}
impl VertexInputBindingDivisorDescriptionEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
        VertexInputBindingDivisorDescriptionEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVertexInputBindingDivisorDescriptionEXT.html)) · Builder of [`VertexInputBindingDivisorDescriptionEXT`] <br/> VkVertexInputBindingDivisorDescriptionEXT - Structure specifying a divisor used in instanced rendering\n[](#_c_specification)C Specification\n----------\n\nThe individual divisor values per binding are specified using the[`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structure which is defined\nas:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkVertexInputBindingDivisorDescriptionEXT {\n    uint32_t    binding;\n    uint32_t    divisor;\n} VkVertexInputBindingDivisorDescriptionEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::binding`] is the binding number for which the divisor is specified.\n\n* [`Self::divisor`] is the number of successive instances that will use the\n  same value of the vertex attribute when instanced rendering is enabled.\n  For example, if the divisor is N, the same vertex attribute will be\n  applied to N successive instances before moving on to the next vertex\n  attribute.\n  The maximum value of [`Self::divisor`] is implementation-dependent and can\n  be queried using[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`.\n  A value of `0` **can** be used for the divisor if the[`vertexAttributeInstanceRateZeroDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateZeroDivisor)feature is enabled.\n  In this case, the same vertex attribute will be applied to all\n  instances.\n[](#_description)Description\n----------\n\nIf this structure is not used to define a divisor value for an attribute,\nthen the divisor has a logical default value of 1.\n\nValid Usage\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-binding-01869  \n  [`Self::binding`] **must** be less than[`crate::vk::PhysicalDeviceLimits`]::`maxVertexInputBindings`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-vertexAttributeInstanceRateZeroDivisor-02228  \n   If the `vertexAttributeInstanceRateZeroDivisor` feature is not\n  enabled, [`Self::divisor`] **must** not be `0`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-vertexAttributeInstanceRateDivisor-02229  \n   If the `vertexAttributeInstanceRateDivisor` feature is not enabled,[`Self::divisor`] **must** be `1`\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-divisor-01870  \n  [`Self::divisor`] **must** be a value between `0` and[`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`]::`maxVertexAttribDivisor`,\n  inclusive\n\n* []() VUID-VkVertexInputBindingDivisorDescriptionEXT-inputRate-01871  \n  [`crate::vk::VertexInputBindingDescription::input_rate`] **must** be of type[`crate::vk::VertexInputRate::INSTANCE`] for this [`Self::binding`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`]\n"]
#[repr(transparent)]
pub struct VertexInputBindingDivisorDescriptionEXTBuilder<'a>(VertexInputBindingDivisorDescriptionEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
        VertexInputBindingDivisorDescriptionEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn binding(mut self, binding: u32) -> Self {
        self.0.binding = binding as _;
        self
    }
    #[inline]
    pub fn divisor(mut self, divisor: u32) -> Self {
        self.0.divisor = divisor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VertexInputBindingDivisorDescriptionEXT {
        self.0
    }
}
impl<'a> std::default::Default for VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
    fn default() -> VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
    type Target = VertexInputBindingDivisorDescriptionEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VertexInputBindingDivisorDescriptionEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineVertexInputDivisorStateCreateInfoEXT.html)) · Structure <br/> VkPipelineVertexInputDivisorStateCreateInfoEXT - Structure specifying vertex attributes assignment during instanced rendering\n[](#_c_specification)C Specification\n----------\n\nIf[`vertexAttributeInstanceRateDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor)feature is enabled and the [`Self::p_next`] chain of[`crate::vk::PipelineVertexInputStateCreateInfo`] includes a[`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`] structure, then that\nstructure controls how vertex attributes are assigned to an instance when\ninstanced rendering is enabled.\n\nThe [`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPipelineVertexInputDivisorStateCreateInfoEXT {\n    VkStructureType                                     sType;\n    const void*                                         pNext;\n    uint32_t                                            vertexBindingDivisorCount;\n    const VkVertexInputBindingDivisorDescriptionEXT*    pVertexBindingDivisors;\n} VkPipelineVertexInputDivisorStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::vertex_binding_divisor_count`] is the number of elements in the[`Self::p_vertex_binding_divisors`] array.\n\n* [`Self::p_vertex_binding_divisors`] is a pointer to an array of[`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structures, which\n  specifies the divisor value for each binding.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-pVertexBindingDivisors-parameter  \n  [`Self::p_vertex_binding_divisors`] **must** be a valid pointer to an array of [`Self::vertex_binding_divisor_count`] [`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structures\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-vertexBindingDivisorCount-arraylength  \n  [`Self::vertex_binding_divisor_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VertexInputBindingDivisorDescriptionEXT`]\n"]
#[doc(alias = "VkPipelineVertexInputDivisorStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineVertexInputDivisorStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub vertex_binding_divisor_count: u32,
    pub p_vertex_binding_divisors: *const crate::extensions::ext_vertex_attribute_divisor::VertexInputBindingDivisorDescriptionEXT,
}
impl PipelineVertexInputDivisorStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineVertexInputDivisorStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), vertex_binding_divisor_count: Default::default(), p_vertex_binding_divisors: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineVertexInputDivisorStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineVertexInputDivisorStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("vertex_binding_divisor_count", &self.vertex_binding_divisor_count).field("p_vertex_binding_divisors", &self.p_vertex_binding_divisors).finish()
    }
}
impl PipelineVertexInputDivisorStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
        PipelineVertexInputDivisorStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineVertexInputDivisorStateCreateInfoEXT.html)) · Builder of [`PipelineVertexInputDivisorStateCreateInfoEXT`] <br/> VkPipelineVertexInputDivisorStateCreateInfoEXT - Structure specifying vertex attributes assignment during instanced rendering\n[](#_c_specification)C Specification\n----------\n\nIf[`vertexAttributeInstanceRateDivisor`](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-vertexAttributeInstanceRateDivisor)feature is enabled and the [`Self::p_next`] chain of[`crate::vk::PipelineVertexInputStateCreateInfo`] includes a[`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`] structure, then that\nstructure controls how vertex attributes are assigned to an instance when\ninstanced rendering is enabled.\n\nThe [`crate::vk::PipelineVertexInputDivisorStateCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPipelineVertexInputDivisorStateCreateInfoEXT {\n    VkStructureType                                     sType;\n    const void*                                         pNext;\n    uint32_t                                            vertexBindingDivisorCount;\n    const VkVertexInputBindingDivisorDescriptionEXT*    pVertexBindingDivisors;\n} VkPipelineVertexInputDivisorStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::vertex_binding_divisor_count`] is the number of elements in the[`Self::p_vertex_binding_divisors`] array.\n\n* [`Self::p_vertex_binding_divisors`] is a pointer to an array of[`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structures, which\n  specifies the divisor value for each binding.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-pVertexBindingDivisors-parameter  \n  [`Self::p_vertex_binding_divisors`] **must** be a valid pointer to an array of [`Self::vertex_binding_divisor_count`] [`crate::vk::VertexInputBindingDivisorDescriptionEXT`] structures\n\n* []() VUID-VkPipelineVertexInputDivisorStateCreateInfoEXT-vertexBindingDivisorCount-arraylength  \n  [`Self::vertex_binding_divisor_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VertexInputBindingDivisorDescriptionEXT`]\n"]
#[repr(transparent)]
pub struct PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a>(PipelineVertexInputDivisorStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
        PipelineVertexInputDivisorStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn vertex_binding_divisors(mut self, vertex_binding_divisors: &'a [crate::extensions::ext_vertex_attribute_divisor::VertexInputBindingDivisorDescriptionEXTBuilder]) -> Self {
        self.0.p_vertex_binding_divisors = vertex_binding_divisors.as_ptr() as _;
        self.0.vertex_binding_divisor_count = vertex_binding_divisors.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineVertexInputDivisorStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineVertexInputDivisorStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineVertexInputDivisorStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT - Structure describing max value of vertex attribute divisor that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxVertexAttribDivisor;\n} VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::max_vertex_attrib_divisor`] is the\n  maximum value of the number of instances that will repeat the value of\n  vertex attribute data when instanced rendering is enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_vertex_attrib_divisor: u32,
}
impl PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_vertex_attrib_divisor: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceVertexAttributeDivisorPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_vertex_attrib_divisor", &self.max_vertex_attrib_divisor).finish()
    }
}
impl PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
        PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT.html)) · Builder of [`PhysicalDeviceVertexAttributeDivisorPropertiesEXT`] <br/> VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT - Structure describing max value of vertex attribute divisor that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxVertexAttribDivisor;\n} VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::max_vertex_attrib_divisor`] is the\n  maximum value of the number of instances that will repeat the value of\n  vertex attribute data when instanced rendering is enabled.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceVertexAttributeDivisorPropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a>(PhysicalDeviceVertexAttributeDivisorPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
        PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_vertex_attrib_divisor(mut self, max_vertex_attrib_divisor: u32) -> Self {
        self.0.max_vertex_attrib_divisor = max_vertex_attrib_divisor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceVertexAttributeDivisorPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceVertexAttributeDivisorPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT - Structure describing if fetching of vertex attribute may be repeated for instanced rendering\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           vertexAttributeInstanceRateDivisor;\n    VkBool32           vertexAttributeInstanceRateZeroDivisor;\n} VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::vertex_attribute_instance_rate_divisor`] specifies whether vertex\n  attribute fetching may be repeated in case of instanced rendering.\n\n* []()[`Self::vertex_attribute_instance_rate_zero_divisor`] specifies whether a zero\n  value for [`crate::vk::VertexInputBindingDivisorDescriptionEXT`]::`divisor`is supported.\n\nIf the [`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub vertex_attribute_instance_rate_divisor: crate::vk1_0::Bool32,
    pub vertex_attribute_instance_rate_zero_divisor: crate::vk1_0::Bool32,
}
impl PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT;
}
impl Default for PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), vertex_attribute_instance_rate_divisor: Default::default(), vertex_attribute_instance_rate_zero_divisor: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceVertexAttributeDivisorFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("vertex_attribute_instance_rate_divisor", &(self.vertex_attribute_instance_rate_divisor != 0)).field("vertex_attribute_instance_rate_zero_divisor", &(self.vertex_attribute_instance_rate_zero_divisor != 0)).finish()
    }
}
impl PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
        PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT.html)) · Builder of [`PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] <br/> VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT - Structure describing if fetching of vertex attribute may be repeated for instanced rendering\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_vertex_attribute_divisor\ntypedef struct VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           vertexAttributeInstanceRateDivisor;\n    VkBool32           vertexAttributeInstanceRateZeroDivisor;\n} VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::vertex_attribute_instance_rate_divisor`] specifies whether vertex\n  attribute fetching may be repeated in case of instanced rendering.\n\n* []()[`Self::vertex_attribute_instance_rate_zero_divisor`] specifies whether a zero\n  value for [`crate::vk::VertexInputBindingDivisorDescriptionEXT`]::`divisor`is supported.\n\nIf the [`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceVertexAttributeDivisorFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a>(PhysicalDeviceVertexAttributeDivisorFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
        PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn vertex_attribute_instance_rate_divisor(mut self, vertex_attribute_instance_rate_divisor: bool) -> Self {
        self.0.vertex_attribute_instance_rate_divisor = vertex_attribute_instance_rate_divisor as _;
        self
    }
    #[inline]
    pub fn vertex_attribute_instance_rate_zero_divisor(mut self, vertex_attribute_instance_rate_zero_divisor: bool) -> Self {
        self.0.vertex_attribute_instance_rate_zero_divisor = vertex_attribute_instance_rate_zero_divisor as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceVertexAttributeDivisorFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceVertexAttributeDivisorFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
