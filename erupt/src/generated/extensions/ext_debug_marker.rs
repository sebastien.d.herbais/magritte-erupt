#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_MARKER_SPEC_VERSION")]
pub const EXT_DEBUG_MARKER_SPEC_VERSION: u32 = 4;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DEBUG_MARKER_EXTENSION_NAME")]
pub const EXT_DEBUG_MARKER_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_debug_marker");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DEBUG_MARKER_SET_OBJECT_NAME_EXT: *const std::os::raw::c_char = crate::cstr!("vkDebugMarkerSetObjectNameEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_DEBUG_MARKER_SET_OBJECT_TAG_EXT: *const std::os::raw::c_char = crate::cstr!("vkDebugMarkerSetObjectTagEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_DEBUG_MARKER_BEGIN_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdDebugMarkerBeginEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_DEBUG_MARKER_END_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdDebugMarkerEndEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_DEBUG_MARKER_INSERT_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdDebugMarkerInsertEXT");
#[doc = "Provided by [`crate::extensions::ext_debug_marker`]"]
impl crate::vk1_0::StructureType {
    pub const DEBUG_MARKER_OBJECT_NAME_INFO_EXT: Self = Self(1000022000);
    pub const DEBUG_MARKER_OBJECT_TAG_INFO_EXT: Self = Self(1000022001);
    pub const DEBUG_MARKER_MARKER_INFO_EXT: Self = Self(1000022002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugMarkerSetObjectNameEXT.html)) · Function <br/> vkDebugMarkerSetObjectNameEXT - Give a user-friendly name to an object\n[](#_c_specification)C Specification\n----------\n\nAn object can be given a user-friendly name by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nVkResult vkDebugMarkerSetObjectNameEXT(\n    VkDevice                                    device,\n    const VkDebugMarkerObjectNameInfoEXT*       pNameInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_name_info`] is a pointer to a [`crate::vk::DebugMarkerObjectNameInfoEXT`]structure specifying the parameters of the name to set on the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugMarkerSetObjectNameEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDebugMarkerSetObjectNameEXT-pNameInfo-parameter  \n  [`Self::p_name_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerObjectNameInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pNameInfo->object` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugMarkerObjectNameInfoEXT`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDebugMarkerSetObjectNameEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_name_info: *const crate::extensions::ext_debug_marker::DebugMarkerObjectNameInfoEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugMarkerSetObjectTagEXT.html)) · Function <br/> vkDebugMarkerSetObjectTagEXT - Attach arbitrary data to an object\n[](#_c_specification)C Specification\n----------\n\nIn addition to setting a name for an object, debugging and validation layers\nmay have uses for additional binary data on a per-object basis that has no\nother place in the Vulkan API.\nFor example, a [`crate::vk::ShaderModule`] could have additional debugging data\nattached to it to aid in offline shader tracing.\nTo attach data to an object, call:\n\n```\n// Provided by VK_EXT_debug_marker\nVkResult vkDebugMarkerSetObjectTagEXT(\n    VkDevice                                    device,\n    const VkDebugMarkerObjectTagInfoEXT*        pTagInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_tag_info`] is a pointer to a [`crate::vk::DebugMarkerObjectTagInfoEXT`]structure specifying the parameters of the tag to attach to the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugMarkerSetObjectTagEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDebugMarkerSetObjectTagEXT-pTagInfo-parameter  \n  [`Self::p_tag_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerObjectTagInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pTagInfo->object` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugMarkerObjectTagInfoEXT`], [`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkDebugMarkerSetObjectTagEXT = unsafe extern "system" fn(device: crate::vk1_0::Device, p_tag_info: *const crate::extensions::ext_debug_marker::DebugMarkerObjectTagInfoEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerBeginEXT.html)) · Function <br/> vkCmdDebugMarkerBeginEXT - Open a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nA marker region can be opened by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerBeginEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugMarkerMarkerInfoEXT*           pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_marker_info`] is a pointer to a [`crate::vk::DebugMarkerMarkerInfoEXT`]structure specifying the parameters of the marker region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerMarkerInfoEXT`] structure\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugMarkerMarkerInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdDebugMarkerBeginEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_marker_info: *const crate::extensions::ext_debug_marker::DebugMarkerMarkerInfoEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerEndEXT.html)) · Function <br/> vkCmdDebugMarkerEndEXT - Close a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nA marker region can be closed by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerEndEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n[](#_description)Description\n----------\n\nAn application **may** open a marker region in one command buffer and close it\nin another, or otherwise split marker regions across multiple command\nbuffers or multiple queue submissions.\nWhen viewed from the linear series of submissions to a single queue, the\ncalls to [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] and [`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`]**must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-01239  \n   There **must** be an outstanding [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] command\n  prior to the [`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`] on the queue that[`Self::command_buffer`] is submitted to\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-01240  \n   If [`Self::command_buffer`] is a secondary command buffer, there **must** be an\n  outstanding [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] command recorded to[`Self::command_buffer`] that has not previously been ended by a call to[`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdDebugMarkerEndEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerInsertEXT.html)) · Function <br/> vkCmdDebugMarkerInsertEXT - Insert a marker label into a command buffer\n[](#_c_specification)C Specification\n----------\n\nA single marker label can be inserted into a command buffer by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerInsertEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugMarkerMarkerInfoEXT*           pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_marker_info`] is a pointer to a [`crate::vk::DebugMarkerMarkerInfoEXT`]structure specifying the parameters of the marker to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerMarkerInfoEXT`] structure\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugMarkerMarkerInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdDebugMarkerInsertEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_marker_info: *const crate::extensions::ext_debug_marker::DebugMarkerMarkerInfoEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerObjectNameInfoEXT.html)) · Structure <br/> VkDebugMarkerObjectNameInfoEXT - Specify parameters of a name to give to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerObjectNameInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerObjectNameInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkDebugReportObjectTypeEXT    objectType;\n    uint64_t                      object;\n    const char*                   pObjectName;\n} VkDebugMarkerObjectNameInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of the object to be named.\n\n* [`Self::object`] is the object to be named.\n\n* [`Self::p_object_name`] is a null-terminated UTF-8 string specifying the name\n  to apply to [`Self::object`].\n[](#_description)Description\n----------\n\nApplications **may** change the name associated with an object simply by\ncalling [`crate::vk::DeviceLoader::debug_marker_set_object_name_ext`] again with a new string.\nTo remove a previously set name, [`Self::p_object_name`] **should** be set to an\nempty string.\n\nValid Usage\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-objectType-01490  \n  [`Self::object_type`] **must** not be[`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-object-01491  \n  [`Self::object`] **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-object-01492  \n  [`Self::object`] **must** be a Vulkan object of the type associated with[`Self::object_type`] as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_OBJECT_NAME_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-pObjectName-parameter  \n  [`Self::p_object_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::debug_marker_set_object_name_ext`]\n"]
#[doc(alias = "VkDebugMarkerObjectNameInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugMarkerObjectNameInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT,
    pub object: u64,
    pub p_object_name: *const std::os::raw::c_char,
}
impl DebugMarkerObjectNameInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_MARKER_OBJECT_NAME_INFO_EXT;
}
impl Default for DebugMarkerObjectNameInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), object_type: Default::default(), object: Default::default(), p_object_name: std::ptr::null() }
    }
}
impl std::fmt::Debug for DebugMarkerObjectNameInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugMarkerObjectNameInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("object_type", &self.object_type).field("object", &self.object).field("p_object_name", &self.p_object_name).finish()
    }
}
impl DebugMarkerObjectNameInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugMarkerObjectNameInfoEXTBuilder<'a> {
        DebugMarkerObjectNameInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerObjectNameInfoEXT.html)) · Builder of [`DebugMarkerObjectNameInfoEXT`] <br/> VkDebugMarkerObjectNameInfoEXT - Specify parameters of a name to give to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerObjectNameInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerObjectNameInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkDebugReportObjectTypeEXT    objectType;\n    uint64_t                      object;\n    const char*                   pObjectName;\n} VkDebugMarkerObjectNameInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of the object to be named.\n\n* [`Self::object`] is the object to be named.\n\n* [`Self::p_object_name`] is a null-terminated UTF-8 string specifying the name\n  to apply to [`Self::object`].\n[](#_description)Description\n----------\n\nApplications **may** change the name associated with an object simply by\ncalling [`crate::vk::DeviceLoader::debug_marker_set_object_name_ext`] again with a new string.\nTo remove a previously set name, [`Self::p_object_name`] **should** be set to an\nempty string.\n\nValid Usage\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-objectType-01490  \n  [`Self::object_type`] **must** not be[`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-object-01491  \n  [`Self::object`] **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-object-01492  \n  [`Self::object`] **must** be a Vulkan object of the type associated with[`Self::object_type`] as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_OBJECT_NAME_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-VkDebugMarkerObjectNameInfoEXT-pObjectName-parameter  \n  [`Self::p_object_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::debug_marker_set_object_name_ext`]\n"]
#[repr(transparent)]
pub struct DebugMarkerObjectNameInfoEXTBuilder<'a>(DebugMarkerObjectNameInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugMarkerObjectNameInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugMarkerObjectNameInfoEXTBuilder<'a> {
        DebugMarkerObjectNameInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn object_type(mut self, object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT) -> Self {
        self.0.object_type = object_type as _;
        self
    }
    #[inline]
    pub fn object(mut self, object: u64) -> Self {
        self.0.object = object as _;
        self
    }
    #[inline]
    pub fn object_name(mut self, object_name: &'a std::ffi::CStr) -> Self {
        self.0.p_object_name = object_name.as_ptr();
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugMarkerObjectNameInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugMarkerObjectNameInfoEXTBuilder<'a> {
    fn default() -> DebugMarkerObjectNameInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugMarkerObjectNameInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugMarkerObjectNameInfoEXTBuilder<'a> {
    type Target = DebugMarkerObjectNameInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugMarkerObjectNameInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerObjectTagInfoEXT.html)) · Structure <br/> VkDebugMarkerObjectTagInfoEXT - Specify parameters of a tag to attach to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerObjectTagInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerObjectTagInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkDebugReportObjectTypeEXT    objectType;\n    uint64_t                      object;\n    uint64_t                      tagName;\n    size_t                        tagSize;\n    const void*                   pTag;\n} VkDebugMarkerObjectTagInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of the object to be named.\n\n* [`Self::object`] is the object to be tagged.\n\n* [`Self::tag_name`] is a numerical identifier of the tag.\n\n* [`Self::tag_size`] is the number of bytes of data to attach to the object.\n\n* [`Self::p_tag`] is a pointer to an array of [`Self::tag_size`] bytes containing\n  the data to be associated with the object.\n[](#_description)Description\n----------\n\nThe [`Self::tag_name`] parameter gives a name or identifier to the type of data\nbeing tagged.\nThis can be used by debugging layers to easily filter for only data that can\nbe used by that implementation.\n\nValid Usage\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-objectType-01493  \n  [`Self::object_type`] **must** not be[`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-object-01494  \n  [`Self::object`] **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-object-01495  \n  [`Self::object`] **must** be a Vulkan object of the type associated with[`Self::object_type`] as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_OBJECT_TAG_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-pTag-parameter  \n  [`Self::p_tag`] **must** be a valid pointer to an array of [`Self::tag_size`] bytes\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-tagSize-arraylength  \n  [`Self::tag_size`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::debug_marker_set_object_tag_ext`]\n"]
#[doc(alias = "VkDebugMarkerObjectTagInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugMarkerObjectTagInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT,
    pub object: u64,
    pub tag_name: u64,
    pub tag_size: usize,
    pub p_tag: *const std::ffi::c_void,
}
impl DebugMarkerObjectTagInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_MARKER_OBJECT_TAG_INFO_EXT;
}
impl Default for DebugMarkerObjectTagInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), object_type: Default::default(), object: Default::default(), tag_name: Default::default(), tag_size: Default::default(), p_tag: std::ptr::null() }
    }
}
impl std::fmt::Debug for DebugMarkerObjectTagInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugMarkerObjectTagInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("object_type", &self.object_type).field("object", &self.object).field("tag_name", &self.tag_name).field("tag_size", &self.tag_size).field("p_tag", &self.p_tag).finish()
    }
}
impl DebugMarkerObjectTagInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugMarkerObjectTagInfoEXTBuilder<'a> {
        DebugMarkerObjectTagInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerObjectTagInfoEXT.html)) · Builder of [`DebugMarkerObjectTagInfoEXT`] <br/> VkDebugMarkerObjectTagInfoEXT - Specify parameters of a tag to attach to an object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerObjectTagInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerObjectTagInfoEXT {\n    VkStructureType               sType;\n    const void*                   pNext;\n    VkDebugReportObjectTypeEXT    objectType;\n    uint64_t                      object;\n    uint64_t                      tagName;\n    size_t                        tagSize;\n    const void*                   pTag;\n} VkDebugMarkerObjectTagInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::object_type`] is a [`crate::vk::DebugReportObjectTypeEXT`] specifying the\n  type of the object to be named.\n\n* [`Self::object`] is the object to be tagged.\n\n* [`Self::tag_name`] is a numerical identifier of the tag.\n\n* [`Self::tag_size`] is the number of bytes of data to attach to the object.\n\n* [`Self::p_tag`] is a pointer to an array of [`Self::tag_size`] bytes containing\n  the data to be associated with the object.\n[](#_description)Description\n----------\n\nThe [`Self::tag_name`] parameter gives a name or identifier to the type of data\nbeing tagged.\nThis can be used by debugging layers to easily filter for only data that can\nbe used by that implementation.\n\nValid Usage\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-objectType-01493  \n  [`Self::object_type`] **must** not be[`crate::vk::DebugReportObjectTypeEXT::UNKNOWN_EXT`]\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-object-01494  \n  [`Self::object`] **must** not be [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-object-01495  \n  [`Self::object`] **must** be a Vulkan object of the type associated with[`Self::object_type`] as defined in [https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debug-report-object-types)\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_OBJECT_TAG_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-objectType-parameter  \n  [`Self::object_type`] **must** be a valid [`crate::vk::DebugReportObjectTypeEXT`] value\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-pTag-parameter  \n  [`Self::p_tag`] **must** be a valid pointer to an array of [`Self::tag_size`] bytes\n\n* []() VUID-VkDebugMarkerObjectTagInfoEXT-tagSize-arraylength  \n  [`Self::tag_size`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugReportObjectTypeEXT`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::debug_marker_set_object_tag_ext`]\n"]
#[repr(transparent)]
pub struct DebugMarkerObjectTagInfoEXTBuilder<'a>(DebugMarkerObjectTagInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugMarkerObjectTagInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugMarkerObjectTagInfoEXTBuilder<'a> {
        DebugMarkerObjectTagInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn object_type(mut self, object_type: crate::extensions::ext_debug_report::DebugReportObjectTypeEXT) -> Self {
        self.0.object_type = object_type as _;
        self
    }
    #[inline]
    pub fn object(mut self, object: u64) -> Self {
        self.0.object = object as _;
        self
    }
    #[inline]
    pub fn tag_name(mut self, tag_name: u64) -> Self {
        self.0.tag_name = tag_name as _;
        self
    }
    #[inline]
    pub fn tag_size(mut self, tag_size: usize) -> Self {
        self.0.tag_size = tag_size;
        self
    }
    #[inline]
    pub fn tag(mut self, tag: *const std::ffi::c_void) -> Self {
        self.0.p_tag = tag;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugMarkerObjectTagInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugMarkerObjectTagInfoEXTBuilder<'a> {
    fn default() -> DebugMarkerObjectTagInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugMarkerObjectTagInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugMarkerObjectTagInfoEXTBuilder<'a> {
    type Target = DebugMarkerObjectTagInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugMarkerObjectTagInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerMarkerInfoEXT.html)) · Structure <br/> VkDebugMarkerMarkerInfoEXT - Specify parameters of a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerMarkerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerMarkerInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    const char*        pMarkerName;\n    float              color[4];\n} VkDebugMarkerMarkerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_marker_name`] is a pointer to a null-terminated UTF-8 string\n  containing the name of the marker.\n\n* [`Self::color`] is an **optional** RGBA color value that can be associated with\n  the marker.\n  A particular implementation **may** choose to ignore this color value.\n  The values contain RGBA values in order, in the range 0.0 to 1.0.\n  If all elements in [`Self::color`] are set to 0.0 then it is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_MARKER_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-pMarkerName-parameter  \n  [`Self::p_marker_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`], [`crate::vk::DeviceLoader::cmd_debug_marker_insert_ext`]\n"]
#[doc(alias = "VkDebugMarkerMarkerInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DebugMarkerMarkerInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_marker_name: *const std::os::raw::c_char,
    pub color: [std::os::raw::c_float; 4],
}
impl DebugMarkerMarkerInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEBUG_MARKER_MARKER_INFO_EXT;
}
impl Default for DebugMarkerMarkerInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_marker_name: std::ptr::null(), color: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for DebugMarkerMarkerInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DebugMarkerMarkerInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_marker_name", &self.p_marker_name).field("color", &self.color).finish()
    }
}
impl DebugMarkerMarkerInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> DebugMarkerMarkerInfoEXTBuilder<'a> {
        DebugMarkerMarkerInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDebugMarkerMarkerInfoEXT.html)) · Builder of [`DebugMarkerMarkerInfoEXT`] <br/> VkDebugMarkerMarkerInfoEXT - Specify parameters of a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::DebugMarkerMarkerInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_debug_marker\ntypedef struct VkDebugMarkerMarkerInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    const char*        pMarkerName;\n    float              color[4];\n} VkDebugMarkerMarkerInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_marker_name`] is a pointer to a null-terminated UTF-8 string\n  containing the name of the marker.\n\n* [`Self::color`] is an **optional** RGBA color value that can be associated with\n  the marker.\n  A particular implementation **may** choose to ignore this color value.\n  The values contain RGBA values in order, in the range 0.0 to 1.0.\n  If all elements in [`Self::color`] are set to 0.0 then it is ignored.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEBUG_MARKER_MARKER_INFO_EXT`]\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkDebugMarkerMarkerInfoEXT-pMarkerName-parameter  \n  [`Self::p_marker_name`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`], [`crate::vk::DeviceLoader::cmd_debug_marker_insert_ext`]\n"]
#[repr(transparent)]
pub struct DebugMarkerMarkerInfoEXTBuilder<'a>(DebugMarkerMarkerInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> DebugMarkerMarkerInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> DebugMarkerMarkerInfoEXTBuilder<'a> {
        DebugMarkerMarkerInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn marker_name(mut self, marker_name: &'a std::ffi::CStr) -> Self {
        self.0.p_marker_name = marker_name.as_ptr();
        self
    }
    #[inline]
    pub fn color(mut self, color: [std::os::raw::c_float; 4]) -> Self {
        self.0.color = color as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DebugMarkerMarkerInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for DebugMarkerMarkerInfoEXTBuilder<'a> {
    fn default() -> DebugMarkerMarkerInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DebugMarkerMarkerInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DebugMarkerMarkerInfoEXTBuilder<'a> {
    type Target = DebugMarkerMarkerInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DebugMarkerMarkerInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_debug_marker`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugMarkerSetObjectNameEXT.html)) · Function <br/> vkDebugMarkerSetObjectNameEXT - Give a user-friendly name to an object\n[](#_c_specification)C Specification\n----------\n\nAn object can be given a user-friendly name by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nVkResult vkDebugMarkerSetObjectNameEXT(\n    VkDevice                                    device,\n    const VkDebugMarkerObjectNameInfoEXT*       pNameInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_name_info`] is a pointer to a [`crate::vk::DebugMarkerObjectNameInfoEXT`]structure specifying the parameters of the name to set on the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugMarkerSetObjectNameEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDebugMarkerSetObjectNameEXT-pNameInfo-parameter  \n  [`Self::p_name_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerObjectNameInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pNameInfo->object` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugMarkerObjectNameInfoEXT`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkDebugMarkerSetObjectNameEXT")]
    pub unsafe fn debug_marker_set_object_name_ext(&self, name_info: &crate::extensions::ext_debug_marker::DebugMarkerObjectNameInfoEXT) -> crate::utils::VulkanResult<()> {
        let _function = self.debug_marker_set_object_name_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, name_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkDebugMarkerSetObjectTagEXT.html)) · Function <br/> vkDebugMarkerSetObjectTagEXT - Attach arbitrary data to an object\n[](#_c_specification)C Specification\n----------\n\nIn addition to setting a name for an object, debugging and validation layers\nmay have uses for additional binary data on a per-object basis that has no\nother place in the Vulkan API.\nFor example, a [`crate::vk::ShaderModule`] could have additional debugging data\nattached to it to aid in offline shader tracing.\nTo attach data to an object, call:\n\n```\n// Provided by VK_EXT_debug_marker\nVkResult vkDebugMarkerSetObjectTagEXT(\n    VkDevice                                    device,\n    const VkDebugMarkerObjectTagInfoEXT*        pTagInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device that created the object.\n\n* [`Self::p_tag_info`] is a pointer to a [`crate::vk::DebugMarkerObjectTagInfoEXT`]structure specifying the parameters of the tag to attach to the object.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkDebugMarkerSetObjectTagEXT-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkDebugMarkerSetObjectTagEXT-pTagInfo-parameter  \n  [`Self::p_tag_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerObjectTagInfoEXT`] structure\n\nHost Synchronization\n\n* Host access to `pTagInfo->object` **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DebugMarkerObjectTagInfoEXT`], [`crate::vk::Device`]\n"]
    #[doc(alias = "vkDebugMarkerSetObjectTagEXT")]
    pub unsafe fn debug_marker_set_object_tag_ext(&self, tag_info: &crate::extensions::ext_debug_marker::DebugMarkerObjectTagInfoEXT) -> crate::utils::VulkanResult<()> {
        let _function = self.debug_marker_set_object_tag_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, tag_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerBeginEXT.html)) · Function <br/> vkCmdDebugMarkerBeginEXT - Open a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nA marker region can be opened by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerBeginEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugMarkerMarkerInfoEXT*           pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_marker_info`] is a pointer to a [`crate::vk::DebugMarkerMarkerInfoEXT`]structure specifying the parameters of the marker region to open.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerMarkerInfoEXT`] structure\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerBeginEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugMarkerMarkerInfoEXT`]\n"]
    #[doc(alias = "vkCmdDebugMarkerBeginEXT")]
    pub unsafe fn cmd_debug_marker_begin_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, marker_info: &crate::extensions::ext_debug_marker::DebugMarkerMarkerInfoEXT) -> () {
        let _function = self.cmd_debug_marker_begin_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, marker_info as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerEndEXT.html)) · Function <br/> vkCmdDebugMarkerEndEXT - Close a command buffer marker region\n[](#_c_specification)C Specification\n----------\n\nA marker region can be closed by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerEndEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n[](#_description)Description\n----------\n\nAn application **may** open a marker region in one command buffer and close it\nin another, or otherwise split marker regions across multiple command\nbuffers or multiple queue submissions.\nWhen viewed from the linear series of submissions to a single queue, the\ncalls to [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] and [`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`]**must** be matched and balanced.\n\nValid Usage\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-01239  \n   There **must** be an outstanding [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] command\n  prior to the [`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`] on the queue that[`Self::command_buffer`] is submitted to\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-01240  \n   If [`Self::command_buffer`] is a secondary command buffer, there **must** be an\n  outstanding [`crate::vk::DeviceLoader::cmd_debug_marker_begin_ext`] command recorded to[`Self::command_buffer`] that has not previously been ended by a call to[`crate::vk::DeviceLoader::cmd_debug_marker_end_ext`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerEndEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdDebugMarkerEndEXT")]
    pub unsafe fn cmd_debug_marker_end_ext(&self, command_buffer: crate::vk1_0::CommandBuffer) -> () {
        let _function = self.cmd_debug_marker_end_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdDebugMarkerInsertEXT.html)) · Function <br/> vkCmdDebugMarkerInsertEXT - Insert a marker label into a command buffer\n[](#_c_specification)C Specification\n----------\n\nA single marker label can be inserted into a command buffer by calling:\n\n```\n// Provided by VK_EXT_debug_marker\nvoid vkCmdDebugMarkerInsertEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkDebugMarkerMarkerInfoEXT*           pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command is\n  recorded.\n\n* [`Self::p_marker_info`] is a pointer to a [`crate::vk::DebugMarkerMarkerInfoEXT`]structure specifying the parameters of the marker to insert.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::DebugMarkerMarkerInfoEXT`] structure\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdDebugMarkerInsertEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::DebugMarkerMarkerInfoEXT`]\n"]
    #[doc(alias = "vkCmdDebugMarkerInsertEXT")]
    pub unsafe fn cmd_debug_marker_insert_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, marker_info: &crate::extensions::ext_debug_marker::DebugMarkerMarkerInfoEXT) -> () {
        let _function = self.cmd_debug_marker_insert_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, marker_info as _);
        ()
    }
}
