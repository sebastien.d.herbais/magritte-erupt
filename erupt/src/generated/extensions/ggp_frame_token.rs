#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GGP_FRAME_TOKEN_SPEC_VERSION")]
pub const GGP_FRAME_TOKEN_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GGP_FRAME_TOKEN_EXTENSION_NAME")]
pub const GGP_FRAME_TOKEN_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_GGP_frame_token");
#[doc = "Provided by [`crate::extensions::ggp_frame_token`]"]
impl crate::vk1_0::StructureType {
    pub const PRESENT_FRAME_TOKEN_GGP: Self = Self(1000191000);
}
impl<'a> crate::ExtendableFromConst<'a, PresentFrameTokenGGP> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PresentFrameTokenGGPBuilder<'_>> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentFrameTokenGGP.html)) · Structure <br/> VkPresentFrameTokenGGP - The Google Games Platform frame token\n[](#_c_specification)C Specification\n----------\n\nWhen the [VK_GGP_frame_token](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_GGP_frame_token.html) extension is enabled, a Google Games\nPlatform frame token **can** be specified when presenting an image to a\nswapchain by adding a [`crate::vk::PresentFrameTokenGGP`] structure to the[`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentFrameTokenGGP`] structure is defined as:\n\n```\n// Provided by VK_GGP_frame_token\ntypedef struct VkPresentFrameTokenGGP {\n    VkStructureType    sType;\n    const void*        pNext;\n    GgpFrameToken      frameToken;\n} VkPresentFrameTokenGGP;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::frame_token`] is the Google Games Platform frame token.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentFrameTokenGGP-frameToken-02680  \n  [`Self::frame_token`] **must** be a valid `GgpFrameToken`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentFrameTokenGGP-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_FRAME_TOKEN_GGP`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPresentFrameTokenGGP")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentFrameTokenGGP {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub frame_token: u64,
}
impl PresentFrameTokenGGP {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PRESENT_FRAME_TOKEN_GGP;
}
impl Default for PresentFrameTokenGGP {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), frame_token: Default::default() }
    }
}
impl std::fmt::Debug for PresentFrameTokenGGP {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentFrameTokenGGP").field("s_type", &self.s_type).field("p_next", &self.p_next).field("frame_token", &self.frame_token).finish()
    }
}
impl PresentFrameTokenGGP {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentFrameTokenGGPBuilder<'a> {
        PresentFrameTokenGGPBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentFrameTokenGGP.html)) · Builder of [`PresentFrameTokenGGP`] <br/> VkPresentFrameTokenGGP - The Google Games Platform frame token\n[](#_c_specification)C Specification\n----------\n\nWhen the [VK_GGP_frame_token](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_GGP_frame_token.html) extension is enabled, a Google Games\nPlatform frame token **can** be specified when presenting an image to a\nswapchain by adding a [`crate::vk::PresentFrameTokenGGP`] structure to the[`Self::p_next`] chain of the [`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentFrameTokenGGP`] structure is defined as:\n\n```\n// Provided by VK_GGP_frame_token\ntypedef struct VkPresentFrameTokenGGP {\n    VkStructureType    sType;\n    const void*        pNext;\n    GgpFrameToken      frameToken;\n} VkPresentFrameTokenGGP;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::frame_token`] is the Google Games Platform frame token.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentFrameTokenGGP-frameToken-02680  \n  [`Self::frame_token`] **must** be a valid `GgpFrameToken`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentFrameTokenGGP-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_FRAME_TOKEN_GGP`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PresentFrameTokenGGPBuilder<'a>(PresentFrameTokenGGP, std::marker::PhantomData<&'a ()>);
impl<'a> PresentFrameTokenGGPBuilder<'a> {
    #[inline]
    pub fn new() -> PresentFrameTokenGGPBuilder<'a> {
        PresentFrameTokenGGPBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn frame_token(mut self, frame_token: u64) -> Self {
        self.0.frame_token = frame_token as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentFrameTokenGGP {
        self.0
    }
}
impl<'a> std::default::Default for PresentFrameTokenGGPBuilder<'a> {
    fn default() -> PresentFrameTokenGGPBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentFrameTokenGGPBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentFrameTokenGGPBuilder<'a> {
    type Target = PresentFrameTokenGGP;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentFrameTokenGGPBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
