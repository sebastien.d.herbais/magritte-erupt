#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEDICATED_ALLOCATION_SPEC_VERSION")]
pub const NV_DEDICATED_ALLOCATION_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_DEDICATED_ALLOCATION_EXTENSION_NAME")]
pub const NV_DEDICATED_ALLOCATION_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_dedicated_allocation");
#[doc = "Provided by [`crate::extensions::nv_dedicated_allocation`]"]
impl crate::vk1_0::StructureType {
    pub const DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV: Self = Self(1000026000);
    pub const DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV: Self = Self(1000026001);
    pub const DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV: Self = Self(1000026002);
}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationMemoryAllocateInfoNV> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationMemoryAllocateInfoNVBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationBufferCreateInfoNV> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationBufferCreateInfoNVBuilder<'_>> for crate::vk1_0::BufferCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationImageCreateInfoNV> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, DedicatedAllocationImageCreateInfoNVBuilder<'_>> for crate::vk1_0::ImageCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationImageCreateInfoNV.html)) · Structure <br/> VkDedicatedAllocationImageCreateInfoNV - Specify that an image is bound to a dedicated memory resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationImageCreateInfoNV`] structure, then that structure\nincludes an enable controlling whether the image will have a dedicated\nmemory allocation bound to it.\n\nThe [`crate::vk::DedicatedAllocationImageCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationImageCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           dedicatedAllocation;\n} VkDedicatedAllocationImageCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::dedicated_allocation`] specifies whether the image will have a\n  dedicated allocation bound to it.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Using a dedicated allocation for color and depth/stencil attachments or<br/>other large images **may** improve performance on some devices.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDedicatedAllocationImageCreateInfoNV-dedicatedAllocation-00994  \n   If [`Self::dedicated_allocation`] is [`crate::vk::TRUE`],[`crate::vk::ImageCreateInfo::flags`] **must** not include[`crate::vk::ImageCreateFlagBits::SPARSE_BINDING`],[`crate::vk::ImageCreateFlagBits::SPARSE_RESIDENCY`], or[`crate::vk::ImageCreateFlagBits::SPARSE_ALIASED`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationImageCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDedicatedAllocationImageCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DedicatedAllocationImageCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub dedicated_allocation: crate::vk1_0::Bool32,
}
impl DedicatedAllocationImageCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV;
}
impl Default for DedicatedAllocationImageCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), dedicated_allocation: Default::default() }
    }
}
impl std::fmt::Debug for DedicatedAllocationImageCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DedicatedAllocationImageCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("dedicated_allocation", &(self.dedicated_allocation != 0)).finish()
    }
}
impl DedicatedAllocationImageCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> DedicatedAllocationImageCreateInfoNVBuilder<'a> {
        DedicatedAllocationImageCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationImageCreateInfoNV.html)) · Builder of [`DedicatedAllocationImageCreateInfoNV`] <br/> VkDedicatedAllocationImageCreateInfoNV - Specify that an image is bound to a dedicated memory resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationImageCreateInfoNV`] structure, then that structure\nincludes an enable controlling whether the image will have a dedicated\nmemory allocation bound to it.\n\nThe [`crate::vk::DedicatedAllocationImageCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationImageCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           dedicatedAllocation;\n} VkDedicatedAllocationImageCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::dedicated_allocation`] specifies whether the image will have a\n  dedicated allocation bound to it.\n[](#_description)Description\n----------\n\n|   |Note<br/><br/>Using a dedicated allocation for color and depth/stencil attachments or<br/>other large images **may** improve performance on some devices.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkDedicatedAllocationImageCreateInfoNV-dedicatedAllocation-00994  \n   If [`Self::dedicated_allocation`] is [`crate::vk::TRUE`],[`crate::vk::ImageCreateInfo::flags`] **must** not include[`crate::vk::ImageCreateFlagBits::SPARSE_BINDING`],[`crate::vk::ImageCreateFlagBits::SPARSE_RESIDENCY`], or[`crate::vk::ImageCreateFlagBits::SPARSE_ALIASED`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationImageCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DedicatedAllocationImageCreateInfoNVBuilder<'a>(DedicatedAllocationImageCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> DedicatedAllocationImageCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> DedicatedAllocationImageCreateInfoNVBuilder<'a> {
        DedicatedAllocationImageCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn dedicated_allocation(mut self, dedicated_allocation: bool) -> Self {
        self.0.dedicated_allocation = dedicated_allocation as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DedicatedAllocationImageCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for DedicatedAllocationImageCreateInfoNVBuilder<'a> {
    fn default() -> DedicatedAllocationImageCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DedicatedAllocationImageCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DedicatedAllocationImageCreateInfoNVBuilder<'a> {
    type Target = DedicatedAllocationImageCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DedicatedAllocationImageCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationBufferCreateInfoNV.html)) · Structure <br/> VkDedicatedAllocationBufferCreateInfoNV - Specify that a buffer is bound to a dedicated memory resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationBufferCreateInfoNV`] structure, then that structure\nincludes an enable controlling whether the buffer will have a dedicated\nmemory allocation bound to it.\n\nThe [`crate::vk::DedicatedAllocationBufferCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationBufferCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           dedicatedAllocation;\n} VkDedicatedAllocationBufferCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::dedicated_allocation`] specifies whether the buffer will have a\n  dedicated allocation bound to it.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationBufferCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDedicatedAllocationBufferCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DedicatedAllocationBufferCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub dedicated_allocation: crate::vk1_0::Bool32,
}
impl DedicatedAllocationBufferCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV;
}
impl Default for DedicatedAllocationBufferCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), dedicated_allocation: Default::default() }
    }
}
impl std::fmt::Debug for DedicatedAllocationBufferCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DedicatedAllocationBufferCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("dedicated_allocation", &(self.dedicated_allocation != 0)).finish()
    }
}
impl DedicatedAllocationBufferCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
        DedicatedAllocationBufferCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationBufferCreateInfoNV.html)) · Builder of [`DedicatedAllocationBufferCreateInfoNV`] <br/> VkDedicatedAllocationBufferCreateInfoNV - Specify that a buffer is bound to a dedicated memory resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationBufferCreateInfoNV`] structure, then that structure\nincludes an enable controlling whether the buffer will have a dedicated\nmemory allocation bound to it.\n\nThe [`crate::vk::DedicatedAllocationBufferCreateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationBufferCreateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           dedicatedAllocation;\n} VkDedicatedAllocationBufferCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::dedicated_allocation`] specifies whether the buffer will have a\n  dedicated allocation bound to it.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationBufferCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DedicatedAllocationBufferCreateInfoNVBuilder<'a>(DedicatedAllocationBufferCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
        DedicatedAllocationBufferCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn dedicated_allocation(mut self, dedicated_allocation: bool) -> Self {
        self.0.dedicated_allocation = dedicated_allocation as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DedicatedAllocationBufferCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
    fn default() -> DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
    type Target = DedicatedAllocationBufferCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DedicatedAllocationBufferCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationMemoryAllocateInfoNV.html)) · Structure <br/> VkDedicatedAllocationMemoryAllocateInfoNV - Specify a dedicated memory allocation resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationMemoryAllocateInfoNV`] structure, then that\nstructure includes a handle of the sole buffer or image resource that the\nmemory **can** be bound to.\n\nThe [`crate::vk::DedicatedAllocationMemoryAllocateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationMemoryAllocateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkImage            image;\n    VkBuffer           buffer;\n} VkDedicatedAllocationMemoryAllocateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of an image which this\n  memory will be bound to.\n\n* [`Self::buffer`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of a buffer which this\n  memory will be bound to.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00649  \n   At least one of [`Self::image`] and [`Self::buffer`] **must** be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00650  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), the image **must** have been\n  created with[`crate::vk::DedicatedAllocationImageCreateInfoNV::dedicated_allocation`]equal to [`crate::vk::TRUE`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00651  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), the buffer **must** have been\n  created with[`crate::vk::DedicatedAllocationBufferCreateInfoNV::dedicated_allocation`]equal to [`crate::vk::TRUE`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00652  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`crate::vk::MemoryAllocateInfo`]::`allocationSize` **must** equal the[`crate::vk::MemoryRequirements`]::`size` of the image\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00653  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`crate::vk::MemoryAllocateInfo`]::`allocationSize` **must** equal the[`crate::vk::MemoryRequirements`]::`size` of the buffer\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00654  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and[`crate::vk::MemoryAllocateInfo`] defines a memory import operation, the memory\n  being imported **must** also be a dedicated image allocation and[`Self::image`] **must** be identical to the image associated with the imported\n  memory\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00655  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and[`crate::vk::MemoryAllocateInfo`] defines a memory import operation, the memory\n  being imported **must** also be a dedicated buffer allocation and[`Self::buffer`] **must** be identical to the buffer associated with the\n  imported memory\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-parameter  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image`] **must** be a valid [`crate::vk::Image`] handle\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-parameter  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-commonparent  \n   Both of [`Self::buffer`], and [`Self::image`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::Image`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkDedicatedAllocationMemoryAllocateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct DedicatedAllocationMemoryAllocateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub image: crate::vk1_0::Image,
    pub buffer: crate::vk1_0::Buffer,
}
impl DedicatedAllocationMemoryAllocateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV;
}
impl Default for DedicatedAllocationMemoryAllocateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), image: Default::default(), buffer: Default::default() }
    }
}
impl std::fmt::Debug for DedicatedAllocationMemoryAllocateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DedicatedAllocationMemoryAllocateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("image", &self.image).field("buffer", &self.buffer).finish()
    }
}
impl DedicatedAllocationMemoryAllocateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
        DedicatedAllocationMemoryAllocateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDedicatedAllocationMemoryAllocateInfoNV.html)) · Builder of [`DedicatedAllocationMemoryAllocateInfoNV`] <br/> VkDedicatedAllocationMemoryAllocateInfoNV - Specify a dedicated memory allocation resource\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain includes a[`crate::vk::DedicatedAllocationMemoryAllocateInfoNV`] structure, then that\nstructure includes a handle of the sole buffer or image resource that the\nmemory **can** be bound to.\n\nThe [`crate::vk::DedicatedAllocationMemoryAllocateInfoNV`] structure is defined as:\n\n```\n// Provided by VK_NV_dedicated_allocation\ntypedef struct VkDedicatedAllocationMemoryAllocateInfoNV {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkImage            image;\n    VkBuffer           buffer;\n} VkDedicatedAllocationMemoryAllocateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of an image which this\n  memory will be bound to.\n\n* [`Self::buffer`] is [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) or a handle of a buffer which this\n  memory will be bound to.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00649  \n   At least one of [`Self::image`] and [`Self::buffer`] **must** be[VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html)\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00650  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), the image **must** have been\n  created with[`crate::vk::DedicatedAllocationImageCreateInfoNV::dedicated_allocation`]equal to [`crate::vk::TRUE`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00651  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), the buffer **must** have been\n  created with[`crate::vk::DedicatedAllocationBufferCreateInfoNV::dedicated_allocation`]equal to [`crate::vk::TRUE`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00652  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`crate::vk::MemoryAllocateInfo`]::`allocationSize` **must** equal the[`crate::vk::MemoryRequirements`]::`size` of the image\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00653  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html),[`crate::vk::MemoryAllocateInfo`]::`allocationSize` **must** equal the[`crate::vk::MemoryRequirements`]::`size` of the buffer\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-00654  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and[`crate::vk::MemoryAllocateInfo`] defines a memory import operation, the memory\n  being imported **must** also be a dedicated image allocation and[`Self::image`] **must** be identical to the image associated with the imported\n  memory\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-00655  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html) and[`crate::vk::MemoryAllocateInfo`] defines a memory import operation, the memory\n  being imported **must** also be a dedicated buffer allocation and[`Self::buffer`] **must** be identical to the buffer associated with the\n  imported memory\n\nValid Usage (Implicit)\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV`]\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-image-parameter  \n   If [`Self::image`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image`] **must** be a valid [`crate::vk::Image`] handle\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-buffer-parameter  \n   If [`Self::buffer`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkDedicatedAllocationMemoryAllocateInfoNV-commonparent  \n   Both of [`Self::buffer`], and [`Self::image`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::Image`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct DedicatedAllocationMemoryAllocateInfoNVBuilder<'a>(DedicatedAllocationMemoryAllocateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
        DedicatedAllocationMemoryAllocateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image(mut self, image: crate::vk1_0::Image) -> Self {
        self.0.image = image as _;
        self
    }
    #[inline]
    pub fn buffer(mut self, buffer: crate::vk1_0::Buffer) -> Self {
        self.0.buffer = buffer as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> DedicatedAllocationMemoryAllocateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
    fn default() -> DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
    type Target = DedicatedAllocationMemoryAllocateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for DedicatedAllocationMemoryAllocateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
