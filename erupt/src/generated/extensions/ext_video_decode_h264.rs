//! ## Versioning Warning ⚠️
//!
//! This is a Vulkan **provisional/beta** extension and **must** be used with
//! caution. Its API/behaviour has not been finalized yet and _may_ therefore
//! change in ways that break backwards compatibility between revisions, and
//! before final release of a non-provisional version of this extension.
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VIDEO_DECODE_H264_SPEC_VERSION")]
pub const EXT_VIDEO_DECODE_H264_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_VIDEO_DECODE_H264_EXTENSION_NAME")]
pub const EXT_VIDEO_DECODE_H264_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_video_decode_h264");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264CreateFlagsEXT.html)) · Bitmask of [`VideoDecodeH264CreateFlagBitsEXT`] <br/> "] # [doc (alias = "VkVideoDecodeH264CreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct VideoDecodeH264CreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`VideoDecodeH264CreateFlagsEXT`] <br/> "]
#[doc(alias = "VkVideoDecodeH264CreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoDecodeH264CreateFlagBitsEXT(pub u32);
impl VideoDecodeH264CreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoDecodeH264CreateFlagsEXT {
        VideoDecodeH264CreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoDecodeH264CreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_video_decode_h264`]"]
impl crate::vk1_0::StructureType {
    pub const VIDEO_DECODE_H264_CAPABILITIES_EXT: Self = Self(1000040000);
    pub const VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT: Self = Self(1000040001);
    pub const VIDEO_DECODE_H264_PICTURE_INFO_EXT: Self = Self(1000040002);
    pub const VIDEO_DECODE_H264_MVC_EXT: Self = Self(1000040003);
    pub const VIDEO_DECODE_H264_PROFILE_EXT: Self = Self(1000040004);
    pub const VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT: Self = Self(1000040005);
    pub const VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT: Self = Self(1000040006);
    pub const VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT: Self = Self(1000040007);
}
#[doc = "Provided by [`crate::extensions::ext_video_decode_h264`]"]
impl crate::extensions::khr_video_queue::VideoCodecOperationFlagBitsKHR {
    pub const DECODE_H264_EXT: Self = Self(1);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagsEXT.html)) · Bitmask of [`VideoDecodeH264FieldLayoutFlagBitsEXT`] <br/> "] # [doc (alias = "VkVideoDecodeH264FieldLayoutFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct VideoDecodeH264FieldLayoutFlagsEXT : u32 { const VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT = VideoDecodeH264FieldLayoutFlagBitsEXT :: VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT . 0 ; const LINE_INTERLACED_PLANE_EXT = VideoDecodeH264FieldLayoutFlagBitsEXT :: LINE_INTERLACED_PLANE_EXT . 0 ; const SEPARATE_INTERLACED_PLANE_EXT = VideoDecodeH264FieldLayoutFlagBitsEXT :: SEPARATE_INTERLACED_PLANE_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagBitsEXT.html)) · Bits enum of [`VideoDecodeH264FieldLayoutFlagsEXT`] <br/> VkVideoDecodeH264FieldLayoutFlagBitsEXT - H.264 video decode field layout flags\n[](#_c_specification)C Specification\n----------\n\nThe H.264 decode interlaced field layout flags are defined with the\nfollowing enum:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef enum VkVideoDecodeH264FieldLayoutFlagBitsEXT {\n    VK_VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT = 0,\n    VK_VIDEO_DECODE_H264_FIELD_LAYOUT_LINE_INTERLACED_PLANE_BIT_EXT = 0x00000001,\n    VK_VIDEO_DECODE_H264_FIELD_LAYOUT_SEPARATE_INTERLACED_PLANE_BIT_EXT = 0x00000002,\n} VkVideoDecodeH264FieldLayoutFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::LINE_INTERLACED_PLANE_EXT`] -\n  Interlaced content is supported where each field is decoded within the\n  even/odd line of a picture resource.\n\n* [`Self::SEPARATE_INTERLACED_PLANE_EXT`]\\\\- Interlaced content is supported where each field is decoded within a\n  separate picture plane or resource.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::VideoDecodeH264FieldLayoutFlagBitsEXT`]\n"]
#[doc(alias = "VkVideoDecodeH264FieldLayoutFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct VideoDecodeH264FieldLayoutFlagBitsEXT(pub u32);
impl VideoDecodeH264FieldLayoutFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> VideoDecodeH264FieldLayoutFlagsEXT {
        VideoDecodeH264FieldLayoutFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for VideoDecodeH264FieldLayoutFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT => "VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT",
            &Self::LINE_INTERLACED_PLANE_EXT => "LINE_INTERLACED_PLANE_EXT",
            &Self::SEPARATE_INTERLACED_PLANE_EXT => "SEPARATE_INTERLACED_PLANE_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_video_decode_h264`]"]
impl crate::extensions::ext_video_decode_h264::VideoDecodeH264FieldLayoutFlagBitsEXT {
    pub const VIDEO_DECODE_H264_PROGRESSIVE_PICTURES_ONLY_EXT: Self = Self(0);
    pub const LINE_INTERLACED_PLANE_EXT: Self = Self(1);
    pub const SEPARATE_INTERLACED_PLANE_EXT: Self = Self(2);
}
impl<'a> crate::ExtendableFromMut<'a, VideoDecodeH264ProfileEXT> for crate::extensions::khr_video_queue::VideoProfileKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoDecodeH264ProfileEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoProfileKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoDecodeH264CapabilitiesEXT> for crate::extensions::khr_video_queue::VideoCapabilitiesKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, VideoDecodeH264CapabilitiesEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoCapabilitiesKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264DpbSlotInfoEXT> for crate::extensions::khr_video_queue::VideoReferenceSlotKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264DpbSlotInfoEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoReferenceSlotKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264PictureInfoEXT> for crate::extensions::khr_video_decode_queue::VideoDecodeInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264PictureInfoEXTBuilder<'_>> for crate::extensions::khr_video_decode_queue::VideoDecodeInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264ProfileEXT.html)) · Structure <br/> VkVideoDecodeH264ProfileEXT - Structure specifying H.264 decode profile\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264ProfileEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264ProfileEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    StdVideoH264ProfileIdc                  stdProfileIdc;\n    VkVideoDecodeH264FieldLayoutFlagsEXT    fieldLayout;\n} VkVideoDecodeH264ProfileEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::std_profile_idc`] is a [`crate::vk::StdVideoH264ProfileIdc`] value specifying\n  the H.264 codec profile IDC\n\n* [`Self::field_layout`] is a bitmask of[VkVideoDecodeH264FieldLayoutFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagBitsEXT.html) specifying the field\n  layout for interlaced content.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_PROFILE_EXT`]\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-fieldLayout-parameter  \n  [`Self::field_layout`] **must** be a valid combination of [VkVideoDecodeH264FieldLayoutFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagBitsEXT.html) values\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-fieldLayout-requiredbitmask  \n  [`Self::field_layout`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264FieldLayoutFlagBitsEXT`]\n"]
#[doc(alias = "VkVideoDecodeH264ProfileEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264ProfileEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub std_profile_idc: crate::external::vk_video::StdVideoH264ProfileIdc,
    pub field_layout: crate::extensions::ext_video_decode_h264::VideoDecodeH264FieldLayoutFlagsEXT,
}
impl VideoDecodeH264ProfileEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_PROFILE_EXT;
}
impl Default for VideoDecodeH264ProfileEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), std_profile_idc: Default::default(), field_layout: Default::default() }
    }
}
impl std::fmt::Debug for VideoDecodeH264ProfileEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264ProfileEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("std_profile_idc", &self.std_profile_idc).field("field_layout", &self.field_layout).finish()
    }
}
impl VideoDecodeH264ProfileEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264ProfileEXTBuilder<'a> {
        VideoDecodeH264ProfileEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264ProfileEXT.html)) · Builder of [`VideoDecodeH264ProfileEXT`] <br/> VkVideoDecodeH264ProfileEXT - Structure specifying H.264 decode profile\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264ProfileEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264ProfileEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    StdVideoH264ProfileIdc                  stdProfileIdc;\n    VkVideoDecodeH264FieldLayoutFlagsEXT    fieldLayout;\n} VkVideoDecodeH264ProfileEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::std_profile_idc`] is a [`crate::vk::StdVideoH264ProfileIdc`] value specifying\n  the H.264 codec profile IDC\n\n* [`Self::field_layout`] is a bitmask of[VkVideoDecodeH264FieldLayoutFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagBitsEXT.html) specifying the field\n  layout for interlaced content.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_PROFILE_EXT`]\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-fieldLayout-parameter  \n  [`Self::field_layout`] **must** be a valid combination of [VkVideoDecodeH264FieldLayoutFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264FieldLayoutFlagBitsEXT.html) values\n\n* []() VUID-VkVideoDecodeH264ProfileEXT-fieldLayout-requiredbitmask  \n  [`Self::field_layout`] **must** not be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264FieldLayoutFlagBitsEXT`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264ProfileEXTBuilder<'a>(VideoDecodeH264ProfileEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264ProfileEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264ProfileEXTBuilder<'a> {
        VideoDecodeH264ProfileEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn std_profile_idc(mut self, std_profile_idc: crate::external::vk_video::StdVideoH264ProfileIdc) -> Self {
        self.0.std_profile_idc = std_profile_idc as _;
        self
    }
    #[inline]
    pub fn field_layout(mut self, field_layout: crate::extensions::ext_video_decode_h264::VideoDecodeH264FieldLayoutFlagsEXT) -> Self {
        self.0.field_layout = field_layout as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264ProfileEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264ProfileEXTBuilder<'a> {
    fn default() -> VideoDecodeH264ProfileEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264ProfileEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264ProfileEXTBuilder<'a> {
    type Target = VideoDecodeH264ProfileEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264ProfileEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264CapabilitiesEXT.html)) · Structure <br/> VkVideoDecodeH264CapabilitiesEXT - Structure specifying H.264 decode capabilities\n[](#_c_specification)C Specification\n----------\n\nWhen using [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`] to query the\ncapabilities for the input `pVideoProfile` with`videoCodecOperation` specified as[`crate::vk::VideoCodecOperationFlagBitsKHR::DECODE_H264_EXT`], the instance of[`crate::vk::VideoDecodeH264CapabilitiesEXT`] structure **must** be chained to[`crate::vk::VideoCapabilitiesKHR`] to get this H.264 decode profile specific\ncapabilities.\n\nThe [`crate::vk::VideoDecodeH264CapabilitiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264CapabilitiesEXT {\n    VkStructureType          sType;\n    void*                    pNext;\n    uint32_t                 maxLevel;\n    VkOffset2D               fieldOffsetGranularity;\n    VkExtensionProperties    stdExtensionVersion;\n} VkVideoDecodeH264CapabilitiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_level`] is the maximum H.264 level supported by the device.\n\n* [`Self::field_offset_granularity`] - if Interlaced Video Content is suported,\n  the maximum field offset granularity supported for the picture resource.\n\n* [`Self::std_extension_version`] is a [`crate::vk::ExtensionProperties`] structure\n  specifying the H.264 extension name and version supported by this\n  implementation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264CapabilitiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_CAPABILITIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExtensionProperties`], [`crate::vk::Offset2D`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkVideoDecodeH264CapabilitiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264CapabilitiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_level: u32,
    pub field_offset_granularity: crate::vk1_0::Offset2D,
    pub std_extension_version: crate::vk1_0::ExtensionProperties,
}
impl VideoDecodeH264CapabilitiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_CAPABILITIES_EXT;
}
impl Default for VideoDecodeH264CapabilitiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_level: Default::default(), field_offset_granularity: Default::default(), std_extension_version: Default::default() }
    }
}
impl std::fmt::Debug for VideoDecodeH264CapabilitiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264CapabilitiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_level", &self.max_level).field("field_offset_granularity", &self.field_offset_granularity).field("std_extension_version", &self.std_extension_version).finish()
    }
}
impl VideoDecodeH264CapabilitiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264CapabilitiesEXTBuilder<'a> {
        VideoDecodeH264CapabilitiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264CapabilitiesEXT.html)) · Builder of [`VideoDecodeH264CapabilitiesEXT`] <br/> VkVideoDecodeH264CapabilitiesEXT - Structure specifying H.264 decode capabilities\n[](#_c_specification)C Specification\n----------\n\nWhen using [`crate::vk::DeviceLoader::get_physical_device_video_capabilities_khr`] to query the\ncapabilities for the input `pVideoProfile` with`videoCodecOperation` specified as[`crate::vk::VideoCodecOperationFlagBitsKHR::DECODE_H264_EXT`], the instance of[`crate::vk::VideoDecodeH264CapabilitiesEXT`] structure **must** be chained to[`crate::vk::VideoCapabilitiesKHR`] to get this H.264 decode profile specific\ncapabilities.\n\nThe [`crate::vk::VideoDecodeH264CapabilitiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264CapabilitiesEXT {\n    VkStructureType          sType;\n    void*                    pNext;\n    uint32_t                 maxLevel;\n    VkOffset2D               fieldOffsetGranularity;\n    VkExtensionProperties    stdExtensionVersion;\n} VkVideoDecodeH264CapabilitiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_level`] is the maximum H.264 level supported by the device.\n\n* [`Self::field_offset_granularity`] - if Interlaced Video Content is suported,\n  the maximum field offset granularity supported for the picture resource.\n\n* [`Self::std_extension_version`] is a [`crate::vk::ExtensionProperties`] structure\n  specifying the H.264 extension name and version supported by this\n  implementation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264CapabilitiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_CAPABILITIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExtensionProperties`], [`crate::vk::Offset2D`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264CapabilitiesEXTBuilder<'a>(VideoDecodeH264CapabilitiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264CapabilitiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264CapabilitiesEXTBuilder<'a> {
        VideoDecodeH264CapabilitiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_level(mut self, max_level: u32) -> Self {
        self.0.max_level = max_level as _;
        self
    }
    #[inline]
    pub fn field_offset_granularity(mut self, field_offset_granularity: crate::vk1_0::Offset2D) -> Self {
        self.0.field_offset_granularity = field_offset_granularity as _;
        self
    }
    #[inline]
    pub fn std_extension_version(mut self, std_extension_version: crate::vk1_0::ExtensionProperties) -> Self {
        self.0.std_extension_version = std_extension_version as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264CapabilitiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264CapabilitiesEXTBuilder<'a> {
    fn default() -> VideoDecodeH264CapabilitiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264CapabilitiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264CapabilitiesEXTBuilder<'a> {
    type Target = VideoDecodeH264CapabilitiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264CapabilitiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionCreateInfoEXT.html)) · Structure <br/> VkVideoDecodeH264SessionCreateInfoEXT - Structure specifies H.264 decode session creation parameters\n[](#_c_specification)C Specification\n----------\n\nThe instance of [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`] structure **can**be chained to [`crate::vk::VideoSessionCreateInfoKHR`] when the function[`crate::vk::DeviceLoader::create_video_session_khr`] is called to create a video session for H.264\ndecode.\n\nThe [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkVideoDecodeH264CreateFlagsEXT    flags;\n    const VkExtensionProperties*       pStdExtensionVersion;\n} VkVideoDecodeH264SessionCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::p_std_extension_version`] is a pointer to a [`crate::vk::ExtensionProperties`]structure specifying the H.264 codec extensions defined in`StdVideoH264Extensions`.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-pStdExtensionVersion-parameter  \n  [`Self::p_std_extension_version`] **must** be a valid pointer to a valid [`crate::vk::ExtensionProperties`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExtensionProperties`], [`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264CreateFlagBitsEXT`]\n"]
#[doc(alias = "VkVideoDecodeH264SessionCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264SessionCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_video_decode_h264::VideoDecodeH264CreateFlagsEXT,
    pub p_std_extension_version: *const crate::vk1_0::ExtensionProperties,
}
impl VideoDecodeH264SessionCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT;
}
impl Default for VideoDecodeH264SessionCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), p_std_extension_version: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264SessionCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264SessionCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("p_std_extension_version", &self.p_std_extension_version).finish()
    }
}
impl VideoDecodeH264SessionCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
        VideoDecodeH264SessionCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionCreateInfoEXT.html)) · Builder of [`VideoDecodeH264SessionCreateInfoEXT`] <br/> VkVideoDecodeH264SessionCreateInfoEXT - Structure specifies H.264 decode session creation parameters\n[](#_c_specification)C Specification\n----------\n\nThe instance of [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`] structure **can**be chained to [`crate::vk::VideoSessionCreateInfoKHR`] when the function[`crate::vk::DeviceLoader::create_video_session_khr`] is called to create a video session for H.264\ndecode.\n\nThe [`crate::vk::VideoDecodeH264SessionCreateInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionCreateInfoEXT {\n    VkStructureType                    sType;\n    const void*                        pNext;\n    VkVideoDecodeH264CreateFlagsEXT    flags;\n    const VkExtensionProperties*       pStdExtensionVersion;\n} VkVideoDecodeH264SessionCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::p_std_extension_version`] is a pointer to a [`crate::vk::ExtensionProperties`]structure specifying the H.264 codec extensions defined in`StdVideoH264Extensions`.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkVideoDecodeH264SessionCreateInfoEXT-pStdExtensionVersion-parameter  \n  [`Self::p_std_extension_version`] **must** be a valid pointer to a valid [`crate::vk::ExtensionProperties`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExtensionProperties`], [`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264CreateFlagBitsEXT`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264SessionCreateInfoEXTBuilder<'a>(VideoDecodeH264SessionCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
        VideoDecodeH264SessionCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_video_decode_h264::VideoDecodeH264CreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn std_extension_version(mut self, std_extension_version: &'a crate::vk1_0::ExtensionProperties) -> Self {
        self.0.p_std_extension_version = std_extension_version as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264SessionCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
    fn default() -> VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
    type Target = VideoDecodeH264SessionCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264SessionCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionParametersAddInfoEXT.html)) · Structure <br/> VkVideoDecodeH264SessionParametersAddInfoEXT - Structure specifies H.264 decoder parameter set information\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionParametersAddInfoEXT {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    uint32_t                                   spsStdCount;\n    const StdVideoH264SequenceParameterSet*    pSpsStd;\n    uint32_t                                   ppsStdCount;\n    const StdVideoH264PictureParameterSet*     pPpsStd;\n} VkVideoDecodeH264SessionParametersAddInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sps_std_count`] is the number of SPS elements in [`Self::p_sps_std`].\n  Its value **must** be less than or equal to the value of`maxSpsStdCount`.\n\n* [`Self::p_sps_std`] is a pointer to an array of[`crate::vk::StdVideoH264SequenceParameterSet`] structures representing H.264\n  sequence parameter sets.\n  Each element of the array **must** have a unique H.264 SPS ID.\n\n* [`Self::pps_std_count`] is the number of PPS provided in [`Self::p_pps_std`].\n  Its value **must** be less than or equal to the value of`maxPpsStdCount`.\n\n* [`Self::p_pps_std`] is a pointer to an array of[`crate::vk::StdVideoH264PictureParameterSet`] structures representing H.264\n  picture parameter sets.\n  Each element of the array **must** have a unique H.264 SPS-PPS ID pair.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-spsStdCount-04822  \n   The values of [`Self::sps_std_count`] and [`Self::pps_std_count`] **must** be less than\n  or equal to the values of `maxSpsStdCount` and `maxPpsStdCount`,\n  respectively.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-maxSpsStdCount-04823  \n   When the `maxSpsStdCount` number of parameters of type\n  StdVideoH264SequenceParameterSet in the Video Session Parameters object\n  is reached, no additional parameters of that type can be added to this\n  object.[`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`] will be returned if an attempt is made\n  to add additional data to this object at this point.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-maxPpsStdCount-04824  \n   When the `maxPpsStdCount` number of parameters of type\n  StdVideoH264PictureParameterSet in the Video Session Parameters object\n  is reached, no additional parameters of that type can be added to this\n  object.[`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`] will be returned if an attempt is made\n  to add additional data to this object at this point.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04825  \n   Each entry to be added **must** have a unique, to the rest of the parameter\n  array entries and the existing parameters in the Video Session\n  Parameters Object that is being updated, SPS-PPS IDs.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04826  \n   Parameter entries that already exist in Video Session Parameters object\n  with a particular SPS-PPS IDs **cannot** be replaced nor updated.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04827  \n   When creating a new object using a Video Session Parameters as a\n  template, the array’s parameters with the same SPS-PPS IDs as the ones\n  from the template take precedence.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04828  \n   SPS/PPS parameters **must** comply with the limits specified in[`crate::vk::VideoSessionCreateInfoKHR`] during Video Session creation.\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-pSpsStd-parameter  \n   If [`Self::p_sps_std`] is not `NULL`, [`Self::p_sps_std`] **must** be a valid pointer to an array of [`Self::sps_std_count`] [`crate::vk::StdVideoH264SequenceParameterSet`] values\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-pPpsStd-parameter  \n   If [`Self::p_pps_std`] is not `NULL`, [`Self::p_pps_std`] **must** be a valid pointer to an array of [`Self::pps_std_count`] [`crate::vk::StdVideoH264PictureParameterSet`] values\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-spsStdCount-arraylength  \n  [`Self::sps_std_count`] **must** be greater than `0`\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-ppsStdCount-arraylength  \n  [`Self::pps_std_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`]\n"]
#[doc(alias = "VkVideoDecodeH264SessionParametersAddInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264SessionParametersAddInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub sps_std_count: u32,
    pub p_sps_std: *const crate::external::vk_video::StdVideoH264SequenceParameterSet,
    pub pps_std_count: u32,
    pub p_pps_std: *const crate::external::vk_video::StdVideoH264PictureParameterSet,
}
impl VideoDecodeH264SessionParametersAddInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT;
}
impl Default for VideoDecodeH264SessionParametersAddInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), sps_std_count: Default::default(), p_sps_std: std::ptr::null(), pps_std_count: Default::default(), p_pps_std: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264SessionParametersAddInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264SessionParametersAddInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("sps_std_count", &self.sps_std_count).field("p_sps_std", &self.p_sps_std).field("pps_std_count", &self.pps_std_count).field("p_pps_std", &self.p_pps_std).finish()
    }
}
impl VideoDecodeH264SessionParametersAddInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
        VideoDecodeH264SessionParametersAddInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionParametersAddInfoEXT.html)) · Builder of [`VideoDecodeH264SessionParametersAddInfoEXT`] <br/> VkVideoDecodeH264SessionParametersAddInfoEXT - Structure specifies H.264 decoder parameter set information\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionParametersAddInfoEXT {\n    VkStructureType                            sType;\n    const void*                                pNext;\n    uint32_t                                   spsStdCount;\n    const StdVideoH264SequenceParameterSet*    pSpsStd;\n    uint32_t                                   ppsStdCount;\n    const StdVideoH264PictureParameterSet*     pPpsStd;\n} VkVideoDecodeH264SessionParametersAddInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sps_std_count`] is the number of SPS elements in [`Self::p_sps_std`].\n  Its value **must** be less than or equal to the value of`maxSpsStdCount`.\n\n* [`Self::p_sps_std`] is a pointer to an array of[`crate::vk::StdVideoH264SequenceParameterSet`] structures representing H.264\n  sequence parameter sets.\n  Each element of the array **must** have a unique H.264 SPS ID.\n\n* [`Self::pps_std_count`] is the number of PPS provided in [`Self::p_pps_std`].\n  Its value **must** be less than or equal to the value of`maxPpsStdCount`.\n\n* [`Self::p_pps_std`] is a pointer to an array of[`crate::vk::StdVideoH264PictureParameterSet`] structures representing H.264\n  picture parameter sets.\n  Each element of the array **must** have a unique H.264 SPS-PPS ID pair.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-spsStdCount-04822  \n   The values of [`Self::sps_std_count`] and [`Self::pps_std_count`] **must** be less than\n  or equal to the values of `maxSpsStdCount` and `maxPpsStdCount`,\n  respectively.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-maxSpsStdCount-04823  \n   When the `maxSpsStdCount` number of parameters of type\n  StdVideoH264SequenceParameterSet in the Video Session Parameters object\n  is reached, no additional parameters of that type can be added to this\n  object.[`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`] will be returned if an attempt is made\n  to add additional data to this object at this point.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-maxPpsStdCount-04824  \n   When the `maxPpsStdCount` number of parameters of type\n  StdVideoH264PictureParameterSet in the Video Session Parameters object\n  is reached, no additional parameters of that type can be added to this\n  object.[`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`] will be returned if an attempt is made\n  to add additional data to this object at this point.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04825  \n   Each entry to be added **must** have a unique, to the rest of the parameter\n  array entries and the existing parameters in the Video Session\n  Parameters Object that is being updated, SPS-PPS IDs.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04826  \n   Parameter entries that already exist in Video Session Parameters object\n  with a particular SPS-PPS IDs **cannot** be replaced nor updated.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04827  \n   When creating a new object using a Video Session Parameters as a\n  template, the array’s parameters with the same SPS-PPS IDs as the ones\n  from the template take precedence.\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-None-04828  \n   SPS/PPS parameters **must** comply with the limits specified in[`crate::vk::VideoSessionCreateInfoKHR`] during Video Session creation.\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-pSpsStd-parameter  \n   If [`Self::p_sps_std`] is not `NULL`, [`Self::p_sps_std`] **must** be a valid pointer to an array of [`Self::sps_std_count`] [`crate::vk::StdVideoH264SequenceParameterSet`] values\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-pPpsStd-parameter  \n   If [`Self::p_pps_std`] is not `NULL`, [`Self::p_pps_std`] **must** be a valid pointer to an array of [`Self::pps_std_count`] [`crate::vk::StdVideoH264PictureParameterSet`] values\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-spsStdCount-arraylength  \n  [`Self::sps_std_count`] **must** be greater than `0`\n\n* []() VUID-VkVideoDecodeH264SessionParametersAddInfoEXT-ppsStdCount-arraylength  \n  [`Self::pps_std_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a>(VideoDecodeH264SessionParametersAddInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
        VideoDecodeH264SessionParametersAddInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sps_std(mut self, sps_std: &'a [crate::external::vk_video::StdVideoH264SequenceParameterSetBuilder]) -> Self {
        self.0.p_sps_std = sps_std.as_ptr() as _;
        self.0.sps_std_count = sps_std.len() as _;
        self
    }
    #[inline]
    pub fn pps_std(mut self, pps_std: &'a [crate::external::vk_video::StdVideoH264PictureParameterSetBuilder]) -> Self {
        self.0.p_pps_std = pps_std.as_ptr() as _;
        self.0.pps_std_count = pps_std.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264SessionParametersAddInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
    fn default() -> VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
    type Target = VideoDecodeH264SessionParametersAddInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264SessionParametersAddInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionParametersCreateInfoEXT.html)) · Structure <br/> VkVideoDecodeH264SessionParametersCreateInfoEXT - Structure specifies H.264 decoder parameter set information\n[](#_c_specification)C Specification\n----------\n\nAn instance of [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`] holding\none H.264 SPS and at least one H.264 PPS paramater set **must** be chained to[`crate::vk::VideoSessionParametersCreateInfoKHR`] when calling[`crate::vk::DeviceLoader::create_video_session_parameters_khr`] to store these parameter set(s) with\nthe decoder parameter set object for later reference.\nThe provided H.264 SPS/PPS parameters **must** be within the limits specified\nduring decoder creation for the decoder specified in[`crate::vk::VideoSessionParametersCreateInfoKHR`].\n\nThe [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionParametersCreateInfoEXT {\n    VkStructureType                                        sType;\n    const void*                                            pNext;\n    uint32_t                                               maxSpsStdCount;\n    uint32_t                                               maxPpsStdCount;\n    const VkVideoDecodeH264SessionParametersAddInfoEXT*    pParametersAddInfo;\n} VkVideoDecodeH264SessionParametersCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_sps_std_count`] is the maximum number of SPS parameters that the[`crate::vk::VideoSessionParametersKHR`] can contain.\n\n* [`Self::max_pps_std_count`] is the maximum number of PPS parameters that the[`crate::vk::VideoSessionParametersKHR`] can contain.\n\n* [`Self::p_parameters_add_info`] is `NULL` or a pointer to a[`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`] structure specifying\n  H.264 parameters to add upon object creation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionParametersCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionParametersCreateInfoEXT-pParametersAddInfo-parameter  \n   If [`Self::p_parameters_add_info`] is not `NULL`, [`Self::p_parameters_add_info`] **must** be a valid pointer to a valid [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`]\n"]
#[doc(alias = "VkVideoDecodeH264SessionParametersCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264SessionParametersCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub max_sps_std_count: u32,
    pub max_pps_std_count: u32,
    pub p_parameters_add_info: *const crate::extensions::ext_video_decode_h264::VideoDecodeH264SessionParametersAddInfoEXT,
}
impl VideoDecodeH264SessionParametersCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT;
}
impl Default for VideoDecodeH264SessionParametersCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), max_sps_std_count: Default::default(), max_pps_std_count: Default::default(), p_parameters_add_info: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264SessionParametersCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264SessionParametersCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_sps_std_count", &self.max_sps_std_count).field("max_pps_std_count", &self.max_pps_std_count).field("p_parameters_add_info", &self.p_parameters_add_info).finish()
    }
}
impl VideoDecodeH264SessionParametersCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
        VideoDecodeH264SessionParametersCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264SessionParametersCreateInfoEXT.html)) · Builder of [`VideoDecodeH264SessionParametersCreateInfoEXT`] <br/> VkVideoDecodeH264SessionParametersCreateInfoEXT - Structure specifies H.264 decoder parameter set information\n[](#_c_specification)C Specification\n----------\n\nAn instance of [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`] holding\none H.264 SPS and at least one H.264 PPS paramater set **must** be chained to[`crate::vk::VideoSessionParametersCreateInfoKHR`] when calling[`crate::vk::DeviceLoader::create_video_session_parameters_khr`] to store these parameter set(s) with\nthe decoder parameter set object for later reference.\nThe provided H.264 SPS/PPS parameters **must** be within the limits specified\nduring decoder creation for the decoder specified in[`crate::vk::VideoSessionParametersCreateInfoKHR`].\n\nThe [`crate::vk::VideoDecodeH264SessionParametersCreateInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264SessionParametersCreateInfoEXT {\n    VkStructureType                                        sType;\n    const void*                                            pNext;\n    uint32_t                                               maxSpsStdCount;\n    uint32_t                                               maxPpsStdCount;\n    const VkVideoDecodeH264SessionParametersAddInfoEXT*    pParametersAddInfo;\n} VkVideoDecodeH264SessionParametersCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::max_sps_std_count`] is the maximum number of SPS parameters that the[`crate::vk::VideoSessionParametersKHR`] can contain.\n\n* [`Self::max_pps_std_count`] is the maximum number of PPS parameters that the[`crate::vk::VideoSessionParametersKHR`] can contain.\n\n* [`Self::p_parameters_add_info`] is `NULL` or a pointer to a[`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`] structure specifying\n  H.264 parameters to add upon object creation.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264SessionParametersCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264SessionParametersCreateInfoEXT-pParametersAddInfo-parameter  \n   If [`Self::p_parameters_add_info`] is not `NULL`, [`Self::p_parameters_add_info`] **must** be a valid pointer to a valid [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::VideoDecodeH264SessionParametersAddInfoEXT`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a>(VideoDecodeH264SessionParametersCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
        VideoDecodeH264SessionParametersCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_sps_std_count(mut self, max_sps_std_count: u32) -> Self {
        self.0.max_sps_std_count = max_sps_std_count as _;
        self
    }
    #[inline]
    pub fn max_pps_std_count(mut self, max_pps_std_count: u32) -> Self {
        self.0.max_pps_std_count = max_pps_std_count as _;
        self
    }
    #[inline]
    pub fn parameters_add_info(mut self, parameters_add_info: &'a crate::extensions::ext_video_decode_h264::VideoDecodeH264SessionParametersAddInfoEXT) -> Self {
        self.0.p_parameters_add_info = parameters_add_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264SessionParametersCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
    fn default() -> VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
    type Target = VideoDecodeH264SessionParametersCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264PictureInfoEXT.html)) · Structure <br/> VkVideoDecodeH264PictureInfoEXT - Structure specifies H.264 decode picture parameters when decoding a picture\n[](#_c_specification)C Specification\n----------\n\nThe structure [`crate::vk::VideoDecodeH264PictureInfoEXT`] representing a picture\ndecode operation is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264PictureInfoEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    const StdVideoDecodeH264PictureInfo*    pStdPictureInfo;\n    uint32_t                                slicesCount;\n    const uint32_t*                         pSlicesDataOffsets;\n} VkVideoDecodeH264PictureInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_std_picture_info`] is a pointer to a[`crate::vk::StdVideoDecodeH264PictureInfo`] structure specifying the codec\n  standard specific picture information from the H.264 specification.\n\n* [`Self::slices_count`] is the number of slices in this picture.\n\n* [`Self::p_slices_data_offsets`] is a pointer to an array of [`Self::slices_count`]offsets indicating the start offset of each slice within the bitstream\n  buffer.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_PICTURE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-pStdPictureInfo-parameter  \n  [`Self::p_std_picture_info`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264PictureInfo`] value\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-pSlicesDataOffsets-parameter  \n  [`Self::p_slices_data_offsets`] **must** be a valid pointer to an array of [`Self::slices_count`] `uint32_t` values\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-slicesCount-arraylength  \n  [`Self::slices_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkVideoDecodeH264PictureInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264PictureInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_std_picture_info: *const crate::external::vk_video::StdVideoDecodeH264PictureInfo,
    pub slices_count: u32,
    pub p_slices_data_offsets: *const u32,
}
impl VideoDecodeH264PictureInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_PICTURE_INFO_EXT;
}
impl Default for VideoDecodeH264PictureInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_std_picture_info: std::ptr::null(), slices_count: Default::default(), p_slices_data_offsets: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264PictureInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264PictureInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_std_picture_info", &self.p_std_picture_info).field("slices_count", &self.slices_count).field("p_slices_data_offsets", &self.p_slices_data_offsets).finish()
    }
}
impl VideoDecodeH264PictureInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264PictureInfoEXTBuilder<'a> {
        VideoDecodeH264PictureInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264MvcEXT> for crate::extensions::ext_video_decode_h264::VideoDecodeH264PictureInfoEXTBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264MvcEXTBuilder<'_>> for crate::extensions::ext_video_decode_h264::VideoDecodeH264PictureInfoEXTBuilder<'a> {}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264PictureInfoEXT.html)) · Builder of [`VideoDecodeH264PictureInfoEXT`] <br/> VkVideoDecodeH264PictureInfoEXT - Structure specifies H.264 decode picture parameters when decoding a picture\n[](#_c_specification)C Specification\n----------\n\nThe structure [`crate::vk::VideoDecodeH264PictureInfoEXT`] representing a picture\ndecode operation is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264PictureInfoEXT {\n    VkStructureType                         sType;\n    const void*                             pNext;\n    const StdVideoDecodeH264PictureInfo*    pStdPictureInfo;\n    uint32_t                                slicesCount;\n    const uint32_t*                         pSlicesDataOffsets;\n} VkVideoDecodeH264PictureInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_std_picture_info`] is a pointer to a[`crate::vk::StdVideoDecodeH264PictureInfo`] structure specifying the codec\n  standard specific picture information from the H.264 specification.\n\n* [`Self::slices_count`] is the number of slices in this picture.\n\n* [`Self::p_slices_data_offsets`] is a pointer to an array of [`Self::slices_count`]offsets indicating the start offset of each slice within the bitstream\n  buffer.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_PICTURE_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-pStdPictureInfo-parameter  \n  [`Self::p_std_picture_info`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264PictureInfo`] value\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-pSlicesDataOffsets-parameter  \n  [`Self::p_slices_data_offsets`] **must** be a valid pointer to an array of [`Self::slices_count`] `uint32_t` values\n\n* []() VUID-VkVideoDecodeH264PictureInfoEXT-slicesCount-arraylength  \n  [`Self::slices_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264PictureInfoEXTBuilder<'a>(VideoDecodeH264PictureInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264PictureInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264PictureInfoEXTBuilder<'a> {
        VideoDecodeH264PictureInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn std_picture_info(mut self, std_picture_info: &'a crate::external::vk_video::StdVideoDecodeH264PictureInfo) -> Self {
        self.0.p_std_picture_info = std_picture_info as _;
        self
    }
    #[inline]
    pub fn slices_data_offsets(mut self, slices_data_offsets: &'a [u32]) -> Self {
        self.0.p_slices_data_offsets = slices_data_offsets.as_ptr() as _;
        self.0.slices_count = slices_data_offsets.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264PictureInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264PictureInfoEXTBuilder<'a> {
    fn default() -> VideoDecodeH264PictureInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264PictureInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264PictureInfoEXTBuilder<'a> {
    type Target = VideoDecodeH264PictureInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264PictureInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264DpbSlotInfoEXT.html)) · Structure <br/> VkVideoDecodeH264DpbSlotInfoEXT - Structure specifies H.264 decode DPB picture information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264DpbSlotInfoEXT`] structure correlates a DPB Slot\nindex with codec-specific information and is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264DpbSlotInfoEXT {\n    VkStructureType                           sType;\n    const void*                               pNext;\n    const StdVideoDecodeH264ReferenceInfo*    pStdReferenceInfo;\n} VkVideoDecodeH264DpbSlotInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_std_reference_info`] is a pointer to a[`crate::vk::StdVideoDecodeH264ReferenceInfo`] structure specifying the codec\n  standard specific picture reference information from the H.264\n  specification.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264DpbSlotInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264DpbSlotInfoEXT-pStdReferenceInfo-parameter  \n  [`Self::p_std_reference_info`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264ReferenceInfo`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkVideoDecodeH264DpbSlotInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264DpbSlotInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_std_reference_info: *const crate::external::vk_video::StdVideoDecodeH264ReferenceInfo,
}
impl VideoDecodeH264DpbSlotInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT;
}
impl Default for VideoDecodeH264DpbSlotInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_std_reference_info: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264DpbSlotInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264DpbSlotInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_std_reference_info", &self.p_std_reference_info).finish()
    }
}
impl VideoDecodeH264DpbSlotInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
        VideoDecodeH264DpbSlotInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264DpbSlotInfoEXT.html)) · Builder of [`VideoDecodeH264DpbSlotInfoEXT`] <br/> VkVideoDecodeH264DpbSlotInfoEXT - Structure specifies H.264 decode DPB picture information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264DpbSlotInfoEXT`] structure correlates a DPB Slot\nindex with codec-specific information and is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264DpbSlotInfoEXT {\n    VkStructureType                           sType;\n    const void*                               pNext;\n    const StdVideoDecodeH264ReferenceInfo*    pStdReferenceInfo;\n} VkVideoDecodeH264DpbSlotInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_std_reference_info`] is a pointer to a[`crate::vk::StdVideoDecodeH264ReferenceInfo`] structure specifying the codec\n  standard specific picture reference information from the H.264\n  specification.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264DpbSlotInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT`]\n\n* []() VUID-VkVideoDecodeH264DpbSlotInfoEXT-pStdReferenceInfo-parameter  \n  [`Self::p_std_reference_info`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264ReferenceInfo`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264DpbSlotInfoEXTBuilder<'a>(VideoDecodeH264DpbSlotInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
        VideoDecodeH264DpbSlotInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn std_reference_info(mut self, std_reference_info: &'a crate::external::vk_video::StdVideoDecodeH264ReferenceInfo) -> Self {
        self.0.p_std_reference_info = std_reference_info as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264DpbSlotInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
    fn default() -> VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
    type Target = VideoDecodeH264DpbSlotInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264DpbSlotInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264MvcEXT.html)) · Structure <br/> VkVideoDecodeH264MvcEXT - Structure specifies parameters of mvc views\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264MvcEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264MvcEXT {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    const StdVideoDecodeH264Mvc*    pStdMvc;\n} VkVideoDecodeH264MvcEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_std_mvc`] is a pointer to a [`crate::vk::StdVideoDecodeH264Mvc`] structure\n  specifying H.264 codec specification information for MVC.\n[](#_description)Description\n----------\n\nWhen the content type is H.264 MVC, an instance of[`crate::vk::VideoDecodeH264MvcEXT`] **must** be chained to[`crate::vk::VideoDecodeH264PictureInfoEXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264MvcEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_MVC_EXT`]\n\n* []() VUID-VkVideoDecodeH264MvcEXT-pStdMvc-parameter  \n  [`Self::p_std_mvc`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264Mvc`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkVideoDecodeH264MvcEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct VideoDecodeH264MvcEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_std_mvc: *const crate::external::vk_video::StdVideoDecodeH264Mvc,
}
impl VideoDecodeH264MvcEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::VIDEO_DECODE_H264_MVC_EXT;
}
impl Default for VideoDecodeH264MvcEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_std_mvc: std::ptr::null() }
    }
}
impl std::fmt::Debug for VideoDecodeH264MvcEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("VideoDecodeH264MvcEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_std_mvc", &self.p_std_mvc).finish()
    }
}
impl VideoDecodeH264MvcEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> VideoDecodeH264MvcEXTBuilder<'a> {
        VideoDecodeH264MvcEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkVideoDecodeH264MvcEXT.html)) · Builder of [`VideoDecodeH264MvcEXT`] <br/> VkVideoDecodeH264MvcEXT - Structure specifies parameters of mvc views\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::VideoDecodeH264MvcEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_video_decode_h264\ntypedef struct VkVideoDecodeH264MvcEXT {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    const StdVideoDecodeH264Mvc*    pStdMvc;\n} VkVideoDecodeH264MvcEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_std_mvc`] is a pointer to a [`crate::vk::StdVideoDecodeH264Mvc`] structure\n  specifying H.264 codec specification information for MVC.\n[](#_description)Description\n----------\n\nWhen the content type is H.264 MVC, an instance of[`crate::vk::VideoDecodeH264MvcEXT`] **must** be chained to[`crate::vk::VideoDecodeH264PictureInfoEXT`].\n\nValid Usage (Implicit)\n\n* []() VUID-VkVideoDecodeH264MvcEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::VIDEO_DECODE_H264_MVC_EXT`]\n\n* []() VUID-VkVideoDecodeH264MvcEXT-pStdMvc-parameter  \n  [`Self::p_std_mvc`] **must** be a valid pointer to a valid [`crate::vk::StdVideoDecodeH264Mvc`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct VideoDecodeH264MvcEXTBuilder<'a>(VideoDecodeH264MvcEXT, std::marker::PhantomData<&'a ()>);
impl<'a> VideoDecodeH264MvcEXTBuilder<'a> {
    #[inline]
    pub fn new() -> VideoDecodeH264MvcEXTBuilder<'a> {
        VideoDecodeH264MvcEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn std_mvc(mut self, std_mvc: &'a crate::external::vk_video::StdVideoDecodeH264Mvc) -> Self {
        self.0.p_std_mvc = std_mvc as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> VideoDecodeH264MvcEXT {
        self.0
    }
}
impl<'a> std::default::Default for VideoDecodeH264MvcEXTBuilder<'a> {
    fn default() -> VideoDecodeH264MvcEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for VideoDecodeH264MvcEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for VideoDecodeH264MvcEXTBuilder<'a> {
    type Target = VideoDecodeH264MvcEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for VideoDecodeH264MvcEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionCreateInfoEXT> for crate::extensions::khr_video_queue::VideoSessionCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionCreateInfoEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoSessionCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionParametersCreateInfoEXT> for crate::extensions::khr_video_queue::VideoSessionParametersCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionParametersCreateInfoEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoSessionParametersCreateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionParametersAddInfoEXT> for crate::extensions::khr_video_queue::VideoSessionParametersUpdateInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, VideoDecodeH264SessionParametersAddInfoEXTBuilder<'_>> for crate::extensions::khr_video_queue::VideoSessionParametersUpdateInfoKHRBuilder<'a> {}
