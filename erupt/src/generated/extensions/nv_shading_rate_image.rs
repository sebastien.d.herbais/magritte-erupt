#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADING_RATE_IMAGE_SPEC_VERSION")]
pub const NV_SHADING_RATE_IMAGE_SPEC_VERSION: u32 = 3;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_SHADING_RATE_IMAGE_EXTENSION_NAME")]
pub const NV_SHADING_RATE_IMAGE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_shading_rate_image");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BIND_SHADING_RATE_IMAGE_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdBindShadingRateImageNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_VIEWPORT_SHADING_RATE_PALETTE_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdSetViewportShadingRatePaletteNV");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_COARSE_SAMPLE_ORDER_NV: *const std::os::raw::c_char = crate::cstr!("vkCmdSetCoarseSampleOrderNV");
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::DynamicState {
    pub const VIEWPORT_SHADING_RATE_PALETTE_NV: Self = Self(1000164004);
    pub const VIEWPORT_COARSE_SAMPLE_ORDER_NV: Self = Self(1000164006);
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::ImageLayout {
    pub const SHADING_RATE_OPTIMAL_NV: Self = Self::FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR;
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::ImageUsageFlagBits {
    pub const SHADING_RATE_IMAGE_NV: Self = Self::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR;
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::AccessFlagBits {
    pub const SHADING_RATE_IMAGE_READ_NV: Self = Self::FRAGMENT_SHADING_RATE_ATTACHMENT_READ_KHR;
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV: Self = Self(1000164000);
    pub const PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV: Self = Self(1000164001);
    pub const PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV: Self = Self(1000164002);
    pub const PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV: Self = Self(1000164005);
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::vk1_0::PipelineStageFlagBits {
    pub const SHADING_RATE_IMAGE_NV: Self = Self::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR;
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShadingRatePaletteEntryNV.html)) · Enum <br/> VkShadingRatePaletteEntryNV - Shading rate image palette entry types\n[](#_c_specification)C Specification\n----------\n\nThe supported shading rate image palette entries are defined by[`crate::vk::ShadingRatePaletteEntryNV`]:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef enum VkShadingRatePaletteEntryNV {\n    VK_SHADING_RATE_PALETTE_ENTRY_NO_INVOCATIONS_NV = 0,\n    VK_SHADING_RATE_PALETTE_ENTRY_16_INVOCATIONS_PER_PIXEL_NV = 1,\n    VK_SHADING_RATE_PALETTE_ENTRY_8_INVOCATIONS_PER_PIXEL_NV = 2,\n    VK_SHADING_RATE_PALETTE_ENTRY_4_INVOCATIONS_PER_PIXEL_NV = 3,\n    VK_SHADING_RATE_PALETTE_ENTRY_2_INVOCATIONS_PER_PIXEL_NV = 4,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_PIXEL_NV = 5,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X1_PIXELS_NV = 6,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_1X2_PIXELS_NV = 7,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X2_PIXELS_NV = 8,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X2_PIXELS_NV = 9,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X4_PIXELS_NV = 10,\n    VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X4_PIXELS_NV = 11,\n} VkShadingRatePaletteEntryNV;\n```\n[](#_description)Description\n----------\n\nThe following table indicates the width and height (in pixels) of each\nfragment generated using the indicated shading rate, as well as the maximum\nnumber of fragment shader invocations launched for each fragment.\nWhen processing regions of a primitive that have a shading rate of[`Self::NO_INVOCATIONS_NV`], no fragments will be\ngenerated in that region.\n\n|                         Shading Rate                         |Width|Height|Invocations|\n|--------------------------------------------------------------|-----|------|-----------|\n|      [`Self::NO_INVOCATIONS_NV`]       |  0  |  0   |     0     |\n| [`Self::_16_INVOCATIONS_PER_PIXEL_NV`]  |  1  |  1   |    16     |\n|  [`Self::_8_INVOCATIONS_PER_PIXEL_NV`]  |  1  |  1   |     8     |\n|  [`Self::_4_INVOCATIONS_PER_PIXEL_NV`]  |  1  |  1   |     4     |\n|  [`Self::_2_INVOCATIONS_PER_PIXEL_NV`]  |  1  |  1   |     2     |\n|  [`Self::_1_INVOCATION_PER_PIXEL_NV`]   |  1  |  1   |     1     |\n|[`Self::_1_INVOCATION_PER_2X1_PIXELS_NV`]|  2  |  1   |     1     |\n|[`Self::_1_INVOCATION_PER_1X2_PIXELS_NV`]|  1  |  2   |     1     |\n|[`Self::_1_INVOCATION_PER_2X2_PIXELS_NV`]|  2  |  2   |     1     |\n|[`Self::_1_INVOCATION_PER_4X2_PIXELS_NV`]|  4  |  2   |     1     |\n|[`Self::_1_INVOCATION_PER_2X4_PIXELS_NV`]|  2  |  4   |     1     |\n|[`Self::_1_INVOCATION_PER_4X4_PIXELS_NV`]|  4  |  4   |     1     |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`], [`crate::vk::ShadingRatePaletteNV`]\n"]
#[doc(alias = "VkShadingRatePaletteEntryNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ShadingRatePaletteEntryNV(pub i32);
impl std::fmt::Debug for ShadingRatePaletteEntryNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::NO_INVOCATIONS_NV => "NO_INVOCATIONS_NV",
            &Self::_16_INVOCATIONS_PER_PIXEL_NV => "_16_INVOCATIONS_PER_PIXEL_NV",
            &Self::_8_INVOCATIONS_PER_PIXEL_NV => "_8_INVOCATIONS_PER_PIXEL_NV",
            &Self::_4_INVOCATIONS_PER_PIXEL_NV => "_4_INVOCATIONS_PER_PIXEL_NV",
            &Self::_2_INVOCATIONS_PER_PIXEL_NV => "_2_INVOCATIONS_PER_PIXEL_NV",
            &Self::_1_INVOCATION_PER_PIXEL_NV => "_1_INVOCATION_PER_PIXEL_NV",
            &Self::_1_INVOCATION_PER_2X1_PIXELS_NV => "_1_INVOCATION_PER_2X1_PIXELS_NV",
            &Self::_1_INVOCATION_PER_1X2_PIXELS_NV => "_1_INVOCATION_PER_1X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_2X2_PIXELS_NV => "_1_INVOCATION_PER_2X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_4X2_PIXELS_NV => "_1_INVOCATION_PER_4X2_PIXELS_NV",
            &Self::_1_INVOCATION_PER_2X4_PIXELS_NV => "_1_INVOCATION_PER_2X4_PIXELS_NV",
            &Self::_1_INVOCATION_PER_4X4_PIXELS_NV => "_1_INVOCATION_PER_4X4_PIXELS_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::extensions::nv_shading_rate_image::ShadingRatePaletteEntryNV {
    pub const NO_INVOCATIONS_NV: Self = Self(0);
    pub const _16_INVOCATIONS_PER_PIXEL_NV: Self = Self(1);
    pub const _8_INVOCATIONS_PER_PIXEL_NV: Self = Self(2);
    pub const _4_INVOCATIONS_PER_PIXEL_NV: Self = Self(3);
    pub const _2_INVOCATIONS_PER_PIXEL_NV: Self = Self(4);
    pub const _1_INVOCATION_PER_PIXEL_NV: Self = Self(5);
    pub const _1_INVOCATION_PER_2X1_PIXELS_NV: Self = Self(6);
    pub const _1_INVOCATION_PER_1X2_PIXELS_NV: Self = Self(7);
    pub const _1_INVOCATION_PER_2X2_PIXELS_NV: Self = Self(8);
    pub const _1_INVOCATION_PER_4X2_PIXELS_NV: Self = Self(9);
    pub const _1_INVOCATION_PER_2X4_PIXELS_NV: Self = Self(10);
    pub const _1_INVOCATION_PER_4X4_PIXELS_NV: Self = Self(11);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoarseSampleOrderTypeNV.html)) · Enum <br/> VkCoarseSampleOrderTypeNV - Shading rate image sample ordering types\n[](#_c_specification)C Specification\n----------\n\nThe type [`crate::vk::CoarseSampleOrderTypeNV`] specifies the technique used to\norder coverage samples in fragments larger than one pixel, and is defined\nas:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef enum VkCoarseSampleOrderTypeNV {\n    VK_COARSE_SAMPLE_ORDER_TYPE_DEFAULT_NV = 0,\n    VK_COARSE_SAMPLE_ORDER_TYPE_CUSTOM_NV = 1,\n    VK_COARSE_SAMPLE_ORDER_TYPE_PIXEL_MAJOR_NV = 2,\n    VK_COARSE_SAMPLE_ORDER_TYPE_SAMPLE_MAJOR_NV = 3,\n} VkCoarseSampleOrderTypeNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::DEFAULT_NV`] specifies that coverage\n  samples will be ordered in an implementation-dependent manner.\n\n* [`Self::CUSTOM_NV`] specifies that coverage\n  samples will be ordered according to the array of custom orderings\n  provided in either the `pCustomSampleOrders` member of[`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`] or the`pCustomSampleOrders` member of [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`].\n\n* [`Self::PIXEL_MAJOR_NV`] specifies that coverage\n  samples will be ordered sequentially, sorted first by pixel coordinate\n  (in row-major order) and then by[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask).\n\n* [`Self::SAMPLE_MAJOR_NV`] specifies that\n  coverage samples will be ordered sequentially, sorted first by[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) and then by\n  pixel coordinate (in row-major order).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`], [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`]\n"]
#[doc(alias = "VkCoarseSampleOrderTypeNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct CoarseSampleOrderTypeNV(pub i32);
impl std::fmt::Debug for CoarseSampleOrderTypeNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEFAULT_NV => "DEFAULT_NV",
            &Self::CUSTOM_NV => "CUSTOM_NV",
            &Self::PIXEL_MAJOR_NV => "PIXEL_MAJOR_NV",
            &Self::SAMPLE_MAJOR_NV => "SAMPLE_MAJOR_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::extensions::nv_shading_rate_image::CoarseSampleOrderTypeNV {
    pub const DEFAULT_NV: Self = Self(0);
    pub const CUSTOM_NV: Self = Self(1);
    pub const PIXEL_MAJOR_NV: Self = Self(2);
    pub const SAMPLE_MAJOR_NV: Self = Self(3);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindShadingRateImageNV.html)) · Function <br/> vkCmdBindShadingRateImageNV - Bind a shading rate image on a command buffer\n[](#_c_specification)C Specification\n----------\n\nWhen shading rate image usage is enabled in the bound pipeline, the pipeline\nuses a shading rate image specified by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdBindShadingRateImageNV(\n    VkCommandBuffer                             commandBuffer,\n    VkImageView                                 imageView,\n    VkImageLayout                               imageLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::image_view`] is an image view handle specifying the shading rate\n  image.[`Self::image_view`] **may** be set to [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), which is equivalent\n  to specifying a view of an image filled with zero values.\n\n* [`Self::image_layout`] is the layout that the image subresources accessible\n  from [`Self::image_view`] will be in when the shading rate image is accessed.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindShadingRateImageNV-None-02058  \n   The [shading rate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-shadingRateImage) feature **must** be\n  enabled\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02059  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** be a valid[`crate::vk::ImageView`] handle of type [`crate::vk::ImageViewType::_2D`] or[`crate::vk::ImageViewType::_2D_ARRAY`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02060  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have a format\n  of [`crate::vk::Format::R8_UINT`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02061  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have been\n  created with a `usage` value including[`crate::vk::ImageUsageFlagBits::SHADING_RATE_IMAGE_NV`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02062  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**match the actual [`crate::vk::ImageLayout`] of each subresource accessible from[`Self::image_view`] at the time the subresource is accessed\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageLayout-02063  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**be [`crate::vk::ImageLayout::SHADING_RATE_OPTIMAL_NV`] or[`crate::vk::ImageLayout::GENERAL`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-parameter  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageLayout-parameter  \n  [`Self::image_layout`] **must** be a valid [`crate::vk::ImageLayout`] value\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdBindShadingRateImageNV-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::image_view`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ImageLayout`], [`crate::vk::ImageView`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBindShadingRateImageNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, image_view: crate::vk1_0::ImageView, image_layout: crate::vk1_0::ImageLayout) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetViewportShadingRatePaletteNV.html)) · Function <br/> vkCmdSetViewportShadingRatePaletteNV - Set shading rate image palettes on a command buffer\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`] enabled, the\nper-viewport shading rate image palettes are set by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdSetViewportShadingRatePaletteNV(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstViewport,\n    uint32_t                                    viewportCount,\n    const VkShadingRatePaletteNV*               pShadingRatePalettes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_viewport`] is the index of the first viewport whose shading\n  rate palette is updated by the command.\n\n* [`Self::viewport_count`] is the number of viewports whose shading rate\n  palettes are updated by the command.\n\n* [`Self::p_shading_rate_palettes`] is a pointer to an array of[`crate::vk::ShadingRatePaletteNV`] structures defining the palette for each\n  viewport.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-None-02064  \n   The [shading rate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-shadingRateImage) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-firstViewport-02067  \n   The sum of [`Self::first_viewport`] and [`Self::viewport_count`] **must** be between`1` and [`crate::vk::PhysicalDeviceLimits`]::`maxViewports`, inclusive\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-firstViewport-02068  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::first_viewport`] **must** be `0`\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-viewportCount-02069  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::viewport_count`] **must** be `1`\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-pShadingRatePalettes-parameter  \n  [`Self::p_shading_rate_palettes`] **must** be a valid pointer to an array of [`Self::viewport_count`] valid [`crate::vk::ShadingRatePaletteNV`] structures\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-viewportCount-arraylength  \n  [`Self::viewport_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ShadingRatePaletteNV`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetViewportShadingRatePaletteNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, first_viewport: u32, viewport_count: u32, p_shading_rate_palettes: *const crate::extensions::nv_shading_rate_image::ShadingRatePaletteNV) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetCoarseSampleOrderNV.html)) · Function <br/> vkCmdSetCoarseSampleOrderNV - Set sample order for coarse fragments on a command buffer\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::VIEWPORT_COARSE_SAMPLE_ORDER_NV`] enabled, the order of\ncoverage samples in fragments larger than one pixel is set by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdSetCoarseSampleOrderNV(\n    VkCommandBuffer                             commandBuffer,\n    VkCoarseSampleOrderTypeNV                   sampleOrderType,\n    uint32_t                                    customSampleOrderCount,\n    const VkCoarseSampleOrderCustomNV*          pCustomSampleOrders);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::sample_order_type`] specifies the mechanism used to order coverage\n  samples in fragments larger than one pixel.\n\n* [`Self::custom_sample_order_count`] specifies the number of custom sample\n  orderings to use when ordering coverage samples.\n\n* [`Self::p_custom_sample_orders`] is a pointer to an array of[`crate::vk::CoarseSampleOrderCustomNV`] structures, each of which specifies\n  the coverage sample order for a single combination of fragment area and\n  coverage sample count.\n[](#_description)Description\n----------\n\nIf [`Self::sample_order_type`] is [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`], the\ncoverage sample order used for any combination of fragment area and coverage\nsample count not enumerated in [`Self::p_custom_sample_orders`] will be identical\nto that used for [`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nValid Usage\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-sampleOrderType-02081  \n   If [`Self::sample_order_type`] is not[`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`],`customSamplerOrderCount` **must** be `0`\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-pCustomSampleOrders-02235  \n   The array [`Self::p_custom_sample_orders`] **must** not contain two structures\n  with matching values for both the `shadingRate` and`sampleCount` members\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-sampleOrderType-parameter  \n  [`Self::sample_order_type`] **must** be a valid [`crate::vk::CoarseSampleOrderTypeNV`] value\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-pCustomSampleOrders-parameter  \n   If [`Self::custom_sample_order_count`] is not `0`, [`Self::p_custom_sample_orders`] **must** be a valid pointer to an array of [`Self::custom_sample_order_count`] valid [`crate::vk::CoarseSampleOrderCustomNV`] structures\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`], [`crate::vk::CoarseSampleOrderTypeNV`], [`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetCoarseSampleOrderNV = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, sample_order_type: crate::extensions::nv_shading_rate_image::CoarseSampleOrderTypeNV, custom_sample_order_count: u32, p_custom_sample_orders: *const crate::extensions::nv_shading_rate_image::CoarseSampleOrderCustomNV) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShadingRateImageFeaturesNV> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceShadingRateImageFeaturesNVBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportShadingRateImageStateCreateInfoNV> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportCoarseSampleOrderStateCreateInfoNV> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'_>> for crate::vk1_0::PipelineViewportStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShadingRateImageFeaturesNV> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShadingRateImageFeaturesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShadingRateImagePropertiesNV> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceShadingRateImagePropertiesNVBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShadingRatePaletteNV.html)) · Structure <br/> VkShadingRatePaletteNV - Structure specifying a single shading rate palette\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShadingRatePaletteNV`] structure specifies to contents of a single\nshading rate image palette and is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkShadingRatePaletteNV {\n    uint32_t                              shadingRatePaletteEntryCount;\n    const VkShadingRatePaletteEntryNV*    pShadingRatePaletteEntries;\n} VkShadingRatePaletteNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::shading_rate_palette_entry_count`] specifies the number of entries in\n  the shading rate image palette.\n\n* [`Self::p_shading_rate_palette_entries`] is a pointer to an array of[`crate::vk::ShadingRatePaletteEntryNV`] enums defining the shading rate for\n  each palette entry.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkShadingRatePaletteNV-shadingRatePaletteEntryCount-02071  \n  [`Self::shading_rate_palette_entry_count`] **must** be between `1` and[`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`]::`shadingRatePaletteSize`,\n  inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkShadingRatePaletteNV-pShadingRatePaletteEntries-parameter  \n  [`Self::p_shading_rate_palette_entries`] **must** be a valid pointer to an array of [`Self::shading_rate_palette_entry_count`] valid [`crate::vk::ShadingRatePaletteEntryNV`] values\n\n* []() VUID-VkShadingRatePaletteNV-shadingRatePaletteEntryCount-arraylength  \n  [`Self::shading_rate_palette_entry_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`], [`crate::vk::ShadingRatePaletteEntryNV`], [`crate::vk::DeviceLoader::cmd_set_viewport_shading_rate_palette_nv`]\n"]
#[doc(alias = "VkShadingRatePaletteNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ShadingRatePaletteNV {
    pub shading_rate_palette_entry_count: u32,
    pub p_shading_rate_palette_entries: *const crate::extensions::nv_shading_rate_image::ShadingRatePaletteEntryNV,
}
impl Default for ShadingRatePaletteNV {
    fn default() -> Self {
        Self { shading_rate_palette_entry_count: Default::default(), p_shading_rate_palette_entries: std::ptr::null() }
    }
}
impl std::fmt::Debug for ShadingRatePaletteNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ShadingRatePaletteNV").field("shading_rate_palette_entry_count", &self.shading_rate_palette_entry_count).field("p_shading_rate_palette_entries", &self.p_shading_rate_palette_entries).finish()
    }
}
impl ShadingRatePaletteNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ShadingRatePaletteNVBuilder<'a> {
        ShadingRatePaletteNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShadingRatePaletteNV.html)) · Builder of [`ShadingRatePaletteNV`] <br/> VkShadingRatePaletteNV - Structure specifying a single shading rate palette\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ShadingRatePaletteNV`] structure specifies to contents of a single\nshading rate image palette and is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkShadingRatePaletteNV {\n    uint32_t                              shadingRatePaletteEntryCount;\n    const VkShadingRatePaletteEntryNV*    pShadingRatePaletteEntries;\n} VkShadingRatePaletteNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::shading_rate_palette_entry_count`] specifies the number of entries in\n  the shading rate image palette.\n\n* [`Self::p_shading_rate_palette_entries`] is a pointer to an array of[`crate::vk::ShadingRatePaletteEntryNV`] enums defining the shading rate for\n  each palette entry.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkShadingRatePaletteNV-shadingRatePaletteEntryCount-02071  \n  [`Self::shading_rate_palette_entry_count`] **must** be between `1` and[`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`]::`shadingRatePaletteSize`,\n  inclusive\n\nValid Usage (Implicit)\n\n* []() VUID-VkShadingRatePaletteNV-pShadingRatePaletteEntries-parameter  \n  [`Self::p_shading_rate_palette_entries`] **must** be a valid pointer to an array of [`Self::shading_rate_palette_entry_count`] valid [`crate::vk::ShadingRatePaletteEntryNV`] values\n\n* []() VUID-VkShadingRatePaletteNV-shadingRatePaletteEntryCount-arraylength  \n  [`Self::shading_rate_palette_entry_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`], [`crate::vk::ShadingRatePaletteEntryNV`], [`crate::vk::DeviceLoader::cmd_set_viewport_shading_rate_palette_nv`]\n"]
#[repr(transparent)]
pub struct ShadingRatePaletteNVBuilder<'a>(ShadingRatePaletteNV, std::marker::PhantomData<&'a ()>);
impl<'a> ShadingRatePaletteNVBuilder<'a> {
    #[inline]
    pub fn new() -> ShadingRatePaletteNVBuilder<'a> {
        ShadingRatePaletteNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate_palette_entries(mut self, shading_rate_palette_entries: &'a [crate::extensions::nv_shading_rate_image::ShadingRatePaletteEntryNV]) -> Self {
        self.0.p_shading_rate_palette_entries = shading_rate_palette_entries.as_ptr() as _;
        self.0.shading_rate_palette_entry_count = shading_rate_palette_entries.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ShadingRatePaletteNV {
        self.0
    }
}
impl<'a> std::default::Default for ShadingRatePaletteNVBuilder<'a> {
    fn default() -> ShadingRatePaletteNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ShadingRatePaletteNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ShadingRatePaletteNVBuilder<'a> {
    type Target = ShadingRatePaletteNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ShadingRatePaletteNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportShadingRateImageStateCreateInfoNV.html)) · Structure <br/> VkPipelineViewportShadingRateImageStateCreateInfoNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineViewportStateCreateInfo`] includes\na [`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the shading rate.\n\nThe [`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPipelineViewportShadingRateImageStateCreateInfoNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkBool32                         shadingRateImageEnable;\n    uint32_t                         viewportCount;\n    const VkShadingRatePaletteNV*    pShadingRatePalettes;\n} VkPipelineViewportShadingRateImageStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::shading_rate_image_enable`] specifies whether shading rate image and\n  palettes are used during rasterization.\n\n* [`Self::viewport_count`] specifies the number of per-viewport palettes used\n  to translate values stored in shading rate images.\n\n* [`Self::p_shading_rate_palettes`] is a pointer to an array of[`crate::vk::ShadingRatePaletteNV`] structures defining the palette for each\n  viewport.\n  If the shading rate palette state is dynamic, this member is ignored.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::shading_rate_image_enable`] is considered\nto be [`crate::vk::FALSE`], and the shading rate image and palettes are not used.\n\nValid Usage\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-viewportCount-02054  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::viewport_count`] **must** be `0` or `1`\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-viewportCount-02055  \n  [`Self::viewport_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-shadingRateImageEnable-02056  \n   If [`Self::shading_rate_image_enable`] is [`crate::vk::TRUE`], [`Self::viewport_count`]**must** be greater or equal to the [`Self::viewport_count`] member of[`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::ShadingRatePaletteNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineViewportShadingRateImageStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineViewportShadingRateImageStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub shading_rate_image_enable: crate::vk1_0::Bool32,
    pub viewport_count: u32,
    pub p_shading_rate_palettes: *const crate::extensions::nv_shading_rate_image::ShadingRatePaletteNV,
}
impl PipelineViewportShadingRateImageStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV;
}
impl Default for PipelineViewportShadingRateImageStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), shading_rate_image_enable: Default::default(), viewport_count: Default::default(), p_shading_rate_palettes: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineViewportShadingRateImageStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineViewportShadingRateImageStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shading_rate_image_enable", &(self.shading_rate_image_enable != 0)).field("viewport_count", &self.viewport_count).field("p_shading_rate_palettes", &self.p_shading_rate_palettes).finish()
    }
}
impl PipelineViewportShadingRateImageStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
        PipelineViewportShadingRateImageStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportShadingRateImageStateCreateInfoNV.html)) · Builder of [`PipelineViewportShadingRateImageStateCreateInfoNV`] <br/> VkPipelineViewportShadingRateImageStateCreateInfoNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineViewportStateCreateInfo`] includes\na [`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the shading rate.\n\nThe [`crate::vk::PipelineViewportShadingRateImageStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPipelineViewportShadingRateImageStateCreateInfoNV {\n    VkStructureType                  sType;\n    const void*                      pNext;\n    VkBool32                         shadingRateImageEnable;\n    uint32_t                         viewportCount;\n    const VkShadingRatePaletteNV*    pShadingRatePalettes;\n} VkPipelineViewportShadingRateImageStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::shading_rate_image_enable`] specifies whether shading rate image and\n  palettes are used during rasterization.\n\n* [`Self::viewport_count`] specifies the number of per-viewport palettes used\n  to translate values stored in shading rate images.\n\n* [`Self::p_shading_rate_palettes`] is a pointer to an array of[`crate::vk::ShadingRatePaletteNV`] structures defining the palette for each\n  viewport.\n  If the shading rate palette state is dynamic, this member is ignored.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::shading_rate_image_enable`] is considered\nto be [`crate::vk::FALSE`], and the shading rate image and palettes are not used.\n\nValid Usage\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-viewportCount-02054  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::viewport_count`] **must** be `0` or `1`\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-viewportCount-02055  \n  [`Self::viewport_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceLimits`]::`maxViewports`\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-shadingRateImageEnable-02056  \n   If [`Self::shading_rate_image_enable`] is [`crate::vk::TRUE`], [`Self::viewport_count`]**must** be greater or equal to the [`Self::viewport_count`] member of[`crate::vk::PipelineViewportStateCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportShadingRateImageStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::ShadingRatePaletteNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a>(PipelineViewportShadingRateImageStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
        PipelineViewportShadingRateImageStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate_image_enable(mut self, shading_rate_image_enable: bool) -> Self {
        self.0.shading_rate_image_enable = shading_rate_image_enable as _;
        self
    }
    #[inline]
    pub fn shading_rate_palettes(mut self, shading_rate_palettes: &'a [crate::extensions::nv_shading_rate_image::ShadingRatePaletteNVBuilder]) -> Self {
        self.0.p_shading_rate_palettes = shading_rate_palettes.as_ptr() as _;
        self.0.viewport_count = shading_rate_palettes.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineViewportShadingRateImageStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
    type Target = PipelineViewportShadingRateImageStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineViewportShadingRateImageStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShadingRateImageFeaturesNV.html)) · Structure <br/> VkPhysicalDeviceShadingRateImageFeaturesNV - Structure describing shading rate image features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPhysicalDeviceShadingRateImageFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shadingRateImage;\n    VkBool32           shadingRateCoarseSampleOrder;\n} VkPhysicalDeviceShadingRateImageFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shading_rate_image`] indicates that the\n  implementation supports the use of a shading rate image to derive an\n  effective shading rate for fragment processing.\n  It also indicates that the implementation supports the`ShadingRateNV` SPIR-V execution mode.\n\n* []()[`Self::shading_rate_coarse_sample_order`] indicates that the implementation\n  supports a user-configurable ordering of coverage samples in fragments\n  larger than one pixel.\n\nSee [Shading Rate Image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-shading-rate-image) for more\ninformation.\n\nIf the [`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShadingRateImageFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShadingRateImageFeaturesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShadingRateImageFeaturesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shading_rate_image: crate::vk1_0::Bool32,
    pub shading_rate_coarse_sample_order: crate::vk1_0::Bool32,
}
impl PhysicalDeviceShadingRateImageFeaturesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV;
}
impl Default for PhysicalDeviceShadingRateImageFeaturesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shading_rate_image: Default::default(), shading_rate_coarse_sample_order: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShadingRateImageFeaturesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShadingRateImageFeaturesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shading_rate_image", &(self.shading_rate_image != 0)).field("shading_rate_coarse_sample_order", &(self.shading_rate_coarse_sample_order != 0)).finish()
    }
}
impl PhysicalDeviceShadingRateImageFeaturesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
        PhysicalDeviceShadingRateImageFeaturesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShadingRateImageFeaturesNV.html)) · Builder of [`PhysicalDeviceShadingRateImageFeaturesNV`] <br/> VkPhysicalDeviceShadingRateImageFeaturesNV - Structure describing shading rate image features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPhysicalDeviceShadingRateImageFeaturesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shadingRateImage;\n    VkBool32           shadingRateCoarseSampleOrder;\n} VkPhysicalDeviceShadingRateImageFeaturesNV;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shading_rate_image`] indicates that the\n  implementation supports the use of a shading rate image to derive an\n  effective shading rate for fragment processing.\n  It also indicates that the implementation supports the`ShadingRateNV` SPIR-V execution mode.\n\n* []()[`Self::shading_rate_coarse_sample_order`] indicates that the implementation\n  supports a user-configurable ordering of coverage samples in fragments\n  larger than one pixel.\n\nSee [Shading Rate Image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-shading-rate-image) for more\ninformation.\n\nIf the [`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceShadingRateImageFeaturesNV`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShadingRateImageFeaturesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a>(PhysicalDeviceShadingRateImageFeaturesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
        PhysicalDeviceShadingRateImageFeaturesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate_image(mut self, shading_rate_image: bool) -> Self {
        self.0.shading_rate_image = shading_rate_image as _;
        self
    }
    #[inline]
    pub fn shading_rate_coarse_sample_order(mut self, shading_rate_coarse_sample_order: bool) -> Self {
        self.0.shading_rate_coarse_sample_order = shading_rate_coarse_sample_order as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShadingRateImageFeaturesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
    fn default() -> PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
    type Target = PhysicalDeviceShadingRateImageFeaturesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShadingRateImageFeaturesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShadingRateImagePropertiesNV.html)) · Structure <br/> VkPhysicalDeviceShadingRateImagePropertiesNV - Structure describing shading rate image limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPhysicalDeviceShadingRateImagePropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkExtent2D         shadingRateTexelSize;\n    uint32_t           shadingRatePaletteSize;\n    uint32_t           shadingRateMaxCoarseSamples;\n} VkPhysicalDeviceShadingRateImagePropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shading_rate_texel_size`] indicates\n  the width and height of the portion of the framebuffer corresponding to\n  each texel in the shading rate image.\n\n* []() [`Self::shading_rate_palette_size`]indicates the maximum number of palette entries supported for the\n  shading rate image.\n\n* []()[`Self::shading_rate_max_coarse_samples`] specifies the maximum number of\n  coverage samples supported in a single fragment.\n  If the product of the fragment size derived from the base shading rate\n  and the number of coverage samples per pixel exceeds this limit, the\n  final shading rate will be adjusted so that its product does not exceed\n  the limit.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nThese properties are related to the [shading\nrate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-shading-rate-image) feature.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShadingRateImagePropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceShadingRateImagePropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceShadingRateImagePropertiesNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shading_rate_texel_size: crate::vk1_0::Extent2D,
    pub shading_rate_palette_size: u32,
    pub shading_rate_max_coarse_samples: u32,
}
impl PhysicalDeviceShadingRateImagePropertiesNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV;
}
impl Default for PhysicalDeviceShadingRateImagePropertiesNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shading_rate_texel_size: Default::default(), shading_rate_palette_size: Default::default(), shading_rate_max_coarse_samples: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceShadingRateImagePropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceShadingRateImagePropertiesNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shading_rate_texel_size", &self.shading_rate_texel_size).field("shading_rate_palette_size", &self.shading_rate_palette_size).field("shading_rate_max_coarse_samples", &self.shading_rate_max_coarse_samples).finish()
    }
}
impl PhysicalDeviceShadingRateImagePropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
        PhysicalDeviceShadingRateImagePropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceShadingRateImagePropertiesNV.html)) · Builder of [`PhysicalDeviceShadingRateImagePropertiesNV`] <br/> VkPhysicalDeviceShadingRateImagePropertiesNV - Structure describing shading rate image limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`] structure is defined\nas:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPhysicalDeviceShadingRateImagePropertiesNV {\n    VkStructureType    sType;\n    void*              pNext;\n    VkExtent2D         shadingRateTexelSize;\n    uint32_t           shadingRatePaletteSize;\n    uint32_t           shadingRateMaxCoarseSamples;\n} VkPhysicalDeviceShadingRateImagePropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::shading_rate_texel_size`] indicates\n  the width and height of the portion of the framebuffer corresponding to\n  each texel in the shading rate image.\n\n* []() [`Self::shading_rate_palette_size`]indicates the maximum number of palette entries supported for the\n  shading rate image.\n\n* []()[`Self::shading_rate_max_coarse_samples`] specifies the maximum number of\n  coverage samples supported in a single fragment.\n  If the product of the fragment size derived from the base shading rate\n  and the number of coverage samples per pixel exceeds this limit, the\n  final shading rate will be adjusted so that its product does not exceed\n  the limit.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nThese properties are related to the [shading\nrate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-shading-rate-image) feature.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceShadingRateImagePropertiesNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Extent2D`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a>(PhysicalDeviceShadingRateImagePropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
        PhysicalDeviceShadingRateImagePropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate_texel_size(mut self, shading_rate_texel_size: crate::vk1_0::Extent2D) -> Self {
        self.0.shading_rate_texel_size = shading_rate_texel_size as _;
        self
    }
    #[inline]
    pub fn shading_rate_palette_size(mut self, shading_rate_palette_size: u32) -> Self {
        self.0.shading_rate_palette_size = shading_rate_palette_size as _;
        self
    }
    #[inline]
    pub fn shading_rate_max_coarse_samples(mut self, shading_rate_max_coarse_samples: u32) -> Self {
        self.0.shading_rate_max_coarse_samples = shading_rate_max_coarse_samples as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceShadingRateImagePropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
    fn default() -> PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
    type Target = PhysicalDeviceShadingRateImagePropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceShadingRateImagePropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoarseSampleLocationNV.html)) · Structure <br/> VkCoarseSampleLocationNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CoarseSampleLocationNV`] structure identifies a specific pixel and[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) for one of the\ncoverage samples in a fragment that is larger than one pixel.\nThis structure is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkCoarseSampleLocationNV {\n    uint32_t    pixelX;\n    uint32_t    pixelY;\n    uint32_t    sample;\n} VkCoarseSampleLocationNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::pixel_x`] is added to the x coordinate of the upper-leftmost pixel of\n  each fragment to identify the pixel containing the coverage sample.\n\n* [`Self::pixel_y`] is added to the y coordinate of the upper-leftmost pixel of\n  each fragment to identify the pixel containing the coverage sample.\n\n* [`Self::sample`] is the number of the coverage sample in the pixel\n  identified by [`Self::pixel_x`] and [`Self::pixel_y`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkCoarseSampleLocationNV-pixelX-02078  \n  [`Self::pixel_x`] **must** be less than the width (in pixels) of the fragment\n\n* []() VUID-VkCoarseSampleLocationNV-pixelY-02079  \n  [`Self::pixel_y`] **must** be less than the height (in pixels) of the fragment\n\n* []() VUID-VkCoarseSampleLocationNV-sample-02080  \n  [`Self::sample`] **must** be less than the number of coverage samples in each\n  pixel belonging to the fragment\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`]\n"]
#[doc(alias = "VkCoarseSampleLocationNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CoarseSampleLocationNV {
    pub pixel_x: u32,
    pub pixel_y: u32,
    pub sample: u32,
}
impl Default for CoarseSampleLocationNV {
    fn default() -> Self {
        Self { pixel_x: Default::default(), pixel_y: Default::default(), sample: Default::default() }
    }
}
impl std::fmt::Debug for CoarseSampleLocationNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CoarseSampleLocationNV").field("pixel_x", &self.pixel_x).field("pixel_y", &self.pixel_y).field("sample", &self.sample).finish()
    }
}
impl CoarseSampleLocationNV {
    #[inline]
    pub fn into_builder<'a>(self) -> CoarseSampleLocationNVBuilder<'a> {
        CoarseSampleLocationNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoarseSampleLocationNV.html)) · Builder of [`CoarseSampleLocationNV`] <br/> VkCoarseSampleLocationNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CoarseSampleLocationNV`] structure identifies a specific pixel and[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) for one of the\ncoverage samples in a fragment that is larger than one pixel.\nThis structure is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkCoarseSampleLocationNV {\n    uint32_t    pixelX;\n    uint32_t    pixelY;\n    uint32_t    sample;\n} VkCoarseSampleLocationNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::pixel_x`] is added to the x coordinate of the upper-leftmost pixel of\n  each fragment to identify the pixel containing the coverage sample.\n\n* [`Self::pixel_y`] is added to the y coordinate of the upper-leftmost pixel of\n  each fragment to identify the pixel containing the coverage sample.\n\n* [`Self::sample`] is the number of the coverage sample in the pixel\n  identified by [`Self::pixel_x`] and [`Self::pixel_y`].\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkCoarseSampleLocationNV-pixelX-02078  \n  [`Self::pixel_x`] **must** be less than the width (in pixels) of the fragment\n\n* []() VUID-VkCoarseSampleLocationNV-pixelY-02079  \n  [`Self::pixel_y`] **must** be less than the height (in pixels) of the fragment\n\n* []() VUID-VkCoarseSampleLocationNV-sample-02080  \n  [`Self::sample`] **must** be less than the number of coverage samples in each\n  pixel belonging to the fragment\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`]\n"]
#[repr(transparent)]
pub struct CoarseSampleLocationNVBuilder<'a>(CoarseSampleLocationNV, std::marker::PhantomData<&'a ()>);
impl<'a> CoarseSampleLocationNVBuilder<'a> {
    #[inline]
    pub fn new() -> CoarseSampleLocationNVBuilder<'a> {
        CoarseSampleLocationNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn pixel_x(mut self, pixel_x: u32) -> Self {
        self.0.pixel_x = pixel_x as _;
        self
    }
    #[inline]
    pub fn pixel_y(mut self, pixel_y: u32) -> Self {
        self.0.pixel_y = pixel_y as _;
        self
    }
    #[inline]
    pub fn sample(mut self, sample: u32) -> Self {
        self.0.sample = sample as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CoarseSampleLocationNV {
        self.0
    }
}
impl<'a> std::default::Default for CoarseSampleLocationNVBuilder<'a> {
    fn default() -> CoarseSampleLocationNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CoarseSampleLocationNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CoarseSampleLocationNVBuilder<'a> {
    type Target = CoarseSampleLocationNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CoarseSampleLocationNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoarseSampleOrderCustomNV.html)) · Structure <br/> VkCoarseSampleOrderCustomNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CoarseSampleOrderCustomNV`] structure is used with a coverage\nsample ordering type of [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`] to\nspecify the order of coverage samples for one combination of fragment width,\nfragment height, and coverage sample count.\nThe structure is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkCoarseSampleOrderCustomNV {\n    VkShadingRatePaletteEntryNV        shadingRate;\n    uint32_t                           sampleCount;\n    uint32_t                           sampleLocationCount;\n    const VkCoarseSampleLocationNV*    pSampleLocations;\n} VkCoarseSampleOrderCustomNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::shading_rate`] is a shading rate palette entry that identifies the\n  fragment width and height for the combination of fragment area and\n  per-pixel coverage sample count to control.\n\n* [`Self::sample_count`] identifies the per-pixel coverage sample count for the\n  combination of fragment area and coverage sample count to control.\n\n* [`Self::sample_location_count`] specifies the number of sample locations in\n  the custom ordering.\n\n* [`Self::p_sample_locations`] is a pointer to an array of[`crate::vk::CoarseSampleLocationNV`] structures specifying the location of\n  each sample in the custom ordering.\n[](#_description)Description\n----------\n\nWhen using a custom sample ordering, element *j* in [`Self::p_sample_locations`]specifies a specific pixel location and[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) that corresponds to[coverage index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) *j* in the\nmulti-pixel fragment.\n\nValid Usage\n\n* []() VUID-VkCoarseSampleOrderCustomNV-shadingRate-02073  \n  [`Self::shading_rate`] **must** be a shading rate that generates fragments with\n  more than one pixel\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleCount-02074  \n  [`Self::sample_count`] **must** correspond to a sample count enumerated in[`crate::vk::SampleCountFlagBits`] whose corresponding bit is set in[`crate::vk::PhysicalDeviceLimits::framebuffer_no_attachments_sample_counts`]\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-02075  \n  [`Self::sample_location_count`] **must** be equal to the product of[`Self::sample_count`], the fragment width for [`Self::shading_rate`], and the\n  fragment height for [`Self::shading_rate`]\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-02076  \n  [`Self::sample_location_count`] **must** be less than or equal to the value of[`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`]::`shadingRateMaxCoarseSamples`\n\n* []() VUID-VkCoarseSampleOrderCustomNV-pSampleLocations-02077  \n   The array [`Self::p_sample_locations`] **must** contain exactly one entry for\n  every combination of valid values for `pixelX`, `pixelY`, and`sample` in the structure [`crate::vk::CoarseSampleOrderCustomNV`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCoarseSampleOrderCustomNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::ShadingRatePaletteEntryNV`] value\n\n* []() VUID-VkCoarseSampleOrderCustomNV-pSampleLocations-parameter  \n  [`Self::p_sample_locations`] **must** be a valid pointer to an array of [`Self::sample_location_count`] [`crate::vk::CoarseSampleLocationNV`] structures\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-arraylength  \n  [`Self::sample_location_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleLocationNV`], [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`], [`crate::vk::ShadingRatePaletteEntryNV`], [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`]\n"]
#[doc(alias = "VkCoarseSampleOrderCustomNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CoarseSampleOrderCustomNV {
    pub shading_rate: crate::extensions::nv_shading_rate_image::ShadingRatePaletteEntryNV,
    pub sample_count: u32,
    pub sample_location_count: u32,
    pub p_sample_locations: *const crate::extensions::nv_shading_rate_image::CoarseSampleLocationNV,
}
impl Default for CoarseSampleOrderCustomNV {
    fn default() -> Self {
        Self { shading_rate: Default::default(), sample_count: Default::default(), sample_location_count: Default::default(), p_sample_locations: std::ptr::null() }
    }
}
impl std::fmt::Debug for CoarseSampleOrderCustomNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CoarseSampleOrderCustomNV").field("shading_rate", &self.shading_rate).field("sample_count", &self.sample_count).field("sample_location_count", &self.sample_location_count).field("p_sample_locations", &self.p_sample_locations).finish()
    }
}
impl CoarseSampleOrderCustomNV {
    #[inline]
    pub fn into_builder<'a>(self) -> CoarseSampleOrderCustomNVBuilder<'a> {
        CoarseSampleOrderCustomNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCoarseSampleOrderCustomNV.html)) · Builder of [`CoarseSampleOrderCustomNV`] <br/> VkCoarseSampleOrderCustomNV - Structure specifying parameters controlling shading rate image usage\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::CoarseSampleOrderCustomNV`] structure is used with a coverage\nsample ordering type of [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`] to\nspecify the order of coverage samples for one combination of fragment width,\nfragment height, and coverage sample count.\nThe structure is defined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkCoarseSampleOrderCustomNV {\n    VkShadingRatePaletteEntryNV        shadingRate;\n    uint32_t                           sampleCount;\n    uint32_t                           sampleLocationCount;\n    const VkCoarseSampleLocationNV*    pSampleLocations;\n} VkCoarseSampleOrderCustomNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::shading_rate`] is a shading rate palette entry that identifies the\n  fragment width and height for the combination of fragment area and\n  per-pixel coverage sample count to control.\n\n* [`Self::sample_count`] identifies the per-pixel coverage sample count for the\n  combination of fragment area and coverage sample count to control.\n\n* [`Self::sample_location_count`] specifies the number of sample locations in\n  the custom ordering.\n\n* [`Self::p_sample_locations`] is a pointer to an array of[`crate::vk::CoarseSampleLocationNV`] structures specifying the location of\n  each sample in the custom ordering.\n[](#_description)Description\n----------\n\nWhen using a custom sample ordering, element *j* in [`Self::p_sample_locations`]specifies a specific pixel location and[sample index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) that corresponds to[coverage index](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#primsrast-multisampling-coverage-mask) *j* in the\nmulti-pixel fragment.\n\nValid Usage\n\n* []() VUID-VkCoarseSampleOrderCustomNV-shadingRate-02073  \n  [`Self::shading_rate`] **must** be a shading rate that generates fragments with\n  more than one pixel\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleCount-02074  \n  [`Self::sample_count`] **must** correspond to a sample count enumerated in[`crate::vk::SampleCountFlagBits`] whose corresponding bit is set in[`crate::vk::PhysicalDeviceLimits::framebuffer_no_attachments_sample_counts`]\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-02075  \n  [`Self::sample_location_count`] **must** be equal to the product of[`Self::sample_count`], the fragment width for [`Self::shading_rate`], and the\n  fragment height for [`Self::shading_rate`]\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-02076  \n  [`Self::sample_location_count`] **must** be less than or equal to the value of[`crate::vk::PhysicalDeviceShadingRateImagePropertiesNV`]::`shadingRateMaxCoarseSamples`\n\n* []() VUID-VkCoarseSampleOrderCustomNV-pSampleLocations-02077  \n   The array [`Self::p_sample_locations`] **must** contain exactly one entry for\n  every combination of valid values for `pixelX`, `pixelY`, and`sample` in the structure [`crate::vk::CoarseSampleOrderCustomNV`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCoarseSampleOrderCustomNV-shadingRate-parameter  \n  [`Self::shading_rate`] **must** be a valid [`crate::vk::ShadingRatePaletteEntryNV`] value\n\n* []() VUID-VkCoarseSampleOrderCustomNV-pSampleLocations-parameter  \n  [`Self::p_sample_locations`] **must** be a valid pointer to an array of [`Self::sample_location_count`] [`crate::vk::CoarseSampleLocationNV`] structures\n\n* []() VUID-VkCoarseSampleOrderCustomNV-sampleLocationCount-arraylength  \n  [`Self::sample_location_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleLocationNV`], [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`], [`crate::vk::ShadingRatePaletteEntryNV`], [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`]\n"]
#[repr(transparent)]
pub struct CoarseSampleOrderCustomNVBuilder<'a>(CoarseSampleOrderCustomNV, std::marker::PhantomData<&'a ()>);
impl<'a> CoarseSampleOrderCustomNVBuilder<'a> {
    #[inline]
    pub fn new() -> CoarseSampleOrderCustomNVBuilder<'a> {
        CoarseSampleOrderCustomNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shading_rate(mut self, shading_rate: crate::extensions::nv_shading_rate_image::ShadingRatePaletteEntryNV) -> Self {
        self.0.shading_rate = shading_rate as _;
        self
    }
    #[inline]
    pub fn sample_count(mut self, sample_count: u32) -> Self {
        self.0.sample_count = sample_count as _;
        self
    }
    #[inline]
    pub fn sample_locations(mut self, sample_locations: &'a [crate::extensions::nv_shading_rate_image::CoarseSampleLocationNVBuilder]) -> Self {
        self.0.p_sample_locations = sample_locations.as_ptr() as _;
        self.0.sample_location_count = sample_locations.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CoarseSampleOrderCustomNV {
        self.0
    }
}
impl<'a> std::default::Default for CoarseSampleOrderCustomNVBuilder<'a> {
    fn default() -> CoarseSampleOrderCustomNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CoarseSampleOrderCustomNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CoarseSampleOrderCustomNVBuilder<'a> {
    type Target = CoarseSampleOrderCustomNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CoarseSampleOrderCustomNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportCoarseSampleOrderStateCreateInfoNV.html)) · Structure <br/> VkPipelineViewportCoarseSampleOrderStateCreateInfoNV - Structure specifying parameters controlling sample order in coarse fragments\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineViewportStateCreateInfo`] includes\na [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the order of coverage\nsamples in fragments larger than one pixel.\n\nThe [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPipelineViewportCoarseSampleOrderStateCreateInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkCoarseSampleOrderTypeNV             sampleOrderType;\n    uint32_t                              customSampleOrderCount;\n    const VkCoarseSampleOrderCustomNV*    pCustomSampleOrders;\n} VkPipelineViewportCoarseSampleOrderStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_order_type`] specifies the mechanism used to order coverage\n  samples in fragments larger than one pixel.\n\n* [`Self::custom_sample_order_count`] specifies the number of custom sample\n  orderings to use when ordering coverage samples.\n\n* [`Self::p_custom_sample_orders`] is a pointer to an array of[`Self::custom_sample_order_count`] [`crate::vk::CoarseSampleOrderCustomNV`]structures, each of which specifies the coverage sample order for a\n  single combination of fragment area and coverage sample count.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::sample_order_type`] is considered to be[`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nIf [`Self::sample_order_type`] is [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`], the\ncoverage sample order used for any combination of fragment area and coverage\nsample count not enumerated in [`Self::p_custom_sample_orders`] will be identical\nto that used for [`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nIf the pipeline was created with[`crate::vk::DynamicState::VIEWPORT_COARSE_SAMPLE_ORDER_NV`], the contents of this\nstructure (if present) are ignored, and the coverage sample order is instead\nspecified by [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`].\n\nValid Usage\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sampleOrderType-02072  \n   If [`Self::sample_order_type`] is not[`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`],`customSamplerOrderCount` **must** be `0`\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-pCustomSampleOrders-02234  \n   The array [`Self::p_custom_sample_orders`] **must** not contain two structures\n  with matching values for both the `shadingRate` and`sampleCount` members\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sampleOrderType-parameter  \n  [`Self::sample_order_type`] **must** be a valid [`crate::vk::CoarseSampleOrderTypeNV`] value\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-pCustomSampleOrders-parameter  \n   If [`Self::custom_sample_order_count`] is not `0`, [`Self::p_custom_sample_orders`] **must** be a valid pointer to an array of [`Self::custom_sample_order_count`] valid [`crate::vk::CoarseSampleOrderCustomNV`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`], [`crate::vk::CoarseSampleOrderTypeNV`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineViewportCoarseSampleOrderStateCreateInfoNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub sample_order_type: crate::extensions::nv_shading_rate_image::CoarseSampleOrderTypeNV,
    pub custom_sample_order_count: u32,
    pub p_custom_sample_orders: *const crate::extensions::nv_shading_rate_image::CoarseSampleOrderCustomNV,
}
impl PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV;
}
impl Default for PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), sample_order_type: Default::default(), custom_sample_order_count: Default::default(), p_custom_sample_orders: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineViewportCoarseSampleOrderStateCreateInfoNV").field("s_type", &self.s_type).field("p_next", &self.p_next).field("sample_order_type", &self.sample_order_type).field("custom_sample_order_count", &self.custom_sample_order_count).field("p_custom_sample_orders", &self.p_custom_sample_orders).finish()
    }
}
impl PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
        PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineViewportCoarseSampleOrderStateCreateInfoNV.html)) · Builder of [`PipelineViewportCoarseSampleOrderStateCreateInfoNV`] <br/> VkPipelineViewportCoarseSampleOrderStateCreateInfoNV - Structure specifying parameters controlling sample order in coarse fragments\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::PipelineViewportStateCreateInfo`] includes\na [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`] structure, then\nthat structure includes parameters that control the order of coverage\nsamples in fragments larger than one pixel.\n\nThe [`crate::vk::PipelineViewportCoarseSampleOrderStateCreateInfoNV`] structure is\ndefined as:\n\n```\n// Provided by VK_NV_shading_rate_image\ntypedef struct VkPipelineViewportCoarseSampleOrderStateCreateInfoNV {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkCoarseSampleOrderTypeNV             sampleOrderType;\n    uint32_t                              customSampleOrderCount;\n    const VkCoarseSampleOrderCustomNV*    pCustomSampleOrders;\n} VkPipelineViewportCoarseSampleOrderStateCreateInfoNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::sample_order_type`] specifies the mechanism used to order coverage\n  samples in fragments larger than one pixel.\n\n* [`Self::custom_sample_order_count`] specifies the number of custom sample\n  orderings to use when ordering coverage samples.\n\n* [`Self::p_custom_sample_orders`] is a pointer to an array of[`Self::custom_sample_order_count`] [`crate::vk::CoarseSampleOrderCustomNV`]structures, each of which specifies the coverage sample order for a\n  single combination of fragment area and coverage sample count.\n[](#_description)Description\n----------\n\nIf this structure is not present, [`Self::sample_order_type`] is considered to be[`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nIf [`Self::sample_order_type`] is [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`], the\ncoverage sample order used for any combination of fragment area and coverage\nsample count not enumerated in [`Self::p_custom_sample_orders`] will be identical\nto that used for [`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nIf the pipeline was created with[`crate::vk::DynamicState::VIEWPORT_COARSE_SAMPLE_ORDER_NV`], the contents of this\nstructure (if present) are ignored, and the coverage sample order is instead\nspecified by [`crate::vk::DeviceLoader::cmd_set_coarse_sample_order_nv`].\n\nValid Usage\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sampleOrderType-02072  \n   If [`Self::sample_order_type`] is not[`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`],`customSamplerOrderCount` **must** be `0`\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-pCustomSampleOrders-02234  \n   The array [`Self::p_custom_sample_orders`] **must** not contain two structures\n  with matching values for both the `shadingRate` and`sampleCount` members\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV`]\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-sampleOrderType-parameter  \n  [`Self::sample_order_type`] **must** be a valid [`crate::vk::CoarseSampleOrderTypeNV`] value\n\n* []() VUID-VkPipelineViewportCoarseSampleOrderStateCreateInfoNV-pCustomSampleOrders-parameter  \n   If [`Self::custom_sample_order_count`] is not `0`, [`Self::p_custom_sample_orders`] **must** be a valid pointer to an array of [`Self::custom_sample_order_count`] valid [`crate::vk::CoarseSampleOrderCustomNV`] structures\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`], [`crate::vk::CoarseSampleOrderTypeNV`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a>(PipelineViewportCoarseSampleOrderStateCreateInfoNV, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
        PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn sample_order_type(mut self, sample_order_type: crate::extensions::nv_shading_rate_image::CoarseSampleOrderTypeNV) -> Self {
        self.0.sample_order_type = sample_order_type as _;
        self
    }
    #[inline]
    pub fn custom_sample_orders(mut self, custom_sample_orders: &'a [crate::extensions::nv_shading_rate_image::CoarseSampleOrderCustomNVBuilder]) -> Self {
        self.0.p_custom_sample_orders = custom_sample_orders.as_ptr() as _;
        self.0.custom_sample_order_count = custom_sample_orders.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineViewportCoarseSampleOrderStateCreateInfoNV {
        self.0
    }
}
impl<'a> std::default::Default for PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
    fn default() -> PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
    type Target = PipelineViewportCoarseSampleOrderStateCreateInfoNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineViewportCoarseSampleOrderStateCreateInfoNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_shading_rate_image`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindShadingRateImageNV.html)) · Function <br/> vkCmdBindShadingRateImageNV - Bind a shading rate image on a command buffer\n[](#_c_specification)C Specification\n----------\n\nWhen shading rate image usage is enabled in the bound pipeline, the pipeline\nuses a shading rate image specified by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdBindShadingRateImageNV(\n    VkCommandBuffer                             commandBuffer,\n    VkImageView                                 imageView,\n    VkImageLayout                               imageLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::image_view`] is an image view handle specifying the shading rate\n  image.[`Self::image_view`] **may** be set to [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), which is equivalent\n  to specifying a view of an image filled with zero values.\n\n* [`Self::image_layout`] is the layout that the image subresources accessible\n  from [`Self::image_view`] will be in when the shading rate image is accessed.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindShadingRateImageNV-None-02058  \n   The [shading rate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-shadingRateImage) feature **must** be\n  enabled\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02059  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** be a valid[`crate::vk::ImageView`] handle of type [`crate::vk::ImageViewType::_2D`] or[`crate::vk::ImageViewType::_2D_ARRAY`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02060  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have a format\n  of [`crate::vk::Format::R8_UINT`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02061  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have been\n  created with a `usage` value including[`crate::vk::ImageUsageFlagBits::SHADING_RATE_IMAGE_NV`]\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-02062  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**match the actual [`crate::vk::ImageLayout`] of each subresource accessible from[`Self::image_view`] at the time the subresource is accessed\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageLayout-02063  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**be [`crate::vk::ImageLayout::SHADING_RATE_OPTIMAL_NV`] or[`crate::vk::ImageLayout::GENERAL`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageView-parameter  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkCmdBindShadingRateImageNV-imageLayout-parameter  \n  [`Self::image_layout`] **must** be a valid [`crate::vk::ImageLayout`] value\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindShadingRateImageNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdBindShadingRateImageNV-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::image_view`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ImageLayout`], [`crate::vk::ImageView`]\n"]
    #[doc(alias = "vkCmdBindShadingRateImageNV")]
    pub unsafe fn cmd_bind_shading_rate_image_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, image_view: Option<crate::vk1_0::ImageView>, image_layout: crate::vk1_0::ImageLayout) -> () {
        let _function = self.cmd_bind_shading_rate_image_nv.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            command_buffer as _,
            match image_view {
                Some(v) => v,
                None => Default::default(),
            },
            image_layout as _,
        );
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetViewportShadingRatePaletteNV.html)) · Function <br/> vkCmdSetViewportShadingRatePaletteNV - Set shading rate image palettes on a command buffer\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::VIEWPORT_SHADING_RATE_PALETTE_NV`] enabled, the\nper-viewport shading rate image palettes are set by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdSetViewportShadingRatePaletteNV(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstViewport,\n    uint32_t                                    viewportCount,\n    const VkShadingRatePaletteNV*               pShadingRatePalettes);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_viewport`] is the index of the first viewport whose shading\n  rate palette is updated by the command.\n\n* [`Self::viewport_count`] is the number of viewports whose shading rate\n  palettes are updated by the command.\n\n* [`Self::p_shading_rate_palettes`] is a pointer to an array of[`crate::vk::ShadingRatePaletteNV`] structures defining the palette for each\n  viewport.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-None-02064  \n   The [shading rate image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-shadingRateImage) feature **must** be\n  enabled\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-firstViewport-02067  \n   The sum of [`Self::first_viewport`] and [`Self::viewport_count`] **must** be between`1` and [`crate::vk::PhysicalDeviceLimits`]::`maxViewports`, inclusive\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-firstViewport-02068  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::first_viewport`] **must** be `0`\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-viewportCount-02069  \n   If the [multiple viewports](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-multiViewport) feature is not\n  enabled, [`Self::viewport_count`] **must** be `1`\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-pShadingRatePalettes-parameter  \n  [`Self::p_shading_rate_palettes`] **must** be a valid pointer to an array of [`Self::viewport_count`] valid [`crate::vk::ShadingRatePaletteNV`] structures\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetViewportShadingRatePaletteNV-viewportCount-arraylength  \n  [`Self::viewport_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ShadingRatePaletteNV`]\n"]
    #[doc(alias = "vkCmdSetViewportShadingRatePaletteNV")]
    pub unsafe fn cmd_set_viewport_shading_rate_palette_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, first_viewport: u32, shading_rate_palettes: &[crate::extensions::nv_shading_rate_image::ShadingRatePaletteNVBuilder]) -> () {
        let _function = self.cmd_set_viewport_shading_rate_palette_nv.expect(crate::NOT_LOADED_MESSAGE);
        let viewport_count = shading_rate_palettes.len();
        let _return = _function(command_buffer as _, first_viewport as _, viewport_count as _, shading_rate_palettes.as_ptr() as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetCoarseSampleOrderNV.html)) · Function <br/> vkCmdSetCoarseSampleOrderNV - Set sample order for coarse fragments on a command buffer\n[](#_c_specification)C Specification\n----------\n\nIf a pipeline state object is created with[`crate::vk::DynamicState::VIEWPORT_COARSE_SAMPLE_ORDER_NV`] enabled, the order of\ncoverage samples in fragments larger than one pixel is set by the command:\n\n```\n// Provided by VK_NV_shading_rate_image\nvoid vkCmdSetCoarseSampleOrderNV(\n    VkCommandBuffer                             commandBuffer,\n    VkCoarseSampleOrderTypeNV                   sampleOrderType,\n    uint32_t                                    customSampleOrderCount,\n    const VkCoarseSampleOrderCustomNV*          pCustomSampleOrders);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::sample_order_type`] specifies the mechanism used to order coverage\n  samples in fragments larger than one pixel.\n\n* [`Self::custom_sample_order_count`] specifies the number of custom sample\n  orderings to use when ordering coverage samples.\n\n* [`Self::p_custom_sample_orders`] is a pointer to an array of[`crate::vk::CoarseSampleOrderCustomNV`] structures, each of which specifies\n  the coverage sample order for a single combination of fragment area and\n  coverage sample count.\n[](#_description)Description\n----------\n\nIf [`Self::sample_order_type`] is [`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`], the\ncoverage sample order used for any combination of fragment area and coverage\nsample count not enumerated in [`Self::p_custom_sample_orders`] will be identical\nto that used for [`crate::vk::CoarseSampleOrderTypeNV::DEFAULT_NV`].\n\nValid Usage\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-sampleOrderType-02081  \n   If [`Self::sample_order_type`] is not[`crate::vk::CoarseSampleOrderTypeNV::CUSTOM_NV`],`customSamplerOrderCount` **must** be `0`\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-pCustomSampleOrders-02235  \n   The array [`Self::p_custom_sample_orders`] **must** not contain two structures\n  with matching values for both the `shadingRate` and`sampleCount` members\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-sampleOrderType-parameter  \n  [`Self::sample_order_type`] **must** be a valid [`crate::vk::CoarseSampleOrderTypeNV`] value\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-pCustomSampleOrders-parameter  \n   If [`Self::custom_sample_order_count`] is not `0`, [`Self::p_custom_sample_orders`] **must** be a valid pointer to an array of [`Self::custom_sample_order_count`] valid [`crate::vk::CoarseSampleOrderCustomNV`] structures\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetCoarseSampleOrderNV-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CoarseSampleOrderCustomNV`], [`crate::vk::CoarseSampleOrderTypeNV`], [`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdSetCoarseSampleOrderNV")]
    pub unsafe fn cmd_set_coarse_sample_order_nv(&self, command_buffer: crate::vk1_0::CommandBuffer, sample_order_type: crate::extensions::nv_shading_rate_image::CoarseSampleOrderTypeNV, custom_sample_orders: &[crate::extensions::nv_shading_rate_image::CoarseSampleOrderCustomNVBuilder]) -> () {
        let _function = self.cmd_set_coarse_sample_order_nv.expect(crate::NOT_LOADED_MESSAGE);
        let custom_sample_order_count = custom_sample_orders.len();
        let _return = _function(command_buffer as _, sample_order_type as _, custom_sample_order_count as _, custom_sample_orders.as_ptr() as _);
        ()
    }
}
