#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_FENCE_WIN32_SPEC_VERSION")]
pub const KHR_EXTERNAL_FENCE_WIN32_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_FENCE_WIN32_EXTENSION_NAME")]
pub const KHR_EXTERNAL_FENCE_WIN32_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_external_fence_win32");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_FENCE_WIN32_HANDLE_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetFenceWin32HandleKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_IMPORT_FENCE_WIN32_HANDLE_KHR: *const std::os::raw::c_char = crate::cstr!("vkImportFenceWin32HandleKHR");
#[doc = "Provided by [`crate::extensions::khr_external_fence_win32`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_FENCE_WIN32_HANDLE_INFO_KHR: Self = Self(1000114000);
    pub const EXPORT_FENCE_WIN32_HANDLE_INFO_KHR: Self = Self(1000114001);
    pub const FENCE_GET_WIN32_HANDLE_INFO_KHR: Self = Self(1000114002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetFenceWin32HandleKHR.html)) · Function <br/> vkGetFenceWin32HandleKHR - Get a Windows HANDLE for a fence\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the state of a fence, call:\n\n```\n// Provided by VK_KHR_external_fence_win32\nVkResult vkGetFenceWin32HandleKHR(\n    VkDevice                                    device,\n    const VkFenceGetWin32HandleInfoKHR*         pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::FenceGetWin32HandleInfoKHR`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the fence\n  state.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_fence_win32_handle_khr`] are owned by the application.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\nExporting a Windows handle from a fence **may** have side effects depending on\nthe transference of the specified handle type, as described in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetFenceWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetFenceWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::FenceGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetFenceWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::FenceGetWin32HandleInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetFenceWin32HandleKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_win32_handle_info: *const crate::extensions::khr_external_fence_win32::FenceGetWin32HandleInfoKHR, p_handle: *mut *mut std::ffi::c_void) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportFenceWin32HandleKHR.html)) · Function <br/> vkImportFenceWin32HandleKHR - Import a fence from a Windows HANDLE\n[](#_c_specification)C Specification\n----------\n\nTo import a fence payload from a Windows handle, call:\n\n```\n// Provided by VK_KHR_external_fence_win32\nVkResult vkImportFenceWin32HandleKHR(\n    VkDevice                                    device,\n    const VkImportFenceWin32HandleInfoKHR*      pImportFenceWin32HandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence.\n\n* [`Self::p_import_fence_win32_handle_info`] is a pointer to a[`crate::vk::ImportFenceWin32HandleInfoKHR`] structure specifying the fence and\n  import parameters.\n[](#_description)Description\n----------\n\nImporting a fence payload from Windows handles does not transfer ownership\nof the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\n\nApplications **can** import the same fence payload into multiple instances of\nVulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportFenceWin32HandleKHR-fence-04448  \n  `fence` **must** not be associated with any queue command that has not\n  yet completed execution on that queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportFenceWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportFenceWin32HandleKHR-pImportFenceWin32HandleInfo-parameter  \n  [`Self::p_import_fence_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportFenceWin32HandleInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportFenceWin32HandleInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkImportFenceWin32HandleKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_import_fence_win32_handle_info: *const crate::extensions::khr_external_fence_win32::ImportFenceWin32HandleInfoKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ExportFenceWin32HandleInfoKHR> for crate::vk1_0::FenceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ExportFenceWin32HandleInfoKHRBuilder<'_>> for crate::vk1_0::FenceCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportFenceWin32HandleInfoKHR.html)) · Structure <br/> VkImportFenceWin32HandleInfoKHR - (None)\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportFenceWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkImportFenceWin32HandleInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkFenceImportFlags                   flags;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n    HANDLE                               handle;\n    LPCWSTR                              name;\n} VkImportFenceWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence into which the state will be imported.\n\n* [`Self::flags`] is a bitmask of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) specifying\n  additional parameters for the fence payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  underlying synchronization primitive to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                    Handle Type                     |Transference|Permanence Supported|\n|----------------------------------------------------|------------|--------------------|\n|  [`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`]  | Reference  |Temporary,Permanent |\n|[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32_KMT`]| Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01457  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportFenceWin32HandleInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fence-handletypes-win32) table\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01459  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`], [`Self::name`] **must**be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01460  \n   If [`Self::handle`] is `NULL`, [`Self::name`] **must** name a valid synchronization\n  primitive of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01461  \n   If [`Self::name`] is `NULL`, [`Self::handle`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handle-01462  \n   If [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handle-01539  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in [external\n  fence handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-name-01540  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in [external\n  fence handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_FENCE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) values\n\nHost Synchronization\n\n* Host access to [`Self::fence`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::FenceImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_fence_win32_handle_khr`]\n"]
#[doc(alias = "VkImportFenceWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportFenceWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub fence: crate::vk1_0::Fence,
    pub flags: crate::vk1_1::FenceImportFlags,
    pub handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits,
    pub handle: *mut std::ffi::c_void,
    pub name: *const u16,
}
impl ImportFenceWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_FENCE_WIN32_HANDLE_INFO_KHR;
}
impl Default for ImportFenceWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), fence: Default::default(), flags: Default::default(), handle_type: Default::default(), handle: std::ptr::null_mut(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ImportFenceWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportFenceWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fence", &self.fence).field("flags", &self.flags).field("handle_type", &self.handle_type).field("handle", &self.handle).field("name", &self.name).finish()
    }
}
impl ImportFenceWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportFenceWin32HandleInfoKHRBuilder<'a> {
        ImportFenceWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportFenceWin32HandleInfoKHR.html)) · Builder of [`ImportFenceWin32HandleInfoKHR`] <br/> VkImportFenceWin32HandleInfoKHR - (None)\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImportFenceWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkImportFenceWin32HandleInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkFenceImportFlags                   flags;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n    HANDLE                               handle;\n    LPCWSTR                              name;\n} VkImportFenceWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence into which the state will be imported.\n\n* [`Self::flags`] is a bitmask of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) specifying\n  additional parameters for the fence payload import operation.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of [`Self::handle`].\n\n* [`Self::handle`] is `NULL` or the external handle to import.\n\n* [`Self::name`] is `NULL` or a null-terminated UTF-16 string naming the\n  underlying synchronization primitive to import.\n[](#_description)Description\n----------\n\nThe handle types supported by [`Self::handle_type`] are:\n\n|                    Handle Type                     |Transference|Permanence Supported|\n|----------------------------------------------------|------------|--------------------|\n|  [`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`]  | Reference  |Temporary,Permanent |\n|[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32_KMT`]| Reference  |Temporary,Permanent |\n\nValid Usage\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01457  \n  [`Self::handle_type`] **must** be a value included in the[Handle Types Supported by[`crate::vk::ImportFenceWin32HandleInfoKHR`]](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fence-handletypes-win32) table\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01459  \n   If [`Self::handle_type`] is not[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`], [`Self::name`] **must**be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01460  \n   If [`Self::handle`] is `NULL`, [`Self::name`] **must** name a valid synchronization\n  primitive of the type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handleType-01461  \n   If [`Self::name`] is `NULL`, [`Self::handle`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handle-01462  \n   If [`Self::handle`] is not `NULL`, [`Self::name`] **must** be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-handle-01539  \n   If [`Self::handle`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in [external\n  fence handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-name-01540  \n   If [`Self::name`] is not `NULL`, it **must** obey any requirements listed for[`Self::handle_type`] in [external\n  fence handle types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-fence-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_FENCE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkImportFenceWin32HandleInfoKHR-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkFenceImportFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceImportFlagBits.html) values\n\nHost Synchronization\n\n* Host access to [`Self::fence`] **must** be externally synchronized\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::FenceImportFlagBits`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::import_fence_win32_handle_khr`]\n"]
#[repr(transparent)]
pub struct ImportFenceWin32HandleInfoKHRBuilder<'a>(ImportFenceWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ImportFenceWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ImportFenceWin32HandleInfoKHRBuilder<'a> {
        ImportFenceWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fence(mut self, fence: crate::vk1_0::Fence) -> Self {
        self.0.fence = fence as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::vk1_1::FenceImportFlags) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn handle(mut self, handle: *mut std::ffi::c_void) -> Self {
        self.0.handle = handle;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportFenceWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ImportFenceWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ImportFenceWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportFenceWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportFenceWin32HandleInfoKHRBuilder<'a> {
    type Target = ImportFenceWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportFenceWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportFenceWin32HandleInfoKHR.html)) · Structure <br/> VkExportFenceWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a fence\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a fence, add a[`crate::vk::ExportFenceWin32HandleInfoKHR`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::FenceCreateInfo`] structure.\nThe [`crate::vk::ExportFenceWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkExportFenceWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportFenceWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  underlying synchronization primitive referenced by NT handles exported\n  from the created fence.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportFenceCreateInfo`] is not inluded in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportFenceCreateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::FenceCreateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportFenceWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights will be\n\n`DXGI_SHARED_RESOURCE_READ` | `DXGI_SHARED_RESOURCE_WRITE`\n\nfor handles of the following types:\n\n[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`]\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-handleTypes-01447  \n   If [`crate::vk::ExportFenceCreateInfo::handle_types`] does not include[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`], a[`crate::vk::ExportFenceWin32HandleInfoKHR`] structure **must** not be included in\n  the [`Self::p_next`] chain of [`crate::vk::FenceCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_FENCE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkExportFenceWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExportFenceWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_attributes: *const std::ffi::c_void,
    pub dw_access: u32,
    pub name: *const u16,
}
impl ExportFenceWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::EXPORT_FENCE_WIN32_HANDLE_INFO_KHR;
}
impl Default for ExportFenceWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_attributes: std::ptr::null(), dw_access: Default::default(), name: std::ptr::null() }
    }
}
impl std::fmt::Debug for ExportFenceWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExportFenceWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_attributes", &self.p_attributes).field("dw_access", &self.dw_access).field("name", &self.name).finish()
    }
}
impl ExportFenceWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ExportFenceWin32HandleInfoKHRBuilder<'a> {
        ExportFenceWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExportFenceWin32HandleInfoKHR.html)) · Builder of [`ExportFenceWin32HandleInfoKHR`] <br/> VkExportFenceWin32HandleInfoKHR - Structure specifying additional attributes of Windows handles exported from a fence\n[](#_c_specification)C Specification\n----------\n\nTo specify additional attributes of NT handles exported from a fence, add a[`crate::vk::ExportFenceWin32HandleInfoKHR`] structure to the [`Self::p_next`] chain of\nthe [`crate::vk::FenceCreateInfo`] structure.\nThe [`crate::vk::ExportFenceWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkExportFenceWin32HandleInfoKHR {\n    VkStructureType               sType;\n    const void*                   pNext;\n    const SECURITY_ATTRIBUTES*    pAttributes;\n    DWORD                         dwAccess;\n    LPCWSTR                       name;\n} VkExportFenceWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_attributes`] is a pointer to a Windows `SECURITY_ATTRIBUTES`structure specifying security attributes of the handle.\n\n* [`Self::dw_access`] is a `DWORD` specifying access rights of the handle.\n\n* [`Self::name`] is a null-terminated UTF-16 string to associate with the\n  underlying synchronization primitive referenced by NT handles exported\n  from the created fence.\n[](#_description)Description\n----------\n\nIf [`crate::vk::ExportFenceCreateInfo`] is not inluded in the same [`Self::p_next`]chain, this structure is ignored.\n\nIf [`crate::vk::ExportFenceCreateInfo`] is included in the [`Self::p_next`] chain of[`crate::vk::FenceCreateInfo`] with a Windows `handleType`, but either[`crate::vk::ExportFenceWin32HandleInfoKHR`] is not included in the [`Self::p_next`]chain, or if it is but [`Self::p_attributes`] is set to `NULL`, default security\ndescriptor values will be used, and child processes created by the\napplication will not inherit the handle, as described in the MSDN\ndocumentation for “Synchronization Object Security and Access Rights”<sup>1</sup>.\nFurther, if the structure is not present, the access rights will be\n\n`DXGI_SHARED_RESOURCE_READ` | `DXGI_SHARED_RESOURCE_WRITE`\n\nfor handles of the following types:\n\n[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`]\n\n1\n\n[https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights](https://docs.microsoft.com/en-us/windows/win32/sync/synchronization-object-security-and-access-rights)\n\nValid Usage\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-handleTypes-01447  \n   If [`crate::vk::ExportFenceCreateInfo::handle_types`] does not include[`crate::vk::ExternalFenceHandleTypeFlagBits::OPAQUE_WIN32`], a[`crate::vk::ExportFenceWin32HandleInfoKHR`] structure **must** not be included in\n  the [`Self::p_next`] chain of [`crate::vk::FenceCreateInfo`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::EXPORT_FENCE_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkExportFenceWin32HandleInfoKHR-pAttributes-parameter  \n   If [`Self::p_attributes`] is not `NULL`, [`Self::p_attributes`] **must** be a valid pointer to a valid `SECURITY_ATTRIBUTES` value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ExportFenceWin32HandleInfoKHRBuilder<'a>(ExportFenceWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ExportFenceWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ExportFenceWin32HandleInfoKHRBuilder<'a> {
        ExportFenceWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn attributes(mut self, attributes: *const std::ffi::c_void) -> Self {
        self.0.p_attributes = attributes;
        self
    }
    #[inline]
    pub fn dw_access(mut self, dw_access: u32) -> Self {
        self.0.dw_access = dw_access as _;
        self
    }
    #[inline]
    pub fn name(mut self, name: &'a u16) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExportFenceWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ExportFenceWin32HandleInfoKHRBuilder<'a> {
    fn default() -> ExportFenceWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExportFenceWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExportFenceWin32HandleInfoKHRBuilder<'a> {
    type Target = ExportFenceWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExportFenceWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceGetWin32HandleInfoKHR.html)) · Structure <br/> VkFenceGetWin32HandleInfoKHR - Structure describing a Win32 handle fence export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FenceGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkFenceGetWin32HandleInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n} VkFenceGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) for a description of the\nproperties of the defined external fence handle types.\n\nValid Usage\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01448  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportFenceCreateInfo::handle_types`] when the [`Self::fence`]’s\n  current payload was created\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01449  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_fence_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::fence`] and [`Self::handle_type`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-fence-01450  \n  [`Self::fence`] **must** not currently have its payload replaced by an imported\n  payload as described below in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing) unless\n  that imported payload’s handle type was included in[`crate::vk::ExternalFenceProperties::export_from_imported_handle_types`] for[`Self::handle_type`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01451  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::fence`] **must** be signaled, or have an\n  associated [fence signal operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-signaling)pending execution\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01452  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FENCE_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_fence_win32_handle_khr`]\n"]
#[doc(alias = "VkFenceGetWin32HandleInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct FenceGetWin32HandleInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub fence: crate::vk1_0::Fence,
    pub handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits,
}
impl FenceGetWin32HandleInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::FENCE_GET_WIN32_HANDLE_INFO_KHR;
}
impl Default for FenceGetWin32HandleInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), fence: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for FenceGetWin32HandleInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("FenceGetWin32HandleInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fence", &self.fence).field("handle_type", &self.handle_type).finish()
    }
}
impl FenceGetWin32HandleInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> FenceGetWin32HandleInfoKHRBuilder<'a> {
        FenceGetWin32HandleInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkFenceGetWin32HandleInfoKHR.html)) · Builder of [`FenceGetWin32HandleInfoKHR`] <br/> VkFenceGetWin32HandleInfoKHR - Structure describing a Win32 handle fence export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::FenceGetWin32HandleInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_fence_win32\ntypedef struct VkFenceGetWin32HandleInfoKHR {\n    VkStructureType                      sType;\n    const void*                          pNext;\n    VkFence                              fence;\n    VkExternalFenceHandleTypeFlagBits    handleType;\n} VkFenceGetWin32HandleInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::fence`] is the fence from which state will be exported.\n\n* [`Self::handle_type`] is a [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the handle returned depend on the value of[`Self::handle_type`].\nSee [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) for a description of the\nproperties of the defined external fence handle types.\n\nValid Usage\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01448  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportFenceCreateInfo::handle_types`] when the [`Self::fence`]’s\n  current payload was created\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01449  \n   If [`Self::handle_type`] is defined as an NT handle,[`crate::vk::DeviceLoader::get_fence_win32_handle_khr`] **must** be called no more than once for\n  each valid unique combination of [`Self::fence`] and [`Self::handle_type`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-fence-01450  \n  [`Self::fence`] **must** not currently have its payload replaced by an imported\n  payload as described below in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing) unless\n  that imported payload’s handle type was included in[`crate::vk::ExternalFenceProperties::export_from_imported_handle_types`] for[`Self::handle_type`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01451  \n   If [`Self::handle_type`] refers to a handle type with copy payload\n  transference semantics, [`Self::fence`] **must** be signaled, or have an\n  associated [fence signal operation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-signaling)pending execution\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-01452  \n  [`Self::handle_type`] **must** be defined as an NT handle or a global share\n  handle\n\nValid Usage (Implicit)\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::FENCE_GET_WIN32_HANDLE_INFO_KHR`]\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-fence-parameter  \n  [`Self::fence`] **must** be a valid [`crate::vk::Fence`] handle\n\n* []() VUID-VkFenceGetWin32HandleInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalFenceHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalFenceHandleTypeFlagBits.html), [`crate::vk::Fence`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_fence_win32_handle_khr`]\n"]
#[repr(transparent)]
pub struct FenceGetWin32HandleInfoKHRBuilder<'a>(FenceGetWin32HandleInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> FenceGetWin32HandleInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> FenceGetWin32HandleInfoKHRBuilder<'a> {
        FenceGetWin32HandleInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fence(mut self, fence: crate::vk1_0::Fence) -> Self {
        self.0.fence = fence as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalFenceHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> FenceGetWin32HandleInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for FenceGetWin32HandleInfoKHRBuilder<'a> {
    fn default() -> FenceGetWin32HandleInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for FenceGetWin32HandleInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for FenceGetWin32HandleInfoKHRBuilder<'a> {
    type Target = FenceGetWin32HandleInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for FenceGetWin32HandleInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_external_fence_win32`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetFenceWin32HandleKHR.html)) · Function <br/> vkGetFenceWin32HandleKHR - Get a Windows HANDLE for a fence\n[](#_c_specification)C Specification\n----------\n\nTo export a Windows handle representing the state of a fence, call:\n\n```\n// Provided by VK_KHR_external_fence_win32\nVkResult vkGetFenceWin32HandleKHR(\n    VkDevice                                    device,\n    const VkFenceGetWin32HandleInfoKHR*         pGetWin32HandleInfo,\n    HANDLE*                                     pHandle);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence being\n  exported.\n\n* [`Self::p_get_win32_handle_info`] is a pointer to a[`crate::vk::FenceGetWin32HandleInfoKHR`] structure containing parameters of\n  the export operation.\n\n* [`Self::p_handle`] will return the Windows handle representing the fence\n  state.\n[](#_description)Description\n----------\n\nFor handle types defined as NT handles, the handles returned by[`crate::vk::DeviceLoader::get_fence_win32_handle_khr`] are owned by the application.\nTo avoid leaking resources, the application **must** release ownership of them\nusing the `CloseHandle` system call when they are no longer needed.\n\nExporting a Windows handle from a fence **may** have side effects depending on\nthe transference of the specified handle type, as described in[Importing Fence Payloads](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-fences-importing).\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetFenceWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetFenceWin32HandleKHR-pGetWin32HandleInfo-parameter  \n  [`Self::p_get_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::FenceGetWin32HandleInfoKHR`] structure\n\n* []() VUID-vkGetFenceWin32HandleKHR-pHandle-parameter  \n  [`Self::p_handle`] **must** be a valid pointer to a `HANDLE` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::FenceGetWin32HandleInfoKHR`]\n"]
    #[doc(alias = "vkGetFenceWin32HandleKHR")]
    pub unsafe fn get_fence_win32_handle_khr(&self, get_win32_handle_info: &crate::extensions::khr_external_fence_win32::FenceGetWin32HandleInfoKHR, handle: *mut *mut std::ffi::c_void) -> crate::utils::VulkanResult<()> {
        let _function = self.get_fence_win32_handle_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, get_win32_handle_info as _, handle);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkImportFenceWin32HandleKHR.html)) · Function <br/> vkImportFenceWin32HandleKHR - Import a fence from a Windows HANDLE\n[](#_c_specification)C Specification\n----------\n\nTo import a fence payload from a Windows handle, call:\n\n```\n// Provided by VK_KHR_external_fence_win32\nVkResult vkImportFenceWin32HandleKHR(\n    VkDevice                                    device,\n    const VkImportFenceWin32HandleInfoKHR*      pImportFenceWin32HandleInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the fence.\n\n* [`Self::p_import_fence_win32_handle_info`] is a pointer to a[`crate::vk::ImportFenceWin32HandleInfoKHR`] structure specifying the fence and\n  import parameters.\n[](#_description)Description\n----------\n\nImporting a fence payload from Windows handles does not transfer ownership\nof the handle to the Vulkan implementation.\nFor handle types defined as NT handles, the application **must** release\nownership using the `CloseHandle` system call when the handle is no\nlonger needed.\n\nApplications **can** import the same fence payload into multiple instances of\nVulkan, into the same instance from which it was exported, and multiple\ntimes into a given Vulkan instance.\n\nValid Usage\n\n* []() VUID-vkImportFenceWin32HandleKHR-fence-04448  \n  `fence` **must** not be associated with any queue command that has not\n  yet completed execution on that queue\n\nValid Usage (Implicit)\n\n* []() VUID-vkImportFenceWin32HandleKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkImportFenceWin32HandleKHR-pImportFenceWin32HandleInfo-parameter  \n  [`Self::p_import_fence_win32_handle_info`] **must** be a valid pointer to a valid [`crate::vk::ImportFenceWin32HandleInfoKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImportFenceWin32HandleInfoKHR`]\n"]
    #[doc(alias = "vkImportFenceWin32HandleKHR")]
    pub unsafe fn import_fence_win32_handle_khr(&self, import_fence_win32_handle_info: &crate::extensions::khr_external_fence_win32::ImportFenceWin32HandleInfoKHR) -> crate::utils::VulkanResult<()> {
        let _function = self.import_fence_win32_handle_khr.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, import_fence_win32_handle_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
}
