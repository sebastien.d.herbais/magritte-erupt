#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_CAPABILITIES_SPEC_VERSION")]
pub const NV_EXTERNAL_MEMORY_CAPABILITIES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NV_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME")]
pub const NV_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_external_memory_capabilities");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_PROPERTIES_NV: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceExternalImageFormatPropertiesNV");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagsNV.html)) · Bitmask of [`ExternalMemoryHandleTypeFlagBitsNV`] <br/> VkExternalMemoryHandleTypeFlagsNV - Bitmask of VkExternalMemoryHandleTypeFlagBitsNV\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef VkFlags VkExternalMemoryHandleTypeFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`] is a bitmask type for setting a mask\nof zero or more [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExportMemoryAllocateInfoNV`], [`crate::vk::ExternalImageFormatPropertiesNV`], [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html), [`crate::vk::ExternalMemoryImageCreateInfoNV`], [`crate::vk::ImportMemoryWin32HandleInfoNV`], [`crate::vk::DeviceLoader::get_memory_win32_handle_nv`], [`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`]\n"] # [doc (alias = "VkExternalMemoryHandleTypeFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct ExternalMemoryHandleTypeFlagsNV : u32 { const OPAQUE_WIN32_NV = ExternalMemoryHandleTypeFlagBitsNV :: OPAQUE_WIN32_NV . 0 ; const OPAQUE_WIN32_KMT_NV = ExternalMemoryHandleTypeFlagBitsNV :: OPAQUE_WIN32_KMT_NV . 0 ; const D3D11_IMAGE_NV = ExternalMemoryHandleTypeFlagBitsNV :: D3D11_IMAGE_NV . 0 ; const D3D11_IMAGE_KMT_NV = ExternalMemoryHandleTypeFlagBitsNV :: D3D11_IMAGE_KMT_NV . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html)) · Bits enum of [`ExternalMemoryHandleTypeFlagsNV`] <br/> VkExternalMemoryHandleTypeFlagBitsNV - Bitmask specifying external memory handle types\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::ImportMemoryWin32HandleInfoNV::handle_type`],\nspecifying the type of an external memory handle, are:\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef enum VkExternalMemoryHandleTypeFlagBitsNV {\n    VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV = 0x00000001,\n    VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV = 0x00000002,\n    VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV = 0x00000004,\n    VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV = 0x00000008,\n} VkExternalMemoryHandleTypeFlagBitsNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::OPAQUE_WIN32_KMT_NV`] specifies a\n  handle to memory returned by [`crate::vk::DeviceLoader::get_memory_win32_handle_nv`].\n\n* [`Self::OPAQUE_WIN32_NV`] specifies a\n  handle to memory returned by [`crate::vk::DeviceLoader::get_memory_win32_handle_nv`], or one\n  duplicated from such a handle using `DuplicateHandle()`.\n\n* [`Self::D3D11_IMAGE_NV`] specifies a\n  valid NT handle to memory returned by`IDXGIResource1::CreateSharedHandle`, or a handle duplicated from such a\n  handle using `DuplicateHandle()`.\n\n* [`Self::D3D11_IMAGE_KMT_NV`] specifies a\n  handle to memory returned by `IDXGIResource::GetSharedHandle()`.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`]\n"]
#[doc(alias = "VkExternalMemoryHandleTypeFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ExternalMemoryHandleTypeFlagBitsNV(pub u32);
impl ExternalMemoryHandleTypeFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ExternalMemoryHandleTypeFlagsNV {
        ExternalMemoryHandleTypeFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ExternalMemoryHandleTypeFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::OPAQUE_WIN32_NV => "OPAQUE_WIN32_NV",
            &Self::OPAQUE_WIN32_KMT_NV => "OPAQUE_WIN32_KMT_NV",
            &Self::D3D11_IMAGE_NV => "D3D11_IMAGE_NV",
            &Self::D3D11_IMAGE_KMT_NV => "D3D11_IMAGE_KMT_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_capabilities`]"]
impl crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagBitsNV {
    pub const OPAQUE_WIN32_NV: Self = Self(1);
    pub const OPAQUE_WIN32_KMT_NV: Self = Self(2);
    pub const D3D11_IMAGE_NV: Self = Self(4);
    pub const D3D11_IMAGE_KMT_NV: Self = Self(8);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagsNV.html)) · Bitmask of [`ExternalMemoryFeatureFlagBitsNV`] <br/> VkExternalMemoryFeatureFlagsNV - Bitmask of VkExternalMemoryFeatureFlagBitsNV\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef VkFlags VkExternalMemoryFeatureFlagsNV;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ExternalMemoryFeatureFlagBitsNV`] is a bitmask type for setting a mask of\nzero or more [VkExternalMemoryFeatureFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagBitsNV.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalImageFormatPropertiesNV`], [VkExternalMemoryFeatureFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagBitsNV.html)\n"] # [doc (alias = "VkExternalMemoryFeatureFlagsNV")] # [derive (Default)] # [repr (transparent)] pub struct ExternalMemoryFeatureFlagsNV : u32 { const DEDICATED_ONLY_NV = ExternalMemoryFeatureFlagBitsNV :: DEDICATED_ONLY_NV . 0 ; const EXPORTABLE_NV = ExternalMemoryFeatureFlagBitsNV :: EXPORTABLE_NV . 0 ; const IMPORTABLE_NV = ExternalMemoryFeatureFlagBitsNV :: IMPORTABLE_NV . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagBitsNV.html)) · Bits enum of [`ExternalMemoryFeatureFlagsNV`] <br/> VkExternalMemoryFeatureFlagBitsNV - Bitmask specifying external memory features\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::ExternalImageFormatPropertiesNV::external_memory_features`],\nindicating properties of the external memory handle type, are:\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef enum VkExternalMemoryFeatureFlagBitsNV {\n    VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV = 0x00000001,\n    VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV = 0x00000002,\n    VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV = 0x00000004,\n} VkExternalMemoryFeatureFlagBitsNV;\n```\n[](#_description)Description\n----------\n\n* [`Self::DEDICATED_ONLY_NV`] specifies that\n  external memory of the specified type **must** be created as a dedicated\n  allocation when used in the manner specified.\n\n* [`Self::EXPORTABLE_NV`] specifies that the\n  implementation supports exporting handles of the specified type.\n\n* [`Self::IMPORTABLE_NV`] specifies that the\n  implementation supports importing handles of the specified type.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalImageFormatPropertiesNV`], [`crate::vk::ExternalMemoryFeatureFlagBitsNV`], [`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`]\n"]
#[doc(alias = "VkExternalMemoryFeatureFlagBitsNV")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ExternalMemoryFeatureFlagBitsNV(pub u32);
impl ExternalMemoryFeatureFlagBitsNV {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ExternalMemoryFeatureFlagsNV {
        ExternalMemoryFeatureFlagsNV::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ExternalMemoryFeatureFlagBitsNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::DEDICATED_ONLY_NV => "DEDICATED_ONLY_NV",
            &Self::EXPORTABLE_NV => "EXPORTABLE_NV",
            &Self::IMPORTABLE_NV => "IMPORTABLE_NV",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_capabilities`]"]
impl crate::extensions::nv_external_memory_capabilities::ExternalMemoryFeatureFlagBitsNV {
    pub const DEDICATED_ONLY_NV: Self = Self(1);
    pub const EXPORTABLE_NV: Self = Self(2);
    pub const IMPORTABLE_NV: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceExternalImageFormatPropertiesNV.html)) · Function <br/> vkGetPhysicalDeviceExternalImageFormatPropertiesNV - determine image capabilities compatible with external memory handle types\n[](#_c_specification)C Specification\n----------\n\nTo determine the image capabilities compatible with an external memory\nhandle type, call:\n\n```\n// Provided by VK_NV_external_memory_capabilities\nVkResult vkGetPhysicalDeviceExternalImageFormatPropertiesNV(\n    VkPhysicalDevice                            physicalDevice,\n    VkFormat                                    format,\n    VkImageType                                 type,\n    VkImageTiling                               tiling,\n    VkImageUsageFlags                           usage,\n    VkImageCreateFlags                          flags,\n    VkExternalMemoryHandleTypeFlagsNV           externalHandleType,\n    VkExternalImageFormatPropertiesNV*          pExternalImageFormatProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the\n  image capabilities\n\n* [`Self::format`] is the image format, corresponding to[`crate::vk::ImageCreateInfo::format`].\n\n* [`Self::_type`] is the image type, corresponding to[`crate::vk::ImageCreateInfo::image_type`].\n\n* [`Self::tiling`] is the image tiling, corresponding to[`crate::vk::ImageCreateInfo::tiling`].\n\n* [`Self::usage`] is the intended usage of the image, corresponding to[`crate::vk::ImageCreateInfo::usage`].\n\n* [`Self::flags`] is a bitmask describing additional parameters of the image,\n  corresponding to [`crate::vk::ImageCreateInfo::flags`].\n\n* [`Self::external_handle_type`] is either one of the bits from[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html), or 0.\n\n* [`Self::p_external_image_format_properties`] is a pointer to a[`crate::vk::ExternalImageFormatPropertiesNV`] structure in which capabilities\n  are returned.\n[](#_description)Description\n----------\n\nIf [`Self::external_handle_type`] is 0,`pExternalImageFormatProperties->imageFormatProperties` will return the\nsame values as a call to [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties`], and\nthe other members of [`Self::p_external_image_format_properties`] will all be 0.\nOtherwise, they are filled in as described for[`crate::vk::ExternalImageFormatPropertiesNV`].\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::ImageType`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-tiling-parameter  \n  [`Self::tiling`] **must** be a valid [`crate::vk::ImageTiling`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-usage-parameter  \n  [`Self::usage`] **must** be a valid combination of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-usage-requiredbitmask  \n  [`Self::usage`] **must** not be `0`\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkImageCreateFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageCreateFlagBits.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-externalHandleType-parameter  \n  [`Self::external_handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-pExternalImageFormatProperties-parameter  \n  [`Self::p_external_image_format_properties`] **must** be a valid pointer to a [`crate::vk::ExternalImageFormatPropertiesNV`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalImageFormatPropertiesNV`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::Format`], [`crate::vk::ImageCreateFlagBits`], [`crate::vk::ImageTiling`], [`crate::vk::ImageType`], [`crate::vk::ImageUsageFlagBits`], [`crate::vk::PhysicalDevice`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceExternalImageFormatPropertiesNV = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, format: crate::vk1_0::Format, _type: crate::vk1_0::ImageType, tiling: crate::vk1_0::ImageTiling, usage: crate::vk1_0::ImageUsageFlags, flags: crate::vk1_0::ImageCreateFlags, external_handle_type: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV, p_external_image_format_properties: *mut crate::extensions::nv_external_memory_capabilities::ExternalImageFormatPropertiesNV) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalImageFormatPropertiesNV.html)) · Structure <br/> VkExternalImageFormatPropertiesNV - Structure specifying external image format properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ExternalImageFormatPropertiesNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef struct VkExternalImageFormatPropertiesNV {\n    VkImageFormatProperties              imageFormatProperties;\n    VkExternalMemoryFeatureFlagsNV       externalMemoryFeatures;\n    VkExternalMemoryHandleTypeFlagsNV    exportFromImportedHandleTypes;\n    VkExternalMemoryHandleTypeFlagsNV    compatibleHandleTypes;\n} VkExternalImageFormatPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::image_format_properties`] will be filled in as when calling[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties`], but the values returned**may** vary depending on the external handle type requested.\n\n* [`Self::external_memory_features`] is a bitmask of[VkExternalMemoryFeatureFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagBitsNV.html), indicating properties of the\n  external memory handle type\n  ([`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`])\n  being queried, or 0 if the external memory handle type is 0.\n\n* [`Self::export_from_imported_handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a bit set for\n  every external handle type that **may** be used to create memory from which\n  the handles of the type specified in[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]**can** be exported, or 0 if the external memory handle type is 0.\n\n* [`Self::compatible_handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a bit set for\n  every external handle type that **may** be specified simultaneously with\n  the handle type specified by[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]when calling [`crate::vk::PFN_vkAllocateMemory`], or 0 if the external memory handle\n  type is 0.[`Self::compatible_handle_types`] will always contain[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryFeatureFlagBitsNV`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::ImageFormatProperties`], [`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`]\n"]
#[doc(alias = "VkExternalImageFormatPropertiesNV")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ExternalImageFormatPropertiesNV {
    pub image_format_properties: crate::vk1_0::ImageFormatProperties,
    pub external_memory_features: crate::extensions::nv_external_memory_capabilities::ExternalMemoryFeatureFlagsNV,
    pub export_from_imported_handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV,
    pub compatible_handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV,
}
impl Default for ExternalImageFormatPropertiesNV {
    fn default() -> Self {
        Self { image_format_properties: Default::default(), external_memory_features: Default::default(), export_from_imported_handle_types: Default::default(), compatible_handle_types: Default::default() }
    }
}
impl std::fmt::Debug for ExternalImageFormatPropertiesNV {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ExternalImageFormatPropertiesNV").field("image_format_properties", &self.image_format_properties).field("external_memory_features", &self.external_memory_features).field("export_from_imported_handle_types", &self.export_from_imported_handle_types).field("compatible_handle_types", &self.compatible_handle_types).finish()
    }
}
impl ExternalImageFormatPropertiesNV {
    #[inline]
    pub fn into_builder<'a>(self) -> ExternalImageFormatPropertiesNVBuilder<'a> {
        ExternalImageFormatPropertiesNVBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalImageFormatPropertiesNV.html)) · Builder of [`ExternalImageFormatPropertiesNV`] <br/> VkExternalImageFormatPropertiesNV - Structure specifying external image format properties\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ExternalImageFormatPropertiesNV`] structure is defined as:\n\n```\n// Provided by VK_NV_external_memory_capabilities\ntypedef struct VkExternalImageFormatPropertiesNV {\n    VkImageFormatProperties              imageFormatProperties;\n    VkExternalMemoryFeatureFlagsNV       externalMemoryFeatures;\n    VkExternalMemoryHandleTypeFlagsNV    exportFromImportedHandleTypes;\n    VkExternalMemoryHandleTypeFlagsNV    compatibleHandleTypes;\n} VkExternalImageFormatPropertiesNV;\n```\n[](#_members)Members\n----------\n\n* [`Self::image_format_properties`] will be filled in as when calling[`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties`], but the values returned**may** vary depending on the external handle type requested.\n\n* [`Self::external_memory_features`] is a bitmask of[VkExternalMemoryFeatureFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryFeatureFlagBitsNV.html), indicating properties of the\n  external memory handle type\n  ([`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`])\n  being queried, or 0 if the external memory handle type is 0.\n\n* [`Self::export_from_imported_handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a bit set for\n  every external handle type that **may** be used to create memory from which\n  the handles of the type specified in[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]**can** be exported, or 0 if the external memory handle type is 0.\n\n* [`Self::compatible_handle_types`] is a bitmask of[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) containing a bit set for\n  every external handle type that **may** be specified simultaneously with\n  the handle type specified by[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]when calling [`crate::vk::PFN_vkAllocateMemory`], or 0 if the external memory handle\n  type is 0.[`Self::compatible_handle_types`] will always contain[`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv::external_handle_type`]\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalMemoryFeatureFlagBitsNV`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::ImageFormatProperties`], [`crate::vk::InstanceLoader::get_physical_device_external_image_format_properties_nv`]\n"]
#[repr(transparent)]
pub struct ExternalImageFormatPropertiesNVBuilder<'a>(ExternalImageFormatPropertiesNV, std::marker::PhantomData<&'a ()>);
impl<'a> ExternalImageFormatPropertiesNVBuilder<'a> {
    #[inline]
    pub fn new() -> ExternalImageFormatPropertiesNVBuilder<'a> {
        ExternalImageFormatPropertiesNVBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image_format_properties(mut self, image_format_properties: crate::vk1_0::ImageFormatProperties) -> Self {
        self.0.image_format_properties = image_format_properties as _;
        self
    }
    #[inline]
    pub fn external_memory_features(mut self, external_memory_features: crate::extensions::nv_external_memory_capabilities::ExternalMemoryFeatureFlagsNV) -> Self {
        self.0.external_memory_features = external_memory_features as _;
        self
    }
    #[inline]
    pub fn export_from_imported_handle_types(mut self, export_from_imported_handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV) -> Self {
        self.0.export_from_imported_handle_types = export_from_imported_handle_types as _;
        self
    }
    #[inline]
    pub fn compatible_handle_types(mut self, compatible_handle_types: crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV) -> Self {
        self.0.compatible_handle_types = compatible_handle_types as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ExternalImageFormatPropertiesNV {
        self.0
    }
}
impl<'a> std::default::Default for ExternalImageFormatPropertiesNVBuilder<'a> {
    fn default() -> ExternalImageFormatPropertiesNVBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ExternalImageFormatPropertiesNVBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ExternalImageFormatPropertiesNVBuilder<'a> {
    type Target = ExternalImageFormatPropertiesNV;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ExternalImageFormatPropertiesNVBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nv_external_memory_capabilities`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceExternalImageFormatPropertiesNV.html)) · Function <br/> vkGetPhysicalDeviceExternalImageFormatPropertiesNV - determine image capabilities compatible with external memory handle types\n[](#_c_specification)C Specification\n----------\n\nTo determine the image capabilities compatible with an external memory\nhandle type, call:\n\n```\n// Provided by VK_NV_external_memory_capabilities\nVkResult vkGetPhysicalDeviceExternalImageFormatPropertiesNV(\n    VkPhysicalDevice                            physicalDevice,\n    VkFormat                                    format,\n    VkImageType                                 type,\n    VkImageTiling                               tiling,\n    VkImageUsageFlags                           usage,\n    VkImageCreateFlags                          flags,\n    VkExternalMemoryHandleTypeFlagsNV           externalHandleType,\n    VkExternalImageFormatPropertiesNV*          pExternalImageFormatProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the physical device from which to query the\n  image capabilities\n\n* [`Self::format`] is the image format, corresponding to[`crate::vk::ImageCreateInfo::format`].\n\n* [`Self::_type`] is the image type, corresponding to[`crate::vk::ImageCreateInfo::image_type`].\n\n* [`Self::tiling`] is the image tiling, corresponding to[`crate::vk::ImageCreateInfo::tiling`].\n\n* [`Self::usage`] is the intended usage of the image, corresponding to[`crate::vk::ImageCreateInfo::usage`].\n\n* [`Self::flags`] is a bitmask describing additional parameters of the image,\n  corresponding to [`crate::vk::ImageCreateInfo::flags`].\n\n* [`Self::external_handle_type`] is either one of the bits from[VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html), or 0.\n\n* [`Self::p_external_image_format_properties`] is a pointer to a[`crate::vk::ExternalImageFormatPropertiesNV`] structure in which capabilities\n  are returned.\n[](#_description)Description\n----------\n\nIf [`Self::external_handle_type`] is 0,`pExternalImageFormatProperties->imageFormatProperties` will return the\nsame values as a call to [`crate::vk::PFN_vkGetPhysicalDeviceImageFormatProperties`], and\nthe other members of [`Self::p_external_image_format_properties`] will all be 0.\nOtherwise, they are filled in as described for[`crate::vk::ExternalImageFormatPropertiesNV`].\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-format-parameter  \n  [`Self::format`] **must** be a valid [`crate::vk::Format`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::ImageType`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-tiling-parameter  \n  [`Self::tiling`] **must** be a valid [`crate::vk::ImageTiling`] value\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-usage-parameter  \n  [`Self::usage`] **must** be a valid combination of [VkImageUsageFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageUsageFlagBits.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-usage-requiredbitmask  \n  [`Self::usage`] **must** not be `0`\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkImageCreateFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageCreateFlagBits.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-externalHandleType-parameter  \n  [`Self::external_handle_type`] **must** be a valid combination of [VkExternalMemoryHandleTypeFlagBitsNV](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html) values\n\n* []() VUID-vkGetPhysicalDeviceExternalImageFormatPropertiesNV-pExternalImageFormatProperties-parameter  \n  [`Self::p_external_image_format_properties`] **must** be a valid pointer to a [`crate::vk::ExternalImageFormatPropertiesNV`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_FORMAT_NOT_SUPPORTED`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ExternalImageFormatPropertiesNV`], [`crate::vk::ExternalMemoryHandleTypeFlagBitsNV`], [`crate::vk::Format`], [`crate::vk::ImageCreateFlagBits`], [`crate::vk::ImageTiling`], [`crate::vk::ImageType`], [`crate::vk::ImageUsageFlagBits`], [`crate::vk::PhysicalDevice`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceExternalImageFormatPropertiesNV")]
    pub unsafe fn get_physical_device_external_image_format_properties_nv(&self, physical_device: crate::vk1_0::PhysicalDevice, format: crate::vk1_0::Format, _type: crate::vk1_0::ImageType, tiling: crate::vk1_0::ImageTiling, usage: crate::vk1_0::ImageUsageFlags, flags: Option<crate::vk1_0::ImageCreateFlags>, external_handle_type: Option<crate::extensions::nv_external_memory_capabilities::ExternalMemoryHandleTypeFlagsNV>) -> crate::utils::VulkanResult<crate::extensions::nv_external_memory_capabilities::ExternalImageFormatPropertiesNV> {
        let _function = self.get_physical_device_external_image_format_properties_nv.expect(crate::NOT_LOADED_MESSAGE);
        let mut external_image_format_properties = Default::default();
        let _return = _function(
            physical_device as _,
            format as _,
            _type as _,
            tiling as _,
            usage as _,
            match flags {
                Some(v) => v,
                None => Default::default(),
            },
            match external_handle_type {
                Some(v) => v,
                None => Default::default(),
            },
            &mut external_image_format_properties,
        );
        crate::utils::VulkanResult::new(_return, external_image_format_properties)
    }
}
