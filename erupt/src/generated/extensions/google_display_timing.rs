#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GOOGLE_DISPLAY_TIMING_SPEC_VERSION")]
pub const GOOGLE_DISPLAY_TIMING_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_GOOGLE_DISPLAY_TIMING_EXTENSION_NAME")]
pub const GOOGLE_DISPLAY_TIMING_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_GOOGLE_display_timing");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_REFRESH_CYCLE_DURATION_GOOGLE: *const std::os::raw::c_char = crate::cstr!("vkGetRefreshCycleDurationGOOGLE");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PAST_PRESENTATION_TIMING_GOOGLE: *const std::os::raw::c_char = crate::cstr!("vkGetPastPresentationTimingGOOGLE");
#[doc = "Provided by [`crate::extensions::google_display_timing`]"]
impl crate::vk1_0::StructureType {
    pub const PRESENT_TIMES_INFO_GOOGLE: Self = Self(1000092000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetRefreshCycleDurationGOOGLE.html)) · Function <br/> vkGetRefreshCycleDurationGOOGLE - Obtain the RC duration of the PE's display\n[](#_c_specification)C Specification\n----------\n\nTo query the duration of a refresh cycle (RC) for the presentation engine’s\ndisplay, call:\n\n```\n// Provided by VK_GOOGLE_display_timing\nVkResult vkGetRefreshCycleDurationGOOGLE(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    VkRefreshCycleDurationGOOGLE*               pDisplayTimingProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to obtain the refresh duration for.\n\n* [`Self::p_display_timing_properties`] is a pointer to a[`crate::vk::RefreshCycleDurationGOOGLE`] structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-pDisplayTimingProperties-parameter  \n  [`Self::p_display_timing_properties`] **must** be a valid pointer to a [`crate::vk::RefreshCycleDurationGOOGLE`] structure\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::RefreshCycleDurationGOOGLE`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetRefreshCycleDurationGOOGLE = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, p_display_timing_properties: *mut crate::extensions::google_display_timing::RefreshCycleDurationGOOGLE) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPastPresentationTimingGOOGLE.html)) · Function <br/> vkGetPastPresentationTimingGOOGLE - Obtain timing of a previously-presented image\n[](#_c_specification)C Specification\n----------\n\nThe implementation will maintain a limited amount of history of timing\ninformation about previous presents.\nBecause of the asynchronous nature of the presentation engine, the timing\ninformation for a given [`crate::vk::DeviceLoader::queue_present_khr`] command will become\navailable some time later.\nThese time values can be asynchronously queried, and will be returned if\navailable.\nAll time values are in nanoseconds, relative to a monotonically-increasing\nclock (e.g. `CLOCK_MONOTONIC` (see clock\\_gettime(2)) on Android and Linux).\n\nTo asynchronously query the presentation engine, for newly-available timing\ninformation about one or more previous presents to a given swapchain, call:\n\n```\n// Provided by VK_GOOGLE_display_timing\nVkResult vkGetPastPresentationTimingGOOGLE(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    uint32_t*                                   pPresentationTimingCount,\n    VkPastPresentationTimingGOOGLE*             pPresentationTimings);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to obtain presentation timing\n  information duration for.\n\n* [`Self::p_presentation_timing_count`] is a pointer to an integer related to the\n  number of [`crate::vk::PastPresentationTimingGOOGLE`] structures to query, as\n  described below.\n\n* [`Self::p_presentation_timings`] is either `NULL` or a pointer to an array of[`crate::vk::PastPresentationTimingGOOGLE`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_presentation_timings`] is `NULL`, then the number of newly-available\ntiming records for the given [`Self::swapchain`] is returned in[`Self::p_presentation_timing_count`].\nOtherwise, [`Self::p_presentation_timing_count`] **must** point to a variable set by\nthe user to the number of elements in the [`Self::p_presentation_timings`] array,\nand on return the variable is overwritten with the number of structures\nactually written to [`Self::p_presentation_timings`].\nIf the value of [`Self::p_presentation_timing_count`] is less than the number of\nnewly-available timing records, at most [`Self::p_presentation_timing_count`]structures will be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead\nof [`crate::vk::Result::SUCCESS`], to indicate that not all the available timing records\nwere returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-pPresentationTimingCount-parameter  \n  [`Self::p_presentation_timing_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-pPresentationTimings-parameter  \n   If the value referenced by [`Self::p_presentation_timing_count`] is not `0`, and [`Self::p_presentation_timings`] is not `NULL`, [`Self::p_presentation_timings`] **must** be a valid pointer to an array of [`Self::p_presentation_timing_count`] [`crate::vk::PastPresentationTimingGOOGLE`] structures\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PastPresentationTimingGOOGLE`], [`crate::vk::SwapchainKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPastPresentationTimingGOOGLE = unsafe extern "system" fn(device: crate::vk1_0::Device, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, p_presentation_timing_count: *mut u32, p_presentation_timings: *mut crate::extensions::google_display_timing::PastPresentationTimingGOOGLE) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, PresentTimesInfoGOOGLE> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PresentTimesInfoGOOGLEBuilder<'_>> for crate::extensions::khr_swapchain::PresentInfoKHRBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRefreshCycleDurationGOOGLE.html)) · Structure <br/> VkRefreshCycleDurationGOOGLE - Structure containing the RC duration of a display\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::RefreshCycleDurationGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkRefreshCycleDurationGOOGLE {\n    uint64_t    refreshDuration;\n} VkRefreshCycleDurationGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::refresh_duration`] is the number of nanoseconds from the start of one\n  refresh cycle to the next.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_refresh_cycle_duration_google`]\n"]
#[doc(alias = "VkRefreshCycleDurationGOOGLE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct RefreshCycleDurationGOOGLE {
    pub refresh_duration: u64,
}
impl Default for RefreshCycleDurationGOOGLE {
    fn default() -> Self {
        Self { refresh_duration: Default::default() }
    }
}
impl std::fmt::Debug for RefreshCycleDurationGOOGLE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("RefreshCycleDurationGOOGLE").field("refresh_duration", &self.refresh_duration).finish()
    }
}
impl RefreshCycleDurationGOOGLE {
    #[inline]
    pub fn into_builder<'a>(self) -> RefreshCycleDurationGOOGLEBuilder<'a> {
        RefreshCycleDurationGOOGLEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRefreshCycleDurationGOOGLE.html)) · Builder of [`RefreshCycleDurationGOOGLE`] <br/> VkRefreshCycleDurationGOOGLE - Structure containing the RC duration of a display\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::RefreshCycleDurationGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkRefreshCycleDurationGOOGLE {\n    uint64_t    refreshDuration;\n} VkRefreshCycleDurationGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::refresh_duration`] is the number of nanoseconds from the start of one\n  refresh cycle to the next.\n[](#_description)Description\n----------\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_refresh_cycle_duration_google`]\n"]
#[repr(transparent)]
pub struct RefreshCycleDurationGOOGLEBuilder<'a>(RefreshCycleDurationGOOGLE, std::marker::PhantomData<&'a ()>);
impl<'a> RefreshCycleDurationGOOGLEBuilder<'a> {
    #[inline]
    pub fn new() -> RefreshCycleDurationGOOGLEBuilder<'a> {
        RefreshCycleDurationGOOGLEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn refresh_duration(mut self, refresh_duration: u64) -> Self {
        self.0.refresh_duration = refresh_duration as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> RefreshCycleDurationGOOGLE {
        self.0
    }
}
impl<'a> std::default::Default for RefreshCycleDurationGOOGLEBuilder<'a> {
    fn default() -> RefreshCycleDurationGOOGLEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for RefreshCycleDurationGOOGLEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for RefreshCycleDurationGOOGLEBuilder<'a> {
    type Target = RefreshCycleDurationGOOGLE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for RefreshCycleDurationGOOGLEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPastPresentationTimingGOOGLE.html)) · Structure <br/> VkPastPresentationTimingGOOGLE - Structure containing timing information about a previously-presented image\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PastPresentationTimingGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPastPresentationTimingGOOGLE {\n    uint32_t    presentID;\n    uint64_t    desiredPresentTime;\n    uint64_t    actualPresentTime;\n    uint64_t    earliestPresentTime;\n    uint64_t    presentMargin;\n} VkPastPresentationTimingGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::present_id`] is an application-provided value that was given to a\n  previous [`crate::vk::DeviceLoader::queue_present_khr`] command via[`crate::vk::PresentTimeGOOGLE::present_id`] (see below).\n  It **can** be used to uniquely identify a previous present with the[`crate::vk::DeviceLoader::queue_present_khr`] command.\n\n* [`Self::desired_present_time`] is an application-provided value that was given\n  to a previous [`crate::vk::DeviceLoader::queue_present_khr`] command via[`crate::vk::PresentTimeGOOGLE::desired_present_time`].\n  If non-zero, it was used by the application to indicate that an image\n  not be presented any sooner than [`Self::desired_present_time`].\n\n* [`Self::actual_present_time`] is the time when the image of the`swapchain` was actually displayed.\n\n* [`Self::earliest_present_time`] is the time when the image of the`swapchain` could have been displayed.\n  This **may** differ from [`Self::actual_present_time`] if the application\n  requested that the image be presented no sooner than[`crate::vk::PresentTimeGOOGLE::desired_present_time`].\n\n* [`Self::present_margin`] is an indication of how early the[`crate::vk::DeviceLoader::queue_present_khr`] command was processed compared to how soon it\n  needed to be processed, and still be presented at[`Self::earliest_present_time`].\n[](#_description)Description\n----------\n\nThe results for a given `swapchain` and [`Self::present_id`] are only\nreturned once from [`crate::vk::DeviceLoader::get_past_presentation_timing_google`].\n\nThe application **can** use the [`crate::vk::PastPresentationTimingGOOGLE`] values to\noccasionally adjust its timing.\nFor example, if [`Self::actual_present_time`] is later than expected (e.g. one`refreshDuration` late), the application may increase its target IPD to\na higher multiple of `refreshDuration` (e.g. decrease its frame rate\nfrom 60Hz to 30Hz).\nIf [`Self::actual_present_time`] and [`Self::earliest_present_time`] are consistently\ndifferent, and if [`Self::present_margin`] is consistently large enough, the\napplication may decrease its target IPD to a smaller multiple of`refreshDuration` (e.g. increase its frame rate from 30Hz to 60Hz).\nIf [`Self::actual_present_time`] and [`Self::earliest_present_time`] are same, and if[`Self::present_margin`] is consistently high, the application may delay the\nstart of its input-render-present loop in order to decrease the latency\nbetween user input and the corresponding present (always leaving some margin\nin case a new image takes longer to render than the previous image).\nAn application that desires its target IPD to always be the same as`refreshDuration`, can also adjust features until[`Self::actual_present_time`] is never late and [`Self::present_margin`] is\nsatisfactory.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_past_presentation_timing_google`]\n"]
#[doc(alias = "VkPastPresentationTimingGOOGLE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PastPresentationTimingGOOGLE {
    pub present_id: u32,
    pub desired_present_time: u64,
    pub actual_present_time: u64,
    pub earliest_present_time: u64,
    pub present_margin: u64,
}
impl Default for PastPresentationTimingGOOGLE {
    fn default() -> Self {
        Self { present_id: Default::default(), desired_present_time: Default::default(), actual_present_time: Default::default(), earliest_present_time: Default::default(), present_margin: Default::default() }
    }
}
impl std::fmt::Debug for PastPresentationTimingGOOGLE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PastPresentationTimingGOOGLE").field("present_id", &self.present_id).field("desired_present_time", &self.desired_present_time).field("actual_present_time", &self.actual_present_time).field("earliest_present_time", &self.earliest_present_time).field("present_margin", &self.present_margin).finish()
    }
}
impl PastPresentationTimingGOOGLE {
    #[inline]
    pub fn into_builder<'a>(self) -> PastPresentationTimingGOOGLEBuilder<'a> {
        PastPresentationTimingGOOGLEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPastPresentationTimingGOOGLE.html)) · Builder of [`PastPresentationTimingGOOGLE`] <br/> VkPastPresentationTimingGOOGLE - Structure containing timing information about a previously-presented image\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PastPresentationTimingGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPastPresentationTimingGOOGLE {\n    uint32_t    presentID;\n    uint64_t    desiredPresentTime;\n    uint64_t    actualPresentTime;\n    uint64_t    earliestPresentTime;\n    uint64_t    presentMargin;\n} VkPastPresentationTimingGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::present_id`] is an application-provided value that was given to a\n  previous [`crate::vk::DeviceLoader::queue_present_khr`] command via[`crate::vk::PresentTimeGOOGLE::present_id`] (see below).\n  It **can** be used to uniquely identify a previous present with the[`crate::vk::DeviceLoader::queue_present_khr`] command.\n\n* [`Self::desired_present_time`] is an application-provided value that was given\n  to a previous [`crate::vk::DeviceLoader::queue_present_khr`] command via[`crate::vk::PresentTimeGOOGLE::desired_present_time`].\n  If non-zero, it was used by the application to indicate that an image\n  not be presented any sooner than [`Self::desired_present_time`].\n\n* [`Self::actual_present_time`] is the time when the image of the`swapchain` was actually displayed.\n\n* [`Self::earliest_present_time`] is the time when the image of the`swapchain` could have been displayed.\n  This **may** differ from [`Self::actual_present_time`] if the application\n  requested that the image be presented no sooner than[`crate::vk::PresentTimeGOOGLE::desired_present_time`].\n\n* [`Self::present_margin`] is an indication of how early the[`crate::vk::DeviceLoader::queue_present_khr`] command was processed compared to how soon it\n  needed to be processed, and still be presented at[`Self::earliest_present_time`].\n[](#_description)Description\n----------\n\nThe results for a given `swapchain` and [`Self::present_id`] are only\nreturned once from [`crate::vk::DeviceLoader::get_past_presentation_timing_google`].\n\nThe application **can** use the [`crate::vk::PastPresentationTimingGOOGLE`] values to\noccasionally adjust its timing.\nFor example, if [`Self::actual_present_time`] is later than expected (e.g. one`refreshDuration` late), the application may increase its target IPD to\na higher multiple of `refreshDuration` (e.g. decrease its frame rate\nfrom 60Hz to 30Hz).\nIf [`Self::actual_present_time`] and [`Self::earliest_present_time`] are consistently\ndifferent, and if [`Self::present_margin`] is consistently large enough, the\napplication may decrease its target IPD to a smaller multiple of`refreshDuration` (e.g. increase its frame rate from 30Hz to 60Hz).\nIf [`Self::actual_present_time`] and [`Self::earliest_present_time`] are same, and if[`Self::present_margin`] is consistently high, the application may delay the\nstart of its input-render-present loop in order to decrease the latency\nbetween user input and the corresponding present (always leaving some margin\nin case a new image takes longer to render than the previous image).\nAn application that desires its target IPD to always be the same as`refreshDuration`, can also adjust features until[`Self::actual_present_time`] is never late and [`Self::present_margin`] is\nsatisfactory.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_past_presentation_timing_google`]\n"]
#[repr(transparent)]
pub struct PastPresentationTimingGOOGLEBuilder<'a>(PastPresentationTimingGOOGLE, std::marker::PhantomData<&'a ()>);
impl<'a> PastPresentationTimingGOOGLEBuilder<'a> {
    #[inline]
    pub fn new() -> PastPresentationTimingGOOGLEBuilder<'a> {
        PastPresentationTimingGOOGLEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn present_id(mut self, present_id: u32) -> Self {
        self.0.present_id = present_id as _;
        self
    }
    #[inline]
    pub fn desired_present_time(mut self, desired_present_time: u64) -> Self {
        self.0.desired_present_time = desired_present_time as _;
        self
    }
    #[inline]
    pub fn actual_present_time(mut self, actual_present_time: u64) -> Self {
        self.0.actual_present_time = actual_present_time as _;
        self
    }
    #[inline]
    pub fn earliest_present_time(mut self, earliest_present_time: u64) -> Self {
        self.0.earliest_present_time = earliest_present_time as _;
        self
    }
    #[inline]
    pub fn present_margin(mut self, present_margin: u64) -> Self {
        self.0.present_margin = present_margin as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PastPresentationTimingGOOGLE {
        self.0
    }
}
impl<'a> std::default::Default for PastPresentationTimingGOOGLEBuilder<'a> {
    fn default() -> PastPresentationTimingGOOGLEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PastPresentationTimingGOOGLEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PastPresentationTimingGOOGLEBuilder<'a> {
    type Target = PastPresentationTimingGOOGLE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PastPresentationTimingGOOGLEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentTimesInfoGOOGLE.html)) · Structure <br/> VkPresentTimesInfoGOOGLE - The earliest time each image should be presented\n[](#_c_specification)C Specification\n----------\n\nWhen the [VK_GOOGLE_display_timing](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_GOOGLE_display_timing.html) extension is enabled, additional\nfields **can** be specified that allow an application to specify the earliest\ntime that an image should be displayed.\nThis allows an application to avoid stutter that is caused by an image being\ndisplayed earlier than planned.\nSuch stuttering can occur with both fixed and variable-refresh-rate\ndisplays, because stuttering occurs when the geometry is not correctly\npositioned for when the image is displayed.\nAn application **can** instruct the presentation engine that an image should\nnot be displayed earlier than a specified time by adding a[`crate::vk::PresentTimesInfoGOOGLE`] structure to the [`Self::p_next`] chain of the[`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentTimesInfoGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPresentTimesInfoGOOGLE {\n    VkStructureType               sType;\n    const void*                   pNext;\n    uint32_t                      swapchainCount;\n    const VkPresentTimeGOOGLE*    pTimes;\n} VkPresentTimesInfoGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to by\n  this command.\n\n* [`Self::p_times`] is `NULL` or a pointer to an array of[`crate::vk::PresentTimeGOOGLE`] elements with [`Self::swapchain_count`] entries.\n  If not `NULL`, each element of [`Self::p_times`] contains the earliest time\n  to present the image corresponding to the entry in the[`crate::vk::PresentInfoKHR`]::`pImageIndices` array.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentTimesInfoGOOGLE-swapchainCount-01247  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where[`crate::vk::PresentInfoKHR`] is included in the [`Self::p_next`] chain of this[`crate::vk::PresentTimesInfoGOOGLE`] structure\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentTimesInfoGOOGLE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_TIMES_INFO_GOOGLE`]\n\n* []() VUID-VkPresentTimesInfoGOOGLE-pTimes-parameter  \n   If [`Self::p_times`] is not `NULL`, [`Self::p_times`] **must** be a valid pointer to an array of [`Self::swapchain_count`] [`crate::vk::PresentTimeGOOGLE`] structures\n\n* []() VUID-VkPresentTimesInfoGOOGLE-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentTimeGOOGLE`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPresentTimesInfoGOOGLE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentTimesInfoGOOGLE {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub swapchain_count: u32,
    pub p_times: *const crate::extensions::google_display_timing::PresentTimeGOOGLE,
}
impl PresentTimesInfoGOOGLE {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PRESENT_TIMES_INFO_GOOGLE;
}
impl Default for PresentTimesInfoGOOGLE {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), swapchain_count: Default::default(), p_times: std::ptr::null() }
    }
}
impl std::fmt::Debug for PresentTimesInfoGOOGLE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentTimesInfoGOOGLE").field("s_type", &self.s_type).field("p_next", &self.p_next).field("swapchain_count", &self.swapchain_count).field("p_times", &self.p_times).finish()
    }
}
impl PresentTimesInfoGOOGLE {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentTimesInfoGOOGLEBuilder<'a> {
        PresentTimesInfoGOOGLEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentTimesInfoGOOGLE.html)) · Builder of [`PresentTimesInfoGOOGLE`] <br/> VkPresentTimesInfoGOOGLE - The earliest time each image should be presented\n[](#_c_specification)C Specification\n----------\n\nWhen the [VK_GOOGLE_display_timing](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_GOOGLE_display_timing.html) extension is enabled, additional\nfields **can** be specified that allow an application to specify the earliest\ntime that an image should be displayed.\nThis allows an application to avoid stutter that is caused by an image being\ndisplayed earlier than planned.\nSuch stuttering can occur with both fixed and variable-refresh-rate\ndisplays, because stuttering occurs when the geometry is not correctly\npositioned for when the image is displayed.\nAn application **can** instruct the presentation engine that an image should\nnot be displayed earlier than a specified time by adding a[`crate::vk::PresentTimesInfoGOOGLE`] structure to the [`Self::p_next`] chain of the[`crate::vk::PresentInfoKHR`] structure.\n\nThe [`crate::vk::PresentTimesInfoGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPresentTimesInfoGOOGLE {\n    VkStructureType               sType;\n    const void*                   pNext;\n    uint32_t                      swapchainCount;\n    const VkPresentTimeGOOGLE*    pTimes;\n} VkPresentTimesInfoGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::swapchain_count`] is the number of swapchains being presented to by\n  this command.\n\n* [`Self::p_times`] is `NULL` or a pointer to an array of[`crate::vk::PresentTimeGOOGLE`] elements with [`Self::swapchain_count`] entries.\n  If not `NULL`, each element of [`Self::p_times`] contains the earliest time\n  to present the image corresponding to the entry in the[`crate::vk::PresentInfoKHR`]::`pImageIndices` array.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPresentTimesInfoGOOGLE-swapchainCount-01247  \n  [`Self::swapchain_count`] **must** be the same value as[`crate::vk::PresentInfoKHR`]::[`Self::swapchain_count`], where[`crate::vk::PresentInfoKHR`] is included in the [`Self::p_next`] chain of this[`crate::vk::PresentTimesInfoGOOGLE`] structure\n\nValid Usage (Implicit)\n\n* []() VUID-VkPresentTimesInfoGOOGLE-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PRESENT_TIMES_INFO_GOOGLE`]\n\n* []() VUID-VkPresentTimesInfoGOOGLE-pTimes-parameter  \n   If [`Self::p_times`] is not `NULL`, [`Self::p_times`] **must** be a valid pointer to an array of [`Self::swapchain_count`] [`crate::vk::PresentTimeGOOGLE`] structures\n\n* []() VUID-VkPresentTimesInfoGOOGLE-swapchainCount-arraylength  \n  [`Self::swapchain_count`] **must** be greater than `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentTimeGOOGLE`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PresentTimesInfoGOOGLEBuilder<'a>(PresentTimesInfoGOOGLE, std::marker::PhantomData<&'a ()>);
impl<'a> PresentTimesInfoGOOGLEBuilder<'a> {
    #[inline]
    pub fn new() -> PresentTimesInfoGOOGLEBuilder<'a> {
        PresentTimesInfoGOOGLEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn times(mut self, times: &'a [crate::extensions::google_display_timing::PresentTimeGOOGLEBuilder]) -> Self {
        self.0.p_times = times.as_ptr() as _;
        self.0.swapchain_count = times.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentTimesInfoGOOGLE {
        self.0
    }
}
impl<'a> std::default::Default for PresentTimesInfoGOOGLEBuilder<'a> {
    fn default() -> PresentTimesInfoGOOGLEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentTimesInfoGOOGLEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentTimesInfoGOOGLEBuilder<'a> {
    type Target = PresentTimesInfoGOOGLE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentTimesInfoGOOGLEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentTimeGOOGLE.html)) · Structure <br/> VkPresentTimeGOOGLE - The earliest time image should be presented\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PresentTimeGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPresentTimeGOOGLE {\n    uint32_t    presentID;\n    uint64_t    desiredPresentTime;\n} VkPresentTimeGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::present_id`] is an application-provided identification value, that**can** be used with the results of[`crate::vk::DeviceLoader::get_past_presentation_timing_google`], in order to uniquely identify\n  this present.\n  In order to be useful to the application, it **should** be unique within\n  some period of time that is meaningful to the application.\n\n* [`Self::desired_present_time`] specifies that the image given **should** not be\n  displayed to the user any earlier than this time.[`Self::desired_present_time`] is a time in nanoseconds, relative to a\n  monotonically-increasing clock (e.g. `CLOCK_MONOTONIC` (see\n  clock\\_gettime(2)) on Android and Linux).\n  A value of zero specifies that the presentation engine **may** display the\n  image at any time.\n  This is useful when the application desires to provide [`Self::present_id`],\n[](#_description)Description\n----------\n\n```\nbut does not need a specific pname:desiredPresentTime.\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentTimesInfoGOOGLE`]\n"]
#[doc(alias = "VkPresentTimeGOOGLE")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PresentTimeGOOGLE {
    pub present_id: u32,
    pub desired_present_time: u64,
}
impl Default for PresentTimeGOOGLE {
    fn default() -> Self {
        Self { present_id: Default::default(), desired_present_time: Default::default() }
    }
}
impl std::fmt::Debug for PresentTimeGOOGLE {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PresentTimeGOOGLE").field("present_id", &self.present_id).field("desired_present_time", &self.desired_present_time).finish()
    }
}
impl PresentTimeGOOGLE {
    #[inline]
    pub fn into_builder<'a>(self) -> PresentTimeGOOGLEBuilder<'a> {
        PresentTimeGOOGLEBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentTimeGOOGLE.html)) · Builder of [`PresentTimeGOOGLE`] <br/> VkPresentTimeGOOGLE - The earliest time image should be presented\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PresentTimeGOOGLE`] structure is defined as:\n\n```\n// Provided by VK_GOOGLE_display_timing\ntypedef struct VkPresentTimeGOOGLE {\n    uint32_t    presentID;\n    uint64_t    desiredPresentTime;\n} VkPresentTimeGOOGLE;\n```\n[](#_members)Members\n----------\n\n* [`Self::present_id`] is an application-provided identification value, that**can** be used with the results of[`crate::vk::DeviceLoader::get_past_presentation_timing_google`], in order to uniquely identify\n  this present.\n  In order to be useful to the application, it **should** be unique within\n  some period of time that is meaningful to the application.\n\n* [`Self::desired_present_time`] specifies that the image given **should** not be\n  displayed to the user any earlier than this time.[`Self::desired_present_time`] is a time in nanoseconds, relative to a\n  monotonically-increasing clock (e.g. `CLOCK_MONOTONIC` (see\n  clock\\_gettime(2)) on Android and Linux).\n  A value of zero specifies that the presentation engine **may** display the\n  image at any time.\n  This is useful when the application desires to provide [`Self::present_id`],\n[](#_description)Description\n----------\n\n```\nbut does not need a specific pname:desiredPresentTime.\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PresentTimesInfoGOOGLE`]\n"]
#[repr(transparent)]
pub struct PresentTimeGOOGLEBuilder<'a>(PresentTimeGOOGLE, std::marker::PhantomData<&'a ()>);
impl<'a> PresentTimeGOOGLEBuilder<'a> {
    #[inline]
    pub fn new() -> PresentTimeGOOGLEBuilder<'a> {
        PresentTimeGOOGLEBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn present_id(mut self, present_id: u32) -> Self {
        self.0.present_id = present_id as _;
        self
    }
    #[inline]
    pub fn desired_present_time(mut self, desired_present_time: u64) -> Self {
        self.0.desired_present_time = desired_present_time as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PresentTimeGOOGLE {
        self.0
    }
}
impl<'a> std::default::Default for PresentTimeGOOGLEBuilder<'a> {
    fn default() -> PresentTimeGOOGLEBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PresentTimeGOOGLEBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PresentTimeGOOGLEBuilder<'a> {
    type Target = PresentTimeGOOGLE;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PresentTimeGOOGLEBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::google_display_timing`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetRefreshCycleDurationGOOGLE.html)) · Function <br/> vkGetRefreshCycleDurationGOOGLE - Obtain the RC duration of the PE's display\n[](#_c_specification)C Specification\n----------\n\nTo query the duration of a refresh cycle (RC) for the presentation engine’s\ndisplay, call:\n\n```\n// Provided by VK_GOOGLE_display_timing\nVkResult vkGetRefreshCycleDurationGOOGLE(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    VkRefreshCycleDurationGOOGLE*               pDisplayTimingProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to obtain the refresh duration for.\n\n* [`Self::p_display_timing_properties`] is a pointer to a[`crate::vk::RefreshCycleDurationGOOGLE`] structure.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-pDisplayTimingProperties-parameter  \n  [`Self::p_display_timing_properties`] **must** be a valid pointer to a [`crate::vk::RefreshCycleDurationGOOGLE`] structure\n\n* []() VUID-vkGetRefreshCycleDurationGOOGLE-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::RefreshCycleDurationGOOGLE`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkGetRefreshCycleDurationGOOGLE")]
    pub unsafe fn get_refresh_cycle_duration_google(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR) -> crate::utils::VulkanResult<crate::extensions::google_display_timing::RefreshCycleDurationGOOGLE> {
        let _function = self.get_refresh_cycle_duration_google.expect(crate::NOT_LOADED_MESSAGE);
        let mut display_timing_properties = Default::default();
        let _return = _function(self.handle, swapchain as _, &mut display_timing_properties);
        crate::utils::VulkanResult::new(_return, display_timing_properties)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPastPresentationTimingGOOGLE.html)) · Function <br/> vkGetPastPresentationTimingGOOGLE - Obtain timing of a previously-presented image\n[](#_c_specification)C Specification\n----------\n\nThe implementation will maintain a limited amount of history of timing\ninformation about previous presents.\nBecause of the asynchronous nature of the presentation engine, the timing\ninformation for a given [`crate::vk::DeviceLoader::queue_present_khr`] command will become\navailable some time later.\nThese time values can be asynchronously queried, and will be returned if\navailable.\nAll time values are in nanoseconds, relative to a monotonically-increasing\nclock (e.g. `CLOCK_MONOTONIC` (see clock\\_gettime(2)) on Android and Linux).\n\nTo asynchronously query the presentation engine, for newly-available timing\ninformation about one or more previous presents to a given swapchain, call:\n\n```\n// Provided by VK_GOOGLE_display_timing\nVkResult vkGetPastPresentationTimingGOOGLE(\n    VkDevice                                    device,\n    VkSwapchainKHR                              swapchain,\n    uint32_t*                                   pPresentationTimingCount,\n    VkPastPresentationTimingGOOGLE*             pPresentationTimings);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated with [`Self::swapchain`].\n\n* [`Self::swapchain`] is the swapchain to obtain presentation timing\n  information duration for.\n\n* [`Self::p_presentation_timing_count`] is a pointer to an integer related to the\n  number of [`crate::vk::PastPresentationTimingGOOGLE`] structures to query, as\n  described below.\n\n* [`Self::p_presentation_timings`] is either `NULL` or a pointer to an array of[`crate::vk::PastPresentationTimingGOOGLE`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_presentation_timings`] is `NULL`, then the number of newly-available\ntiming records for the given [`Self::swapchain`] is returned in[`Self::p_presentation_timing_count`].\nOtherwise, [`Self::p_presentation_timing_count`] **must** point to a variable set by\nthe user to the number of elements in the [`Self::p_presentation_timings`] array,\nand on return the variable is overwritten with the number of structures\nactually written to [`Self::p_presentation_timings`].\nIf the value of [`Self::p_presentation_timing_count`] is less than the number of\nnewly-available timing records, at most [`Self::p_presentation_timing_count`]structures will be written, and [`crate::vk::Result::INCOMPLETE`] will be returned instead\nof [`crate::vk::Result::SUCCESS`], to indicate that not all the available timing records\nwere returned.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-swapchain-parameter  \n  [`Self::swapchain`] **must** be a valid [`crate::vk::SwapchainKHR`] handle\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-pPresentationTimingCount-parameter  \n  [`Self::p_presentation_timing_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-pPresentationTimings-parameter  \n   If the value referenced by [`Self::p_presentation_timing_count`] is not `0`, and [`Self::p_presentation_timings`] is not `NULL`, [`Self::p_presentation_timings`] **must** be a valid pointer to an array of [`Self::p_presentation_timing_count`] [`crate::vk::PastPresentationTimingGOOGLE`] structures\n\n* []() VUID-vkGetPastPresentationTimingGOOGLE-commonparent  \n   Both of [`Self::device`], and [`Self::swapchain`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Instance`]\n\nHost Synchronization\n\n* Host access to [`Self::swapchain`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_DEVICE_LOST`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DATE_KHR`]\n\n* [`crate::vk::Result::ERROR_SURFACE_LOST_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PastPresentationTimingGOOGLE`], [`crate::vk::SwapchainKHR`]\n"]
    #[doc(alias = "vkGetPastPresentationTimingGOOGLE")]
    pub unsafe fn get_past_presentation_timing_google(&self, swapchain: crate::extensions::khr_swapchain::SwapchainKHR, presentation_timing_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::google_display_timing::PastPresentationTimingGOOGLE>> {
        let _function = self.get_past_presentation_timing_google.expect(crate::NOT_LOADED_MESSAGE);
        let mut presentation_timing_count = match presentation_timing_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(self.handle, swapchain as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut presentation_timings = crate::SmallVec::from_elem(Default::default(), presentation_timing_count as _);
        let _return = _function(self.handle, swapchain as _, &mut presentation_timing_count, presentation_timings.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, presentation_timings)
    }
}
