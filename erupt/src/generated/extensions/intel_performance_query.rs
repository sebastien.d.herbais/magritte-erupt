#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_INTEL_PERFORMANCE_QUERY_SPEC_VERSION")]
pub const INTEL_PERFORMANCE_QUERY_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_INTEL_PERFORMANCE_QUERY_EXTENSION_NAME")]
pub const INTEL_PERFORMANCE_QUERY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_INTEL_performance_query");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_INITIALIZE_PERFORMANCE_API_INTEL: *const std::os::raw::c_char = crate::cstr!("vkInitializePerformanceApiINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_UNINITIALIZE_PERFORMANCE_API_INTEL: *const std::os::raw::c_char = crate::cstr!("vkUninitializePerformanceApiINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_PERFORMANCE_MARKER_INTEL: *const std::os::raw::c_char = crate::cstr!("vkCmdSetPerformanceMarkerINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_PERFORMANCE_STREAM_MARKER_INTEL: *const std::os::raw::c_char = crate::cstr!("vkCmdSetPerformanceStreamMarkerINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_PERFORMANCE_OVERRIDE_INTEL: *const std::os::raw::c_char = crate::cstr!("vkCmdSetPerformanceOverrideINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_ACQUIRE_PERFORMANCE_CONFIGURATION_INTEL: *const std::os::raw::c_char = crate::cstr!("vkAcquirePerformanceConfigurationINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_RELEASE_PERFORMANCE_CONFIGURATION_INTEL: *const std::os::raw::c_char = crate::cstr!("vkReleasePerformanceConfigurationINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_QUEUE_SET_PERFORMANCE_CONFIGURATION_INTEL: *const std::os::raw::c_char = crate::cstr!("vkQueueSetPerformanceConfigurationINTEL");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PERFORMANCE_PARAMETER_INTEL: *const std::os::raw::c_char = crate::cstr!("vkGetPerformanceParameterINTEL");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolCreateInfoINTEL.html)) · Alias <br/> VkQueryPoolPerformanceQueryCreateInfoINTEL - Structure specifying parameters to create a pool of performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkQueryPoolPerformanceQueryCreateInfoINTEL {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkQueryPoolSamplingModeINTEL    performanceCountersSampling;\n} VkQueryPoolPerformanceQueryCreateInfoINTEL;\n```\n[](#_members)Members\n----------\n\nTo create a pool for Intel performance queries, set[`crate::vk::QueryPoolCreateInfo::query_type`] to[`crate::vk::QueryType::PERFORMANCE_QUERY_INTEL`] and add a[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure to the`pNext` chain of the [`crate::vk::QueryPoolCreateInfo`] structure.\n[](#_description)Description\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `performanceCountersSampling` describe how performance queries\n  should be captured.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL`]\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-performanceCountersSampling-parameter  \n  `performanceCountersSampling` **must** be a valid [`crate::vk::QueryPoolSamplingModeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueryPoolSamplingModeINTEL`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkQueryPoolCreateInfoINTEL")]
#[allow(non_camel_case_types)]
pub type QueryPoolCreateInfoINTEL = crate::extensions::intel_performance_query::QueryPoolPerformanceQueryCreateInfoINTEL;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolCreateInfoINTEL.html)) · Alias <br/> VkQueryPoolPerformanceQueryCreateInfoINTEL - Structure specifying parameters to create a pool of performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkQueryPoolPerformanceQueryCreateInfoINTEL {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkQueryPoolSamplingModeINTEL    performanceCountersSampling;\n} VkQueryPoolPerformanceQueryCreateInfoINTEL;\n```\n[](#_members)Members\n----------\n\nTo create a pool for Intel performance queries, set[`crate::vk::QueryPoolCreateInfo::query_type`] to[`crate::vk::QueryType::PERFORMANCE_QUERY_INTEL`] and add a[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure to the`pNext` chain of the [`crate::vk::QueryPoolCreateInfo`] structure.\n[](#_description)Description\n----------\n\n* `sType` is the type of this structure.\n\n* `pNext` is `NULL` or a pointer to a structure extending this\n  structure.\n\n* `performanceCountersSampling` describe how performance queries\n  should be captured.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-sType-sType  \n  `sType` **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL`]\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-performanceCountersSampling-parameter  \n  `performanceCountersSampling` **must** be a valid [`crate::vk::QueryPoolSamplingModeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueryPoolSamplingModeINTEL`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkQueryPoolCreateInfoINTEL")]
#[allow(non_camel_case_types)]
pub type QueryPoolCreateInfoINTELBuilder<'a> = crate::extensions::intel_performance_query::QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a>;
crate::non_dispatchable_handle!(PerformanceConfigurationINTEL, PERFORMANCE_CONFIGURATION_INTEL, "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceConfigurationINTEL.html)) · Non-dispatchable Handle <br/> VkPerformanceConfigurationINTEL - Device configuration for performance queries\n[](#_c_specification)C Specification\n----------\n\nBefore submitting command buffers containing performance queries commands to\na device queue, the application must acquire and set a performance query\nconfiguration.\nThe configuration can be released once all command buffers containing\nperformance query commands are not in a pending state.\n\n```\n// Provided by VK_INTEL_performance_query\nVK_DEFINE_NON_DISPATCHABLE_HANDLE(VkPerformanceConfigurationINTEL)\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::acquire_performance_configuration_intel`], [`crate::vk::DeviceLoader::queue_set_performance_configuration_intel`], [`crate::vk::DeviceLoader::release_performance_configuration_intel`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPerformanceConfigurationINTEL)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n", "VkPerformanceConfigurationINTEL");
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::vk1_0::QueryType {
    pub const PERFORMANCE_QUERY_INTEL: Self = Self(1000210000);
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::vk1_0::StructureType {
    pub const QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL: Self = Self(1000210000);
    pub const INITIALIZE_PERFORMANCE_API_INFO_INTEL: Self = Self(1000210001);
    pub const PERFORMANCE_MARKER_INFO_INTEL: Self = Self(1000210002);
    pub const PERFORMANCE_STREAM_MARKER_INFO_INTEL: Self = Self(1000210003);
    pub const PERFORMANCE_OVERRIDE_INFO_INTEL: Self = Self(1000210004);
    pub const PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL: Self = Self(1000210005);
    pub const QUERY_POOL_CREATE_INFO_INTEL: Self = Self::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL;
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::vk1_0::ObjectType {
    pub const PERFORMANCE_CONFIGURATION_INTEL: Self = Self(1000210000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceConfigurationTypeINTEL.html)) · Enum <br/> VkPerformanceConfigurationTypeINTEL - Type of performance configuration\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PerformanceConfigurationAcquireInfoINTEL::_type`], specifying\nperformance configuration types, are:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef enum VkPerformanceConfigurationTypeINTEL {\n    VK_PERFORMANCE_CONFIGURATION_TYPE_COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL = 0,\n} VkPerformanceConfigurationTypeINTEL;\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceConfigurationAcquireInfoINTEL`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPerformanceConfigurationTypeINTEL)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n"]
#[doc(alias = "VkPerformanceConfigurationTypeINTEL")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceConfigurationTypeINTEL(pub i32);
impl std::fmt::Debug for PerformanceConfigurationTypeINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL => "COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::extensions::intel_performance_query::PerformanceConfigurationTypeINTEL {
    pub const COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL: Self = Self(0);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolSamplingModeINTEL.html)) · Enum <br/> VkQueryPoolSamplingModeINTEL - Enum specifying how performance queries should be captured\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL::performance_counters_sampling`]are:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef enum VkQueryPoolSamplingModeINTEL {\n    VK_QUERY_POOL_SAMPLING_MODE_MANUAL_INTEL = 0,\n} VkQueryPoolSamplingModeINTEL;\n```\n[](#_description)Description\n----------\n\n* [`Self::MANUAL_INTEL`] is the default mode in\n  which the application calls [`crate::vk::PFN_vkCmdBeginQuery`] and[`crate::vk::PFN_vkCmdEndQuery`] to record performance data.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`]\n"]
#[doc(alias = "VkQueryPoolSamplingModeINTEL")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct QueryPoolSamplingModeINTEL(pub i32);
impl std::fmt::Debug for QueryPoolSamplingModeINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::MANUAL_INTEL => "MANUAL_INTEL",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::extensions::intel_performance_query::QueryPoolSamplingModeINTEL {
    pub const MANUAL_INTEL: Self = Self(0);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceOverrideTypeINTEL.html)) · Enum <br/> VkPerformanceOverrideTypeINTEL - Performance override type\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::PerformanceOverrideInfoINTEL::_type`],\nspecifying performance override types, are:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef enum VkPerformanceOverrideTypeINTEL {\n    VK_PERFORMANCE_OVERRIDE_TYPE_NULL_HARDWARE_INTEL = 0,\n    VK_PERFORMANCE_OVERRIDE_TYPE_FLUSH_GPU_CACHES_INTEL = 1,\n} VkPerformanceOverrideTypeINTEL;\n```\n[](#_description)Description\n----------\n\n* [`Self::NULL_HARDWARE_INTEL`] turns all\n  rendering operations into noop.\n\n* [`Self::FLUSH_GPU_CACHES_INTEL`] stalls the\n  stream of commands until all previously emitted commands have completed\n  and all caches been flushed and invalidated.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceOverrideInfoINTEL`]\n"]
#[doc(alias = "VkPerformanceOverrideTypeINTEL")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceOverrideTypeINTEL(pub i32);
impl std::fmt::Debug for PerformanceOverrideTypeINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::NULL_HARDWARE_INTEL => "NULL_HARDWARE_INTEL",
            &Self::FLUSH_GPU_CACHES_INTEL => "FLUSH_GPU_CACHES_INTEL",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::extensions::intel_performance_query::PerformanceOverrideTypeINTEL {
    pub const NULL_HARDWARE_INTEL: Self = Self(0);
    pub const FLUSH_GPU_CACHES_INTEL: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceParameterTypeINTEL.html)) · Enum <br/> VkPerformanceParameterTypeINTEL - Parameters that can be queried\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::DeviceLoader::get_performance_parameter_intel::parameter`],\nspecifying a performance query feature, are:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef enum VkPerformanceParameterTypeINTEL {\n    VK_PERFORMANCE_PARAMETER_TYPE_HW_COUNTERS_SUPPORTED_INTEL = 0,\n    VK_PERFORMANCE_PARAMETER_TYPE_STREAM_MARKER_VALID_BITS_INTEL = 1,\n} VkPerformanceParameterTypeINTEL;\n```\n[](#_description)Description\n----------\n\n* [`Self::HW_COUNTERS_SUPPORTED_INTEL`] has a\n  boolean result which tells whether hardware counters can be captured.\n\n* [`Self::STREAM_MARKER_VALIDS_INTEL`] has a\n  32 bits integer result which tells how many bits can be written into the[`crate::vk::PerformanceValueINTEL`] value.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceLoader::get_performance_parameter_intel`]\n"]
#[doc(alias = "VkPerformanceParameterTypeINTEL")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceParameterTypeINTEL(pub i32);
impl std::fmt::Debug for PerformanceParameterTypeINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::HW_COUNTERS_SUPPORTED_INTEL => "HW_COUNTERS_SUPPORTED_INTEL",
            &Self::STREAM_MARKER_VALIDS_INTEL => "STREAM_MARKER_VALIDS_INTEL",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::extensions::intel_performance_query::PerformanceParameterTypeINTEL {
    pub const HW_COUNTERS_SUPPORTED_INTEL: Self = Self(0);
    pub const STREAM_MARKER_VALIDS_INTEL: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceValueTypeINTEL.html)) · Enum <br/> VkPerformanceValueTypeINTEL - Type of the parameters that can be queried\n[](#_c_specification)C Specification\n----------\n\nPossible values of [`crate::vk::PerformanceValueINTEL::_type`], specifying the\ntype of the data returned in [`crate::vk::PerformanceValueINTEL::data`], are:\n\n* [`Self::UINT32_INTEL`] specifies that unsigned\n  32-bit integer data is returned in `data.value32`.\n\n* [`Self::UINT64_INTEL`] specifies that unsigned\n  64-bit integer data is returned in `data.value64`.\n\n* [`Self::FLOAT_INTEL`] specifies that\n  floating-point data is returned in `data.valueFloat`.\n\n* [`Self::BOOL_INTEL`] specifies that`Bool32` data is returned in `data.valueBool`.\n\n* [`Self::STRING_INTEL`] specifies that a pointer to\n  a null-terminated UTF-8 string is returned in `data.valueString`.\n  The pointer is valid for the lifetime of the `device` parameter\n  passed to [`crate::vk::DeviceLoader::get_performance_parameter_intel`].\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef enum VkPerformanceValueTypeINTEL {\n    VK_PERFORMANCE_VALUE_TYPE_UINT32_INTEL = 0,\n    VK_PERFORMANCE_VALUE_TYPE_UINT64_INTEL = 1,\n    VK_PERFORMANCE_VALUE_TYPE_FLOAT_INTEL = 2,\n    VK_PERFORMANCE_VALUE_TYPE_BOOL_INTEL = 3,\n    VK_PERFORMANCE_VALUE_TYPE_STRING_INTEL = 4,\n} VkPerformanceValueTypeINTEL;\n```\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceValueINTEL`]\n[](#_document_notes)Document Notes\n----------\n\nFor more information, see the [Vulkan Specification](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPerformanceValueTypeINTEL)\n\nThis page is extracted from the Vulkan Specification.\nFixes and changes should be made to the Specification, not directly.\n[](#_copyright)Copyright\n----------\n\nCopyright 2014-2021 The Khronos Group Inc.\n\nSPDX-License-Identifier: CC-BY-4.0\n"]
#[doc(alias = "VkPerformanceValueTypeINTEL")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PerformanceValueTypeINTEL(pub i32);
impl std::fmt::Debug for PerformanceValueTypeINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::UINT32_INTEL => "UINT32_INTEL",
            &Self::UINT64_INTEL => "UINT64_INTEL",
            &Self::FLOAT_INTEL => "FLOAT_INTEL",
            &Self::BOOL_INTEL => "BOOL_INTEL",
            &Self::STRING_INTEL => "STRING_INTEL",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::extensions::intel_performance_query::PerformanceValueTypeINTEL {
    pub const UINT32_INTEL: Self = Self(0);
    pub const UINT64_INTEL: Self = Self(1);
    pub const FLOAT_INTEL: Self = Self(2);
    pub const BOOL_INTEL: Self = Self(3);
    pub const STRING_INTEL: Self = Self(4);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkInitializePerformanceApiINTEL.html)) · Function <br/> vkInitializePerformanceApiINTEL - Initialize a device for performance queries\n[](#_c_specification)C Specification\n----------\n\nPrior to creating a performance query pool, initialize the device for\nperformance queries with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkInitializePerformanceApiINTEL(\n    VkDevice                                    device,\n    const VkInitializePerformanceApiInfoINTEL*  pInitializeInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used for the queries.\n\n* [`Self::p_initialize_info`] is a pointer to a[`crate::vk::InitializePerformanceApiInfoINTEL`] structure specifying\n  initialization parameters.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkInitializePerformanceApiINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkInitializePerformanceApiINTEL-pInitializeInfo-parameter  \n  [`Self::p_initialize_info`] **must** be a valid pointer to a valid [`crate::vk::InitializePerformanceApiInfoINTEL`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::InitializePerformanceApiInfoINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkInitializePerformanceApiINTEL = unsafe extern "system" fn(device: crate::vk1_0::Device, p_initialize_info: *const crate::extensions::intel_performance_query::InitializePerformanceApiInfoINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkUninitializePerformanceApiINTEL.html)) · Function <br/> vkUninitializePerformanceApiINTEL - Uninitialize a device for performance queries\n[](#_c_specification)C Specification\n----------\n\nOnce performance query operations have completed, uninitalize the device for\nperformance queries with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nvoid vkUninitializePerformanceApiINTEL(\n    VkDevice                                    device);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used for the queries.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkUninitializePerformanceApiINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkUninitializePerformanceApiINTEL = unsafe extern "system" fn(device: crate::vk1_0::Device) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceMarkerINTEL.html)) · Function <br/> vkCmdSetPerformanceMarkerINTEL - Markers\n[](#_c_specification)C Specification\n----------\n\nTo help associate query results with a particular point at which an\napplication emitted commands, markers can be set into the command buffers\nwith the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceMarkerINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceMarkerInfoINTEL*         pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\nThe last marker set onto a command buffer before the end of a query will be\npart of the query result.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceMarkerInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceMarkerInfoINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetPerformanceMarkerINTEL = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_marker_info: *const crate::extensions::intel_performance_query::PerformanceMarkerInfoINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceStreamMarkerINTEL.html)) · Function <br/> vkCmdSetPerformanceStreamMarkerINTEL - Markers\n[](#_c_specification)C Specification\n----------\n\nWhen monitoring the behavior of an application wihtin the dataset generated\nby the entire set of applications running on the system, it is useful to\nidentify draw calls within a potentially huge amount of performance data.\nTo do so, application can generate stream markers that will be used to trace\nback a particular draw call with a particular performance data item.\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceStreamMarkerINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceStreamMarkerInfoINTEL*   pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceStreamMarkerInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceStreamMarkerInfoINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetPerformanceStreamMarkerINTEL = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_marker_info: *const crate::extensions::intel_performance_query::PerformanceStreamMarkerInfoINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceOverrideINTEL.html)) · Function <br/> vkCmdSetPerformanceOverrideINTEL - Performance override settings\n[](#_c_specification)C Specification\n----------\n\nSome applications might want measure the effect of a set of commands with a\ndifferent settings.\nIt is possible to override a particular settings using :\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceOverrideINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceOverrideInfoINTEL*       pOverrideInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer where the override takes\n  place.\n\n* [`Self::p_override_info`] is a pointer to a[`crate::vk::PerformanceOverrideInfoINTEL`] structure selecting the parameter\n  to override.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-pOverrideInfo-02736  \n  [`Self::p_override_info`] **must** not be used with a[`crate::vk::PerformanceOverrideTypeINTEL`] that is not reported available by[`crate::vk::DeviceLoader::get_performance_parameter_intel`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-pOverrideInfo-parameter  \n  [`Self::p_override_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceOverrideInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceOverrideInfoINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetPerformanceOverrideINTEL = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_override_info: *const crate::extensions::intel_performance_query::PerformanceOverrideInfoINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquirePerformanceConfigurationINTEL.html)) · Function <br/> vkAcquirePerformanceConfigurationINTEL - Acquire the performance query capability\n[](#_c_specification)C Specification\n----------\n\nTo acquire a device performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkAcquirePerformanceConfigurationINTEL(\n    VkDevice                                    device,\n    const VkPerformanceConfigurationAcquireInfoINTEL* pAcquireInfo,\n    VkPerformanceConfigurationINTEL*            pConfiguration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that the performance query commands\n  will be submitted to.\n\n* [`Self::p_acquire_info`] is a pointer to a[`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure, specifying\n  the performance configuration to acquire.\n\n* [`Self::p_configuration`] is a pointer to a[`crate::vk::PerformanceConfigurationINTEL`] handle in which the resulting\n  configuration object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-pAcquireInfo-parameter  \n  [`Self::p_acquire_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-pConfiguration-parameter  \n  [`Self::p_configuration`] **must** be a valid pointer to a [`crate::vk::PerformanceConfigurationINTEL`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`], [`crate::vk::PerformanceConfigurationINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkAcquirePerformanceConfigurationINTEL = unsafe extern "system" fn(device: crate::vk1_0::Device, p_acquire_info: *const crate::extensions::intel_performance_query::PerformanceConfigurationAcquireInfoINTEL, p_configuration: *mut crate::extensions::intel_performance_query::PerformanceConfigurationINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleasePerformanceConfigurationINTEL.html)) · Function <br/> vkReleasePerformanceConfigurationINTEL - Release a configuration to capture performance data\n[](#_c_specification)C Specification\n----------\n\nTo release a device performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkReleasePerformanceConfigurationINTEL(\n    VkDevice                                    device,\n    VkPerformanceConfigurationINTEL             configuration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated to the configuration object to\n  release.\n\n* [`Self::configuration`] is the configuration object to release.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-02737  \n  [`Self::configuration`] **must** not be released before all command buffers\n  submitted while the configuration was set are in[pending state](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#commandbuffers-lifecycle)\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-parameter  \n   If [`Self::configuration`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::configuration`] **must** be a valid [`crate::vk::PerformanceConfigurationINTEL`] handle\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-parent  \n   If [`Self::configuration`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::configuration`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceConfigurationINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkReleasePerformanceConfigurationINTEL = unsafe extern "system" fn(device: crate::vk1_0::Device, configuration: crate::extensions::intel_performance_query::PerformanceConfigurationINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueSetPerformanceConfigurationINTEL.html)) · Function <br/> vkQueueSetPerformanceConfigurationINTEL - Set a performance query\n[](#_c_specification)C Specification\n----------\n\nTo set a performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkQueueSetPerformanceConfigurationINTEL(\n    VkQueue                                     queue,\n    VkPerformanceConfigurationINTEL             configuration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue on which the configuration will be used.\n\n* [`Self::configuration`] is the configuration to use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-configuration-parameter  \n  [`Self::configuration`] **must** be a valid [`crate::vk::PerformanceConfigurationINTEL`] handle\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-commonparent  \n   Both of [`Self::configuration`], and [`Self::queue`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceConfigurationINTEL`], [`crate::vk::Queue`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkQueueSetPerformanceConfigurationINTEL = unsafe extern "system" fn(queue: crate::vk1_0::Queue, configuration: crate::extensions::intel_performance_query::PerformanceConfigurationINTEL) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPerformanceParameterINTEL.html)) · Function <br/> vkGetPerformanceParameterINTEL - Query performance capabilities of the device\n[](#_c_specification)C Specification\n----------\n\nSome performance query features of a device can be discovered with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkGetPerformanceParameterINTEL(\n    VkDevice                                    device,\n    VkPerformanceParameterTypeINTEL             parameter,\n    VkPerformanceValueINTEL*                    pValue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to query.\n\n* [`Self::parameter`] is the parameter to query.\n\n* [`Self::p_value`] is a pointer to a [`crate::vk::PerformanceValueINTEL`] structure\n  in which the type and value of the parameter are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPerformanceParameterINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPerformanceParameterINTEL-parameter-parameter  \n  [`Self::parameter`] **must** be a valid [`crate::vk::PerformanceParameterTypeINTEL`] value\n\n* []() VUID-vkGetPerformanceParameterINTEL-pValue-parameter  \n  [`Self::p_value`] **must** be a valid pointer to a [`crate::vk::PerformanceValueINTEL`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceParameterTypeINTEL`], [`crate::vk::PerformanceValueINTEL`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPerformanceParameterINTEL = unsafe extern "system" fn(device: crate::vk1_0::Device, parameter: crate::extensions::intel_performance_query::PerformanceParameterTypeINTEL, p_value: *mut crate::extensions::intel_performance_query::PerformanceValueINTEL) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, QueryPoolPerformanceQueryCreateInfoINTEL> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, QueryPoolPerformanceQueryCreateInfoINTELBuilder<'_>> for crate::vk1_0::QueryPoolCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceValueDataINTEL.html)) · Structure <br/> VkPerformanceValueDataINTEL - Values returned for the parameters\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceValueDataINTEL`] union is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef union VkPerformanceValueDataINTEL {\n    uint32_t       value32;\n    uint64_t       value64;\n    float          valueFloat;\n    VkBool32       valueBool;\n    const char*    valueString;\n} VkPerformanceValueDataINTEL;\n```\n[](#_members)Members\n----------\n\n* `data.value32` represents 32-bit integer data.\n\n* `data.value64` represents 64-bit integer data.\n\n* `data.valueFloat` represents floating-point data.\n\n* `data.valueBool` represents `Bool32` data.\n\n* `data.valueString` represents a pointer to a null-terminated UTF-8\n  string.\n[](#_description)Description\n----------\n\nThe correct member of the union is determined by the associated[`crate::vk::PerformanceValueTypeINTEL`] value.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::PerformanceValueINTEL`]\n"]
#[doc(alias = "VkPerformanceValueDataINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub union PerformanceValueDataINTEL {
    pub value32: u32,
    pub value64: u64,
    pub value_float: std::os::raw::c_float,
    pub value_bool: crate::vk1_0::Bool32,
    pub value_string: *const std::os::raw::c_char,
}
impl Default for PerformanceValueDataINTEL {
    fn default() -> Self {
        unsafe { std::mem::zeroed() }
    }
}
impl std::fmt::Debug for PerformanceValueDataINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceValueDataINTEL").finish()
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceValueINTEL.html)) · Structure <br/> VkPerformanceValueINTEL - Container for value and types of parameters that can be queried\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceValueINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceValueINTEL {\n    VkPerformanceValueTypeINTEL    type;\n    VkPerformanceValueDataINTEL    data;\n} VkPerformanceValueINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is a [`crate::vk::PerformanceValueTypeINTEL`] value specifying the\n  type of the returned data.\n\n* [`Self::data`] is a [`crate::vk::PerformanceValueDataINTEL`] union specifying the\n  value of the returned data.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceValueINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceValueTypeINTEL`] value\n\n* []() VUID-VkPerformanceValueINTEL-valueString-parameter  \n   If [`Self::_type`] is [`crate::vk::PerformanceValueTypeINTEL::STRING_INTEL`], the `valueString` member of [`Self::data`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceValueDataINTEL`], [`crate::vk::PerformanceValueTypeINTEL`], [`crate::vk::DeviceLoader::get_performance_parameter_intel`]\n"]
#[doc(alias = "VkPerformanceValueINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceValueINTEL {
    pub _type: crate::extensions::intel_performance_query::PerformanceValueTypeINTEL,
    pub data: crate::extensions::intel_performance_query::PerformanceValueDataINTEL,
}
impl Default for PerformanceValueINTEL {
    fn default() -> Self {
        Self { _type: Default::default(), data: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceValueINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceValueINTEL").field("_type", &self._type).field("data", &self.data).finish()
    }
}
impl PerformanceValueINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceValueINTELBuilder<'a> {
        PerformanceValueINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceValueINTEL.html)) · Builder of [`PerformanceValueINTEL`] <br/> VkPerformanceValueINTEL - Container for value and types of parameters that can be queried\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceValueINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceValueINTEL {\n    VkPerformanceValueTypeINTEL    type;\n    VkPerformanceValueDataINTEL    data;\n} VkPerformanceValueINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is a [`crate::vk::PerformanceValueTypeINTEL`] value specifying the\n  type of the returned data.\n\n* [`Self::data`] is a [`crate::vk::PerformanceValueDataINTEL`] union specifying the\n  value of the returned data.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceValueINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceValueTypeINTEL`] value\n\n* []() VUID-VkPerformanceValueINTEL-valueString-parameter  \n   If [`Self::_type`] is [`crate::vk::PerformanceValueTypeINTEL::STRING_INTEL`], the `valueString` member of [`Self::data`] **must** be a null-terminated UTF-8 string\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceValueDataINTEL`], [`crate::vk::PerformanceValueTypeINTEL`], [`crate::vk::DeviceLoader::get_performance_parameter_intel`]\n"]
#[repr(transparent)]
pub struct PerformanceValueINTELBuilder<'a>(PerformanceValueINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceValueINTELBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceValueINTELBuilder<'a> {
        PerformanceValueINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(mut self, _type: crate::extensions::intel_performance_query::PerformanceValueTypeINTEL) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn data(mut self, data: crate::extensions::intel_performance_query::PerformanceValueDataINTEL) -> Self {
        self.0.data = data as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceValueINTEL {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceValueINTELBuilder<'a> {
    fn default() -> PerformanceValueINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceValueINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceValueINTELBuilder<'a> {
    type Target = PerformanceValueINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceValueINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkInitializePerformanceApiInfoINTEL.html)) · Structure <br/> VkInitializePerformanceApiInfoINTEL - Structure specifying parameters of initialize of the device\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::InitializePerformanceApiInfoINTEL`] structure is defined as :\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkInitializePerformanceApiInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    void*              pUserData;\n} VkInitializePerformanceApiInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_user_data`] is a pointer for application data.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkInitializePerformanceApiInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INITIALIZE_PERFORMANCE_API_INFO_INTEL`]\n\n* []() VUID-VkInitializePerformanceApiInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::initialize_performance_api_intel`]\n"]
#[doc(alias = "VkInitializePerformanceApiInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct InitializePerformanceApiInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub p_user_data: *mut std::ffi::c_void,
}
impl InitializePerformanceApiInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::INITIALIZE_PERFORMANCE_API_INFO_INTEL;
}
impl Default for InitializePerformanceApiInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), p_user_data: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for InitializePerformanceApiInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("InitializePerformanceApiInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("p_user_data", &self.p_user_data).finish()
    }
}
impl InitializePerformanceApiInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> InitializePerformanceApiInfoINTELBuilder<'a> {
        InitializePerformanceApiInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkInitializePerformanceApiInfoINTEL.html)) · Builder of [`InitializePerformanceApiInfoINTEL`] <br/> VkInitializePerformanceApiInfoINTEL - Structure specifying parameters of initialize of the device\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::InitializePerformanceApiInfoINTEL`] structure is defined as :\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkInitializePerformanceApiInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    void*              pUserData;\n} VkInitializePerformanceApiInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::p_user_data`] is a pointer for application data.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkInitializePerformanceApiInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::INITIALIZE_PERFORMANCE_API_INFO_INTEL`]\n\n* []() VUID-VkInitializePerformanceApiInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::initialize_performance_api_intel`]\n"]
#[repr(transparent)]
pub struct InitializePerformanceApiInfoINTELBuilder<'a>(InitializePerformanceApiInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> InitializePerformanceApiInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> InitializePerformanceApiInfoINTELBuilder<'a> {
        InitializePerformanceApiInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn user_data(mut self, user_data: *mut std::ffi::c_void) -> Self {
        self.0.p_user_data = user_data;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> InitializePerformanceApiInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for InitializePerformanceApiInfoINTELBuilder<'a> {
    fn default() -> InitializePerformanceApiInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for InitializePerformanceApiInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for InitializePerformanceApiInfoINTELBuilder<'a> {
    type Target = InitializePerformanceApiInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for InitializePerformanceApiInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolPerformanceQueryCreateInfoINTEL.html)) · Structure <br/> VkQueryPoolPerformanceQueryCreateInfoINTEL - Structure specifying parameters to create a pool of performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkQueryPoolPerformanceQueryCreateInfoINTEL {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkQueryPoolSamplingModeINTEL    performanceCountersSampling;\n} VkQueryPoolPerformanceQueryCreateInfoINTEL;\n```\n[](#_members)Members\n----------\n\nTo create a pool for Intel performance queries, set[`crate::vk::QueryPoolCreateInfo::query_type`] to[`crate::vk::QueryType::PERFORMANCE_QUERY_INTEL`] and add a[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure to the[`Self::p_next`] chain of the [`crate::vk::QueryPoolCreateInfo`] structure.\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::performance_counters_sampling`] describe how performance queries\n  should be captured.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL`]\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-performanceCountersSampling-parameter  \n  [`Self::performance_counters_sampling`] **must** be a valid [`crate::vk::QueryPoolSamplingModeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueryPoolSamplingModeINTEL`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkQueryPoolPerformanceQueryCreateInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct QueryPoolPerformanceQueryCreateInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub performance_counters_sampling: crate::extensions::intel_performance_query::QueryPoolSamplingModeINTEL,
}
impl QueryPoolPerformanceQueryCreateInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL;
}
impl Default for QueryPoolPerformanceQueryCreateInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), performance_counters_sampling: Default::default() }
    }
}
impl std::fmt::Debug for QueryPoolPerformanceQueryCreateInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("QueryPoolPerformanceQueryCreateInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("performance_counters_sampling", &self.performance_counters_sampling).finish()
    }
}
impl QueryPoolPerformanceQueryCreateInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
        QueryPoolPerformanceQueryCreateInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkQueryPoolPerformanceQueryCreateInfoINTEL.html)) · Builder of [`QueryPoolPerformanceQueryCreateInfoINTEL`] <br/> VkQueryPoolPerformanceQueryCreateInfoINTEL - Structure specifying parameters to create a pool of performance queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkQueryPoolPerformanceQueryCreateInfoINTEL {\n    VkStructureType                 sType;\n    const void*                     pNext;\n    VkQueryPoolSamplingModeINTEL    performanceCountersSampling;\n} VkQueryPoolPerformanceQueryCreateInfoINTEL;\n```\n[](#_members)Members\n----------\n\nTo create a pool for Intel performance queries, set[`crate::vk::QueryPoolCreateInfo::query_type`] to[`crate::vk::QueryType::PERFORMANCE_QUERY_INTEL`] and add a[`crate::vk::QueryPoolPerformanceQueryCreateInfoINTEL`] structure to the[`Self::p_next`] chain of the [`crate::vk::QueryPoolCreateInfo`] structure.\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::performance_counters_sampling`] describe how performance queries\n  should be captured.\n\nValid Usage (Implicit)\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL`]\n\n* []() VUID-VkQueryPoolPerformanceQueryCreateInfoINTEL-performanceCountersSampling-parameter  \n  [`Self::performance_counters_sampling`] **must** be a valid [`crate::vk::QueryPoolSamplingModeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::QueryPoolSamplingModeINTEL`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a>(QueryPoolPerformanceQueryCreateInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
        QueryPoolPerformanceQueryCreateInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn performance_counters_sampling(mut self, performance_counters_sampling: crate::extensions::intel_performance_query::QueryPoolSamplingModeINTEL) -> Self {
        self.0.performance_counters_sampling = performance_counters_sampling as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> QueryPoolPerformanceQueryCreateInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
    fn default() -> QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
    type Target = QueryPoolPerformanceQueryCreateInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for QueryPoolPerformanceQueryCreateInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceMarkerInfoINTEL.html)) · Structure <br/> VkPerformanceMarkerInfoINTEL - Structure specifying performance markers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceMarkerInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceMarkerInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint64_t           marker;\n} VkPerformanceMarkerInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::marker`] is the marker value that will be recorded into the opaque\n  query results.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceMarkerInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_MARKER_INFO_INTEL`]\n\n* []() VUID-VkPerformanceMarkerInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_marker_intel`]\n"]
#[doc(alias = "VkPerformanceMarkerInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceMarkerInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub marker: u64,
}
impl PerformanceMarkerInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_MARKER_INFO_INTEL;
}
impl Default for PerformanceMarkerInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), marker: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceMarkerInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceMarkerInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("marker", &self.marker).finish()
    }
}
impl PerformanceMarkerInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceMarkerInfoINTELBuilder<'a> {
        PerformanceMarkerInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceMarkerInfoINTEL.html)) · Builder of [`PerformanceMarkerInfoINTEL`] <br/> VkPerformanceMarkerInfoINTEL - Structure specifying performance markers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceMarkerInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceMarkerInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint64_t           marker;\n} VkPerformanceMarkerInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::marker`] is the marker value that will be recorded into the opaque\n  query results.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceMarkerInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_MARKER_INFO_INTEL`]\n\n* []() VUID-VkPerformanceMarkerInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_marker_intel`]\n"]
#[repr(transparent)]
pub struct PerformanceMarkerInfoINTELBuilder<'a>(PerformanceMarkerInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceMarkerInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceMarkerInfoINTELBuilder<'a> {
        PerformanceMarkerInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn marker(mut self, marker: u64) -> Self {
        self.0.marker = marker as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceMarkerInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceMarkerInfoINTELBuilder<'a> {
    fn default() -> PerformanceMarkerInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceMarkerInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceMarkerInfoINTELBuilder<'a> {
    type Target = PerformanceMarkerInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceMarkerInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceStreamMarkerInfoINTEL.html)) · Structure <br/> VkPerformanceStreamMarkerInfoINTEL - Structure specifying stream performance markers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceStreamMarkerInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceStreamMarkerInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           marker;\n} VkPerformanceStreamMarkerInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::marker`] is the marker value that will be recorded into the reports\n  consumed by an external application.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-marker-02735  \n   The value written by the application into [`Self::marker`] **must** only used\n  the valid bits as reported by [`crate::vk::DeviceLoader::get_performance_parameter_intel`] with\n  the [`crate::vk::PerformanceParameterTypeINTEL::STREAM_MARKER_VALIDS_INTEL`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_STREAM_MARKER_INFO_INTEL`]\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_stream_marker_intel`]\n"]
#[doc(alias = "VkPerformanceStreamMarkerInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceStreamMarkerInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub marker: u32,
}
impl PerformanceStreamMarkerInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_STREAM_MARKER_INFO_INTEL;
}
impl Default for PerformanceStreamMarkerInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), marker: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceStreamMarkerInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceStreamMarkerInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("marker", &self.marker).finish()
    }
}
impl PerformanceStreamMarkerInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceStreamMarkerInfoINTELBuilder<'a> {
        PerformanceStreamMarkerInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceStreamMarkerInfoINTEL.html)) · Builder of [`PerformanceStreamMarkerInfoINTEL`] <br/> VkPerformanceStreamMarkerInfoINTEL - Structure specifying stream performance markers\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceStreamMarkerInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceStreamMarkerInfoINTEL {\n    VkStructureType    sType;\n    const void*        pNext;\n    uint32_t           marker;\n} VkPerformanceStreamMarkerInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::marker`] is the marker value that will be recorded into the reports\n  consumed by an external application.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-marker-02735  \n   The value written by the application into [`Self::marker`] **must** only used\n  the valid bits as reported by [`crate::vk::DeviceLoader::get_performance_parameter_intel`] with\n  the [`crate::vk::PerformanceParameterTypeINTEL::STREAM_MARKER_VALIDS_INTEL`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_STREAM_MARKER_INFO_INTEL`]\n\n* []() VUID-VkPerformanceStreamMarkerInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_stream_marker_intel`]\n"]
#[repr(transparent)]
pub struct PerformanceStreamMarkerInfoINTELBuilder<'a>(PerformanceStreamMarkerInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceStreamMarkerInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceStreamMarkerInfoINTELBuilder<'a> {
        PerformanceStreamMarkerInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn marker(mut self, marker: u32) -> Self {
        self.0.marker = marker as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceStreamMarkerInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceStreamMarkerInfoINTELBuilder<'a> {
    fn default() -> PerformanceStreamMarkerInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceStreamMarkerInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceStreamMarkerInfoINTELBuilder<'a> {
    type Target = PerformanceStreamMarkerInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceStreamMarkerInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceOverrideInfoINTEL.html)) · Structure <br/> VkPerformanceOverrideInfoINTEL - Performance override information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceOverrideInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceOverrideInfoINTEL {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkPerformanceOverrideTypeINTEL    type;\n    VkBool32                          enable;\n    uint64_t                          parameter;\n} VkPerformanceOverrideInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is the particular [`crate::vk::PerformanceOverrideTypeINTEL`] to\n  set.\n\n* [`Self::enable`] defines whether the override is enabled.\n\n* [`Self::parameter`] is a potential required parameter for the override.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_OVERRIDE_INFO_INTEL`]\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceOverrideTypeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::PerformanceOverrideTypeINTEL`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_override_intel`]\n"]
#[doc(alias = "VkPerformanceOverrideInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceOverrideInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub _type: crate::extensions::intel_performance_query::PerformanceOverrideTypeINTEL,
    pub enable: crate::vk1_0::Bool32,
    pub parameter: u64,
}
impl PerformanceOverrideInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_OVERRIDE_INFO_INTEL;
}
impl Default for PerformanceOverrideInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), _type: Default::default(), enable: Default::default(), parameter: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceOverrideInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceOverrideInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("_type", &self._type).field("enable", &(self.enable != 0)).field("parameter", &self.parameter).finish()
    }
}
impl PerformanceOverrideInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceOverrideInfoINTELBuilder<'a> {
        PerformanceOverrideInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceOverrideInfoINTEL.html)) · Builder of [`PerformanceOverrideInfoINTEL`] <br/> VkPerformanceOverrideInfoINTEL - Performance override information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceOverrideInfoINTEL`] structure is defined as:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceOverrideInfoINTEL {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkPerformanceOverrideTypeINTEL    type;\n    VkBool32                          enable;\n    uint64_t                          parameter;\n} VkPerformanceOverrideInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::_type`] is the particular [`crate::vk::PerformanceOverrideTypeINTEL`] to\n  set.\n\n* [`Self::enable`] defines whether the override is enabled.\n\n* [`Self::parameter`] is a potential required parameter for the override.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_OVERRIDE_INFO_INTEL`]\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPerformanceOverrideInfoINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceOverrideTypeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::PerformanceOverrideTypeINTEL`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_set_performance_override_intel`]\n"]
#[repr(transparent)]
pub struct PerformanceOverrideInfoINTELBuilder<'a>(PerformanceOverrideInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceOverrideInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceOverrideInfoINTELBuilder<'a> {
        PerformanceOverrideInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(mut self, _type: crate::extensions::intel_performance_query::PerformanceOverrideTypeINTEL) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    pub fn enable(mut self, enable: bool) -> Self {
        self.0.enable = enable as _;
        self
    }
    #[inline]
    pub fn parameter(mut self, parameter: u64) -> Self {
        self.0.parameter = parameter as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceOverrideInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceOverrideInfoINTELBuilder<'a> {
    fn default() -> PerformanceOverrideInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceOverrideInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceOverrideInfoINTELBuilder<'a> {
    type Target = PerformanceOverrideInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceOverrideInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceConfigurationAcquireInfoINTEL.html)) · Structure <br/> VkPerformanceConfigurationAcquireInfoINTEL - Acquire a configuration to capture performance data\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceConfigurationAcquireInfoINTEL {\n    VkStructureType                        sType;\n    const void*                            pNext;\n    VkPerformanceConfigurationTypeINTEL    type;\n} VkPerformanceConfigurationAcquireInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::_type`] is one of the [`crate::vk::PerformanceConfigurationTypeINTEL`] type\n  of performance configuration that will be acquired.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL`]\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceConfigurationTypeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceConfigurationTypeINTEL`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::acquire_performance_configuration_intel`]\n"]
#[doc(alias = "VkPerformanceConfigurationAcquireInfoINTEL")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PerformanceConfigurationAcquireInfoINTEL {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub _type: crate::extensions::intel_performance_query::PerformanceConfigurationTypeINTEL,
}
impl PerformanceConfigurationAcquireInfoINTEL {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL;
}
impl Default for PerformanceConfigurationAcquireInfoINTEL {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), _type: Default::default() }
    }
}
impl std::fmt::Debug for PerformanceConfigurationAcquireInfoINTEL {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PerformanceConfigurationAcquireInfoINTEL").field("s_type", &self.s_type).field("p_next", &self.p_next).field("_type", &self._type).finish()
    }
}
impl PerformanceConfigurationAcquireInfoINTEL {
    #[inline]
    pub fn into_builder<'a>(self) -> PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
        PerformanceConfigurationAcquireInfoINTELBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPerformanceConfigurationAcquireInfoINTEL.html)) · Builder of [`PerformanceConfigurationAcquireInfoINTEL`] <br/> VkPerformanceConfigurationAcquireInfoINTEL - Acquire a configuration to capture performance data\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure is defined\nas:\n\n```\n// Provided by VK_INTEL_performance_query\ntypedef struct VkPerformanceConfigurationAcquireInfoINTEL {\n    VkStructureType                        sType;\n    const void*                            pNext;\n    VkPerformanceConfigurationTypeINTEL    type;\n} VkPerformanceConfigurationAcquireInfoINTEL;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::_type`] is one of the [`crate::vk::PerformanceConfigurationTypeINTEL`] type\n  of performance configuration that will be acquired.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL`]\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkPerformanceConfigurationAcquireInfoINTEL-type-parameter  \n  [`Self::_type`] **must** be a valid [`crate::vk::PerformanceConfigurationTypeINTEL`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceConfigurationTypeINTEL`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::acquire_performance_configuration_intel`]\n"]
#[repr(transparent)]
pub struct PerformanceConfigurationAcquireInfoINTELBuilder<'a>(PerformanceConfigurationAcquireInfoINTEL, std::marker::PhantomData<&'a ()>);
impl<'a> PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
    #[inline]
    pub fn new() -> PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
        PerformanceConfigurationAcquireInfoINTELBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn _type(mut self, _type: crate::extensions::intel_performance_query::PerformanceConfigurationTypeINTEL) -> Self {
        self.0._type = _type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PerformanceConfigurationAcquireInfoINTEL {
        self.0
    }
}
impl<'a> std::default::Default for PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
    fn default() -> PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
    type Target = PerformanceConfigurationAcquireInfoINTEL;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PerformanceConfigurationAcquireInfoINTELBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::intel_performance_query`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkInitializePerformanceApiINTEL.html)) · Function <br/> vkInitializePerformanceApiINTEL - Initialize a device for performance queries\n[](#_c_specification)C Specification\n----------\n\nPrior to creating a performance query pool, initialize the device for\nperformance queries with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkInitializePerformanceApiINTEL(\n    VkDevice                                    device,\n    const VkInitializePerformanceApiInfoINTEL*  pInitializeInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used for the queries.\n\n* [`Self::p_initialize_info`] is a pointer to a[`crate::vk::InitializePerformanceApiInfoINTEL`] structure specifying\n  initialization parameters.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkInitializePerformanceApiINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkInitializePerformanceApiINTEL-pInitializeInfo-parameter  \n  [`Self::p_initialize_info`] **must** be a valid pointer to a valid [`crate::vk::InitializePerformanceApiInfoINTEL`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::InitializePerformanceApiInfoINTEL`]\n"]
    #[doc(alias = "vkInitializePerformanceApiINTEL")]
    pub unsafe fn initialize_performance_api_intel(&self, initialize_info: &crate::extensions::intel_performance_query::InitializePerformanceApiInfoINTEL) -> crate::utils::VulkanResult<()> {
        let _function = self.initialize_performance_api_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, initialize_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkUninitializePerformanceApiINTEL.html)) · Function <br/> vkUninitializePerformanceApiINTEL - Uninitialize a device for performance queries\n[](#_c_specification)C Specification\n----------\n\nOnce performance query operations have completed, uninitalize the device for\nperformance queries with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nvoid vkUninitializePerformanceApiINTEL(\n    VkDevice                                    device);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device used for the queries.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkUninitializePerformanceApiINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`]\n"]
    #[doc(alias = "vkUninitializePerformanceApiINTEL")]
    pub unsafe fn uninitialize_performance_api_intel(&self) -> () {
        let _function = self.uninitialize_performance_api_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceMarkerINTEL.html)) · Function <br/> vkCmdSetPerformanceMarkerINTEL - Markers\n[](#_c_specification)C Specification\n----------\n\nTo help associate query results with a particular point at which an\napplication emitted commands, markers can be set into the command buffers\nwith the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceMarkerINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceMarkerInfoINTEL*         pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n\nThe last marker set onto a command buffer before the end of a query will be\npart of the query result.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceMarkerInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceMarkerINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceMarkerInfoINTEL`]\n"]
    #[doc(alias = "vkCmdSetPerformanceMarkerINTEL")]
    pub unsafe fn cmd_set_performance_marker_intel(&self, command_buffer: crate::vk1_0::CommandBuffer, marker_info: &crate::extensions::intel_performance_query::PerformanceMarkerInfoINTEL) -> crate::utils::VulkanResult<()> {
        let _function = self.cmd_set_performance_marker_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, marker_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceStreamMarkerINTEL.html)) · Function <br/> vkCmdSetPerformanceStreamMarkerINTEL - Markers\n[](#_c_specification)C Specification\n----------\n\nWhen monitoring the behavior of an application wihtin the dataset generated\nby the entire set of applications running on the system, it is useful to\nidentify draw calls within a potentially huge amount of performance data.\nTo do so, application can generate stream markers that will be used to trace\nback a particular draw call with a particular performance data item.\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceStreamMarkerINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceStreamMarkerInfoINTEL*   pMarkerInfo);\n```\n[](#_parameters)Parameters\n----------\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-pMarkerInfo-parameter  \n  [`Self::p_marker_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceStreamMarkerInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceStreamMarkerINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceStreamMarkerInfoINTEL`]\n"]
    #[doc(alias = "vkCmdSetPerformanceStreamMarkerINTEL")]
    pub unsafe fn cmd_set_performance_stream_marker_intel(&self, command_buffer: crate::vk1_0::CommandBuffer, marker_info: &crate::extensions::intel_performance_query::PerformanceStreamMarkerInfoINTEL) -> crate::utils::VulkanResult<()> {
        let _function = self.cmd_set_performance_stream_marker_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, marker_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetPerformanceOverrideINTEL.html)) · Function <br/> vkCmdSetPerformanceOverrideINTEL - Performance override settings\n[](#_c_specification)C Specification\n----------\n\nSome applications might want measure the effect of a set of commands with a\ndifferent settings.\nIt is possible to override a particular settings using :\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkCmdSetPerformanceOverrideINTEL(\n    VkCommandBuffer                             commandBuffer,\n    const VkPerformanceOverrideInfoINTEL*       pOverrideInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer where the override takes\n  place.\n\n* [`Self::p_override_info`] is a pointer to a[`crate::vk::PerformanceOverrideInfoINTEL`] structure selecting the parameter\n  to override.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-pOverrideInfo-02736  \n  [`Self::p_override_info`] **must** not be used with a[`crate::vk::PerformanceOverrideTypeINTEL`] that is not reported available by[`crate::vk::DeviceLoader::get_performance_parameter_intel`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-pOverrideInfo-parameter  \n  [`Self::p_override_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceOverrideInfoINTEL`] structure\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetPerformanceOverrideINTEL-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, compute, or transfer operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |  Graphics  <br/>Compute  <br/>Transfer  |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::PerformanceOverrideInfoINTEL`]\n"]
    #[doc(alias = "vkCmdSetPerformanceOverrideINTEL")]
    pub unsafe fn cmd_set_performance_override_intel(&self, command_buffer: crate::vk1_0::CommandBuffer, override_info: &crate::extensions::intel_performance_query::PerformanceOverrideInfoINTEL) -> crate::utils::VulkanResult<()> {
        let _function = self.cmd_set_performance_override_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, override_info as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkAcquirePerformanceConfigurationINTEL.html)) · Function <br/> vkAcquirePerformanceConfigurationINTEL - Acquire the performance query capability\n[](#_c_specification)C Specification\n----------\n\nTo acquire a device performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkAcquirePerformanceConfigurationINTEL(\n    VkDevice                                    device,\n    const VkPerformanceConfigurationAcquireInfoINTEL* pAcquireInfo,\n    VkPerformanceConfigurationINTEL*            pConfiguration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that the performance query commands\n  will be submitted to.\n\n* [`Self::p_acquire_info`] is a pointer to a[`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure, specifying\n  the performance configuration to acquire.\n\n* [`Self::p_configuration`] is a pointer to a[`crate::vk::PerformanceConfigurationINTEL`] handle in which the resulting\n  configuration object is returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-pAcquireInfo-parameter  \n  [`Self::p_acquire_info`] **must** be a valid pointer to a valid [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`] structure\n\n* []() VUID-vkAcquirePerformanceConfigurationINTEL-pConfiguration-parameter  \n  [`Self::p_configuration`] **must** be a valid pointer to a [`crate::vk::PerformanceConfigurationINTEL`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceConfigurationAcquireInfoINTEL`], [`crate::vk::PerformanceConfigurationINTEL`]\n"]
    #[doc(alias = "vkAcquirePerformanceConfigurationINTEL")]
    pub unsafe fn acquire_performance_configuration_intel(&self, acquire_info: &crate::extensions::intel_performance_query::PerformanceConfigurationAcquireInfoINTEL) -> crate::utils::VulkanResult<crate::extensions::intel_performance_query::PerformanceConfigurationINTEL> {
        let _function = self.acquire_performance_configuration_intel.expect(crate::NOT_LOADED_MESSAGE);
        let mut configuration = Default::default();
        let _return = _function(self.handle, acquire_info as _, &mut configuration);
        crate::utils::VulkanResult::new(_return, configuration)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkReleasePerformanceConfigurationINTEL.html)) · Function <br/> vkReleasePerformanceConfigurationINTEL - Release a configuration to capture performance data\n[](#_c_specification)C Specification\n----------\n\nTo release a device performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkReleasePerformanceConfigurationINTEL(\n    VkDevice                                    device,\n    VkPerformanceConfigurationINTEL             configuration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the device associated to the configuration object to\n  release.\n\n* [`Self::configuration`] is the configuration object to release.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-02737  \n  [`Self::configuration`] **must** not be released before all command buffers\n  submitted while the configuration was set are in[pending state](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#commandbuffers-lifecycle)\n\nValid Usage (Implicit)\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-parameter  \n   If [`Self::configuration`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::configuration`] **must** be a valid [`crate::vk::PerformanceConfigurationINTEL`] handle\n\n* []() VUID-vkReleasePerformanceConfigurationINTEL-configuration-parent  \n   If [`Self::configuration`] is a valid handle, it **must** have been created, allocated, or retrieved from [`Self::device`]\n\nHost Synchronization\n\n* Host access to [`Self::configuration`] **must** be externally synchronized\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceConfigurationINTEL`]\n"]
    #[doc(alias = "vkReleasePerformanceConfigurationINTEL")]
    pub unsafe fn release_performance_configuration_intel(&self, configuration: Option<crate::extensions::intel_performance_query::PerformanceConfigurationINTEL>) -> crate::utils::VulkanResult<()> {
        let _function = self.release_performance_configuration_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            self.handle,
            match configuration {
                Some(v) => v,
                None => Default::default(),
            },
        );
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkQueueSetPerformanceConfigurationINTEL.html)) · Function <br/> vkQueueSetPerformanceConfigurationINTEL - Set a performance query\n[](#_c_specification)C Specification\n----------\n\nTo set a performance configuration, call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkQueueSetPerformanceConfigurationINTEL(\n    VkQueue                                     queue,\n    VkPerformanceConfigurationINTEL             configuration);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::queue`] is the queue on which the configuration will be used.\n\n* [`Self::configuration`] is the configuration to use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-queue-parameter  \n  [`Self::queue`] **must** be a valid [`crate::vk::Queue`] handle\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-configuration-parameter  \n  [`Self::configuration`] **must** be a valid [`crate::vk::PerformanceConfigurationINTEL`] handle\n\n* []() VUID-vkQueueSetPerformanceConfigurationINTEL-commonparent  \n   Both of [`Self::configuration`], and [`Self::queue`] **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|                      \\-                      |                    \\-                    |                   Any                   |\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PerformanceConfigurationINTEL`], [`crate::vk::Queue`]\n"]
    #[doc(alias = "vkQueueSetPerformanceConfigurationINTEL")]
    pub unsafe fn queue_set_performance_configuration_intel(&self, queue: crate::vk1_0::Queue, configuration: crate::extensions::intel_performance_query::PerformanceConfigurationINTEL) -> crate::utils::VulkanResult<()> {
        let _function = self.queue_set_performance_configuration_intel.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(queue as _, configuration as _);
        crate::utils::VulkanResult::new(_return, ())
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPerformanceParameterINTEL.html)) · Function <br/> vkGetPerformanceParameterINTEL - Query performance capabilities of the device\n[](#_c_specification)C Specification\n----------\n\nSome performance query features of a device can be discovered with the call:\n\n```\n// Provided by VK_INTEL_performance_query\nVkResult vkGetPerformanceParameterINTEL(\n    VkDevice                                    device,\n    VkPerformanceParameterTypeINTEL             parameter,\n    VkPerformanceValueINTEL*                    pValue);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device to query.\n\n* [`Self::parameter`] is the parameter to query.\n\n* [`Self::p_value`] is a pointer to a [`crate::vk::PerformanceValueINTEL`] structure\n  in which the type and value of the parameter are returned.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPerformanceParameterINTEL-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetPerformanceParameterINTEL-parameter-parameter  \n  [`Self::parameter`] **must** be a valid [`crate::vk::PerformanceParameterTypeINTEL`] value\n\n* []() VUID-vkGetPerformanceParameterINTEL-pValue-parameter  \n  [`Self::p_value`] **must** be a valid pointer to a [`crate::vk::PerformanceValueINTEL`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::PerformanceParameterTypeINTEL`], [`crate::vk::PerformanceValueINTEL`]\n"]
    #[doc(alias = "vkGetPerformanceParameterINTEL")]
    pub unsafe fn get_performance_parameter_intel(&self, parameter: crate::extensions::intel_performance_query::PerformanceParameterTypeINTEL) -> crate::utils::VulkanResult<crate::extensions::intel_performance_query::PerformanceValueINTEL> {
        let _function = self.get_performance_parameter_intel.expect(crate::NOT_LOADED_MESSAGE);
        let mut value = Default::default();
        let _return = _function(self.handle, parameter as _, &mut value);
        crate::utils::VulkanResult::new(_return, value)
    }
}
