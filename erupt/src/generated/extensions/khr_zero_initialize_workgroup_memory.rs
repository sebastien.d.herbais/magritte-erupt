#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_ZERO_INITIALIZE_WORKGROUP_MEMORY_SPEC_VERSION")]
pub const KHR_ZERO_INITIALIZE_WORKGROUP_MEMORY_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_ZERO_INITIALIZE_WORKGROUP_MEMORY_EXTENSION_NAME")]
pub const KHR_ZERO_INITIALIZE_WORKGROUP_MEMORY_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_zero_initialize_workgroup_memory");
#[doc = "Provided by [`crate::extensions::khr_zero_initialize_workgroup_memory`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR: Self = Self(1000325000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR.html)) · Structure <br/> VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR - Structure describing support for zero initialization of workgroup memory by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_zero_initialize_workgroup_memory\ntypedef struct VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderZeroInitializeWorkgroupMemory;\n} VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::shader_zero_initialize_workgroup_memory`] specifies whether the\n  implementation supports initializing a variable in Workgroup storage\n  class.\n\nIf the [`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub shader_zero_initialize_workgroup_memory: crate::vk1_0::Bool32,
}
impl PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR;
}
impl Default for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), shader_zero_initialize_workgroup_memory: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("shader_zero_initialize_workgroup_memory", &(self.shader_zero_initialize_workgroup_memory != 0)).finish()
    }
}
impl PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
        PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR.html)) · Builder of [`PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] <br/> VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR - Structure describing support for zero initialization of workgroup memory by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_zero_initialize_workgroup_memory\ntypedef struct VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           shaderZeroInitializeWorkgroupMemory;\n} VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::shader_zero_initialize_workgroup_memory`] specifies whether the\n  implementation supports initializing a variable in Workgroup storage\n  class.\n\nIf the [`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a>(PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
        PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn shader_zero_initialize_workgroup_memory(mut self, shader_zero_initialize_workgroup_memory: bool) -> Self {
        self.0.shader_zero_initialize_workgroup_memory = shader_zero_initialize_workgroup_memory as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
    type Target = PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
