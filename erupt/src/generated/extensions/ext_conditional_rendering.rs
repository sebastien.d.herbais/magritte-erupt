#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CONDITIONAL_RENDERING_SPEC_VERSION")]
pub const EXT_CONDITIONAL_RENDERING_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_CONDITIONAL_RENDERING_EXTENSION_NAME")]
pub const EXT_CONDITIONAL_RENDERING_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_conditional_rendering");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BEGIN_CONDITIONAL_RENDERING_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdBeginConditionalRenderingEXT");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_END_CONDITIONAL_RENDERING_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdEndConditionalRenderingEXT");
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::vk1_0::BufferUsageFlagBits {
    pub const CONDITIONAL_RENDERING_EXT: Self = Self(512);
}
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::vk1_0::AccessFlagBits {
    pub const CONDITIONAL_RENDERING_READ_EXT: Self = Self(1048576);
}
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::vk1_0::StructureType {
    pub const COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT: Self = Self(1000081000);
    pub const PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT: Self = Self(1000081001);
    pub const CONDITIONAL_RENDERING_BEGIN_INFO_EXT: Self = Self(1000081002);
}
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::vk1_0::PipelineStageFlagBits {
    pub const CONDITIONAL_RENDERING_EXT: Self = Self(262144);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagsEXT.html)) · Bitmask of [`ConditionalRenderingFlagBitsEXT`] <br/> VkConditionalRenderingFlagsEXT - Bitmask of VkConditionalRenderingFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef VkFlags VkConditionalRenderingFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ConditionalRenderingFlagBitsEXT`] is a bitmask type for setting a mask of\nzero or more [VkConditionalRenderingFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ConditionalRenderingBeginInfoEXT`], [VkConditionalRenderingFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagBitsEXT.html)\n"] # [doc (alias = "VkConditionalRenderingFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct ConditionalRenderingFlagsEXT : u32 { const INVERTED_EXT = ConditionalRenderingFlagBitsEXT :: INVERTED_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagBitsEXT.html)) · Bits enum of [`ConditionalRenderingFlagsEXT`] <br/> VkConditionalRenderingFlagBitsEXT - Specify the behavior of conditional rendering\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in[`crate::vk::DeviceLoader::cmd_begin_conditional_rendering_ext::flags`] specifying the behavior\nof conditional rendering are:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef enum VkConditionalRenderingFlagBitsEXT {\n    VK_CONDITIONAL_RENDERING_INVERTED_BIT_EXT = 0x00000001,\n} VkConditionalRenderingFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::INVERTED_EXT`] specifies the condition\n  used to determine whether to discard rendering commands or not.\n  That is, if the 32-bit predicate read from `buffer` memory at`offset` is zero, the rendering commands are not discarded, and if\n  non zero, then they are discarded.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ConditionalRenderingFlagBitsEXT`]\n"]
#[doc(alias = "VkConditionalRenderingFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ConditionalRenderingFlagBitsEXT(pub u32);
impl ConditionalRenderingFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ConditionalRenderingFlagsEXT {
        ConditionalRenderingFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ConditionalRenderingFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INVERTED_EXT => "INVERTED_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::extensions::ext_conditional_rendering::ConditionalRenderingFlagBitsEXT {
    pub const INVERTED_EXT: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginConditionalRenderingEXT.html)) · Function <br/> vkCmdBeginConditionalRenderingEXT - Define the beginning of a conditional rendering block\n[](#_c_specification)C Specification\n----------\n\nTo begin conditional rendering, call:\n\n```\n// Provided by VK_EXT_conditional_rendering\nvoid vkCmdBeginConditionalRenderingEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkConditionalRenderingBeginInfoEXT*   pConditionalRenderingBegin);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which this command will\n  be recorded.\n\n* [`Self::p_conditional_rendering_begin`] is a pointer to a[`crate::vk::ConditionalRenderingBeginInfoEXT`] structure specifying parameters\n  of conditional rendering.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-None-01980  \n   Conditional rendering **must** not already be[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering)\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-pConditionalRenderingBegin-parameter  \n  [`Self::p_conditional_rendering_begin`] **must** be a valid pointer to a valid [`crate::vk::ConditionalRenderingBeginInfoEXT`] structure\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ConditionalRenderingBeginInfoEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBeginConditionalRenderingEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, p_conditional_rendering_begin: *const crate::extensions::ext_conditional_rendering::ConditionalRenderingBeginInfoEXT) -> ();
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndConditionalRenderingEXT.html)) · Function <br/> vkCmdEndConditionalRenderingEXT - Define the end of a conditional rendering block\n[](#_c_specification)C Specification\n----------\n\nTo end conditional rendering, call:\n\n```\n// Provided by VK_EXT_conditional_rendering\nvoid vkCmdEndConditionalRenderingEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which this command will\n  be recorded.\n[](#_description)Description\n----------\n\nOnce ended, conditional rendering becomes inactive.\n\nValid Usage\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01985  \n   Conditional rendering **must** be [active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01986  \n   If conditional rendering was made[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) outside of a render pass\n  instance, it **must** not be ended inside a render pass instance\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01987  \n   If conditional rendering was made[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) within a subpass it **must** be\n  ended in the same subpass\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdEndConditionalRenderingEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceConditionalRenderingFeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceConditionalRenderingInfoEXT> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'_>> for crate::vk1_0::CommandBufferInheritanceInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceConditionalRenderingFeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingBeginInfoEXT.html)) · Structure <br/> VkConditionalRenderingBeginInfoEXT - Structure specifying conditional rendering begin information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ConditionalRenderingBeginInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkConditionalRenderingBeginInfoEXT {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkBuffer                          buffer;\n    VkDeviceSize                      offset;\n    VkConditionalRenderingFlagsEXT    flags;\n} VkConditionalRenderingBeginInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::buffer`] is a buffer containing the predicate for conditional\n  rendering.\n\n* [`Self::offset`] is the byte offset into [`Self::buffer`] where the predicate is\n  located.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::ConditionalRenderingFlagBitsEXT`]specifying the behavior of conditional rendering.\n[](#_description)Description\n----------\n\nIf the 32-bit value at [`Self::offset`] in [`Self::buffer`] memory is zero, then the\nrendering commands are discarded, otherwise they are executed as normal.\nIf the value of the predicate in buffer memory changes while conditional\nrendering is active, the rendering commands **may** be discarded in an\nimplementation-dependent way.\nSome implementations may latch the value of the predicate upon beginning\nconditional rendering while others may read it before every rendering\ncommand.\n\nValid Usage\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-01981  \n   If [`Self::buffer`] is non-sparse then it **must** be bound completely and\n  contiguously to a single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-01982  \n  [`Self::buffer`] **must** have been created with the[`crate::vk::BufferUsageFlagBits::CONDITIONAL_RENDERING_EXT`] bit set\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-offset-01983  \n  [`Self::offset`] **must** be less than the size of [`Self::buffer`] by at least 32\n  bits\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-offset-01984  \n  [`Self::offset`] **must** be a multiple of 4\n\nValid Usage (Implicit)\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CONDITIONAL_RENDERING_BEGIN_INFO_EXT`]\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-parameter  \n  [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkConditionalRenderingFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagBitsEXT.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::ConditionalRenderingFlagBitsEXT`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_begin_conditional_rendering_ext`]\n"]
#[doc(alias = "VkConditionalRenderingBeginInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ConditionalRenderingBeginInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub buffer: crate::vk1_0::Buffer,
    pub offset: crate::vk1_0::DeviceSize,
    pub flags: crate::extensions::ext_conditional_rendering::ConditionalRenderingFlagsEXT,
}
impl ConditionalRenderingBeginInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::CONDITIONAL_RENDERING_BEGIN_INFO_EXT;
}
impl Default for ConditionalRenderingBeginInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), buffer: Default::default(), offset: Default::default(), flags: Default::default() }
    }
}
impl std::fmt::Debug for ConditionalRenderingBeginInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ConditionalRenderingBeginInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("buffer", &self.buffer).field("offset", &self.offset).field("flags", &self.flags).finish()
    }
}
impl ConditionalRenderingBeginInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> ConditionalRenderingBeginInfoEXTBuilder<'a> {
        ConditionalRenderingBeginInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingBeginInfoEXT.html)) · Builder of [`ConditionalRenderingBeginInfoEXT`] <br/> VkConditionalRenderingBeginInfoEXT - Structure specifying conditional rendering begin information\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ConditionalRenderingBeginInfoEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkConditionalRenderingBeginInfoEXT {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkBuffer                          buffer;\n    VkDeviceSize                      offset;\n    VkConditionalRenderingFlagsEXT    flags;\n} VkConditionalRenderingBeginInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::buffer`] is a buffer containing the predicate for conditional\n  rendering.\n\n* [`Self::offset`] is the byte offset into [`Self::buffer`] where the predicate is\n  located.\n\n* [`Self::flags`] is a bitmask of [`crate::vk::ConditionalRenderingFlagBitsEXT`]specifying the behavior of conditional rendering.\n[](#_description)Description\n----------\n\nIf the 32-bit value at [`Self::offset`] in [`Self::buffer`] memory is zero, then the\nrendering commands are discarded, otherwise they are executed as normal.\nIf the value of the predicate in buffer memory changes while conditional\nrendering is active, the rendering commands **may** be discarded in an\nimplementation-dependent way.\nSome implementations may latch the value of the predicate upon beginning\nconditional rendering while others may read it before every rendering\ncommand.\n\nValid Usage\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-01981  \n   If [`Self::buffer`] is non-sparse then it **must** be bound completely and\n  contiguously to a single [`crate::vk::DeviceMemory`] object\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-01982  \n  [`Self::buffer`] **must** have been created with the[`crate::vk::BufferUsageFlagBits::CONDITIONAL_RENDERING_EXT`] bit set\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-offset-01983  \n  [`Self::offset`] **must** be less than the size of [`Self::buffer`] by at least 32\n  bits\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-offset-01984  \n  [`Self::offset`] **must** be a multiple of 4\n\nValid Usage (Implicit)\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::CONDITIONAL_RENDERING_BEGIN_INFO_EXT`]\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-buffer-parameter  \n  [`Self::buffer`] **must** be a valid [`crate::vk::Buffer`] handle\n\n* []() VUID-VkConditionalRenderingBeginInfoEXT-flags-parameter  \n  [`Self::flags`] **must** be a valid combination of [VkConditionalRenderingFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkConditionalRenderingFlagBitsEXT.html) values\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Buffer`], [`crate::vk::ConditionalRenderingFlagBitsEXT`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::cmd_begin_conditional_rendering_ext`]\n"]
#[repr(transparent)]
pub struct ConditionalRenderingBeginInfoEXTBuilder<'a>(ConditionalRenderingBeginInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> ConditionalRenderingBeginInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> ConditionalRenderingBeginInfoEXTBuilder<'a> {
        ConditionalRenderingBeginInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn buffer(mut self, buffer: crate::vk1_0::Buffer) -> Self {
        self.0.buffer = buffer as _;
        self
    }
    #[inline]
    pub fn offset(mut self, offset: crate::vk1_0::DeviceSize) -> Self {
        self.0.offset = offset as _;
        self
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_conditional_rendering::ConditionalRenderingFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ConditionalRenderingBeginInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for ConditionalRenderingBeginInfoEXTBuilder<'a> {
    fn default() -> ConditionalRenderingBeginInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ConditionalRenderingBeginInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ConditionalRenderingBeginInfoEXTBuilder<'a> {
    type Target = ConditionalRenderingBeginInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ConditionalRenderingBeginInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceConditionalRenderingInfoEXT.html)) · Structure <br/> VkCommandBufferInheritanceConditionalRenderingInfoEXT - Structure specifying command buffer inheritance information\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] includes a[`crate::vk::CommandBufferInheritanceConditionalRenderingInfoEXT`] structure, then\nthat structure controls whether a command buffer **can** be executed while\nconditional rendering is [active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) in the\nprimary command buffer.\n\nThe [`crate::vk::CommandBufferInheritanceConditionalRenderingInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkCommandBufferInheritanceConditionalRenderingInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           conditionalRenderingEnable;\n} VkCommandBufferInheritanceConditionalRenderingInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::conditional_rendering_enable`] specifies whether the command buffer**can** be executed while conditional rendering is active in the primary\n  command buffer.\n  If this is [`crate::vk::TRUE`], then this command buffer **can** be executed\n  whether the primary command buffer has active conditional rendering or\n  not.\n  If this is [`crate::vk::FALSE`], then the primary command buffer **must** not\n  have conditional rendering active.\n[](#_description)Description\n----------\n\nIf this structure is not present, the behavior is as if[`Self::conditional_rendering_enable`] is [`crate::vk::FALSE`].\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceConditionalRenderingInfoEXT-conditionalRenderingEnable-01977  \n   If the [inherited conditional\n  rendering](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-inheritedConditionalRendering) feature is not enabled, [`Self::conditional_rendering_enable`]**must** be [`crate::vk::FALSE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceConditionalRenderingInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkCommandBufferInheritanceConditionalRenderingInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CommandBufferInheritanceConditionalRenderingInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub conditional_rendering_enable: crate::vk1_0::Bool32,
}
impl CommandBufferInheritanceConditionalRenderingInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT;
}
impl Default for CommandBufferInheritanceConditionalRenderingInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), conditional_rendering_enable: Default::default() }
    }
}
impl std::fmt::Debug for CommandBufferInheritanceConditionalRenderingInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("CommandBufferInheritanceConditionalRenderingInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("conditional_rendering_enable", &(self.conditional_rendering_enable != 0)).finish()
    }
}
impl CommandBufferInheritanceConditionalRenderingInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
        CommandBufferInheritanceConditionalRenderingInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkCommandBufferInheritanceConditionalRenderingInfoEXT.html)) · Builder of [`CommandBufferInheritanceConditionalRenderingInfoEXT`] <br/> VkCommandBufferInheritanceConditionalRenderingInfoEXT - Structure specifying command buffer inheritance information\n[](#_c_specification)C Specification\n----------\n\nIf the [`Self::p_next`] chain of [`crate::vk::CommandBufferInheritanceInfo`] includes a[`crate::vk::CommandBufferInheritanceConditionalRenderingInfoEXT`] structure, then\nthat structure controls whether a command buffer **can** be executed while\nconditional rendering is [active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) in the\nprimary command buffer.\n\nThe [`crate::vk::CommandBufferInheritanceConditionalRenderingInfoEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkCommandBufferInheritanceConditionalRenderingInfoEXT {\n    VkStructureType    sType;\n    const void*        pNext;\n    VkBool32           conditionalRenderingEnable;\n} VkCommandBufferInheritanceConditionalRenderingInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::conditional_rendering_enable`] specifies whether the command buffer**can** be executed while conditional rendering is active in the primary\n  command buffer.\n  If this is [`crate::vk::TRUE`], then this command buffer **can** be executed\n  whether the primary command buffer has active conditional rendering or\n  not.\n  If this is [`crate::vk::FALSE`], then the primary command buffer **must** not\n  have conditional rendering active.\n[](#_description)Description\n----------\n\nIf this structure is not present, the behavior is as if[`Self::conditional_rendering_enable`] is [`crate::vk::FALSE`].\n\nValid Usage\n\n* []() VUID-VkCommandBufferInheritanceConditionalRenderingInfoEXT-conditionalRenderingEnable-01977  \n   If the [inherited conditional\n  rendering](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-inheritedConditionalRendering) feature is not enabled, [`Self::conditional_rendering_enable`]**must** be [`crate::vk::FALSE`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkCommandBufferInheritanceConditionalRenderingInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a>(CommandBufferInheritanceConditionalRenderingInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
        CommandBufferInheritanceConditionalRenderingInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn conditional_rendering_enable(mut self, conditional_rendering_enable: bool) -> Self {
        self.0.conditional_rendering_enable = conditional_rendering_enable as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> CommandBufferInheritanceConditionalRenderingInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
    fn default() -> CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
    type Target = CommandBufferInheritanceConditionalRenderingInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for CommandBufferInheritanceConditionalRenderingInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceConditionalRenderingFeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceConditionalRenderingFeaturesEXT - Structure describing if a secondary command buffer can be executed if conditional rendering is active in the primary command buffer\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkPhysicalDeviceConditionalRenderingFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           conditionalRendering;\n    VkBool32           inheritedConditionalRendering;\n} VkPhysicalDeviceConditionalRenderingFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::conditional_rendering`] specifies\n  whether conditional rendering is supported.\n\n* []()[`Self::inherited_conditional_rendering`] specifies whether a secondary\n  command buffer **can** be executed while conditional rendering is active in\n  the primary command buffer.\n\nIf the [`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceConditionalRenderingFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceConditionalRenderingFeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceConditionalRenderingFeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub conditional_rendering: crate::vk1_0::Bool32,
    pub inherited_conditional_rendering: crate::vk1_0::Bool32,
}
impl PhysicalDeviceConditionalRenderingFeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT;
}
impl Default for PhysicalDeviceConditionalRenderingFeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), conditional_rendering: Default::default(), inherited_conditional_rendering: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceConditionalRenderingFeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceConditionalRenderingFeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("conditional_rendering", &(self.conditional_rendering != 0)).field("inherited_conditional_rendering", &(self.inherited_conditional_rendering != 0)).finish()
    }
}
impl PhysicalDeviceConditionalRenderingFeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
        PhysicalDeviceConditionalRenderingFeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceConditionalRenderingFeaturesEXT.html)) · Builder of [`PhysicalDeviceConditionalRenderingFeaturesEXT`] <br/> VkPhysicalDeviceConditionalRenderingFeaturesEXT - Structure describing if a secondary command buffer can be executed if conditional rendering is active in the primary command buffer\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_conditional_rendering\ntypedef struct VkPhysicalDeviceConditionalRenderingFeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           conditionalRendering;\n    VkBool32           inheritedConditionalRendering;\n} VkPhysicalDeviceConditionalRenderingFeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::conditional_rendering`] specifies\n  whether conditional rendering is supported.\n\n* []()[`Self::inherited_conditional_rendering`] specifies whether a secondary\n  command buffer **can** be executed while conditional rendering is active in\n  the primary command buffer.\n\nIf the [`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceConditionalRenderingFeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceConditionalRenderingFeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a>(PhysicalDeviceConditionalRenderingFeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
        PhysicalDeviceConditionalRenderingFeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn conditional_rendering(mut self, conditional_rendering: bool) -> Self {
        self.0.conditional_rendering = conditional_rendering as _;
        self
    }
    #[inline]
    pub fn inherited_conditional_rendering(mut self, inherited_conditional_rendering: bool) -> Self {
        self.0.inherited_conditional_rendering = inherited_conditional_rendering as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceConditionalRenderingFeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceConditionalRenderingFeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceConditionalRenderingFeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_conditional_rendering`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBeginConditionalRenderingEXT.html)) · Function <br/> vkCmdBeginConditionalRenderingEXT - Define the beginning of a conditional rendering block\n[](#_c_specification)C Specification\n----------\n\nTo begin conditional rendering, call:\n\n```\n// Provided by VK_EXT_conditional_rendering\nvoid vkCmdBeginConditionalRenderingEXT(\n    VkCommandBuffer                             commandBuffer,\n    const VkConditionalRenderingBeginInfoEXT*   pConditionalRenderingBegin);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which this command will\n  be recorded.\n\n* [`Self::p_conditional_rendering_begin`] is a pointer to a[`crate::vk::ConditionalRenderingBeginInfoEXT`] structure specifying parameters\n  of conditional rendering.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-None-01980  \n   Conditional rendering **must** not already be[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering)\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-pConditionalRenderingBegin-parameter  \n  [`Self::p_conditional_rendering_begin`] **must** be a valid pointer to a valid [`crate::vk::ConditionalRenderingBeginInfoEXT`] structure\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBeginConditionalRenderingEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ConditionalRenderingBeginInfoEXT`]\n"]
    #[doc(alias = "vkCmdBeginConditionalRenderingEXT")]
    pub unsafe fn cmd_begin_conditional_rendering_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, conditional_rendering_begin: &crate::extensions::ext_conditional_rendering::ConditionalRenderingBeginInfoEXT) -> () {
        let _function = self.cmd_begin_conditional_rendering_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _, conditional_rendering_begin as _);
        ()
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdEndConditionalRenderingEXT.html)) · Function <br/> vkCmdEndConditionalRenderingEXT - Define the end of a conditional rendering block\n[](#_c_specification)C Specification\n----------\n\nTo end conditional rendering, call:\n\n```\n// Provided by VK_EXT_conditional_rendering\nvoid vkCmdEndConditionalRenderingEXT(\n    VkCommandBuffer                             commandBuffer);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which this command will\n  be recorded.\n[](#_description)Description\n----------\n\nOnce ended, conditional rendering becomes inactive.\n\nValid Usage\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01985  \n   Conditional rendering **must** be [active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01986  \n   If conditional rendering was made[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) outside of a render pass\n  instance, it **must** not be ended inside a render pass instance\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-None-01987  \n   If conditional rendering was made[active](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#active-conditional-rendering) within a subpass it **must** be\n  ended in the same subpass\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdEndConditionalRenderingEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics, or compute operations\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |         Graphics  <br/>Compute          |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`]\n"]
    #[doc(alias = "vkCmdEndConditionalRenderingEXT")]
    pub unsafe fn cmd_end_conditional_rendering_ext(&self, command_buffer: crate::vk1_0::CommandBuffer) -> () {
        let _function = self.cmd_end_conditional_rendering_ext.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(command_buffer as _);
        ()
    }
}
