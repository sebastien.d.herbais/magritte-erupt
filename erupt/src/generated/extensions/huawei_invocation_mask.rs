#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_HUAWEI_INVOCATION_MASK_SPEC_VERSION")]
pub const HUAWEI_INVOCATION_MASK_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_HUAWEI_INVOCATION_MASK_EXTENSION_NAME")]
pub const HUAWEI_INVOCATION_MASK_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_HUAWEI_invocation_mask");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_BIND_INVOCATION_MASK_HUAWEI: *const std::os::raw::c_char = crate::cstr!("vkCmdBindInvocationMaskHUAWEI");
#[doc = "Provided by [`crate::extensions::huawei_invocation_mask`]"]
impl crate::extensions::khr_synchronization2::AccessFlagBits2KHR {
    pub const INVOCATION_MASK_READ_HUAWEI: Self = Self(549755813888);
}
#[doc = "Provided by [`crate::extensions::huawei_invocation_mask`]"]
impl crate::extensions::khr_synchronization2::PipelineStageFlagBits2KHR {
    pub const INVOCATION_MASK_HUAWEI: Self = Self(1099511627776);
}
#[doc = "Provided by [`crate::extensions::huawei_invocation_mask`]"]
impl crate::vk1_0::ImageUsageFlagBits {
    pub const INVOCATION_MASK_HUAWEI: Self = Self(262144);
}
#[doc = "Provided by [`crate::extensions::huawei_invocation_mask`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI: Self = Self(1000370000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindInvocationMaskHUAWEI.html)) · Function <br/> vkCmdBindInvocationMaskHUAWEI - Bind an invocation mask image on a command buffer\n[](#_c_specification)C Specification\n----------\n\nWhen invocation mask image usage is enabled in the bound ray tracing\npipeline, the pipeline uses an invocation mask image specified by the\ncommand:\n\n```\n// Provided by VK_HUAWEI_invocation_mask\nvoid vkCmdBindInvocationMaskHUAWEI(\n    VkCommandBuffer                             commandBuffer,\n    VkImageView                                 imageView,\n    VkImageLayout                               imageLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded\n\n* [`Self::image_view`] is an image view handle that specifies the invocation\n  mask image [`Self::image_view`] **may** be set to [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), which is\n  equivalent to specifying a view of an image filled with ones value.\n\n* [`Self::image_layout`] is the layout that the image subresources accessible\n  from [`Self::image_view`] will be in when the invocation mask image is\n  accessed\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-None-04976  \n   The [invocation mask image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-invocationMask) feature **must** be\n  enabled\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04977  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** be a valid[`crate::vk::ImageView`] handle of type [`crate::vk::ImageViewType::_2D`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04978  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have a format\n  of [`crate::vk::Format::R8_UINT`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04979  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have been\n  created with [`crate::vk::ImageUsageFlagBits::INVOCATION_MASK_HUAWEI`] set\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04980  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**be [`crate::vk::ImageLayout::GENERAL`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-width-04981  \n   Thread mask image resolution must match the `width` and `height`in [`crate::vk::DeviceLoader::cmd_trace_rays_khr`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-None-04982  \n   Each element in the invocation mask image **must** have the value `0` or`1`.\n  The value 1 means the invocation is active\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-width-04983  \n  `width` in [`crate::vk::DeviceLoader::cmd_trace_rays_khr`] should be 1\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-parameter  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageLayout-parameter  \n  [`Self::image_layout`] **must** be a valid [`crate::vk::ImageLayout`] value\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support compute operations\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::image_view`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                 Outside                  |                 Compute                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ImageLayout`], [`crate::vk::ImageView`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdBindInvocationMaskHUAWEI = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, image_view: crate::vk1_0::ImageView, image_layout: crate::vk1_0::ImageLayout) -> ();
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceInvocationMaskFeaturesHUAWEI> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceInvocationMaskFeaturesHUAWEI> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceInvocationMaskFeaturesHUAWEI.html)) · Structure <br/> VkPhysicalDeviceInvocationMaskFeaturesHUAWEI - Structure describing invocation mask features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] structure is defined\nas:\n\n```\n// Provided by VK_HUAWEI_invocation_mask\ntypedef struct VkPhysicalDeviceInvocationMaskFeaturesHUAWEI {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           invocationMask;\n} VkPhysicalDeviceInvocationMaskFeaturesHUAWEI;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::invocation_mask`] indicates that the\n  implementation supports the use of an invocation mask image to optimize\n  the ray dispatch.\n\nIf the [`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceInvocationMaskFeaturesHUAWEI-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceInvocationMaskFeaturesHUAWEI")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub invocation_mask: crate::vk1_0::Bool32,
}
impl PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI;
}
impl Default for PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), invocation_mask: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceInvocationMaskFeaturesHUAWEI").field("s_type", &self.s_type).field("p_next", &self.p_next).field("invocation_mask", &(self.invocation_mask != 0)).finish()
    }
}
impl PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
        PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceInvocationMaskFeaturesHUAWEI.html)) · Builder of [`PhysicalDeviceInvocationMaskFeaturesHUAWEI`] <br/> VkPhysicalDeviceInvocationMaskFeaturesHUAWEI - Structure describing invocation mask features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] structure is defined\nas:\n\n```\n// Provided by VK_HUAWEI_invocation_mask\ntypedef struct VkPhysicalDeviceInvocationMaskFeaturesHUAWEI {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           invocationMask;\n} VkPhysicalDeviceInvocationMaskFeaturesHUAWEI;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::invocation_mask`] indicates that the\n  implementation supports the use of an invocation mask image to optimize\n  the ray dispatch.\n\nIf the [`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceInvocationMaskFeaturesHUAWEI`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceInvocationMaskFeaturesHUAWEI-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a>(PhysicalDeviceInvocationMaskFeaturesHUAWEI, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
        PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn invocation_mask(mut self, invocation_mask: bool) -> Self {
        self.0.invocation_mask = invocation_mask as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceInvocationMaskFeaturesHUAWEI {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
    fn default() -> PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
    type Target = PhysicalDeviceInvocationMaskFeaturesHUAWEI;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceInvocationMaskFeaturesHUAWEIBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::huawei_invocation_mask`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdBindInvocationMaskHUAWEI.html)) · Function <br/> vkCmdBindInvocationMaskHUAWEI - Bind an invocation mask image on a command buffer\n[](#_c_specification)C Specification\n----------\n\nWhen invocation mask image usage is enabled in the bound ray tracing\npipeline, the pipeline uses an invocation mask image specified by the\ncommand:\n\n```\n// Provided by VK_HUAWEI_invocation_mask\nvoid vkCmdBindInvocationMaskHUAWEI(\n    VkCommandBuffer                             commandBuffer,\n    VkImageView                                 imageView,\n    VkImageLayout                               imageLayout);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded\n\n* [`Self::image_view`] is an image view handle that specifies the invocation\n  mask image [`Self::image_view`] **may** be set to [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), which is\n  equivalent to specifying a view of an image filled with ones value.\n\n* [`Self::image_layout`] is the layout that the image subresources accessible\n  from [`Self::image_view`] will be in when the invocation mask image is\n  accessed\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-None-04976  \n   The [invocation mask image](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#features-invocationMask) feature **must** be\n  enabled\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04977  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** be a valid[`crate::vk::ImageView`] handle of type [`crate::vk::ImageViewType::_2D`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04978  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have a format\n  of [`crate::vk::Format::R8_UINT`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04979  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), it **must** have been\n  created with [`crate::vk::ImageUsageFlagBits::INVOCATION_MASK_HUAWEI`] set\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-04980  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_layout`] **must**be [`crate::vk::ImageLayout::GENERAL`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-width-04981  \n   Thread mask image resolution must match the `width` and `height`in [`crate::vk::DeviceLoader::cmd_trace_rays_khr`]\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-None-04982  \n   Each element in the invocation mask image **must** have the value `0` or`1`.\n  The value 1 means the invocation is active\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-width-04983  \n  `width` in [`crate::vk::DeviceLoader::cmd_trace_rays_khr`] should be 1\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageView-parameter  \n   If [`Self::image_view`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-imageLayout-parameter  \n  [`Self::image_layout`] **must** be a valid [`crate::vk::ImageLayout`] value\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support compute operations\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-renderpass  \n   This command **must** only be called outside of a render pass instance\n\n* []() VUID-vkCmdBindInvocationMaskHUAWEI-commonparent  \n   Both of [`Self::command_buffer`], and [`Self::image_view`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                 Outside                  |                 Compute                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::ImageLayout`], [`crate::vk::ImageView`]\n"]
    #[doc(alias = "vkCmdBindInvocationMaskHUAWEI")]
    pub unsafe fn cmd_bind_invocation_mask_huawei(&self, command_buffer: crate::vk1_0::CommandBuffer, image_view: Option<crate::vk1_0::ImageView>, image_layout: crate::vk1_0::ImageLayout) -> () {
        let _function = self.cmd_bind_invocation_mask_huawei.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(
            command_buffer as _,
            match image_view {
                Some(v) => v,
                None => Default::default(),
            },
            image_layout as _,
        );
        ()
    }
}
