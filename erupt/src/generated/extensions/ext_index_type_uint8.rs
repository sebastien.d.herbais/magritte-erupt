#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_INDEX_TYPE_UINT8_SPEC_VERSION")]
pub const EXT_INDEX_TYPE_UINT8_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_INDEX_TYPE_UINT8_EXTENSION_NAME")]
pub const EXT_INDEX_TYPE_UINT8_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_index_type_uint8");
#[doc = "Provided by [`crate::extensions::ext_index_type_uint8`]"]
impl crate::vk1_0::IndexType {
    pub const UINT8_EXT: Self = Self(1000265000);
}
#[doc = "Provided by [`crate::extensions::ext_index_type_uint8`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT: Self = Self(1000265000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceIndexTypeUint8FeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceIndexTypeUint8FeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceIndexTypeUint8FeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceIndexTypeUint8FeaturesEXT - Structure describing whether uint8 index type can be used\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_index_type_uint8\ntypedef struct VkPhysicalDeviceIndexTypeUint8FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           indexTypeUint8;\n} VkPhysicalDeviceIndexTypeUint8FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::index_type_uint8`] indicates that[`crate::vk::IndexType::UINT8_EXT`] can be used with[`crate::vk::PFN_vkCmdBindIndexBuffer`].\n\nIf the [`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceIndexTypeUint8FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceIndexTypeUint8FeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceIndexTypeUint8FeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub index_type_uint8: crate::vk1_0::Bool32,
}
impl PhysicalDeviceIndexTypeUint8FeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT;
}
impl Default for PhysicalDeviceIndexTypeUint8FeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), index_type_uint8: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceIndexTypeUint8FeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceIndexTypeUint8FeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("index_type_uint8", &(self.index_type_uint8 != 0)).finish()
    }
}
impl PhysicalDeviceIndexTypeUint8FeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
        PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceIndexTypeUint8FeaturesEXT.html)) · Builder of [`PhysicalDeviceIndexTypeUint8FeaturesEXT`] <br/> VkPhysicalDeviceIndexTypeUint8FeaturesEXT - Structure describing whether uint8 index type can be used\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_index_type_uint8\ntypedef struct VkPhysicalDeviceIndexTypeUint8FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           indexTypeUint8;\n} VkPhysicalDeviceIndexTypeUint8FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following feature:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::index_type_uint8`] indicates that[`crate::vk::IndexType::UINT8_EXT`] can be used with[`crate::vk::PFN_vkCmdBindIndexBuffer`].\n\nIf the [`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceIndexTypeUint8FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceIndexTypeUint8FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a>(PhysicalDeviceIndexTypeUint8FeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
        PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn index_type_uint8(mut self, index_type_uint8: bool) -> Self {
        self.0.index_type_uint8 = index_type_uint8 as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceIndexTypeUint8FeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceIndexTypeUint8FeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceIndexTypeUint8FeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
