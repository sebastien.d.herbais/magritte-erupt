#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FRAGMENT_DENSITY_MAP_2_SPEC_VERSION")]
pub const EXT_FRAGMENT_DENSITY_MAP_2_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_FRAGMENT_DENSITY_MAP_2_EXTENSION_NAME")]
pub const EXT_FRAGMENT_DENSITY_MAP_2_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_fragment_density_map2");
#[doc = "Provided by [`crate::extensions::ext_fragment_density_map2`]"]
impl crate::vk1_0::ImageViewCreateFlagBits {
    pub const FRAGMENT_DENSITY_MAP_DEFERRED_EXT: Self = Self(2);
}
#[doc = "Provided by [`crate::extensions::ext_fragment_density_map2`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT: Self = Self(1000332000);
    pub const PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT: Self = Self(1000332001);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceFragmentDensityMap2FeaturesEXT> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentDensityMap2FeaturesEXT> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentDensityMap2PropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentDensityMap2FeaturesEXT.html)) · Structure <br/> VkPhysicalDeviceFragmentDensityMap2FeaturesEXT - Structure describing additional fragment density map features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_fragment_density_map2\ntypedef struct VkPhysicalDeviceFragmentDensityMap2FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fragmentDensityMapDeferred;\n} VkPhysicalDeviceFragmentDensityMap2FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::fragment_density_map_deferred`]specifies whether the implementation supports deferred reads of fragment\n  density map image views.\n  If this feature is not enabled,[`crate::vk::ImageViewCreateFlagBits::FRAGMENT_DENSITY_MAP_DEFERRED_EXT`] **must**not be included in [`crate::vk::ImageViewCreateInfo`]::`flags`.\n\nIf the [`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentDensityMap2FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceFragmentDensityMap2FeaturesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub fragment_density_map_deferred: crate::vk1_0::Bool32,
}
impl PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT;
}
impl Default for PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), fragment_density_map_deferred: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceFragmentDensityMap2FeaturesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("fragment_density_map_deferred", &(self.fragment_density_map_deferred != 0)).finish()
    }
}
impl PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
        PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentDensityMap2FeaturesEXT.html)) · Builder of [`PhysicalDeviceFragmentDensityMap2FeaturesEXT`] <br/> VkPhysicalDeviceFragmentDensityMap2FeaturesEXT - Structure describing additional fragment density map features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_fragment_density_map2\ntypedef struct VkPhysicalDeviceFragmentDensityMap2FeaturesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           fragmentDensityMapDeferred;\n} VkPhysicalDeviceFragmentDensityMap2FeaturesEXT;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::fragment_density_map_deferred`]specifies whether the implementation supports deferred reads of fragment\n  density map image views.\n  If this feature is not enabled,[`crate::vk::ImageViewCreateFlagBits::FRAGMENT_DENSITY_MAP_DEFERRED_EXT`] **must**not be included in [`crate::vk::ImageViewCreateInfo`]::`flags`.\n\nIf the [`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceFragmentDensityMap2FeaturesEXT`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentDensityMap2FeaturesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a>(PhysicalDeviceFragmentDensityMap2FeaturesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
        PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn fragment_density_map_deferred(mut self, fragment_density_map_deferred: bool) -> Self {
        self.0.fragment_density_map_deferred = fragment_density_map_deferred as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceFragmentDensityMap2FeaturesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
    type Target = PhysicalDeviceFragmentDensityMap2FeaturesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceFragmentDensityMap2FeaturesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentDensityMap2PropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceFragmentDensityMap2PropertiesEXT - Structure describing additional fragment density map properties that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentDensityMap2PropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_fragment_density_map2\ntypedef struct VkPhysicalDeviceFragmentDensityMap2PropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           subsampledLoads;\n    VkBool32           subsampledCoarseReconstructionEarlyAccess;\n    uint32_t           maxSubsampledArrayLayers;\n    uint32_t           maxDescriptorSetSubsampledSamplers;\n} VkPhysicalDeviceFragmentDensityMap2PropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::subsampled_loads`] specifies if performing\n  image data read with load operations on subsampled attachments will be\n  resampled to the fragment density of the render pass\n\n* []()[`Self::subsampled_coarse_reconstruction_early_access`] specifies if performing\n  image data read with samplers created with `flags` containing[`crate::vk::SamplerCreateFlagBits::SUBSAMPLED_COARSE_RECONSTRUCTION_EXT`] in\n  fragment shader will trigger additional reads during[`crate::vk::PipelineStageFlagBits::VERTEX_SHADER`]\n\n* []() [`Self::max_subsampled_array_layers`] is\n  the maximum number of [`crate::vk::ImageView`] array layers for usages\n  supporting subsampled samplers\n\n* []()[`Self::max_descriptor_set_subsampled_samplers`] is the maximum number of\n  subsampled samplers that **can** be included in a [`crate::vk::PipelineLayout`]\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceFragmentDensityMap2PropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentDensityMap2PropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceFragmentDensityMap2PropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub subsampled_loads: crate::vk1_0::Bool32,
    pub subsampled_coarse_reconstruction_early_access: crate::vk1_0::Bool32,
    pub max_subsampled_array_layers: u32,
    pub max_descriptor_set_subsampled_samplers: u32,
}
impl PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), subsampled_loads: Default::default(), subsampled_coarse_reconstruction_early_access: Default::default(), max_subsampled_array_layers: Default::default(), max_descriptor_set_subsampled_samplers: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceFragmentDensityMap2PropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("subsampled_loads", &(self.subsampled_loads != 0)).field("subsampled_coarse_reconstruction_early_access", &(self.subsampled_coarse_reconstruction_early_access != 0)).field("max_subsampled_array_layers", &self.max_subsampled_array_layers).field("max_descriptor_set_subsampled_samplers", &self.max_descriptor_set_subsampled_samplers).finish()
    }
}
impl PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
        PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceFragmentDensityMap2PropertiesEXT.html)) · Builder of [`PhysicalDeviceFragmentDensityMap2PropertiesEXT`] <br/> VkPhysicalDeviceFragmentDensityMap2PropertiesEXT - Structure describing additional fragment density map properties that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceFragmentDensityMap2PropertiesEXT`] structure is\ndefined as:\n\n```\n// Provided by VK_EXT_fragment_density_map2\ntypedef struct VkPhysicalDeviceFragmentDensityMap2PropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           subsampledLoads;\n    VkBool32           subsampledCoarseReconstructionEarlyAccess;\n    uint32_t           maxSubsampledArrayLayers;\n    uint32_t           maxDescriptorSetSubsampledSamplers;\n} VkPhysicalDeviceFragmentDensityMap2PropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::subsampled_loads`] specifies if performing\n  image data read with load operations on subsampled attachments will be\n  resampled to the fragment density of the render pass\n\n* []()[`Self::subsampled_coarse_reconstruction_early_access`] specifies if performing\n  image data read with samplers created with `flags` containing[`crate::vk::SamplerCreateFlagBits::SUBSAMPLED_COARSE_RECONSTRUCTION_EXT`] in\n  fragment shader will trigger additional reads during[`crate::vk::PipelineStageFlagBits::VERTEX_SHADER`]\n\n* []() [`Self::max_subsampled_array_layers`] is\n  the maximum number of [`crate::vk::ImageView`] array layers for usages\n  supporting subsampled samplers\n\n* []()[`Self::max_descriptor_set_subsampled_samplers`] is the maximum number of\n  subsampled samplers that **can** be included in a [`crate::vk::PipelineLayout`]\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceFragmentDensityMap2PropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceFragmentDensityMap2PropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a>(PhysicalDeviceFragmentDensityMap2PropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
        PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn subsampled_loads(mut self, subsampled_loads: bool) -> Self {
        self.0.subsampled_loads = subsampled_loads as _;
        self
    }
    #[inline]
    pub fn subsampled_coarse_reconstruction_early_access(mut self, subsampled_coarse_reconstruction_early_access: bool) -> Self {
        self.0.subsampled_coarse_reconstruction_early_access = subsampled_coarse_reconstruction_early_access as _;
        self
    }
    #[inline]
    pub fn max_subsampled_array_layers(mut self, max_subsampled_array_layers: u32) -> Self {
        self.0.max_subsampled_array_layers = max_subsampled_array_layers as _;
        self
    }
    #[inline]
    pub fn max_descriptor_set_subsampled_samplers(mut self, max_descriptor_set_subsampled_samplers: u32) -> Self {
        self.0.max_descriptor_set_subsampled_samplers = max_descriptor_set_subsampled_samplers as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceFragmentDensityMap2PropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceFragmentDensityMap2PropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceFragmentDensityMap2PropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
