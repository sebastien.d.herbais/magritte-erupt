#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_TOOLING_INFO_SPEC_VERSION")]
pub const EXT_TOOLING_INFO_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_TOOLING_INFO_EXTENSION_NAME")]
pub const EXT_TOOLING_INFO_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_tooling_info");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT: *const std::os::raw::c_char = crate::cstr!("vkGetPhysicalDeviceToolPropertiesEXT");
#[doc = "Provided by [`crate::extensions::ext_tooling_info`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT: Self = Self(1000245000);
}
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagsEXT.html)) · Bitmask of [`ToolPurposeFlagBitsEXT`] <br/> VkToolPurposeFlagsEXT - Bitmask of VkToolPurposeFlagBitsEXT\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_tooling_info\ntypedef VkFlags VkToolPurposeFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::ToolPurposeFlagBitsEXT`] is a bitmask type for setting a mask of zero or\nmore [VkToolPurposeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagBitsEXT.html).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDeviceToolPropertiesEXT`], [VkToolPurposeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagBitsEXT.html)\n"] # [doc (alias = "VkToolPurposeFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct ToolPurposeFlagsEXT : u32 { const VALIDATION_EXT = ToolPurposeFlagBitsEXT :: VALIDATION_EXT . 0 ; const PROFILING_EXT = ToolPurposeFlagBitsEXT :: PROFILING_EXT . 0 ; const TRACING_EXT = ToolPurposeFlagBitsEXT :: TRACING_EXT . 0 ; const ADDITIONAL_FEATURES_EXT = ToolPurposeFlagBitsEXT :: ADDITIONAL_FEATURES_EXT . 0 ; const MODIFYING_FEATURES_EXT = ToolPurposeFlagBitsEXT :: MODIFYING_FEATURES_EXT . 0 ; const DEBUG_REPORTING_EXT = ToolPurposeFlagBitsEXT :: DEBUG_REPORTING_EXT . 0 ; const DEBUG_MARKERS_EXT = ToolPurposeFlagBitsEXT :: DEBUG_MARKERS_EXT . 0 ; } }
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagBitsEXT.html)) · Bits enum of [`ToolPurposeFlagsEXT`] <br/> VkToolPurposeFlagBitsEXT - Bitmask specifying the purposes of an active tool\n[](#_c_specification)C Specification\n----------\n\nBits which **can** be set in [`crate::vk::DeviceQueueCreateInfo::purposes`]specifying the purposes of an active tool are:\n\n```\n// Provided by VK_EXT_tooling_info\ntypedef enum VkToolPurposeFlagBitsEXT {\n    VK_TOOL_PURPOSE_VALIDATION_BIT_EXT = 0x00000001,\n    VK_TOOL_PURPOSE_PROFILING_BIT_EXT = 0x00000002,\n    VK_TOOL_PURPOSE_TRACING_BIT_EXT = 0x00000004,\n    VK_TOOL_PURPOSE_ADDITIONAL_FEATURES_BIT_EXT = 0x00000008,\n    VK_TOOL_PURPOSE_MODIFYING_FEATURES_BIT_EXT = 0x00000010,\n  // Provided by VK_EXT_tooling_info with VK_EXT_debug_report, VK_EXT_tooling_info with VK_EXT_debug_utils\n    VK_TOOL_PURPOSE_DEBUG_REPORTING_BIT_EXT = 0x00000020,\n  // Provided by VK_EXT_tooling_info with VK_EXT_debug_marker, VK_EXT_tooling_info with VK_EXT_debug_utils\n    VK_TOOL_PURPOSE_DEBUG_MARKERS_BIT_EXT = 0x00000040,\n} VkToolPurposeFlagBitsEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::VALIDATION_EXT`] specifies that the tool\n  provides validation of API usage.\n\n* [`Self::PROFILING_EXT`] specifies that the tool provides\n  profiling of API usage.\n\n* [`Self::TRACING_EXT`] specifies that the tool is\n  capturing data about the application’s API usage, including anything\n  from simple logging to capturing data for later replay.\n\n* [`Self::ADDITIONAL_FEATURES_EXT`] specifies that the\n  tool provides additional API features/extensions on top of the\n  underlying implementation.\n\n* [`Self::MODIFYING_FEATURES_EXT`] specifies that the tool\n  modifies the API features/limits/extensions presented to the\n  application.\n\n* [`Self::DEBUG_REPORTING_EXT`] specifies that the tool\n  reports additional information to the application via callbacks\n  specified by[`crate::vk::InstanceLoader::create_debug_report_callback_ext`]or[`crate::vk::InstanceLoader::create_debug_utils_messenger_ext`]\n\n* [`Self::DEBUG_MARKERS_EXT`] specifies that the tool\n  consumes[debug markers](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-debug-markers)or[object debug annotation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-object-debug-annotation),[queue labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-queue-labels), or[command buffer labels](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#debugging-command-buffer-labels)\n[](#_see_also)See Also\n----------\n\n[`crate::vk::ToolPurposeFlagBitsEXT`]\n"]
#[doc(alias = "VkToolPurposeFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct ToolPurposeFlagBitsEXT(pub u32);
impl ToolPurposeFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> ToolPurposeFlagsEXT {
        ToolPurposeFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for ToolPurposeFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::VALIDATION_EXT => "VALIDATION_EXT",
            &Self::PROFILING_EXT => "PROFILING_EXT",
            &Self::TRACING_EXT => "TRACING_EXT",
            &Self::ADDITIONAL_FEATURES_EXT => "ADDITIONAL_FEATURES_EXT",
            &Self::MODIFYING_FEATURES_EXT => "MODIFYING_FEATURES_EXT",
            &Self::DEBUG_REPORTING_EXT => "DEBUG_REPORTING_EXT",
            &Self::DEBUG_MARKERS_EXT => "DEBUG_MARKERS_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_tooling_info`]"]
impl crate::extensions::ext_tooling_info::ToolPurposeFlagBitsEXT {
    pub const VALIDATION_EXT: Self = Self(1);
    pub const PROFILING_EXT: Self = Self(2);
    pub const TRACING_EXT: Self = Self(4);
    pub const ADDITIONAL_FEATURES_EXT: Self = Self(8);
    pub const MODIFYING_FEATURES_EXT: Self = Self(16);
    pub const DEBUG_REPORTING_EXT: Self = Self(32);
    pub const DEBUG_MARKERS_EXT: Self = Self(64);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceToolPropertiesEXT.html)) · Function <br/> vkGetPhysicalDeviceToolPropertiesEXT - Reports properties of tools active on the specified physical device\n[](#_c_specification)C Specification\n----------\n\nInformation about tools providing debugging, profiling, or similar services,\nactive for a given physical device, can be obtained by calling:\n\n```\n// Provided by VK_EXT_tooling_info\nVkResult vkGetPhysicalDeviceToolPropertiesEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pToolCount,\n    VkPhysicalDeviceToolPropertiesEXT*          pToolProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device to query for\n  active tools.\n\n* [`Self::p_tool_count`] is a pointer to an integer describing the number of\n  tools active on [`Self::physical_device`].\n\n* [`Self::p_tool_properties`] is either `NULL` or a pointer to an array of[`crate::vk::PhysicalDeviceToolPropertiesEXT`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_tool_properties`] is `NULL`, then the number of tools currently\nactive on [`Self::physical_device`] is returned in [`Self::p_tool_count`].\nOtherwise, [`Self::p_tool_count`] **must** point to a variable set by the user to the\nnumber of elements in the [`Self::p_tool_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_tool_properties`].\nIf [`Self::p_tool_count`] is less than the number of currently active tools, at\nmost [`Self::p_tool_count`] structures will be written.\n\nThe count and properties of active tools **may** change in response to events\noutside the scope of the specification.\nAn application **should** assume these properties might change at any given\ntime.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-pToolCount-parameter  \n  [`Self::p_tool_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-pToolProperties-parameter  \n   If the value referenced by [`Self::p_tool_count`] is not `0`, and [`Self::p_tool_properties`] is not `NULL`, [`Self::p_tool_properties`] **must** be a valid pointer to an array of [`Self::p_tool_count`] [`crate::vk::PhysicalDeviceToolPropertiesEXT`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceToolPropertiesEXT`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetPhysicalDeviceToolPropertiesEXT = unsafe extern "system" fn(physical_device: crate::vk1_0::PhysicalDevice, p_tool_count: *mut u32, p_tool_properties: *mut crate::extensions::ext_tooling_info::PhysicalDeviceToolPropertiesEXT) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceToolPropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceToolPropertiesEXT - Structure providing information about an active tool\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceToolPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_tooling_info\ntypedef struct VkPhysicalDeviceToolPropertiesEXT {\n    VkStructureType          sType;\n    void*                    pNext;\n    char                     name[VK_MAX_EXTENSION_NAME_SIZE];\n    char                     version[VK_MAX_EXTENSION_NAME_SIZE];\n    VkToolPurposeFlagsEXT    purposes;\n    char                     description[VK_MAX_DESCRIPTION_SIZE];\n    char                     layer[VK_MAX_EXTENSION_NAME_SIZE];\n} VkPhysicalDeviceToolPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is a null-terminated UTF-8 string containing the name of the\n  tool.\n\n* [`Self::version`] is a null-terminated UTF-8 string containing the version\n  of the tool.\n\n* [`Self::purposes`] is a bitmask of [VkToolPurposeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagBitsEXT.html) which is\n  populated with purposes supported by the tool.\n\n* [`Self::description`] is a null-terminated UTF-8 string containing a\n  description of the tool.\n\n* [`Self::layer`] is a null-terminated UTF-8 string that contains the name of\n  the layer implementing the tool, if the tool is implemented in a layer -\n  otherwise it **may** be an empty string.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceToolPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT`]\n\n* []() VUID-VkPhysicalDeviceToolPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ToolPurposeFlagBitsEXT`], [`crate::vk::DeviceLoader::get_physical_device_tool_properties_ext`]\n"]
#[doc(alias = "VkPhysicalDeviceToolPropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceToolPropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub name: [std::os::raw::c_char; 256],
    pub version: [std::os::raw::c_char; 256],
    pub purposes: crate::extensions::ext_tooling_info::ToolPurposeFlagsEXT,
    pub description: [std::os::raw::c_char; 256],
    pub layer: [std::os::raw::c_char; 256],
}
impl PhysicalDeviceToolPropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceToolPropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), name: unsafe { std::mem::zeroed() }, version: unsafe { std::mem::zeroed() }, purposes: Default::default(), description: unsafe { std::mem::zeroed() }, layer: unsafe { std::mem::zeroed() } }
    }
}
impl std::fmt::Debug for PhysicalDeviceToolPropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceToolPropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("name", unsafe { &std::ffi::CStr::from_ptr(self.name.as_ptr()) }).field("version", unsafe { &std::ffi::CStr::from_ptr(self.version.as_ptr()) }).field("purposes", &self.purposes).field("description", unsafe { &std::ffi::CStr::from_ptr(self.description.as_ptr()) }).field("layer", unsafe { &std::ffi::CStr::from_ptr(self.layer.as_ptr()) }).finish()
    }
}
impl PhysicalDeviceToolPropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceToolPropertiesEXTBuilder<'a> {
        PhysicalDeviceToolPropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceToolPropertiesEXT.html)) · Builder of [`PhysicalDeviceToolPropertiesEXT`] <br/> VkPhysicalDeviceToolPropertiesEXT - Structure providing information about an active tool\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceToolPropertiesEXT`] structure is defined as:\n\n```\n// Provided by VK_EXT_tooling_info\ntypedef struct VkPhysicalDeviceToolPropertiesEXT {\n    VkStructureType          sType;\n    void*                    pNext;\n    char                     name[VK_MAX_EXTENSION_NAME_SIZE];\n    char                     version[VK_MAX_EXTENSION_NAME_SIZE];\n    VkToolPurposeFlagsEXT    purposes;\n    char                     description[VK_MAX_DESCRIPTION_SIZE];\n    char                     layer[VK_MAX_EXTENSION_NAME_SIZE];\n} VkPhysicalDeviceToolPropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::name`] is a null-terminated UTF-8 string containing the name of the\n  tool.\n\n* [`Self::version`] is a null-terminated UTF-8 string containing the version\n  of the tool.\n\n* [`Self::purposes`] is a bitmask of [VkToolPurposeFlagBitsEXT](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkToolPurposeFlagBitsEXT.html) which is\n  populated with purposes supported by the tool.\n\n* [`Self::description`] is a null-terminated UTF-8 string containing a\n  description of the tool.\n\n* [`Self::layer`] is a null-terminated UTF-8 string that contains the name of\n  the layer implementing the tool, if the tool is implemented in a layer -\n  otherwise it **may** be an empty string.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceToolPropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT`]\n\n* []() VUID-VkPhysicalDeviceToolPropertiesEXT-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::ToolPurposeFlagBitsEXT`], [`crate::vk::DeviceLoader::get_physical_device_tool_properties_ext`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceToolPropertiesEXTBuilder<'a>(PhysicalDeviceToolPropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceToolPropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceToolPropertiesEXTBuilder<'a> {
        PhysicalDeviceToolPropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn name(mut self, name: [std::os::raw::c_char; 256]) -> Self {
        self.0.name = name as _;
        self
    }
    #[inline]
    pub fn version(mut self, version: [std::os::raw::c_char; 256]) -> Self {
        self.0.version = version as _;
        self
    }
    #[inline]
    pub fn purposes(mut self, purposes: crate::extensions::ext_tooling_info::ToolPurposeFlagsEXT) -> Self {
        self.0.purposes = purposes as _;
        self
    }
    #[inline]
    pub fn description(mut self, description: [std::os::raw::c_char; 256]) -> Self {
        self.0.description = description as _;
        self
    }
    #[inline]
    pub fn layer(mut self, layer: [std::os::raw::c_char; 256]) -> Self {
        self.0.layer = layer as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceToolPropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceToolPropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceToolPropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceToolPropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceToolPropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceToolPropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceToolPropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_tooling_info`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetPhysicalDeviceToolPropertiesEXT.html)) · Function <br/> vkGetPhysicalDeviceToolPropertiesEXT - Reports properties of tools active on the specified physical device\n[](#_c_specification)C Specification\n----------\n\nInformation about tools providing debugging, profiling, or similar services,\nactive for a given physical device, can be obtained by calling:\n\n```\n// Provided by VK_EXT_tooling_info\nVkResult vkGetPhysicalDeviceToolPropertiesEXT(\n    VkPhysicalDevice                            physicalDevice,\n    uint32_t*                                   pToolCount,\n    VkPhysicalDeviceToolPropertiesEXT*          pToolProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::physical_device`] is the handle to the physical device to query for\n  active tools.\n\n* [`Self::p_tool_count`] is a pointer to an integer describing the number of\n  tools active on [`Self::physical_device`].\n\n* [`Self::p_tool_properties`] is either `NULL` or a pointer to an array of[`crate::vk::PhysicalDeviceToolPropertiesEXT`] structures.\n[](#_description)Description\n----------\n\nIf [`Self::p_tool_properties`] is `NULL`, then the number of tools currently\nactive on [`Self::physical_device`] is returned in [`Self::p_tool_count`].\nOtherwise, [`Self::p_tool_count`] **must** point to a variable set by the user to the\nnumber of elements in the [`Self::p_tool_properties`] array, and on return the\nvariable is overwritten with the number of structures actually written to[`Self::p_tool_properties`].\nIf [`Self::p_tool_count`] is less than the number of currently active tools, at\nmost [`Self::p_tool_count`] structures will be written.\n\nThe count and properties of active tools **may** change in response to events\noutside the scope of the specification.\nAn application **should** assume these properties might change at any given\ntime.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-physicalDevice-parameter  \n  [`Self::physical_device`] **must** be a valid [`crate::vk::PhysicalDevice`] handle\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-pToolCount-parameter  \n  [`Self::p_tool_count`] **must** be a valid pointer to a `uint32_t` value\n\n* []() VUID-vkGetPhysicalDeviceToolPropertiesEXT-pToolProperties-parameter  \n   If the value referenced by [`Self::p_tool_count`] is not `0`, and [`Self::p_tool_properties`] is not `NULL`, [`Self::p_tool_properties`] **must** be a valid pointer to an array of [`Self::p_tool_count`] [`crate::vk::PhysicalDeviceToolPropertiesEXT`] structures\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\n* [`crate::vk::Result::INCOMPLETE`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PhysicalDevice`], [`crate::vk::PhysicalDeviceToolPropertiesEXT`]\n"]
    #[doc(alias = "vkGetPhysicalDeviceToolPropertiesEXT")]
    pub unsafe fn get_physical_device_tool_properties_ext(&self, physical_device: crate::vk1_0::PhysicalDevice, tool_count: Option<u32>) -> crate::utils::VulkanResult<crate::SmallVec<crate::extensions::ext_tooling_info::PhysicalDeviceToolPropertiesEXT>> {
        let _function = self.get_physical_device_tool_properties_ext.expect(crate::NOT_LOADED_MESSAGE);
        let mut tool_count = match tool_count {
            Some(v) => v,
            None => {
                let mut v = Default::default();
                _function(physical_device as _, &mut v, std::ptr::null_mut());
                v
            }
        };
        let mut tool_properties = crate::SmallVec::from_elem(Default::default(), tool_count as _);
        let _return = _function(physical_device as _, &mut tool_count, tool_properties.as_mut_ptr());
        crate::utils::VulkanResult::new(_return, tool_properties)
    }
}
