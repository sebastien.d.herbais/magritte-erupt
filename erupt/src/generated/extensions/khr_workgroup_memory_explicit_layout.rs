#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_SPEC_VERSION")]
pub const KHR_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_EXTENSION_NAME")]
pub const KHR_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_workgroup_memory_explicit_layout");
#[doc = "Provided by [`crate::extensions::khr_workgroup_memory_explicit_layout`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR: Self = Self(1000336000);
}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'_>> for crate::vk1_0::DeviceCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'_>> for crate::vk1_1::PhysicalDeviceFeatures2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR.html)) · Structure <br/> VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR - Structure describing the workgroup storage explicit layout features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_workgroup_memory_explicit_layout\ntypedef struct VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           workgroupMemoryExplicitLayout;\n    VkBool32           workgroupMemoryExplicitLayoutScalarBlockLayout;\n    VkBool32           workgroupMemoryExplicitLayout8BitAccess;\n    VkBool32           workgroupMemoryExplicitLayout16BitAccess;\n} VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::workgroup_memory_explicit_layout`] indicates whether the implementation\n  supports the SPIR-V `WorkgroupMemoryExplicitLayoutKHR` capability.\n\n* []()[`Self::workgroup_memory_explicit_layout_scalar_block_layout`] indicates whether\n  the implementation supports scalar alignment for laying out Workgroup\n  Blocks.\n\n* []()[`Self::workgroup_memory_explicit_layout8_bit_access`] indicates whether objects\n  in the `Workgroup` storage class with the `Block` decoration **can**have 8-bit integer members.\n  If this feature is not enabled, 8-bit integer members **must** not be used\n  in such objects.\n  This also indicates whether shader modules **can** declare the`WorkgroupMemoryExplicitLayout8BitAccessKHR` capability.\n\n* []()[`Self::workgroup_memory_explicit_layout16_bit_access`] indicates whether objects\n  in the `Workgroup` storage class with the `Block` decoration **can**have 16-bit integer and 16-bit floating-point members.\n  If this feature is not enabled, 16-bit integer or 16-bit floating-point\n  members **must** not be used in such objects.\n  This also indicates whether shader modules **can** declare the`WorkgroupMemoryExplicitLayout16BitAccessKHR` capability.\n\nIf the [`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub workgroup_memory_explicit_layout: crate::vk1_0::Bool32,
    pub workgroup_memory_explicit_layout_scalar_block_layout: crate::vk1_0::Bool32,
    pub workgroup_memory_explicit_layout8_bit_access: crate::vk1_0::Bool32,
    pub workgroup_memory_explicit_layout16_bit_access: crate::vk1_0::Bool32,
}
impl PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR;
}
impl Default for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), workgroup_memory_explicit_layout: Default::default(), workgroup_memory_explicit_layout_scalar_block_layout: Default::default(), workgroup_memory_explicit_layout8_bit_access: Default::default(), workgroup_memory_explicit_layout16_bit_access: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("workgroup_memory_explicit_layout", &(self.workgroup_memory_explicit_layout != 0)).field("workgroup_memory_explicit_layout_scalar_block_layout", &(self.workgroup_memory_explicit_layout_scalar_block_layout != 0)).field("workgroup_memory_explicit_layout8_bit_access", &(self.workgroup_memory_explicit_layout8_bit_access != 0)).field("workgroup_memory_explicit_layout16_bit_access", &(self.workgroup_memory_explicit_layout16_bit_access != 0)).finish()
    }
}
impl PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
        PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR.html)) · Builder of [`PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] <br/> VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR - Structure describing the workgroup storage explicit layout features that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] structure\nis defined as:\n\n```\n// Provided by VK_KHR_workgroup_memory_explicit_layout\ntypedef struct VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    VkBool32           workgroupMemoryExplicitLayout;\n    VkBool32           workgroupMemoryExplicitLayoutScalarBlockLayout;\n    VkBool32           workgroupMemoryExplicitLayout8BitAccess;\n    VkBool32           workgroupMemoryExplicitLayout16BitAccess;\n} VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR;\n```\n[](#_members)Members\n----------\n\nThis structure describes the following features:\n[](#_description)Description\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []()[`Self::workgroup_memory_explicit_layout`] indicates whether the implementation\n  supports the SPIR-V `WorkgroupMemoryExplicitLayoutKHR` capability.\n\n* []()[`Self::workgroup_memory_explicit_layout_scalar_block_layout`] indicates whether\n  the implementation supports scalar alignment for laying out Workgroup\n  Blocks.\n\n* []()[`Self::workgroup_memory_explicit_layout8_bit_access`] indicates whether objects\n  in the `Workgroup` storage class with the `Block` decoration **can**have 8-bit integer members.\n  If this feature is not enabled, 8-bit integer members **must** not be used\n  in such objects.\n  This also indicates whether shader modules **can** declare the`WorkgroupMemoryExplicitLayout8BitAccessKHR` capability.\n\n* []()[`Self::workgroup_memory_explicit_layout16_bit_access`] indicates whether objects\n  in the `Workgroup` storage class with the `Block` decoration **can**have 16-bit integer and 16-bit floating-point members.\n  If this feature is not enabled, 16-bit integer or 16-bit floating-point\n  members **must** not be used in such objects.\n  This also indicates whether shader modules **can** declare the`WorkgroupMemoryExplicitLayout16BitAccessKHR` capability.\n\nIf the [`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceFeatures2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceFeatures2`], it is filled in to indicate whether each\ncorresponding feature is supported.[`crate::vk::PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR`] **can** also be used in the [`Self::p_next`] chain of[`crate::vk::DeviceCreateInfo`] to selectively enable these features.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Bool32`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a>(PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
        PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn workgroup_memory_explicit_layout(mut self, workgroup_memory_explicit_layout: bool) -> Self {
        self.0.workgroup_memory_explicit_layout = workgroup_memory_explicit_layout as _;
        self
    }
    #[inline]
    pub fn workgroup_memory_explicit_layout_scalar_block_layout(mut self, workgroup_memory_explicit_layout_scalar_block_layout: bool) -> Self {
        self.0.workgroup_memory_explicit_layout_scalar_block_layout = workgroup_memory_explicit_layout_scalar_block_layout as _;
        self
    }
    #[inline]
    pub fn workgroup_memory_explicit_layout8_bit_access(mut self, workgroup_memory_explicit_layout8_bit_access: bool) -> Self {
        self.0.workgroup_memory_explicit_layout8_bit_access = workgroup_memory_explicit_layout8_bit_access as _;
        self
    }
    #[inline]
    pub fn workgroup_memory_explicit_layout16_bit_access(mut self, workgroup_memory_explicit_layout16_bit_access: bool) -> Self {
        self.0.workgroup_memory_explicit_layout16_bit_access = workgroup_memory_explicit_layout16_bit_access as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
    fn default() -> PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
    type Target = PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
