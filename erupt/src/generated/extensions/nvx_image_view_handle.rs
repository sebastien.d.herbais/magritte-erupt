#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NVX_IMAGE_VIEW_HANDLE_SPEC_VERSION")]
pub const NVX_IMAGE_VIEW_HANDLE_SPEC_VERSION: u32 = 2;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_NVX_IMAGE_VIEW_HANDLE_EXTENSION_NAME")]
pub const NVX_IMAGE_VIEW_HANDLE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NVX_image_view_handle");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_IMAGE_VIEW_HANDLE_NVX: *const std::os::raw::c_char = crate::cstr!("vkGetImageViewHandleNVX");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_IMAGE_VIEW_ADDRESS_NVX: *const std::os::raw::c_char = crate::cstr!("vkGetImageViewAddressNVX");
#[doc = "Provided by [`crate::extensions::nvx_image_view_handle`]"]
impl crate::vk1_0::StructureType {
    pub const IMAGE_VIEW_HANDLE_INFO_NVX: Self = Self(1000030000);
    pub const IMAGE_VIEW_ADDRESS_PROPERTIES_NVX: Self = Self(1000030001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageViewHandleNVX.html)) · Function <br/> vkGetImageViewHandleNVX - Get the handle for an image view for a specific descriptor type\n[](#_c_specification)C Specification\n----------\n\nTo get the handle for an image view, call:\n\n```\n// Provided by VK_NVX_image_view_handle\nuint32_t vkGetImageViewHandleNVX(\n    VkDevice                                    device,\n    const VkImageViewHandleInfoNVX*             pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image view.\n\n* [`Self::p_info`] describes the image view to query and type of handle.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageViewHandleNVX-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageViewHandleNVX-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::ImageViewHandleInfoNVX`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImageViewHandleInfoNVX`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetImageViewHandleNVX = unsafe extern "system" fn(device: crate::vk1_0::Device, p_info: *const crate::extensions::nvx_image_view_handle::ImageViewHandleInfoNVX) -> u32;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageViewAddressNVX.html)) · Function <br/> vkGetImageViewAddressNVX - Get the device address of an image view\n[](#_c_specification)C Specification\n----------\n\nTo get the device address for an image view, call:\n\n```\n// Provided by VK_NVX_image_view_handle\nVkResult vkGetImageViewAddressNVX(\n    VkDevice                                    device,\n    VkImageView                                 imageView,\n    VkImageViewAddressPropertiesNVX*            pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image view.\n\n* [`Self::image_view`] is a handle to the image view.\n\n* [`Self::p_properties`] contains the device address and size when the call\n  returns.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageViewAddressNVX-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageViewAddressNVX-imageView-parameter  \n  [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkGetImageViewAddressNVX-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::ImageViewAddressPropertiesNVX`] structure\n\n* []() VUID-vkGetImageViewAddressNVX-imageView-parent  \n  [`Self::image_view`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_UNKNOWN`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImageView`], [`crate::vk::ImageViewAddressPropertiesNVX`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetImageViewAddressNVX = unsafe extern "system" fn(device: crate::vk1_0::Device, image_view: crate::vk1_0::ImageView, p_properties: *mut crate::extensions::nvx_image_view_handle::ImageViewAddressPropertiesNVX) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewHandleInfoNVX.html)) · Structure <br/> VkImageViewHandleInfoNVX - Structure specifying the image view for handle queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageViewHandleInfoNVX`] structure is defined as:\n\n```\n// Provided by VK_NVX_image_view_handle\ntypedef struct VkImageViewHandleInfoNVX {\n    VkStructureType     sType;\n    const void*         pNext;\n    VkImageView         imageView;\n    VkDescriptorType    descriptorType;\n    VkSampler           sampler;\n} VkImageViewHandleInfoNVX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_view`] is the image view to query.\n\n* [`Self::descriptor_type`] is the type of descriptor for which to query a\n  handle.\n\n* [`Self::sampler`] is the sampler to combine with the image view when\n  generating the handle.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageViewHandleInfoNVX-descriptorType-02654  \n  [`Self::descriptor_type`] **must** be [`crate::vk::DescriptorType::SAMPLED_IMAGE`],[`crate::vk::DescriptorType::STORAGE_IMAGE`], or[`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`]\n\n* []() VUID-VkImageViewHandleInfoNVX-sampler-02655  \n  [`Self::sampler`] **must** be a valid [`crate::vk::Sampler`] if [`Self::descriptor_type`]is [`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`]\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-02656  \n   If descriptorType is [`crate::vk::DescriptorType::SAMPLED_IMAGE`] or[`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`], the image that[`Self::image_view`] was created from **must** have been created with the[`crate::vk::ImageUsageFlagBits::SAMPLED`] usage bit set\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-02657  \n   If descriptorType is [`crate::vk::DescriptorType::STORAGE_IMAGE`], the image\n  that [`Self::image_view`] was created from **must** have been created with the[`crate::vk::ImageUsageFlagBits::STORAGE`] usage bit set\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewHandleInfoNVX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_HANDLE_INFO_NVX`]\n\n* []() VUID-VkImageViewHandleInfoNVX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-parameter  \n  [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-VkImageViewHandleInfoNVX-descriptorType-parameter  \n  [`Self::descriptor_type`] **must** be a valid [`crate::vk::DescriptorType`] value\n\n* []() VUID-VkImageViewHandleInfoNVX-sampler-parameter  \n   If [`Self::sampler`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sampler`] **must** be a valid [`crate::vk::Sampler`] handle\n\n* []() VUID-VkImageViewHandleInfoNVX-commonparent  \n   Both of [`Self::image_view`], and [`Self::sampler`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DescriptorType`], [`crate::vk::ImageView`], [`crate::vk::Sampler`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_view_handle_nvx`]\n"]
#[doc(alias = "VkImageViewHandleInfoNVX")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageViewHandleInfoNVX {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub image_view: crate::vk1_0::ImageView,
    pub descriptor_type: crate::vk1_0::DescriptorType,
    pub sampler: crate::vk1_0::Sampler,
}
impl ImageViewHandleInfoNVX {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_VIEW_HANDLE_INFO_NVX;
}
impl Default for ImageViewHandleInfoNVX {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), image_view: Default::default(), descriptor_type: Default::default(), sampler: Default::default() }
    }
}
impl std::fmt::Debug for ImageViewHandleInfoNVX {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageViewHandleInfoNVX").field("s_type", &self.s_type).field("p_next", &self.p_next).field("image_view", &self.image_view).field("descriptor_type", &self.descriptor_type).field("sampler", &self.sampler).finish()
    }
}
impl ImageViewHandleInfoNVX {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageViewHandleInfoNVXBuilder<'a> {
        ImageViewHandleInfoNVXBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewHandleInfoNVX.html)) · Builder of [`ImageViewHandleInfoNVX`] <br/> VkImageViewHandleInfoNVX - Structure specifying the image view for handle queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageViewHandleInfoNVX`] structure is defined as:\n\n```\n// Provided by VK_NVX_image_view_handle\ntypedef struct VkImageViewHandleInfoNVX {\n    VkStructureType     sType;\n    const void*         pNext;\n    VkImageView         imageView;\n    VkDescriptorType    descriptorType;\n    VkSampler           sampler;\n} VkImageViewHandleInfoNVX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::image_view`] is the image view to query.\n\n* [`Self::descriptor_type`] is the type of descriptor for which to query a\n  handle.\n\n* [`Self::sampler`] is the sampler to combine with the image view when\n  generating the handle.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkImageViewHandleInfoNVX-descriptorType-02654  \n  [`Self::descriptor_type`] **must** be [`crate::vk::DescriptorType::SAMPLED_IMAGE`],[`crate::vk::DescriptorType::STORAGE_IMAGE`], or[`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`]\n\n* []() VUID-VkImageViewHandleInfoNVX-sampler-02655  \n  [`Self::sampler`] **must** be a valid [`crate::vk::Sampler`] if [`Self::descriptor_type`]is [`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`]\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-02656  \n   If descriptorType is [`crate::vk::DescriptorType::SAMPLED_IMAGE`] or[`crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER`], the image that[`Self::image_view`] was created from **must** have been created with the[`crate::vk::ImageUsageFlagBits::SAMPLED`] usage bit set\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-02657  \n   If descriptorType is [`crate::vk::DescriptorType::STORAGE_IMAGE`], the image\n  that [`Self::image_view`] was created from **must** have been created with the[`crate::vk::ImageUsageFlagBits::STORAGE`] usage bit set\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewHandleInfoNVX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_HANDLE_INFO_NVX`]\n\n* []() VUID-VkImageViewHandleInfoNVX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkImageViewHandleInfoNVX-imageView-parameter  \n  [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-VkImageViewHandleInfoNVX-descriptorType-parameter  \n  [`Self::descriptor_type`] **must** be a valid [`crate::vk::DescriptorType`] value\n\n* []() VUID-VkImageViewHandleInfoNVX-sampler-parameter  \n   If [`Self::sampler`] is not [VK\\_NULL\\_HANDLE](VK_NULL_HANDLE.html), [`Self::sampler`] **must** be a valid [`crate::vk::Sampler`] handle\n\n* []() VUID-VkImageViewHandleInfoNVX-commonparent  \n   Both of [`Self::image_view`], and [`Self::sampler`] that are valid handles of non-ignored parameters **must** have been created, allocated, or retrieved from the same [`crate::vk::Device`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DescriptorType`], [`crate::vk::ImageView`], [`crate::vk::Sampler`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_view_handle_nvx`]\n"]
#[repr(transparent)]
pub struct ImageViewHandleInfoNVXBuilder<'a>(ImageViewHandleInfoNVX, std::marker::PhantomData<&'a ()>);
impl<'a> ImageViewHandleInfoNVXBuilder<'a> {
    #[inline]
    pub fn new() -> ImageViewHandleInfoNVXBuilder<'a> {
        ImageViewHandleInfoNVXBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn image_view(mut self, image_view: crate::vk1_0::ImageView) -> Self {
        self.0.image_view = image_view as _;
        self
    }
    #[inline]
    pub fn descriptor_type(mut self, descriptor_type: crate::vk1_0::DescriptorType) -> Self {
        self.0.descriptor_type = descriptor_type as _;
        self
    }
    #[inline]
    pub fn sampler(mut self, sampler: crate::vk1_0::Sampler) -> Self {
        self.0.sampler = sampler as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageViewHandleInfoNVX {
        self.0
    }
}
impl<'a> std::default::Default for ImageViewHandleInfoNVXBuilder<'a> {
    fn default() -> ImageViewHandleInfoNVXBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageViewHandleInfoNVXBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageViewHandleInfoNVXBuilder<'a> {
    type Target = ImageViewHandleInfoNVX;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageViewHandleInfoNVXBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewAddressPropertiesNVX.html)) · Structure <br/> VkImageViewAddressPropertiesNVX - Structure specifying the image view for handle queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageViewAddressPropertiesNVX`] structure is defined as:\n\n```\n// Provided by VK_NVX_image_view_handle\ntypedef struct VkImageViewAddressPropertiesNVX {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceAddress    deviceAddress;\n    VkDeviceSize       size;\n} VkImageViewAddressPropertiesNVX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::device_address`] is the device address of the image view.\n\n* [`Self::size`] is the size in bytes of the image view device memory.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewAddressPropertiesNVX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_ADDRESS_PROPERTIES_NVX`]\n\n* []() VUID-VkImageViewAddressPropertiesNVX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_view_address_nvx`]\n"]
#[doc(alias = "VkImageViewAddressPropertiesNVX")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImageViewAddressPropertiesNVX {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub device_address: crate::vk1_0::DeviceAddress,
    pub size: crate::vk1_0::DeviceSize,
}
impl ImageViewAddressPropertiesNVX {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMAGE_VIEW_ADDRESS_PROPERTIES_NVX;
}
impl Default for ImageViewAddressPropertiesNVX {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), device_address: Default::default(), size: Default::default() }
    }
}
impl std::fmt::Debug for ImageViewAddressPropertiesNVX {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImageViewAddressPropertiesNVX").field("s_type", &self.s_type).field("p_next", &self.p_next).field("device_address", &self.device_address).field("size", &self.size).finish()
    }
}
impl ImageViewAddressPropertiesNVX {
    #[inline]
    pub fn into_builder<'a>(self) -> ImageViewAddressPropertiesNVXBuilder<'a> {
        ImageViewAddressPropertiesNVXBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageViewAddressPropertiesNVX.html)) · Builder of [`ImageViewAddressPropertiesNVX`] <br/> VkImageViewAddressPropertiesNVX - Structure specifying the image view for handle queries\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::ImageViewAddressPropertiesNVX`] structure is defined as:\n\n```\n// Provided by VK_NVX_image_view_handle\ntypedef struct VkImageViewAddressPropertiesNVX {\n    VkStructureType    sType;\n    void*              pNext;\n    VkDeviceAddress    deviceAddress;\n    VkDeviceSize       size;\n} VkImageViewAddressPropertiesNVX;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::device_address`] is the device address of the image view.\n\n* [`Self::size`] is the size in bytes of the image view device memory.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkImageViewAddressPropertiesNVX-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMAGE_VIEW_ADDRESS_PROPERTIES_NVX`]\n\n* []() VUID-VkImageViewAddressPropertiesNVX-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceAddress`], [`crate::vk::DeviceSize`], [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_image_view_address_nvx`]\n"]
#[repr(transparent)]
pub struct ImageViewAddressPropertiesNVXBuilder<'a>(ImageViewAddressPropertiesNVX, std::marker::PhantomData<&'a ()>);
impl<'a> ImageViewAddressPropertiesNVXBuilder<'a> {
    #[inline]
    pub fn new() -> ImageViewAddressPropertiesNVXBuilder<'a> {
        ImageViewAddressPropertiesNVXBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn device_address(mut self, device_address: crate::vk1_0::DeviceAddress) -> Self {
        self.0.device_address = device_address as _;
        self
    }
    #[inline]
    pub fn size(mut self, size: crate::vk1_0::DeviceSize) -> Self {
        self.0.size = size as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImageViewAddressPropertiesNVX {
        self.0
    }
}
impl<'a> std::default::Default for ImageViewAddressPropertiesNVXBuilder<'a> {
    fn default() -> ImageViewAddressPropertiesNVXBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImageViewAddressPropertiesNVXBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImageViewAddressPropertiesNVXBuilder<'a> {
    type Target = ImageViewAddressPropertiesNVX;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImageViewAddressPropertiesNVXBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::nvx_image_view_handle`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageViewHandleNVX.html)) · Function <br/> vkGetImageViewHandleNVX - Get the handle for an image view for a specific descriptor type\n[](#_c_specification)C Specification\n----------\n\nTo get the handle for an image view, call:\n\n```\n// Provided by VK_NVX_image_view_handle\nuint32_t vkGetImageViewHandleNVX(\n    VkDevice                                    device,\n    const VkImageViewHandleInfoNVX*             pInfo);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image view.\n\n* [`Self::p_info`] describes the image view to query and type of handle.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageViewHandleNVX-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageViewHandleNVX-pInfo-parameter  \n  [`Self::p_info`] **must** be a valid pointer to a valid [`crate::vk::ImageViewHandleInfoNVX`] structure\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImageViewHandleInfoNVX`]\n"]
    #[doc(alias = "vkGetImageViewHandleNVX")]
    pub unsafe fn get_image_view_handle_nvx(&self, info: &crate::extensions::nvx_image_view_handle::ImageViewHandleInfoNVX) -> u32 {
        let _function = self.get_image_view_handle_nvx.expect(crate::NOT_LOADED_MESSAGE);
        let _return = _function(self.handle, info as _);
        _return
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetImageViewAddressNVX.html)) · Function <br/> vkGetImageViewAddressNVX - Get the device address of an image view\n[](#_c_specification)C Specification\n----------\n\nTo get the device address for an image view, call:\n\n```\n// Provided by VK_NVX_image_view_handle\nVkResult vkGetImageViewAddressNVX(\n    VkDevice                                    device,\n    VkImageView                                 imageView,\n    VkImageViewAddressPropertiesNVX*            pProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that owns the image view.\n\n* [`Self::image_view`] is a handle to the image view.\n\n* [`Self::p_properties`] contains the device address and size when the call\n  returns.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetImageViewAddressNVX-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetImageViewAddressNVX-imageView-parameter  \n  [`Self::image_view`] **must** be a valid [`crate::vk::ImageView`] handle\n\n* []() VUID-vkGetImageViewAddressNVX-pProperties-parameter  \n  [`Self::p_properties`] **must** be a valid pointer to a [`crate::vk::ImageViewAddressPropertiesNVX`] structure\n\n* []() VUID-vkGetImageViewAddressNVX-imageView-parent  \n  [`Self::image_view`] **must** have been created, allocated, or retrieved from [`Self::device`]\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_UNKNOWN`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::ImageView`], [`crate::vk::ImageViewAddressPropertiesNVX`]\n"]
    #[doc(alias = "vkGetImageViewAddressNVX")]
    pub unsafe fn get_image_view_address_nvx(&self, image_view: crate::vk1_0::ImageView, properties: Option<crate::extensions::nvx_image_view_handle::ImageViewAddressPropertiesNVX>) -> crate::utils::VulkanResult<crate::extensions::nvx_image_view_handle::ImageViewAddressPropertiesNVX> {
        let _function = self.get_image_view_address_nvx.expect(crate::NOT_LOADED_MESSAGE);
        let mut properties = match properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, image_view as _, &mut properties);
        crate::utils::VulkanResult::new(_return, {
            properties.p_next = std::ptr::null_mut() as _;
            properties
        })
    }
}
