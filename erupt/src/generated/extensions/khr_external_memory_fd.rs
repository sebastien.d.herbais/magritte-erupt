#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_MEMORY_FD_SPEC_VERSION")]
pub const KHR_EXTERNAL_MEMORY_FD_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME")]
pub const KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_external_memory_fd");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_FD_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryFdKHR");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_GET_MEMORY_FD_PROPERTIES_KHR: *const std::os::raw::c_char = crate::cstr!("vkGetMemoryFdPropertiesKHR");
#[doc = "Provided by [`crate::extensions::khr_external_memory_fd`]"]
impl crate::vk1_0::StructureType {
    pub const IMPORT_MEMORY_FD_INFO_KHR: Self = Self(1000074000);
    pub const MEMORY_FD_PROPERTIES_KHR: Self = Self(1000074001);
    pub const MEMORY_GET_FD_INFO_KHR: Self = Self(1000074002);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryFdKHR.html)) · Function <br/> vkGetMemoryFdKHR - Get a POSIX file descriptor for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export a POSIX file descriptor referencing the payload of a Vulkan device\nmemory object, call:\n\n```\n// Provided by VK_KHR_external_memory_fd\nVkResult vkGetMemoryFdKHR(\n    VkDevice                                    device,\n    const VkMemoryGetFdInfoKHR*                 pGetFdInfo,\n    int*                                        pFd);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_get_fd_info`] is a pointer to a [`crate::vk::MemoryGetFdInfoKHR`] structure\n  containing parameters of the export operation.\n\n* [`Self::p_fd`] will return a file descriptor referencing the payload of the\n  device memory object.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_memory_fd_khr`] **must** create a new file descriptor\nholding a reference to the memory object’s payload and transfer ownership of\nthe file descriptor to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nfile descriptor using the `close` system call when it is no longer\nneeded, or by importing a Vulkan memory object from it.\nWhere supported by the operating system, the implementation **must** set the\nfile descriptor to be closed automatically when an `execve` system call\nis made.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryFdKHR-pGetFdInfo-parameter  \n  [`Self::p_get_fd_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetFdInfoKHR`] structure\n\n* []() VUID-vkGetMemoryFdKHR-pFd-parameter  \n  [`Self::p_fd`] **must** be a valid pointer to an `int` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetFdInfoKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryFdKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, p_get_fd_info: *const crate::extensions::khr_external_memory_fd::MemoryGetFdInfoKHR, p_fd: *mut std::os::raw::c_int) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryFdPropertiesKHR.html)) · Function <br/> vkGetMemoryFdPropertiesKHR - Get Properties of External Memory File Descriptors\n[](#_c_specification)C Specification\n----------\n\nPOSIX file descriptor memory handles compatible with Vulkan **may** also be\ncreated by non-Vulkan APIs using methods beyond the scope of this\nspecification.\nTo determine the correct parameters to use when importing such handles,\ncall:\n\n```\n// Provided by VK_KHR_external_memory_fd\nVkResult vkGetMemoryFdPropertiesKHR(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    int                                         fd,\n    VkMemoryFdPropertiesKHR*                    pMemoryFdProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::fd`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::fd`].\n\n* [`Self::fd`] is the handle which will be imported.\n\n* [`Self::p_memory_fd_properties`] is a pointer to a[`crate::vk::MemoryFdPropertiesKHR`] structure in which the properties of the\n  handle [`Self::fd`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-fd-00673  \n  [`Self::fd`] **must** be an external memory handle created outside of the\n  Vulkan API\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-handleType-00674  \n  [`Self::handle_type`] **must** not be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-pMemoryFdProperties-parameter  \n  [`Self::p_memory_fd_properties`] **must** be a valid pointer to a [`crate::vk::MemoryFdPropertiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryFdPropertiesKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkGetMemoryFdPropertiesKHR = unsafe extern "system" fn(device: crate::vk1_0::Device, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, fd: std::os::raw::c_int, p_memory_fd_properties: *mut crate::extensions::khr_external_memory_fd::MemoryFdPropertiesKHR) -> crate::vk1_0::Result;
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryFdInfoKHR> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, ImportMemoryFdInfoKHRBuilder<'_>> for crate::vk1_0::MemoryAllocateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryFdInfoKHR.html)) · Structure <br/> VkImportMemoryFdInfoKHR - Import memory created on the same physical device from a file descriptor\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a POSIX file descriptor handle, add a[`crate::vk::ImportMemoryFdInfoKHR`] structure to the [`Self::p_next`] chain of the[`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportMemoryFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkImportMemoryFdInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    int                                   fd;\n} VkImportMemoryFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the handle type of [`Self::fd`].\n\n* [`Self::fd`] is the external handle to import.\n[](#_description)Description\n----------\n\nImporting memory from a file descriptor transfers ownership of the file\ndescriptor from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the file descriptor\nafter a successful import.\nThe imported memory object holds a reference to its payload.\n\nApplications **can** import the same payload into multiple instances of Vulkan,\ninto the same instance from which it was exported, and multiple times into a\ngiven Vulkan instance.\nIn all cases, each import operation **must** create a distinct[`crate::vk::DeviceMemory`] object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00667  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported by [`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-00668  \n   The memory from which [`Self::fd`] was exported **must** have been created on\n  the same underlying physical device as `device`\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00669  \n   If [`Self::handle_type`] is not `0`, it **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00670  \n   If [`Self::handle_type`] is not `0`, [`Self::fd`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-01746  \n   The memory represented by [`Self::fd`] **must** have been created from a\n  physical device and driver that is compatible with `device` and[`Self::handle_type`], as described in[https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-01520  \n  [`Self::fd`] **must** obey any requirements listed for [`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_FD_INFO_KHR`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-parameter  \n   If [`Self::handle_type`] is not `0`, [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkImportMemoryFdInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ImportMemoryFdInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
    pub fd: std::os::raw::c_int,
}
impl ImportMemoryFdInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::IMPORT_MEMORY_FD_INFO_KHR;
}
impl Default for ImportMemoryFdInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), handle_type: Default::default(), fd: Default::default() }
    }
}
impl std::fmt::Debug for ImportMemoryFdInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("ImportMemoryFdInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("handle_type", &self.handle_type).field("fd", &self.fd).finish()
    }
}
impl ImportMemoryFdInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> ImportMemoryFdInfoKHRBuilder<'a> {
        ImportMemoryFdInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImportMemoryFdInfoKHR.html)) · Builder of [`ImportMemoryFdInfoKHR`] <br/> VkImportMemoryFdInfoKHR - Import memory created on the same physical device from a file descriptor\n[](#_c_specification)C Specification\n----------\n\nTo import memory from a POSIX file descriptor handle, add a[`crate::vk::ImportMemoryFdInfoKHR`] structure to the [`Self::p_next`] chain of the[`crate::vk::MemoryAllocateInfo`] structure.\nThe [`crate::vk::ImportMemoryFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkImportMemoryFdInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n    int                                   fd;\n} VkImportMemoryFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the handle type of [`Self::fd`].\n\n* [`Self::fd`] is the external handle to import.\n[](#_description)Description\n----------\n\nImporting memory from a file descriptor transfers ownership of the file\ndescriptor from the application to the Vulkan implementation.\nThe application **must** not perform any operations on the file descriptor\nafter a successful import.\nThe imported memory object holds a reference to its payload.\n\nApplications **can** import the same payload into multiple instances of Vulkan,\ninto the same instance from which it was exported, and multiple times into a\ngiven Vulkan instance.\nIn all cases, each import operation **must** create a distinct[`crate::vk::DeviceMemory`] object.\n\nValid Usage\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00667  \n   If [`Self::handle_type`] is not `0`, it **must** be supported for import, as\n  reported by [`crate::vk::ExternalImageFormatProperties`] or[`crate::vk::ExternalBufferProperties`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-00668  \n   The memory from which [`Self::fd`] was exported **must** have been created on\n  the same underlying physical device as `device`\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00669  \n   If [`Self::handle_type`] is not `0`, it **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-00670  \n   If [`Self::handle_type`] is not `0`, [`Self::fd`] **must** be a valid handle of the\n  type specified by [`Self::handle_type`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-01746  \n   The memory represented by [`Self::fd`] **must** have been created from a\n  physical device and driver that is compatible with `device` and[`Self::handle_type`], as described in[https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\n* []() VUID-VkImportMemoryFdInfoKHR-fd-01520  \n  [`Self::fd`] **must** obey any requirements listed for [`Self::handle_type`] in[external memory handle\n  types compatibility](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#external-memory-handle-types-compatibility)\n\nValid Usage (Implicit)\n\n* []() VUID-VkImportMemoryFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::IMPORT_MEMORY_FD_INFO_KHR`]\n\n* []() VUID-VkImportMemoryFdInfoKHR-handleType-parameter  \n   If [`Self::handle_type`] is not `0`, [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct ImportMemoryFdInfoKHRBuilder<'a>(ImportMemoryFdInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> ImportMemoryFdInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> ImportMemoryFdInfoKHRBuilder<'a> {
        ImportMemoryFdInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    pub fn fd(mut self, fd: std::os::raw::c_int) -> Self {
        self.0.fd = fd as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> ImportMemoryFdInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for ImportMemoryFdInfoKHRBuilder<'a> {
    fn default() -> ImportMemoryFdInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for ImportMemoryFdInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for ImportMemoryFdInfoKHRBuilder<'a> {
    type Target = ImportMemoryFdInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for ImportMemoryFdInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryFdPropertiesKHR.html)) · Structure <br/> VkMemoryFdPropertiesKHR - Properties of External Memory File Descriptors\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryFdPropertiesKHR`] structure returned is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkMemoryFdPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryFdPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified file descriptor **can** be imported as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryFdPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_FD_PROPERTIES_KHR`]\n\n* []() VUID-VkMemoryFdPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_fd_properties_khr`]\n"]
#[doc(alias = "VkMemoryFdPropertiesKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryFdPropertiesKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub memory_type_bits: u32,
}
impl MemoryFdPropertiesKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_FD_PROPERTIES_KHR;
}
impl Default for MemoryFdPropertiesKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), memory_type_bits: Default::default() }
    }
}
impl std::fmt::Debug for MemoryFdPropertiesKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryFdPropertiesKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory_type_bits", &self.memory_type_bits).finish()
    }
}
impl MemoryFdPropertiesKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryFdPropertiesKHRBuilder<'a> {
        MemoryFdPropertiesKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryFdPropertiesKHR.html)) · Builder of [`MemoryFdPropertiesKHR`] <br/> VkMemoryFdPropertiesKHR - Properties of External Memory File Descriptors\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryFdPropertiesKHR`] structure returned is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkMemoryFdPropertiesKHR {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           memoryTypeBits;\n} VkMemoryFdPropertiesKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory_type_bits`] is a bitmask containing one bit set for every\n  memory type which the specified file descriptor **can** be imported as.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryFdPropertiesKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_FD_PROPERTIES_KHR`]\n\n* []() VUID-VkMemoryFdPropertiesKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_fd_properties_khr`]\n"]
#[repr(transparent)]
pub struct MemoryFdPropertiesKHRBuilder<'a>(MemoryFdPropertiesKHR, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryFdPropertiesKHRBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryFdPropertiesKHRBuilder<'a> {
        MemoryFdPropertiesKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory_type_bits(mut self, memory_type_bits: u32) -> Self {
        self.0.memory_type_bits = memory_type_bits as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryFdPropertiesKHR {
        self.0
    }
}
impl<'a> std::default::Default for MemoryFdPropertiesKHRBuilder<'a> {
    fn default() -> MemoryFdPropertiesKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryFdPropertiesKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryFdPropertiesKHRBuilder<'a> {
    type Target = MemoryFdPropertiesKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryFdPropertiesKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetFdInfoKHR.html)) · Structure <br/> VkMemoryGetFdInfoKHR - Structure describing a POSIX FD semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkMemoryGetFdInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the handle will be\n  exported.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the file descriptor exported depend on the value of[`Self::handle_type`].\nSee [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) for a description of the\nproperties of the defined external memory handle types.\n\n|   |Note<br/><br/>The size of the exported file **may** be larger than the size requested by[`crate::vk::MemoryAllocateInfo::allocation_size`].<br/>If [`Self::handle_type`] is [`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`],<br/>then the application **can** query the file’s actual size with[lseek(2)](man:lseek(2)).|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-00671  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-00672  \n  [`Self::handle_type`] **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_FD_INFO_KHR`]\n\n* []() VUID-VkMemoryGetFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetFdInfoKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_fd_khr`]\n"]
#[doc(alias = "VkMemoryGetFdInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MemoryGetFdInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub memory: crate::vk1_0::DeviceMemory,
    pub handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits,
}
impl MemoryGetFdInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::MEMORY_GET_FD_INFO_KHR;
}
impl Default for MemoryGetFdInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), memory: Default::default(), handle_type: Default::default() }
    }
}
impl std::fmt::Debug for MemoryGetFdInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("MemoryGetFdInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("memory", &self.memory).field("handle_type", &self.handle_type).finish()
    }
}
impl MemoryGetFdInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> MemoryGetFdInfoKHRBuilder<'a> {
        MemoryGetFdInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryGetFdInfoKHR.html)) · Builder of [`MemoryGetFdInfoKHR`] <br/> VkMemoryGetFdInfoKHR - Structure describing a POSIX FD semaphore export operation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::MemoryGetFdInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_external_memory_fd\ntypedef struct VkMemoryGetFdInfoKHR {\n    VkStructureType                       sType;\n    const void*                           pNext;\n    VkDeviceMemory                        memory;\n    VkExternalMemoryHandleTypeFlagBits    handleType;\n} VkMemoryGetFdInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::memory`] is the memory object from which the handle will be\n  exported.\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of handle requested.\n[](#_description)Description\n----------\n\nThe properties of the file descriptor exported depend on the value of[`Self::handle_type`].\nSee [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) for a description of the\nproperties of the defined external memory handle types.\n\n|   |Note<br/><br/>The size of the exported file **may** be larger than the size requested by[`crate::vk::MemoryAllocateInfo::allocation_size`].<br/>If [`Self::handle_type`] is [`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`],<br/>then the application **can** query the file’s actual size with[lseek(2)](man:lseek(2)).|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nValid Usage\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-00671  \n  [`Self::handle_type`] **must** have been included in[`crate::vk::ExportMemoryAllocateInfo::handle_types`] when [`Self::memory`]was created\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-00672  \n  [`Self::handle_type`] **must** be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD`] or[`crate::vk::ExternalMemoryHandleTypeFlagBits::DMA_BUF_EXT`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkMemoryGetFdInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::MEMORY_GET_FD_INFO_KHR`]\n\n* []() VUID-VkMemoryGetFdInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkMemoryGetFdInfoKHR-memory-parameter  \n  [`Self::memory`] **must** be a valid [`crate::vk::DeviceMemory`] handle\n\n* []() VUID-VkMemoryGetFdInfoKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DeviceMemory`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::StructureType`], [`crate::vk::DeviceLoader::get_memory_fd_khr`]\n"]
#[repr(transparent)]
pub struct MemoryGetFdInfoKHRBuilder<'a>(MemoryGetFdInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> MemoryGetFdInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> MemoryGetFdInfoKHRBuilder<'a> {
        MemoryGetFdInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn memory(mut self, memory: crate::vk1_0::DeviceMemory) -> Self {
        self.0.memory = memory as _;
        self
    }
    #[inline]
    pub fn handle_type(mut self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits) -> Self {
        self.0.handle_type = handle_type as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> MemoryGetFdInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for MemoryGetFdInfoKHRBuilder<'a> {
    fn default() -> MemoryGetFdInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for MemoryGetFdInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for MemoryGetFdInfoKHRBuilder<'a> {
    type Target = MemoryGetFdInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for MemoryGetFdInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_external_memory_fd`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryFdKHR.html)) · Function <br/> vkGetMemoryFdKHR - Get a POSIX file descriptor for a memory object\n[](#_c_specification)C Specification\n----------\n\nTo export a POSIX file descriptor referencing the payload of a Vulkan device\nmemory object, call:\n\n```\n// Provided by VK_KHR_external_memory_fd\nVkResult vkGetMemoryFdKHR(\n    VkDevice                                    device,\n    const VkMemoryGetFdInfoKHR*                 pGetFdInfo,\n    int*                                        pFd);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that created the device memory being\n  exported.\n\n* [`Self::p_get_fd_info`] is a pointer to a [`crate::vk::MemoryGetFdInfoKHR`] structure\n  containing parameters of the export operation.\n\n* [`Self::p_fd`] will return a file descriptor referencing the payload of the\n  device memory object.\n[](#_description)Description\n----------\n\nEach call to [`crate::vk::DeviceLoader::get_memory_fd_khr`] **must** create a new file descriptor\nholding a reference to the memory object’s payload and transfer ownership of\nthe file descriptor to the application.\nTo avoid leaking resources, the application **must** release ownership of the\nfile descriptor using the `close` system call when it is no longer\nneeded, or by importing a Vulkan memory object from it.\nWhere supported by the operating system, the implementation **must** set the\nfile descriptor to be closed automatically when an `execve` system call\nis made.\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryFdKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryFdKHR-pGetFdInfo-parameter  \n  [`Self::p_get_fd_info`] **must** be a valid pointer to a valid [`crate::vk::MemoryGetFdInfoKHR`] structure\n\n* []() VUID-vkGetMemoryFdKHR-pFd-parameter  \n  [`Self::p_fd`] **must** be a valid pointer to an `int` value\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_TOO_MANY_OBJECTS`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [`crate::vk::MemoryGetFdInfoKHR`]\n"]
    #[doc(alias = "vkGetMemoryFdKHR")]
    pub unsafe fn get_memory_fd_khr(&self, get_fd_info: &crate::extensions::khr_external_memory_fd::MemoryGetFdInfoKHR) -> crate::utils::VulkanResult<std::os::raw::c_int> {
        let _function = self.get_memory_fd_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut fd = Default::default();
        let _return = _function(self.handle, get_fd_info as _, &mut fd);
        crate::utils::VulkanResult::new(_return, fd)
    }
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetMemoryFdPropertiesKHR.html)) · Function <br/> vkGetMemoryFdPropertiesKHR - Get Properties of External Memory File Descriptors\n[](#_c_specification)C Specification\n----------\n\nPOSIX file descriptor memory handles compatible with Vulkan **may** also be\ncreated by non-Vulkan APIs using methods beyond the scope of this\nspecification.\nTo determine the correct parameters to use when importing such handles,\ncall:\n\n```\n// Provided by VK_KHR_external_memory_fd\nVkResult vkGetMemoryFdPropertiesKHR(\n    VkDevice                                    device,\n    VkExternalMemoryHandleTypeFlagBits          handleType,\n    int                                         fd,\n    VkMemoryFdPropertiesKHR*                    pMemoryFdProperties);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::device`] is the logical device that will be importing [`Self::fd`].\n\n* [`Self::handle_type`] is a [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n  specifying the type of the handle [`Self::fd`].\n\n* [`Self::fd`] is the handle which will be imported.\n\n* [`Self::p_memory_fd_properties`] is a pointer to a[`crate::vk::MemoryFdPropertiesKHR`] structure in which the properties of the\n  handle [`Self::fd`] are returned.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-fd-00673  \n  [`Self::fd`] **must** be an external memory handle created outside of the\n  Vulkan API\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-handleType-00674  \n  [`Self::handle_type`] **must** not be[`crate::vk::ExternalMemoryHandleTypeFlagBits::OPAQUE_FD_KHR`]\n\nValid Usage (Implicit)\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-device-parameter  \n  [`Self::device`] **must** be a valid [`crate::vk::Device`] handle\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-handleType-parameter  \n  [`Self::handle_type`] **must** be a valid [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html) value\n\n* []() VUID-vkGetMemoryFdPropertiesKHR-pMemoryFdProperties-parameter  \n  [`Self::p_memory_fd_properties`] **must** be a valid pointer to a [`crate::vk::MemoryFdPropertiesKHR`] structure\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::Device`], [VkExternalMemoryHandleTypeFlagBits](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkExternalMemoryHandleTypeFlagBits.html), [`crate::vk::MemoryFdPropertiesKHR`]\n"]
    #[doc(alias = "vkGetMemoryFdPropertiesKHR")]
    pub unsafe fn get_memory_fd_properties_khr(&self, handle_type: crate::vk1_1::ExternalMemoryHandleTypeFlagBits, fd: std::os::raw::c_int, memory_fd_properties: Option<crate::extensions::khr_external_memory_fd::MemoryFdPropertiesKHR>) -> crate::utils::VulkanResult<crate::extensions::khr_external_memory_fd::MemoryFdPropertiesKHR> {
        let _function = self.get_memory_fd_properties_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut memory_fd_properties = match memory_fd_properties {
            Some(v) => v,
            None => Default::default(),
        };
        let _return = _function(self.handle, handle_type as _, fd as _, &mut memory_fd_properties);
        crate::utils::VulkanResult::new(_return, {
            memory_fd_properties.p_next = std::ptr::null_mut() as _;
            memory_fd_properties
        })
    }
}
