#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISCARD_RECTANGLES_SPEC_VERSION")]
pub const EXT_DISCARD_RECTANGLES_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_EXT_DISCARD_RECTANGLES_EXTENSION_NAME")]
pub const EXT_DISCARD_RECTANGLES_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_EXT_discard_rectangles");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CMD_SET_DISCARD_RECTANGLE_EXT: *const std::os::raw::c_char = crate::cstr!("vkCmdSetDiscardRectangleEXT");
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineDiscardRectangleStateCreateFlagsEXT.html)) · Bitmask of [`PipelineDiscardRectangleStateCreateFlagBitsEXT`] <br/> VkPipelineDiscardRectangleStateCreateFlagsEXT - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef VkFlags VkPipelineDiscardRectangleStateCreateFlagsEXT;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::PipelineDiscardRectangleStateCreateFlagBitsEXT`] is a bitmask type for\nsetting a mask, but is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineDiscardRectangleStateCreateInfoEXT`]\n"] # [doc (alias = "VkPipelineDiscardRectangleStateCreateFlagsEXT")] # [derive (Default)] # [repr (transparent)] pub struct PipelineDiscardRectangleStateCreateFlagsEXT : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`PipelineDiscardRectangleStateCreateFlagsEXT`] <br/> "]
#[doc(alias = "VkPipelineDiscardRectangleStateCreateFlagBitsEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct PipelineDiscardRectangleStateCreateFlagBitsEXT(pub u32);
impl PipelineDiscardRectangleStateCreateFlagBitsEXT {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> PipelineDiscardRectangleStateCreateFlagsEXT {
        PipelineDiscardRectangleStateCreateFlagsEXT::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for PipelineDiscardRectangleStateCreateFlagBitsEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_discard_rectangles`]"]
impl crate::vk1_0::DynamicState {
    pub const DISCARD_RECTANGLE_EXT: Self = Self(1000099000);
}
#[doc = "Provided by [`crate::extensions::ext_discard_rectangles`]"]
impl crate::vk1_0::StructureType {
    pub const PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT: Self = Self(1000099000);
    pub const PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT: Self = Self(1000099001);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDiscardRectangleModeEXT.html)) · Enum <br/> VkDiscardRectangleModeEXT - Specify the discard rectangle mode\n[](#_c_specification)C Specification\n----------\n\n[`crate::vk::DiscardRectangleModeEXT`] values are:\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef enum VkDiscardRectangleModeEXT {\n    VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT = 0,\n    VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT = 1,\n} VkDiscardRectangleModeEXT;\n```\n[](#_description)Description\n----------\n\n* [`Self::INCLUSIVE_EXT`] specifies that the discard\n  rectangle test is inclusive.\n\n* [`Self::EXCLUSIVE_EXT`] specifies that the discard\n  rectangle test is exclusive.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineDiscardRectangleStateCreateInfoEXT`]\n"]
#[doc(alias = "VkDiscardRectangleModeEXT")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct DiscardRectangleModeEXT(pub i32);
impl std::fmt::Debug for DiscardRectangleModeEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::INCLUSIVE_EXT => "INCLUSIVE_EXT",
            &Self::EXCLUSIVE_EXT => "EXCLUSIVE_EXT",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::ext_discard_rectangles`]"]
impl crate::extensions::ext_discard_rectangles::DiscardRectangleModeEXT {
    pub const INCLUSIVE_EXT: Self = Self(0);
    pub const EXCLUSIVE_EXT: Self = Self(1);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetDiscardRectangleEXT.html)) · Function <br/> vkCmdSetDiscardRectangleEXT - Set discard rectangles dynamically\n[](#_c_specification)C Specification\n----------\n\nThe discard rectangles **can** be set dynamically with the command:\n\n```\n// Provided by VK_EXT_discard_rectangles\nvoid vkCmdSetDiscardRectangleEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstDiscardRectangle,\n    uint32_t                                    discardRectangleCount,\n    const VkRect2D*                             pDiscardRectangles);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_discard_rectangle`] is the index of the first discard rectangle\n  whose state is updated by the command.\n\n* [`Self::discard_rectangle_count`] is the number of discard rectangles whose\n  state are updated by the command.\n\n* [`Self::p_discard_rectangles`] is a pointer to an array of [`crate::vk::Rect2D`]structures specifying discard rectangles.\n[](#_description)Description\n----------\n\nThe discard rectangle taken from element i of [`Self::p_discard_rectangles`]replace the current state for the discard rectangle at index[`Self::first_discard_rectangle`] + i, for i in [0,[`Self::discard_rectangle_count`]).\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-firstDiscardRectangle-00585  \n   The sum of [`Self::first_discard_rectangle`] and [`Self::discard_rectangle_count`]**must** be less than or equal to[`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT::max_discard_rectangles`]\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-x-00587  \n   The `x` and `y` member of `offset` in each [`crate::vk::Rect2D`]element of [`Self::p_discard_rectangles`] **must** be greater than or equal to`0`\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-offset-00588  \n   Evaluation of (`offset.x` + `extent.width`) in each[`crate::vk::Rect2D`] element of [`Self::p_discard_rectangles`] **must** not cause a\n  signed integer addition overflow\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-offset-00589  \n   Evaluation of (`offset.y` + `extent.height`) in each[`crate::vk::Rect2D`] element of [`Self::p_discard_rectangles`] **must** not cause a\n  signed integer addition overflow\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-viewportScissor2D-04788  \n   If this command is recorded in a secondary command buffer with[`crate::vk::CommandBufferInheritanceViewportScissorInfoNV::viewport_scissor2_d`]enabled, then this function **must** not be called.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-pDiscardRectangles-parameter  \n  [`Self::p_discard_rectangles`] **must** be a valid pointer to an array of [`Self::discard_rectangle_count`] [`crate::vk::Rect2D`] structures\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-discardRectangleCount-arraylength  \n  [`Self::discard_rectangle_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Rect2D`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCmdSetDiscardRectangleEXT = unsafe extern "system" fn(command_buffer: crate::vk1_0::CommandBuffer, first_discard_rectangle: u32, discard_rectangle_count: u32, p_discard_rectangles: *const crate::vk1_0::Rect2D) -> ();
impl<'a> crate::ExtendableFromConst<'a, PipelineDiscardRectangleStateCreateInfoEXT> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineDiscardRectangleStateCreateInfoEXTBuilder<'_>> for crate::vk1_0::GraphicsPipelineCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDiscardRectanglePropertiesEXT> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
impl<'a> crate::ExtendableFromMut<'a, PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'_>> for crate::vk1_1::PhysicalDeviceProperties2Builder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDiscardRectanglePropertiesEXT.html)) · Structure <br/> VkPhysicalDeviceDiscardRectanglePropertiesEXT - Structure describing discard rectangle limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef struct VkPhysicalDeviceDiscardRectanglePropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxDiscardRectangles;\n} VkPhysicalDeviceDiscardRectanglePropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::max_discard_rectangles`] is the\n  maximum number of active discard rectangles that **can** be specified.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDiscardRectanglePropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPhysicalDeviceDiscardRectanglePropertiesEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PhysicalDeviceDiscardRectanglePropertiesEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *mut std::ffi::c_void,
    pub max_discard_rectangles: u32,
}
impl PhysicalDeviceDiscardRectanglePropertiesEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT;
}
impl Default for PhysicalDeviceDiscardRectanglePropertiesEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null_mut(), max_discard_rectangles: Default::default() }
    }
}
impl std::fmt::Debug for PhysicalDeviceDiscardRectanglePropertiesEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PhysicalDeviceDiscardRectanglePropertiesEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("max_discard_rectangles", &self.max_discard_rectangles).finish()
    }
}
impl PhysicalDeviceDiscardRectanglePropertiesEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
        PhysicalDeviceDiscardRectanglePropertiesEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPhysicalDeviceDiscardRectanglePropertiesEXT.html)) · Builder of [`PhysicalDeviceDiscardRectanglePropertiesEXT`] <br/> VkPhysicalDeviceDiscardRectanglePropertiesEXT - Structure describing discard rectangle limits that can be supported by an implementation\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef struct VkPhysicalDeviceDiscardRectanglePropertiesEXT {\n    VkStructureType    sType;\n    void*              pNext;\n    uint32_t           maxDiscardRectangles;\n} VkPhysicalDeviceDiscardRectanglePropertiesEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* []() [`Self::max_discard_rectangles`] is the\n  maximum number of active discard rectangles that **can** be specified.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`] structure is included in the [`Self::p_next`] chain of the[`crate::vk::PhysicalDeviceProperties2`] structure passed to[`crate::vk::PFN_vkGetPhysicalDeviceProperties2`], it is filled in with each\ncorresponding implementation-dependent property.\n\nValid Usage (Implicit)\n\n* []() VUID-VkPhysicalDeviceDiscardRectanglePropertiesEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a>(PhysicalDeviceDiscardRectanglePropertiesEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
        PhysicalDeviceDiscardRectanglePropertiesEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn max_discard_rectangles(mut self, max_discard_rectangles: u32) -> Self {
        self.0.max_discard_rectangles = max_discard_rectangles as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PhysicalDeviceDiscardRectanglePropertiesEXT {
        self.0
    }
}
impl<'a> std::default::Default for PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
    fn default() -> PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
    type Target = PhysicalDeviceDiscardRectanglePropertiesEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PhysicalDeviceDiscardRectanglePropertiesEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineDiscardRectangleStateCreateInfoEXT.html)) · Structure <br/> VkPipelineDiscardRectangleStateCreateInfoEXT - Structure specifying discard rectangle\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineDiscardRectangleStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef struct VkPipelineDiscardRectangleStateCreateInfoEXT {\n    VkStructureType                                  sType;\n    const void*                                      pNext;\n    VkPipelineDiscardRectangleStateCreateFlagsEXT    flags;\n    VkDiscardRectangleModeEXT                        discardRectangleMode;\n    uint32_t                                         discardRectangleCount;\n    const VkRect2D*                                  pDiscardRectangles;\n} VkPipelineDiscardRectangleStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::discard_rectangle_mode`] is a [`crate::vk::DiscardRectangleModeEXT`] value\n  determining whether the discard rectangle test is inclusive or\n  exclusive.\n\n* [`Self::discard_rectangle_count`] is the number of discard rectangles to use.\n\n* [`Self::p_discard_rectangles`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining discard rectangles.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`] dynamic state is enabled\nfor a pipeline, the [`Self::p_discard_rectangles`] member is ignored.\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::GraphicsPipelineCreateInfo`], it defines parameters of the discard\nrectangle test.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with a [`Self::discard_rectangle_count`] of `0`.\n\nValid Usage\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-discardRectangleCount-00582  \n  [`Self::discard_rectangle_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`]::`maxDiscardRectangles`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-discardRectangleMode-parameter  \n  [`Self::discard_rectangle_mode`] **must** be a valid [`crate::vk::DiscardRectangleModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DiscardRectangleModeEXT`], [`crate::vk::PipelineDiscardRectangleStateCreateFlagBitsEXT`], [`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineDiscardRectangleStateCreateInfoEXT")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineDiscardRectangleStateCreateInfoEXT {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::ext_discard_rectangles::PipelineDiscardRectangleStateCreateFlagsEXT,
    pub discard_rectangle_mode: crate::extensions::ext_discard_rectangles::DiscardRectangleModeEXT,
    pub discard_rectangle_count: u32,
    pub p_discard_rectangles: *const crate::vk1_0::Rect2D,
}
impl PipelineDiscardRectangleStateCreateInfoEXT {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT;
}
impl Default for PipelineDiscardRectangleStateCreateInfoEXT {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), discard_rectangle_mode: Default::default(), discard_rectangle_count: Default::default(), p_discard_rectangles: std::ptr::null() }
    }
}
impl std::fmt::Debug for PipelineDiscardRectangleStateCreateInfoEXT {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineDiscardRectangleStateCreateInfoEXT").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("discard_rectangle_mode", &self.discard_rectangle_mode).field("discard_rectangle_count", &self.discard_rectangle_count).field("p_discard_rectangles", &self.p_discard_rectangles).finish()
    }
}
impl PipelineDiscardRectangleStateCreateInfoEXT {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
        PipelineDiscardRectangleStateCreateInfoEXTBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineDiscardRectangleStateCreateInfoEXT.html)) · Builder of [`PipelineDiscardRectangleStateCreateInfoEXT`] <br/> VkPipelineDiscardRectangleStateCreateInfoEXT - Structure specifying discard rectangle\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::PipelineDiscardRectangleStateCreateInfoEXT`] structure is defined\nas:\n\n```\n// Provided by VK_EXT_discard_rectangles\ntypedef struct VkPipelineDiscardRectangleStateCreateInfoEXT {\n    VkStructureType                                  sType;\n    const void*                                      pNext;\n    VkPipelineDiscardRectangleStateCreateFlagsEXT    flags;\n    VkDiscardRectangleModeEXT                        discardRectangleMode;\n    uint32_t                                         discardRectangleCount;\n    const VkRect2D*                                  pDiscardRectangles;\n} VkPipelineDiscardRectangleStateCreateInfoEXT;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::discard_rectangle_mode`] is a [`crate::vk::DiscardRectangleModeEXT`] value\n  determining whether the discard rectangle test is inclusive or\n  exclusive.\n\n* [`Self::discard_rectangle_count`] is the number of discard rectangles to use.\n\n* [`Self::p_discard_rectangles`] is a pointer to an array of [`crate::vk::Rect2D`]structures defining discard rectangles.\n[](#_description)Description\n----------\n\nIf the [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`] dynamic state is enabled\nfor a pipeline, the [`Self::p_discard_rectangles`] member is ignored.\n\nWhen this structure is included in the [`Self::p_next`] chain of[`crate::vk::GraphicsPipelineCreateInfo`], it defines parameters of the discard\nrectangle test.\nIf this structure is not included in the [`Self::p_next`] chain, it is equivalent\nto specifying this structure with a [`Self::discard_rectangle_count`] of `0`.\n\nValid Usage\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-discardRectangleCount-00582  \n  [`Self::discard_rectangle_count`] **must** be less than or equal to[`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT`]::`maxDiscardRectangles`\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT`]\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n\n* []() VUID-VkPipelineDiscardRectangleStateCreateInfoEXT-discardRectangleMode-parameter  \n  [`Self::discard_rectangle_mode`] **must** be a valid [`crate::vk::DiscardRectangleModeEXT`] value\n[](#_see_also)See Also\n----------\n\n[`crate::vk::DiscardRectangleModeEXT`], [`crate::vk::PipelineDiscardRectangleStateCreateFlagBitsEXT`], [`crate::vk::Rect2D`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a>(PipelineDiscardRectangleStateCreateInfoEXT, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
        PipelineDiscardRectangleStateCreateInfoEXTBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::ext_discard_rectangles::PipelineDiscardRectangleStateCreateFlagsEXT) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn discard_rectangle_mode(mut self, discard_rectangle_mode: crate::extensions::ext_discard_rectangles::DiscardRectangleModeEXT) -> Self {
        self.0.discard_rectangle_mode = discard_rectangle_mode as _;
        self
    }
    #[inline]
    pub fn discard_rectangles(mut self, discard_rectangles: &'a [crate::vk1_0::Rect2DBuilder]) -> Self {
        self.0.p_discard_rectangles = discard_rectangles.as_ptr() as _;
        self.0.discard_rectangle_count = discard_rectangles.len() as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineDiscardRectangleStateCreateInfoEXT {
        self.0
    }
}
impl<'a> std::default::Default for PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
    fn default() -> PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
    type Target = PipelineDiscardRectangleStateCreateInfoEXT;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineDiscardRectangleStateCreateInfoEXTBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::ext_discard_rectangles`]"]
impl crate::DeviceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCmdSetDiscardRectangleEXT.html)) · Function <br/> vkCmdSetDiscardRectangleEXT - Set discard rectangles dynamically\n[](#_c_specification)C Specification\n----------\n\nThe discard rectangles **can** be set dynamically with the command:\n\n```\n// Provided by VK_EXT_discard_rectangles\nvoid vkCmdSetDiscardRectangleEXT(\n    VkCommandBuffer                             commandBuffer,\n    uint32_t                                    firstDiscardRectangle,\n    uint32_t                                    discardRectangleCount,\n    const VkRect2D*                             pDiscardRectangles);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::command_buffer`] is the command buffer into which the command will be\n  recorded.\n\n* [`Self::first_discard_rectangle`] is the index of the first discard rectangle\n  whose state is updated by the command.\n\n* [`Self::discard_rectangle_count`] is the number of discard rectangles whose\n  state are updated by the command.\n\n* [`Self::p_discard_rectangles`] is a pointer to an array of [`crate::vk::Rect2D`]structures specifying discard rectangles.\n[](#_description)Description\n----------\n\nThe discard rectangle taken from element i of [`Self::p_discard_rectangles`]replace the current state for the discard rectangle at index[`Self::first_discard_rectangle`] + i, for i in [0,[`Self::discard_rectangle_count`]).\n\nThis command sets the state for a given draw when the graphics pipeline is\ncreated with [`crate::vk::DynamicState::DISCARD_RECTANGLE_EXT`] set in[`crate::vk::PipelineDynamicStateCreateInfo::p_dynamic_states`].\n\nValid Usage\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-firstDiscardRectangle-00585  \n   The sum of [`Self::first_discard_rectangle`] and [`Self::discard_rectangle_count`]**must** be less than or equal to[`crate::vk::PhysicalDeviceDiscardRectanglePropertiesEXT::max_discard_rectangles`]\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-x-00587  \n   The `x` and `y` member of `offset` in each [`crate::vk::Rect2D`]element of [`Self::p_discard_rectangles`] **must** be greater than or equal to`0`\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-offset-00588  \n   Evaluation of (`offset.x` + `extent.width`) in each[`crate::vk::Rect2D`] element of [`Self::p_discard_rectangles`] **must** not cause a\n  signed integer addition overflow\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-offset-00589  \n   Evaluation of (`offset.y` + `extent.height`) in each[`crate::vk::Rect2D`] element of [`Self::p_discard_rectangles`] **must** not cause a\n  signed integer addition overflow\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-viewportScissor2D-04788  \n   If this command is recorded in a secondary command buffer with[`crate::vk::CommandBufferInheritanceViewportScissorInfoNV::viewport_scissor2_d`]enabled, then this function **must** not be called.\n\nValid Usage (Implicit)\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-parameter  \n  [`Self::command_buffer`] **must** be a valid [`crate::vk::CommandBuffer`] handle\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-pDiscardRectangles-parameter  \n  [`Self::p_discard_rectangles`] **must** be a valid pointer to an array of [`Self::discard_rectangle_count`] [`crate::vk::Rect2D`] structures\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-recording  \n  [`Self::command_buffer`] **must** be in the [recording state](#commandbuffers-lifecycle)\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-commandBuffer-cmdpool  \n   The [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** support graphics operations\n\n* []() VUID-vkCmdSetDiscardRectangleEXT-discardRectangleCount-arraylength  \n  [`Self::discard_rectangle_count`] **must** be greater than `0`\n\nHost Synchronization\n\n* Host access to [`Self::command_buffer`] **must** be externally synchronized\n\n* Host access to the [`crate::vk::CommandPool`] that [`Self::command_buffer`] was allocated from **must** be externally synchronized\n\nCommand Properties\n\n|[Command Buffer Levels](#VkCommandBufferLevel)|[Render Pass Scope](#vkCmdBeginRenderPass)|[Supported Queue Types](#VkQueueFlagBits)|\n|----------------------------------------------|------------------------------------------|-----------------------------------------|\n|           Primary  <br/>Secondary            |                   Both                   |                Graphics                 |\n[](#_see_also)See Also\n----------\n\n[`crate::vk::CommandBuffer`], [`crate::vk::Rect2D`]\n"]
    #[doc(alias = "vkCmdSetDiscardRectangleEXT")]
    pub unsafe fn cmd_set_discard_rectangle_ext(&self, command_buffer: crate::vk1_0::CommandBuffer, first_discard_rectangle: u32, discard_rectangles: &[crate::vk1_0::Rect2DBuilder]) -> () {
        let _function = self.cmd_set_discard_rectangle_ext.expect(crate::NOT_LOADED_MESSAGE);
        let discard_rectangle_count = discard_rectangles.len();
        let _return = _function(command_buffer as _, first_discard_rectangle as _, discard_rectangle_count as _, discard_rectangles.as_ptr() as _);
        ()
    }
}
