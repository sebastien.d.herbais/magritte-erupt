#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_ANDROID_SURFACE_SPEC_VERSION")]
pub const KHR_ANDROID_SURFACE_SPEC_VERSION: u32 = 6;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_KHR_ANDROID_SURFACE_EXTENSION_NAME")]
pub const KHR_ANDROID_SURFACE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_KHR_android_surface");
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
pub const FN_CREATE_ANDROID_SURFACE_KHR: *const std::os::raw::c_char = crate::cstr!("vkCreateAndroidSurfaceKHR");
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/ANativeWindow.html)) · Basetype <br/> ANativeWindow - Android native window type\n[](#_c_specification)C Specification\n----------\n\nTo remove an unnecessary compile-time dependency, an incomplete type\ndefinition of [`crate::vk::ANativeWindow`] is provided in the Vulkan headers:\n\n```\n// Provided by VK_KHR_android_surface\nstruct ANativeWindow;\n```\n[](#_description)Description\n----------\n\nThe actual [`crate::vk::ANativeWindow`] type is defined in Android NDK headers.\n[](#_see_also)See Also\n----------\n\nNo cross-references are available\n"]
pub type ANativeWindow = std::ffi::c_void;
bitflags::bitflags! { # [doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidSurfaceCreateFlagsKHR.html)) · Bitmask of [`AndroidSurfaceCreateFlagBitsKHR`] <br/> VkAndroidSurfaceCreateFlagsKHR - Reserved for future use\n[](#_c_specification)C Specification\n----------\n\n```\n// Provided by VK_KHR_android_surface\ntypedef VkFlags VkAndroidSurfaceCreateFlagsKHR;\n```\n[](#_description)Description\n----------\n\n[`crate::vk::AndroidSurfaceCreateFlagBitsKHR`] is a bitmask type for setting a mask,\nbut is currently reserved for future use.\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AndroidSurfaceCreateInfoKHR`]\n"] # [doc (alias = "VkAndroidSurfaceCreateFlagsKHR")] # [derive (Default)] # [repr (transparent)] pub struct AndroidSurfaceCreateFlagsKHR : u32 { # [cfg (empty_bitflag_workaround)] const EMPTY_BITFLAG_WORKAROUND = 0 ; } }
#[doc = "<s>Vulkan Manual Page</s> · Bits enum of [`AndroidSurfaceCreateFlagsKHR`] <br/> "]
#[doc(alias = "VkAndroidSurfaceCreateFlagBitsKHR")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct AndroidSurfaceCreateFlagBitsKHR(pub u32);
impl AndroidSurfaceCreateFlagBitsKHR {
    #[inline]
    #[doc = "Converts this enum variant to the corresponding bitmask"]
    pub const fn bitmask(&self) -> AndroidSurfaceCreateFlagsKHR {
        AndroidSurfaceCreateFlagsKHR::from_bits_truncate(self.0)
    }
}
impl std::fmt::Debug for AndroidSurfaceCreateFlagBitsKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::khr_android_surface`]"]
impl crate::vk1_0::StructureType {
    pub const ANDROID_SURFACE_CREATE_INFO_KHR: Self = Self(1000008000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateAndroidSurfaceKHR.html)) · Function <br/> vkCreateAndroidSurfaceKHR - Create a slink:VkSurfaceKHR object for an Android native window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an Android native window, call:\n\n```\n// Provided by VK_KHR_android_surface\nVkResult vkCreateAndroidSurfaceKHR(\n    VkInstance                                  instance,\n    const VkAndroidSurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::AndroidSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nDuring the lifetime of a surface created using a particular[`crate::vk::ANativeWindow`] handle any attempts to create another surface for the\nsame [`crate::vk::ANativeWindow`] and any attempts to connect to the same[`crate::vk::ANativeWindow`] through other platform mechanisms will fail.\n\n|   |Note<br/><br/>In particular, only one [`crate::vk::SurfaceKHR`] **can** exist at a time for a given<br/>window.<br/>Similarly, a native window **cannot** be used by both a [`crate::vk::SurfaceKHR`] and`EGLSurface` simultaneously.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nIf successful, [`crate::vk::InstanceLoader::create_android_surface_khr`] increments the[`crate::vk::ANativeWindow`]’s reference count, and [`crate::vk::InstanceLoader::destroy_surface_khr`] will\ndecrement it.\n\nOn Android, when a swapchain’s `imageExtent` does not match the\nsurface’s `currentExtent`, the presentable images will be scaled to the\nsurface’s dimensions during presentation.`minImageExtent` is (1,1), and `maxImageExtent` is the maximum\nimage size supported by the consumer.\nFor the system compositor, `currentExtent` is the window size (i.e. the\nconsumer’s preferred size).\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateAndroidSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::AndroidSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::AndroidSurfaceCreateInfoKHR`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
#[allow(non_camel_case_types)]
pub type PFN_vkCreateAndroidSurfaceKHR = unsafe extern "system" fn(instance: crate::vk1_0::Instance, p_create_info: *const crate::extensions::khr_android_surface::AndroidSurfaceCreateInfoKHR, p_allocator: *const crate::vk1_0::AllocationCallbacks, p_surface: *mut crate::extensions::khr_surface::SurfaceKHR) -> crate::vk1_0::Result;
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidSurfaceCreateInfoKHR.html)) · Structure <br/> VkAndroidSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Android surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AndroidSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_android_surface\ntypedef struct VkAndroidSurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkAndroidSurfaceCreateFlagsKHR    flags;\n    struct ANativeWindow*             window;\n} VkAndroidSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::window`] is a pointer to the [`crate::vk::ANativeWindow`] to associate the\n  surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-window-01248  \n  [`Self::window`] **must** point to a valid Android [`crate::vk::ANativeWindow`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AndroidSurfaceCreateFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_android_surface_khr`]\n"]
#[doc(alias = "VkAndroidSurfaceCreateInfoKHR")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct AndroidSurfaceCreateInfoKHR {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub flags: crate::extensions::khr_android_surface::AndroidSurfaceCreateFlagsKHR,
    pub window: *mut crate::extensions::khr_android_surface::ANativeWindow,
}
impl AndroidSurfaceCreateInfoKHR {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::ANDROID_SURFACE_CREATE_INFO_KHR;
}
impl Default for AndroidSurfaceCreateInfoKHR {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), flags: Default::default(), window: std::ptr::null_mut() }
    }
}
impl std::fmt::Debug for AndroidSurfaceCreateInfoKHR {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("AndroidSurfaceCreateInfoKHR").field("s_type", &self.s_type).field("p_next", &self.p_next).field("flags", &self.flags).field("window", &self.window).finish()
    }
}
impl AndroidSurfaceCreateInfoKHR {
    #[inline]
    pub fn into_builder<'a>(self) -> AndroidSurfaceCreateInfoKHRBuilder<'a> {
        AndroidSurfaceCreateInfoKHRBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkAndroidSurfaceCreateInfoKHR.html)) · Builder of [`AndroidSurfaceCreateInfoKHR`] <br/> VkAndroidSurfaceCreateInfoKHR - Structure specifying parameters of a newly created Android surface object\n[](#_c_specification)C Specification\n----------\n\nThe [`crate::vk::AndroidSurfaceCreateInfoKHR`] structure is defined as:\n\n```\n// Provided by VK_KHR_android_surface\ntypedef struct VkAndroidSurfaceCreateInfoKHR {\n    VkStructureType                   sType;\n    const void*                       pNext;\n    VkAndroidSurfaceCreateFlagsKHR    flags;\n    struct ANativeWindow*             window;\n} VkAndroidSurfaceCreateInfoKHR;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::flags`] is reserved for future use.\n\n* [`Self::window`] is a pointer to the [`crate::vk::ANativeWindow`] to associate the\n  surface with.\n[](#_description)Description\n----------\n\nValid Usage\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-window-01248  \n  [`Self::window`] **must** point to a valid Android [`crate::vk::ANativeWindow`]\n\nValid Usage (Implicit)\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::ANDROID_SURFACE_CREATE_INFO_KHR`]\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-pNext-pNext  \n  [`Self::p_next`] **must** be `NULL`\n\n* []() VUID-VkAndroidSurfaceCreateInfoKHR-flags-zerobitmask  \n  [`Self::flags`] **must** be `0`\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AndroidSurfaceCreateFlagBitsKHR`], [`crate::vk::StructureType`], [`crate::vk::InstanceLoader::create_android_surface_khr`]\n"]
#[repr(transparent)]
pub struct AndroidSurfaceCreateInfoKHRBuilder<'a>(AndroidSurfaceCreateInfoKHR, std::marker::PhantomData<&'a ()>);
impl<'a> AndroidSurfaceCreateInfoKHRBuilder<'a> {
    #[inline]
    pub fn new() -> AndroidSurfaceCreateInfoKHRBuilder<'a> {
        AndroidSurfaceCreateInfoKHRBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn flags(mut self, flags: crate::extensions::khr_android_surface::AndroidSurfaceCreateFlagsKHR) -> Self {
        self.0.flags = flags as _;
        self
    }
    #[inline]
    pub fn window(mut self, window: &'a mut crate::extensions::khr_android_surface::ANativeWindow) -> Self {
        self.0.window = window as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> AndroidSurfaceCreateInfoKHR {
        self.0
    }
}
impl<'a> std::default::Default for AndroidSurfaceCreateInfoKHRBuilder<'a> {
    fn default() -> AndroidSurfaceCreateInfoKHRBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for AndroidSurfaceCreateInfoKHRBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for AndroidSurfaceCreateInfoKHRBuilder<'a> {
    type Target = AndroidSurfaceCreateInfoKHR;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for AndroidSurfaceCreateInfoKHRBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
#[doc = "Provided by [`crate::extensions::khr_android_surface`]"]
impl crate::InstanceLoader {
    #[inline]
    #[track_caller]
    #[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkCreateAndroidSurfaceKHR.html)) · Function <br/> vkCreateAndroidSurfaceKHR - Create a slink:VkSurfaceKHR object for an Android native window\n[](#_c_specification)C Specification\n----------\n\nTo create a [`crate::vk::SurfaceKHR`] object for an Android native window, call:\n\n```\n// Provided by VK_KHR_android_surface\nVkResult vkCreateAndroidSurfaceKHR(\n    VkInstance                                  instance,\n    const VkAndroidSurfaceCreateInfoKHR*        pCreateInfo,\n    const VkAllocationCallbacks*                pAllocator,\n    VkSurfaceKHR*                               pSurface);\n```\n[](#_parameters)Parameters\n----------\n\n* [`Self::instance`] is the instance to associate the surface with.\n\n* [`Self::p_create_info`] is a pointer to a [`crate::vk::AndroidSurfaceCreateInfoKHR`]structure containing parameters affecting the creation of the surface\n  object.\n\n* [`Self::p_allocator`] is the allocator used for host memory allocated for the\n  surface object when there is no more specific allocator available (see[Memory Allocation](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#memory-allocation)).\n\n* [`Self::p_surface`] is a pointer to a [`crate::vk::SurfaceKHR`] handle in which the\n  created surface object is returned.\n[](#_description)Description\n----------\n\nDuring the lifetime of a surface created using a particular[`crate::vk::ANativeWindow`] handle any attempts to create another surface for the\nsame [`crate::vk::ANativeWindow`] and any attempts to connect to the same[`crate::vk::ANativeWindow`] through other platform mechanisms will fail.\n\n|   |Note<br/><br/>In particular, only one [`crate::vk::SurfaceKHR`] **can** exist at a time for a given<br/>window.<br/>Similarly, a native window **cannot** be used by both a [`crate::vk::SurfaceKHR`] and`EGLSurface` simultaneously.|\n|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n\nIf successful, [`crate::vk::InstanceLoader::create_android_surface_khr`] increments the[`crate::vk::ANativeWindow`]’s reference count, and [`crate::vk::InstanceLoader::destroy_surface_khr`] will\ndecrement it.\n\nOn Android, when a swapchain’s `imageExtent` does not match the\nsurface’s `currentExtent`, the presentable images will be scaled to the\nsurface’s dimensions during presentation.`minImageExtent` is (1,1), and `maxImageExtent` is the maximum\nimage size supported by the consumer.\nFor the system compositor, `currentExtent` is the window size (i.e. the\nconsumer’s preferred size).\n\nValid Usage (Implicit)\n\n* []() VUID-vkCreateAndroidSurfaceKHR-instance-parameter  \n  [`Self::instance`] **must** be a valid [`crate::vk::Instance`] handle\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pCreateInfo-parameter  \n  [`Self::p_create_info`] **must** be a valid pointer to a valid [`crate::vk::AndroidSurfaceCreateInfoKHR`] structure\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pAllocator-parameter  \n   If [`Self::p_allocator`] is not `NULL`, [`Self::p_allocator`] **must** be a valid pointer to a valid [`crate::vk::AllocationCallbacks`] structure\n\n* []() VUID-vkCreateAndroidSurfaceKHR-pSurface-parameter  \n  [`Self::p_surface`] **must** be a valid pointer to a [`crate::vk::SurfaceKHR`] handle\n\nReturn Codes\n\nOn success, this command returns\n\n* [`crate::vk::Result::SUCCESS`]\n\nOn failure, this command returns\n\n* [`crate::vk::Result::ERROR_OUT_OF_HOST_MEMORY`]\n\n* [`crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY`]\n\n* [`crate::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR`]\n[](#_see_also)See Also\n----------\n\n[`crate::vk::AllocationCallbacks`], [`crate::vk::AndroidSurfaceCreateInfoKHR`], [`crate::vk::Instance`], [`crate::vk::SurfaceKHR`]\n"]
    #[doc(alias = "vkCreateAndroidSurfaceKHR")]
    pub unsafe fn create_android_surface_khr(&self, create_info: &crate::extensions::khr_android_surface::AndroidSurfaceCreateInfoKHR, allocator: Option<&crate::vk1_0::AllocationCallbacks>) -> crate::utils::VulkanResult<crate::extensions::khr_surface::SurfaceKHR> {
        let _function = self.create_android_surface_khr.expect(crate::NOT_LOADED_MESSAGE);
        let mut surface = Default::default();
        let _return = _function(
            self.handle,
            create_info as _,
            match allocator {
                Some(v) => v,
                None => std::ptr::null(),
            },
            &mut surface,
        );
        crate::utils::VulkanResult::new(_return, surface)
    }
}
