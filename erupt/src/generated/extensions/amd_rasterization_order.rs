#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_RASTERIZATION_ORDER_SPEC_VERSION")]
pub const AMD_RASTERIZATION_ORDER_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant <br/> "]
#[doc(alias = "VK_AMD_RASTERIZATION_ORDER_EXTENSION_NAME")]
pub const AMD_RASTERIZATION_ORDER_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_AMD_rasterization_order");
#[doc = "Provided by [`crate::extensions::amd_rasterization_order`]"]
impl crate::vk1_0::StructureType {
    pub const PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD: Self = Self(1000018000);
}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRasterizationOrderAMD.html)) · Enum <br/> VkRasterizationOrderAMD - Specify rasterization order for a graphics pipeline\n[](#_c_specification)C Specification\n----------\n\nPossible values of[`crate::vk::PipelineRasterizationStateRasterizationOrderAMD::rasterization_order`],\nspecifying the primitive rasterization order, are:\n\n```\n// Provided by VK_AMD_rasterization_order\ntypedef enum VkRasterizationOrderAMD {\n    VK_RASTERIZATION_ORDER_STRICT_AMD = 0,\n    VK_RASTERIZATION_ORDER_RELAXED_AMD = 1,\n} VkRasterizationOrderAMD;\n```\n[](#_description)Description\n----------\n\n* [`Self::STRICT_AMD`] specifies that operations for\n  each primitive in a subpass **must** occur in [primitive order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#drawing-primitive-order).\n\n* [`Self::RELAXED_AMD`] specifies that operations for\n  each primitive in a subpass **may** not occur in [primitive order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#drawing-primitive-order).\n[](#_see_also)See Also\n----------\n\n[`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`]\n"]
#[doc(alias = "VkRasterizationOrderAMD")]
#[derive(Copy, Clone, PartialEq, Eq, Hash, Default, Ord, PartialOrd)]
#[repr(transparent)]
pub struct RasterizationOrderAMD(pub i32);
impl std::fmt::Debug for RasterizationOrderAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            &Self::STRICT_AMD => "STRICT_AMD",
            &Self::RELAXED_AMD => "RELAXED_AMD",
            _ => "(unknown variant)",
        })
    }
}
#[doc = "Provided by [`crate::extensions::amd_rasterization_order`]"]
impl crate::extensions::amd_rasterization_order::RasterizationOrderAMD {
    pub const STRICT_AMD: Self = Self(0);
    pub const RELAXED_AMD: Self = Self(1);
}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationStateRasterizationOrderAMD> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
impl<'a> crate::ExtendableFromConst<'a, PipelineRasterizationStateRasterizationOrderAMDBuilder<'_>> for crate::vk1_0::PipelineRasterizationStateCreateInfoBuilder<'a> {}
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationStateRasterizationOrderAMD.html)) · Structure <br/> VkPipelineRasterizationStateRasterizationOrderAMD - Structure defining rasterization order for a graphics pipeline\n[](#_c_specification)C Specification\n----------\n\nThe rasterization order to use for a graphics pipeline is specified by\nadding a [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`] structure\nto the [`Self::p_next`] chain of a [`crate::vk::PipelineRasterizationStateCreateInfo`]structure.\n\nThe [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`] structure is\ndefined as:\n\n```\n// Provided by VK_AMD_rasterization_order\ntypedef struct VkPipelineRasterizationStateRasterizationOrderAMD {\n    VkStructureType            sType;\n    const void*                pNext;\n    VkRasterizationOrderAMD    rasterizationOrder;\n} VkPipelineRasterizationStateRasterizationOrderAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::rasterization_order`] is a [`crate::vk::RasterizationOrderAMD`] value\n  specifying the primitive rasterization order to use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationStateRasterizationOrderAMD-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD`]\n\n* []() VUID-VkPipelineRasterizationStateRasterizationOrderAMD-rasterizationOrder-parameter  \n  [`Self::rasterization_order`] **must** be a valid [`crate::vk::RasterizationOrderAMD`] value\n\nIf the [VK_AMD_rasterization_order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_rasterization_order.html) device extension is not enabled or\nthe application does not request a particular rasterization order through\nspecifying a [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`]structure then the rasterization order used by the graphics pipeline\ndefaults to [`crate::vk::RasterizationOrderAMD::STRICT_AMD`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RasterizationOrderAMD`], [`crate::vk::StructureType`]\n"]
#[doc(alias = "VkPipelineRasterizationStateRasterizationOrderAMD")]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PipelineRasterizationStateRasterizationOrderAMD {
    pub s_type: crate::vk1_0::StructureType,
    pub p_next: *const std::ffi::c_void,
    pub rasterization_order: crate::extensions::amd_rasterization_order::RasterizationOrderAMD,
}
impl PipelineRasterizationStateRasterizationOrderAMD {
    pub const STRUCTURE_TYPE: crate::vk1_0::StructureType = crate::vk1_0::StructureType::PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD;
}
impl Default for PipelineRasterizationStateRasterizationOrderAMD {
    fn default() -> Self {
        Self { s_type: Self::STRUCTURE_TYPE, p_next: std::ptr::null(), rasterization_order: Default::default() }
    }
}
impl std::fmt::Debug for PipelineRasterizationStateRasterizationOrderAMD {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipelineRasterizationStateRasterizationOrderAMD").field("s_type", &self.s_type).field("p_next", &self.p_next).field("rasterization_order", &self.rasterization_order).finish()
    }
}
impl PipelineRasterizationStateRasterizationOrderAMD {
    #[inline]
    pub fn into_builder<'a>(self) -> PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
        PipelineRasterizationStateRasterizationOrderAMDBuilder(self, std::marker::PhantomData)
    }
}
#[derive(Copy, Clone)]
#[doc = "[Vulkan Manual Page](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPipelineRasterizationStateRasterizationOrderAMD.html)) · Builder of [`PipelineRasterizationStateRasterizationOrderAMD`] <br/> VkPipelineRasterizationStateRasterizationOrderAMD - Structure defining rasterization order for a graphics pipeline\n[](#_c_specification)C Specification\n----------\n\nThe rasterization order to use for a graphics pipeline is specified by\nadding a [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`] structure\nto the [`Self::p_next`] chain of a [`crate::vk::PipelineRasterizationStateCreateInfo`]structure.\n\nThe [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`] structure is\ndefined as:\n\n```\n// Provided by VK_AMD_rasterization_order\ntypedef struct VkPipelineRasterizationStateRasterizationOrderAMD {\n    VkStructureType            sType;\n    const void*                pNext;\n    VkRasterizationOrderAMD    rasterizationOrder;\n} VkPipelineRasterizationStateRasterizationOrderAMD;\n```\n[](#_members)Members\n----------\n\n* [`Self::s_type`] is the type of this structure.\n\n* [`Self::p_next`] is `NULL` or a pointer to a structure extending this\n  structure.\n\n* [`Self::rasterization_order`] is a [`crate::vk::RasterizationOrderAMD`] value\n  specifying the primitive rasterization order to use.\n[](#_description)Description\n----------\n\nValid Usage (Implicit)\n\n* []() VUID-VkPipelineRasterizationStateRasterizationOrderAMD-sType-sType  \n  [`Self::s_type`] **must** be [`crate::vk::StructureType::PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD`]\n\n* []() VUID-VkPipelineRasterizationStateRasterizationOrderAMD-rasterizationOrder-parameter  \n  [`Self::rasterization_order`] **must** be a valid [`crate::vk::RasterizationOrderAMD`] value\n\nIf the [VK_AMD_rasterization_order](https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_AMD_rasterization_order.html) device extension is not enabled or\nthe application does not request a particular rasterization order through\nspecifying a [`crate::vk::PipelineRasterizationStateRasterizationOrderAMD`]structure then the rasterization order used by the graphics pipeline\ndefaults to [`crate::vk::RasterizationOrderAMD::STRICT_AMD`].\n[](#_see_also)See Also\n----------\n\n[`crate::vk::RasterizationOrderAMD`], [`crate::vk::StructureType`]\n"]
#[repr(transparent)]
pub struct PipelineRasterizationStateRasterizationOrderAMDBuilder<'a>(PipelineRasterizationStateRasterizationOrderAMD, std::marker::PhantomData<&'a ()>);
impl<'a> PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
    #[inline]
    pub fn new() -> PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
        PipelineRasterizationStateRasterizationOrderAMDBuilder(Default::default(), std::marker::PhantomData)
    }
    #[inline]
    pub fn rasterization_order(mut self, rasterization_order: crate::extensions::amd_rasterization_order::RasterizationOrderAMD) -> Self {
        self.0.rasterization_order = rasterization_order as _;
        self
    }
    #[inline]
    #[doc = r" Discards all lifetime information."]
    #[doc = r" Use the `Deref` and `DerefMut` implementations if possible."]
    pub fn build(self) -> PipelineRasterizationStateRasterizationOrderAMD {
        self.0
    }
}
impl<'a> std::default::Default for PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
    fn default() -> PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
        Self::new()
    }
}
impl<'a> std::fmt::Debug for PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
impl<'a> std::ops::Deref for PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
    type Target = PipelineRasterizationStateRasterizationOrderAMD;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a> std::ops::DerefMut for PipelineRasterizationStateRasterizationOrderAMDBuilder<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
